<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Raspberry Pi WEB</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="MoST">

<!-- Bootstrap v3.3.2 CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Bootstrap v3.3.2 Responsive CSS -->
<link href="css/bootstrap-responsive.css" rel="stylesheet">

<!-- Font Awesome 4.2.0 CSS -->
<link href="css/font-awesome.css" rel="stylesheet">

<!-- Main Style CSS -->
<link href="css/styles.css" rel="stylesheet">

<!-- Add-on CSS -->
<link href="css/callout.css" rel="stylesheet">
<link href="css/kbd.css" rel="stylesheet">
<link href="css/panels.css" rel="stylesheet">

<!-- Material Design CSS -->
<link href="css/roboto.min.css" rel="stylesheet">
<link href="css/material.min.css" rel="stylesheet">
<link href="css/ripples.min.css" rel="stylesheet">

<!-- Bootstrap JavaScript -->
<script src="js/jquery-1.9.0.min.js"></script>
<script src="js/application.js"></script>

<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-dropdown.js"></script>

<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>

<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>

<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-carousel.js"></script>
<script src="js/bootstrap-typeahead.js"></script>

<!-- Cookies JavaScript -->
<script src="js/jquery.cookie.min.js"></script>

<!-- Material JavaScript -->
<script src="js/ripples.min.js"></script>
<script src="js/material.min.js"></script>
        <script>
            $(document).ready(function() {
                // This command is used to initialize some elements and make them work properly
                $.material.init();
            });
        </script>
</head>

<style type="text/css">

.container .jumbotron, .container-fluid .jumbotron
{
	margin-bottom: 120px;
}

</style>

<body>

<div class="container">
	
        <h1 class="text-muted"><i class="fa text-warning fa-cogs"></i> <a class="noDecoration" href="<?$_SERVER['HTTP_HOST']?>">Raspberry Pi Web-Panel</a></h1>
<?php
if ($_POST['delete']) {
	$filename = $_POST['delete'];
	
	if ($filename == 'all'){
	$dir=opendir('music/');
	while ($file=readdir($dir)){
	unlink ('music/'.$file); //Удалить все
	}
		print "
		 <div class='alert alert-dismissable alert-info'>
			<button type='button' class='close' data-dismiss='alert'>×</button>
			<strong>Видалено всі пісні !</strong>
		</div>
		";
	
	}
	else{
		if (file_exists('music/'.$filename)) {
		unlink ('music/'.$filename); //Удалить песню

		print "
		 <div class='alert alert-dismissable alert-info'>
			<button type='button' class='close' data-dismiss='alert'>×</button>
			<strong>$filename</strong> видалено.
		</div>
		";
		}
	}

}
?>

<ul class="nav nav-tabs well-material-light-green" style="margin-bottom: 15px;">
    <li class="active"><a href="#home" data-toggle="tab"><i class="fa fa-home"></i> Головна</a></li>
    <li><a href="#upload2" data-toggle="tab"><i class="fa fa-upload"></i> Завантажити</a></li>
    <li><a href="#files" data-toggle="tab"><i class="fa fa-list"></i> Список пісень</a></li>
    <li><a href="#about" data-toggle="tab"><i class="fa fa-info-circle"></i> Про проект</a></li>
</ul>
<div id="myTabContent" class="tab-content">
    <div class="tab-pane fade active in" id="home">

		<div class="progress">
		    <div class="progress-bar progress-bar-info" style="width: 25%"></div>
		</div>

		<div class="jumbotron">
		    <h1 class="page-header">Вітаємо!</h1>
		    <p>Вітаємо вас на веб-інтерфейсі аудіо-системи <code>RasPi Sound Box</code>.</p>
		    <p>Ви можете:</p>
		    <ul>
		    	<li class="text-warning">завантажувати</li>
		    	<li class="text-success">прослуховувати</li>
		    	<li class="text-info">видаляти</li>
		    	<li class="text-muted">скачувати пісні с фонотеки.</li>
		    </ul>
		</div>
  
    </div>

    <div class="tab-pane fade" id="upload2">

		<div class="progress">
		    <div class="progress-bar progress-bar-success" style="width: 50%"></div>
		</div>

 		<div class="jumbotron">
		    <h1 class="page-header">Завантаження пісень!</h1>

			<p>Будь ласка виберіть файли для завантаження, які відповідають критеріям:</p>

			<ul>
				<li>Доступні типи файлів: <b>.mp3</b></li>
				<li>Максимальний розмір завантажуваного файлу: <b>10 МБ.</b></li>
				<li>Кількість одночасних завантажень: <b>2.</b></li>
			</ul>


<link href="css/jquery.fs.dropper.css" rel="stylesheet">
<script src="js/jquery.fs.dropper.js"></script>


				<div class="upload">
					<form action="#" class="demo_form">
						<div class="dropped"></div>

						<div class="filelists">
							<h5>Завантажені</h5>
							<ol class="filelist complete">
							</ol>
							<h5>Помилка</h5>
							<ol class="filelist queue">
							</ol>
						</div>
					</form>
				</div>

<script>
			var $filequeue,
				$filelist;

			$(document).ready(function() {
				$filequeue = $(".upload .filelist.queue");
				$filelist = $(".upload .filelist.complete");

				$(".upload .dropped").dropper({
					action: "upload.php",
					label: "Перетягніть пісню для завантаження <i class='fa fa-music'></i>",
					maxQueue: 2, //Количество одновременых загрузок
					maxSize: 10048576 //10 MB
				}).on("start.dropper", onStart)
				  .on("complete.dropper", onComplete)
				  .on("fileStart.dropper", onFileStart)
				  .on("fileProgress.dropper", onFileProgress)
				  .on("fileComplete.dropper", onFileComplete)
				  .on("fileError.dropper", onFileError);

			
			});

			function onStart(e, files) {
				console.log("Start");

				var html = '';

				for (var i = 0; i < files.length; i++) {
					html += '<div data-index="' + files[i].index + '"><li><span class="file">' + files[i].name + '</span><span class="status">Черга</span></li><div class="progress"><div class="progress-bar progress-bar-info" style="width: 0%"></div></div></div>';
				}

				$filelist.append(html);
			}

			function onComplete(e) {
				console.log("Complete");
				// All done!
			}

			function onFileStart(e, file) {
				console.log("File Start");

				$filelist.find("div[data-index=" + file.index + "]")
						  .find(".progress .progress-bar").css('width','0%');
			}

			function onFileProgress(e, file, percent) {
				console.log("File Progress");

				$filelist.find("div[data-index=" + file.index + "]")
						  .find(".progress .progress-bar").css('width',percent + '%');

				$filelist.find("div[data-index=" + file.index + "]")
						  .find(".status").text(percent + "%");

			}

			function onFileComplete(e, file, response) {
				console.log("File Complete");

				$filelist.find("div[data-index=" + file.index + "]")
						  .find(".progress .progress-bar").removeClass('progress-bar-info').addClass('progress-bar-success');

				$filelist.find("div[data-index=" + file.index + "]").addClass("success").find(".status").html("Загружено <i class='fa fa-check'>");

				//alert(file.name);

				/*if (response.trim() === "" || response.toLowerCase().indexOf("error") > -1) {
					$filequeue.find("div[data-index=" + file.index + "]").addClass("error")
							  .find(".progress").text(response.trim());
				} else {
					var $target = $filequeue.find("div[data-index=" + file.index + "]");
					$target.find(".file").text(file.name);



					//$target.find(".progress").remove();
					//$target.appendTo($filelist);
				}*/
			}

			function onFileError(e, file, error) {
				console.log("File Error");

				$filelist.find("div[data-index=" + file.index + "]")
						  .find(".progress .progress-bar").css('width','100%').removeClass('progress-bar-info').addClass('progress-bar-danger');

				$filelist.find("div[data-index=" + file.index + "]").addClass("error").find(".status").html("Error: " + error +" <i class='fa fa-times'>");
				$filelist.find("div[data-index=" + file.index + "]").appendTo($filequeue);
			}

	
		</script>




		</div>

    </div>

    <div class="tab-pane fade" id="files">        

		<div class="progress">
		    <div class="progress-bar progress-bar-danger" style="width: 75%"></div>
		</div>

		<div class="jumbotron">
			<h1 class="page-header">Пісні!</h1>

			<!-- <p>Нижче представлено список пісень:</p> -->

<script type="text/javascript">
$(document).ready(function() {
	$('.delete').click(function(event) {
		a=$(this).closest('.action').find('.filename').val();
		$('.alert.alert-danger').html('<i class="fa fa-exclamation-triangle"></i> Ви впевнені що хочете видалити <b>'+a+'</b>?');
		$('#filename').val(a);
	});

	$('.confirm').click(function(event) {
		$('#form-delete').submit();
	});

});
</script>

<?php 
$dir= 'music';
$d = opendir($dir);
$files = array();
while ( false !== ($name = readdir($d)) ){
    if ($name == "." or $name == ".." )
        continue;
$files[] = $name;
}
//shuffle($files); //Перемешивание масива

//print_r($files);
if(empty($files)) {

		print "
		 <div class='alert alert-dismissable alert-danger'>
			<button type='button' class='close' data-dismiss='alert'>×</button>
			<strong>Не знайдено файлів.</strong>
		</div>
		";
		
}else{

?>

<div class="row">
<div class="col-md-12">
<div class="table-responsive">
        
<table id="mytable" class="table table-bordred table-striped">
                   
	       <thead>
	       
	       	<th>№</th>
	       	<th>Зображення</th>
			<th>Виконавець</th>
			<th>Назва</th>
			<th>Альбом</th>
			<th>Жанр</th>
			<th>Рік</th>
			<th class='action'>
			    <button class='delete btn btn-info' data-toggle='modal' data-target='#delete'><i class='fa fa-trash'></i></button>
			    <input class='filename' type='hidden' name='delete' value='all'>
			</th>
				
	       </thead>
    <tbody>

<?php
require_once('Zend/Media/Id3v2.php');

$count=0;

	for ($i = 0; $i <= (count($files)-1); $i++)
	{

	$id3 = new Zend_Media_Id3v2("$dir/$files[$i]");

	$cover = 'data:'.$id3->apic->mimeType.';charset=utf-8;base64,'.base64_encode($id3->apic->imageData); //Кодируем изображение в base64
	$performer = $id3->tpe1->text; //Исполнитель
	$title = $id3->tit2->text; //Название
	$album = $id3->talb->text; //Альбом
	$genre = $id3->tcon->text; //Жанр

	$year = $id3->tyer->text; //Год
	$year = substr($year, 2); //Год 4 цыфры

	$count++;
	print "
	    <tr>
	    	<td><span class='badge'>$count</span></td>
	    	<td><img src='$cover' width='100px'></td>
		    <td>$performer</td>
		    <td>$title</td>
		    <td>$album</td>
		    <td>$genre</td>
		    <td>$year</td>
			    <td class='action'>
			    <button class='delete btn btn-danger' data-toggle='modal' data-target='#delete'><i class='fa fa-trash'></i></button>
			    <input class='filename' type='hidden' name='delete' value='$files[$i]'>
			    </td>
	    
	    </tr>    
	";
	} 

}



closedir($d);
?>

    </tbody>
        
</table>

</div>
</div>
</div>

<!-- 					<div class="list-group">
						    <div class="list-group-item">
						        <div class="row-action-primary">
						            <i class="mdi-file-folder"></i>
						        </div>
						        <div class="row-content">
						            <div class="least-content">15m</div>
						            <h4 class="list-group-item-heading">Tile with a label</h4>
						            <p class="list-group-item-text">Donec id elit non mi porta gravida at eget metus.</p>
						        </div>
						    </div>
						    <div class="list-group-separator"></div>

					</div> -->


   		 </div>
    </div>

    <div class="tab-pane fade" id="about">
    	
		<div class="progress">
		    <div class="progress-bar" style="width: 100%"></div>
		</div>

 		<div class="jumbotron">
		    <h1 class="page-header">Дипломна робота!</h1>

			<p>Проект був задуманий й розроблений мною <code>Пацура Сергій</code> протягом 10 тижнів.</p>
			<p>Проект є частиною дипломної роботи (веб-інтерфейсом для керування файлів на сервері):</p>
			<p><kbd>Програмний модуль під Linux для віддаленого керування аудіоцентром через мобільний телефон</kbd></p>
			<p>Веб-інтерфейс реалізовано на мові <span class="text-primary">PHP</span>, за допомогою <span class="text-success">HTML</span>,<span class="text-info">CSS</span>,<span class="text-warning">Javascript.</span></p>

		</div>


    </div>

</div>

</div>

<footer>
	<div class="panel panel-default">
		<div class="panel-body">

			<div class="pull-left text-success">
			    Powered by <b>Bootstrap</b> & <b>Material Design</b> & <b>Font Awesome</b> <i class="fa fa-flag"></i>
			</div>
			<div class="pull-right text-info">
			    Design by <b><kbd>Serega</kbd> <code class="most-text">MoST</code></b>. Copyright © 2015 All Rights Reserved <i class="fa fa-exclamation-triangle"></i>

			</div>

		</div>
	</div>
</footer>


	<!-- Модальное окно для удаления -->
	<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
	  <div class="modal-dialog">
	<div class="modal-content">
	      <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
	    <h4 class="modal-title custom_align" id="Heading">Видалити пісню:</h4>
	  </div>
	      <div class="modal-body">
	   
	   <div class="alert alert-danger"><i class="fa fa-exclamation-triangle"></i> Ви впевнені що хочете видалити <b> </b>?</div>
	   
		<form id="form-delete" method='post'>
			<!-- <input class='filename' type='hidden' name='delete' value=''> -->
			<input id='filename' type='hidden' name='delete'>
		</form>

	  </div>
	    <div class="modal-footer ">
	    <button type="button" class="confirm btn btn-raised btn-info" ><i class="fa fa-check-square-o"></i> Так</button>
	    <button type="button" class="btn btn-default btn-raised" data-dismiss="modal"><i class="fa fa-minus-square"></i> Ні</button>
	  </div>
	    </div>
	<!-- /.modal-content --> 
	</div>
	  <!-- /.modal-dialog --> 
	</div>

</body>
</html>