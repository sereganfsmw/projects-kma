//Поиск
Ext.define('TutorialApp.view.main.Search', {
    extend: 'Ext.form.field.Trigger',
    alias: 'widget.search',
    triggerCls: 'x-form-clear-trigger',
    trigger2Cls: 'x-form-search-trigger',
    onTriggerClick: function() {
        this.setValue('')
        this.setFilter(this.up().dataIndex, '')
    },
    onTrigger2Click: function() {
        this.setFilter(this.up().dataIndex, this.getValue())
    },
    setFilter: function(filterId, value){
        var store = this.up('grid').getStore();
        var field = Ext.getCmp('combo').getValue();
        //alert(field);
        //Ext.getCmp('combo').getValue()
        if(value!==''){
            store.removeFilter(filterId, false)
            var filter = {id: filterId, property: filterId, value: value};
            if(this.anyMatch) filter.anyMatch = this.anyMatch
            if(this.caseSensitive) filter.caseSensitive = this.caseSensitive
            if(this.exactMatch) filter.exactMatch = this.exactMatch
            if(this.operator) filter.operator = this.operator
            console.log(this.anyMatch, filter);
            //store.filter(name, value, false, false);
            store.filter(field, value, false, false);
            //Добавить фильр
            store.addFilter(filter)
            //alert(filterId);
        } else {

            //this.up('grid').getView().refresh()
                //store.remoteFilter = false;
                
                //store.remoteFilter = true;

                //Очистить фильтры и вывести все записи
                store.clearFilter();
                store.filter();
            //store.removeFilter(last_filter);
            //store.clearFilters();

            //console.log(this.up('grid'));
/*filters.filters.each(function (filter) {
    filter.setActive(false);
    filter.setValue("");
});*/            



            //alert(store.filters);
            console.log(store.filters);
            store.clearFilter(true);
            console.log(store.filters);
            //alert(store.filters);
            //store.removeFilter(false);
            //store.filters.removeAtKey(filterId);
            //store.removeFilter(filterId);
            /*store.reload();
            store.load();*/
            //alert('Пользователей не найдено.');
        }
    },
    listeners: {
        render: function(){
            var me = this;
            me.ownerCt.on('resize', function(){
                me.setWidth(this.getEl().getWidth())
            })
        },
        change: function() {
            //if(this.autoSearch) this.setFilter(this.up().dataIndex, this.getValue())
                if(this.autoSearch) this.setFilter(this.up().dataIndex, this.getValue())
        }
    }
})