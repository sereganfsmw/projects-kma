var pages = Ext.create('Ext.data.Store', {
    data : [
       // {"field":"id"},
        {"field":"index"},
        {"field":"files"},
        {"field":"about"},        
        {"field":"contacts"}        
    ]
});

//Форма для добавления пользователей
Ext.define('TutorialApp.view.main.Add', {
    extend: 'Ext.form.Panel',
    xtype: 'mainAdd',

    title: 'Добавить новый баннер:',
    //width: 500,
    bodyPadding: 10,
    
    requires: [
        'TutorialApp.view.main.AddController',
        'TutorialApp.store.Banners',
        'TutorialApp.store.Pages',
    ],
    queryMode: 'local',

    controller: 'add',

    // defaultType: 'textfield',
    items: [
        {
            xtype: 'textfield',
            fieldLabel: 'Название',
            name: 'name',
            emptyText:'Введите название',
            id: 'name',
            width: 500,

            //Validators
            allowBlank: false, //Обезательное      
            blankText: "Поле обезательное",                   
            maxLength: 20,
            regex: /^[a-zA-Z0-9 ]{5,20}$/, //Имя пользователя (с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)
            regexText: 'Название банера на английском (минимальная длина 5 символом, максимальная 20 символов, которыми могут быть буквы и цифры)',
            msgTarget: 'side' 
        },

        {
            xtype: 'textfield',
            fieldLabel: 'Пользователь',
            name: 'user',
            emptyText:'Введите пользователя',
            id: 'user',
            width: 500,       
            msgTarget: 'side',

            //Validators
            allowBlank: false, //Обезательное
            blankText: "Поле обезательное",     
            maxLength: 20,     
            regex: /^[a-zA-Z][a-zA-Z0-9-_ \.]{1,20}$/, //Имя пользователя (с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)
            regexText: 'Имя пользователя на английском (с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)',
        },
        {
            xtype     : 'textareafield',
            height: 400,
            width: '100%',
            //grow      : true,
            fieldLabel: 'Содержимое',
            name: 'content',
            emptyText:'Введите текст (содержимое) банера',
            id: 'content',
            msgTarget: 'side',    

            //Validators            
            allowBlank: false, //Обезательное
            blankText: "Поле обезательное",    
        },
        {
            xtype: 'datefield',
            fieldLabel: 'Показывать с',
            name: 'date_from',            
            emptyText:'Выберите дату',
            id: 'date_from',    
            width: 500,      
            value: new Date(),
            minValue: new Date(),   
            
            //Validators
            editable: false,
            format: 'Y-m-d',
            //format: 'Y-m-d H:i:s',
                listeners: {change: function(el,type, value) {
                    var from=Ext.getCmp('date_from').getValue();
                    //alert(from);
                    Ext.getCmp('date_to').setMinValue(Ext.Date.add(from, Ext.Date.DAY, +1));
                    Ext.getCmp('date_to').setValue(Ext.Date.add(from, Ext.Date.DAY, +1));
                    Ext.getCmp('date_to').enable();
                }                         
            }
        },   
        {
            xtype: 'datefield',
            fieldLabel: 'Показывать до',
            name: 'date_to',            
            emptyText:'Выберите дату',
            id: 'date_to',    
            width: 500,     
            disabled: true, //Отключить елемент
            value: Ext.Date.add(new Date(), Ext.Date.DAY, +1),
            //minValue: Ext.Date.add(new Date(), Ext.Date.DAY, +1),
            //maxValue: new Date(),   
            format: 'Y-m-d',
            //format: 'Y-m-d H:i:s',
        },          
        {
            xtype: 'combobox',
            multiSelect: true,
            fieldLabel: 'Страницу:',
            id:'pages',
            name: 'pages', 
            width: 500,
            itemSelector: 'pages',
            value:'index',
            store: 'Pages',
            valueField:'page',
            displayField:'page',
            queryMode:'local',

            //Validators
            editable: false, //Нельзя редактировать только выбор со списка          
        },
        {
            xtype: 'checkbox',
            fieldLabel: 'Enable',
            header: 'Enable',
            id: 'checkbox',
            name: 'checkbox',
            width: 65,
        },
        {
            xtype: 'button',
            text: 'Заполнить',

            handler: function(btn) {
                //Заполнить поля данными
                Ext.getCmp('name').setValue('Banner name');
                Ext.getCmp('user').setValue('User');
                Ext.getCmp('content').setValue('<H1>HTML</H1>');
            }
        },

        {
            xtype : 'hiddenfield',
            name  : 'id',
            id: 'id'
        },

    ],
    
    buttons: [
        /*{
            text: 'Заполнить',
            formBind: true,
            listeners: {
                click: 'onFill'
            },
        },*/
        {
            text: 'Добавить',
            formBind: true,
            listeners: {
                click: 'onSave'
            },
        }

        ]
});