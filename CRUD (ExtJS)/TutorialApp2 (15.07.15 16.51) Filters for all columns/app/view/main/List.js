//Значение combobox
var states = Ext.create('Ext.data.Store', {
    data : [
       // {"field":"id"},
        {"field":"name"},
        {"field":"user"},
        {"field":"content"},
        {"field":"pages"}
    ]
});
/*
var editorGridSelectionModel = Ext.create('Ext.selection.CheckboxModel',{
        checkOnly: true,
        hideable : false,
        draggable: false,
        mode: 'SIMPLE'
});*/

//Главный список пользователей, обработка удаления пользователей
Ext.define('TutorialApp.view.main.List', {

    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',


    requires: [
        'TutorialApp.store.Banners',
        'TutorialApp.store.Pages',
        'TutorialApp.view.main.Search',
    ],
    //selModel: editorGridSelectionModel,
    scroll: 'vertical',


    title: 'Список баннеров:',


    store: 'Banners',
    id: 'user_grid',

    //height: 500,

    /*listeners:{
        afterRender: function() {
            Ext.getCmp('paging').doRefresh();
        }
    },*/

    tbar: [
        {
            xtype: 'combobox',
            //multiSelect: true,
            fieldLabel: 'Поле:',
            id:'combo',
            itemSelector: 'combo',
            value:'name',
            store: states,
            valueField:'field',
            displayField:'field',
            queryMode:'local',

            //Validators
            editable: false, //Нельзя редактировать только выбор со списка              
        },
    {
            //xtype: 'search',  //Старая функция поиска
            xtype: 'textfield',
            autoSearch: true,
            name: 'search',
            emptyText:'Введите имя для поиска',
            id: 'search',            
            msgTarget: 'side',
            listeners: {
                change: 'onChangeListener',
            }
    },


           /* {
                xtype: 'datefield',
                fieldLabel: 'Показать с',           
                emptyText:'Выберите дату',
                id: 'start_value',     
                
                //Validators
                editable: false,
                format: 'Y-m-d', 
            }, 

            {
                xtype: 'datefield',
                fieldLabel: 'Показать до',         
                emptyText:'Выберите дату',
                id: 'end_value',     
                
                //Validators
                editable: false,
                format: 'Y-m-d', 
            }, */

    {
            xtype: 'checkboxfield',
            boxLabel: 'Enabled Pages',
            id : 'page_enabled',   
            //checked: true,        
    },

    ], 
    /*bbar: Ext.create('Ext.PagingToolbar', {
        //store: 'Banners',
        width: '95%',
        displayInfo: true,
        displayMsg: '{0} - {1} of {2}',
        emptyMsg: "No topics to display"
    }),*/
    dockedItems: [{
      xtype: 'pagingtoolbar',
      id:'paging',
      store: 'Banners',
      dock: 'bottom',
      displayInfo: true,

    }],

    //tbar lbar rbar bbar
    plugins: 'gridfilters',


    columns: [
        { text: 'ID',  width: 50, dataIndex: 'id', filter: {type: 'number'} },
        { text: 'Name',  width: '20%', dataIndex: 'name', filter: {type: 'string'} },
        { text: 'User', width: '10%', dataIndex: 'user', filter: {type: 'string'} },
        { text: 'Content', width: '20%', dataIndex: 'content' },
        { text: 'Pages', width: '20%', dataIndex: 'pages', filter: {type: 'string'} },
        { text: 'Date from',  xtype: 'datecolumn', dataIndex: 'date_from', filter: {type: 'date'}, renderer:  Ext.util.Format.dateRenderer('Y-m-d')},
        { text: 'Date to', xtype: 'datecolumn', dataIndex: 'date_to', filter: {type: 'date'}, renderer:  Ext.util.Format.dateRenderer('Y-m-d')},    
 
       
       /* xtype: 'datefield',
        anchor: '100%',
        fieldLabel: 'From',
        name: 'from_date',
        maxValue: new Date()  // limited to the current date or prior
*/
   

        {
            xtype: 'checkcolumn',
            header: 'Enable',
            dataIndex: 'checkbox',
            width: 65,
            filter: {
                type: 'boolean',
            },


            /*checked: true,
            value: true,
            inputValue: '1',
            active: true,
            text: 'Active',
            isChecked: true,
            isChecked: 1,
            isChecked: "true",*/


            
            listeners: {
             'checkchange': function(comp, rowIndex, checked, eOpts){ 
                console.log(checked);
                console.log(this);
                    //Checkbox
                    if (checked==true) {alert('Enabled');}else{alert('Disabled');}
                }
            },        
        },
        /*{
            xtype: 'combobox',
            //multiSelect: true,
            fieldLabel: 'Поле:',
            id:'combo2',
            itemSelector: 'combo',
            value:name,
            store: states,
            valueField:'field',
            displayField:'field',
            queryMode:'local'
        },*/

        {
            xtype:'actioncolumn', 
            width:50,
            items: [{
                icon: 'images/edit.png',  // Use a URL in the icon config
                tooltip: 'Edit',
                handler: function(grid, rowIndex, colIndex) {
                    //Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);

                    //Номер текущей строчки
                    //alert(rowIndex);

                    //Получить текущую запись
                    rec = grid.getStore().getAt(rowIndex);
                    /*var rec = grid.getStore().getAt(rowIndex);
                    console.log(rec.get("id"));
                    console.log(rec);*/

                    name=rec.get('name');
                    user=rec.get('user');
                    content=rec.get('content');
                    date_from=rec.get('date_from');
                    date_to=rec.get('date_to');
                    pages=rec.get('pages');
                    id=rec.get('id');

                    //alert("Edit " + rec.get('id'));
                    
//Форма
//new Ext.Window({
win=Ext.create('Ext.Window',{
    title: "Изменить данные: ID-"+id,
    border: false,
    modal: true,
    width: '50%',
    //height: 200,
    layout:'fit',
    items: [
        //new Ext.form.FormPanel({
            formPanel=Ext.create('Ext.form.Panel',{

            frame: true,
            padding: 10,
            name: "updateForm",
            id: "updateForm",
            items: [{
                    xtype: 'textfield',
                    width: '100%',
                    fieldLabel: 'Название',
                    name: 'form_name',
                    value: name,
                    id: 'form_name',
                    msgTarget: 'side', 

                    //Validators
                    allowBlank: false, //Обезательное      
                    blankText: "Поле обезательное",                   
                    maxLength: 20,
                    regex: /^[a-zA-Z0-9 ]{5,20}$/, //Имя пользователя (с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)
                    regexText: 'Название банера на английском (минимальная длина 5 символом, максимальная 20 символов, которыми могут быть буквы и цифры)',
                    msgTarget: 'side'                     
            },{
                    xtype: 'textfield',
                    width: '100%',
                    fieldLabel: 'Пользователь',
                    name: 'form_user',
                    value: user,
                    id: 'form_user',
                    msgTarget: 'side', 

                    //Validators
                    allowBlank: false, //Обезательное
                    blankText: "Поле обезательное",     
                    maxLength: 20,     
                    regex: /^[a-zA-Z][a-zA-Z0-9-_ \.]{1,20}$/, //Имя пользователя (с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)
                    regexText: 'Имя пользователя на английском (с ограничением 2-20 символов, которыми могут быть буквы и цифры, первый символ обязательно буква)',                    
            },{
                    /*xtype: 'textfield',
                    fieldLabel: content,
                    name: 'form_content',
                    value: content,
                    id: 'form_content',
                    regex: /[0-9,-]+/,
                    msgTarget: 'side' */

                    xtype     : 'textareafield',
                    height: 400,
                    width: '100%',
                    //grow      : true,
                    fieldLabel: 'Содержимое',
                    name: 'form_content',
                    value: content,
                    id: 'form_content',
                    //regex: /[0-9,-]+/,
                    msgTarget: 'side',

                    //Validators            
                    allowBlank: false, //Обезательное
                    blankText: "Поле обезательное",  
            },
            {
                xtype: 'datefield',
                fieldLabel: 'Показывать с',
                name: 'form_date_from',            
                emptyText:'Выберите дату',
                id: 'form_date_from',    
                width: 500,      
                value: date_from,
                minValue: new Date(),   
                
                //Validators
                editable: false,
                format: 'Y-m-d',
                //format: 'Y-m-d H:i:s',
                    listeners: {change: function(el,type, value) {
                        var from=Ext.getCmp('form_date_from').getValue();
                        //alert(from);
                        Ext.getCmp('form_date_to').setMinValue(Ext.Date.add(from, Ext.Date.DAY, +1));
                        Ext.getCmp('form_date_to').setValue(Ext.Date.add(from, Ext.Date.DAY, +1));
                        Ext.getCmp('form_date_to').enable();
                    }                         
                }
            },           
            {
                xtype: 'datefield',
                fieldLabel: 'Показывать до',
                name: 'form_date_to',            
                emptyText:'Выберите дату',
                id: 'form_date_to',    
                width: 500,     
                value: date_to,    
                minValue: date_from,    

                format: 'Y-m-d',
                allowBlank: false, //Обезательное
            },                 
            {
                    xtype: 'combobox',
                    width: '100%',
                    multiSelect: true,
                    fieldLabel: 'Pages:',
                    id:'form_pages',
                    name: 'form_pages', 
                    value: pages,  
                    itemSelector: 'form_pages',
                    store: 'Pages',
                    valueField:'page',
                    displayField:'page',
                    queryMode:'local',

                    //Validators
                    editable: false, //Нельзя редактировать только выбор со списка                      
            },
            {
                    xtype : 'hiddenfield',
                    name  : 'form_id',
                    value: id,
                    id: 'form_id'
            },
            ]
        })
    ],
        buttons: [{
            text: 'Изменить',
            formBind: true,
            handler: function() {

                //var form = Ext.getCmp('updateForm');
                values = formPanel.getValues();
                console.log(values);

                console.log();

        if (values.form_name=='' || values.form_user=='' || values.form_content=='') {
            Ext.Msg.alert('Ошибка', 'Поле не может быть пустым!');
        }else{
                //Обновление даних
                //alert(rowIndex);
                var store = Ext.getStore('Banners');
                store.getAt(rowIndex).set('name',values.form_name);
                store.getAt(rowIndex).set('user',values.form_user);
                store.getAt(rowIndex).set('content',values.form_content);
                store.getAt(rowIndex).set('date_from',values.form_date_from);
                store.getAt(rowIndex).set('date_to',values.form_date_to);
                store.getAt(rowIndex).set('pages',values.form_pages);
                win.close();
            }

/*

                console.log(formPanel.getValues());
                console.log('Form');

                
                //id = form.getRecord().get('id');
                console.log('Hello there is ID');
                console.log(id);
                console.log(form);
                console.log('ololo');



                var form_data = form.getValues();
                console.log(form_data);

        //see if the record exists
        var store = Ext.getStore('Banners');
        //console.log(data.id);
        var record = store.getById(form_data.id);
        record.load();

        if (!record) {
            Ext.Msg.alert('Error', 'Sorry, no record with that ID exists.');
            
            return;
        }*/
        
        //manually update the record
        //form.load();       

        //alert(id);         


        //get reference to the form
       // var form_values = form.getValues(),.getForm();
        
        //get the form inputs
       // var data = form_values.getValues();
/*
            win    = button.up('window'),
            form   = win.down('form'),
            values = form.getValues(), */
          //  console.log(data);               


                // действие отправки
                //alert('1');


            }
        }/*, {
            text: 'Отмена',
            handler: function() {
                // Действие отмены
                alert('2');
                }
        }*/],

 });
win.show();
/*if (!win.isVisible()) { // ЕСЛИ окна еще нет - оно создается.
        win.show();
      }*/

//win.show();
/*

                Ext.create('Ext.window.Window', {

                    title: 'Hello',
                    height: 200,
                    width: 400,
                    layout: 'fit',
                    items: {  // Let's put an empty grid in just to illustrate fit layout
                    new

                        xtype: 'grid',
                        border: false,
                        columns: [{header: name}],                 // One header just for show. There's no data,
                        store: Ext.create('Ext.data.ArrayStore', {}) // A dummy empty data store
                    }
                }).show();*/
/*
Ext.create('Ext.form.Panel',{
        title: 'Форма авторизации',
        width: 300,
        height:200,
        layout: 'anchor',
        defaults: {
            anchor: '80%'
        },
        renderTo: Ext.getBody(),
        items:[{
                xtype: 'textfield',
                fieldLabel: 'Логин',
                name: 'login',
                labelAlign: 'top',
                cls: 'field-margin',
                flex: 1
               }, {
                xtype: 'textfield',
                fieldLabel: 'Пароль',
                name: 'password',
                labelAlign: 'top',
                cls: 'field-margin',
                flex: 1
              }],       
        // кнопки формы
        buttons: [{
            text: 'Оправить',
            handler: function() {
                // действие отправки
            }
        }, {
            text: 'Отмена',
            handler: function() {
                // действие отмены
                }
        }],
    }).show();*/






                   /* var rec = grid.getStore().getAt(rowIndex);
                    alert("Edit " + rec.get(name));*/
                    // var store = Ext.getStore('Banners');
                    // store.delete(data);
                    // alert(store.count());
                }
            },{
                icon: 'images/delete.png',
                tooltip: 'Delete',
                handler: function(grid, rowIndex, colIndex) {


                Ext.Msg.confirm('Удалить', 'Вы уверены что хотите удалить запись?', function (id) {
                if (id === 'yes') {

                var store = Ext.getStore('Banners');
                var rec = grid.getStore().getAt(rowIndex);
                    store.remove(rec);

                    alert('Запись удалена');
                //combo.setValue(combo.currVal);
                }
                }, this); 


                }                
            }]
        }
    ],


    listeners: {
        //select: 'onItemSelected',
        itemclick: 'onItemClick',
        afterRender: function() {

            Ext.getCmp('paging').doRefresh();

            var data = Ext.getStore('Banners');
            data.setPageSize(3);

        }
    }
});
