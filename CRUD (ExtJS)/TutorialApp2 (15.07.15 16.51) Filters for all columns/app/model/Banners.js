//Store для хранения данных
Ext.define('TutorialApp.model.Banners', {
    extend: 'Ext.data.Model',

    //alias: 'store.Banners',

    fields : [
        {
            name : 'name',
            type : 'string'
        },
        {
            name : 'user',
            type : 'string'
        },
        {
            name : 'content',
            type : 'string'
        },
        {
            name : 'id',
            type : 'string'
        },
        {
            name : 'checkbox',
            type : 'boolean'
        },
        {
            name : 'date_from',
            type : 'date'
        },
        {
            name : 'date_to',
            type : 'date'
        },        
        {
            name : 'pages',
            type : 'string'
        } 

    ],
});