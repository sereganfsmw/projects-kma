//Store для хранения данных
Ext.define('TutorialApp.store.Banners', {
    extend  : 'Ext.data.Store',

    alias: 'store.Banners',

    requires : [
        'TutorialApp.model.Banners',
        'Ext.data.proxy.LocalStorage',
        'Ext.grid.plugin.BufferedRenderer'
    ],
    
    storeId : 'Banners',
    model   : 'TutorialApp.model.Banners',

    //Количество записей на странице
    //pageSize : 3,
     loadCount: 3,

        buffered: true,
        pageSize: 5,
        trailingBufferZone: 3,
        leadingBufferZone: 3,
        purgePageCount: 0,
        scrollToLoadBuffer: 5,


    autoLoad: true, 
    autoSync: true, 


    //Прокси модель
    /*proxy: {
        enablePaging: true,
        //type: 'memory',
        type: 'localstorage',
        id  : 'Names'
    },*/


    proxy: {
        enablePaging: true,

        type: 'ajax',
        url: 'app/baners.php',
        api: {
            create: 'app/baners.php?action=create',
            read: 'app/baners.php?action=read',
            update: 'app/baners.php?action=update',
            destroy: 'app/baners.php?action=destroy'
        },
        jsonData: true,
     
        reader: {
            type: 'json',
            root: 'banners',
            totalProperty: 3,
        },

        writer: {
            type: 'json',
            //Передавать все поля
            writeAllFields: true,
            allowSingle: false,
            root: 'banners',
        },


    },

/*
    proxy: {
        type: 'jsonp',
        url: 'app/myJsonPhp.php', //this file should return json.
        reader: {
            root: 'data',
            
        }},*/


    
    /*data : [
        { 
            'id'    : "1",
            'name'  : 'First Baner',  
            'user' : 'Jean Luc',
            'content' : 'jeanluc.picard@enterprise.com<br>555-111-1224',
            'checkbox' : true,
            'date'  : '07/10/2015',
            'pages' : 'index'

        },
        {   
            'id'    : "2",
            'name'  : 'Second Baner',  
            'user' : 'Worf',
            'content' : 'worf.moghsson@enterprise.com<br>555-222-1234',
            'checkbox' : false,
            'date'  : '07/09/2015',
            'pages' : 'about'            
        },
        { 
            'id'    : "3",
            'name'  : 'Third Baner', 
            'user' : 'Deanna',
            'content' : 'deanna.troi@enterprise.com<br>555-222-1244',
            'checkbox' : true,
            'date'  : '07/08/2015',
            'pages' : 'files'            
        },
        { 
            'id'    : "4",
            'name'  : 'Fourth Baner', 
            'user' : 'Nick',
            'content' : 'Nick.smith@enterprise.com<br>555-222-1254',
            'checkbox' : false,
            'date'  : '07/06/2015',
            'pages' : 'contacts'            
        },
        { 
            'id'    : "5",
            'name'  : 'Fifth Baner', 
            'user' : 'Bill',
            'content' : 'Bill.kolinz@enterprise.com<br>555-222-1254',
            'checkbox' : true,
            'date'  : '07/06/2015',
            'pages' : 'index'            
        },
        { 
            'id'    : "6",
            'name'  : 'Sixth Baner', 
            'user' : 'Jim',
            'content' : 'Jim.white@enterprise.com<br>555-222-1254',
            'checkbox' : false,
            'date'  : '07/06/2015',
            'pages' : 'about'            
        },
                        { 
            'id'    : "7",
            'name'  : 'Seventh Baner', 
            'user' : 'Johnny',
            'content' : 'Johnny.bullet@enterprise.com<br>555-222-1254',
            'checkbox' : true,
            'date'  : '07/06/2015',
            'pages' : 'files'            
        }

    ]*/
});
