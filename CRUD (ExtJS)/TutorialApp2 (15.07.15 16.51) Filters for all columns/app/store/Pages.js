//Store для хранения данных
Ext.define('TutorialApp.store.Pages', {
    extend  : 'Ext.data.Store',

    alias: 'store.Pages',

    requires : [
        'TutorialApp.model.Pages'
    ],
    
    storeId : 'Pages',
    model   : 'TutorialApp.model.Pages',

    
    data : [
        { 
            'page' : 'index'

        },
        {   
            'page' : 'about'            
        },
        { 
            'page' : 'files'            
        },
        { 
            'page' : 'contacts'            
        },

    ]
});
