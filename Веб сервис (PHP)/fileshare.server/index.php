<?
//______________________________���� ���� ��������� ��������� ��� ��� ��� �������� � ��� ������ ��������� � ��������� ������______________________________
//���������� ���������� NuSOAP 
require_once('lib/nusoap.php');

//������� ��������� ������� 
$server = new soap_server;

//�����������  WSDL
$server->configureWSDL('Service WSDL Documentation By Serga [MoST]', 'urn:uploadwsdl');

//������������ ��������������� �����
$server->register('upload_file',                                 						//�����
	array('file' => 'xsd:string','location' => 'xsd:string','user' => 'xsd:string'),    //�������� ���������
	array('return' => 'xsd:string'),                            						//�������� ��������
	'urn:uploadwsdl',                                           						//������������ ����
	'urn:uploadwsdl#upload_file',                                						//��������
	'rpc',                                                       						//�����
	'encoded',                                                   						//������������
	'�������� ������ �� ������ by Serega MoST');                            			//��������


//������������ ��������������� �����
$server->register('clear_all',                                 					
	array('dir' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#clear_all',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'�������� ���� ������ � ������ ������������� by Serega MoST');                              			

//������������ ��������������� �����
$server->register('add_comment',                                 					
	array('user' => 'xsd:string','text' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#add_comment',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� ���������� ������������� by Serega MoST');

//������������ ��������������� �����
$server->register('delete_file',                                 					
	array('id' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#delete_file',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'������� ����� ������������� by Serega MoST'); 
	
//������������ ��������������� �����
$server->register('getuser',                                 					
	array('user' => 'xsd:string'),  
	array('Username' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#getuser',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� ����� ������������ by Serega MoST'); 
	
//������������ ��������������� �����
$server->register('getpass',                                 					
	array('pass' => 'xsd:string'),  
	array('Password' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#getpass',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� ������ ������������ � ������������ ���� by Serega MoST'); 
	
//������������ ��������������� �����
$server->register('cheackadmin',                                 					
	array('pass_md5' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#cheackadmin',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� �� ������ ������ by Serega MoST'); 
	
//������������ ��������������� �����
$server->register('add_user',                                 					
	array('user' => 'xsd:string','pass' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#add_user',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'����������� ������ ������������ by Serega MoST'); 
	
//������������ ��������������� �����
$server->register('proccess_name',                                 					
	array('filename' => 'xsd:string'),  
	array('translit' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#proccess_name',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� �������� ����� (��������������, ������ ���������) by Serega MoST'); 
	
//������������ ��������������� �����
$server->register('user_folder',                                 					
	array('folder' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#user_folder',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'������� ����� ������������ ��� �������� ������ �� ������� by Serega MoST'); 

//������������ ��������������� �����
$server->register('add_files',                                 					
	array('type' => 'xsd:string','file' => 'xsd:string','size' => 'xsd:string',
	'date' => 'xsd:string','time' => 'xsd:string','pass' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#add_files',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� ����� � ���� ����� �� ������� by Serega MoST');
	
//������������ ��������������� �����
$server->register('add_user_files',                                 					
	array('user' => 'xsd:string','type' => 'xsd:string','user_file' => 'xsd:string',
	'size' => 'xsd:string','date' => 'xsd:string','time' => 'xsd:string','pass' => 'xsd:string'),  
	array('return' => 'xsd:string'),                            					
	'urn:uploadwsdl',                                           				
	'urn:uploadwsdl#add_user_files',                                						
	'rpc',                                                       					
	'encoded',                                                   					
	'��������� ����� (������������) � ���� ����� �� ������� by Serega MoST');
	
//������� HTTP
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>