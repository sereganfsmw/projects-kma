<?
//���������� ���������� NuSOAP 
require_once('lib/nusoap.php');

//������� ��������� ������� 
$server = new soap_server;

//�����������  WSDL
$server->configureWSDL('Upload File', 'urn:uploadwsdl');

//������������ ��������������� �����
$server->register('upload_file',                                 						//�����
	array('file' => 'xsd:string','location' => 'xsd:string','user' => 'xsd:string'),    //�������� ���������
	array('return' => 'xsd:string'),                            						//��������� ���������
	'urn:uploadwsdl',                                           						//������������ ����
	'urn:uploadwsdl#upload_file',                                						//��������
	'rpc',                                                       						//�����
	'encoded',                                                   						//������������
	'�������� ������ �� ������ by Serega MoST'                              			//��������
);

//______________________________������������� ����� (������������)______________________________
    function upload_file($encoded,$filename,$user) {
		
		if(empty($user)) //��������� ������������
		{
        $location = "uploads\\".$filename;                           //����� ��� �������� ������
		}
		else
		{
        $location = "uploads\\".$user.'\\'.$filename;                //����� ��� �������� ������ (������������)
		}
		
        $current = file_get_contents($location);                     //�������� ���������� �����     
        $current = base64_decode($encoded);                          //������������ �������     
        file_put_contents($location, $current);                      //�������� ���� � �����      
        if($filename!="")
        {
            return "���� �������� �������.";                           
        }
        else
        {
            return "������.";
        }
    }

//������� HTTP
$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
$server->service($HTTP_RAW_POST_DATA);
?>