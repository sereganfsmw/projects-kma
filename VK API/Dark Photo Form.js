var head = document.children[0];
var style_vk = document.createElement('link');
style_vk.rel  = 'stylesheet';
style_vk.type = 'text/css';
style_vk.href = 'https://vk.com/css/al/photos.css';
head.appendChild(style_vk);

//Удаляем блоки дарк-боксов чтобы не мешали
if (document.contains(document.getElementById('box_layer_bg'))) {
            document.getElementById("box_layer_bg").remove();
}
if (document.contains(document.getElementById('box_layer_wrap'))) {
            document.getElementById("box_layer_wrap").remove();
}


var newDiv = document.createElement('div');
newDiv.id = 'Dark';

newDiv.innerHTML= `
<div id="box_layer_bg" class="fixed bg_dark" style="height: 643px; display: block;"></div>
<div id="box_layer_wrap" class="scroll_fix_wrap fixed" style="width: 914px; height: 643px; display: block;"><div id="box_layer" style="width: 896px;"><div id="box_loader" style="margin-top: 197.667px; display: none;"><div class="loader"></div><div class="back"></div></div><div class="popup_box_container box_dark" style="width: 410px; height: auto; margin-top: 95.6667px;"><div class="box_layout" onclick="__bq.skip=true;"><div class="box_title_wrap"><div class="box_x_button">Закрыть</div><div class="box_title">Создать альбом</div></div><div class="box_body" style="display: block; padding: 5px 20px;"><div class="photos_edit_data">
  <div class="photos_edit_header">Название</div>
  <input type="text" class="text photos_edit_title" id="new_album_title" maxlength="64" onchange="curBox().changed=true;" onkeyup="onCtrlEnter(event, cur.saveNewAlbum)">
  <div class="photos_edit_header">Описание</div>
  <textarea class="photos_edit_description" id="new_album_description" onchange="curBox().changed=true;" onkeyup="onCtrlEnter(event, cur.saveNewAlbum)" style="overflow: hidden; resize: none;"></textarea>
  <div class="photos_privacy_controls">
  <div class="photos_privacy_control">Кто может просматривать этот альбом? <a id="privacy_edit_album0" onclick="return Privacy.show(this, event, 'album0')">Все пользователи</a><span></span>.</div>
  <div class="photos_privacy_control">Кто может комментировать фотографии? <a id="privacy_edit_albumcomm0" onclick="return Privacy.show(this, event, 'albumcomm0')">Все пользователи</a><span></span>.</div>
</div>
</div></div><div class="box_controls_wrap" style="display: block;"><div class="box_controls"><table cellspacing="0" cellpadding="0" class="fl_r"><tbody><tr><td><button class="flat_button">Создать альбом</button></td></tr></tbody></table><div class="progress" id="box_progress3"></div><div class="box_controls_text"></div></div></div></div></div></div></div>
`;
document.getElementsByTagName('body')[0].appendChild(newDiv);