<div id="out" class="container"></div>



/**
 * ��� ������� http://hashcode.ru/questions/367945/
 */
var params = {
        owner_id: -59215951,
        count: 15,
        need_covers: 1,
        need_system: 1,
    };

function gotAlbums( data){
    var html, i, al;

    if( !data || !data.response) {
        console.log('VK error:', data); return;
    }
    html = '<div class="row">';
    for( i=0; i<data.response.length;i++) {
        al = data.response[i];
        html = html
            +'<div class="col-xs-6 col-md-3">'
            +'<a href="https://vk.com/album'+al.owner_id+'_'+al.aid+'" target="_blank"  class="thumbnail">'
            +'<img src="'+al.thumb_src+'" title="'+al.title+'" alt="">'
            +'</a></div>';
    }
    html = html + '</div>';
    $('#out').html( html);
}

function vkApi( method, params, callback) {
    var cb = 'cb_vkapi';
    $.ajax({
        url: 'https://api.vk.com/method/'+method,
        data: params,
        dataType: "jsonp",
        callback: cb,
        success: callback
    });
}

vkApi( 'photos.getAlbums', params, gotAlbums);
