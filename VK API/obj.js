//Обявляем ассоциативный массив (хэш), объект
obj = new Object();

obj['First'] = 1;
obj['Second'] = 'two'
obj['Third'] = true;
obj['Fourth'] = [2, 3]

//Обявляем ассоциативный массив (хэш), объект (Вариант 2)
obj.First = 1;
obj.Second = 'two'
obj.Third = true;
obj.Fourth = [2, 3]

//Обявляем ассоциативный массив (хэш), объект (Вариант 3)
var obj = {
	First: 1,
	Second: 'two',
	Third: true,
	Fourth: [2,3]
}

//Добавление елементов push()
//Object {First: 1, Second: "two", Third: true, Fourth: Array[2]}
obj.First = false;
obj.Fourth.push(4,5);
obj.Fourth.push('6','7');
//Object {First: false, Second: "two", Third: true, Fourth: Array[6]}

//Удаление елементов
delete obj.First;
delete obj.['Second'];

//Копирование объектов
var obj = {First: 1, Second: 'two',	Third: true, Fourth: [2,3]}
new_obj = new Object();
for (var key in obj) {
    new_obj[key] = obj[key];
}
console.log(new_obj); //Object {First: 1, Second: "two", Third: true, Fourth: Array[2]}

//Обход ассоциативного массива (выввод)
var obj = {First: 1, Second: 'two',	Third: true, Fourth: [2,3]}
for (var key in obj) {
    console.log(key+':'+obj[key])
}

//Поиск ключа в объекте
var obj = {First: 1, Second: 'two',	Third: true, Fourth: [2,3]}
if('First' in obj) {alert("Элемент с ключём First");}

//Поиск значения в объекте
var obj = {First: 1, Second: 'two',	Third: true, Fourth: [2,3]}
for (var key in obj)
{
    if(obj[key] == 'two') alert("Элемент с значением 'two'");
}

//Сортировка свойства (name) объекта
var items = [
  { name: 'Car', value: 10 },
  { name: 'Wait', value: 5 },
  { name: 'Do', value: 15 },
  { name: 'Apple', value: 2 },
  { name: 'Zero', value: 4 },
  { name: 'Banan', value: 7 }
];
items.sort(function (a, b) {
  if (a.name > b.name) {
    return 1;
  }
  if (a.name < b.name) {
    return -1;
  }
  return 0;
});