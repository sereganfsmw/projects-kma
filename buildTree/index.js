/*
@Author - Serega MoST [01.12.2018]
[Задача реализовать функционал, который будет конвертировать исходный массив rows в объект Tree и считать сумму для каждого уровня в этом объекте.]

При реализации я пользовался 2 библиотеками и 3 внешними функциями:
  -jQuery для работы с объектами
  -Lodash (_.extendWith / _.get / _.set)
    extendWith для объединения объектов а в частности для конфлитных ситуация при объединении индексных массивов (без перезаписи елементов) а именно добавления;
    get для получения значения объекта через переданный строковый (path) к елементу узла
    get для того чтобы установить значение через строку пути (path)
  -compare_arr для сравнения узлов 2 объектов
  -extendext это скорее расширения функционала функции (extend) в jQuery, добавляет 4 параметра (replace, concat, extend, и default)
  -objectRecursive вот это самая крутая функция для перебора каждого узла объекта, это лучшая реализация "Рекурсивного обхода многомерного объекта" что я когда либо встречал в Интернете, она позволяет контролировать обработку объекта на любом уровне вложености, и для каждого передает 5 параметров (текущий узел, значение, ключь, строковый путь и глубину)

В каждой интерестной части кода я оставил точки останова [debugger], так что их можно расскоментировать и просмотреть результат в любом участке скрипта.
*/

// ---------------------------------Дополнительные функции (Не мои)---------------------------------
//Рекурсивный обход многомерного объекта
(function(f){var h=Object.keys,t=f.isNaN;f.objectRecursive=function(k,f,u,l,m,d){l=1===l;m=!!m;d="number"!==typeof d||t(d)?100:d;var a=[[],0,h(k).sort(),k],q=[];do{var g=a.pop(),n=a.pop(),e=a.pop(),r=a.pop();for(q.push(g);n[0];){var c=n.shift(),b=g[c],p=r.concat(c),c=f.call(u,g,b,c,p,e);if(!0!==c)if(!1===c){a.length=0;break}else if(!(d<=e)&&b instanceof Object){if(-1!==q.indexOf(b)){if(m)continue;throw Error("Circular reference");}if(l)a.unshift(p,e+1,h(b).sort(),b);else{a.push(r,e,n,g);a.push(p,
e+1,h(b).sort(),b);break}}}}while(a[0]);return k}})(window);

//Функция для сравнения узлов объекта (Возвращает массив путей к измененному узлу в виде строк)
var compare_arr = function (a, b) {

  var result = {
    different: [],
    missing_from_first: [],
    missing_from_second: []
  };

  _.reduce(a, function (result, value, key) {
    if (b.hasOwnProperty(key)) {
      if (_.isEqual(value, b[key])) {
        return result;
      } else {
        if (typeof (a[key]) != typeof ({}) || typeof (b[key]) != typeof ({})) {
          //dead end.
          result.different.push(key);
          return result;
        } else {
          var deeper = compare_arr(a[key], b[key]);
          result.different = result.different.concat(_.map(deeper.different, (sub_path) => {
            return key + "." + sub_path;
          }));

          result.missing_from_second = result.missing_from_second.concat(_.map(deeper.missing_from_second, (sub_path) => {
            return key + "." + sub_path;
          }));

          result.missing_from_first = result.missing_from_first.concat(_.map(deeper.missing_from_first, (sub_path) => {
            return key + "." + sub_path;
          }));
          return result;
        }
      }
    } else {
      result.missing_from_second.push(key);
      return result;
    }
  }, result);

  _.reduce(b, function (result, value, key) {
    if (a.hasOwnProperty(key)) {
      return result;
    } else {
      result.missing_from_first.push(key);
      return result;
    }
  }, result);

  return result;
}
// ---------------------------------/Дополнительные функции (Не мои)---------------------------------

var Tree = {}; //Дерево

var tree_names = {}; //Массив с именнами узлов и суммой их значений
var tree_arr = []; //Скелетон (чистая структура) дерева
var tree_arr_nested = []; //Структура с вложенными ветками (без значений)

//Исходный массив
var rows = [
  { value:10, levels:['js','es5','jquery','dom-selector'] },
  { value:12, levels:['js','es5','jquery','dom-selector','event'] },
  { value:3, levels:['js','es5','jquery','easing'] },
  { value:5, levels:['js','es5','backbone','mvc'] },
  { value:13, levels:['js','es5','backbone','dom-selector'] },
  { value:18, levels:['js','es5','backbone','cool'] },
  { value:1, levels:['js','es6','angular','digest'] },
  { value:9, levels:['js','es6','angular','dependency-injection'] },
  { value:8, levels:['js','es6','vue','hipsers'] },
  { value:5, levels:['js','es6','react','virtual-dom'] },
  { value:4, levels:['js','es6','react','stateless-components'] },
  { value:7, levels:['js','es6','react','lifecycle'] },
  { value:2, levels:['css','less','bootstrap','themes'] },
  { value:8, levels:['css','less','foundation','components'] },
  { value:9, levels:['css','sass','semantic-ui','naming-convention'] },
  { value:11, levels:['css','css3+','positioning','grid'] },
  { value:6, levels:['css','css3+','positioning','calc'] },
  { value:20, levels:['html','html4','dom','events-bubling'] },
  { value:13, levels:['html','html4','display','block'] },
  { value:12, levels:['html','html5','dom','new-semantic'] },
  { value:17, levels:['html','html5','api','media'] },
  { value:8, levels:['html','html5','layout','flex'] },
];

//Добавляем значение узлов для дерева, путем перебора объекта с свойствами именни узлов и их значений
function tree_set_node_values(){

  objectRecursive(Tree, function(node, value, key, path, depth) {

      if (typeof node.name != 'undefined'){ //Если сейчас перебираеться узел у которого есть свойство 'name' а не массив children
        if (tree_names[node.name].unique){
          node.value = tree_names[node.name].obj[0].value; //Если значение уникальное то получить значение
        } else {
          var parent_node = _.get(Tree, path.slice(0,-3)); //Получаем родительский узел от текущего
          var parent_node_name = parent_node.name;
          
          jQuery.each(tree_names[node.name].obj, function(index, item) { //Проходим массив родителей
            if (item.parent == parent_node_name){ //Если родитель текущего узла совпадает с тем елементов что в массиве то получить его значение и установить для текущего узла
              node.value = item.value;
            }
          });
        }
      }

  }, null, null, true);

  //Вывод дерева в консоли
  print_tree(Tree);
}

function print_tree(){
  var print_tree = {name: Tree.name, children: Tree.children, value: Tree.value};

  console.log('Готовый результат, дерево:');
  console.log(Tree);
  //Форматированый вывод объекта в консоле
  console.log(JSON.stringify(print_tree, null, 4));
}

//Конкатенируем ветки верхнего уровня (3 шт. js/css/html)
function merge_neasted_tree_arr(arr){
  var merge_arr = []; //Пустой массив куда будут добавляться главные ветки

  jQuery.each(arr, function(index, val) { //Перебираем каждую ветку
    _.extendWith(merge_arr, val, function (a, b) { //Склеиваем пустой массив саждым елементом массива
      if (_.isArray(a)) {
        return a.concat(b); //Конкатенируем (Добавляем в конец)
      }
    }); 
  });

  //Присваиваем дереву склеинный массив 
  Tree = merge_arr;

  console.error('Вложенное дерево [без значений узлов]:');
  console.error(merge_arr);
  console.error('-------------');

  //Расставляем значение для узлов дерева
  tree_set_node_values();
}

function make_tree_from_arr(arr) {
    for (var obj = {}, ptr = obj, i = 0, j = arr.length; i < j; i++){
        ptr.name = arr[i]; //Присваиваем имя из каждого елемента массива
        ptr.children = [];
        if (i != arr.length - 1){
            ptr.children[0] = {name:arr[i]}; //Создаем массив children для каждого уровня
        } else {
          ptr.children = false; //Если больше нету дочерних то ставим заглушку false
        }
          ptr = (ptr.children[0] = {});
      }

    tree_arr.push(obj); //Добавляем в структуру получившийся объект
    return obj;
}

function concate_tree_arr(){
  var temp_tree_arr = []; //Пустой массив куда будут добавляться склеинные ветки

  for (var i = 0; i < tree_arr.length; i++) {
    merge_tree_nodes(temp_tree_arr, tree_arr[i]); //Склеиваем пустой массив с каждым елементом массива
  }
  //Добавляем последнюю ветку [html] в вложенное дерево
  tree_arr_nested.push(temp_tree_arr);

  console.warn('Вложенное дерево [нужно объеденить ветки, и расставить значения для них]:');
  console.warn(tree_arr_nested);
  console.warn('-------------');

  //Конкатенируем ветки верхнего уровня
  merge_neasted_tree_arr(tree_arr_nested);
}

//Глубокий поиск различия в 2 объектах
function deep_find_different_node(a, b){
  var a_copy = [...a]; //Делаем копии объектов (Чтобы небыло ссылок)
  var b_copy = [...b];

  var result_obj = {}; //Объект который будет возвращаться
  var c = []; //Этот массив будет служить когда будут конкатинироваться 2 узла а ему будет присваиваться значение
  var different_obj = compare_arr(a_copy,b_copy); //Объект результата сравнения
  var different_path = different_obj.different; //массив со строками разницы в узлах

  if (a_copy[0].name != b_copy[0].name){ //Эта проверка для того чтобы если разные именна у веток склеить их
    result_obj = a_copy.concat(b_copy);
    return result_obj;
  }
  
  jQuery.each(different_path, function(index, val) { //Перебираем массиив строк путей (Path)
    var temp_obj = {};

    var a_node = _.get(a_copy, val); //Получаем значение по строке пути
    var b_node = _.get(b_copy, val);
    var c_node = [];

    if (a_node == false){ //Если тут нету child то взять ее из другого узла (где она есть)
      temp_obj = _.set(a_copy, val, b_node);
    } else if (b_node == false){
      temp_obj = _.set(b_copy, val, a_node);
    } else if (typeof a_node === 'string' && typeof b_node === 'string'){ //Если разница в именнах узлов (а не в child массивах)
      var path_arr = val.split('.'); //Разбиваем строку на массив 
      if (path_arr.slice(-1) == 'name'){ //Проверяем если последнее свойство name, то убираем 0.children
        path_arr = path_arr.slice(0,-2);
        val = path_arr.join('.'); //И склеиваем массив обратно в строку
      }

      a_node = _.get(a_copy, val); //Получаем значение узла по уже новому пути
      b_node = _.get(b_copy, val);
      if (a_node[0].name == b_node[0].name){ //Проверяем что эта ветка не имеет одно и то же имя
        c_node = [...a_node]
      } else {
        c_node = [...b_node,...a_node]; //Если разные имення то склеиваем их, предварительно "распаковав" каждый из дочерних массивов
      }

      temp_obj = _.set(a_copy, val, c_node); //Устанавливаем новое значение (2 склеиных массива)    
    }

    result_obj = jQuery.extendext(true, 'extend', {}, result_obj, temp_obj); //Склеиваем каждый результат цыкла с результирующим объектом
  });
  return result_obj;
}

//Склеиваем пустой массив с каждым елементом массива (Функция изменяет объект первый параметр)
function merge_tree_nodes(first_arr, second_arr){
  var temp_obj = {};
  _.extendWith(first_arr, second_arr, function (a, b) {
    if (_.isArray(a)) { // Проверяем что текущий узел это массив
        //Если мы сравниваем ветки с одинаковыми именами, так 'js' == 'js', а не 'js' == 'css'
      if (a[0].name == b[0].name){
          temp_obj = deep_find_different_node(a,b); //То выполняем сравнение для каждой уникальной ветки чтобы склеить недостающие дочерние узлы
      } else {
        //Иначе бы добавляем ветку что пришла первым параметром, к примеру 'js' в "вложенный массив", чтобы можно было сравнивать ветки с одинаковыми именами
        let clone_arr = Object.assign([], first_arr); //Создаем копию объекта, чтобы небыло ссылки, иначе на каждой итерации значение будет перезаписываться
        tree_arr_nested.push(clone_arr);
        temp_obj = second_arr; //И возвращаем результатом функции вторую ветку, тоесть 'css', чтобы можно было дальше сравнивать css & css
      }
      return temp_obj;
    }
  }); 
}

//Получаем значение суммы для каждой строки и соответствующих елементов массива
function make_names_and_values(rows){
  console.log('Оригинальный массив, где каждая строка значение и массив:');
  console.log(rows);
  console.log('-------------');
  var total = 0; //Общая сумма всех узлов
  jQuery.each(rows, function(index, val) {
    if (typeof val == 'object'){
      let outer_value;

      jQuery.map(val, function(items, index) { //Перебираем отдельно значение вессов для строки
        if (index == 'value'){
          outer_value = items;
          total += items;
        }

        if (index == 'levels'){ //И отдельно массив с елементами ветки
      
          make_tree_from_arr(['root',...items]); //Делаем структуру (скелетон), рекурсивный объект с вложеными children значениями из елементов массива, вложеность идет нисходящая, те родитель сначала, а дальше дочерний узел

          items.forEach(function (name, i, arr) {
            let inner_value = outer_value; //Внутренее значение получено из "внешнего", внутрение сбрасываеться на каждой итерации, так как область видимости let (блок)
            let temp_obj = {unique:true, obj: []}; //Ставим что свойство уникальное, если будет повторяться то поставим в false
            // let temp_arr = [], temp_arr_value = [];
            // let temp_arr = [], temp_arr_value = [];
            let temp_arr = []
            let temp_obj_inner = {};
            //Создаем временный объект, который будет хранить свойство obj, в который будет записываться массив с елементами родителя и значение

            if (!(name in tree_names)){
              //Тут нужно добавлять родителя так как "dom-selector" повторяеться 2 раза
              if (i != 0){ //Если текущий елемент имеет родителя
                temp_obj_inner.parent = arr[i-1];             
              } else { //Если это и есть главный елемент
                temp_obj_inner.parent = arr[i];
              }
              temp_obj_inner.value = inner_value;
              temp_arr.push(temp_obj_inner);            
              temp_obj.obj = temp_arr;

              //Если в массиве еще нету свойства с таким именнем то добавить его
              tree_names[name] = temp_obj;
            } else {
              //Проверяем что это разные узлы так как "dom-selector" повторяеться 2 раза, и поэтому нужно проверить его родителя
              if (tree_names[name].unique){ //Если это значение еще не повторялось в другой ветке
                if (i != 0){
                  if (tree_names[name].obj[0].parent == arr[i-1]){
                    inner_value += tree_names[name].obj[0].value;
                    tree_names[name].obj[0].value = inner_value;
                  } else { //Найдено повторение второго "dom-selector"
                    temp_obj.unique = false; //Ставим флаг что свойство не уникальное
                    temp_arr = tree_names[name].obj; //Получаем все текущие узлы где повторяеться 
                    temp_obj_inner.parent = arr[i-1];
                    temp_obj_inner.value = inner_value;
                    temp_arr.push(temp_obj_inner);  //+ добавляем новое значение                  
                    temp_obj.obj = temp_arr; //Добавляем в массив все узлы где повторяеться

                    tree_names[name] = temp_obj;
                  }
                } else {
                  // Если уже есть до добавляем к нему значение суммы для текущей строки
                  if (tree_names[name].obj[0].parent == arr[i]){
                    inner_value += tree_names[name].obj[0].value;
                    tree_names[name].obj[0].value = inner_value;
                  }
                }               
              }
            }
          });
        }
      });
    }
  });

  //Инициализируем главный объект 'root'
  var root_temp_arr = [];
  var root_temp_obj = {unique:true, obj: []};
  var root_temp_obj_inner = {};
  root_temp_obj_inner.value = total; //Добавляем в массив, сумму всех узлов
  root_temp_obj_inner.parent = 'root';
  root_temp_arr.push(root_temp_obj_inner);
  root_temp_obj.obj = root_temp_arr;

  tree_names['root'] = root_temp_obj;

  console.warn('Скелетон (Рекурсивно созданая структура дерева):');
  console.warn(tree_arr);
  console.warn('-------------');

  console.info('Объект с весами (значениями) узлов:');
  console.info(tree_names);
  console.info('-------------');

  concate_tree_arr();
}

//Вызываем главную функцию откуда начинаеться алгоритм
make_names_and_values(rows);