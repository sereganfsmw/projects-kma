<?php
//Масштабирование изображения
//Функция работает с PNG, GIF и JPEG изображениями.
//Масштабирование возможно как с указаниями одной стороны, так и двух, в процентах или пикселях.
function resize($file_input, $file_output, $w_o, $h_o, $percent = false) {
	list($w_i, $h_i, $type) = getimagesize($file_input);
	if (!$w_i || !$h_i) {
		echo 'Невозможно получить длину и ширину изображения';
		return;
    }
    $types = array('','gif','jpeg','png');
    $ext = $types[$type];
    if ($ext) {
    	$func = 'imagecreatefrom'.$ext;
    	$img = $func($file_input);
    } else {
    	echo 'Некорректный формат файла';
		return;
    }
	if ($percent) {
		$w_o *= $w_i / 100;
		$h_o *= $h_i / 100;
	}
	if (!$h_o) $h_o = $w_o/($w_i/$h_i);
	if (!$w_o) $w_o = $h_o/($h_i/$w_i);
	$img_o = imagecreatetruecolor($w_o, $h_o);
	imagecopyresampled($img_o, $img, 0, 0, 0, 0, $w_o, $h_o, $w_i, $h_i);
	if ($type == 2) {
		return imagejpeg($img_o,$file_output,100);
	} else {
		$func = 'image'.$ext;
		return $func($img_o,$file_output);
	}
}

//Обрезка изображения
//Функция работает с PNG, GIF и JPEG изображениями.
//Обрезка идёт как с указанием абсоютной длины, так и относительной (отрицательной).
function crop($file_input, $file_output, $crop = 'square',$percent = false) {
	list($w_i, $h_i, $type) = getimagesize($file_input);
	if (!$w_i || !$h_i) {
		echo 'Невозможно получить длину и ширину изображения';
		return;
    }
    $types = array('','gif','jpeg','png');
    $ext = $types[$type];
    if ($ext) {
    	$func = 'imagecreatefrom'.$ext;
    	$img = $func($file_input);
    } else {
    	echo 'Некорректный формат файла';
		return;
    }
	if ($crop == 'square') {
		$min = $w_i;
		if ($w_i > $h_i) $min = $h_i;
		$w_o = $h_o = $min;
	} else {
		list($x_o, $y_o, $w_o, $h_o) = $crop;
		if ($percent) {
			$w_o *= $w_i / 100;
			$h_o *= $h_i / 100;
			$x_o *= $w_i / 100;
			$y_o *= $h_i / 100;
		}
    	if ($w_o < 0) $w_o += $w_i;
	    $w_o -= $x_o;
	   	if ($h_o < 0) $h_o += $h_i;
		$h_o -= $y_o;
	}
	$img_o = imagecreatetruecolor($w_o, $h_o);
	imagecopy($img_o, $img, 0, 0, $x_o, $y_o, $w_o, $h_o);
	if ($type == 2) {
		return imagejpeg($img_o,$file_output,100);
	} else {
		$func = 'image'.$ext;
		return $func($img_o,$file_output);
	}
}
//Функции для обработки фотографий

// Создаем подключение к серверу
//$db = mysql_connect ("localhost","mysql","mysql"); 
// Выбираем БД
//mysql_select_db ("upload",$db);

// Вытаскиваем необходимые данные
$file = $_POST['value'];
$name = $_POST['name'];
$user_dir = $_POST['user'];


//Директории для загрузки
$upload_dir = 'users/'.$user_dir.'/';


  if (!is_dir('users/'.$user_dir.'/')) //Проверить существует ли папка пользователя , если нет то...
  {
	mkdir('users/'.$user_dir.'/'); //Создать папку пользователя
  }

// Получаем расширение файла
$getMime = explode('.', $name);
$mime = end($getMime);

// Выделим данные
$data = explode(',', $file);

// Декодируем данные, закодированные алгоритмом MIME base64
$encodedData = str_replace(' ','+',$data[1]);
$file = base64_decode($encodedData); //decodedData

// Мы будем создавать произвольное имя!
//$randomName = substr_replace(sha1(microtime(true)), '', 12).'.'.$mime;
$fileName = 'avatar.'.$mime;
$fileName_thumbs = 'thumbs.'.$mime;

				$target = $upload_dir . $fileName; //Изображения
				$thumbs = $upload_dir . $fileName_thumbs; //Миниатюры
				
				file_put_contents($target , $file);
				
				//move_uploaded_file($decodedData, $target); //Перемешение изображения в папку	
				
				//Редактирование изображения
				crop($target, $thumbs); //Обрезание фотографии (Квадрат)
				resize($thumbs, $thumbs, 150, 150); //Уменьшение , маштабирование
				
?>