<?php
session_start(); //Начать сессию
//Роутер
//Author@: Serega MoST (02.02.2015)

class ctrlIndex extends ctrl {
    
    function index(){
        
        //$this->posts = $this->db->query("SELECT * FROM baners ORDER BY ctime DESC")->all();
        //echo "Hello Serega MoST";
        $this->out('home.php');
    }

    function baners(){
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");

        if ($this->user){
        //Показать банеры текущего пользователя
        $this->users = $this->db->query("SELECT * FROM users WHERE username = ?",$_COOKIE['username'])->all();
        $this->posts = $this->db->query("SELECT * FROM baners WHERE user = ? ORDER BY id ASC",$_COOKIE['username'])->all(); //Доделать для текущего пользователя
        }

        if ($this->admin){
        //Показать все банеры (Админ)
        $this->users = $this->db->query("SELECT * FROM admin WHERE login = ?",$_COOKIE['username'])->all();
        $this->posts = $this->db->query("SELECT * FROM baners ORDER BY id ASC")->all(); //Админ видит все записи
        }

        //Поиск банеров
        if(!empty($_POST)){
        $search='%'.$_POST['search'].'%';
        $this->posts = $this->db->query("SELECT * FROM baners WHERE title LIKE ?",$search)->all();
        }

        $this->out('baners.php');
    }
//________________________________________________________________________Get BANER ________________________________________________________________________
    function getBaners ($url,$user) {
        // header('Access-Control-Allow-Origin: http://tm-site');
        // header('Access-Control-Allow-Methods: GET, POST');
        // header('Access-Control-Allow-Headers: Content-Type, X-Requested-With');
        // header('Access-Control-Allow-Credentials: true');
        // header('x-frame-options: SAMEORIGIN');
        header('Content-Type: text/html');


        echo "<meta charset='UTF-8'>";
        echo "<link href='css/slider.css' rel='stylesheet'>";
        echo "<link href='css/font-awesome.css' rel='stylesheet'>";
        echo "<link href='css/bootstrap.css' rel='stylesheet'>";
        echo "<link href='css/stars-bootstrap.css' rel='stylesheet'>";
        echo "<link href='css/star-rating.css' rel='stylesheet'>";

        echo "<script type='text/javascript' src='js/jquery.js'></script>";

        echo "<script type='text/javascript' src='js/slider.js'></script>";
        echo "<script type='text/javascript' src='js/star-rating.js'></script>";

        print "
        <style>
        // html{
        // overflow: hidden;
        </style>
        ";

        //echo "<script>alert('Serega')</script>";

        print "
        <script>
        $(document).ready(function() {
            //$('body').css('background','red')
            //alert($('.control-slide').length) //Количество слайдов

            //Открыть сылку из iframe в новой вкладке
            $('a').attr('target', '_top');

              // $('#slider').hover(
              //   function() {
              //     $('.switch').show();
              //   }, function() {
              //     $('.switch').hide();}
              // );

        //window.parent.document; //Нужен CORS HEADER, (Кроссдоменый запрос)
        });
        </script>
        ";

        

        $this->posts = $this->db->query("SELECT * FROM baners WHERE user = ?",$user)->all();
        print "
        <div id='slider-wrap'>
            <div id='slider'>
            
        ";
        $count=0;
        foreach ($this->posts as $key => $value){
            if ($value['enable'] == 'true') { //Если банер включен

                $pieces = explode(",", $value['pages']);

                if ((in_array($url, $pieces)) OR ($value['pages'] == '*')) { //Если банер есть в списке страниц для показа (*-для всех страниц)

                    $curent_data = date("Y-m-d"); 
                    $baner_data_range = $this->db->query("SELECT * FROM baners WHERE ? BETWEEN ? AND ?",$curent_data,$value['data_start'],$value['data_end'])->all();
                    if(!empty($baner_data_range)) //Если дата оторажения банера есть в диапазоне между 'data_start' и 'data_end', (ЗАПРОС НЕ ПУСТОЙ)
                    {

                        $count++; //Количество вхождений
                        $comment_count = $this->db->query("SELECT count(postid) FROM comment WHERE postid=?",$value['id'])->assoc(); //Количество коментариев
                        $views_count = $this->db->query("SELECT views FROM baners WHERE id=?",$value['id'])->assoc(); //Количество просмотров
                    
                        print "
                        <!-- _________________________________________________________Слайд_________________________________________________________ -->
                            <div class='slide'>

                                <div class='slideheader switch'><i class='icon-user'></i> <b>Автор:</b> {$value['author']}, <i class='fa fa-clock-o'></i> <b>Время:</b> {$value['ctime']}, <i class='icon-calendar'></i> <b>Дата:</b> {$value['cdate']}, <i class='fa fa-comment'></i> <b>Коментарии:</b> <span class='badge'>{$comment_count['count(postid)']}</div>

                                <div class='slidecontent'><a class='banner-link' href='/?topic/{$value['id']}'><div class='post-in-slide' <!--style='margin:0px;padding: 20px 10px 20px 10px;height:100%;-->'>{$value['baner']}</div></a></div>

                                <div class='slidefooter switch'>
                                   <div class='slidestar'><input class='rating' type='number' value='{$value['stars']}' class='rating' min=0 max=5 step=0.1 data-size='xs' data-stars='5'/>
                                    <input type='hidden' class='postid' value='{$value['id']}'>
                                   </div>

                                    <div class='slideinfo switch'>
                                        <i class='fa fa-desktop'></i> <b>Банер:</b><a class='banner-title' href='/?topic/{$value['id']}'>{$value['title']} <i class='fa fa-chevron-right'></i></a> , 
                                        <i class='fa fa-eye'></i> <b>Просмотров:</b> <span class='badge'>{$views_count['views']}</span>
                                    </div>
                                </div>

                            </div>
                        <!-- _________________________________________________________Слайд_________________________________________________________ -->
                        ";

                    } //Если дата оторажения банера есть в диапазоне между 'data_start' и 'data_end', (ЗАПРОС НЕ ПУСТОЙ)

                }//Если банер есть в списке страниц для показа (*-для всех страниц)

            } //Если банер включен
        }

        print "
            </div>
        </div>
        ";
        if ($count=='0'){ //Если не найдено банеров показать картинку TemplateMonster
            echo "<a href='http://{$_SERVER['SERVER_NAME']}'><img style='width: 100%; height: 100%;' src='img/tm.jpg'></a>";
            echo "<script>$('#slider-wrap').remove(); </script>";
        }
    }
//________________________________________________________________________Get BANER ________________________________________________________________________
    function getJs ($user) {
        header('Content-Type: text/javascript');
        print "

        var script1 = document.createElement('script');
        script1.src = 'http://{$_SERVER['SERVER_NAME']}/js/jquery.js'; //jQuery 1.9
        document.documentElement.appendChild(script1);

        var script2 = document.createElement('script');
        script2.src = 'http://{$_SERVER['SERVER_NAME']}/js/jquery.cookie.js'; //jQuery cookies
        document.documentElement.appendChild(script2);

            script2.onload = function() {

                $(document).ready(function() {
                
                    //Куки, посещения страниц пользователем
                    if(!$.cookie('views_count')) 
                    {
                        views_count=1;
                        $.cookie('views_count', views_count, { expires: 7 });
                    }
                    else
                    {
                        views_count=$.cookie('views_count');
                        views_count++;
                        $.cookie('views_count', views_count, { expires: 7 });
                        //alert(views_count);
                    }

                    //Минимальное количество просмотров страниц, после которого показываем банер 5
                    if (views_count>5) {

                        var body =  $('body');
                        //Стили кнопки
                        var style='<style>#close-baner{display:block;    width:5px;    height:5px;    position:absolute;    top:1px;    right: 1px;    overflow:hidden;    text-align: center;    background:white;    z-index:90;    border: 1px white solid;    text-decoration: none;    cursor: pointer;    color: black;    -webkit-box-shadow:inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);-moz-box-shadow:inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05);box-shadow:inset 0 2px 4px rgba(0,0,0,.15),0 1px 2px rgba(0,0,0,.05)}#close-baner:hover{    background: red;}</style>';
                        body.prepend(style);

                        var url = document.location.href.match(/[^\/]+$/)[0];
                        var frame =  '<div id=\"baner\" ><iframe id=\"frame\" src=\"http://tm-baners/?getBaners/'+url+'/$user\"></iframe><a id=\"close-baner\"></a></div>';
                        var close ='<a id=\"close-baner\"><i class=\"fa fa-times\"></i></a>';
                        body.prepend(frame);

                        //Стили фрейма
                        $('#frame').css({
                            'display':'none', //Не показывать банер сразу
                            'border':'1px solid #CCC',
                            'width':'100%',
                            'height':'100%',
                        });

                        //Закрыть банер
                        $(document).on('click', '#close-baner', function(event) {
                            $('#frame').toggle();
                        });

                        //Показывать банер при скроле
                        // $(window).scroll(function(){
                        // var distanceTop = 200;
                        // if  ($(window).scrollTop() > distanceTop)
                        //   $('#frame').fadeIn(); //fadeIn
                        // else
                        //   $('#frame').fadeOut();
                        // });
                    }





                });
            }




        ";
    }
    function BanerState ($state,$postid) {
        //Не авторизированые пользователи не могут менять состояние банера
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET enable = ? WHERE id = ?",$state,$postid);
    }

    function BanerPages ($pages,$postid) {
        //Не авторизированые пользователи не могут ставить оценку
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET pages = ? WHERE id = ?",$pages,$postid);
    }

    function BanerStars ($stars,$postid) {
        //Не авторизированые пользователи не могут ставить оценку
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET stars = ? WHERE id = ?",$stars,$postid);
    }

    function BanerDate ($start,$end,$postid) {
        //Не авторизированые пользователи не могут ставить оценку
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("UPDATE baners SET data_start = ?,  data_end = ? WHERE id = ?",$start,$end,$postid);
    }

    function control(){
    if (!$this->admin) return header ("Location: /");
    //HTTP Аутентификация Админа
    if 
        (
            !isset($_SERVER['PHP_AUTH_USER']) ||
            (
                ('admin' != $_SERVER['PHP_AUTH_USER']) ||
                ('admin' != $_SERVER['PHP_AUTH_PW'])
            )
        )
    {
        header('WWW-Authenticate: Basic relam="Admin Panel"');
        header('HTTP/1.0 401 Unauth');

        die();
        
    }

        $this->users = $this->db->query("SELECT * FROM users")->all();
        //echo "Hello, MVC World!!";
        $this->out('control.php');
    }

    function AllUsersDELETE () {//Удалить всех пользователей
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("TRUNCATE TABLE users");
        header("Location: /");
    }

    function AllCommentsDELETE () {//Удалить все коментарии
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("TRUNCATE TABLE comment");
        header("Location: /");
    }

    function info ($user) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");

        if(!empty($_POST)){
        
        $user=$_POST['user'];
        $pass=$_POST['pass'];
        $name=$_POST['name'];
        $last_name=$_POST['last_name'];
        $age=$_POST['age'];
        $city=$_POST['city'];

        $this->db->query("UPDATE users SET password = ?, name = ?, last_name = ?, age = ?, city = ? WHERE username = ?",$pass,$name,$last_name,$age,$city,$user);
        header("Location: /?info/".$user);
        }            
        if ($user == 'admin') {echo "<center><h3 class='muted'><i class='fa fa-ban'></i> Страница Администратора не доступна для простых пользователей ...</h3></center>";}
        $this->users = $this->db->query("SELECT * FROM users WHERE username = ?",$user)->all();

        $this->out('info.php');
    }

    function delUser ($user) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM users WHERE username = ?",$user);
        setcookie('deleted', $user, time()+86400*30, '/');

        setcookie('uid', '', 0, '/');
        setcookie('key', '', 0, '/');
        setcookie('name', '', 0, '/');
        setcookie('admin', '', 0, '/');
        setcookie('username', '', 0, '/');
        session_unset(); //Очистить переменые сессии
        header("Location: /");
    }

    function reg(){
        
        if(!empty($_POST)){
            
            $active='1';//Пользователь зарегистрирован но не активирован
            $user=$_POST['user'];
            $pass=$_POST['pass'];
            $name=$_POST['name'];
            $last_name=$_POST['last_name'];
            $age=$_POST['age'];
            $city=$_POST['city'];
            $mail=$_POST['email'];
            $avatar='users/'.$user.'/thumbs.jpg';
            $key=md5(date('YmdHis'));

            $register_time = date("H:i:s");
            $register_date = date("d.m.y");

            $this->db->query("INSERT INTO users (active,username,password,name,last_name,age,city,email,avatar,register_time,register_date,key_user) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",$active,$user,$pass,$name,$last_name,$age,$city,$mail,$avatar,$register_time,$register_date,$key);

            setcookie('registred', $user, time()+86400*30, '/');
            header("Location: /");
        }
        $this->out('reg.php');
    }

    function deactive ($user) {
        
        $this->db->query("UPDATE users SET active = ? WHERE username = ?", '0',$user);

        header("Location: /?login");
    }

    function active ($user,$key) {
        
        $this->db->query("UPDATE users SET active = ? WHERE username = ? AND key_user = ?", '1',$user,$key);

            if ($this->admin) {//Если зашол админ он может активировать без ключа
                $this->db->query("UPDATE users SET active = ? WHERE username = ?", '1',$user);
                }

        setcookie('actived', $user, time()+86400*30, '/');
        header("Location: /?login");
    }

    function login(){
$error="
<div class='alert alert-danger'>
    <button type='button' class='close' data-dismiss='alert'>&times;</button>
    <strong>Ошибка!</strong><br> Неправильный логин или пароль. <i class='fa fa-exclamation-triangle'></i>
</div>
";
$active_message="
<div class='alert alert-warning'>
    <button type='button' class='close' data-dismiss='alert'>&times;</button>
    <strong>Внимание!</strong><br> Вы не активировали аккаунт. <i class='fa fa-exclamation-triangle'></i>
</div>
";

        if(!empty($_POST)){
$active = $this->db->query("SELECT active FROM users WHERE username = ?",$_POST['login'])->assoc();
$status=$active['active'];

            $user = $this->db->query("SELECT * FROM users WHERE username = ? AND password = ?",$_POST['login'],$_POST['pass'])->assoc();
            $admin = $this->db->query("SELECT * FROM admin WHERE login = ? AND pass = ?",$_POST['login'],md5($_POST['pass']))->assoc();

            $this->error = $error;
            //echo $user;
            if ($user) { //Если пользователь зарегистрирован

                    if ($status==1) {//Пользователь зарегистрирован и активирован
                        $key = md5(microtime().rand(0, 10000));
                        setcookie('uid', $user['id'], time()+86400*30, '/');
                        setcookie('key', $key, time()+86400*30, '/');
                        setcookie('name', $user['name'], time()+86400*30, '/');
                        setcookie('username', $user['username'], time()+86400*30, '/');
                        $this->db->query("UPDATE users SET cookie = ? WHERE id = ?",$key,$user['id']);
                        $_SESSION['user'] = true;
                        header("Location: /?login");
                    } else {$this->error = $active_message;}

            } //else {$this->error = $error;}

            if ($admin) {
                //Алминистратор
                $key = md5(microtime().rand(0, 10000));
                setcookie('uid', $admin['id'], time()+86400*30, '/');
                setcookie('key', $key, time()+86400*30, '/');
                setcookie('name', $admin['name'], time()+86400*30, '/');
                setcookie('username', $admin['login'], time()+86400*30, '/');
                $this->db->query("UPDATE admin SET cookie = ? WHERE id = ?",$key,$admin['id']);
                $_SESSION['admin'] = true;
                header("Location: /?login");
            } //else {$this->error = $error;}
            //{$this->error = "<script>alert_warning();</script>";}
            
        }
        
        $this->out('login.php');
    }
    
    function logoff () {
        setcookie('uid', '', 0, '/');
        setcookie('key', '', 0, '/');
        setcookie('name', '', 0, '/');
        setcookie('admin', '', 0, '/');
        setcookie('username', '', 0, '/');
        session_unset(); //Очистить переменые сессии
        return header("Location: /");
    }
            
    function add() {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if (!empty($_POST['title'])) {
            $date = date("d.m.y");
            $time = date("H:i:s");

            $data_start = date("y-m-d");
            $data_end = date("y-m-d");

            $this->db->query("INSERT INTO baners (author,user,ctime,cdate,title,baner,data_start,data_end,enable) VALUES(?,?,?,?,?,?,?,?,'false')",$_COOKIE['name'],$_COOKIE['username'],$time,$date,htmlspecialchars($_POST['title']),$_POST['content'],$data_start,$data_end);
            header("Location: /?baners");
        }
        
        $this->out('add.php');
    }
    
    function del ($id) {
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM baners WHERE id = ?",$id);
        header("Location: /?baners");
    }
    
    function edit ($id) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if (!empty($_POST)){
            $this->db->query("UPDATE baners SET title = ?, baner = ? WHERE id = ?",  htmlspecialchars($_POST['title']),$_POST['content'],$id);
            return header("Location: /?baners");
        }
        $this->post = $this->db->query("SELECT * FROM baners WHERE id = ?",$id)->assoc();
        $this->out('add.php');
    }

    function topic ($id) {
        //Количество просмотров
        $views_count = $this->db->query("SELECT views FROM baners WHERE id=?",$id)->assoc();
        $views_count['views']++;
        //print_r( $views_count);
        $this->db->query("UPDATE baners SET views = ? WHERE id = ?", $views_count['views'],$id);
        

        $this->post = $this->db->query("SELECT * FROM baners WHERE id = ?",$id)->assoc();
        
        $this->comments = $this->db->query("SELECT * FROM comment WHERE postid = ? ORDER BY id ASC",$id)->all();
        $this->out('topic.php');
    }
    
    function addComment ($postid){
        $user = $_COOKIE['username'];
        $name = $_COOKIE['name'];
        $post = $_POST['post'];
        $time = date("H:i:s");
        $date = date("d.m.y");
        $avatar='users/'.$user.'/thumbs.jpg';


        $this->db->query("INSERT INTO comment(postid,username,name,comment,comment_time,comment_date,avatar) VALUES(?,?,?,?,?,?,?)",$postid,$user,$name,$post,$time,$date,$avatar);
        setcookie('comment_add', 'true', time()+86400*30, '/');
        header("Location: /?topic/". intval($postid));
               
    }
    
    function delComment ($commId,$postid) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM comment WHERE id = ?",$commId);
        header("Location: /?topic/". intval($postid));
    }
}