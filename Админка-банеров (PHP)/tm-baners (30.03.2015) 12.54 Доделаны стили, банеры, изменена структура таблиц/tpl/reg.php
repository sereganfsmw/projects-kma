<div class="container">

	<center><h3 class="muted"><i class="fa fa-user"></i> Форма Регистрации:</h3></center>


	<script src="js/ajax_image_upload.js"></script>
	<link href="css/file_drop.css" rel="stylesheet" type="text/css" />


	<div class="row">
		<div class="span4 well" style="width:300px;">
			<legend><i class="fa fa-info-circle"></i> Регистрация <button id="avatar_show" class="btn btn-block btn-success" type="button">Аватар <i class="fa fa-chevron-right"></i></button></legend>
			<? echo $this->error; ?>
			<form id="register_form" method="POST">

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-user"></i></span>
			<input class="span3" id="prependedInput" type="text" name="user" placeholder="Пользователь" maxlength="15" title="A-Z a-z">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-key"></i></span>
			<input class="span3" id="prependedInput" type="password" name="pass" placeholder="Пароль">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-male"></i></span>
			<input class="span3" id="prependedInput" type="text" name="name" placeholder="Имя" maxlength="15">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-male"></i></span>
			<input class="span3" id="prependedInput" type="text" name="last_name" placeholder="Фамилия" maxlength="15">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-calendar"></i></span>
			<input class="span1" id="prependedInput" type="text" name="age" placeholder="Возраст" maxlength="2" title="0-9">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-compass"></i></span>
			<input class="span3" id="prependedInput" type="text" name="city" placeholder="Город">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-at"></i></span>
			<input class="span3" id="prependedInput" type="text" name="email" value="test-mail@localhost" placeholder="Електроная почта">
			</div>
			<button type="submit" name="submit" class="btn btn-large btn-inverse btn-block">Зарегестрироваться <i class="fa fa-sign-in"></i></button>
			</form>    
		</div>

		<div id="avatar" class="span4 well">
		<legend><i class="fa fa-camera"></i> Аватар</legend>

			<div id="drop-files" ondragover="return false">

				<!-- Область для перетаскивания -->
				<p>Перетащите изображение сюда</p>
		        <form id="frm">
		        	<input type="file" id="uploadbtn"/>
		        </form>
			</div>

		</div>

		<div id="preview" class="span3 well" style="width:300px;">
		<legend><i class="fa fa-picture-o"></i> Предпросмотр</legend>

	    <!-- Область предпросмотра -->
		<div id="uploaded-holder"> 
			<div id="dropped-files">
	        	<!-- Кнопки загрузить и удалить, а также количество файлов -->
	        	<div id="upload-button">
	             		
							<button id="delete" class="btn btn-danger btn-block" type="button"><i class="fa fa-times"></i> Отмена</button>
						
	              
				</div>  
	        </div>
		</div>

		</div>

	</div>

</div>


<script type="text/javascript">
$(document).ready(function($) {
	$('li').removeClass('active');
	$('#reg').addClass('active');
	$('#avatar').hide();

	$('input[name=user]').attr('oncontextmenu','return false;'); //Запрет контекстного меню
	$('input[name=age]').attr('oncontextmenu','return false;'); //Запрет контекстного меню

});
</script>

<script>
//Reg.php
$(document).ready(function() {

$('#avatar_show').click(function(event) {

	//Пользователь
	if ($('input[name=user]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите пользователя", { type: 'danger' });
			    }, 100);
	}
	else
		{
		$('#avatar').slideDown();

		$('input[name=user]').addClass('uneditable-input');
		$('#avatar_show').attr('disabled','disabled'); //Выключить кнопку
		$('input[name=user]').attr('disabled','disabled'); //Выключить елемент

		//$('input[name=user]').removeAttr('disabled'); //Включить елемент
		};

});

	//Ограничения на ввод символов в поле A-Z (64-91) a-z (96-123) Backspace (8) Shift (16) TAB (9)
	$('input[name=user]').keypress(function(event) {
		if (!((event.charCode>64 && event.charCode<91) || (event.charCode>96 && event.charCode<123) || event.which==8 || event.keyCode==9 || event.which==16 )) return false;
		if (event.ctrlKey) return false //запрет CTRL+V, CTRL+X, CTRL+C
	});

	//Ограничения на ввод символов в поле 0-9 (47-58) Backspace (8) TAB (9)
	$('input[name=age]').keypress(function(event) {
		if (!((event.charCode>47 && event.charCode<58) || event.which==8 || event.keyCode==9 )) return false;
		if (event.ctrlKey) return false //запрет CTRL+V, CTRL+X, CTRL+C
	});


	        



	// Проверка полей в reg.php -->
	$('#register_form').submit(function () {
	var flag = true;
	//uploadFiles(); //!!!!!!! Рабочая

		//Пользователь
		if ($('input[name=user]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите пользователя", { type: 'success' });
					}, 100);
		flag=false;
		};

		//Пароль
		if ($('input[name=pass]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите пароль", { type: 'info' });
					}, 500);
		flag=false;
		};

		//Имя
		if ($('input[name=name]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите имя", { type: 'warning' });
					}, 1000);
		flag=false;
		};

		//Фамилия
		if ($('input[name=last_name]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите фамилию", { type: 'danger' }); 
					}, 1500);
		flag=false;
		};

		//Возраст
		if ($('input[name=age]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите возраст", { type: 'success' });
					}, 2000);
		flag=false;
		};

		//Город
		if ($('input[name=city]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите город", { type: 'info' });
					}, 2500);
		flag=false;
		};

		//Електроная почта
		if ($('input[name=email]').val() == '')
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Введите електроную почту", { type: 'warning' });
					}, 3000);
		flag=false;
		};


		//Аватар
		if(dataArray.length == 0)
		{
					setTimeout(function() {
						$.bootstrapGrowl("Ошибка: ","Выберите аватар.", { type: 'danger' }); 
					}, 3500);
		  flag=false;
		};


	//uploadfiles();
	if (flag == false)
	{
	  return false;
	}
	else{$('input[name=user]').removeAttr('disabled');};
		
	});


});
</script>

