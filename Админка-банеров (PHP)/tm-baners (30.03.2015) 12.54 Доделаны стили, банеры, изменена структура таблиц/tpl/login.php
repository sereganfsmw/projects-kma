<div class="container">

	<center><h3 class="muted"><i class="fa fa-sign-in"></i> Форма Входа:</h3></center>

	<script type="text/javascript">
	$(document).ready(function($) {
		$('li').removeClass('active');
		$('#log-in').addClass('active');
	});
	</script>

	<div class="row">
		<div class="span4 offset4 well" style="width:300px;">
			<legend>Log - in</legend>
			<? echo $this->error; ?>
			<form id="auth" method="POST">
				<? if (($this->user) or ($this->admin)) { ?>
				<img class="login-img" src="users/<?= $_COOKIE['username'];?>/thumbs.jpg" alt="User Pic">
				
				<a href="/?baners" class="btn btn-large btn-block btn-success" type="button">Здрастуйте: <b><?= $_COOKIE['name'];?></b></a>
				<? }else{ ?>
				<img class="login-img" src="img/no-avatar.png" alt="User Pic">
			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-user"></i></span>
			<input class="span3" id="prependedInput" type="text" name="login" placeholder="Username">
			</div>

			<div class="input-prepend">
			<span class="add-on"><i class="fa fa-key"></i></span>
			<input class="span3" id="prependedInput" type="password" name="pass" placeholder="Password">
			</div>

			<button type="submit" name="submit" class="btn btn-large btn-info btn-block">Войти <i class="fa fa-sign-in"></i></button>
			<? } ?>
			</form>    
		</div>
	</div>

</div>

<script>
//Login.php
// Проверка логина и пароля в login.php -->
$(document).ready(function() {

$('#auth').submit(function () {
var flag = true;
var message = '';
    
		if ($('input[name=login]').val() == '')
		  {
			message+="Введите имя\n"; 
			flag=false;
		  };
		if ($('input[name=pass]').val() == '')
		  {
			message+="Введите пароль\n";  
			flag=false;
		  };

	if (flag == false)
	{
			$('.warning-box').slideDown();

	  return false;
	};
	});

});
</script>