<div class="container">

	<center><h3 class="muted"><i class="fa fa-home"></i> Главная:</h3></center>


	<div class="hero-unit">
		<div class="container">
			<h1>Приветствуем! <i class="fa fa-flag"></i></h1>
			<p>Создавайте свои персональные банеры и делитесь с друзьями, всего за несколько простых шагов.</p>
			<p>
				<a a href="/?reg" class="btn btn-primary btn-large">1. Зарегестрируйтесь <i class="fa fa-plus-circle"></i></a>
				<a a href="/?add" class="btn btn-success btn-large">2. Создавайте банеры <i class="fa fa-bars"></i></a>
				<a a href="/?baners" class="btn btn-danger btn-large">3. Делитесь своими идеями <i class="fa fa-pencil-square-o"></i></a>
			</p>
		</div>
	</div>
	
</div>