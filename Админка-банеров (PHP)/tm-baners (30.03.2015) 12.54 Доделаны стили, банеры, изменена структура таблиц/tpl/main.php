<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Baners System</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="MoST">

  <!-- Bootstrap v2.3.2 CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="favicon.png">

    <style>
      body {
        padding-top: 60px;
        background-color: #f5f5f5;

        /*height: 1000px; Для появления банера при скроле*/
      }
    </style>

    <!-- Шрифт для банеров -->
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,700" rel="stylesheet" type="text/css">

    <!-- Bootstrap v2.3.2 Responsive CSS -->
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Font Awesome 4.2.0 CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
	
    <!-- Add-on CSS -->
    <link href="css/callout.css" rel="stylesheet">
    <link href="css/kbd.css" rel="stylesheet">
    <link href="css/panels.css" rel="stylesheet">

    <!-- TemplateMonster CSS -->
    <link href="css/template-monster.css" rel="stylesheet">

    <!-- Rating Stars -->
    <link rel="stylesheet" type="text/css" href="css/stars-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="css/star-rating.css">

    <!-- Bootstrap Switch CSS -->
    <link href="css/bootstrap-switch.css" rel="stylesheet">

    <!-- Bootstrap Datepicker CSS -->
    <link href="css/bootstrap-datepicker.css" rel="stylesheet">

</head>

       <!-- Navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
     
      <div class="navbar-inner">
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="brand" data-toggle="modal" href='#modal-id'><i class="fa fa-history"></i> История</a>
          <a data-original-title="Версия сборки:" href="#" data-toggle="popover" data-placement="bottom" data-html="true" data-content="29.03.2015" title=""class="brand" href="#"><i class="fa fa-exclamation-triangle"></i> Baners Control System</a>
          <div class="nav-collapse collapse">
        
          <? if (($this->user) or ($this->admin)) { ?>
          <form class="navbar-form pull-right">

          <ul class="nav">

              <li class="dropdown">
                <a href="#" class="dropdown-toggle user" data-toggle="dropdown"><i class='fa fa-user'></i> Вы вошли как: <b><?= $_COOKIE['name'];?></b><b class="caret"></b></a>
                <ul class="dropdown-menu">

                  <li id="info-user"><a href="/?info/<?=$_COOKIE['username'];?>"><i class="fa fa-info-circle"></i> Информация</a></li>

                  <? if ($this->admin) { /*Если зашол администратор*/?>
                  <li id="control-panel"><a href="/?control"><i class="fa fa-sliders"></i> Админ</a></li>
                  <? } ?>
                  <li class="divider"></li>

                  <li><a href="/?logoff"><i class="fa fa-power-off"></i> Выход</a></li>
                </ul>
              </li>

          </ul>

          </form>
          <?}?>



        <ul class="nav"> 
<!-- ________________________________________________________М Е Н Ю________________________________________________________ -->

  			  <li id="blogs" class="active"><a href="/"><i class="fa fa-home"></i> Главная</a></li>

  				<? if ((!$this->user) and (!$this->admin)){ /*Если пользователь не авторизован*/?>
  				<li id="log-in"><a href="/?login"><i class="fa fa-sign-in"></i> Войти</a></li>
          <li id="reg"><a href="/?reg"><i class="fa fa-user"></i> Регистрация</a></li>
  				<? } ?>

  				<? if (($this->user) or ($this->admin)) {/*Если зашол пользователь или админ*/?>
          <li id="baners"><a href="/?baners"><i class="fa fa-bars"></i> Мои банеры</a></li>
  				<li id="add-post"><a href="/?add"><i class="fa fa-file-text"></i> Новый Банер</a></li>
  				
  				<? } ?>
        </ul>


          </div>
        </div>
      </div>
    </div>	
 

<body>

<div class="modal fade" id="modal-id" style="width:100%;height:100%;margin-left:0px;left:0px;display:none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">История изменений: <button type="button" class="btn btn-danger"><i class="fa fa-picture-o"></i> Редактора Банеров</button> для <img src="ico/favicon.png"> Template Monster</h4>
      </div>
      <div class="modal-body">
        <b>02.02.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - Создан репозиторий на BitBucket, подготовка файлов для проекта (Bootstrap v2.3 | jQuery 1.9 | FontAwesome v4.2), создание структуры БД<br><b>03.02.2015</b> - <b>05.02.2015</b> - написание роутера страниц, проверка вводимого URL и редирект на главную страницу index.php и подключение к БД (template_monster),создание шаблонов страниц<br><b>06.02.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - проверка простых запросов выборки (добавление 2-х записей пользователя)<br><b>07.02.2015</b> - написание скриптов для авторизации пользователя и админа (Создание формы входа и регистрации(простая форма 2-х поля))<br><b>08.02.2015</b> - создание меню, разметок страниц (Главной,списка банеров,админки,создание и редактирование банера на одной)<br><b>09.02.2015</b> - создание функций для редактирования, удаления, добавления, отображения даных из БД, внесены даные для 1 банера<br><b>10.02.2015</b> - доработана проверка на админа, добавлены ($this->admin) для меню, установка и проверка куков, вывод информации о пользователе<br><b>11.02.2015</b> - вывод некоторых полей контента банера, добавлен WYSIWYG редактор (TinyMCE для Админа) и (Imperavi для Пользователя)<br><b>12.02.2015</b> - доработаные некоторые стили, добавлены иконки (FontAwesome), доработана форма регистрации (больше полей) информативнее<br><b>13.02.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - добавлены Alert-Box, Slide-Alerts, доработаны скрипты проверки полей, добавлена AJAX загрузка изображений при регистрации,скрипт обрезания изображения,создание и загрузка в папку пользователя<br><b>14.02.2015</b> - WYSIWYG редактор прикручен для редактирования и создания контента банеров, вывод частичного контента на страницу,добавлено редактирование профиля пользователя<br><b>15.02.2015</b> - добавлена возможность добавления коментариев к банерам,доделана админка,коректный вывод всего содержимого банера, создание разметки "Акардиона" для вывода, добавлена стрелка "Вверх" при прокрутке<br><b>16.02.2015</b> - вывод банера в "Акардеоне", исправлены многие CSS правила, все JS скрипты подключаються в первичном файле (главном), подчищен код удалены коментарии, доработаны PHP скрипты для обработки даных, дописана админка<br><b>17.02.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - добавлен "корвый" слайдер, и некоректно выводит в него содержимое ОДНОГО банера<br><b>18.02.2015 - 28.02.2015</b> - нечего не делал были проблемы со здоровьем и с учебой<br><b>01.03.2015</b> - исправил слайдер коректно выводит ВСЕ банеры и генерирует ссылки внизу<br><b>02.03.2015</b> - добавил корявые (нерабочие) звезды рейтинга "Bootstrap Rating Star, kartik", добавлены поля для рейтинга каждого банера<br><b>06.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - рабочий рейтинг, изменение значения через AJAX<input class="rating" type="number" value="3.5" min=0 max=5 step=0.1 data-size="xs" data-stars="5"/><br><b>08.03.2015 - 15.03.2015</b> - сессия (небыло времени)<br><b>15.03.2015</b> - добавлен подсчет коментариев и количество просмотра банера)<br><b>17.03.2015</b> - исправлены/доработаны JS скрипты для проверки полей, исправлены стили отображения банера, добавлена картинка логина в окне входа<br><b>19.03.2015</b> - доработано меню некоторые елементы перенесены в кнопку с всплывающим списком, добавлены иконки для многих полей, доработан "Акордион" переделано через плавающие колонки "Bootstrap row-fluid"<br><b>20.03.2015</b> - с главной страницы убрано ненужное, добавлено 2-х колонки для отображения списка юанкров, СЛЕВА (Аватар пользователя), СПРАВА "Акордион" с банерами, размерами 2 и 10 соответствено<br><b>21.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - до аватара пользователя СЛЕВА добавлено информацию про него, без возможности редактирования непостредственно<br><b>22.03.2015</b> - добавлена стилизированая "страница 404" при неправельном "URL"<br><hr><b>23.03.2015</b> - [ВАЖНО!!!] Уточнил задание, каким должен быть финальный результат, банерная система должна генерировать банера определеного пользователя по ссылке (ссылка генерируеться автоматически), ссылка подгружает JS скрипт который генерирует PHP на стороне сервера, для создания iframe "окна к серверу", в которое подгружаеться банер, банер выводиться в зависимости от параметров (включен/выключен,страницы на которых его показываться,и диапазон даты для отображения банера)<br><b>24.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - написал скрипт для внедрения iframe на сайт где будет размещен банер, разбил проект на 2 части на ПЕРВОМ сервере (5 страниц Главная,товары,поиск,категории,контакты), на ВТОРОМ сервере (7 страницы Главная,список банеров,новый банер,информация о пользователе,админка,регистрация и форма входа), добавил стили для позиционирования iframe (банера) в блоке, "корявый" вывод контента в iframe, добавил HTTP Auth для админки<br><b>25.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - исправил вывод информации банера в iframe, добавил новые CSS правила которіе генерирується на стороне клиента через JS скрипт, внедрил в JS скрипт подключение библиотеки jQuery (если она не присутствует на конечном сайте), а также библиотеку для работы с кукис "COOKIES", через куки добавил проверку количества просмотров страниц на сайте для отображения банера, дописал CSS стили банер теперь коректно выводиться вне зависимости от размеров блока<br><b>26.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - на сайте админки банеров зделал автоматическую генерацию ссылки на банеры, переделал структуру таблиц в БД добавив новые поля для параметров банера (enable = включить/выключить,pages = страницы на которых показывать,date_start/date_end = диапазон даты для отображения банера),добавил возможность включения/выключения банеров через "Bootstrap Switch", добавил поле для ввода страниц отображения банера (все страницы пишуться через ',' запятую, цыфры водить нельзя), добавил в скрипт генерации банера (getBaner) проверки<input type="checkbox" id="swith_example" checked data-label-text="<span class='fa fa-power-off'></span> Включить банер" data-label-width="150" data-handle-width="50" data-size="small" data-on-color="success" data-off-color="primary"><br><b>27.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - в списке банеров добавил поле для посика банеров, банера отображаються свои для каждого пользователя, но админ может видеть банера всех пользователей, каждый пользователь может редактировать только свой банер, поиск банера производиться по его названию<br><b>28.03.2015</b> - добавил "Bootstrap Datepicker" календарь для выбора диапазона даты для отображения банера, в календаре присутствуют проверки на коректность введеного формата даты (YYYY-MM-DD), такой формат был выбран изза того что в БД MYSQL имено такой формат, дописал скрипты для проверки диапазона даты который сравниваеться с текущей датой, запрос вида (WHERE $curent BETWEEN $start and $stop)<input type="text" type="text" id="kalendar"><br><b>29.03.2015</b> <span class="muted">(Commit Bitbucket <i class="fa fa-bitbucket"></i>)</span> - дописал проверки в случае если нету банеров для показа то отображать картинку (TemplateMonster "Банеров не найдено"), подчистил скрипты от ненужных коментариев и переменых для "дебага", внедрение банера происходит всего через 1 ссылку, в результате получилось 2 скрипта (1) генерирует стили для отображения банера, (2) делает фильтрацию банеров по заданым параметрам и выводит пользователю список банеров в виде стилизированого слайдера, с возможностью перехода на страницу самого банера
      </div>
      <div class="modal-footer">
        <span class="pull-left"><kbd class="most-text">Serega</kbd> <code class="most-text">MoST</code></span>
        
        <button type="button" class="btn btn-success pull-right" data-dismiss="modal"><i class="fa fa-times"></i> Закрыть</button><center><h3 class="muted"><i class="fa fa-check-square-o"></i> Админка для банера готова :)</h3></center>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- ________________________________________________________А Л Е Р Т Ы________________________________________________________ -->
<!-- Slide Alerts -->
<div style="display:none" class="success-box alert-box">
<div class="msg">Success &#8211; Your message will goes here</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="error-box alert-box">
<div class="msg"><b>Внимание!</b> &#8211; Таблица пользователей очищена...</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="info-box alert-box">
<div class="msg">Info &#8211; Information message will goes here</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="warning-box alert-box">
<div class="msg"><b>Внимание!</b> &#8211; Введите логин и пароль.</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="download-box alert-box">
<div class="msg">Внимание &#8211; Все коментарии удалены.</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>
<!-- ______________________________________________________________________________________________________________________________ -->

<div class="container">

<!-- Bootstrap JavaScript -->
<script src="js/jquery.js"></script>
<script src="js/application.js"></script>

<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-dropdown.js"></script>

<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>

<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>

<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-carousel.js"></script>
<script src="js/bootstrap-typeahead.js"></script>

<!-- Color Picker JavaScript -->
<script src="js/tinycolor.js"></script>
<script src="js/colorpicker.js"></script>

<!-- Cookies JavaScript -->
<script src="js/jquery.cookie.js"></script>

<!-- Всплывающие Alerts -->
<script src="js/alert-growl.js"></script>

<!-- Стрелка вверх -->
<script src="js/jquery.scrollUp.js"></script>

<!-- Bootstrap Switch -->
<script src="js/bootstrap-switch.min.js"></script>

<!-- Bootstrap Datepicker -->
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/locales/bootstrap-datepicker.ru.min.js"></script>

<!-- Bootstrap Rating Stars -->
<script src="js/star-rating.js"></script>
<? //echo $_COOKIE['name'];//print_r ($this->user);//$_SESSION['user'] //print_r ($this->user);//print_r($_COOKIE); ?>


<script>
$("#swith_example").bootstrapSwitch();

$('#kalendar').datepicker({
    format: "yyyy-mm-dd",
    todayBtn: "linked",
    clearBtn: true,
    language: "ru",
    orientation: "bottom auto",
    todayHighlight: true
});

  //Стрелка Вверх
  $(function () {
      $.scrollUp({
      animation: 'fade',
      //activeOverlay: '#00FFFF',
      scrollImg: { active: true, type: 'background', src: 'img/top.png' }
      });
  });

  //Скрываем слайд-алерты
  $(function() {

  $(".alert-box .toggle-alert").click(function(){
    $(this).closest(".alert-box").slideUp();
    return false;
  });
  });

$(document).ready(function() {
    $('#most').click(function() {
     $('.most-div').toggle();
  });
});

</script>

<!-- ________________________________________________________Ш А Б Л О Н Ы________________________________________________________ -->

</div>  <!-- /container -->
<? $this->out($this->tpl,true); ?>

<div class="most-div"><kbd class="most-text">Serega</kbd> <code class="most-text">MoST</code></div>

<!-- Footer -->
<div class="navbar navbar-inverse navbar-fixed-bottom" style="height: 20px;">
    <div class="navbar-inner">
        <div class="pull-left muted">
            Powered by <b>Bootstrap</b> & <b>Font Awesome</b> <i class="fa fa-flag"></i>
        </div>
        <div id="most" class="pull-right text-info">
            Design by <b><kbd>Serega</kbd> <code>MoST</code></b>. Copyright © 2015 All Rights Reserved <i class="fa fa-exclamation-triangle"></i>
        </div>
        
    </div>
</div>

    </body>
</html>


