<div class="container">

	<script type="text/javascript">
	$(document).ready(function($) {
		$('li').removeClass('active');
		$('#control-panel').addClass('active');
	});
	</script>

	<div class="row">
		<div class="span7 well">
			<legend><i class="fa fa-users"></i> Пользователи</legend>
			<form id="users" method="POST">


	<select class="span7" multiple="multiple" name="select" size="10">
	<? foreach ($this->users as $key => $value) {?>
	<? if ($value['active']==1) {?><option value="<?= $value['username'];?>">Пользователь: <?= $value['username'];?>  | Пароль: <?= $value['password'];?> | Имя: <?= $value['name'];?> | Статус: [Активирован]</option><?}?>
	<? if ($value['active']==0) {?><option value="<?= $value['username'];?>">Пользователь: <?= $value['username'];?>  | Пароль: <?= $value['password'];?> | Имя: <?= $value['name'];?> | Статус: [Неактивирован]</option><?}?>
	<? } ?>
	</select>

			<a id="delete" type="submit" name="submit" class="btn btn-danger btn-block" disabled="disabled" onclick="return confirm('Точно удалить пользователя?');"><i class="fa fa-trash"></i> Удалить пользователя</a>
			<a id="activate" type="submit" name="submit" class="btn btn-success btn-block" disabled="disabled"><i class="fa fa-check"></i> Активировать</a>
			<a id="edit" type="submit" name="submit" class="btn btn-warning btn-block" disabled="disabled"><i class="fa fa-pencil-square-o"></i> Редактировать</a>
			<a id="deactivate" type="submit" name="submit" class="btn btn-inverse btn-block" disabled="disabled"><i class="fa fa-ban"></i> Деактивировать</a>
			</form>    
		</div>
		<div class="span4 well">
			<legend><i class="fa fa-cogs"></i> Система</legend>
			<form id="system" method="POST">

			<a href="/?AllUsersDELETE" class="btn btn-danger btn-block" onclick="return confirm('Это удалит всех пользователей !!!, уверены ?');"><i class="fa fa-users"></i> Удалить всех пользователей</a>
			<a href="/?AllCommentsDELETE" class="btn btn-inverse btn-block" onclick="return confirm('Это удалит все коментарии !!!, уверены ?');"><i class="fa fa-comments"></i> Удалить все коментарии</a>
			</form>    
		</div>

	</div>

</div>
<script>
//Control.php
$(document).ready(function() {

	$('select').click(function() {
		user=$(this).val(); //Выделеный пользователь
		$('#delete').removeAttr('disabled'); //Включить елемент
		$('#delete').attr("href","/?delUser/"+user);

		$('#activate').removeAttr('disabled'); //Включить елемент
		$('#activate').attr("href","/?active/"+user+"/yes");

		$('#edit').removeAttr('disabled'); //Включить елемент
		$('#edit').attr("href","/?info/"+user);

		$('#deactivate').removeAttr('disabled'); //Включить елемент
		$('#deactivate').attr("href","/?deactive/"+user);
		//alert(user);
	});
	
});

</script>