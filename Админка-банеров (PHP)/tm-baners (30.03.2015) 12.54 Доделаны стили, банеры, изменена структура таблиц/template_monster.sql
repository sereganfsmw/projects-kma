-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 30 2015 г., 12:55
-- Версия сервера: 5.5.35-log
-- Версия PHP: 5.4.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `template_monster`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `cookie` varchar(32) NOT NULL,
  `name` varchar(15) NOT NULL,
  `avatar` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `login`, `pass`, `cookie`, `name`, `avatar`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'a2168d9a80679af380cc3141f6e33ada', 'Администратор', 'users/admin/thumbs.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `baners`
--

CREATE TABLE IF NOT EXISTS `baners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(60) NOT NULL,
  `user` varchar(60) NOT NULL,
  `ctime` varchar(10) NOT NULL,
  `cdate` varchar(15) NOT NULL,
  `title` varchar(80) NOT NULL,
  `baner` mediumtext NOT NULL,
  `enable` varchar(10) NOT NULL,
  `pages` varchar(255) NOT NULL,
  `data_start` date NOT NULL,
  `data_end` date NOT NULL,
  `stars` float NOT NULL,
  `views` int(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Дамп данных таблицы `baners`
--

INSERT INTO `baners` (`id`, `author`, `user`, `ctime`, `cdate`, `title`, `baner`, `enable`, `pages`, `data_start`, `data_end`, `stars`, `views`) VALUES
(23, 'Администратор', 'admin', '21:19:05', '29.03.15', 'Банер №1', 'Вывод текста с форматированием <b>жирный</b>, <i>италик </i>, <u>подчеркнутый</u>, <strike>зачеркнутый </strike>, <b><span style="color: orange;">оранжевый</span>, <span style="color: red;">красный</span>, <span style="color: lime;">зеленый</span>, <span style="color: blue;">синий </span>, <span style="color: magenta;">магента</span>.</b><br>\r\n', 'false', '', '2015-03-19', '2015-03-27', 0, 4),
(24, 'Администратор', 'admin', '18:20:07', '29.03.15', 'Банер №2', 'Список:<br><ul><li>Первый</li><li>Второй</li><li>Третий</li><li>Четвертый</li><li>Пятый</li></ul><p>Нумерованый список:</p><p><ol><li>Первый</li><li>Второй</li><li>Третий</li><li>Четвертый</li><li>Пятый</li></ol></p>', '0', '', '2015-03-29', '2015-03-29', 0, 0),
(25, 'Администратор', 'admin', '11:17:23', '29.03.15', 'Банер №3', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<h2 style="text-align: center;"><img style="float: left;" src="WYSIWYG/Tinymce/upload_dir/Avatar.jpg" alt="" width="263" height="321" />Air Conditing</h2>\r\n<h3 style="text-align: center;">Responsive Magento Theme</h3>\r\n<p>P TEXT Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>\r\n<h1>Button H1</h1>\r\n</body>\r\n</html>', 'true', 'test.html,index.html', '2015-03-29', '2015-03-29', 3, 32),
(26, 'Администратор', 'admin', '10:15:46', '29.03.15', 'Банер №4', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Добавляем таблицу , размером 3 х 3</p>\r\n<table id="table14973" style="height: 48px;" border="1" width="79">\r\n<tbody>\r\n<tr>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">1</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">2</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">3</span></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">4</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">5</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">6</span></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">7</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">8</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">9</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Делаем табуляцию</p>\r\n<blockquote>1<br />\r\n<blockquote>2<br />\r\n<blockquote>3<br />\r\n<blockquote>4</blockquote>\r\n</blockquote>\r\n</blockquote>\r\n</blockquote>\r\n</body>\r\n</html>', 'true', 'test.html', '2015-03-29', '2015-03-29', 0, 1),
(38, 'Администратор', 'admin', '16:11:18', '29.03.15', 'Банер №5', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Добавляем спец символы , Х<sub>2</sub> Х<sup>2</sup>&nbsp; а также смайлики <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /> <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cry.gif" alt="cry" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-foot-in-mouth.gif" alt="foot-in-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-frown.gif" alt="frown" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-innocent.gif" alt="innocent" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-kiss.gif" alt="kiss" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-money-mouth.gif" alt="money-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-sealed.gif" alt="sealed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-smile.gif" alt="smile" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-surprised.gif" alt="surprised" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-tongue-out.gif" alt="tongue-out" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-undecided.gif" alt="undecided" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-wink.gif" alt="wink" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-yell.gif" alt="yell" /> и тд...</p>\r\n</body>\r\n</html>', 'false', 'test.html,index.html', '2015-03-12', '2015-03-31', 2, 7),
(39, 'Администратор', 'admin', '23:30:15', '29.03.15', 'Банер №6', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Форматирование текста, положение теста, размер шрифта:</p>\r\n<p style="text-align: left;">Первый</p>\r\n<p style="text-align: center;">Второй</p>\r\n<p style="text-align: right;">Третий</p>\r\n<p style="text-align: left;"><span style="background-color: #ff6600;">Разный Шрифт</span></p>\r\n<p style="text-align: left;"><span style="font-family: arial black,avant garde; font-size: 18pt;">Первый</span></p>\r\n<p style="text-align: left;"><span style="font-family: comic sans ms,sans-serif; font-size: 18pt;">Второй</span></p>\r\n<p style="text-align: left;"><span style="font-family: times new roman,times; font-size: 18pt;">Третий</span></p>\r\n</body>\r\n</html>', 'false', 'test.html,index.html', '2015-03-05', '2015-03-28', 4.4, 6),
(40, 'Администратор', 'admin', '20:20:37', '29.03.15', 'Банер №7', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>12Добавляем видео с Youtube <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-smile.gif" alt="smile" /></p>\r\n<p>Добавляем спец символы , Х<sub>2</sub> Х<sup>2</sup>&nbsp; а также смайлики <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /> <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cry.gif" alt="cry" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-foot-in-mouth.gif" alt="foot-in-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-frown.gif" alt="frown" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-innocent.gif" alt="innocent" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-kiss.gif" alt="kiss" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-money-mouth.gif" alt="money-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-sealed.gif" alt="sealed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-smile.gif" alt="smile" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-surprised.gif" alt="surprised" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-tongue-out.gif" alt="tongue-out" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-undecided.gif" alt="undecided" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-wink.gif" alt="wink" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-yell.gif" alt="yell" /> и тд...</p>\r\n<p>Добавляем таблицу , размером 3 х 3</p>\r\n<p><iframe src="//www.youtube.com/embed/3jbLIkITRow" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>\r\n</body>\r\n</html>', 'false', 'index.html,page.html', '2015-03-20', '2015-03-30', 2.4, 43),
(41, 'Serega', 'user', '14:03:58', '29.03.15', 'Банер №8', '<p></p><p><img style="width: 572.012px; height: 322px;" src="WYSIWYG/Imperavi/images/79db1f27ae6a151da192d9a4e029c1f8.jpg"></p>Rainbow CC 2014<br><p></p>\r\n', 'true', '*', '2015-03-29', '2015-03-29', 4, 17),
(42, 'Serega', 'user', '13:10:17', '29.03.15', 'Банер №9', '<p>Проверка банера =)<br></p>', 'false', 'index.html,contacts.html,info.html,tovar.html,search.html,categories.html,test.html', '2015-03-29', '2015-03-29', 0, 13),
(43, 'Администратор', 'admin', '12:07:26', '30.03.15', 'Банер №10', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Wallpaper MoST (White).jpg" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', '*', '2015-03-30', '2015-03-30', 5, 0),
(44, 'Администратор', 'admin', '12:10:32', '30.03.15', 'Банер №11', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Bridge 1.jpg" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', 'index.html', '2015-03-30', '2015-03-30', 0, 0),
(45, 'Администратор', 'admin', '12:10:53', '30.03.15', 'Банер №11', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Bridge 2.jpg" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', 'index.html', '2015-03-30', '2015-03-30', 3.4, 0),
(46, 'Администратор', 'admin', '12:11:12', '30.03.15', 'Банер №12', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Color Lines.jpg" alt="" width="1349" height="758" /></p>\r\n</body>\r\n</html>', 'true', '*', '2015-03-30', '2015-03-30', 4, 0),
(47, 'Администратор', 'admin', '12:11:26', '30.03.15', 'Банер №13', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Rainbow.jpg" alt="" width="1349" height="758" /></p>\r\n</body>\r\n</html>', 'true', 'index.html', '2015-03-30', '2015-03-30', 3.8, 0),
(48, 'Администратор', 'admin', '12:11:47', '30.03.15', 'Банер №14', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Windows 7 (After Effects) 2.jpg" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', 'index.html', '2015-03-30', '2015-03-30', 4.4, 0),
(49, 'Администратор', 'admin', '12:12:05', '30.03.15', 'Банер №15', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Windows 7 Logon (After Effects).jpg" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', 'index.html', '2015-03-30', '2015-03-30', 2.2, 0),
(50, 'Администратор', 'admin', '12:16:56', '30.03.15', 'Банер №16', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Diagram 3D 2.jpg" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', '*', '2015-03-30', '2015-03-30', 2.7, 0),
(51, 'Администратор', 'admin', '12:17:13', '30.03.15', 'Банер №17', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Diagram.jpg" alt="" width="1100" height="880" /></p>\r\n</body>\r\n</html>', 'true', '*', '2015-03-30', '2015-03-30', 2, 0),
(52, 'Администратор', 'admin', '12:17:37', '30.03.15', 'Банер №18', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Ubuntu.jpg" alt="" width="1280" height="1024" /></p>\r\n</body>\r\n</html>', 'true', '*', '2015-03-30', '2015-03-30', 4.3, 0),
(53, 'Администратор', 'admin', '12:17:53', '30.03.15', 'Банер №19', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><img src="WYSIWYG/Tinymce/upload_dir/Work Plata.JPG" alt="" width="1349" height="843" /></p>\r\n</body>\r\n</html>', 'true', '*', '2015-03-30', '2015-03-30', 4.5, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postid` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `comment` text NOT NULL,
  `comment_time` varchar(30) NOT NULL,
  `comment_date` varchar(30) NOT NULL,
  `avatar` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postid` (`postid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` varchar(10) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `age` int(11) NOT NULL,
  `city` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `cookie` varchar(32) NOT NULL,
  `key_user` varchar(33) NOT NULL,
  `avatar` varchar(40) NOT NULL,
  `register_time` varchar(20) NOT NULL,
  `register_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `active`, `username`, `password`, `name`, `last_name`, `age`, `city`, `email`, `cookie`, `key_user`, `avatar`, `register_time`, `register_date`) VALUES
(1, '1', 'user', 'user', 'Serega', 'MoST', 22, 'Николаев', 'personal.notes@yandex.ru', '02fae0f05bc1ee64a8cb8252a26d9905', '8480093613fd4d3727b16da88166343f', 'users/user/thumbs.jpg', '20:58:39', '09.11.14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
