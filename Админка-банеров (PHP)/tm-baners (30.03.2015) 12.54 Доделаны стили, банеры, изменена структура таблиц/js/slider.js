/**
Baner Slider
*/

(function ($) {
var hwSlideSpeed = 700;
var hwTimeOut = 20000; //Смена через 20 Секунд
var hwNeedLinks = true;

$(document).ready(function(e) {
  $('.slide').css(
    {"position" : "absolute",
     "top":'0', "left": '0'}).hide().eq(0).show();
  var slideNum = 0;
  var slideTime;
  slideCount = $("#slider .slide").size();
  var animSlide = function(arrow){
    clearTimeout(slideTime);
    $('.slide').eq(slideNum).fadeOut(hwSlideSpeed);
    if(arrow == "next"){
      if(slideNum == (slideCount-1)){slideNum=0;}
      else{slideNum++}
      }
    else if(arrow == "prew")
    {
      if(slideNum == 0){slideNum=slideCount-1;}
      else{slideNum-=1}
    }
    else{
      slideNum = arrow;
      }
    $('.slide').eq(slideNum).fadeIn(hwSlideSpeed, rotator);
    $(".control-slide.active").removeClass("active");
    $('.control-slide').eq(slideNum).addClass('active');
    }

  var $adderSpan = '';
  $('.slide').each(function(index) {
      $adderSpan += '<span class = "control-slide">' + index + '</span>';
    });
  $('<div class ="sli-links">' + $adderSpan +'</div>').appendTo('#slider-wrap');
  $(".control-slide:first").addClass("active");
  $('.control-slide').click(function(){
  var goToNum = parseFloat($(this).text());
  animSlide(goToNum);
  });

  if($('.control-slide').length>1){ //Если количество слайдов 1 то не показываь кнопки
  var $linkArrow = $('<a id="prewbutton" href="#">&lt;</a><a id="nextbutton" href="#">&gt;</a>')
    .prependTo('#slider');    
    $('#nextbutton').click(function(){
      animSlide("next");
      return false;
      })
    $('#prewbutton').click(function(){
      animSlide("prew");
      return false;
      })
  }

  var pause = false;
  var rotator = function(){
      if(!pause){slideTime = setTimeout(function(){animSlide('next')}, hwTimeOut);}
      }
  $('#slider-wrap').hover(  
    function(){clearTimeout(slideTime); pause = true;},
    function(){pause = false; rotator();
    });
  rotator();
});
})(jQuery);