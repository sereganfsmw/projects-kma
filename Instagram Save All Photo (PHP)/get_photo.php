<?php
header("Content-Type: text/html; charset=utf-8");

//Отключаем время ожидания выполнения скрипта
set_time_limit(0);

$dir = 'instagram';
if (!is_dir($dir )) {
    mkdir($dir);         
}

$img_arrays = json_decode($_POST['urls']);

save_image_from_url($img_arrays);

//Потоковое скачивание картинок
function save_image_from_url($img_arr){
	$maxThreads = 100; // Количество потоков
	$dir = 'instagram';
	$multicurlInit = curl_multi_init();
	$total_count = count($img_arr);
	$url_arr = $img_arr;
	do
	{
	    while(@$active <= $maxThreads)
	    {
	        @$active++;
	        if(count($img_arr) == 0){
				echo "All Photos Downloads [$total_count]";
				echo "<hr><br>";
				echo "<pre>";
				var_dump($url_arr);
				echo "</pre>";
	            break;
	        }

	        $id = array_rand($img_arr);
	        $link = $img_arr[$id]->url;
	        $num = $img_arr[$id]->num;
	        	  
			//Получить имя
			preg_match('|\/([^/]*)$|', $link, $file_name);

			$name = $file_name[1];
	        unset($img_arr[$id]);

	        //Формируем название и создаем дескриптор файла
	        $file_name = $dir .'/('.$num.') '. $name;
		    if(file_exists($file_name)){
		        unlink($file_name);
		    }
		    $fp = fopen($file_name,'x');

		    //Создаем поток
	        $newThread = curl_init();
	        curl_setopt_array($newThread, array(
	                CURLOPT_URL => $link,
	                CURLOPT_RETURNTRANSFER => true,
	                CURLOPT_CONNECTTIMEOUT => 10,
	                CURLOPT_TIMEOUT => 100,
	                CURLOPT_FAILONERROR => false,
	                CURLOPT_FILE => $fp // <- Сохраняем результат в файл

	            )
	        );

	        //Добавляем в мульти curl
	        curl_multi_add_handle($multicurlInit, $newThread);
	        //Закрываем поток
	        unset($newThread);
	    }
	    $curlMultiResult = curl_multi_exec($multicurlInit, $active);
	    do
	    {
	        $result = curl_multi_info_read($multicurlInit);
	        if( ! is_array($result))
	            break;

	        $info = curl_getinfo($result['handle']);
	        $raw = curl_multi_getcontent($result['handle']);

	        curl_multi_remove_handle($multicurlInit, $result['handle']);
	        curl_close($result['handle']);
	    } while(true);
	    if(count($img_arr) == 0 && $active == 0)
	        break;
	} while(true);

}

?>