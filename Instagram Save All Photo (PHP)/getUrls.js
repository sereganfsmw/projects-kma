var photo_count;

//Внедряем скрипт на страницу
function addScript(src)
{
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src =  src;
    var done = false;
    document.getElementsByTagName('head')[0].appendChild(script);

    script.onload = script.onreadystatechange = function() {
        if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
        {
            done = true;
            script.onload = script.onreadystatechange = null;
            loaded();
        }
    };
}

//Подгружаем JQuery
addScript("https://code.jquery.com/jquery-2.1.4.min.js");

//После загрузки скприпта
function loaded(){
    console.info('%c JQuery Injected','color:red');
    get_photo_count();
}

function get_photo_count(){
    var re = /\s/gi;
    var str = jQuery('span._bkw5z:eq(0)').text();
    str = str.replace(re, '');
    photo_count = parseInt(str);
    console.log(photo_count);

    scroll_bottom();
}

function generate_photo_list(){
    var re = /\/s640x640/gi;
    var str;
    var urls_arr = Array();
    jQuery.each(jQuery('._nljxa ._icyx7'), function(index, val) {
        var temp_arr = Object();
        str = val.src;
        str = str.replace(re, '');
        temp_arr['num'] = index;
        temp_arr['url'] = str;
        urls_arr.push(temp_arr)
        console.log(str);
    });    
    alert('Done');
    console.clear();
    console.log(JSON.stringify(urls_arr));
}

function scroll_bottom(){
    var interval = setInterval(function() {
        console.warn(jQuery('._nljxa ._icyx7').length);

        if (jQuery('._nljxa ._icyx7').length <= photo_count){
            if (jQuery('._nljxa ._icyx7').length == photo_count){
                clearInterval(interval);
                generate_photo_list();
            } else {
                jQuery('html, body').scrollTop( jQuery(document).height() - jQuery(window).height() );                
            }   
        }

    },1000);

}