-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 09 2013 г., 18:33
-- Версия сервера: 5.1.68-community-log
-- Версия PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `fileshare`
--

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `user` char(30) NOT NULL,
  `text` char(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `user`, `text`) VALUES
(2, 'Serega', 'Неплохой сайт'),
(3, 'Серега', 'Мост'),
(4, 'Серега', 'Мост');

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `user`, `type`, `file`, `size`, `time`, `date`, `password`) VALUES
(1, '', '.rar', 'Arhiv_RAR.rar', '0.02 Kb', '22:04:10', '08.07.13', ''),
(2, '', '.zip', 'Arhiv_ZIP.zip', '440.92 Kb', '22:04:39', '08.07.13', 'zip'),
(3, '', '.doc', 'Dokument_DOC.doc', '21.5 Kb', '22:04:47', '08.07.13', 'doc'),
(4, '', '.docx', 'Dokument_DOCX.docx', '12.33 Kb', '22:04:56', '08.07.13', ''),
(5, '', '.xls', 'Dokument_XLS.xls', '23.5 Kb', '22:05:07', '08.07.13', 'xls'),
(6, '', '.xlsx', 'Dokument_XLSX.xlsx', '8.28 Kb', '22:05:13', '08.07.13', ''),
(7, '', '.bmp', 'Izobrazhenie_BMP.bmp', '3075.05 Kb', '22:05:22', '08.07.13', 'bmp'),
(8, '', '.gif', 'Kartinka_GIF.gif', '271.85 Kb', '22:05:29', '08.07.13', ''),
(9, '', '.jpg', 'Kartinka_JPG.jpg', '477.45 Kb', '22:05:39', '08.07.13', 'jpg'),
(10, '', '.tif', 'Kartinka_TIF.tif', '1180.29 Kb', '22:05:46', '08.07.13', ''),
(11, '', '.pdf', 'Kniga_PDF.pdf', '1465.35 Kb', '22:05:57', '08.07.13', ''),
(12, '', '.png', 'Risunok_PNG.png', '673.04 Kb', '22:06:07', '08.07.13', 'png'),
(13, '', '.chm', 'Spravka_CHM.chm', '304.5 Kb', '22:06:22', '08.07.13', 'chm'),
(14, '', '.txt', 'Tekstovyy_fayl.txt', '0.87 Kb', '22:06:38', '08.07.13', ''),
(15, 'serega', '.rar', 'Arhiv_RAR.rar', '0.02 Kb', '22:21:29', '08.07.13', ''),
(16, 'serega', '.zip', 'Arhiv_ZIP.zip', '440.92 Kb', '22:34:22', '08.07.13', '');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(1) unsigned NOT NULL AUTO_INCREMENT,
  `username` char(30) NOT NULL,
  `password` char(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'root', 'root'),
(4, 'serega', 'most'),
(7, 'dima', 'smykov');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
