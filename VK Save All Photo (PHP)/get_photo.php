<?php
header("Content-Type: text/html; charset=utf-8");

//Отключаем время ожидания выполнения скрипта
set_time_limit(0);

$total_count;
$img_arrays = array();

function vk_api($offset,&$img_arrays){
	global $total_count;
	$offset = isset($offset) ? $offset : 0;
/*
	$request_params = array(
		'owner_id' => $_POST['vk_id'],
		'access_token' => $_POST['vk_token'],
		'extended' => false,
		'offset' => $offset,
		'count' => 200,
		'no_service_albums' => false,
		'need_hidden' => false,
		'v' => '5.64'
	);*/

	$request_params = array(
		'owner_id' => $_POST['vk_id'],
		'access_token' => $_POST['vk_token'],
		'album_id' => 'wall',
		'offset' => $offset,
		'count' => 200,
		'v' => '5.64'
	);

	$get_params = http_build_query($request_params);
	// $result = json_decode(file_get_contents('https://api.vk.com/method/photos.getAll?'. $get_params));
	$result = json_decode(file_get_contents('https://api.vk.com/method/photos.get?'. $get_params));

	if (isset($result->response) && !empty($result->response->items)){
		$total_count = $result->response->count;

		for ($i=0; $i < count($result->response->items); $i++) { 
			$temp = array();

			$temp['id'] = $i;

			//Получить ссылки
			if (isset($result->response->items[$i]->photo_2560)){
				$temp['url'] = $result->response->items[$i]->photo_2560;
			} else if(isset($result->response->items[$i]->photo_1280)){
				$temp['url'] = $result->response->items[$i]->photo_1280;
			} else if(isset($result->response->items[$i]->photo_807)){
				$temp['url'] = $result->response->items[$i]->photo_807;
			} else if(isset($result->response->items[$i]->photo_604)){
				$temp['url'] = $result->response->items[$i]->photo_604;
			} else if(isset($result->response->items[$i]->photo_130)){
				$temp['url'] = $result->response->items[$i]->photo_130;
			} else if(isset($result->response->items[$i]->photo_75)){
				$temp['url'] = $result->response->items[$i]->photo_75;
			}

			//Получить дату
			$temp['date'] = date('Y.m.d',$result->response->items[$i]->date);

			//Получить имя
			preg_match('|\/([^/]*)$|', $temp['url'], $file_name);
			$temp['name'] = $file_name[1];

			$img_arrays[] = $temp;
		}

		//Рекурсивный вызов
		if ($offset <= $total_count){
			vk_api($offset+200,$img_arrays);			
		}
	} else {
		if ($total_count == count($img_arrays)){
			echo "All Photos Downloads [$total_count]";
			echo "<hr><br>";
			save_image_from_url($img_arrays);
			echo "<pre>";
			var_dump($img_arrays);
			echo "</pre>";
		}
	}

}
$dir = $_POST['vk_folder'];
if (!is_dir($dir)) {
    mkdir($dir);         
}

vk_api(0,$img_arrays);

//Потоковое скачивание картинок
function save_image_from_url($img_arr){

	$maxThreads = 100; // Количество потоков
	$multicurlInit = curl_multi_init();
	do
	{
	    while(@$active <= $maxThreads)
	    {
	        @$active++;
	        if(count($img_arr) == 0)
	            break;

	        $id = array_rand($img_arr);
	        $link = $img_arr[$id]['url'];
	        $date = $img_arr[$id]['date'];
	        $name = $img_arr[$id]['name'];
	        unset($img_arr[$id]);

	        //Формируем название и создаем дескриптор файла
	        $file_name = $_POST['vk_folder'].'/'.$date.' '.$name;
		    if(file_exists($file_name)){
		        unlink($file_name);
		    }
		    $fp = fopen($file_name,'x');

		    //Создаем поток
	        $newThread = curl_init();
	        curl_setopt_array($newThread, array(
	                CURLOPT_URL => $link,
	                CURLOPT_RETURNTRANSFER => true,
	                CURLOPT_CONNECTTIMEOUT => 10,
	                CURLOPT_TIMEOUT => 100,
	                CURLOPT_FAILONERROR => false,
	                CURLOPT_FILE => $fp // <- Сохраняем результат в файл

	            )
	        );

	        //Добавляем в мульти curl
	        curl_multi_add_handle($multicurlInit, $newThread);
	        //Закрываем поток
	        unset($newThread);
	    }
	    $curlMultiResult = curl_multi_exec($multicurlInit, $active);
	    do
	    {
	        $result = curl_multi_info_read($multicurlInit);
	        if( ! is_array($result))
	            break;

	        $info = curl_getinfo($result['handle']);
	        $raw = curl_multi_getcontent($result['handle']);

	        curl_multi_remove_handle($multicurlInit, $result['handle']);
	        curl_close($result['handle']);
	    } while(true);
	    if(count($img_arr) == 0 && $active == 0)
	        break;
	} while(true);

}

?>