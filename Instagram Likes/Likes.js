var scroll_count=20;
var window_height = 0;

//Внедряем скрипт на страницу
function addScript(src)
{
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src =  src;
    var done = false;
    document.getElementsByTagName('head')[0].appendChild(script);

    script.onload = script.onreadystatechange = function() {
        if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
        {
            done = true;
            script.onload = script.onreadystatechange = null;
            loaded();
        }
    };
}

//Подгружаем JQuery
addScript("https://code.jquery.com/jquery-2.1.4.min.js");

//После загрузки скприпта
function loaded(){
    console.clear();
    console.info('%c JQuery Injected','color:red');
    scroll_bottom();
}

function like_all_hearts(){
    var buttons = jQuery('.coreSpriteHeartOpen').parent();
    console.log(buttons);

    jQuery( buttons ).each(function( index, element ) {
        element.click();
    });
    jQuery('html, body').scrollTop( window_height + jQuery(window).height() );
    window_height += jQuery(window).height();
}

function scroll_bottom(){
    var counter = 0;
    var interval = setInterval(function() {

        if (counter < scroll_count){
            console.warn('Counter: '+counter);
            counter++;
            like_all_hearts();
             
        } else {
            clearInterval(interval);  
        }

    },2000);
}