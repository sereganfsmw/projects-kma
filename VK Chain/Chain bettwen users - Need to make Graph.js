// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript (28.04.2016)
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';

console.log('%c VK Script by Serega MoST: Loaded','color:blue;');
//Посылаем JSONP запрос к серверу
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
} 

//===============ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============
var users = {
    first : null,
    second : null
}
//Массив друзей первого пользователя
var friends_user_1 = new Array;
//Массив друзей второго пользователя
var friends_user_2 = new Array;
//Текущий польхователь
var current_user;
var secondUserFriends = new Array;
//Цепочка между пользователями
var chain_friens = new Array;

//Очередь пользователей для проверки
var queue = new Array;
//Проверенные пользователи
var checked = new Array;
var checked_counter = 0;
//Флаг найдено
var find = false;
var ids_valid = {
    first : false,
    second : false,
};
var reverse = false;
var new_search = false;
var start_time;
var end_time;
//===============/ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============

    function addScript(src)
    {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src =  src;
        var done = false;
        document.getElementsByTagName('head')[0].appendChild(script);

        script.onload = script.onreadystatechange = function() {
            if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
            {
                done = true;
                script.onload = script.onreadystatechange = null;
                loaded();
                               
                css();

                //Проверяем загрузился ли JQuery
                if ($) {
                    ready();
                }
            }
        };
    }

    //Подгружаем JQuery
    addScript("https://code.jquery.com/jquery-2.1.4.min.js");

    //Дополнительные скрипты и css файлы
    function addon(){

        var scripts_js = [
            'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24VXM0WC1FWm1SVkk',
        ];

        var style_css = [
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
            'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24M25wTDB2eUpBNm8',
        ];

        //Скрипты
        if (scripts_js.length > 0){    
            $.each(scripts_js, function(index, val) {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = val;
                document.getElementsByTagName('head')[0].appendChild(script);
            });
        }

        //Стили
        if (style_css.length > 0){  
            $.each(style_css, function(index, val) {
                var style = document.createElement("link");
                style.type = "text/css";
                style.rel  = 'stylesheet';
                style.href = val;
                document.getElementsByTagName('head')[0].appendChild(style);
            });              
        }
    }

    //After Script Load
    function loaded(){
        console.log('%c JQuery Injected','color:red');           
    }

    //Создание стилей CSS для новых елементов
    function css(){
        var head = document.children[0];
        var css = `

        #chain_script.chain_block{
            margin-top: 100px;
        }

        #chain_script .page_top .chain_status{
            float: right;
            color: #828282;
            width: 25%; 
        }

        #chain_script .chain_status{
            display:none;           
        }

        #chain_script .chain_status span#current_user,
        #chain_script .chain_status span#check_count
        {
            font-weight: bold;
        }

        #chain_script .chain-progress{
            margin-top: 20px;
            margin-bottom: 20px;
        }

        #chain_script .user_fields{

            -webkit-transition: all .25s linear;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;      

            border-width: 0px;
            background: transparent;
            width: 100%;             
            text-align: center;      
        }

        #chain_script span.input_wrapper{
            position: relative;
            width: 100%;                   
        }

        #chain_script span.input_wrapper:after{
            content: "";
            position: absolute;

            -webkit-transition: all .5s linear;
            -moz-transition: all .5s linear;
            -ms-transition: all .5s linear;
            -o-transition: all .5s linear;
            transition: all .5s linear;   

            height: 2px;
            background: #d7d8db;
            width: 10%;
            bottom: -5px;
            left: 50%;
            transform: translateX(-50%);                            
        }

        #chain_script span.input_wrapper.focus:after,
        #chain_script span.input_wrapper.warning:after,
        #chain_script span.input_wrapper.success:after{                
            width: 50%;  
        }

        #chain_script span.input_wrapper.warning:after{                
            background: red;
        }

        #chain_script span.input_wrapper.success:after{                
            background: blue;
        }

        #chain_script .users_inputs_wrapper span:first-child{
            text-align: left;
        }

        #chain_script .users_inputs_wrapper span:last-child{
            text-align: right;
        }

        #chain_script .open_block{
            cursor: pointer;
        }

        #chain_script .users_inputs_wrapper{
            margin-top: 20px;
            margin-bottom: 20px;
        }

        #chain_script .chain_container, #chain_script .users_inputs_wrapper{
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-align-content: center;
            -ms-flex-line-pack: center;
            align-content: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;            
        }

        #chain_script .chain_container{
            overflow-x:scroll;            
        }

        #chain_script .users_chain{
            position: relative;
            background: #edeef0;
            border-radius: 2px;
            box-shadow: 0 1px 0 0 #d7d8db, 0 0 0 1px #e3e4e8;
        }

        #chain_script .chain{
            position: relative;
            text-align: center;
            padding: 30px;
            z-index: 1;
            width: 50%;

            -webkit-transition: all 1s linear;
            -moz-transition: all 1s linear;
            -ms-transition: all 1s linear;
            -o-transition: all 1s linear;
            transition: all 1s linear;             
        }

        #chain_script .chain.added .link_to_user{
            opacity:0; 
        }

        #chain_script .users_chain.find .chain.added + .chain.added:before,
        #chain_script .users_chain.find .chain.first_user + .chain.added:before,
        #chain_script .users_chain.find .chain.added + .chain.second_user:before,
        #chain_script .users_chain.find .chain.first_user + .chain.second_user:before
        {
            content: "";
            height: 2px;
            top: 45%;
            transform: translateY(-45%);
            position: absolute;
            background: #fff;
            box-shadow: 0 1px 0 0 #d7d8db, 0 0 0 1px #e3e4e8;
            width: 100%;
            right: 50%;
            z-index: -1;
        }   

        #chain_script .chain.first_user
        {
            z-index: 20;
        }

        #chain_script .chain.second_user
        {
            z-index: 0;
        }

        #chain_script .link_to_user img.user_img{
            border-radius: 50%;
            max-width: 100px;
            height: 100px;

            -webkit-transition: all .25s linear;
               -moz-transition: all .25s linear;
                -ms-transition: all .25s linear;
                 -o-transition: all .25s linear;
                    transition: all .25s linear;                      
        }

        #chain_script .link_to_user .img_wrapper{
            position: relative;
            margin: 0px 30px 10px 30px;
            border-radius: 50%;
            overflow: hidden;
            border: 2px solid #ffffff;
            max-width: 100%;
            height: 100px;
            -webkit-transition: all .25s linear;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;
            display: inline-block;                     
        }

        #chain_script .link_to_user .img_wrapper .user_id{
            position: absolute;
            bottom: 0px;
            left: 50%;
            transform: translateX(-50%);
            opacity: 0;
            color: white;
            display: block;
            width: 100%;
            padding: 1px;
            -webkit-transition: all .25s linear ;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;                     
        }

        #chain_script .link_to_user .img_wrapper .user_id:hover{
            background: rgba(0,0,0,0.5);
        }

        #chain_script .link_to_user .img_wrapper:hover,
        #chain_script .link_to_user .img_wrapper:hover img.user_img
        {
            border-radius: 0%;           
        }

        #chain_script .link_to_user .img_wrapper:hover .user_id{
            opacity: 1;
        }

        #chain_script .link_to_user img.user_img[src=""] {
            content:url("/images/error404.png");
            background: white;
            max-width: 100px;
        }

        #chain_script .link_to_user .user_label{
            display: block;
        }
        

        #chain_script .chain_content{
            -webkit-transition: all .25s linear;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;     

            display:none;     
        }

        `;
        var inline_style = document.createElement('style');
        inline_style.type = 'text/css';
        inline_style.appendChild(document.createTextNode(css));   
        head.appendChild(inline_style);        
    }      

    //JQuery загружен
    function ready() {

        //ID страницы текущего пользователя
        var my_id = vk.id;

        //Подгружаем дополнительные скрипты и стили
        addon(); 

        //Функция для вызова методов API VK
        function vkApi( method, params, callback) {
            var cb = 'cb_vkapi';
            $.ajax({
                url: 'https://api.vk.com/method/'+method,
                data: params,
                dataType: "jsonp",
                callback: cb,
                success: callback
            });
        }

        function validButton(){
            console.log(ids_valid);
            console.warn($('#first_user_id').val());
            console.log($('#second_user_id').val());
            if (($('#first_user_id').val() != '') && ($('#second_user_id').val() != '') && ids_valid.first && ids_valid.second && ( $('#first_user_id').val() != $('#second_user_id').val() )){
                $('#run_script').prop('disabled', false).removeClass('secondary');
            } else {
                $('#run_script').prop('disabled', true).addClass('secondary');
            }    
        }

        function drawBlock(){
            $('#page_layout').prepend(`

                <div class="page_block chain_block" id="chain_script">
                <div class="page_info_wrap">
                <div class="page_top open_block">
                <div class="chain_status">Анализ страницы:<span id="current_user"></span></div>
                <div class="chain_status">Проверенно страниц:<span id="check_count"></span></div>
                <h2 class="page_name">Цепочка между пользователями</h2>
                <div class="page_current_info">Серега Мост</div>
                </div>

                    <div class="chain_content">

                        <div class="users_chain">
                            <div class="chain_container">
                                <div id="user_1" class="chain first_user">
                                    <a class="link_to_user" href="#" target="_blank">
                                    <div class="img_wrapper">
                                        <img class="user_img" src="" alt="">
                                        <span class="user_id"></span>
                                    </div>
                                    <span class="user_label">Не найдено</span>
                                    </a>
                                </div>

                                <div id="user_2" class="chain second_user">
                                    <a class="link_to_user" href="#" target="_blank">
                                    <div class="img_wrapper">
                                        <img class="user_img" src="" alt="">
                                        <span class="user_id"></span>
                                    </div>
                                    <span class="user_label">Не найдено</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                           <div class="users_inputs_wrapper">
                                <span class="input_wrapper"><input type="text" class="user_fields" data-id="first" id="first_user_id" placeholder="Введите ID первого пользователя"></span>
                                <span class="input_wrapper"><input type="text" class="user_fields" data-id="second" id="second_user_id" placeholder="Введите ID второго пользователя"></span>
                            </div>        

                            <div class="chain-progress chain_status">
                                <div id="progress_bar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span id="progress_text">Поиск</span>
                                </div>
                            </div>

                        <button class="flat_button button_wide secondary" id="run_script" disabled><i class="fa fa-users"></i> <b>Поиск связей</b></button>                </div>

                    </div>
                </div>

            `);
        }

        function fillUserInfo(el,data){
            $(el).find('.link_to_user').attr({
                href: '/id'+data[0]
            });
            $(el).find('.img_wrapper .user_img').attr({
                src: data[3],
                alt: data[1]+' '+data[2]
            });
            $(el).find('.img_wrapper .user_id').text(data[0]);
            $(el).find('.user_label').text(data[1]+' '+data[2]);
        }

        function clearUserInfo(el){
            $(el).find('.link_to_user').attr({
                href: '#'
            });
            $(el).find('.img_wrapper .user_img').attr({
                src: '',
                alt: ''
            });
            $(el).find('.img_wrapper .user_id').text('');
            $(el).find('.user_label').text('Не найдено');
        }

        function getUserInfo(el){
            var field = $(el);
            var id = field.val();
            var info_block = $('.chain.'+field.data('id')+'_user');

                vkApi('users.get', {user_ids:id,fields:'photo_100',lang:'ru'}, function(data){  
                    if(data.response!=undefined && (typeof data.response[0].deactivated == "undefined") ){
                        var user_info = data.response[0];
                        var id = user_info.uid;
                        var name = user_info.first_name;
                        var last_name = user_info.last_name;
                        var img = user_info.photo_100;
                        var data = [id,name,last_name,img];

                        ids_valid[field.data('id')] = true;
                        field.parent().removeClass('warning').addClass('success');
                        fillUserInfo(info_block,data);
                        validButton();
                    } else {
                        ids_valid[field.data('id')] = false;
                        field.parent().removeClass('success').addClass('warning');                    
                        clearUserInfo(info_block);
                        validButton();
                    }
                });    
        }

        function disableElements(){
            $('#run_script').prop('disabled', true).addClass('secondary');
            $('.user_fields').prop('disabled', true);
        }

        function enableElements(){
            $('#run_script').prop('disabled', false).removeClass('secondary');
            $('.user_fields').prop('disabled', false);
        }

        function checkUsers(){
            console.clear();
            //Засикаем время начала работы скрипта
            start_time = new Date().getTime();
            //Массив цепочки
            chain_friens = [];
            //Уоличество друзей у пользователей
            var count_friends_user_1;
            var count_friends_user_2;

            vkApi('friends.get', {user_id:users.first}, function(data){
                //Получаем информацию о пользователе по странице
                friends_user_1 = data.response; 
                count_friends_user_1 = data.response.length;
                
                if (count_friends_user_1 == 0){
                    $('#progress_text').html('Невозможно построить цепочку [!] (У пользователя 1 нет друзей...)').parent().addClass('progress-bar-danger');
                    $('#check_count').html(' 0');
                    enableElements();            
                    return;
                }

                vkApi('friends.get', {user_id:users.second}, function(data){
                    //Получаем информацию о пользователе по странице
                    friends_user_2 = data.response; 
                    count_friends_user_2 = data.response.length;

                    if (count_friends_user_2 == 0){
                        $('#progress_text').html('Невозможно построить цепочку [!] (У пользователя 2 нет друзей...)').parent().addClass('progress-bar-danger');
                        $('#check_count').html(' 0');
                        enableElements();
                        return;
                    }

                    //Меняем пользователей местами (Начиная поиск с того у кого меньше друзей)
                    if (count_friends_user_1 > count_friends_user_2){
                        var buff = users.first;
                        users.first = users.second;
                        users.second = buff;
                        console.log('Swap users');
                        reverse = true;
                    }
                    //Добавляем второго пользователя в начало цепочки
                    chain_friens.push(users.second);
                    //Поиск между двумя пользователями
                    searchUser(users.first,users.second);            
                });
            });

        }

        //Поиск между двумя пользователями
        function searchUser(user1,user2){    
        //Очередь пользователей для проверки
        queue = new Array;
        //Проверенные пользователи
        checked = new Array;
        //Текущий пользователь
        current_user = user1;
        find = false;

                vkApi('friends.get', {user_id:user2}, function(data){
                    //Получаем информацию о пользователе по странице
                    secondUserFriends = data.response; 

                        //Если первый пользователь друг второго
                        if(secondUserFriends.indexOf(user1) != -1){
                            chain_friens.unshift(user1);
                            console.log('CHAIN DONE [SIMPLE]');
                            console.info(chain_friens);
                            //Показать цепочку друзей
                            showChain(chain_friens);
                        }else{                
                            //Если нужно проверять рекурсивно кажлого пользователя
                            userIntersection();
                        }                    
                });

                function userIntersection() {
                        var url = 'https://api.vk.com/method/friends.get?user_id='+current_user;
                        //Запрос (Получаем информацию о пользователях)
                        $.ajax({
                            url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                            type: 'GET',
                            dataType: 'jsonp',
                            crossDomain: true,
                            success: function(data){
                                console.log(current_user);
                                                      
                                //Если ошибка 
                                if (data.error && queue.length == 0){
                                    $('#progress_text').html('Невозможно построить цепочку [!] (У пользователя слишком мало друзей или они заблокированы)').parent().addClass('progress-bar-danger');
                                    enableElements();                                    
                                    return;
                                }

                                //Если страница не удаленна и не ошибка
                                if(data.response!=undefined && !data.error){
                                    for (var i = 0; i < data.response.length; i++) {
                                        if(checked.indexOf(data.response[i]) == -1 ){ //Если пользователь еше не проверялся
                                            //Выводить результаты перебора
                                            $('#current_user').html(' [ID:'+data.response[i]+']');
                                            $('#check_count').html(' '+(!find ? checked.length : checked_counter));

                                                if(secondUserFriends.indexOf(data.response[i]) !=-1 ){ //Если найдено пользователя
                                                    find = true;
                                                    chain_friens.unshift(data.response[i]);
                                                    chain_friens.unshift(current_user);
                                                    console.warn('FOUND IN RECURSION [!]');
                                                    i=data.response.count;
                                                } else{
                                                    queue.push(data.response[i]); //(Не найдено) Нужно проверить у друзей пользователя                                                
                                                }                
                                        }
                                    }
                                }

                                //Добавить пользователя в массив проверенных
                                checked.push(current_user);
                                console.log(checked);
                                
                                console.log(current_user);
                                current_user = queue.shift();                        
                                console.warn(current_user);                        
                
                                if ((queue.length == 0 && (typeof current_user == "undefined")) || find == true ) {
                                
                                    if(chain_friens[0] == user1){
                                        console.log('CHAIN DONE [RECURSION]');  
                                        console.info(chain_friens);
                                        //Показать цепочку друзей
                                        showChain(chain_friens);
                                    }else{                                                                    
                                        console.log('Compare two users');
                                        if (checked_counter == 0){
                                            checked_counter = checked.length;
                                        }
                                        searchUser(user1,chain_friens[0]);
                                    }
                                }else{
                                    //Рекурсивно вызываем саму себя
                                    userIntersection();
                                }
                            }
                        });
                }
        }

function showChain(chain){
    new_search = true;
    $('.users_chain').addClass('find');
    $('#check_count').html(' '+checked_counter);
    enableElements();
    end_time = new Date().getTime() - start_time;
    $('#progress_text').html('Поиск завершен (Время: <b>'+Math.floor(end_time/1000)+' секунд</b>)').parent().removeClass('progress-bar-danger').addClass('progress-bar-success');

    // jQuery('.users_chain .chain.added').fadeOut('slow', function() {
    //     jQuery(this).remove();
    // });



    // console.log('Chain');
    // console.warn(chain);
    
    if (reverse) {
        chain = chain.reverse();
    }
    
    
    // delete chain[users.first];
    // delete chain[users.second];   

    for (var i = 0; i < chain.length; i++) {
        if (chain[i] == users.first || chain[i] == users.second){
            delete chain[i];
        }
    };

    

    var str_users_ids = chain.join(',');
    


        vkApi('users.get', {user_ids:str_users_ids,fields:'photo_100',lang:'ru'}, function(data){
            if(data.response!=undefined && (typeof data.response[0].deactivated == "undefined") ){
                for (var i = 0; i < data.response.length; i++) {
                    var z_index = data.response.length - i;

                    var user_info = data.response[i];
                    var id = user_info.uid;
                    var name = user_info.first_name;
                    var last_name = user_info.last_name;
                    var img = user_info.photo_100;
                    
                    var html = 
                    `

                        <div class="chain added" style="z-index: `+z_index+`;">
                            <a class="link_to_user" href="/id`+id+`" target="_blank">
                            <div class="img_wrapper">
                                <img class="user_img" src="`+img+`" alt="`+name+` `+last_name+`">
                                <span class="user_id">`+id+`</span>
                            </div>
                            <span class="user_label">`+name+` `+last_name+`</span>
                            </a>
                        </div>

                    `;
    
                        // jQuery('.users_chain .chain').first().after(html);
                    // if (reverse) {
                        jQuery('.users_chain .chain').last().before(html);

                    // } else {
                    // }
    
                }

                
            }

            jQuery('.users_chain .chain.added .link_to_user').animate({
                opacity: 1,
            }, 2000, function() {
            // Animation complete.
            });

            // jQuery('.users_chain .chain.added').fadeIn('3000', function() {
                
            // });
        }); 

}
        //SEND
        //Функция для отправки сообщений всем друзям


        //Получаем все сообщения с сервера
        function getMessages(data,key){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }

            //Пожалуйста подождите
            $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Загрузка <b>сообщений</b>...</div>
                    </div>
                      </div>
                    `
            );            

            var users = new Array();
            var msg_data = new Array();
            
            //Если нужно вернуть ключи
            if (key) {
                for( i=1; i<data.response.length;i++) {
                    if (/\[(.+)\]/.test(data.response[i].body)) {
                        //Формируем массив ключей
                        msg_data.push(data.response[i]);
                    }
                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                } 
            } else {
            //Если нужно показать все сообщения
                for( i=1; i<data.response.length;i++) {
                    //Формируем массив сообщений
                    msg_data.push(data.response[i]);

                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                }                 
            }

            var flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            // console.clear();
            var user_data = new Array();
            for (key in users) {
                
                //Исправление ошибки (встречаються недопустимые символы - в ID)
                var fixed_key = users[key]+'';
                fixed_key = Number(fixed_key.match(/\d+/));
                // var fixed_key = parseInt(value.replace(/\D+/g,""));
 
                //Получаем информацию о пользователях
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+fixed_key+'&fields=photo_100,online&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        // console.warn(data);
                        var obj = data.response[0];
                        // console.log(user_data);
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 

        //===============/МЕНЮ СЛЕВА===============
        //Кнопка включения скрипта
        // $('#side_bar').prepend('<button class="flat_button fl_l view_other" style="width:100%; margin-top:50px;" id="activate_script"><i class="fa fa-power-off"></i> <b>Активировать</b></button><p>Script by Serega MoST</p>');
        // document.getElementById('activate_script').onclick = function() {
        //     activate();
        // }; 

//Запуск скрипта
var activate = function() {
    //Очищаем все обработчики событий с document (для многократного вызова функции)
    $(document).off();
        //Проверка на главную страницу
        if (/(^https:\/\/)?vk\.com\/(?!friends|albums|video|im|groups|feed|settings|apps)[a-zA-Z][a-zA-Z_\.0-9]+[a-zA-Z0-9]$/.test(document.location.href)) {
            console.warn('Main Page');

            drawBlock();
            //===============ОБРАБОТЧИКИ СОБЫТИЙ===============
            $(document).on('click', '.open_block', function(event) {
                $('#chain_script .chain_content ').toggle('fast');
            }); 
            
           $(document).on('keyup', '.user_fields', function(event) {
                if ($(this).val() != ''){
                    if (new_search){
                        jQuery('.users_chain .chain.added .link_to_user').animate({
                            opacity: 0,
                        }, 1000, function() {
                            $('.users_chain').removeClass('find');
                            $('.chain_status').hide();                
                            jQuery('.users_chain .chain.added').remove();        
                        });          
                        new_search = false;
                    }
                    
                    getUserInfo($(this));
                } else {
                    clearUserInfo($('.chain.'+$(this).data('id')+'_user'));
                    $(this).parent().removeClass('success').removeClass('warning');
                    validButton();
                }
            });  

           $(document).on('click', '#run_script', function(event) {
                $('#progress_text').html('Поиск').parent().removeClass('progress-bar-success').removeClass('progress-bar-danger');
                disableElements();


                $('.chain_status').show();

                $('.user_fields').each(function(index, el) {
                    var id = $(el).val();
                    users[$(el).data('id')] = parseInt(id);
                });

                checkUsers();
            }); 


            $(".user_fields").focus(function(){
                $(this).parent().addClass('focus');
            });

            $(".user_fields").blur(function(){
                $(this).parent().removeClass('focus');
            });         
            


        }    
        //===============СООБЩЕНИЯ===============
    }//Activate
    activate();
    }//Ready







/*        function sendMessages() {
            //Если значение щетчика еше меньше длины массива
            if (counter_send < send_arr.length){

                
                var url = 'https://api.vk.com/method/messages.send?user_id='+send_arr[counter_send]+'&message='+all_msg_text+'&access_token='+search_token;

                //Запрос (Отправляем сообщение всем друзям)
                $.ajax({
                    url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function(data){
                        console.warn(data); //ID
                        console.log(counter_send); //Щечик

                        //Если ошибка
                        if (data.error){
                            //6:Too many requests per second
                            if (data.error.error_code == 6){
                                //Ставим задержку на вызов функции в 1 секунды
                                setTimeout(sendMessages,1000);
                            }

                            //14:Captcha needed
                            if (data.error.error_code == 14){
                                //Действие которое выполняеться
                                $('#captcha-action').val('send');
                                //Картинка каптчи
                                var img = data.error.captcha_img;
                                $('#captcha-img').attr('src', img);
                                //Идентификатор каптчи
                                captcha_sid = data.error.captcha_sid;
                                //Показать блок каптчи
                                $('#captcha').show();
                                //Ставим фокус на поле ввода
                                $('#captcha-text').focus();

                                //Меняем прогресс-бар (Пауза)
                                var percents= (Math.ceil(100/(send_arr.length/counter_send))/2)+'%';                                     
                                $('#progress_bar').width(percents);
                                $('#progress_text').html('<b>'+percents+'</b> Пауза [<b>'+counter_send+'</b>/'+send_arr.length+']');
                                //Меняем стиль прогресс бара   
                                $('#progress_bar').addClass('progress-bar-warning');
                            }                                        
                        } else { //Если все нормально
                            console.log(data.response);

                            //Меняем прогресс-бар
                            var percents= (Math.ceil(100/(send_arr.length/counter_send))/2)+'%';                                         
                            $('#progress_bar').width(percents);
                            $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter_send+'</b>/'+send_arr.length+']'); 

                            //Инкреминтируем значение в случае удачного запроса
                            counter_send++;

                            //Рекурсивно вызываем саму себя
                            sendMessages();
                        }//ERROR
                    }//SUCCESS
                });
            } else {
                // Сообщение отправлено
                $("#view_message_info").slideDown(300).delay(5000).slideUp(300);
                //Очищаем поле
                $('#other_message_text').text('');
                //Удаляем блок загрузки
                $('#loader').remove();
            } 
        }
*/

/*

        function getMessages(data,key){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }

            //Пожалуйста подождите
            $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Загрузка <b>сообщений</b>...</div>
                    </div>
                      </div>
                    `
            );            

            var users = new Array();
            var msg_data = new Array();
            
            //Если нужно вернуть ключи
            if (key) {
                for( i=1; i<data.response.length;i++) {
                    if (/\[(.+)\]/.test(data.response[i].body)) {
                        //Формируем массив ключей
                        msg_data.push(data.response[i]);
                    }
                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                } 
            } else {
            //Если нужно показать все сообщения
                for( i=1; i<data.response.length;i++) {
                    //Формируем массив сообщений
                    msg_data.push(data.response[i]);

                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                }                 
            }

            var flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            // console.clear();
            var user_data = new Array();
            for (key in users) {
                
                //Исправление ошибки (встречаються недопустимые символы - в ID)
                var fixed_key = users[key]+'';
                fixed_key = Number(fixed_key.match(/\d+/));
                // var fixed_key = parseInt(value.replace(/\D+/g,""));
 
                //Получаем информацию о пользователях
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+fixed_key+'&fields=photo_100,online&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        // console.warn(data);
                        var obj = data.response[0];
                        // console.log(user_data);
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 


*/

// Тестовая страница (Мало друзей)
// https://vk.com/id75847586
//75847586
//89798788
//43243893