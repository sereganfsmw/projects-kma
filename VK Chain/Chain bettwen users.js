// ==UserScript==
// @name         VK Script
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Try API VK witch JavaScript (28.04.2016)
// @author       Serega MoST
// @include      https://*vk.com/*
// @include      *vk.com/*
// @grant        none
// @run-at       document-end
// ==/UserScript==
//'use strict';

console.log('%c VK Script by Serega MoST: Loaded','color:blue;');
//Посылаем JSONP запрос к серверу
function jsonp(url, callback) {
    var callbackName = 'jsonp_callback_' + Math.round(100000 * Math.random());
    window[callbackName] = function(data) {
        delete window[callbackName];
        document.body.removeChild(script);
        callback(data);
    };

    var script = document.createElement('script');
    script.src = url + (url.indexOf('?') >= 0 ? '&' : '?') + 'callback=' + callbackName;
    document.body.appendChild(script);
} 

//===============ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============
//Главная функция
var activate;

var find_way;
//Дерево пользователей
var users_tree = new Object;

var users = {
    first : null,
    second : null
}

var info_users = new Object;
//Массив друзей первого пользователя
var friends_user_1 = new Array;
//Массив друзей второго пользователя
var friends_user_2 = new Array;
//Текущий польхователь
var current_user;
var secondUserFriends = new Array;
//Цепочка между пользователями
var chain_friends = new Array;
var original_chain = new Array;
var tree = new Object;
var tree_way = new Object;

//Очередь пользователей для проверки
var queue = new Array;
//Проверенные пользователи
var checked = new Array;

var unique_id = new Array;

var checked_counter = 0;
//Флаг найдено
var find = false;
var ids_valid = {
    first : false,
    second : false,
};
var reverse = false;
var new_search = false;
var start_time;
var end_time;
//===============/ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ===============

    function addScript(src)
    {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src =  src;
        var done = false;
        document.getElementsByTagName('head')[0].appendChild(script);

        script.onload = script.onreadystatechange = function() {
            if ( !done && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
            {
                done = true;
                script.onload = script.onreadystatechange = null;
                loaded();                            
                css();

                //Проверяем загрузился ли JQuery
                if ($) {
                    ready();
                }
            }
        }
    }

    //Подгружаем JQuery
    addScript("https://code.jquery.com/jquery-2.1.4.min.js");

    //Дополнительные скрипты и css файлы
    function addon(){

        var scripts_js = [
            'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24WTI5ejdBWjBnRG8',
        ];

        var style_css = [
            'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
            'https://drive.google.com/uc?export=download&id=0BysHDmPRTH24M25wTDB2eUpBNm8',
        ];

        //Скрипты
        if (scripts_js.length > 0){   
            var scripts_loaded = 0; 
            $.each(scripts_js, function(index, val) {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.src = val;
                var loaded = false;
                document.getElementsByTagName('head')[0].appendChild(script);

                script.onload = script.onreadystatechange = function() {
                    if ( !loaded && (!this.readyState || this.readyState === "loaded" || this.readyState === "complete") )
                    {
                        loaded = true;
                        script.onload = script.onreadystatechange = null;
                        
                        console.warn('GRAPH Loaded');
                        scripts_loaded ++;
                    }
                }

            });
        }

        //Таймер
        var interval = setInterval(function() {
            //Проверка
            if (scripts_js.length != scripts_loaded) return;

            console.warn('RUN MAIN');
            //Отключить таймер
            clearInterval(interval);             

            //Запустить главную функцию после хагрузки всех скриптов
            activate();
        },300);


        //Стили
        if (style_css.length > 0){  
            $.each(style_css, function(index, val) {
                var style = document.createElement("link");
                style.type = "text/css";
                style.rel  = 'stylesheet';
                style.href = val;
                document.getElementsByTagName('head')[0].appendChild(style);
            });              
        }
    }

    //After Script Load
    function loaded(){
        console.log('%c JQuery Injected','color:red');           
    }

    //Создание стилей CSS для новых елементов
    function css(){
        var head = document.children[0];
        var css = `



.node {
    cursor: pointer;
  }

  .overlay{
      background-color:#EEE;
  }
   
  .node circle {
    fill: #fff;
    stroke: steelblue;
    stroke-width: 1.5px;
  }
   
  .node text {
    font-size:10px; 
    font-family:sans-serif;
  }
   
  .link {
    fill: none;
    stroke: #ccc;
    stroke-width: 1.5px;
  }

  .templink {
    fill: none;
    stroke: red;
    stroke-width: 3px;
  }

  .ghostCircle.show{
      display:block;
  }

  .ghostCircle, .activeDrag .ghostCircle{
       display: none;
  }

        #chain_script.chain_block{
            margin-top: 100px;
            z-index: 1;
        }

        #chain_tree{
            display:none;
            position: relative;
            box-shadow: 0 1px 0 0 #d7d8db, 0 0 0 1px #e3e4e8;
            overflow: hidden;
        }

        #show_tree{
            margin-top: 20px;
            display:none;
            background-color: #ce2121;
        }

        #show_tree:hover{
            background-color: #ad1b1b;
        }

        #show_tree:active{
            background-color: #e21a1a;
        }

        #chain_tree .node_info{
            position: absolute;
            left: 0;
            top: 0;
            text-align: center;
            background-color: #EEE;
            box-shadow: 0 1px 0 0 #d7d8db, 0 0 0 1px #e3e4e8;

    -webkit-transition: all .25s linear;
       -moz-transition: all .25s linear;
        -ms-transition: all .25s linear;
         -o-transition: all .25s linear;
            transition: all .25s linear;            
        }

        #chain_tree .node_info.hide{
    -webkit-transform: translateY(-100%);
       -moz-transform: translateY(-100%);
        -ms-transform: translateY(-100%);
         -o-transform: translateY(-100%);
            transform: translateY(-100%);
        }
        

        #chain_tree .node_info.show{
    -webkit-transform: translateY(0);
       -moz-transform: translateY(0);
        -ms-transform: translateY(0);
         -o-transform: translateY(0);
            transform: translateY(0);
        }
        

        #chain_tree .node_info .node_wrapper{
            padding: 20px;            
        }


        #chain_tree .node_info .node_img{
            max-width: 200px;
            border: 2px solid #ffffff;        
        }

        #chain_script .page_top .chain_status{
            float: right;
            color: #828282;
            width: 25%; 
        }

        #chain_script .chain_status{
            display:none;           
        }

        #chain_script .chain_status span#current_user,
        #chain_script .chain_status span#check_count
        {
            font-weight: bold;
        }

        #chain_script .chain-progress{
            margin-top: 20px;
            margin-bottom: 20px;
        }

        #chain_script .user_fields{

            -webkit-transition: all .25s linear;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;      

            border-width: 0px;
            background: transparent;
            width: 100%;             
            text-align: center;      
        }

        #chain_script span.input_wrapper{
            position: relative;
            width: 100%;                   
        }

        #chain_script span.input_wrapper:after{
            content: "";
            position: absolute;

            -webkit-transition: all .5s linear;
            -moz-transition: all .5s linear;
            -ms-transition: all .5s linear;
            -o-transition: all .5s linear;
            transition: all .5s linear;   

            height: 2px;
            background: #d7d8db;
            width: 10%;
            bottom: -5px;
            left: 50%;
            transform: translateX(-50%);                            
        }

        #chain_script span.input_wrapper.focus:after,
        #chain_script span.input_wrapper.warning:after,
        #chain_script span.input_wrapper.success:after{                
            width: 50%;  
        }

        #chain_script span.input_wrapper.warning:after{                
            background: red;
        }

        #chain_script span.input_wrapper.success:after{                
            background: blue;
        }

        #chain_script .users_inputs_wrapper span:first-child{
            text-align: left;
        }

        #chain_script .users_inputs_wrapper span:last-child{
            text-align: right;
        }

        #chain_script .open_block{
            cursor: pointer;
        }

        #chain_script .users_inputs_wrapper{
            margin-top: 20px;
            margin-bottom: 20px;
        }

        #chain_script .chain_container, #chain_script .users_inputs_wrapper{
            display: -ms-flexbox;
            display: -webkit-flex;
            display: flex;
            -webkit-flex-direction: row;
            -ms-flex-direction: row;
            flex-direction: row;
            -webkit-flex-wrap: nowrap;
            -ms-flex-wrap: nowrap;
            flex-wrap: nowrap;
            -webkit-justify-content: space-between;
            -ms-flex-pack: justify;
            justify-content: space-between;
            -webkit-align-content: center;
            -ms-flex-line-pack: center;
            align-content: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;            
        }

        #chain_script .chain_container{
            overflow-x:scroll;            
        }

        #chain_script .users_chain{
            position: relative;
            background: #edeef0;
            border-radius: 2px;
            box-shadow: 0 1px 0 0 #d7d8db, 0 0 0 1px #e3e4e8;
        }

        #chain_script .chain{
            position: relative;
            text-align: center;
            padding: 30px;
            z-index: 1;
            width: 50%;

            -webkit-transition: all 1s linear;
            -moz-transition: all 1s linear;
            -ms-transition: all 1s linear;
            -o-transition: all 1s linear;
            transition: all 1s linear;             
        }

        #chain_script .chain.added .link_to_user{
            opacity:0; 
        }

        #chain_script .users_chain.find .chain.added + .chain.added:before,
        #chain_script .users_chain.find .chain.first_user + .chain.added:before,
        #chain_script .users_chain.find .chain.added + .chain.second_user:before,
        #chain_script .users_chain.find .chain.first_user + .chain.second_user:before
        {
            content: "";
            height: 2px;
            top: 45%;
            transform: translateY(-45%);
            position: absolute;
            background: #fff;
            box-shadow: 0 1px 0 0 #d7d8db, 0 0 0 1px #e3e4e8;
            width: 100%;
            right: 50%;
            z-index: -1;
        }   

        #chain_script .chain.first_user
        {
            z-index: 20;
        }

        #chain_script .chain.second_user
        {
            z-index: 0;
        }

        #chain_script .link_to_user img.user_img{
            border-radius: 50%;
            max-width: 100px;
            height: 100px;

            -webkit-transition: all .25s linear;
               -moz-transition: all .25s linear;
                -ms-transition: all .25s linear;
                 -o-transition: all .25s linear;
                    transition: all .25s linear;                      
        }

        #chain_script .link_to_user .img_wrapper{
            position: relative;
            margin: 0px 30px 10px 30px;
            border-radius: 50%;
            overflow: hidden;
            border: 2px solid #ffffff;
            max-width: 100%;
            height: 100px;
            -webkit-transition: all .25s linear;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;
            display: inline-block;                     
        }

        #chain_script .link_to_user .img_wrapper .user_id{
            position: absolute;
            bottom: 0px;
            left: 50%;
            transform: translateX(-50%);
            opacity: 0;
            color: white;
            display: block;
            width: 100%;
            padding: 1px;
            -webkit-transition: all .25s linear ;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;                     
        }

        #chain_script .link_to_user .img_wrapper .user_id:hover{
            background: rgba(0,0,0,0.5);
        }

        #chain_script .link_to_user .img_wrapper:hover,
        #chain_script .link_to_user .img_wrapper:hover img.user_img
        {
            border-radius: 0%;           
        }

        #chain_script .link_to_user .img_wrapper:hover .user_id{
            opacity: 1;
        }

        #chain_script .link_to_user img.user_img[src=""] {
            content:url("/images/error404.png");
            background: white;
            max-width: 100px;
        }

        #chain_script .link_to_user .user_label{
            display: block;
        }
        

        #chain_script .chain_content{
            -webkit-transition: all .25s linear;
            -moz-transition: all .25s linear;
            -ms-transition: all .25s linear;
            -o-transition: all .25s linear;
            transition: all .25s linear;     

            display:none;     
        }

        `;
        var inline_style = document.createElement('style');
        inline_style.type = 'text/css';
        inline_style.appendChild(document.createTextNode(css));   
        head.appendChild(inline_style);        
    }      

    //JQuery загружен
    function ready() {

        //ID страницы текущего пользователя
        var my_id = vk.id;

        //Подгружаем дополнительные скрипты и стили
        addon(); 

        //Функция для вызова методов API VK
        function vkApi( method, params, callback) {
            var cb = 'cb_vkapi';
            $.ajax({
                url: 'https://api.vk.com/method/'+method,
                data: params,
                dataType: "jsonp",
                callback: cb,
                success: callback
            });
        }

        function validButton(){
            console.log(ids_valid);
            console.warn($('#first_user_id').val());
            console.log($('#second_user_id').val());
            if (($('#first_user_id').val() != '') && ($('#second_user_id').val() != '') && ids_valid.first && ids_valid.second && ( $('#first_user_id').val() != $('#second_user_id').val() )){
                $('#run_script').prop('disabled', false).removeClass('secondary');                
            } else {
                $('#run_script').prop('disabled', true).addClass('secondary');
                $('#show_tree').hide();
                $('#chain_tree').fadeOut();
            }    
        }

        function drawBlock(){
            $('#page_layout').prepend(`

                <div class="page_block chain_block" id="chain_script">
                <div class="page_info_wrap">
                <div class="page_top open_block">
                <div class="chain_status">Анализ страницы:<span id="current_user"></span></div>
                <div class="chain_status">Проверенно страниц:<span id="check_count"></span></div>
                <h2 class="page_name">Цепочка между пользователями</h2>
                <div class="page_current_info">Серега Мост</div>
                </div>

                    <div class="chain_content">

                        <div class="users_chain">
                            <div class="chain_container">
                                <div id="user_1" class="chain first_user">
                                    <a class="link_to_user" href="#" target="_blank">
                                    <div class="img_wrapper">
                                        <img class="user_img" src="" alt="">
                                        <span class="user_id"></span>
                                    </div>
                                    <span class="user_label">Не найдено</span>
                                    </a>
                                </div>

                                <div id="user_2" class="chain second_user">
                                    <a class="link_to_user" href="#" target="_blank">
                                    <div class="img_wrapper">
                                        <img class="user_img" src="" alt="">
                                        <span class="user_id"></span>
                                    </div>
                                    <span class="user_label">Не найдено</span>
                                    </a>
                                </div>
                            </div>
                        </div>

                           <div class="users_inputs_wrapper">
                                <span class="input_wrapper">
                                    <input type="text" class="user_fields" data-id="first" id="first_user_id" placeholder="Введите ID первого пользователя">
                                    <input type="hidden" class="hidden_user_fields" data-id="first">
                                </span>

                                <span class="input_wrapper">
                                    <input type="text" class="user_fields" data-id="second" id="second_user_id" placeholder="Введите ID второго пользователя">
                                    <input type="hidden" class="hidden_user_fields" data-id="second" >
                                </span>
                            </div>        

                            <div class="chain-progress chain_status">
                                <div id="progress_bar" class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 100%;">
                                    <span id="progress_text">Поиск</span>
                                </div>
                            </div>

                        <button class="flat_button button_wide secondary" id="run_script" disabled><i class="fa fa-users"></i> <b>Поиск связей</b></button>
                        <button class="flat_button button_wide" id="show_tree"><i class="fa fa-share-alt"></i> <b>Показать дерево</b></button>

                        </div>

                    </div>

                    <div id="chain_tree">

                    <div class="node_info hide">
                        <div class="node_wrapper">
                            <a class="node_img_url" href="#" target="_blank"><img class="node_img" src="" alt=""></a>
                            <div class="node_name"></div>
                            <a class="node_url" href="#" target="_blank"></a>
                        </div>
                        <button class="flat_button button_wide" id="hide_node"><i class="fa fa-eye"></i> <b>Скрыть</b></button>
                    </div>
                    <div id="tree-container"></div>
                    </div>
                </div>

            `);

            $('#first_user_id').val(vk.id);
            getUserInfo($('#first_user_id'));
        }

        function drawGraph(treeData){
console.clear();
console.log(JSON.stringify(treeData, null, 4));


    // Calculate total nodes, max label length
    var totalNodes = 0;
    var maxLabelLength = 0;
    // variables for drag/drop
    var selectedNode = null;
    var draggingNode = null;
    // panning variables
    var panSpeed = 200;
    var panBoundary = 20; // Within 20px from edges will pan when dragging.
    // Misc. variables
    var i = 0;
    var duration = 750;
    var root;

    // size of the diagram
    // var viewerWidth = $(document).width();
    // var viewerHeight = $(document).height();
    var viewerWidth = $('#chain_script').width();
    var viewerHeight = 700;

    var tree = d3.layout.tree()
        .size([viewerHeight, viewerWidth]);

    // define a d3 diagonal projection for use by the node paths later on.
    var diagonal = d3.svg.diagonal()
        .projection(function(d) {
            return [d.y, d.x];
        });

    // A recursive helper function for performing some setup by walking through all nodes

    function visit(parent, visitFn, childrenFn) {
        if (!parent) return;

        visitFn(parent);

        var children = childrenFn(parent);
        if (children) {
            var count = children.length;
            for (var i = 0; i < count; i++) {
                visit(children[i], visitFn, childrenFn);
            }
        }
    }

    // Call visit function to establish maxLabelLength
    visit(treeData, function(d) {
        totalNodes++;
        maxLabelLength = 50;
        // maxLabelLength = Math.max(d.name.length, maxLabelLength);

    }, function(d) {
        return d.children && d.children.length > 0 ? d.children : null;
    });


    // sort the tree according to the node names

    function sortTree() {
        tree.sort(function(a, b) {
            return 1;
            // return b.name.toLowerCase() < a.name.toLowerCase() ? 1 : -1;
        });
    }
    // Sort the tree initially incase the JSON isn't in a sorted order.
    sortTree();

    // TODO: Pan function, can be better implemented.

    function pan(domNode, direction) {
        var speed = panSpeed;
        if (panTimer) {
            clearTimeout(panTimer);
            translateCoords = d3.transform(svgGroup.attr("transform"));
            if (direction == 'left' || direction == 'right') {
                translateX = direction == 'left' ? translateCoords.translate[0] + speed : translateCoords.translate[0] - speed;
                translateY = translateCoords.translate[1];
            } else if (direction == 'up' || direction == 'down') {
                translateX = translateCoords.translate[0];
                translateY = direction == 'up' ? translateCoords.translate[1] + speed : translateCoords.translate[1] - speed;
            }
            scaleX = translateCoords.scale[0];
            scaleY = translateCoords.scale[1];
            scale = zoomListener.scale();
            svgGroup.transition().attr("transform", "translate(" + translateX + "," + translateY + ")scale(" + scale + ")");
            d3.select(domNode).select('g.node').attr("transform", "translate(" + translateX + "," + translateY + ")");
            zoomListener.scale(zoomListener.scale());
            zoomListener.translate([translateX, translateY]);
            panTimer = setTimeout(function() {
                pan(domNode, speed, direction);
            }, 50);
        }
    }

    // Define the zoom function for the zoomable tree

    function zoom() {
        svgGroup.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
    }


    // define the zoomListener which calls the zoom function on the "zoom" event constrained within the scaleExtents
    var zoomListener = d3.behavior.zoom().scaleExtent([0.1, 3]).on("zoom", zoom);

    function initiateDrag(d, domNode) {
        draggingNode = d;
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', 'none');
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle show');
        d3.select(domNode).attr('class', 'node activeDrag');

        svgGroup.selectAll("g.node").sort(function(a, b) { // select the parent and sort the path's
            if (a.id != draggingNode.id) return 1; // a is not the hovered element, send "a" to the back
            else return -1; // a is the hovered element, bring "a" to the front
        });
        // if nodes has children, remove the links and nodes
        if (nodes.length > 1) {
            // remove link paths
            links = tree.links(nodes);
            nodePaths = svgGroup.selectAll("path.link")
                .data(links, function(d) {
                    return d.target.id;
                }).remove();
            // remove child nodes
            nodesExit = svgGroup.selectAll("g.node")
                .data(nodes, function(d) {
                    return d.id;
                }).filter(function(d, i) {
                    if (d.id == draggingNode.id) {
                        return false;
                    }
                    return true;
                }).remove();
        }

        // remove parent link
        parentLink = tree.links(tree.nodes(draggingNode.parent));
        svgGroup.selectAll('path.link').filter(function(d, i) {
            if (d.target.id == draggingNode.id) {
                return true;
            }
            return false;
        }).remove();

        dragStarted = null;
    }

    // define the baseSvg, attaching a class for styling and the zoomListener
    var baseSvg = d3.select("#tree-container").append("svg")
        .attr("width", viewerWidth)
        .attr("height", viewerHeight)
        .attr("class", "overlay")
        .call(zoomListener);


    // Define the drag listeners for drag/drop behaviour of nodes.
    //Действие при перетаскивании узла
/*    dragListener = d3.behavior.drag()
        .on("dragstart", function(d) {
            if (d == root) {
                return;
            }
            dragStarted = true;
            nodes = tree.nodes(d);
            d3.event.sourceEvent.stopPropagation();
            // it's important that we suppress the mouseover event on the node being dragged. Otherwise it will absorb the mouseover event and the underlying node will not detect it d3.select(this).attr('pointer-events', 'none');
        })
        .on("drag", function(d) {
            if (d == root) {
                return;
            }
            if (dragStarted) {
                domNode = this;
                initiateDrag(d, domNode);
            }

            // get coords of mouseEvent relative to svg container to allow for panning
            relCoords = d3.mouse($('svg').get(0));
            if (relCoords[0] < panBoundary) {
                panTimer = true;
                pan(this, 'left');
            } else if (relCoords[0] > ($('svg').width() - panBoundary)) {

                panTimer = true;
                pan(this, 'right');
            } else if (relCoords[1] < panBoundary) {
                panTimer = true;
                pan(this, 'up');
            } else if (relCoords[1] > ($('svg').height() - panBoundary)) {
                panTimer = true;
                pan(this, 'down');
            } else {
                try {
                    clearTimeout(panTimer);
                } catch (e) {

                }
            }

            d.x0 += d3.event.dy;
            d.y0 += d3.event.dx;
            var node = d3.select(this);
            node.attr("transform", "translate(" + d.y0 + "," + d.x0 + ")");
            updateTempConnector();
        }).on("dragend", function(d) {
            if (d == root) {
                return;
            }
            domNode = this;
            if (selectedNode) {
                // now remove the element from the parent, and insert it into the new elements children
                var index = draggingNode.parent.children.indexOf(draggingNode);
                if (index > -1) {
                    draggingNode.parent.children.splice(index, 1);
                }
                if (typeof selectedNode.children !== 'undefined' || typeof selectedNode._children !== 'undefined') {
                    if (typeof selectedNode.children !== 'undefined') {
                        selectedNode.children.push(draggingNode);
                    } else {
                        selectedNode._children.push(draggingNode);
                    }
                } else {
                    selectedNode.children = [];
                    selectedNode.children.push(draggingNode);
                }
                // Make sure that the node being added to is expanded so user can see added node is correctly moved
                expand(selectedNode);
                sortTree();
                endDrag();
            } else {
                endDrag();
            }
        });*/

    function endDrag() {
        selectedNode = null;
        d3.selectAll('.ghostCircle').attr('class', 'ghostCircle');
        d3.select(domNode).attr('class', 'node');
        // now restore the mouseover event or we won't be able to drag a 2nd time
        d3.select(domNode).select('.ghostCircle').attr('pointer-events', '');
        updateTempConnector();
        if (draggingNode !== null) {
            update(root);
            centerNode(draggingNode);
            draggingNode = null;
        }
    }

    // Helper functions for collapsing and expanding nodes.

    function collapse(d) {
        if (d.children) {
            d._children = d.children;
            d._children.forEach(collapse);
            d.children = null;
        }
    }

    function expand(d) {
        if (d._children) {
            d.children = d._children;
            d.children.forEach(expand);
            d._children = null;
        }
    }

    var overCircle = function(d) {
        selectedNode = d;
        updateTempConnector();
    };
    var outCircle = function(d) {
        selectedNode = null;
        updateTempConnector();
    };

    // Function to update the temporary connector indicating dragging affiliation
    var updateTempConnector = function() {
        var data = [];
        if (draggingNode !== null && selectedNode !== null) {
            // have to flip the source coordinates since we did this for the existing connectors on the original tree
            data = [{
                source: {
                    x: selectedNode.y0,
                    y: selectedNode.x0
                },
                target: {
                    x: draggingNode.y0,
                    y: draggingNode.x0
                }
            }];
        }
        var link = svgGroup.selectAll(".templink").data(data);

        link.enter().append("path")
            .attr("class", "templink")
            .attr("d", d3.svg.diagonal())
            .attr('pointer-events', 'none');

        link.attr("d", d3.svg.diagonal());

        link.exit().remove();
    };

    // Function to center node when clicked/dropped so node doesn't get lost when collapsing/moving with large amount of children.

    function centerNode(source) {
        scale = zoomListener.scale();
        x = -source.y0;
        y = -source.x0;
        x = x * scale + viewerWidth / 2;
        y = y * scale + viewerHeight / 2;
        d3.select('g').transition()
            .duration(duration)
            .attr("transform", "translate(" + x + "," + y + ")scale(" + scale + ")");
        zoomListener.scale(scale);
        zoomListener.translate([x, y]);
    }

    // Toggle children function

    function toggleChildren(d) {
        if (d.children) {
            d._children = d.children;
            d.children = null;
        } else if (d._children) {
            d.children = d._children;
            d._children = null;
        }
        return d;
    }

    // Toggle children on click.
    //Раскрытие дочерних елементов при нажании и центрирование
    function click(d) {
        // if (d3.event.defaultPrevented) return; // click suppressed
        // d = toggleChildren(d);
        // update(d);
        // centerNode(d);
    }

    function update(source) {
        // Compute the new height, function counts total children of root node and sets tree height accordingly.
        // This prevents the layout looking squashed when new nodes are made visible or looking sparse when nodes are removed
        // This makes the layout more consistent.
        var levelWidth = [1];
        var childCount = function(level, n) {

            if (n.children && n.children.length > 0) {
                if (levelWidth.length <= level + 1) levelWidth.push(0);

                levelWidth[level + 1] += n.children.length;
                n.children.forEach(function(d) {
                    childCount(level + 1, d);
                });
            }
        };
        childCount(0, root);
        var newHeight = d3.max(levelWidth) * 200; // 25 pixels per line  
        tree = tree.size([newHeight, viewerWidth]);

        // Compute the new tree layout.
        var nodes = tree.nodes(root).reverse(),
            links = tree.links(nodes);

        // Set widths between levels based on maxLabelLength.
        nodes.forEach(function(d) {
            d.y = (d.depth * (maxLabelLength * 10)); //maxLabelLength * 10px
            // alternatively to keep a fixed scale one can set a fixed depth per level
            // Normalize for fixed-depth by commenting out below line
            // d.y = (d.depth * 500); //500px per level.
        });

        // Update the nodes…
        node = svgGroup.selectAll("g.node")
            .data(nodes, function(d) {
                return d.id || (d.id = ++i);
            });

        // Enter any new nodes at the parent's previous position.
        var nodeEnter = node.enter().append("g")
            // .call(dragListener)
            .attr("class", "node")
            .attr("transform", function(d) {
                return "translate(" + source.y0 + "," + source.x0 + ")";
            })
            .on('click', click);

        nodeEnter.append("circle")
            .attr('class', 'nodeCircle')
            .attr("r", 50)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";
            });

  // Append images
    var images = nodeEnter.append("image")
          .attr("xlink:href", function(d) { return d.img; })
          .attr("x", function(d) { return -25;})
          .attr("y", function(d) { return -25;})
          .attr("height", 50)
          .attr("width", 50);           
  
  // make the image grow a little on mouse over and add the text details on click
  var setEvents = images
          // Append hero text
          .on( 'click', function (d) {    
                
                $(".node_info").addClass('show');

                d3.select(".node_img").attr({
                    src: d.img,
                    alt: d.hero
                });

                d3.select(".node_name").html(d.name);

                d3.select(".node_img_url").attr({
                    href: d.link
                }); 

                d3.select(".node_url").attr({
                    href: d.link
                });       
    
                d3.select(".node_url").html('http://vk'+d.link);                          
           })

          .on( 'mouseenter', function() {
            // select element in current context
            d3.select( this )
              .transition()
              .attr("x", function(d) { return -35;})
              .attr("y", function(d) { return -35;})
              .attr("height", 70)
              .attr("width", 70);
          })
          // set back
          .on( 'mouseleave', function() {
            d3.select( this )
              .transition()
              .attr("x", function(d) { return -25;})
              .attr("y", function(d) { return -25;})
              .attr("height", 50)
              .attr("width", 50);
          });

        nodeEnter.append("text")
            .attr("x", function(d) {
                return d.children || d._children ? -10 : 10;
            })
            .attr("dy", ".35em")
            .attr('class', 'nodeText')
            .attr("text-anchor", function(d) {
                return d.children || d._children ? "end" : "start";
            })
            .text(function(d) {
                return d.name;
            })
            .style("fill-opacity", 0);

        // phantom node to give us mouseover in a radius around it
        nodeEnter.append("circle")
            .attr('class', 'ghostCircle')
            .attr("r", 30)
            .attr("opacity", 0.2) // change this to zero to hide the target area
        .style("fill", "red")
            .attr('pointer-events', 'mouseover')
            .on("mouseover", function(node) {
                overCircle(node);
            })
            .on("mouseout", function(node) {
                outCircle(node);
            });

        // Update the text to reflect whether node has children or not.
        node.select('text')
            .attr("x", function(d) {
                return d.children || d._children ? -40 : 40;
            })
            .attr("text-anchor", function(d) {
                return d.children || d._children ? "end" : "start";
            })
            .text(function(d) {
                return d.name;
            });

        // Change the circle fill depending on whether it has children and is collapsed
        node.select("circle.nodeCircle")
            .attr("r", 4.5)
            .style("fill", function(d) {
                return d._children ? "lightsteelblue" : "#fff";
            });

        // Transition nodes to their new position.
        var nodeUpdate = node.transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + d.y + "," + d.x + ")";
            });

        // Fade the text in
        nodeUpdate.select("text")
            .style("fill-opacity", 1);

        // Transition exiting nodes to the parent's new position.
        var nodeExit = node.exit().transition()
            .duration(duration)
            .attr("transform", function(d) {
                return "translate(" + source.y + "," + source.x + ")";
            })
            .remove();

        nodeExit.select("circle")
            .attr("r", 0);

        nodeExit.select("text")
            .style("fill-opacity", 0);

        // Update the links…
        var link = svgGroup.selectAll("path.link")
            .data(links, function(d) {
                return d.target.id;
            });

        // Enter any new links at the parent's previous position.
        link.enter().insert("path", "g")
            .attr("class", "link")
            .style("stroke", function(d) { return d.target.path; })
            .attr("d", function(d) {
                var o = {
                    x: source.x0,
                    y: source.y0
                };
                return diagonal({
                    source: o,
                    target: o
                });
            });

        // Transition links to their new position.
        link.transition()
            .duration(duration)
            .attr("d", diagonal);

        // Transition exiting nodes to the parent's new position.
        link.exit().transition()
            .duration(duration)
            .attr("d", function(d) {
                var o = {
                    x: source.x,
                    y: source.y
                };
                return diagonal({
                    source: o,
                    target: o
                });
            })
            .remove();

        // Stash the old positions for transition.
        nodes.forEach(function(d) {
            d.x0 = d.x;
            d.y0 = d.y;
        });
    }

    // Append a group which holds all nodes and which the zoom Listener can act upon.
    var svgGroup = baseSvg.append("g");

    // Define the root
    root = treeData;
    root.x0 = viewerHeight / 2;
    root.y0 = 0;

    // Layout the tree initially and center on the root node.
    update(root);
    centerNode(root);

        }

        function fillUserInfo(el,data){
            $(el).find('.link_to_user').attr({
                href: '/id'+data[0]
            });
            $(el).find('.img_wrapper .user_img').attr({
                src: data[3],
                alt: data[1]+' '+data[2]
            });
            $(el).find('.img_wrapper .user_id').text(data[0]);
            $(el).find('.user_label').text(data[1]+' '+data[2]);
        }

        function clearUserInfo(el){
            $(el).find('.link_to_user').attr({
                href: '#'
            });
            $(el).find('.img_wrapper .user_img').attr({
                src: '',
                alt: ''
            });
            $(el).find('.img_wrapper .user_id').text('');
            $(el).find('.user_label').text('Не найдено');
        }

        function getUserInfo(el){
            var field = $(el);
            var id = field.val();
            var info_block = $('.chain.'+field.data('id')+'_user');

                vkApi('users.get', {user_ids:id,fields:'photo_100',lang:'ru'}, function(data){  
                    if(data.response!=undefined && (typeof data.response[0].deactivated == "undefined") ){
                        var user_info = data.response[0];
                        var id = user_info.uid;
                        var name = user_info.first_name;
                        var last_name = user_info.last_name;
                        var img = user_info.photo_100;
                        var data = [id,name,last_name,img];

                        ids_valid[field.data('id')] = true;
                        field.parent().removeClass('warning').addClass('success');
                        fillUserInfo(info_block,data);
                        validButton();
                        field.siblings('.hidden_user_fields').val(id)
                    } else {
                        ids_valid[field.data('id')] = false;
                        field.parent().removeClass('success').addClass('warning');                    
                        clearUserInfo(info_block);
                        validButton();
                    }
                });    
        }

        function disableElements(){
            $('#run_script').prop('disabled', true).addClass('secondary');
            $('#show_tree').hide();
            $('#chain_tree').fadeOut();
            $('.user_fields').prop('disabled', true);
        }

        function enableElements(){
            $('#run_script').prop('disabled', false).removeClass('secondary');
            $('#show_tree').show();
            $('.user_fields').prop('disabled', false);
        }

        function checkUsers(){
            // console.clear();
            //Засикаем время начала работы скрипта
            start_time = new Date().getTime();
            //Массив цепочки
            chain_friends = [];
            //Уоличество друзей у пользователей
            var count_friends_user_1;
            var count_friends_user_2;

            vkApi('friends.get', {user_id:users.first}, function(data){
                //Получаем информацию о пользователе по странице
                friends_user_1 = data.response; 
                count_friends_user_1 = data.response.length;
                
                if (count_friends_user_1 == 0){
                    $('#progress_text').html('Невозможно построить цепочку [!] (У пользователя 1 нет друзей...)').parent().addClass('progress-bar-danger');
                    $('#check_count').html(' 0');
                    enableElements();            
                    return;
                }

                vkApi('friends.get', {user_id:users.second}, function(data){
                    //Получаем информацию о пользователе по странице
                    friends_user_2 = data.response; 
                    count_friends_user_2 = data.response.length;

                    if (count_friends_user_2 == 0){
                        $('#progress_text').html('Невозможно построить цепочку [!] (У пользователя 2 нет друзей...)').parent().addClass('progress-bar-danger');
                        $('#check_count').html(' 0');
                        enableElements();
                        return;
                    }

                    //Меняем пользователей местами (Начиная поиск с того у кого меньше друзей)
                    if (count_friends_user_1 > count_friends_user_2){
                        var buff = users.first;
                        users.first = users.second;
                        users.second = buff;
                        console.log('Swap users');
                        reverse = true;
                    }
                    //Добавляем второго пользователя в начало цепочки
                    chain_friends.push(users.second);
                    //Поиск между двумя пользователями
                    searchUser(users.first,users.second);            
                });
            });

        }

        //Поиск между двумя пользователями
        function searchUser(user1,user2){    
        //Очередь пользователей для проверки
        queue = new Array;
        //Проверенные пользователи
        checked = new Array;
        //Текущий пользователь
        current_user = user1;
        find = false;

                vkApi('friends.get', {user_id:user2}, function(data){
                    //Получаем информацию о пользователе по странице
                    secondUserFriends = data.response; 

                        //Если первый пользователь друг второго
                        if(secondUserFriends.indexOf(user1) != -1){
                            chain_friends.unshift(user1);
                            console.log('CHAIN DONE [SIMPLE]');
                            console.info(chain_friends);
                            //Показать цепочку друзей
                            createChain(chain_friends);
                        }else{                
                            //Если нужно проверять рекурсивно кажлого пользователя
                            userIntersection();
                        }                    
                });

                function userIntersection() {
                        var url = 'https://api.vk.com/method/friends.get?user_id='+current_user;
                        //Запрос (Получаем информацию о пользователях)
                        $.ajax({
                            url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                            type: 'GET',
                            dataType: 'jsonp',
                            crossDomain: true,
                            success: function(data){
                                console.log(current_user);
                                
                                //Если ошибка 
                                if (data.error && queue.length == 0){
                                    $('#progress_text').html('Невозможно построить цепочку [!] (У пользователя слишком мало друзей или они заблокированы)').parent().addClass('progress-bar-danger');
                                    enableElements();                                    
                                    return;
                                }

                                //Если страница не удаленна и не ошибка
                                if(data.response!=undefined && !data.error){
                                    for (var i = 0; i < data.response.length; i++) {
                                        if(checked.indexOf(data.response[i]) == -1 ){ //Если пользователь еше не проверялся
                                            //Выводить результаты перебора
                                            $('#current_user').html(' [ID:'+data.response[i]+']');
                                            $('#check_count').html(' '+(!find ? checked.length : checked_counter));

                                            if(secondUserFriends.indexOf(data.response[i]) !=-1 ){ //Если найдено пользователя                                                
                                                find = true;
                                                chain_friends.unshift(data.response[i]);
                                                chain_friends.unshift(current_user);
                                                console.warn('FOUND IN RECURSION [!]');
                                                i=data.response.count;
                                            } else{       
                                                queue.push(data.response[i]); //(Не найдено) Нужно проверить у друзей пользователя                                                
                                            }                
                                        }
                                    }
                                }
                                //Добавить пользователя в массив проверенных
                                checked.push(current_user);
                                console.log(checked);
                                
                                console.log(current_user);
                                current_user = queue.shift();                        
                                console.warn(current_user);                        
                
                                if ((queue.length == 0 && (typeof current_user == "undefined")) || find == true ) {
                                
                                    if(chain_friends[0] == user1){
                                        console.log('CHAIN DONE [RECURSION]');  
                                        console.info(chain_friends);
                                        //Показать цепочку друзей
                                        createChain();
                                    }else{                                                                    
                                        console.log('Compare two users');
                                        if (checked_counter == 0){
                                            checked_counter = checked.length;
                                        }
                                        searchUser(user1,chain_friends[0]);
                                    }
                                }else{
                                    //Рекурсивно вызываем саму себя
                                    userIntersection();
                                }
                            }
                        });
                }
        }

//Получаем друзей пользователей из цепочки
function createChain(){
    if (original_chain.length == 0){    
        //Скопировать массив в другой массив (создает новый обект а не ссылку, если нету циклических ссылок)
        original_chain = JSON.parse(JSON.stringify(chain_friends));
        console.log(JSON.stringify(chain_friends, null, 4));    
    }

    //Получаем следующего пользователя для проверки
    var current_id = chain_friends.shift();
    //Количество друзей
    var count_users = 20;    

    vkApi('friends.get', {user_id:current_id}, function(data){
        //Временый массив друзей текущего пользователя
        var current_user_friends = new Array;
        unique_id.push(current_id);
        //Если нету ошибки
        if(data.response!=undefined && !data.error){                        
            for (var i = 0; i < count_users; i++) {
                //Если человека нету в списке проверенных
                if(unique_id.indexOf(data.response[i]) == -1 ){
                    if (data.response[i] != chain_friends[0]){
                        var each_friend_obj = new Object;
                        each_friend_obj.id = data.response[i].toString();
                        current_user_friends.push(each_friend_obj);   
                        unique_id.push(data.response[i]);         
                    }
                } else {
                    count_users++;
                }            
            }
        }
        //Добавляем в конец проверки следующего пользователя
        if (chain_friends.length != 0){
    
            var next_id = new Object;
            next_id.id = chain_friends[0].toString();
            current_user_friends.push(next_id); 
            // unique_id.push(chain_friends[0]);  
        }
        
        //Если массив обектов пустой
        if (objectCount(tree) == 0){
    
            tree.id = current_id.toString();
            tree.children = current_user_friends;
        } else {
            //Если нет то обходим рекурсивно многомерный обект
            var iterator = new objectRecursive(tree, 0, true, 100);

            for(var item = iterator.next(); !item.done; item = iterator.next()) {
                var obj = item.value;
                if (obj.node == current_id.toString()){
                    obj.parent.children = current_user_friends;
                }    
            }

        }
        //Если массив для проверки пустой
        if (chain_friends.length == 0){
            console.clear();
            console.log(JSON.stringify(tree, null, 4));
            
            createChainWay();
            // getDataChain();
        } else {
            //Рекурсивно вызываем саму себя
            createChain();
        }
    });
}

//Формируем пути к пользователям
function createChainWay(){
    var iterator = new objectRecursive(tree, 0, true, 100);

    for(var item = iterator.next(); !item.done; item = iterator.next()) {
        var obj = item.value;                          

        if (obj.key == 'id'){
            tree_way[obj.node] = obj.path.slice(0,(obj.path.length-1)).join('.');
        } 
    }
    console.warn('WAY:');
    console.log(JSON.stringify(tree_way, null, 4));

    getDataChain();
}

//Получаем данные о каждом человеке
function getDataChain(){
    var ids_str = unique_id.join(',');

    vkApi('users.get', {user_ids:ids_str,fields:'photo_100,photo_max',lang:'ru'}, function(data){              
        if(data.response!=undefined && (typeof data.response[0].deactivated == "undefined") ){
            console.warn(JSON.stringify(data.response, null, 4));
            for (var i = 0; i < data.response.length; i++) {                                
                var id = data.response[i].uid;
                var dot = '.';
                var path = tree_way[id];
                // 
                if (path == ''){
                  dot = '';
                    
                } 
                    objectPath.set(tree, path+dot+'hero', data.response[i].first_name); 
                    objectPath.set(tree, path+dot+'name', data.response[i].first_name + ' ' + data.response[i].last_name); 
                    objectPath.set(tree, path+dot+'link', '/id' + data.response[i].uid); 
                    objectPath.set(tree, path+dot+'img', data.response[i].photo_max); 
                    objectPath.set(tree, path+dot+'size', 40000);     
                    
                    if (original_chain.indexOf(id) == -1 ){
                        objectPath.set(tree, path+dot+'path', 'white'); 
                    } else {
                        objectPath.set(tree, path+dot+'path', 'red'); 
                    }                
            }

            // console.clear();
            console.warn(JSON.stringify(tree, null, 4));
            console.info('all done dave fun');

            showChain();
            $('#tree-container').html('');
            drawGraph(tree);
        }
    });    
}


        function showChain(){
            new_search = true;
            $('#show_tree').fadeIn();            
            $('.users_chain').addClass('find');
            $('#check_count').html(' '+checked_counter);
            enableElements();
            end_time = new Date().getTime() - start_time;
            $('#progress_text').html('Поиск завершен (Время: <b>'+Math.floor(end_time/1000)+' секунд</b>)').parent().removeClass('progress-bar-danger').addClass('progress-bar-success');

            if (reverse) {
                original_chain = original_chain.reverse();
            } 

            //Удаляем из цепочки искомых пользователей
            for (var i = 0; i < original_chain.length; i++) {
                if (original_chain[i] == users.first || original_chain[i] == users.second){
                    delete original_chain[i];
                }
            };

            //Конкатинировал строчку
            var str_users_ids = original_chain.join(',');
            
                vkApi('users.get', {user_ids:str_users_ids,fields:'photo_100',lang:'ru'}, function(data){
                    if(data.response!=undefined && (typeof data.response[0].deactivated == "undefined") ){
                        for (var i = 0; i < data.response.length; i++) {
                            var z_index = data.response.length - i;

                            var user_info = data.response[i];
                            var id = user_info.uid;
                            var name = user_info.first_name;
                            var last_name = user_info.last_name;
                            var img = user_info.photo_100;
                            
                            var html = 
                            `
                                <div class="chain added" style="z-index: `+z_index+`;">
                                    <a class="link_to_user" href="/id`+id+`" target="_blank">
                                    <div class="img_wrapper">
                                        <img class="user_img" src="`+img+`" alt="`+name+` `+last_name+`">
                                        <span class="user_id">`+id+`</span>
                                    </div>
                                    <span class="user_label">`+name+` `+last_name+`</span>
                                    </a>
                                </div>

                            `;
                            jQuery('.users_chain .chain').last().before(html);
                        }
                    }

                    //Плавно показать блоки после формирования
                    jQuery('.users_chain .chain.added .link_to_user').animate({
                        opacity: 1,
                    }, 2000, function() {
                        // Animation complete.
                    });
                }); 
        }
        //SEND
        //Функция для отправки сообщений всем друзям


        //Получаем все сообщения с сервера
        function getMessages(data,key){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }

            //Пожалуйста подождите
            $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Загрузка <b>сообщений</b>...</div>
                    </div>
                      </div>
                    `
            );            

            var users = new Array();
            var msg_data = new Array();
            
            //Если нужно вернуть ключи
            if (key) {
                for( i=1; i<data.response.length;i++) {
                    if (/\[(.+)\]/.test(data.response[i].body)) {
                        //Формируем массив ключей
                        msg_data.push(data.response[i]);
                    }
                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                } 
            } else {
            //Если нужно показать все сообщения
                for( i=1; i<data.response.length;i++) {
                    //Формируем массив сообщений
                    msg_data.push(data.response[i]);

                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                }                 
            }

            var flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            // console.clear();
            var user_data = new Array();
            for (key in users) {
                
                //Исправление ошибки (встречаються недопустимые символы - в ID)
                var fixed_key = users[key]+'';
                fixed_key = Number(fixed_key.match(/\d+/));
                // var fixed_key = parseInt(value.replace(/\D+/g,""));
 
                //Получаем информацию о пользователях
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+fixed_key+'&fields=photo_100,online&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        // console.warn(data);
                        var obj = data.response[0];
                        // console.log(user_data);
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 

        //===============/МЕНЮ СЛЕВА===============
        //Кнопка включения скрипта
        // $('#side_bar').prepend('<button class="flat_button fl_l view_other" style="width:100%; margin-top:50px;" id="activate_script"><i class="fa fa-power-off"></i> <b>Активировать</b></button><p>Script by Serega MoST</p>');
        // document.getElementById('activate_script').onclick = function() {
        //     activate();
        // }; 

//Запуск скрипта
activate = function() {
    //Очищаем все обработчики событий с document (для многократного вызова функции)
    $(document).off();
        //Проверка на главную страницу
        if (/(^https:\/\/)?vk\.com\/(?!friends|albums|video|im|groups|feed|settings|apps)[a-zA-Z][a-zA-Z_\.0-9]+[a-zA-Z0-9]$/.test(document.location.href)) {
            console.warn('Main Page');

            drawBlock();
            // drawGraph();
            
            //===============ОБРАБОТЧИКИ СОБЫТИЙ===============
            $(document).on('click', '.open_block', function(event) {
                $('#chain_script .chain_content ').toggle('fast');
            }); 
            
           $(document).on('keyup', '.user_fields', function(event) {
                if ($(this).val() != ''){
                    if (new_search){
                        jQuery('.users_chain .chain.added .link_to_user').animate({
                            opacity: 0,
                        }, 1000, function() {
                            $('.users_chain').removeClass('find');
                            $('.chain_status').hide();                
                            jQuery('.users_chain .chain.added').remove();        
                        });          
                        new_search = false;
                    }
                    
                    getUserInfo($(this));
                } else {
                    clearUserInfo($('.chain.'+$(this).data('id')+'_user'));
                    $(this).parent().removeClass('success').removeClass('warning');
                    validButton();
                }
            });  

           $(document).on('click', '#run_script', function(event) {
                $('#progress_text').html('Поиск').parent().removeClass('progress-bar-success').removeClass('progress-bar-danger');
                disableElements();


                $('.chain_status').show();

                $('.hidden_user_fields').each(function(index, el) {
                    var id = $(el).val();
                    users[$(el).data('id')] = parseInt(id);
                });

                //Копируем обект
                // users = Object.assign({}, info_users);


                original_chain = [];
                checkUsers();
            }); 

           $(document).on('click', '#show_tree', function(event) {
                $('#chain_tree').fadeToggle();
            }); 

           $(document).on('click', '#hide_node', function(event) {
                $('.node_info').removeClass('show').addClass('hide');
            }); 

            $(".user_fields").focus(function(){
                $(this).parent().addClass('focus');
            });

            $(".user_fields").blur(function(){
                $(this).parent().removeClass('focus');
            });         
            


        }    
        
    }//Activate
    // activate();
    }//Ready







/*        function sendMessages() {
            //Если значение щетчика еше меньше длины массива
            if (counter_send < send_arr.length){

                
                var url = 'https://api.vk.com/method/messages.send?user_id='+send_arr[counter_send]+'&message='+all_msg_text+'&access_token='+search_token;

                //Запрос (Отправляем сообщение всем друзям)
                $.ajax({
                    url: encodeURI(url), //Исправление проблем с кодировкой, путем кодирования URL строки
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    success: function(data){
                        console.warn(data); //ID
                        console.log(counter_send); //Щечик

                        //Если ошибка
                        if (data.error){
                            //6:Too many requests per second
                            if (data.error.error_code == 6){
                                //Ставим задержку на вызов функции в 1 секунды
                                setTimeout(sendMessages,1000);
                            }

                            //14:Captcha needed
                            if (data.error.error_code == 14){
                                //Действие которое выполняеться
                                $('#captcha-action').val('send');
                                //Картинка каптчи
                                var img = data.error.captcha_img;
                                $('#captcha-img').attr('src', img);
                                //Идентификатор каптчи
                                captcha_sid = data.error.captcha_sid;
                                //Показать блок каптчи
                                $('#captcha').show();
                                //Ставим фокус на поле ввода
                                $('#captcha-text').focus();

                                //Меняем прогресс-бар (Пауза)
                                var percents= (Math.ceil(100/(send_arr.length/counter_send))/2)+'%';                                     
                                $('#progress_bar').width(percents);
                                $('#progress_text').html('<b>'+percents+'</b> Пауза [<b>'+counter_send+'</b>/'+send_arr.length+']');
                                //Меняем стиль прогресс бара   
                                $('#progress_bar').addClass('progress-bar-warning');
                            }                                        
                        } else { //Если все нормально
                            console.log(data.response);

                            //Меняем прогресс-бар
                            var percents= (Math.ceil(100/(send_arr.length/counter_send))/2)+'%';                                         
                            $('#progress_bar').width(percents);
                            $('#progress_text').html('<b>'+percents+'</b> Выполненно [<b>'+counter_send+'</b>/'+send_arr.length+']'); 

                            //Инкреминтируем значение в случае удачного запроса
                            counter_send++;

                            //Рекурсивно вызываем саму себя
                            sendMessages();
                        }//ERROR
                    }//SUCCESS
                });
            } else {
                // Сообщение отправлено
                $("#view_message_info").slideDown(300).delay(5000).slideUp(300);
                //Очищаем поле
                $('#other_message_text').text('');
                //Удаляем блок загрузки
                $('#loader').remove();
            } 
        }
*/

/*

        function getMessages(data,key){
            var html, i, msg;
            console.warn(data);

            //В случае ошибки
            if( !data || !data.response) {
                console.log('VK error:', data); 
                $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Не найдено человека по введенному <b>коду доступа</b> [!].</div>
                    <div class="im_typing_wrap"><div class="im_typing" id="im_typing16772416" style="opacity: 0; display: none;"></div><div id="im_lastact16772416" class="im_lastact"></div></div>
                    <div class="im_error"></div>
                    </div>
                      </div>
                    `
                );
                return;
            }

            //Пожалуйста подождите
            $('#im_rows_wrap').html(
                    `
                    <div id="im_rows" style="height: 400px;">
                    <div class="im_rows im_peer_rows" style="">
                    <div class="im_none" style="display: block;">Загрузка <b>сообщений</b>...</div>
                    </div>
                      </div>
                    `
            );            

            var users = new Array();
            var msg_data = new Array();
            
            //Если нужно вернуть ключи
            if (key) {
                for( i=1; i<data.response.length;i++) {
                    if (/\[(.+)\]/.test(data.response[i].body)) {
                        //Формируем массив ключей
                        msg_data.push(data.response[i]);
                    }
                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                } 
            } else {
            //Если нужно показать все сообщения
                for( i=1; i<data.response.length;i++) {
                    //Формируем массив сообщений
                    msg_data.push(data.response[i]);

                    //Получение ID пользователей
                    var id = 'id'+data.response[i].uid;
                    if(!(id in users)){users[id]=[id];}
                }                 
            }

            var flags = new Object();
            //Копируем обект users в flags
            for (var key in users) {
                flags[key] = users[key];
            }

            //Заполняем массив(обект) с флагами
            for (var key in flags) {
                flags[key] = false;
            }

            // console.clear();
            var user_data = new Array();
            for (key in users) {
                
                //Исправление ошибки (встречаються недопустимые символы - в ID)
                var fixed_key = users[key]+'';
                fixed_key = Number(fixed_key.match(/\d+/));
                // var fixed_key = parseInt(value.replace(/\D+/g,""));
 
                //Получаем информацию о пользователях
                $.ajax({
                    url: 'https://api.vk.com/method/users.get?user_ids='+fixed_key+'&fields=photo_100,online&lang=ru',
                    type: 'GET',
                    dataType: 'jsonp',
                    crossDomain: true,
                    //Передаем переменую key для использования в методе "success"
                    flag: key,
                    success: function(data){
                        // console.warn(data);
                        var obj = data.response[0];
                        // console.log(user_data);
                        user_data.push(obj);
                        //Использование переменной key (this.flag)
                        flags[this.flag] = true;
                        //Ставим флаг в true
                    }
                });
            }   

            //Таймер
            var interval = setInterval(function() {

                //Проверка значения в массиве (Ждем пока все флаги не будут true)
                for (var key in flags)
                {
                    if(flags[key] == false) return;
                }

                //Очистить консоль
                //clear();
                //Склеиваем массивы (сообщений и информации о пользователях) 
                for( i=0; i<msg_data.length;i++) { //Массив сообщений
                    user_data_keys = Object.keys(user_data);
                    for( j=0; j<user_data_keys.length;j++) { //массив информации о uID
                        key = user_data_keys[j];
                        if(msg_data[i].uid == user_data[key].uid){
                            msg_data[i].uid=user_data[key];
                        }
                    }        
                }
                //Ввыводим сообщения
                showMessages(msg_data);

                //Отключить таймер
                clearInterval(interval);
                 
            },300);

        }                 


*/

// Тестовая страница (Мало друзей)
// https://vk.com/id75847586
//75847586
//89798788
//43243893
//43278687      -   Очень долгий поиск

//Подсчёт количества елементов в обекте
function objectCount(obj) {
    var count = 0; 
    for(var prs in obj) 
    { 
        if(obj.hasOwnProperty(prs)) count++;
    } 
    return count; 
}

//Рекурсивный обход многомерного объекта (https://github.com/nervgh/recursive-iterator)
var $jscomp={scope:{}};$jscomp.defineProperty="function"==typeof Object.defineProperties?Object.defineProperty:function(a,b,c){if(c.get||c.set)throw new TypeError("ES3 does not support getters and setters.");a!=Array.prototype&&a!=Object.prototype&&(a[b]=c.value)};$jscomp.getGlobal=function(a){return"undefined"!=typeof window&&window===a?a:"undefined"!=typeof global?global:a};$jscomp.global=$jscomp.getGlobal(this);$jscomp.SYMBOL_PREFIX="jscomp_symbol_";
$jscomp.initSymbol=function(){$jscomp.initSymbol=function(){};$jscomp.global.Symbol||($jscomp.global.Symbol=$jscomp.Symbol)};$jscomp.symbolCounter_=0;$jscomp.Symbol=function(a){return $jscomp.SYMBOL_PREFIX+(a||"")+$jscomp.symbolCounter_++};
$jscomp.initSymbolIterator=function(){$jscomp.initSymbol();var a=$jscomp.global.Symbol.iterator;a||(a=$jscomp.global.Symbol.iterator=$jscomp.global.Symbol("iterator"));"function"!=typeof Array.prototype[a]&&$jscomp.defineProperty(Array.prototype,a,{configurable:!0,writable:!0,value:function(){return $jscomp.arrayIterator(this)}});$jscomp.initSymbolIterator=function(){}};$jscomp.arrayIterator=function(a){var b=0;return $jscomp.iteratorPrototype(function(){return b<a.length?{done:!1,value:a[b++]}:{done:!0}})};
$jscomp.iteratorPrototype=function(a){$jscomp.initSymbolIterator();a={next:a};a[$jscomp.global.Symbol.iterator]=function(){return this};return a};
(function(a,b){"object"===typeof exports&&"object"===typeof module?module.exports=b():"function"===typeof define&&define.amd?define([],b):"object"===typeof exports?exports.objectRecursive=b():a.objectRecursive=b()})(this,function(){return function(a){function b(e){if(c[e])return c[e].exports;var f=c[e]={exports:{},id:e,loaded:!1};a[e].call(f.exports,f,f.exports,b);f.loaded=!0;return f.exports}var c={};b.m=a;b.c=c;b.p="";return b(0)}([function(a,b,c){var e=function(a){if(Array.isArray(a)){for(var g=
0,b=Array(a.length);g<a.length;g++)b[g]=a[g];return b}return Array.from(a)},f=function(a,g,b){g&&Object.defineProperties(a,g);b&&Object.defineProperties(a.prototype,b)};b=c(1);var l=b.isObject,d=b.getKeys,h={};b=function(){function a(b,d,c,k){d=void 0===d?0:d;c=void 0===c?!1:c;k=void 0===k?100:k;if(!(this instanceof a))throw new TypeError("Cannot call a class as a function");this.__bypassMode=d;this.__ignoreCircular=c;this.__maxDeep=k;this.__cache=[];this.__queue=[];this.__state=this.getState(void 0,
b);this.__makeIterable()}f(a,null,{next:{value:function(){var a=this.__state||h,b=a.node,d=a.path,a=a.deep;if(this.__maxDeep>a&&this.isNode(b))if(this.isCircular(b)){if(!this.__ignoreCircular)throw Error("Circular reference");}else if(this.onStepInto(this.__state)){var c,d=this.getStatesOfChildNodes(b,d,a),a=this.__bypassMode?"push":"unshift";(c=this.__queue)[a].apply(c,e(d));this.__cache.push(b)}b=this.__queue.shift();c=!b;this.__state=b;c&&this.destroy();return{value:b,done:c}},writable:!0,configurable:!0},
destroy:{value:function(){this.__queue.length=0;this.__cache.length=0;this.__state=null},writable:!0,configurable:!0},isNode:{value:function(a){return l(a)},writable:!0,configurable:!0},isLeaf:{value:function(a){return!this.isNode(a)},writable:!0,configurable:!0},isCircular:{value:function(a){return-1!==this.__cache.indexOf(a)},writable:!0,configurable:!0},getStatesOfChildNodes:{value:function(a,b,c){var h=this;return d(a).map(function(d){return h.getState(a,a[d],d,b.concat(d),c+1)})},writable:!0,
configurable:!0},getState:{value:function(a,b,d,c,h){return{parent:a,node:b,key:d,path:void 0===c?[]:c,deep:void 0===h?0:h}},writable:!0,configurable:!0},onStepInto:{value:function(a){return!0},writable:!0,configurable:!0},__makeIterable:{value:function(){var a=this;try{$jscomp.initSymbol(),$jscomp.initSymbolIterator(),this[Symbol.iterator]=function(){return a}}catch(b){}},writable:!0,configurable:!0}});return a}();a.exports=b},function(a,b){function c(a){return null!==a&&"object"===typeof a}function e(a){if(!(c(a)&&
"length"in a))return!1;var b=a.length;if(!f(b))return!1;if(0<b)return b-1 in a;for(var e in a)return!1}function f(a){return"number"===typeof a}b.isObject=c;b.isArrayLike=e;b.isNumber=f;b.getKeys=function(a){var b=Object.keys(a);l(a)||(e(a)?(a=b.indexOf("length"),-1<a&&b.splice(a,1)):b=b.sort());return b};var l=b.isArray=Array.isArray;Object.defineProperty(b,"__esModule",{value:!0})}])});

//Получения значения объекта через строку "путь", 'foo.bar.object.a.serega' (https://github.com/mariocasciaro/object-path)
(function(f,k){"object"===typeof module&&"object"===typeof module.exports?module.exports=k():"function"===typeof define&&define.amd?define([],k):f.objectPath=k()})(this,function(){function f(d,g){return null==d?!1:Object.prototype.hasOwnProperty.call(d,g)}function k(d){if(!d||l(d)&&0===d.length)return!0;if("string"!==typeof d){for(var g in d)if(f(d,g))return!1;return!0}return!1}function m(d){var g=parseInt(d);return g.toString()===d?g:d}function p(d){function g(b,a){if(d.includeInheritedProps||"number"===
typeof a&&Array.isArray(b)||f(b,a))return b[a]}function n(b,a,c,e){"number"===typeof a&&(a=[a]);if(!a||0===a.length)return b;if("string"===typeof a)return n(b,a.split(".").map(m),c,e);var d=a[0],f=g(b,d);if(1===a.length)return void 0!==f&&e||(b[d]=c),f;void 0===f&&(b[d]="number"===typeof a[1]?[]:{});return n(b[d],a.slice(1),c,e)}d=d||{};var e=function(b){return Object.keys(e).reduce(function(a,c){if("create"===c)return a;"function"===typeof e[c]&&(a[c]=e[c].bind(e,b));return a},{})};e.has=function(b,
a){"number"===typeof a?a=[a]:"string"===typeof a&&(a=a.split("."));if(!a||0===a.length)return!!b;for(var c=0;c<a.length;c++){var e=m(a[c]);if("number"===typeof e&&l(b)&&e<b.length||(d.includeInheritedProps?e in Object(b):f(b,e)))b=b[e];else return!1}return!0};e.ensureExists=function(b,a,c){return n(b,a,c,!0)};e.set=function(b,a,c,e){return n(b,a,c,e)};e.insert=function(b,a,c,d){var h=e.get(b,a);d=~~d;l(h)||(h=[],e.set(b,a,h));h.splice(d,0,c)};e.empty=function(b,a){if(!k(a)&&null!=b){var c,d;if(c=
e.get(b,a)){if("string"===typeof c)return e.set(b,a,"");if("boolean"===typeof c||"[object Boolean]"===q.call(c))return e.set(b,a,!1);if("number"===typeof c)return e.set(b,a,0);if(l(c))c.length=0;else if("object"===typeof c&&"[object Object]"===q.call(c))for(d in c)f(c,d)&&delete c[d];else return e.set(b,a,null)}}};e.push=function(b,a){var c=e.get(b,a);l(c)||(c=[],e.set(b,a,c));c.push.apply(c,Array.prototype.slice.call(arguments,2))};e.coalesce=function(b,a,c){for(var d,h=0,f=a.length;h<f;h++)if(void 0!==
(d=e.get(b,a[h])))return d;return c};e.get=function(b,a,c){"number"===typeof a&&(a=[a]);if(!a||0===a.length)return b;if(null==b)return c;if("string"===typeof a)return e.get(b,a.split("."),c);var d=m(a[0]),f=g(b,d);return void 0===f?c:1===a.length?f:e.get(b[d],a.slice(1),c)};e.del=function(b,a){"number"===typeof a&&(a=[a]);if(null==b||k(a))return b;if("string"===typeof a)return e.del(b,a.split("."));var c=m(a[0]),d=g(b,c);if(null==d)return d;if(1===a.length)l(b)?b.splice(c,1):delete b[c];else if(void 0!==
b[c])return e.del(b[c],a.slice(1));return b};return e}var q=Object.prototype.toString,l=Array.isArray||function(d){return"[object Array]"===q.call(d)},r=p();r.create=p;r.withInheritedProps=p({includeInheritedProps:!0});return r});