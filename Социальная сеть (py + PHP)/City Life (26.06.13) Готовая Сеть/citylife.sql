-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 27 2013 г., 13:28
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `citylife`
--

-- --------------------------------------------------------

--
-- Структура таблицы `fileshare`
--

CREATE TABLE IF NOT EXISTS `fileshare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `file` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user1` int(6) NOT NULL,
  `user2` int(6) NOT NULL,
  `whois` varchar(16) COLLATE utf8_bin NOT NULL,
  `yes` varchar(3) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=29 ;

--
-- Дамп данных таблицы `friends`
--

INSERT INTO `friends` (`id`, `user1`, `user2`, `whois`, `yes`) VALUES
(5, 2, 4, 'одногрупник', 'yes'),
(17, 1, 27, '', 'yes'),
(18, 3, 2, '', 'yes'),
(20, 3, 4, '', ''),
(24, 2, 25, '', ''),
(25, 2, 25, '', ''),
(26, 2, 22, '', ''),
(27, 2, 22, '', ''),
(28, 1, 2, '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `from_full_name` varchar(50) COLLATE utf8_bin NOT NULL,
  `image` varchar(60) COLLATE utf8_bin NOT NULL,
  `user1` int(6) NOT NULL,
  `user2` int(6) NOT NULL,
  `datetime` varchar(50) COLLATE utf8_bin NOT NULL,
  `text` varchar(300) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=14 ;

--
-- Дамп данных таблицы `messages`
--

INSERT INTO `messages` (`id`, `from_full_name`, `image`, `user1`, `user2`, `datetime`, `text`) VALUES
(13, 'Slava Osadchy', '/users/1/Avatar.jpg', 1, 2, '16:47:8 (26/6/2013)', 'Hello');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `mail` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `parol` varchar(16) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `name` varchar(16) NOT NULL,
  `l_name` varchar(16) NOT NULL,
  `date` date NOT NULL,
  `stat` varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `info` varchar(300) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `sex` char(1) NOT NULL,
  `city` varchar(16) NOT NULL,
  `photo` varchar(20) NOT NULL,
  `full_name` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mail` (`mail`,`parol`)
) ENGINE=InnoDB  DEFAULT CHARSET=cp1251 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `mail`, `parol`, `name`, `l_name`, `date`, `stat`, `info`, `sex`, `city`, `photo`, `full_name`) VALUES
(1, 'slava-osadchiy@mail.ru', 'Kyrosyn', 'Slava', 'Osadchy', '1993-10-12', '0000', '***********************', 'М', 'Николаев', '/users/1/Avatar.jpg', 'Slava Osadchy'),
(2, 'serega@mail.ru', 'Kyrosyn', 'Serega', 'Patsura', '1993-10-13', '0000', '+++++++++++++++++++', 'М', 'Николаев', '/users/2/Avatar.jpg', 'Serega Patsura'),
(3, 'max@mail.ru', 'Kyrosyn', 'Max', 'Trifonov', '1993-10-14', '0000', '((((((((((((((((((((((((((((((((((', 'М', 'Николаев', '/users/3/Avatar.jpg', 'Max Trifonov'),
(4, 'dima@mail.ru', 'Kyrosyn', 'Dima', 'Smykov', '1993-10-15', '0000', '%%%%%%%%%%%%%%%%%%%%%', 'М', 'Николаев', '/users/4/Avatar.jpg', 'Dima Smykov'),
(21, 'dimas@mail.ru', '111111', 'dima', 'terentiev', '1993-10-12', NULL, '111111111111', '?', 'Nikolayev', '', 'dima terentiev'),
(22, 'sergey@mail.ru', '222222', 'sergey', 'bershadskiy', '1993-10-12', NULL, '111111111111', '?', 'Nikolayev', '', 'sergey bershadskiy'),
(25, 'serg@mail.ru', '22222222', 'sereja', 'dobrojan', '1992-12-25', NULL, '2222222222222', 'М', 'Nikolayev', '', 'sereja dobrojan'),
(27, 'ser@mail.ru', '22222222', 'sereja', 'dobrojan', '1992-12-25', NULL, '2222222222222', 'М', 'Nikolayev', '', 'sereja dobrojan');

-- --------------------------------------------------------

--
-- Структура таблицы `wall`
--

CREATE TABLE IF NOT EXISTS `wall` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `from_full_name` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `user1` int(6) NOT NULL,
  `user2` int(6) NOT NULL,
  `text` varchar(100) NOT NULL,
  `datetime` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

--
-- Дамп данных таблицы `wall`
--

INSERT INTO `wall` (`id`, `from_full_name`, `image`, `user1`, `user2`, `text`, `datetime`) VALUES
(39, 'Serega Patsura', '/users/2/Avatar.jpg', 2, 2, 'Привет', '10:49:37 (26/6/2013)'),
(40, 'Serega Patsura', '/users/2/Avatar.jpg', 2, 2, 'Хелоу', '10:49:45 (26/6/2013)'),
(41, 'Serega Patsura', '/users/2/Avatar.jpg', 2, 4, '123', '10:50:4 (26/6/2013)'),
(42, 'Serega Patsura', '/users/2/Avatar.jpg', 2, 2, 'Привет Мир', '12:6:30 (26/6/2013)'),
(43, 'Serega Patsura', '/users/2/Avatar.jpg', 2, 22, 'Привет Серега', '12:6:52 (26/6/2013)'),
(44, 'Serega Patsura', '/users/2/Avatar.jpg', 2, 4, '123f', '13:24:38 (27/6/2013)');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
