#!/usr/bin/python
import os
import re
import _mysql
def get_uid():
	if 'HTTP_COOKIE' in os.environ:
		cookies = os.environ['HTTP_COOKIE']
		exp=re.compile("user-id=\d{1,6}$|user-id=\d{1,6};")
		res=exp.findall(cookies)
		res=res[0]
		exp=re.compile("[0-9]{1,6}")
		res=exp.findall(res)
		uid=res[0]
		return uid
def who_is(page_id):
	if get_uid()==page_id:
		return "user"
	else:
		return "guest"
def add_friend(uid):
	db=_mysql.connect(host="localhost",user="root",passwd="",db="citylife")
	db.query("INSERT INTO friends (user1, user2) VALUES (\"%s\", \"%s\")"%(get_uid(),uid))