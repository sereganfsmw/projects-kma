"use strict";
/**********************************
************ CWS LIBRARY **********

**********************************/
console.log('000: script.js');

//objectRecursive
(function(f){var h=Object.keys,t=f.isNaN;f.objectRecursive=function(k,f,u,l,m,d){l=1===l;m=!!m;d="number"!==typeof d||t(d)?100:d;var a=[[],0,h(k).sort(),k],q=[];do{var g=a.pop(),n=a.pop(),e=a.pop(),r=a.pop();for(q.push(g);n[0];){var c=n.shift(),b=g[c],p=r.concat(c),c=f.call(u,g,b,c,p,e);if(!0!==c)if(!1===c){a.length=0;break}else if(!(d<=e)&&b instanceof Object){if(-1!==q.indexOf(b)){if(m)continue;throw Error("Circular reference");}if(l)a.unshift(p,e+1,h(b).sort(),b);else{a.push(r,e,n,g);a.push(p,e+1,h(b).sort(),b);break}}}}while(a[0]);return k}})(window);

function cws_uniq_id ( prefix ){
	var prefix = prefix != undefined && typeof prefix == 'string' ? prefix : "";
	var d = new Date();
	var t = d.getTime();
	var unique = Math.random() * t;
	var unique_id = prefix + unique;
	return unique_id;
}
function cws_has_class ( el, cls_name ){
	var re = new RegExp( "(^|\\s)" + cls_name + "(\\s|$)", 'g' );
	return re.test( el.className );
}
function cws_add_class ( el, cls_name ){
	if(!el){
		return false;
	}
	el.className =  el.className.length ? el.className + " " + cls_name : cls_name;		
}
function cws_remove_class ( el, cls_name ){
	var re = new RegExp( "\\s?" + cls_name, "g" );
	el.className = el.className.replace( re, '' );
}

/*--------- CWS_RESPONSIVE_SCRIPTS ----------*/
function is_mobile_device(){
	if ( navigator.userAgent.match( /(Android|iPhone|iPod|iPad|Phone|DROID|webOS|BlackBerry|Windows Phone|ZuneWP7|IEMobile|Tablet|Kindle|Playbook|Nexus|Xoom|SM-N900T|GT-N7100|SAMSUNG-SGH-I717|SM-T330NU)/ ) ) {
		return true;
	} else {
		return false;
	}
}

function cws_is_tablet_viewport () {
	if ( window.innerWidth > 767 && window.innerWidth < 1200 ){
		return true;
	} else {
		return false;
	}		
}

function is_mobile () {
	if ( window.innerWidth < 768 ){
		return true;
	} else {
		return false;
	}		
}

function not_desktop(){
	return window.innerWidth <= 1366;
}

function has_mobile_class(){
	return jQuery("body").hasClass("cws_mobile");
}

function cws_resize_controller() {
	var cwsBody = jQuery('body');
	if( is_mobile_device() && cws_is_tablet_viewport()){
		cwsBody.removeClass('cws_mobile');
		cwsBody.addClass('cws_tablet');
	} else if ( is_mobile_device() && is_mobile() ){
		cwsBody.removeClass('cws_tablet');
		cwsBody.addClass('cws_mobile');
	} else {
		cwsBody.removeClass('cws_tablet');
		cwsBody.removeClass('cws_mobile');
	}
}

function cws_resize() {
	cws_resize_controller();
	window.addEventListener( "resize", function (){
		cws_resize_controller();
	}, false );
}

function cws_is_mobile () {
	var device = is_mobile_device();
	var viewport = not_desktop();
	return device || viewport;
}
/*\--------- CWS_RESPONSIVE_SCRIPTS ----------\*/
function cws_merge_trees ( arr1, arr2 ){
	if ( typeof arr1 != 'object' || typeof arr2 != 'object' ){
		return false;
	}
	return cws_merge_trees_walker ( arr1, arr2 );
}
function cws_merge_trees_walker ( arr1, arr2 ){
	if ( typeof arr1 != 'object' || typeof arr2 != 'object' ){
		return false;
	}
	var keys1 = Object.keys( arr1 ); /* ! not working with null value */
	var keys2 = Object.keys( arr2 );
	var r = {};
	var i;
	for ( i = 0; i < keys2.length; i++ ){
		if ( typeof arr2[keys2[i]] == 'object' ){
			if ( Array.isArray( arr2[keys2[i]] ) ){
				if ( keys1.indexOf( keys2[i] ) === -1 ){
					r[keys2[i]] = arr2[keys2[i]];
				}
				else{
					r[keys2[i]] = arr1[keys2[i]];
				}				
			}
			else{
				if ( typeof arr1[keys2[i]] == 'object' ){
					r[keys2[i]] = cws_merge_trees_walker( arr1[keys2[i]], arr2[keys2[i]] );
				}
				else{
					r[keys2[i]] = cws_merge_trees_walker( {}, arr2[keys2[i]] );
				}
			}
		}
		else{
			if ( keys1.indexOf( keys2[i] ) === -1 ){
				r[keys2[i]] = arr2[keys2[i]];
			}
			else{
				r[keys2[i]] = arr1[keys2[i]];
			}
		}
	}
	return r;
}

function cws_get_flowed_previous ( el ){
	var prev = el.previousSibling;
	var is_prev_flowed;
	if ( !prev ) return false;
	is_prev_flowed = cws_is_element_flowed( prev );
	if ( !is_prev_flowed ){
		return cws_get_flowed_previous( prev );
	}
	else{
		return prev;
	}
}

function cws_is_element_flowed ( el ){
	var el_styles;
	if ( el.nodeName === "#text" ){
		return false;
	}
	el_styles = getComputedStyle( el );
	if ( el_styles.display === "none" || ["fixed","absolute"].indexOf( el_styles.position ) != -1 ){
		return false;
	}else{
		return true;
	}
}

function cws_empty_p_filter_callback (){
	var el = this;
	if ( el.tagName === "P" && !el.innerHTML.length ){
		return false;
	}
	else{
		return true;
	}	
}
function cws_br_filter_callback (){
	var el = this;
	if ( el.tagName === "BR" ){
		return false;
	}
	else{
		return true;
	}	
}

function cws_advanced_resize_init (){
	window.cws_adv_resize = {};
	var resize = window.cws_adv_resize;
	resize.hooks = {
		"start"	: [],
		"end"	: []
	};
	resize.opts = {
		timeout: 150
	}
	resize.timeout_instance = null;
	resize.resize_controller = cws_advanced_resize_resize_controller;
	resize.timeout_instance_prototype = cws_advanced_resize_timeout_instance_prototype;
	resize.run_hook = cws_advanced_resize_run_hook;
	window.addEventListener( "resize", resize.resize_controller );
}
function cws_advanced_resize_resize_controller (){
	if ( !window.cws_adv_resize ) return false;
	if ( window.cws_adv_resize.timeout_instance === null ){
		window.cws_adv_resize.run_hook( "start" );
	}
	window.cws_adv_resize.timeout_instance = new window.cws_adv_resize.timeout_instance_prototype();
}
function cws_advanced_resize_timeout_instance_prototype (){
	var that = this;
	that.id = cws_getRandomInt();
	setTimeout( function (){
		if ( window.cws_adv_resize.timeout_instance.id !== that.id ){
			return false;
		}
		else{
			window.cws_adv_resize.run_hook( "end" );
			window.cws_adv_resize.timeout_instance = null;
		}		
	}, window.cws_adv_resize.opts.timeout );
}
function cws_advanced_resize_run_hook ( hook ){
	var actions = this.hooks[hook]
	var i;
	for ( i = 0; i < actions.length; i++ ){
		actions[i].call();
	} 
}

function cws_attachToResizeStart( func ){
	if ( typeof func !== "function" || window.cws_adv_resize === undefined ){
		return false;
	}
	window.cws_adv_resize.hooks.start.push( func );
}
function cws_attachToResizeEnd( func ){
	if ( typeof func !== "function" || window.cws_adv_resize === undefined ){
		return false;
	}
	window.cws_adv_resize.hooks.end.push( func );
}

function cws_getRandomInt ( min, max ){
	var min = min !== undefined ? min : 0;
	var max = max !== undefined ? max : 1000000;
	return Math.floor( Math.random() * (max - min + 1) ) + min;
}

// Converts from degrees to radians.
function cws_math_radians (degrees){
		return degrees * Math.PI / 180;
};

// Converts from radians to degrees.
function cws_math_degrees (radians){
		return radians * 180 / Math.PI;
};

/**********
* CWS HOOKS
**********/
function cws_hooks_init (){
		window.cws_hooks = {}
}
function cws_add_action ( tag, callback ){
		if ( typeof tag !== "string" || !tag.length ){
			return false;
		}
		if ( typeof callback !== "function" ){
			return false;
		}
		var hooks 	= window.cws_hooks;
		var hook;
		if ( hooks[tag] === 'object' ){
			hook = hooks[tag];
		}
		else{
			hooks[tag] = hook = new cws_hook ( tag );
		}
		hook.addAction( callback );
}
function cws_do_action ( tag, args ){
		var args 		= Array.isArray( args ) ? args : new Array ();
		var hooks 		= window.cws_hooks;
		var hook 		= hooks[tag]; 
		var hook_exists = typeof hook === 'object';
		if ( hook_exists ){
			hook.run( args );
		}
		return false;
}
function cws_hook ( tag ){
		this.tag = tag;
		this.actions = {};
		this.genActionID 	= function (){
			return cws_uniq_id( "cws_action_" );
		}
		this.addAction 		= function ( callback ){
			var actionID 			= this.genActionID();
			var action 				= new cws_action( this, actionID, callback )
			this.actions[actionID] 	= action;
		}
		this.run 			= function ( args ){
			var actionID, action;
			for ( actionID in this.actions ){
				action = this.actions[actionID];
				action.do( args );
			}
		}
}
function cws_action ( hook, actionID, callback ){
		this.hook 		= hook;
		this.id 		= actionID;
		this.callback 	= callback;
		this.do 		= function ( args ){
			this.callback.apply( this, args );
		}
}
/***********
* \CWS HOOKS
***********/


/**********************************
************ \CWS LIBRARY *********
**********************************/	


/**********************************
************ CWS LOADER *********
**********************************/	
(function ($){

	var loader;
	$.fn.start_cws_loader = start_cws_loader;
	$.fn.stop_cws_loader = stop_cws_loader;

	$( document ).ready(function (){
		cws_page_loader_controller ();
	});

	function cws_page_loader_controller (){
		var cws_page_loader, interval, timeLaps ;
		cws_page_loader = $( "#cws_page_loader" );
		timeLaps = 0;
		interval = setInterval( function (){
			var page_loaded = cws_check_if_page_loaded ();	
			timeLaps ++;		
			if ( page_loaded ||  timeLaps == 12) {
				clearInterval ( interval );
				cws_page_loader.stop_cws_loader ();
			}
		}, 10);
	}

	function cws_check_if_page_loaded (){
		var keys, key, i, r;
		if ( window.cws_modules_state == undefined ) return false;
		r = true;
		keys = Object.keys( window.cws_modules_state );
		for ( i = 0; i < keys.length; i++ ){
			key = keys[i];
			if ( !window.cws_modules_state[key] ){
				r = false;
				break;
			}
		}
		return r;
	}	

	function start_cws_loader (){
		var loader_obj, loader_container, indicators;
		loader = jQuery( this );
		if ( !loader.length ) return;
		loader_container = loader[0].parentNode;
		if ( loader_container != null ){
			loader_container.style.opacity = 1;
			setTimeout( function (){
				loader_container.style.display = "block";
			}, 10);
		}
	}

	function stop_cws_loader (){
		var loader_obj, loader_container, indicators;
		loader = jQuery( this );
		if ( !loader.length ) return;
		loader_container = loader[0].parentNode;
		if ( loader_container != null ){
			loader_container.style.opacity = 0;
			setTimeout( function (){
				loader_container.style.display = "none";
				jQuery( ".cws_textmodule_icon_wrapper.add_animation_icon" ).cws_services_icon();
			}, 200);
		}
	}

	function setFilter(filter){
		jQuery("#cws_loader").css({
			webkitFilter:filter,
			mozFilter:filter,
			filter:filter,
		});
	}

	function setGoo(){
		setFilter("url(#goo)");
	}
	
	function setGooNoComp(){
		setFilter("url(#goo-no-comp)");
	}

	function updateCirclePos(){
		var circle=$obj.data("circle");
		TweenMax.set($obj,{
			x:Math.cos(circle.angle)*circle.radius,
			y:Math.sin(circle.angle)*circle.radius,
		})
		requestAnimationFrame(updateCirclePos);
	}

	function setupCircle($obj){
		if(typeof($obj.data("circle"))=="undefined"){
			$obj.data("circle",{radius:0,angle:0});

			updateCirclePos();
		}
	}

	function startCircleAnim($obj,radius,delay,startDuration,loopDuration){
		setupCircle($obj);
		$obj.data("circle").radius=0;
		$obj.data("circle").angle=0;
		TweenMax.to($obj.data("circle"),startDuration,{
			delay:delay,
			radius:radius,
			ease:Quad.easeInOut
		});
		TweenMax.to($obj.data("circle"),loopDuration,{
			delay:delay,
			angle:Math.PI*2,
			ease:Linear.easeNone,
			repeat:-1
		});
	}

	function stopCircleAnim($obj,duration){
		TweenMax.to($obj.data("circle"),duration,{
			radius:0,
			ease:Quad.easeInOut,
			onComplete:function(){
				TweenMax.killTweensOf($obj.data("circle"));
			}
		});
	}

}(jQuery));

/**********************************
************ \CWS LOADER *********
**********************************/	

/**********************************
************ CWS PARALLAX SCROLL PLUGIN *********
**********************************/	

(function ( $ ){
  
	$.fn.cws_prlx = cws_prlx;

	window.addEventListener( 'scroll', function (){
		if ( window.cws_prlx != undefined && !window.cws_prlx.disabled ){
			window.cws_prlx.translate_layers();
		}
	}, false );

	window.addEventListener( 'resize', function (){
		var i, section_id, section_params, layer_id;
		if ( window.cws_prlx != undefined ){
			if ( window.cws_prlx.servant.is_mobile() ){
				if ( !window.cws_prlx.disabled ){
					for ( layer_id in window.cws_prlx.layers ){
						window.cws_prlx.layers[layer_id].el.removeAttribute( 'style' );
					}
					window.cws_prlx.disabled = true;          
				}
			}
			else{
				if ( window.cws_prlx.disabled ){
					window.cws_prlx.disabled = false;
				}
				for ( section_id in window.cws_prlx.sections ){
					section_params = window.cws_prlx.sections[section_id];
					if ( section_params.height != section_params.el.offsetHeight ){
						window.cws_prlx.prepare_section_data( section_id );
					}
				}
			}
		}
	}, false );

	function cws_prlx ( args ){
		var factory, sects;
		sects = $( this );
		if ( !sects.length ) return;
		factory = new cws_prlx_factory( args );
		window.cws_prlx = window.cws_prlx != undefined ? window.cws_prlx : new cws_prlx_builder ();
		sects.each( function (){
			var sect = $( this );
			var sect_id = factory.add_section( sect );
			if ( sect_id ) window.cws_prlx.prepare_section_data( sect_id );
		});
	}

	function cws_prlx_factory ( args ){
		var args = args != undefined ? args : {};
		args.def_speed = args.def_speed != undefined && !isNaN( parseInt( args.def_speed ) ) && parseInt( args.def_speed > 0 ) && parseInt( args.def_speed <= 100 ) ? args.def_speed : 50;
		args.layer_sel = args.layer_sel != undefined && typeof args.layer_sel == "string" && args.layer_sel.length ? args.layer_sel : ".cws_prlx_layer";  
		this.args = args;
		this.add_section = cws_prlx_add_section;
		this.add_layer = cws_prlx_add_layer; 
		this.remove_layer = cws_prlx_remove_layer;   
	}

	function cws_prlx_builder (){
		this.servant = new cws_servant ();
		this.sections = {};
		this.layers = {}; 
		this.calc_layer_speed = cws_prlx_calc_layer_speed;
		this.prepare_section_data = cws_prlx_prepare_section_data;
		this.prepare_layer_data = cws_prlx_prepare_layer_data;
		this.translate_layers = cws_prlx_translate_layers;
		this.translate_layer = cws_prlx_translate_layer;
		this.conditions = {};
		this.conditions.layer_loaded = cws_prlx_layer_loaded_condition;
		this.disabled = false;
	}

	function cws_prlx_add_section ( section_obj ){
		var factory, section, section_id, layers, layer, i;
		factory = this;
		section = section_obj[0];
		layers = $( factory.args.layer_sel, section_obj );
		if ( !layers.length ) return false;
		section_id = window.cws_prlx.servant.uniq_id( 'cws_prlx_section_' );
		section.id = section_id;

		window.cws_prlx.sections[section_id] = {
			'el' : section,
			'height' : null,
			'layer_sel' : factory.args.layer_sel
		}

		if ( /cws_Yt_video_bg/.test( section.className ) ){  /* for youtube video background */ 
			section.addEventListener( "DOMNodeRemoved", function ( e ){
				var el = e.srcElement ? e.srcElement : e.target;
				if ( $( el ).is( factory.args.layer_sel ) ){
					factory.remove_layer( el.id );
				}
			}, false );
			section.addEventListener( "DOMNodeInserted", function ( e ){
				var el = e.srcElement ? e.srcElement : e.target;
				if ( $( el ).is( factory.args.layer_sel ) ){
					factory.add_layer( el, section_id );
				}
			}, false );
		}

		section.addEventListener( "DOMNodeRemoved", function ( e ){ /* for dynamically removed content */
			window.cws_prlx.prepare_section_data( section_id );
		},false );
		section.addEventListener( "DOMNodeInserted", function ( e ){ /* for dynamically added content */
			window.cws_prlx.prepare_section_data( section_id );
		},false );

		for ( i = 0; i < layers.length; i++ ){
			layer = layers[i];
			factory.add_layer( layer, section_id )
		}

		return section_id;

	}

	function cws_prlx_add_layer ( layer, section_id ){
		var factory, layer_rel_speed, layer_params;
		factory = this;
		layer.id = !layer.id.length ? window.cws_prlx.servant.uniq_id( 'cws_prlx_layer_' ) : layer.id;
		layer_rel_speed = $( layer ).data( 'scroll-speed' );
		layer_rel_speed = layer_rel_speed != undefined ? layer_rel_speed : factory.args.def_speed;
		layer_params = {
			'el' : layer,
			'section_id' : section_id,
			'height' : null,
			'loaded' : false,
			'rel_speed' : layer_rel_speed,
			'speed' : null
		}
		window.cws_prlx.layers[layer.id] = layer_params;
		return layer.id;
	}

	function cws_prlx_remove_layer ( layer_id ){
		var layers;
		layers = window.cws_prlx.layers;
		if ( layers[layer_id] != undefined ){
			delete layers[layer_id];
		}
	}

	function cws_prlx_prepare_section_data ( section_id ){
		var section, section_params, layer_sel, layers, layer, layer_id, i, section_obj;
		if ( !Object.keys( window.cws_prlx.sections ).length || window.cws_prlx.sections[section_id] == undefined ) return false;
		section_params = window.cws_prlx.sections[section_id];
		section = section_params.el;
		section_params.height = section.offsetHeight;
		section_obj = $( section );
		layers = $( section_params.layer_sel, section_obj );
		for ( i=0; i<layers.length; i++ ){
			layer = layers[i];
			layer_id = layer.id;
			if ( layer_id ) window.cws_prlx.prepare_layer_data( layer_id, section_id );
		}
	}

	function cws_prlx_prepare_layer_data ( layer_id, section_id ){
		window.cws_prlx.servant.wait_for( 'layer_loaded', [ layer_id ], function ( layer_id ){
			var layer_params, layer;
			layer_params = window.cws_prlx.layers[layer_id];
			layer = layer_params.el;
			layer_params.height = layer.offsetHeight;
			window.cws_prlx.calc_layer_speed( layer_id );
			window.cws_prlx.translate_layer( layer_id );
			layer_params.loaded = true;
		}, [ layer_id ]);    
	}

	function cws_prlx_translate_layers (){
		var layers, layer_ids, layer_id, i;
		if ( window.cws_prlx == undefined ) return;
		layers = window.cws_prlx.layers;
		layer_ids = Object.keys( layers );
		for ( i = 0; i < layer_ids.length; i++ ){
			layer_id = layer_ids[i];
			window.cws_prlx.translate_layer( layer_id );
		}
	}

	function cws_prlx_translate_layer ( layer_id ){
		var layer_params, section, layer, layer_translation, style_adjs;
		if ( window.cws_prlx == undefined || window.cws_prlx.layers[layer_id] == undefined ) return false;
		layer_params = window.cws_prlx.layers[layer_id];
		if ( layer_params.speed == null ) return false;
		if ( layer_params.section_id == undefined || window.cws_prlx.sections[layer_params.section_id] == undefined ) return false;
		section = window.cws_prlx.sections[layer_params.section_id].el;
		if ( window.cws_prlx.servant.is_visible( section ) ) {
			layer = layer_params.el;

			layer_translation = ( section.getBoundingClientRect().top - window.innerHeight ) * layer_params.rel_speed;
			style_adjs = {
				"WebkitTransform" : "translate(0%," + layer_translation + "px)",
				"MozTransform" : "translate(0%," + layer_translation + "px)",
				"msTransform" : "translate(0%," + layer_translation + "px)",
				"OTransform" : "translate(0%," + layer_translation + "px)",
				"transform" : "translate(0%," + layer_translation + "px)"
			}

			for (var key in style_adjs ){
				layer.style[key] = style_adjs[key];
			}
		}
	}

	function cws_servant (){
		this.uniq_id = cws_uniq_id;
		this.wait_for = cws_wait_for;
		this.is_visible = cws_is_visible;
		this.is_mobile = cws_is_mobile;
	}

	function cws_uniq_id ( prefix ){
		var d, t, n, id;
		var prefix = prefix != undefined ? prefix : "";
		d = new Date();
		t = d.getTime();
		n = parseInt( Math.random() * t );
		id = prefix + n;
		return id;
	}

	function cws_wait_for ( condition, condition_args, callback, callback_args ){
		var match = false;
		var condition_args = condition_args != undefined && typeof condition_args == 'object' ? condition_args : new Array();
		var callback_args = callback_args != undefined && typeof callback_args == 'object' ? callback_args : new Array();
		if ( condition == undefined || typeof condition != 'string' || callback == undefined || typeof callback != 'function' ) return match;
		match = window.cws_prlx.conditions[condition].apply( window, condition_args );
		if ( match == true ){
			callback.apply( window, callback_args );
			return true;
		}
		else if ( match == false ){
			setTimeout( function (){
				cws_wait_for ( condition, condition_args, callback, callback_args );
			}, 10);
		}
		else{
			return false;
		}
	}

	function cws_is_visible ( el ){
		var window_top, window_height, window_bottom, el_top, el_height, el_bottom, r;
		window_top = window.pageYOffset;
		window_height = window.innerHeight;
		window_bottom = window_top + window_height;
		el_top = $( el ).offset().top;
		el_height = el.offsetHeight;
		el_bottom = el_top + el_height;
		r = ( el_top > window_top && el_top < window_bottom ) || ( el_top < window_top && el_bottom > window_bottom ) || ( el_bottom > window_top && el_bottom < window_bottom ) ? true : false;
		return r;
	}

	function cws_is_mobile (){
		return window.innerWidth < 768;
	}

	function cws_prlx_layer_loaded_condition ( layer_id ){
		var layer, r;
		r = false;
		if ( layer_id == undefined || typeof layer_id != 'string' ) return r;
		if ( window.cws_prlx.layers[layer_id] == undefined ) return r;
		layer = window.cws_prlx.layers[layer_id].el;
		switch ( layer.tagName ){
			case "IMG":
			if ( layer.complete == undefined ){
			}
			else{
				if ( !layer.complete ){
					return r;
				}
			}
			break;  
			case "DIV":  /* for youtube video background */
			if ( /^video-/.test( layer.id ) ){
				return r;
			}
			break;      
		}
		return true;
	}

	function cws_prlx_calc_layer_speed ( layer_id ){
		var layer_params, layer, section_id, section_params, window_height;
		layer_params = window.cws_prlx.layers[layer_id];
		layer = layer_params.el;
		section_id = layer_params.section_id;
		section_params = window.cws_prlx.sections[section_id];
		window_height = window.innerHeight;
		layer_params.speed = ( ( layer_params.height - section_params.height ) / ( window_height + section_params.height ) ) * ( layer_params.rel_speed / 100 );
	}

}(jQuery));

/**********************************
************ CWS PARALLAX SCROLL PLUGIN *********
**********************************/	
cws_modules_state_init ();
is_visible_init ();
cws_milestone_init();
cws_pie_chart_init();
cws_widget_divider_init();
setTimeout(cws_widget_services_init,0);
var directRTL;
var wait_load_portfolio = false;
if (jQuery("html").attr('dir') == 'rtl') {
	directRTL =  'rtl'
}else{
	directRTL =  ''
};

window.addEventListener( "load", function (){
	window.cws_modules_state.sync = true;
	cws_revslider_pause_init();
	cws_header_bg_init();
	cws_header_imgs_cover_init();
	cws_header_parallax_init();
	cws_scroll_parallax_init();
	widget_carousel_init();
	cws_sc_carousel_init();
	twitter_carousel_init();
	testimonials_carousel_init();
	cws_owl_carousel_init();
	// category_carousel_init();
	// isotope_init();
	cws_DividerSvgWrap();

	// -> Start Menu Scripts
	cws_toggle_menu();
	cws_close_menu();
	cws_click_menu_item();
	cws_simple_sticky_menu();
	cws_smart_sticky_menu();
	// <- End Menu Scripts

	cws_parallax_init();
	cws_prlx_init_waiter ();
	cws_sticky_footer_init(false);
	cws_animate_title_init();
	single_sticky_content();
	responsive_table();
	cws_megamenu_active();
	// cws_unite_boxed_wth_vc_stretch_row_content();
	cws_footer_init();

	/* cws megamenu */
		if ( window.cws_megamenu != undefined ){

		var menu = document.querySelectorAll( ".main-nav-container .main-menu" );
		for (var i = 0; i <= menu.length; i++) {
			window.cws_megamenu_main 	= new cws_megamenu( menu[i], {
				'fw_sel'							: '.container',
				'bottom_level_sub_menu_width_adj'	: 2
			});
		}
		
		window.cws_megamenu_sticky 	= new cws_megamenu( document.querySelector( "#sticky_menu" ), {
			'fw_sel'							: '.wide_container',
			'bottom_level_sub_menu_width_adj'	: 2
		});		
	}
	onYouTubePlayerAPIReady()
	Video_resizer ();

}, false );
jQuery(document).ready(function (){	
	cws_resize();
	vimeo_init();
	cws_self_hosted_video ();
	// cws_touch_events_fix ();
	cws_adaptive_top_bar_init();
	cws_hover_fix();
	cws_page_focus();
	cws_top_panel_search ();
	boxed_var_init ();
	cws_fs_video_bg_init ();
	wp_standard_processing ();
	cws_page_header_video_init ();
	cws_top_social_init ();
	cws_fancybox_init();
	wow_init();
	cws_revslider_class_add();
	cws_blog_full_width_layout();
	// jQuery( "aside .cws-widget, .posts_grid .portfolio_item.under_img.add_divider" ).cws_widget_divider(); //Uncomment this line to add divider to widgets
	scroll_down_init ();

	cws_button_animation_init();
	cws_fix_styles_init();
	cws_widgets_on_tablet();
	cws_widget_hover_init();
	cws_go_to_page_init();
	cws_sticky_sidebars_init();
	cws_sidebar_bottom_info();
	cws_side_panel_init();
	cws_center_menu_init();

	scroll_top_init ();
	cws_woo_product_thumbnails_carousel_init ();

	jQuery(window).resize( function (){
		cws_slider_video_height (jQuery( ".fs_video_slider" ));
		cws_slider_video_height (jQuery( ".fs_img_header" ));
	} );
});

jQuery(window).resize( function (){
	vimeo_init();
	cws_self_hosted_video ();
	Video_resizer ();
} );


/*function cws_unite_boxed_wth_vc_stretch_row_content (){
	jQuery( ".cws-layer .vc_row" ).each( function (){
		if(jQuery(this).data( "layerMargin" )){
			jQuery(this).css({'margin': jQuery(this).data( "layerMargin" )});
		}
	});
}*/

function cws_megamenu_active (){
	jQuery( ".main-menu .cws_megamenu_item .menu-item.current-menu-item" ).each(function(){
		jQuery(this).closest( ".menu-item-object-megamenu_item" ).addClass( "current-menu-item" );
	})
}

function cws_modules_state_init (){
	window.cws_modules_state = {
		"sync" : false,		
	}
}

function cws_revslider_class_add (){
	if (jQuery('.rev_slider_wrapper.fullwidthbanner-container').length) {
		jQuery('.rev_slider_wrapper.fullwidthbanner-container').next().addClass('benefits_after_slider');
		if (jQuery('.rev_slider_wrapper.fullwidthbanner-container').length && jQuery('.site-main main .benefits_cont:first-child').length) {
			if (jQuery('.site-main main .benefits_cont:first-child').css("margin-top").replace("px", "") < -90) {
				jQuery('.site-main main .benefits_cont:first-child').addClass('responsive-minus-margin');
			}
		}
	};
}

function cws_prlx_init_waiter (){
	var interval, layers, layer_ids, i, layer_id, layer_obj, layer_loaded;
	if ( window.cws_prlx == undefined ){
		return;
	}
	layers = cws_clone_obj( window.cws_prlx.layers );
	interval = setInterval( function (){
		layer_ids = Object.keys( layers );
		for ( i = 0; i < layer_ids.length; i++ ){
			layer_id = layer_ids[i];
			layer_obj = window.cws_prlx.layers[layer_id];
			layer_loaded = layer_obj.loaded;
			if ( layer_loaded ){
				delete layers[layer_id];
			}
		}
		if ( !Object.keys( layers ).length ){
			clearInterval ( interval );
		}
	}, 100);
}

function cws_adaptive_top_bar(first_init, top_bar_window_resolution, top_bar_left_items, top_bar_right_items){
	if( jQuery(window).width() < 768 && (localStorage.getItem('top_bar_check') != 'mobile' || first_init)){
		//Set mobile top-bar
		jQuery('.top_bar_wrapper .container').append(top_bar_left_items);
		jQuery('.top_bar_wrapper .container').append(top_bar_right_items);

		//Сheck if the script was started
		localStorage.setItem('top_bar_check', 'mobile');
	} else if( jQuery(window).width() > 767 && localStorage.getItem('top_bar_check') != 'desktop' ) {
		//Remove mobile top-bar
		jQuery(top_bar_left_items).remove();
		jQuery(top_bar_right_items).remove();

		//Set desktop top-bar
		jQuery('.top_bar_wrapper .top_bar_icons.left_icons').append(top_bar_left_items);
		jQuery('.top_bar_wrapper .top_bar_icons.right_icons').append(top_bar_right_items);

		//Сheck if the script was started
		localStorage.setItem('top_bar_check', 'desktop');

		//Reinit JS
		cws_top_panel_search();
		cws_top_social_init();
		cws_side_panel_toggle();
	}
}

function cws_adaptive_top_bar_init(){
	//Definite variables
	var top_bar_left_items = jQuery('.top_bar_wrapper .top_bar_icons.left_icons > *');
	var top_bar_right_items = jQuery('.top_bar_wrapper .top_bar_icons.right_icons > *');
	var first_init = false;

	if( jQuery(window).width() < 768 ){
		var top_bar_window_resolution = localStorage.setItem('top_bar_check', 'mobile');
		first_init = true;
	} else {
		var top_bar_window_resolution = localStorage.setItem('top_bar_check', 'desktop');
	}

	cws_adaptive_top_bar(first_init, top_bar_window_resolution, top_bar_left_items, top_bar_right_items);

	jQuery(window).resize( function (){
		cws_adaptive_top_bar(false, top_bar_window_resolution, top_bar_left_items, top_bar_right_items);
	});
}


function cws_touch_events_fix (){
	if ( is_mobile_device() || is_mobile() ){
		jQuery( "body" ).on( "click", ".pic_alt .link_overlay, .products .pic > a, .category-block > a", function (e){
			if ( jQuery(this).hasClass('mobile_hover') ) {
			} else {
				jQuery(this).closest('body').find('a').removeClass('mobile_hover');
				jQuery(this).addClass('mobile_hover');
				e.preventDefault();			
			}
		});
	}
}

function cws_hover_fix(){
	if( not_desktop() ){
		jQuery('a.woo_icon').on('click', function(e) {
			setTimeout(function(){},0);
			e.preventDefault();
		});
	}
}

function cws_is_rtl(){
	return jQuery("body").hasClass("rtl");
}

function cws_page_focus(){
	document.getElementsByTagName('html')[0].setAttribute('data-focus-chek', 'focused');
		window.addEventListener('focus', function() {
		document.getElementsByTagName('html')[0].setAttribute('data-focus-chek', 'focused');
	});
}

function boxed_var_init(){
	var body_el = document.body;
	var children = body_el.childNodes;
	var child_class = "";
	var match;
	window.boxed_layout = false;
	for ( var i=0; i<children.length; i++ ){
		child_class = children[i].className;
		if ( child_class != undefined ){
			match = /page_boxed/.test( child_class );
			if ( match ){
				window.boxed_layout = true;
				break;
			}
		}
	}
}

function reload_scripts(){
	wp_standard_processing();
	cws_fancybox_init();
}

function is_visible_init (){
	jQuery.fn.is_visible = function (){
		return ( jQuery(this).offset().top >= jQuery(window).scrollTop() ) && ( jQuery(this).offset().top <= jQuery(window).scrollTop() + jQuery(window).height() );
	}
}

//Animate header title
function cws_animate_title_init (){ 
	var header_title = jQuery('.page_title');

	if (header_title.hasClass('animate_title') && !is_mobile() && !is_mobile_device() && !has_mobile_class() ){
		if (typeof fancybox === 'function') {
		skrollr.init({
		    constants: {
		        header: jQuery('.bg_page_header').offset().top
		    }
		});
	}
	}
}

function cws_footer_init (){
	if( jQuery(window).height() > jQuery('body').height() ){
		jQuery('.copyrights_area').addClass('absolute');
	}
}

function get_logo_position(){
	if (jQuery(".site_header").length) {
		return /logo-\w+/.exec(jQuery(".site_header").attr("class"))[0];
	};
}

/* sticky */

function cws_top_panel_search (){
	//Top bar search
	jQuery(".top_bar_search .search_icon").click(function(){
		var el = jQuery(this);
		el.closest('.top_bar_search').find('.row_text_search .search-field').val('');
		if(jQuery(window).width() > 1366){
			el.closest('.top_bar_search').find('.row_text_search .search-field').focus();
		}
		el.closest('.top_bar_search').toggleClass( "show-search" );
	});

	//Clear text (ESC)
	jQuery(".top_bar_search .row_text_search .search-field").keydown(function(event) {
		if (event.keyCode == 27){
			jQuery(this).val('');
		}
	});
}

/* carousel */

function count_carousel_items ( cont, layout_class_prefix, item_class, margin ){
	var re, matches, cols, cont_width, items, item_width, margins_count, cont_without_margins, items_count;
	if ( !cont ) return 1;
	layout_class_prefix = layout_class_prefix ? layout_class_prefix : 'grid-';
	item_class = item_class ? item_class : 'item';
	margin = margin ? margin : 30;
	re = new RegExp( layout_class_prefix + "(\d+)" );
	matches = re.exec( cont.attr( "class" ) );
	cols = matches == null ? 1 : parseInt( matches[1] );
	cont_width = cont.outerWidth();
	items = cont.children( "." + item_class );
	item_width = items.eq(0).outerWidth();
	margins_count = cols - 1;
	cont_without_margins = cont_width - ( margins_count * margin ); /* margins = 30px */
	items_count = Math.floor( cont_without_margins / ( item_width -5 ) );	
	return items_count;
}

function widget_carousel_init(){
	jQuery( ".widget_carousel" ).each( function (){
		var cont = jQuery(this);
		var nav = cont.hasClass('nav');
		var dots = cont.hasClass('dots');

		if (not_desktop){
			nav = false;
			dots = true;
		}

		var owl = cont.owlCarousel( {
			direction: directRTL,
			singleItem: true,
			slideSpeed: 300,
			nav: nav,
			dots: dots,
			autoHeight:true,
			items: 1,
			margin : 10,
		});

		//Add arrow controls events
		if ( nav.length ){
			jQuery( ".next", nav ).click( function (){
				owl.trigger( "owl.next" );
			});
			jQuery( ".prev", nav ).click( function (){
				owl.trigger( "owl.prev" );
			});						
		}

	});
}

jQuery.fn.cws_flex_carousel = function ( parent_sel, header_sel ){
	parent_sel = parent_sel != undefined ? parent_sel : '';
	header_sel = header_sel != undefined ? header_sel : '';
	jQuery( this ).each( function (){
		var owl = jQuery( this );
		var nav = jQuery(this).parents(parent_sel).find( ".carousel_nav_panel_container" );
		owl.cws_flex_carousel_controller( parent_sel, header_sel );
		if ( nav.length ){
			jQuery( ".next", nav ).click( function (){
				owl.trigger( "owl.next" );
			});
			jQuery( ".prev", nav ).click( function (){
				owl.trigger( "owl.prev" );
			});						
		}
		jQuery( window ).resize( function (){
			owl.cws_flex_carousel_controller( parent_sel, header_sel );
		});
	});
}

jQuery.fn.cws_flex_carousel_controller = function ( parent_sel, header_sel ){
	var owl = jQuery(this);
	// var nav = jQuery( ".carousel_nav_panel_container", parent_sel );
	var nav = jQuery(this).closest(parent_sel).find('.carousel_nav_panel_container');
	var show_hide_el = nav.siblings().length ? nav : nav.closest( header_sel );
	var show_pagination = false;
	if (show_hide_el.length) {
		var show_hide_el_display_prop = window.getComputedStyle( show_hide_el[0] ).display;
		show_pagination = false;
	}else{
		show_pagination = true;
	}

	var is_init = owl.hasClass( 'owl-carousel' );
	if ( is_init ){
		owl.data('owlCarousel').destroy();
		show_hide_el.css( 'display', 'none' );
	}

	var items_count = owl.children().length;
	var visible_items_count = count_carousel_items( owl );
	var args = {
		direction: directRTL,
		items: visible_items_count,
		slideSpeed: 300,
		navigation: false,
		pagination: show_pagination,
		responsive: false,
		autoHeight:true,
		margin : 10,
	}
	if ( items_count > visible_items_count ){
		owl.owlCarousel( args );
		if (show_hide_el.length) {
			show_hide_el.css( 'display', show_hide_el_display_prop );
		}
		
	}
}

/* Image Circle Wrap */
function cws_DividerSvgWrap(){
	jQuery(".div_title").each(function(){
		var self = jQuery(this);
		if(!jQuery(this).find('span:not(.svg_lotus)')[0]){
			jQuery(this).addClass('standard_color');
		}
		jQuery(this).css({'fill' : jQuery(this).find('span:not(.svg_lotus)').css("color"),
			'stroke' : jQuery(this).find('span:not(.svg_lotus)').css("color")
		});
		var ajax = new XMLHttpRequest();
		ajax.open("GET", ajaxurl.templateDir + "/img/lotos.svg", true);
		ajax.send();
		ajax.onload = function(e) {
		  var span = document.createElement("span");
		  span.className = 'svg_lotus';
		  span.innerHTML = ajax.responseText;
		  jQuery(self).append(span);
		}
	});	
}

//Owl Carousel Init
function cws_owl_carousel_init ( area ){
	var area = area == undefined ? document : area;
	jQuery( ".cws_owl_carousel:not(.owl-loaded)", area ).each( function (){
		var defaults_args = {
			items : 1,
			direction: directRTL,
			responsiveClass : true,
			cols : 1,
			nav : false,
			dots : false,
			autoheight : false,
			autoplay : false,
			autoplaySpeed : false,
			autoplayHoverPause : false,
			margin : 10,
			stagePadding : 0,
			center : false,
			loop : false,
			animateIn : false,
			animateOut : false,
			smartSpeed : 300
		};

		var args = new Object();
		var el = jQuery(this);
		var new_args = el.data('owl-args');

		//Custom selector
		var carousel_selector = el.data('owl-selector');
		if(typeof carousel_selector != 'undefined'){
			var found_selector = el.find(carousel_selector);
			if (found_selector.length){
				el = found_selector;
			}
		}

		args = Object.assign(defaults_args, new_args);
		//Responsive
		switch ( args.cols ){
			case 4:
				args.responsive = {
				    0:{
				        items:1,
				    },
				    479:{
				        items:2,
				    },
				    980:{
				        items:3,
				    },
				    1170:{
				        items:4,
				    },        
				};
				break
			case 3:
				args.responsive = {
				    0:{
				        items:1,
				    },
				    479:{
				        items:2,
				    },
				    980:{
				        items:3,
				    }      
				};
				break
			case 2:
				args.responsive = {
				    0:{
				        items:1,
				    },
				    479:{
				        items:2,
				    }    
				};				
				break
			default:
				args.singleItem = true;
		}

		jQuery( el ).owlCarousel( args );


		/*jQuery(this).parent().find(".carousel_nav.next").click(function (){
			owl.trigger('next.owl.carousel');
		});
		jQuery(this).parent().find(".carousel_nav.prev").click(function (){
			owl.trigger('prev.owl.carousel');
		});*/

	});
}



	// var area = area == undefined ? document : area;
	// jQuery( ".cws_owl_carousel", area ).each( function (){
	// 	var carousel = this;
	// 	var section = jQuery( carousel ).closest( ".posts_grid" );
	// 	var nav = jQuery( ".carousel_nav", section );
	// 	var cols = carousel.dataset.cols;
	// 	var args = {
	// 		itemsel: "*:not(style)",	/* for staff members because they have custom color styles */
	// 		slideSpeed: 300,
	// 		navigation: false,
	// 		pagination: false,
	// 		autoHeight:true,
	// 		items: 1,
	// 		margin : 10,
	// 	};

	// 	if(jQuery( carousel ).hasClass('auto_play_owl')){
	// 		args.autoPlay = true;
	// 	}
	// 	if(jQuery( carousel ).hasClass('pagination_owl')){
	// 		args.pagination = true;
	// 	}
	// 	//if(jQuery(this).hasClass('carousel_pagination')){
	// 	//	args.pagination = true;
	// 	//}
	// 	switch ( cols ){
	// 		case '4':
	// 		args.itemsCustom = [
	// 			[0,1],
	// 			[479,2],
	// 			[980,3],
	// 			[1170, 4]
	// 		];
	// 		break
	// 		case '3':
	// 		args.itemsCustom = [
	// 			[0,1],
	// 			[479,2],
	// 			[980,3]
	// 		];
	// 		break
	// 		case '2':
	// 		args.itemsCustom = [
	// 			[0,1],
	// 			[479,2]
	// 		];
	// 		break
	// 		default:
	// 		args.singleItem = true;
	// 	}
	// 	jQuery( carousel ).owlCarousel( args );

	// 	if ( nav.length && jQuery( carousel ).hasClass('navigation_owl')){
	// 		jQuery( ".next", nav ).click( function (){
	// 			jQuery( carousel ).trigger( "next.owl.carousel" );
	// 		});
	// 		jQuery( ".prev", nav ).click( function (){
	// 			jQuery( carousel ).trigger( "prev.owl.carousel" );
	// 		});
	// 	}
	// });


function cws_sc_carousel_init (){
	jQuery( ".cws_sc_carousel" ).each( cws_sc_carousel_controller );
	window.addEventListener( 'resize', function (){
		jQuery( ".cws_sc_carousel" ).each( cws_sc_carousel_controller );		
	}, false);
}
function cws_sc_carousel_controller (){
	var el = jQuery( this );
	var bullets_nav = el.hasClass( "bullets_nav" );
	var content_wrapper = jQuery( ".cws_wrapper", el );
	var owl = content_wrapper;
	var content_top_level = content_wrapper.children();
	var nav = jQuery( ".carousel_nav_panel", el );
	var cols = el.data( "columns" );
	var items_count, grid_class, col_class, items, is_init, matches, args, page_content_section, sb_count;
	var autoplay_speed = (el.hasClass( "autoplay" ) ? el.data( "autoplay" ) : false);

	page_content_section = jQuery( ".page_content" );
	if ( page_content_section.hasClass( "double_sidebar" ) ){
		sb_count = 2;
	}
	else if ( page_content_section.hasClass( "single_sidebar" ) ){
		sb_count = 1;
	}
	else{
		sb_count = 0;
	}
	if ( content_top_level.find(".gallery[class*='galleryid-']").length > 0 ){
		owl = content_top_level.find( ".gallery[class*='galleryid-']" );
		is_init = owl.hasClass( "owl-carousel" );
		if ( is_init ) owl.data( "owlCarousel" ).destroy();
		owl.children( ":not(.gallery-item)" ).remove();
		items_count = count_carousel_items( owl, "gallery-columns-", "gallery-item" );
	}
	else if ( content_top_level.is( ".woocommerce" ) ){
		owl = content_top_level.children( ".products" );
		is_init = owl.hasClass( "owl-carousel" );
		if ( is_init ) owl.data( "owlCarousel" ).destroy();
		owl.children( ":not(.product)" ).remove();
		matches = /columns-\d+/.exec( content_top_level.attr( "class" ) );
		grid_class = matches != null && matches[0] != undefined ? matches[0] : '';
		owl.addClass( grid_class );
		items_count = count_carousel_items( owl, "columns-", "product" );
		owl.removeClass( grid_class );
	}
	else if ( content_top_level.is( "ul" ) ){
		owl = content_top_level;
		is_init = owl.hasClass( "owl-carousel" );
		if ( is_init ) owl.data( "owlCarousel" ).destroy();
		items = owl.children();
		grid_class = "crsl-grid-" + cols;
		col_class = "grid_col_" + Math.round( 12 / cols );
		owl.addClass( grid_class );
		if ( !items.hasClass( "item" ) ) items.addClass( "item" )
		items.addClass( col_class );
		items_count = count_carousel_items( owl, "crsl-grid-", "item" );
		owl.removeClass( grid_class );
		items.removeClass( col_class );
	}
	else {
		is_init = owl.hasClass( "owl-carousel" );
		if ( is_init ) owl.data( "owlCarousel" ).destroy();
		items = owl.children();
		grid_class = "crsl-grid-" + cols;
		col_class = "grid_col_" + Math.round( 12 / cols );
		owl.addClass( grid_class );
		if ( !items.hasClass( "item" ) ) items.addClass( "item" )
		items.addClass( col_class );
		items_count = count_carousel_items( owl, "crsl-grid-", "item" );
		owl.removeClass( grid_class );
		items.removeClass( col_class );
	}

	args = {
		direction: directRTL,
		slideSpeed: 300,
		navigation: false,
		pagination: bullets_nav,
		autoPlay: autoplay_speed,
		autoHeight:true,
		items: 1,
		margin : 10,
	}

	switch ( items_count ){
		case 4:
			if ( sb_count == 2 ){
				args.itemsCustom = [
					[0,1],
					[750,2],
					[980,2],
					[1170, 2]
				];
			}
			else if ( sb_count == 1 ){
				args.itemsCustom = [
					[0,1],
					[750,3],
					[980,3],
					[1170, 3]
				];
			}
			else{
				args.itemsCustom = [
					[0,1],
					[750,4],
					[980,4],
					[1170, 4]
				];
			}
			break;
		case 3:
			if ( sb_count == 2 ){
				args.itemsCustom = [
					[0,1],
					[750,2],
					[980,2],
					[1170, 2]
				];
			}
			else if ( sb_count == 1 ){
				args.itemsCustom = [
					[0,1],
					[750,3],
					[980,3],
					[1170, 3]
				];
			}
			else{
				args.itemsCustom = [
					[0,1],
					[750,3],
					[980,3]
				];	
			}
			break;
		case 2:
			if ( sb_count == 2 ){
				args.itemsCustom = [
					[0,1],
					[750,2],
					[980,2],
					[1170, 2]
				];
			}
			else if ( sb_count == 1 ){
				args.itemsCustom = [
					[0,1],
					[750,2],
					[980,2],
					[1170, 2]
				];
			}
			else{
				args.itemsCustom = [
					[0,1],
					[750,2],
					[980,2],
					[1170, 2]
				];	
			}
			break;
		default:
			args.singleItem = true;
	}
	owl.owlCarousel(args);
	if ( nav.length ){
		jQuery( ".next", nav ).click( function (){
			owl.trigger( "owl.next" );
		});
		jQuery( ".prev", nav ).click( function (){
			owl.trigger( "owl.prev" );
		});
	}	
}

function cws_woo_product_thumbnails_carousel_init (){
	jQuery( ".woo_product_thumbnail_carousel" ).each( function (){
		var cols, args, prev, next;
		var owl = jQuery( this );
		var matches = /carousel_cols_(\d+)/.exec( this.className );
		if ( !matches ){
			cols = 3;
		}
		else{
			cols = matches[1];
		}
		args = {
			slideSpeed: 300,
			navigation: false,
			pagination: false,
			items: cols,
			autoHeight:true,
			items: 1,
			margin : 10,
	
		}
		owl.owlCarousel( args );
		prev = this.parentNode.querySelector( ":scope > .prev" );
		next = this.parentNode.querySelector( ":scope > .next" );
		if ( prev ){
			prev.addEventListener( "click", function (){
				owl.trigger( "owl.prev" );
			}, false );
		}
		if ( next ){
			next.addEventListener( "click", function (){
				owl.trigger( "owl.next" );
			}, false );
		}
	});
}

function twitter_carousel_init (){
	jQuery( ".tweets_carousel" ).each( function (){
		var el = jQuery( this );
		var owl = jQuery( ".cws_wrapper", el );
		owl.owlCarousel({
			direction: directRTL,
			singleItem: true,
			slideSpeed: 300,
			navigation: false,
			pagination: true,
			autoHeight:true,
			items: 1,
			margin : 10,

		});
	});
}

function testimonials_carousel_init (){
	jQuery( ".testimonials_carousel" ).each( function (){
		var carousel = jQuery( this );
		var cols = carousel.data('col');
		var autoplay_speed = (carousel.hasClass( "autoplay" ) ? carousel.data( "autoplay" ) : false);
		var args = {
			direction: directRTL,
			slideSpeed: 300,
			navigation: false,
			pagination: true,
			autoPlay: autoplay_speed,
			autoHeight:true,
			items: 1,
			margin : 10,

		}
		switch ( cols ){
			case 3:
				args.itemsCustom = [
					[0,1],
					[479,1],
					[980,2],
					[1200,3]
				];
			break
			case 2:
				args.itemsCustom = [
					[0,1],
					[1200,2]
				];
			break
			default:
			args.singleItem = true;
		}
		carousel.owlCarousel(args);
	});
}

function wp_standard_processing (){
	var galls;
	jQuery( "img[class*='wp-image-']" ).each( function (){
		var canvas_id;
		var el = jQuery( this );
		var parent = el.parent( "a" );
		var align_class_matches = /align\w+/.exec( el.attr( "class" ) );
		var align_class = align_class_matches != null && align_class_matches[0] != undefined ? align_class_matches[0] : "";
		var added_class = "cws_img_frame";
		if ( align_class.length ){
			if ( parent.length ){
				el.removeClass( align_class );
			}
			added_class += " " + align_class;
		}
		if ( parent.length ){
			parent.addClass( added_class );
			parent.children().wrapAll( "<div class='cws_blur_wrapper' />" );
		}
	});
	galls = jQuery( ".gallery[class*='galleryid-']" );
	if ( galls.length ){
		galls.each( function (){
			var gallery = jQuery( this );
			var gallery_id = cws_unique_id ( "wp_gallery_" );
			jQuery( "a", gallery ).attr( "data-fancybox-group", gallery_id );
		});
	}

	//Check if function exist
	if (typeof fancybox === 'function') {
		jQuery( ".gallery-icon a[href*='.jpg'], .gallery-icon a[href*='.jpeg'], .gallery-icon a[href*='.png'], .gallery-icon a[href*='.gif'], .cws_img_frame[href*='.jpg'], .cws_img_frame[href*='.jpeg'], .cws_img_frame[href*='.png'], .cws_img_frame[href*='.gif']" ).fancybox({
			padding: 1
		});
	}
}

function cws_unique_id ( prefix ){
	var prefix = prefix != undefined && typeof prefix == 'string' ? prefix : "";
	var d = new Date();
	var t = d.getTime();
	var unique = Math.random() * t;	
	var unique_id = prefix + unique;
	return unique_id;	
}

function cws_toggle_topbar(){
	if( is_mobile() ){
		
	}
}

/* -----> New menu scripts <----- */

function cws_toggle_menu(){
	if( jQuery('.site_header').hasClass('active-sandwich-menu') && !not_desktop() ){

		jQuery('.mobile_menu_hamburger').on('click', function() {
			jQuery(this).toggleClass('active');
			jQuery('.main-nav-container').toggleClass('active');
		});
	} else if( not_desktop() ){
		jQuery('.mobile_menu_hamburger').on('click', function() {
			jQuery(this).addClass('active');
			jQuery('body').addClass('menu-visible');
			jQuery('.menu-overlay').addClass('active');
			jQuery('.menu_box_wrapper').addClass('active');
		});
	}
}

function cws_close_menu(){
	jQuery('.menu-overlay').on('click', function() {
		jQuery(this).removeClass('active');
		jQuery('.menu_box_wrapper').removeClass('active');
		jQuery('.mobile_menu_hamburger').removeClass('active');
		jQuery('body').removeClass('menu-visible');
	});
}

function cws_click_menu_item(){
	if( not_desktop() && is_mobile_device() ){
		var menuItem = jQuery('.menu_box_wrapper .main-menu .menu-item > a'),
			menuItemTrigger = jQuery('.menu_box_wrapper .main-menu .menu-item > .button_open'),
			currentParents;

		menuItem.on('click', function(e) {

			if( jQuery(this).parent().hasClass('menu-item-has-children') ){

				if( !jQuery(this).parent().hasClass('active') ){
					//Close all anouther sub-menu`s
					currentParents = jQuery(this).parents('.menu-item');
					menuItem.parent().not(currentParents).removeClass('active');
					menuItem.parent().not(currentParents).find('.sub-menu').slideUp(300);

					jQuery(this).parent().addClass('active');
					jQuery(this).parent().children('.sub-menu').slideDown(300);

					e.preventDefault();
					return false;
				}
			}

		});

		menuItemTrigger.on('click', function() {
			
			if( jQuery(this).parent().hasClass('active') ){

				jQuery(this).next().slideUp(300);
				jQuery(this).parent().removeClass('active');

			} else {
				//Close all anouther sub-menu`s
				currentParents = jQuery(this).parents('.menu-item');
				menuItemTrigger.parent().not(currentParents).removeClass('active');
				menuItemTrigger.parent().not(currentParents).find('.sub-menu').slideUp(300);

				jQuery(this).next().slideDown(300);
				jQuery(this).parent().addClass('active');

			}

		});
	}
}

function cws_hover_menu_item(){

}

function cws_open_menu_item(){
	if( jQuery('.site_header').hasClass('active-sandwich-menu') && !not_desktop() ){

	}
}

function cws_simple_sticky_menu(){
	if( jQuery('.site_header').hasClass('sticky_simple') ){

		var topBarHeight = jQuery('.top_bar_wrapper').outerHeight() + jQuery('.logo_box').outerHeight() + jQuery('#wpadminbar').height(); 
		var currentScroll = jQuery(window).scrollTop();

		jQuery('.sticky_enable .menu_box').css('transform', 'translateY('+topBarHeight+'px)');
		jQuery('.sticky_enable .menu_box').addClass('visible-sticky');

		jQuery(window).on('scroll', function() {
			var currentScroll = jQuery(this).scrollTop();

			if(jQuery(this).scrollTop() < topBarHeight){
				jQuery('.sticky_enable .menu_box').css('transform', 'translateY('+ (topBarHeight - currentScroll - 1) +'px)');
				jQuery('.sticky_enable').removeClass('sticky_active');
			} else {
				jQuery('.sticky_enable .menu_box').css('transform', 'translateY(0px)');
				jQuery('.sticky_enable').addClass('sticky_active');
			}
		});

		if(currentScroll > topBarHeight){
			jQuery('.sticky_enable .menu_box').css('transform', 'translateY(0px)');
			jQuery('.sticky_enable').addClass('sticky_active');
		} else if(currentScroll <= topBarHeight && currentScroll != 0){
			jQuery('.sticky_enable .menu_box').css('transform', 'translateY('+ (topBarHeight - currentScroll - 1) +'px)');
		}
	}
}

function cws_smart_sticky_menu(){
	if( jQuery('.site_header').hasClass('sticky_smart') ){

		var topBarHeight = jQuery('.top_bar_wrapper').outerHeight();
		var headerHeight = jQuery('.site_header').outerHeight();
		var menuBoxHeight = jQuery('.menu_box').outerHeight();
		var currentScroll = jQuery(window).scrollTop();
		var tempScroll = jQuery(window).scrollTop();
		var scrollMarkerUp = 0;
		var scrollMarkerDown = 0;
		var headerCurrentTransform = 0;
		var headerTransformY = 0;
		var translateY_down = 0;
		var smoothSlideDown = false;
		var defaultSlideUp = false;
		var smoothSlideUp = false;
		var startSmooth = false;
		var smoothTopBar = false;

		jQuery('.site_header').css('transform', 'translateY(-'+parseInt(currentScroll)+'px)');

		jQuery(window).on('scroll', function() {
			currentScroll = jQuery(this).scrollTop();

			//When scrolling down
			if( tempScroll < currentScroll ){ 

				//From default position (Top page position)
				if(currentScroll <= headerHeight){
					jQuery('.site_header').css('transform', 'translateY(-'+parseInt(currentScroll)+'px)');
					defaultSlideUp = true;
				}					
				//From default position (Sharp movement of the mouse wheel)
				else if(currentScroll > headerHeight && defaultSlideUp){
					jQuery('.site_header').css('transform', 'translateY(-'+headerHeight+'px)');
					jQuery('.sticky_enable').addClass('sticky_active');
				}
				//Smooth slide up (Hide menu)
				else if(currentScroll > headerHeight && !defaultSlideUp){
					//Taking the scroll position
					if(smoothSlideUp == false){
						headerCurrentTransform = parseInt(jQuery('.site_header').css('transform').split(',')[5]);
						scrollMarkerDown = currentScroll;
						smoothSlideUp = true;
					}
					var translateY_up = headerCurrentTransform - (currentScroll - scrollMarkerDown);
					jQuery('.site_header').css('transform', 'translateY('+ parseInt(translateY_up) +'px)');
				}

				smoothSlideDown = false;
				smoothTopBar = false;
			} 

			//When scrolling top
			else if( tempScroll >  currentScroll ){ 
				//Show header from default position (Top page position)
				if(currentScroll <= headerHeight && !smoothSlideDown){
					jQuery('.site_header').css('transform', 'translateY(-'+parseInt(currentScroll)+'px)');
					startSmooth = false;
				}
				//Smooth slide down (Show menu)
				else if(currentScroll > headerHeight * 3){
					startSmooth = true;
				}
				if(startSmooth){
					//Taking the scroll position
					if(smoothSlideDown == false){
						scrollMarkerUp = currentScroll;
						headerTransformY = parseInt(jQuery('.site_header').css('transform').split(',')[5]);
						smoothSlideDown = true;
					}
					//Smooth slide down if menu-box is visible now
					if(Math.abs(headerTransformY) < headerHeight){
						//Smooth slide down
						if((scrollMarkerUp - currentScroll) <= (menuBoxHeight - (headerHeight - Math.abs(headerTransformY)))){
							translateY_down = Math.abs(headerTransformY) - (scrollMarkerUp - currentScroll);
							jQuery('.site_header').css('transform', 'translateY(-'+parseInt(translateY_down)+'px)');
						}
						//Show menu box (Sharp movement of the mouse wheel / stop before topbar)
						else {
							jQuery('.site_header').css('transform', 'translateY(-'+topBarHeight+'px)');
						}
					}
					//Smooth slide down if menu-box is not visible now
					else {
						//Smooth slide down
						if((scrollMarkerUp - currentScroll) <= menuBoxHeight){
							translateY_down = headerHeight - (scrollMarkerUp - currentScroll);
							jQuery('.site_header').css('transform', 'translateY(-'+parseInt(translateY_down)+'px)');
						} 
						//Show menu box (Sharp movement of the mouse wheel / stop before topbar)
						else {
							jQuery('.site_header').css('transform', 'translateY(-'+topBarHeight+'px)');
						}
					}
					//Smooth show top-bar when going top
					if(currentScroll < topBarHeight){
						//Taking the header transformY value
						if(smoothTopBar == false){
							headerCurrentTransform = parseInt(jQuery('.site_header').css('transform').split(',')[5]);
							smoothTopBar = true;
						}
						var translateY_topbar = (headerCurrentTransform + topBarHeight) - currentScroll;
						jQuery('.site_header').css('transform', 'translateY('+parseInt(translateY_topbar)+'px)');
					}
				}
				//Header default settings
				if(currentScroll == 0){
					jQuery('.sticky_enable').removeClass('sticky_active');
				}

				defaultSlideUp = false;
				smoothSlideUp = false;
			}

			tempScroll = currentScroll;
		});
	}
}
/* -----> End menu scripts <----- */

//Fancybox Init
function cws_fancybox_init ( area ){
	var area = area == undefined ? document : area;
	if ( typeof jQuery.fn.fancybox == 'function' ){
		jQuery(".fancy").fancybox({
			padding: 1
		});
		
		jQuery("a.cws_img_frame[href*='.jpg'], a.cws_img_frame[href*='.jpeg'], a.cws_img_frame[href*='.png'], a.cws_img_frame[href*='.gif'],.gallery-icon a[href*='.jpg'],.gallery-icon a[href*='.jpeg'],.gallery-icon a[href*='.png'],.gallery-icon a[href*='.gif']").fancybox({
			padding: 1
		});
	}
}

function wow_init (){
	if (typeof WOW === 'function') {
		new WOW().init();	
	}
}
/* wow */

function cws_widget_divider_init (){
	jQuery.fn.cws_widget_divider = function (){
		jQuery(this).each( function (){
			var el = jQuery(this);
			var done = false;
			if (!done) done = cws_widget_divider_controller(el);
			jQuery(window).scroll(function (){
				if (!done) done = cws_widget_divider_controller(el);
			});
		});
	}
}

function cws_widget_divider_controller (el){
	if (el.is_visible()){
		jQuery(el).addClass('divider_init');
		return true;
	}
	return false;
}

function cws_widget_services_init (){
	jQuery.fn.cws_services_icon = function (){
		jQuery(this).each( function (){
			var el = jQuery(this);
			var done = false;
			if (!done) done = cws_icon_animation_controller(el);
			jQuery(window).scroll(function (){
				if (!done) done = cws_icon_animation_controller(el);
			});
		});
	}
}

function cws_icon_animation_controller (el){
	if (el.is_visible() && jQuery(el).hasClass('add_animation_icon') ){
		jQuery(el).addClass('icon_init');
		return true;
	}
	return false;
}


/* parallax */
function cws_parallax_init(){
	if (jQuery( ".cws_prlx_section" ).length) {
		jQuery( ".cws_prlx_section" ).cws_prlx();
	};
}
/* \parallax */

// ==================CWS ELEMENTS INIT==================
/* milestone */
function cws_milestone_init (){
	jQuery.fn.cws_milestone = function (){
		jQuery(this).each( function (){
			var el = jQuery(this);
			var done = false;
			var number_container = el.find(".cws_milestone_number");

			if (number_container.length){
				if ( !done ) done = milestone_controller (el, number_container);
				jQuery(window).scroll(function (){
					if ( !done ) done = milestone_controller (el, number_container);
				});
			}
		});
	}
}
function milestone_controller (el, number_container){
	var od, args;
	var speed = number_container.data( 'speed' );
	var number = number_container.text();
	if (el.is_visible()){
		args= {
			el: number_container[0],
			format: 'd',
		};
		if ( speed ) args['duration'] = speed;
		od = new Odometer( args );
		od.update( number );
		return true;
	}
	return false;
}
function get_digit (number, digit){
	var exp = Math.pow(10, digit);
	return Math.round(number/exp%1*10);
}
/* \milestone */

/* pie chart */
function cws_pie_chart_init (){
	jQuery.fn.cws_pie_chart = function (){
		jQuery(this).each( function (){
			var el = jQuery(this);
			var done = false;
			if (!done) done = pie_chart_controller(el);
			jQuery(window).scroll(function (){
				if (!done) done = pie_chart_controller(el);
			});
		});
	}
}
function pie_chart_controller (el){
	if (el.is_visible()){
		var el_duration = parseInt(el.attr("data-duration"));
		var el_percent = parseInt(el.attr("data-percent"));
		var el_width = parseInt(el.attr("data-width"));
		var el_bar_color_start = el.attr("data-barColorStart");
		var el_bar_color_end = el.attr("data-barColorEnd");
		var el_line_color = el.attr("data-lineColor");

		el.pieChart( {
			barColor: {
				start_color : el_bar_color_start,
				end_color : el_bar_color_end
			}, // bar color
			trackColor: el_line_color, // background color
			lineWidth: el_width, // line width
			size: (is_mobile() ? 100 : 200), // chart size
			rotate: 0, // the origin of coordinates is at the top
			animate: { // custom animation
			  duration: el_duration,
			  enabled: true
			},
			easing: function (x, t, b, c, d) { // jquery easing function
			  t = t / (d / 2);
			  if (t < 1) {
			    return c / 2 * t * t + b;
			  }
			  return -c / 2 * ((--t) * (t - 2) - 1) + b;
			},
			onStep: function (from, to, currentValue) { // show the percent digital animate
				var digit = Math.round(currentValue);
				if (el_percent == digit) return;

				el.find('.cws_pie_char_value').html(digit+1+'%');					
			  return;
			},
		});
		return true;
	}
	return false;
}
/* \pie chart */
// ==================//CWS ELEMENTS INIT==================

function cws_Hex2RGB(hex) {
	var hex = hex.replace("#", "");
	var color = '';
	if (hex.length == 3) {
		color = hexdec(hex.substr(0,1))+',';
		color = color + hexdec(hex.substr(1,1))+',';
		color = color + hexdec(hex.substr(2,1));
	}else if(hex.length == 6){
		color = hexdec(hex.substr(0,2))+',';
		color = color + hexdec(hex.substr(2,2))+',';
		color = color + hexdec(hex.substr(4,2));
	}
	return color;
}
function hexdec(hex_string) {
	hex_string = (hex_string + '')
	.replace(/[^a-f0-9]/gi, '');
	return parseInt(hex_string, 16);
}

/* header parallax */


function cws_header_imgs_cover_init (){
	cws_header_imgs_cover_controller ();
	window.addEventListener( "resize", cws_header_imgs_cover_controller, false );
}

function cws_header_imgs_cover_controller (){
	var prlx_sections, prlx_section, section_imgs, section_img, i, j;
	var prlx_sections = jQuery( '.cws_parallax_scene_container > .cws_parallax_scene, .header_bg_img > .cws_parallax_section');	
	for ( i = 0; i < prlx_sections.length; i++ ){
		prlx_section = prlx_sections[i];
		section_imgs = jQuery( "img", jQuery( prlx_section ) );
		for ( j = 0; j < section_imgs.length; j++ ){
			section_img = section_imgs[j];
			cws_cover_image( section_img, prlx_section );
		}
	}
}

function cws_cover_image ( img, section ){
	var section_w, section_h, img_nat_w, img_nat_h, img_ar, img_w, img_h, canvas;
	if ( img == undefined || section == undefined ) return;
	section_w = section.offsetWidth;
	section_h = section.offsetHeight;	
	img_nat_w = img.naturalWidth;
	img_nat_h = img.naturalHeight;
	img_ar = img_nat_w / img_nat_h;
	if ( img_ar > 1 ){
		img_h = section_h;
		img_w = section_h * img_ar;
	}
	else{
		img_w = section_w;
		img_h = section_w / img_ar;
	}
	img.width = img_w;
	img.height = img_h;
}

function cws_header_bg_init(){
	var bg_sections = jQuery('.header_bg_img, .cws_parallax_scene_container');
	bg_sections.each( function (){
		var bg_section = jQuery( this );
		cws_header_bg_controller( bg_section );
	});
	window.addEventListener( 'resize', function (){
		var bg_sections = jQuery('.header_bg_img, .cws_parallax_scene_container');
		bg_sections.each( function (){
			var bg_section = jQuery( this );
			cws_header_bg_controller( bg_section );
		});
	}, false );
}

function cws_header_bg_controller ( bg_section ){
	var benefits_area = jQuery( ".benefits_area" ).eq( 0 );
	var page_content_section = jQuery( ".page_content" ).eq( 0 );
	var top_curtain_hidden_class = "hidden";
	var top_panel = jQuery( "#site_top_panel" );
	var top_curtain = jQuery( "#top_panel_curtain" );
	var consider_top_panel = top_panel.length && top_curtain.length && top_curtain.hasClass( top_curtain_hidden_class );
		if ( benefits_area.length ){
			if ( consider_top_panel ){
				bg_section.css( {
					'height' : bg_section.parent().outerHeight() + 200 + bg_section.parent().offset().top + top_panel.outerHeight() + "px",
					'margin-top' : "-" + ( bg_section.parent().offset().top + top_panel.outerHeight() ) + "px"
				});
			}
			else{
				bg_section.css( {
					'height' : bg_section.parent().outerHeight() + 200 + bg_section.parent().offset().top + "px",
					'margin-top' : "-" + bg_section.parent().offset().top + "px"
				});
			}
			bg_section.addClass( 'height_assigned' );
		}
		else if ( page_content_section.length ){
			if ( page_content_section.hasClass( "single_sidebar" ) || page_content_section.hasClass( "double_sidebar" ) ){
				if ( consider_top_panel ){
					bg_section.css({
						'height' : bg_section.parent().outerHeight() + bg_section.parent().offset().top + top_panel.outerHeight() + "px",
						'margin-top' : "-" + ( bg_section.parent().offset().top + top_panel.outerHeight() ) + "px"
					});
				}
				else{
					bg_section.css({
						'height' : bg_section.parent().outerHeight() + bg_section.parent().offset().top + "px",
						'margin-top' : "-" + bg_section.parent().offset().top + "px"
					});
				}
				bg_section.addClass( 'height_assigned' );				
			}
			else{
				if ( consider_top_panel ){
					bg_section.css({
						'height' : bg_section.parent().outerHeight() + 200 + bg_section.parent().offset().top + top_panel.outerHeight() + "px",
						'margin-top' : "-" + ( bg_section.parent().offset().top + top_panel.outerHeight() ) + "px"
					});
				}
				else{
					bg_section.css({
						'height' : bg_section.parent().outerHeight() + 200 + bg_section.parent().offset().top + "px",
						'margin-top' : "-" + bg_section.parent().offset().top + "px"
					});
				}
				bg_section.addClass( 'height_assigned' );				
			}
		}
}

function cws_header_parallax_init(){
	// var scenes = jQuery( ".cws_parallax_section, .cws_parallax_scene" );
	var header = jQuery( ".title_box .cws_parallax_section" );
	cws_parallax_section_init(header);
}

function cws_parallax_section_init(scenes){
	if (typeof Parallax === 'function') {
		scenes.each( function (){
			var scene = this;
			var prlx_scene = new Parallax ( scene );
		});
	}
}

// Title image with parallax effect
function cws_scroll_parallax_init (){
	var scroll = 0;
	var window_width = jQuery(window).width();
	var background_size_width;

	jQuery(window).scroll(function() {
		scroll = jQuery(window).scrollTop();
		window_width = jQuery(window).width();
	});


	if(jQuery('.title.has_fixed_background').length){

		var background_size_width = parseInt(jQuery('.title.has_fixed_background').css('background-size').match(/\d+/));
		var title_holder_height = jQuery('.title.has_fixed_background').height();

		if (jQuery('.bg_page_header').hasClass('hide_header')){
			var top = jQuery('.bg_page_header').data('top');
			var bottom = jQuery('.bg_page_header').data('bottom');
			title_holder_height = top+bottom+88;
		}

		var title_rate = (title_holder_height / 10000) * 7;
		var title_distance = scroll - jQuery('.title.has_fixed_background').offset().top;
		var title_bpos = -(title_distance * title_rate);
		jQuery('.title.has_fixed_background').css({'background-position': 'center 0px' });
		if(jQuery('.title.has_fixed_background').hasClass('zoom_out')){
			jQuery('.title.has_fixed_background').css({'background-size': background_size_width-scroll + 'px auto'});
		}
	}

	jQuery(window).on('scroll', function() {

		if(jQuery('.title.has_fixed_background').length){
			var title_distance = scroll - jQuery('.title.has_fixed_background').offset().top;
			var title_bpos = -(title_distance * title_rate);
			jQuery('.title.has_fixed_background').css({'background-position': 'center ' + title_bpos + 'px' });
			if(jQuery('.title.has_fixed_background').hasClass('zoom_out') && (background_size_width-scroll > window_width)){
				jQuery('.title.has_fixed_background').css({'background-size': background_size_width-scroll + 'px auto'});
			}
		}
	});

}

function cws_carousels_init_waiter ( els, callback ){
	for ( var i = 0; i < els.length; i++ ){
		if ( jQuery( els[i] ).hasClass( 'owl-carousel' ) ){
			els.splice( i, 1 );
		}
	}
	if ( els.length ){
		setTimeout( function (){
			cws_carousels_init_waiter ( els, callback );
		}, 10 );
	}
	else{
		callback ();
		return true;
	}
}

function cws_wait_for_header_bg_height_assigned ( callback ){
	var header_bg_sections = jQuery( '.header_bg_img, .cws_parallax_scene_container' );
	if ( callback == undefined || typeof callback != 'function' ) return;
	cws_header_bg_height_assigned_waiter ( header_bg_sections, callback );
}

function cws_header_bg_height_assigned_waiter ( els, callback ){
	var i;
	for ( i = 0; i < els.length; i++ ){
		if ( jQuery( els[i] ).hasClass( 'height_assigned' ) ){
			els.splice( i, 1 );
		}
	}
	if ( els.length ){
		setTimeout( function (){
			cws_header_bg_height_assigned_waiter ( els, callback );
		}, 10 );
	}
	else{
		callback ();
		return true;
	}
}

/* \header parallax */

/* full screen video */

function cws_page_header_video_init (){
	cws_set_header_video_wrapper_height();
	window.addEventListener( 'resize', cws_set_header_video_wrapper_height, false )
}

function cws_set_header_video_wrapper_height (){
	var containers = document.getElementsByClassName( 'page_header_video_wrapper' );
	for ( var i=0; i<containers.length; i++ ){
		cws_set_window_height( containers[i] );
	}			
}

function scroll_down_init (){
	jQuery( ".fs_video_slider" ).on( "click", ".scroll_down", function ( e ){
		var anchor, matches, id, el, el_offset;
		e.preventDefault();
		anchor = jQuery( this ).attr( "href" );
		matches = /#(\w+)/.exec( anchor );
		if ( matches == null ) return;
		id = matches[1];
		el = document.getElementById( id );
		if ( el == null ) return;
		el_offset = jQuery( el ).offset().top;
		jQuery( "html, body" ).animate({
			scrollTop : el_offset
		}, 300);
	});	
}

/* \full screen video */

/* BLUR */

function cws_wait_for_image ( img, callback ){
	var complete = false;
	if ( img == undefined || img.complete == undefined || callback == undefined || typeof callback != 'function' ) return;
	if ( !img.complete ){
		setTimeout( function (){
			cws_wait_for_image ( img, callback );
		}, 10 );
	}
	else{
		callback ();
		return true;
	} 
}

function cws_wait_for_canvas ( canvas, callback ){
	var drawn = false;
	if ( canvas == undefined || typeof canvas != 'object' || callback == undefined || typeof callback != 'function' ) return;
	if ( !jQuery( canvas ).hasClass( 'drawn' ) ){
		setTimeout( function (){
			cws_wait_for_canvas ( canvas, callback );
		}, 10);
	}
	else{
		callback ();
		return true;
	}
}

/* \BLUR */

/* SCROLL TO TOP */ //CHANGE THIS  //Save variables  animation_curve_speed, animation_curve_scrolltop
function scroll_top_vars_init (){
	window.scroll_top = {
		el : jQuery( ".scroll_to_top" ),
		anim_in_class : "fadeIn",
		anim_out_class : "fadeOut"
	};
}
function scroll_top_init (){
	scroll_top_vars_init ();
	scroll_top_controller ();
	window.addEventListener( 'scroll', scroll_top_controller, false);
	window.scroll_top.el.on( 'click', function (){
		window.scroll_top.el.css({
			"pointer-events" : "none"
		});		
		jQuery( "html, body" ).animate( {scrollTop : 0}, animation_curve_speed, animation_curve_scrolltop, function (){
			window.scroll_top.el.addClass( window.scroll_top.anim_out_class );
		});
	});
}
function scroll_top_controller (){
	var scroll_pos = window.pageYOffset;
	if ( window.scroll_top == undefined ) return;
	if ( scroll_pos < window.innerHeight && window.scroll_top.el.hasClass( window.scroll_top.anim_in_class ) ){
		window.scroll_top.el.css({
			"pointer-events" : "none"
		});
		window.scroll_top.el.removeClass( window.scroll_top.anim_in_class );
		window.scroll_top.el.addClass( window.scroll_top.anim_out_class );
	}
	else if( scroll_pos >= window.innerHeight && !window.scroll_top.el.hasClass( window.scroll_top.anim_in_class ) ){
		window.scroll_top.el.css({
			"pointer-events" : "auto"
		});
		window.scroll_top.el.removeClass( window.scroll_top.anim_out_class );
		window.scroll_top.el.addClass( window.scroll_top.anim_in_class );
	}
}
/* \SCROLL TO TOP */

function cws_set_window_width ( el ){
	var window_w;
	if ( el != undefined ){
		window_w = document.body.clientWidth;
		el.style.width = window_w + 'px';
	}
}
function cws_set_window_height ( el ){
	var window_h;
	if ( el != undefined ){
		window_h = window.innerHeight;
		el.style.height = window_h + 'px';
	}
}

function cws_top_social_init (){
	jQuery('.social_links_wrapper.toogle-of').on('click', function() {
		jQuery(this).find('.cws_social_links').toggleClass('active');
		jQuery(this).find('.social-btn-open-icon').toggleClass('active');
		jQuery(this).find('.social-btn-open').toggleClass('active');
	});
}

function single_sticky_content(){
	var item = jQuery(".cws_portfolio_description.sticky_desc");
	var item_p = item.parent();
	if(typeof item_p.theiaStickySidebar != 'undefined'){
		item_p.theiaStickySidebar({
			additionalMarginTop: 80,
			additionalMarginBottom: 30
		}); 		
	}
}

function cws_fs_video_bg_init (){
	var slider_wrappers, header_height_is_set;
	header_height_is_set = document.getElementsByClassName( 'header_video_fs_view' );

	if ( !header_height_is_set.length) return;
		cws_fs_video_slider_controller( header_height_is_set[0] );
	window.addEventListener( 'resize', function (){
		cws_fs_video_slider_controller( header_height_is_set[0] );
	});
}
function cws_fs_video_slider_controller ( el ){
	cws_set_window_width( el );
	cws_set_window_height( el );
}

function cws_slider_video_height (element){
	var height_coef = element.attr('data-wrapper-height')
	if (height_coef) {
		if (window.innerWidth<960) {
			element.height(window.innerWidth/height_coef)
		}else{
			element.height(960/height_coef)
		}
	}	
}

/* SLIDER SCROLL CONTROLLER */
function cws_revslider_pause_init (){
	var slider_els, slider_el, slider_id, id_parts, revapi_ind, revapi_id, i;
	var slider_els = document.getElementsByClassName( "rev_slider" );
	window.cws_revsliders = {};
	if ( !slider_els.length ) return;
	for ( i = 0; i < slider_els.length; i++ ){
		slider_el = slider_els[i];
		slider_id = slider_el.id;
		id_parts = /rev_slider_(\d+)(_\d+)?/.exec( slider_id );
		if ( id_parts == null ) continue;
		if ( id_parts[1] == undefined ) continue;
		revapi_ind = id_parts[1];
		revapi_id = "revapi" + revapi_ind;
		window.cws_revsliders[slider_id] = {
			'el' : slider_el,
			'api_id' : revapi_id,
			'stopped' : false
		}
		window[revapi_id].bind( 'revolution.slide.onloaded', function (){
			cws_revslider_scroll_controller ( slider_id );
		});
		window.addEventListener( 'scroll', function (){
			cws_revslider_scroll_controller ( slider_id );
		});	
	}	
}

function cws_revslider_scroll_controller ( slider_id ){
	var slider_obj, is_visible;
	if ( slider_id == undefined ) return;
	slider_obj = window.cws_revsliders[slider_id];
	is_visible = jQuery( slider_obj.el ).is_visible();
	if ( is_visible && slider_obj.stopped ){
		window[slider_obj.api_id].revresume();
		slider_obj.stopped = false;
	}
	else if ( !is_visible && !slider_obj.stopped ){
		window[slider_obj.api_id].revpause();	
		slider_obj.stopped = true;		
	}
}
/* \SLIDER SCROLL CONTROLLER */

/* \TOP PANEL MOBILE */

function cws_clone_obj ( src_obj ){
	var new_obj, keys, i, key, val;
	if ( src_obj == undefined || typeof src_obj != 'object' ) return false;
	new_obj = {};
	keys = Object.keys( src_obj );
	for ( i = 0; i < keys.length; i++ ){
		key = keys[i];
		val = src_obj[key];
		new_obj[key] = val;
	}
	return new_obj;
}

//Detect browser
function cws_detect_browser() { 
    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
    {
        return 'Opera';
    }
    else if(navigator.userAgent.indexOf("Chrome") != -1 )
    {
        return 'Chrome';
    }
    else if(navigator.userAgent.indexOf("Safari") != -1)
    {
        return 'Safari';
    }
    else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
    {
        return 'Firefox';
    }
    else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
    {
    	return 'IE';
    }  
    else 
    {
       return 'unknown';
    }
}

// Fix styles
function cws_button_animation_init(){
	jQuery('.cws_button_bubble').each(function() {
		var element = jQuery(this);
		var btnWidth = jQuery(this).innerWidth();

		var btnHeight = jQuery(this).innerHeight();
		jQuery(this).parent().find('.cws_effect_button').width(btnWidth-4);
		jQuery(this).parent().find('.cws_effect_button').height(btnHeight);

		var circlesTopLeft = jQuery(this).parent().find('.circle.top-left');
		var circlesBottomRight = jQuery(this).parent().find('.circle.bottom-right');

		var tl = new TimelineLite();
		var tl2 = new TimelineLite();

		var btTl = new TimelineLite({ paused: true });

		tl.to(circlesTopLeft, 1.2, { x: -25, y: -25, scaleY: 2, ease: SlowMo.ease.config(0.1, 0.7, false) });
		tl.to(circlesTopLeft.eq(0), 0.1, { scale: 0.2, x: '+=6', y: '-=2' });
		tl.to(circlesTopLeft.eq(1), 0.1, { scaleX: 1, scaleY: 0.8, x: '-=10', y: '-=7' }, '-=0.1');
		tl.to(circlesTopLeft.eq(2), 0.1, { scale: 0.2, x: '-=15', y: '+=6' }, '-=0.1');
		tl.to(circlesTopLeft.eq(0), 1, { scale: 0, x: '-=5', y: '-=15', opacity: 0 });
		tl.to(circlesTopLeft.eq(1), 1, { scaleX: 0.4, scaleY: 0.4, x: '-=10', y: '-=10', opacity: 0 }, '-=1');
		tl.to(circlesTopLeft.eq(2), 1, { scale: 0, x: '-=15', y: '+=5', opacity: 0 }, '-=1');

		var tlBt1 = new TimelineLite();
		var tlBt2 = new TimelineLite();

		tlBt1.set(circlesTopLeft, { x: 0, y: 0, rotation: -45 });
		tlBt1.add(tl);

		tl2.set(circlesBottomRight, { x: 0, y: 0 });
		tl2.to(circlesBottomRight, 1.1, { x: 30, y: 30, ease: SlowMo.ease.config(0.1, 0.7, false) });
		tl2.to(circlesBottomRight.eq(0), 0.1, { scale: 0.2, x: '-=6', y: '+=3' });
		tl2.to(circlesBottomRight.eq(1), 0.1, { scale: 0.8, x: '+=7', y: '+=3' }, '-=0.1');
		tl2.to(circlesBottomRight.eq(2), 0.1, { scale: 0.2, x: '+=15', y: '-=6' }, '-=0.2');
		tl2.to(circlesBottomRight.eq(0), 1, { scale: 0, x: '+=5', y: '+=15', opacity: 0 });
		tl2.to(circlesBottomRight.eq(1), 1, { scale: 0.4, x: '+=7', y: '+=7', opacity: 0 }, '-=1');
		tl2.to(circlesBottomRight.eq(2), 1, { scale: 0, x: '+=15', y: '-=5', opacity: 0 }, '-=1');

		tlBt2.set(circlesBottomRight, { x: 0, y: 0, rotation: 45 });
		tlBt2.add(tl2);

		btTl.add(tlBt1);
		btTl.to(jQuery(this).parent().find('.button.cws_effect_button'), 0.8, { scaleY: 1.1 }, 0.1);
		btTl.add(tlBt2, 0.2);
		btTl.to(jQuery(this).parent().find('.button.cws_effect_button'), 1.8, { scale: 1, ease: Elastic.easeOut.config(1.2, 0.4) }, 1.2);

		btTl.timeScale(2.6);

		jQuery(this).on('mouseover', function() {
			btTl.restart();
		});

		jQuery(this).on('click', function() {
			btTl.restart();
		});		
	});
}
// Fix styles
function cws_fix_styles_init(){
	jQuery( window ).resize(function() {
		cws_sticky_footer_init(false);
	});
	
	var resizeTimer;
	jQuery('footer.footer_fixed').on('click', 'ul.menu li.has_children span', function(event) {
		console.log(jQuery(this));
		clearTimeout(resizeTimer);
		resizeTimer = setTimeout(function() {
			cws_sticky_footer_init(true);	
		}, 500);
	});

	var browser = cws_detect_browser();
	//Fixed IE styles
	if (browser == 'IE') jQuery('body').addClass('ie11');
}

function cws_go_to_page_init(){
	if(!not_desktop()){
		var hashTagActive = false;

		jQuery('.menu-item a').click(function(event) {
			if(!jQuery(this).hasClass("fancy") && jQuery(this).attr("href") != "#" && jQuery(this).attr("target") != "_blank"){
				event.stopPropagation();
			    var anchor = jQuery(this).attr("href");
			    var link = anchor.replace('/#','#')
				var re = new RegExp( "^#.*$" );
				var matches = re.exec( link );

				if ((matches == null && jQuery(this).attr("href").indexOf("#") != -1) || (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/))){
					return true;
				} else {
					event.preventDefault();
				}

			    if (hashTagActive) return;
			    hashTagActive = true;      
			    
			    if (jQuery(this).attr("href").indexOf("#") != -1 && matches !== null){

			        if (jQuery(link).length){
			                jQuery('html, body').animate({
			                scrollTop: jQuery(link).offset().top
			            }, animation_curve_speed, animation_curve_menu, function () {
			                hashTagActive = false;
			            });              
			        }

			    } else {
			        jQuery('body').fadeOut(1000, newpage(anchor)); 
			    }
			       
			}
		});

	   function newpage(e) {
	     window.location = e;
	   }
	}
}

function cws_sticky_sidebars_init(){
	//Check if function exist
	if (typeof jQuery.fn.theiaStickySidebar === 'function') {
		if (sticky_sidebars == 1 && !is_mobile() ){
			jQuery('aside.sb_left, aside.sb_right').theiaStickySidebar({
			      additionalMarginTop: 60,
			      additionalMarginBottom: 60
			}); 		
		}
	}	
}

function cws_sidebar_bottom_info(){
    var sideBarBottomHeight = jQuery('.side_panel_bottom').height();
    jQuery('.side_panel').css('padding-bottom', sideBarBottomHeight + 80);
}

function cws_side_panel_toggle(){
	jQuery(".side_panel_trigger").on( 'click', function(){
		jQuery("body").toggleClass("side_panel_show");
		return false;
	});
}

function cws_side_panel_init () {
	var container = jQuery('.side_panel_container');
	if (container.hasClass('appear-slide')){
		if (container.hasClass('panel-left')){
			jQuery("body").addClass('slide_side_panel').addClass('left_slide');
		} else if (container.hasClass('panel-right')){
			jQuery("body").addClass('slide_side_panel').addClass('right_slide');
		}		
	} else if (container.hasClass('appear-pull')){
		if (container.hasClass('panel-left')){
			jQuery("body").addClass('slide_side_panel').addClass('left_pull');
		} else if (container.hasClass('panel-right')){
			jQuery("body").addClass('slide_side_panel').addClass('right_pull');
		}		
	}

	cws_side_panel_toggle();

	jQuery(".side_panel_overlay, .close_side_panel").on( 'click', function(){
		jQuery("body").removeClass("side_panel_show");
		jQuery("body").removeClass("show-overlay");
		jQuery("body").removeClass('oveflow-hidden');
		jQuery(".page_content aside").removeClass('active');
	});
}

function cws_center_menu_init(){
	if (!is_mobile() && !is_mobile_device() && !has_mobile_class() && jQuery('header').hasClass('menu-center') ){
		var icons_block_width = [];
		var max_block_width;

		var headerLogoWidth = jQuery('header').find('.header_logo_part').width(),
			menuIconLeft = jQuery('.menu_left_icons .menu_icons_wrapper'),
			menuIconRight = jQuery('.menu_right_icons .menu_icons_wrapper');

		jQuery.each([menuIconLeft,menuIconRight], function(i, el) {
			icons_block_width.push(jQuery(el).width());
		});

		max_block_width = Math.max.apply(null, icons_block_width);

		if( jQuery('header').hasClass('logo-left') ){
			menuIconLeft.css('min-width', max_block_width);
			menuIconRight.css('min-width', max_block_width+headerLogoWidth);
		}

		if( jQuery('header').hasClass('logo-center') ){
			menuIconLeft.css('min-width', max_block_width);
			menuIconRight.css('min-width', max_block_width);
		}	

		if( jQuery('header').hasClass('logo-right') ){
			menuIconLeft.css('min-width', max_block_width+headerLogoWidth);
			menuIconRight.css('min-width', max_block_width);
		}		

	}
}

function cws_widgets_on_tablet(){
	if( cws_is_tablet_viewport() ){

	var body = jQuery('body'),
		cwsWindow = jQuery(window),
		widgetsTrigger = jQuery('.sidebar-tablet-trigger');

	var bodyHeight = body.height(),
		windowHeight = jQuery(window).height(),
		showTrigger = 70 * ((bodyHeight - windowHeight) / 100);

	var aside = jQuery('.page_content aside'),
		widgetsPos = aside.attr('class');
			
		widgetsPos = 'trigger_'+widgetsPos;
		widgetsTrigger.addClass(widgetsPos);

		jQuery(window).on('scroll', function() {
			if( cwsWindow.scrollTop() > showTrigger ) {
				widgetsTrigger.addClass('active');
			} else {
				widgetsTrigger.removeClass('active');
			}
		});

		widgetsTrigger.on('click', function() {
			aside.toggleClass('active');
			body.toggleClass('oveflow-hidden');
			body.toggleClass('show-overlay');
		});

	}
}

function cws_widget_hover_init(){
	jQuery('.widget_hover').hover(
		//in
		function(event) {
			var hover_color = jQuery(this).data('hover-color');
			jQuery(this).css('border-color', hover_color);
		},
		//out
		function(event) {
			var border_color = jQuery(this).data('border-color');
			jQuery(this).css('border-color', border_color);
		}
	);	
}

function cws_sticky_footer_init(scroll){
	var fixed = Boolean(jQuery('footer.footer_fixed').length);
	var el_offset = jQuery('.scroll_block').offset().top;
	var footer_height = jQuery('footer').innerHeight();
	var copyrights_height = jQuery('.copyrights_area').innerHeight();
	var footerPartHeight = footer_height + copyrights_height;
	var win_height = jQuery(window).height();

	function enable_sticky_footer(){
		jQuery('footer.footer_fixed, .copyrights_area.footer_fixed').addClass('fixed');
		jQuery('footer.footer_fixed').css({'bottom': copyrights_height});
		jQuery('#main').css({
			'background-color': '#ffffff',
			'margin-bottom': footerPartHeight
		});	
	}

	function disable_sticky_footer(){
		jQuery('footer.footer_fixed, .copyrights_area.footer_fixed').removeClass('fixed'); 
		jQuery('#main').css({
			'background-color': 'transparent',
			'margin-bottom': '0px'
		});	
	}

	if (fixed && !is_mobile() && !is_mobile_device() && !has_mobile_class()) {
		if (Boolean(jQuery('.page_boxed').length)){
			var content_width = jQuery('.page_content').width();
			jQuery('footer, .copyrights_area').width(content_width);
		}

		if (footer_height > win_height){
			jQuery('footer.footer_fixed, .copyrights_area.footer_fixed').addClass('large_sidebar');

			//If sidebar hight then window
			if (Boolean(jQuery('.sticky_header_wrapper').length)){			
				var sticky_header_wrapper_height =  jQuery('.sticky_header_wrapper').height()+50;
				jQuery('footer').css('padding-top',sticky_header_wrapper_height+'px');
			}

			jQuery(window).scroll(function(event){
				if (fixed && !is_mobile() && !is_mobile_device() && !has_mobile_class()){
					var st = jQuery(this).scrollTop();

					if (st > el_offset){
						//From line to BOTTOM
						if (Boolean(jQuery('.page_boxed').length)){
							jQuery('footer.footer_fixed, .copyrights_area.footer_fixed').addClass('no_shadow');
						}

						disable_sticky_footer();		
					} else {
						//From line to TOP	
						if (Boolean(jQuery('.page_boxed').length)){
							jQuery('footer.footer_fixed, .copyrights_area.footer_fixed').removeClass('no_shadow');
						}

						enable_sticky_footer();
					}				
				} else {
					disable_sticky_footer();
				}
			});
		} else {
			enable_sticky_footer();	
		}
	if (scroll) jQuery( "html, body" ).animate( {scrollTop : jQuery( "html").height()}, 'slow');
	} else {
		disable_sticky_footer();
	}
}

function responsive_table(){
	var headertext = [];
	var headers = document.querySelectorAll("thead");
	var tablebody = document.querySelectorAll("tbody");	

	if(headers.length == 0){
		headers = document.querySelectorAll("tbody");
	}

	for(var i = 0; i < headers.length; i++) {
		headertext[i]=[];
		headers[i].classList.add("responsive_table");
		for (var j = 0, headrow; headrow = headers[i].rows[0].cells[j]; j++) {
		  var current = headrow;
		  headertext[i].push(current.textContent.replace(/\r?\n|\r/,""));
		  }
	} 


	
	if (headers.length > 0) {
		for (var h = 0, tbody; tbody = tablebody[h]; h++) {
			for (var i = 0, row; row = tbody.rows[i]; i++) {
			  for (var j = 0, col; col = row.cells[j]; j++) {
			  	if(headertext[h]){
			  		col.setAttribute("data-th", headertext[h][j]);
			  	}
			    
			  } 
			}
		}
	}
}

function cws_blog_full_width_layout() {
	var div = jQuery('.posts_grid.posts_grid_fw_img');
	jQuery(div).each(function(){
		div = jQuery(this);
		console.log(div);
		if (!div.hasClass('posts_grid_carousel')) {
			var doc_w = jQuery(document).width();
			var div_w = jQuery('.page_content > main .grid_row').width();
			var marg = ( doc_w - div_w ) / 2;
			div.each(function() {
				jQuery(this).css({
					'margin-left' : '-'+marg+'px',
					'margin-right' : '-'+marg+'px'
				})
			});
			div.find('article.posts_grid_post').each(function() {
				jQuery(this).css({
					'padding-left' : marg+15+'px',
					'padding-right' : marg+15+'px'
				})
			})
		}
	});
	
}

/*******************************************************
************** CWS Self Vimeo Background ***************
*******************************************************/
function vimeo_init() {
	var element;
	var vimeoId;
	var chek;
	jQuery(".cws_Vimeo_video_bg").each(function(){
		element = jQuery(this);
		var el_width;
		var el_height;
		vimeoId = jQuery(".cws_Vimeo_video_bg").attr('data-video-id');

		jQuery("#"+vimeoId).vimeo("play");
			jQuery("#"+vimeoId).vimeo("setVolume", 0);
			jQuery("#"+vimeoId).vimeo("setLoop", true);
			el_width = element[0].offsetWidth;

		if (element[0].offsetHeight<((el_width/16)*9)) {
			el_height = (element[0].offsetWidth/16)*9;
		}else{
			el_height = element[0].offsetHeight;
			el_width = (el_height/9)*16;
		}
		jQuery("#"+vimeoId)[0].style.width = el_width+'px';
		jQuery("#"+vimeoId)[0].style.height = el_height+'px';
		setInterval(check_on_page, 1000);
	})

	function check_on_page (){
		if (document.getElementsByTagName('html')[0].hasAttribute('data-focus-chek')) {		
			if (chek < 1) {
				chek++
				jQuery("#"+vimeoId).vimeo("play");
			}else{
				chek = 1
			}									
		}else{
			jQuery("#"+vimeoId).vimeo("pause");
			chek = 0;
		}
	}	
}

function cws_self_hosted_video (){
	var element,el_width,video
	jQuery('.cws_self_hosted_video').each(function(){
		element = jQuery(this)
		video = element.find('video')
		el_width = element[0].offsetWidth;

		if (element[0].offsetHeight<((el_width/16)*9)) {
			el_height = (element[0].offsetWidth/16)*9;
		}else{
			el_height = element[0].offsetHeight;
			el_width = (el_height/9)*16;
		}
		video[0].style.width = el_width+'px';
		video[0].style.height = el_height+'px';
	})	
}

/*******************************************************
************** \CWS Self Vimeo Background ***************
*******************************************************/

/*******************************************************
************** YouTube video Background ****************
*******************************************************/

var i,
	currTime,
	duration,
	video_source,
	video_id,
	el_height,
	element,
	el_width,
	el_quality,
	player;

	element = document.getElementsByClassName("cws_Yt_video_bg"); 
	
function onYouTubePlayerAPIReady() {
	if(typeof element === 'undefined') 
		return; 
	for (var i = element.length - 1; i >= 0; i--) {
		video_source = element[i].getAttribute("data-video-source");
		video_id = element[i].getAttribute("data-video-id");
		el_width = element[i].offsetWidth;

		

		if (element[i].offsetHeight<((el_width/16)*9)) {
			el_height = (element[i].offsetWidth/16)*9;
		}else{
			el_height = element[i].offsetHeight;
			el_width = (el_height/9)*16;
		}
		if (el_width > 1920){
			el_quality = 'highres'; 
		}
		if (el_width < 1920){
			el_quality = 'hd1080'; 
		}
		if (el_width < 1280) {
			el_quality = 'hd720'; 
		}
		if (el_width < 853) {
			el_quality = 'large';
		}
		if (el_width < 640) {
			el_quality = 'medium';
		};
		rev (video_id,video_source,el_width,el_height);
		//console.log(el_height);
		
	};
}
function rev (video_id,video_source,el_width,el_height){
	window.setTimeout(function() {
		if (!YT.loaded) {
			console.log('not loaded yet');
			window.setTimeout(arguments.callee, 50)
		} else {
			var curplayer = video_control(video_id,video_source,el_width,el_height);		
		}
	}, 50);
}

var chek = 0;
var YouTube;
// youTube.pauseVideo();

function video_control (uniqid,video_source,el_width,el_height) {
	var interval;
	// var player;

	player = new YT.Player(uniqid, {
			height: el_height,
			width: el_width,
			videoId: video_source,
			playerVars: {
				'autoplay' : 1,
				'rel' : 0,
				'showinfo' : 0,
				'showsearch' : 0,
				'controls' : 0,
				'loop' : 1,
				'enablejsapi' : 1,
				'theme' : 'dark',
				'modestbranding' : 0,
				'wmode' : 'transparent',
			},
			events: {
				'onReady': onPlayerReady,
				'onStateChange': onPlayerStateChange
			}
		}
	);
}

window.addEventListener('focus', function() {
	checkPlayer();
	return true;
});
function onPlayerReady(event){
	YouTube = event.target;
	YouTube.mute();
	YouTube.setPlaybackQuality(el_quality);	    
}
function onPlayerStateChange(event) {	
	YouTube.playVideo();
}
function seekTo(event) {
	player.seekTo(0);									
}
function checkPlayer() {	
	if (undefined !== player && undefined !== player.getCurrentTime) {
		currTime = player.getCurrentTime(); //get video position	
		duration = player.getDuration(); //get video duration
		(currTime > (duration - 0.8)) ? seekTo(event) : '';		
	};		
					
}
function chek_on_page (){
	if (document.getElementsByTagName('html')[0].hasAttribute('data-focus-chek')) {		
		if (chek < 1 && undefined !== player.playVideo) {
			chek++
			player.playVideo();
		}else{
			chek = 1
		}									
	}else if (undefined !== player.pauseVideo) {
		player.pauseVideo();
		chek = 0;
	}
}

function Video_resizer (){
	if (element.length) {
		for (var i = element.length - 1; i >= 0; i--) {
			video_source = element[i].getAttribute("data-video-source");
			video_id = element[i].getAttribute("data-video-id");
			el_width = element[i].offsetWidth;
		

			if (element[i].offsetHeight<((el_width/16)*9)) {
				el_height = (element[i].offsetWidth/16)*9;
			}else{
				console.log(element[i].offsetHeight);
				el_height = element[i].offsetHeight;
				el_width = (el_height/9)*16;
			}
			var el_iframe = document.getElementById(element[i].getAttribute("data-video-id"));
			el_iframe.style.width = el_width+'px';
			el_iframe.style.height = el_height+'px';
		};
	};
}
/*******************************************************
************** \YouTube video Background ****************
*******************************************************/


/*******************************************************
************** RETINA ****************
*******************************************************/
var retina = {};
retina.root = (typeof exports === 'undefined' ? window : exports);
retina.config = {
        // An option to choose a suffix for 2x images
        retinaImageSuffix : '@2x',

        // Ensure Content-Type is an image before trying to load @2x image
        // https://github.com/imulus/retinajs/pull/45)
        check_mime_type: true,

        // Resize high-resolution images to original image's pixel dimensions
        // https://github.com/imulus/retinajs/issues/8
        force_original_dimensions: true
    };
retina.config.retinaImagePattern = new RegExp( retina.config.retinaImageSuffix + "." );

(function() {
    function Retina() {}

    window.retina.root.Retina = Retina;

    Retina.configure = function(options) {
        if (options === null) {
            options = {};
        }

        for (var prop in options) {
            if (options.hasOwnProperty(prop)) {
                window.retina.config[prop] = options[prop];
            }
        }
    };

    Retina.init = function(context) {
        if (context === null) {
            context = window.retina.root;
        }

        var existing_onload = context.onload || function(){};

        context.onload = function() {
            var images = document.getElementsByTagName('img'), retinaImages = [], i, image;
            for (i = 0; i < images.length; i += 1) {
                image = images[i];
                if ( !retina.config.retinaImagePattern.test(image.getAttribute("src")) ){
                    if (!!!image.getAttributeNode('data-no-retina')) {
                        retinaImages.push(new RetinaImage(image));
                    }
                }
            }
            existing_onload();
        };
    };

    Retina.isRetina = function(){
        var mediaQuery = '(-webkit-min-device-pixel-ratio: 1.5), (min--moz-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min-resolution: 1.5dppx)';

        if (window.retina.root.devicePixelRatio > 1) {
            return true;
        }

        if (window.retina.root.matchMedia && window.retina.root.matchMedia(mediaQuery).matches) {
            return true;
        }

        return false;
    };


    var regexMatch = /\.\w+$/;
    function suffixReplace (match) {
        return window.retina.config.retinaImageSuffix + match;
    }

    function RetinaImagePath(path, at_2x_path) {
        this.path = path || '';
        if (typeof at_2x_path !== 'undefined' && at_2x_path !== null) {
            this.at_2x_path = at_2x_path;
            this.perform_check = false;
        } else {
            if (undefined !== document.createElement) {
                var locationObject = document.createElement('a');
                locationObject.href = this.path;
                locationObject.pathname = locationObject.pathname.replace(regexMatch, suffixReplace);
                this.at_2x_path = locationObject.href;
            } else {
                var parts = this.path.split('?');
                parts[0] = parts[0].replace(regexMatch, suffixReplace);
                this.at_2x_path = parts.join('?');
            }
            this.perform_check = true;
        }
    }

    window.retina.root.RetinaImagePath = RetinaImagePath;

    RetinaImagePath.confirmed_paths = [];

    RetinaImagePath.prototype.is_external = function() {
        return !!(this.path.match(/^https?\:/i) && !this.path.match('//' + document.domain) );
    };

    RetinaImagePath.prototype.check_2x_variant = function(callback) {
        var http, that = this;
        if (this.is_external()) {
            return callback(false);
        } else if (!this.perform_check && typeof this.at_2x_path !== 'undefined' && this.at_2x_path !== null) {
            return callback(true);
        } else if (this.at_2x_path in RetinaImagePath.confirmed_paths) {
            return callback(true);
        } else {
            return callback(false);
        }
    };


    function RetinaImage(el) {
        this.el = el;
        this.path = new RetinaImagePath(this.el.getAttribute('src'), this.el.getAttribute('data-at2x'));
        var that = this;
        this.path.check_2x_variant(function(hasVariant) {
            if (hasVariant) {
                that.swap();
            }
        });
    }

    window.retina.root.RetinaImage = RetinaImage;

    RetinaImage.prototype.swap = function(path) {
        if (typeof path === 'undefined') {
            path = this.path.at_2x_path;
        }

        var that = this;
        function load() {
            var width = that.el.offsetWidth;
            var height = that.el.offsetHeight;
            if ( !that.el.complete || !width || !height ) {
                setTimeout(load, 5);
            } else {
                if (window.retina.config.force_original_dimensions) {
                    that.el.setAttribute('width', width);
                    that.el.setAttribute('height', height);
                }

                that.el.setAttribute('src', path);
            }
        }
        load();
    };


    if (Retina.isRetina()) {
        Retina.init(window.retina.root);
    }
})();

/*******************************************************
************** \RETINA ****************
*******************************************************/