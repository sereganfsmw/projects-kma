<?php
	get_header ();
	global $cws_theme_funcs;
?>
<div class="page_content">
	<?php
	$home_url = get_home_url();
	?>
	<main>
		<div class="grid_row clearfix">
			<div class="grid_col grid_col_12">
				<div class="ce">
					<div class="not_found">
						<div class="banner_404">
							<img src="<?php echo CRYPTOP_URI . "/img/404.png"; ?>" alt />
						</div>
						<div class="desc_404">
							<div class="msg_404">
								<?php
									esc_html_e( 'Sorry', 'cryptop' );
									echo "<br />";
									esc_html_e( "This page doesn't exist.", "cryptop" );
								?>
							</div>
							<div class="link">
								<?php
									esc_html_e( 'Please, proceed to our ', 'cryptop' );
									echo "<a href='".esc_url($home_url)."'>";
									esc_html_e( 'Home page', 'cryptop' );
									echo "</a>";
								?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>

<?php
get_footer ();
?>