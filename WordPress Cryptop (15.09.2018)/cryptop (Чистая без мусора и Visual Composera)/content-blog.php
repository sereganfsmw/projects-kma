<?php
	global $cws_theme_funcs;
	global $cws_theme_default;
	if ($cws_theme_funcs){

		if (is_page()){
			$blogtype = $cws_theme_funcs->cws_get_meta_option( 'blogtype' );
		} elseif (is_front_page() || is_category() || is_tag() || is_archive()) {
			$blogtype = $cws_theme_funcs->cws_get_option( 'blog_options' )['def_blogtype'];
		}
	}
	$taxonomy = "category";
	$terms  = array();

	if ( is_page() ) {
		if($cws_theme_funcs){
			$cats = $cws_theme_funcs->cws_get_meta_option( 'category' );
			$terms = !empty($cats) ? explode(',', $cats) : '';			
		}
	}	else if ( is_category() ) {
		$term_id = get_query_var( 'cat' );
		$term = get_term_by( 'id', $term_id, 'category' );
		$term_slug = $term->slug;
		$terms = array( $term_slug );
	} else if ( is_tag() ) {
		$taxonomy = 'post_tag';
		$term_slug = get_query_var( 'tag' );
		$terms = array( $term_slug );
	}

	$terms = implode( ",", $terms);

	$post_type_array = array("post");
	$posts_per_page = (int)get_option('posts_per_page');
	$ajax = isset( $_POST['ajax'] ) ? (bool)$_POST['ajax'] : false;
	$paged_var = get_query_var( 'paged' );
	$paged = $ajax && isset( $_POST['paged'] ) ? $_POST['paged'] : ( $paged_var ? $paged_var : 1 );

	$args = array(
		'items_count' => PHP_INT_MAX,
		'items_per_page' => $posts_per_page,
		'paged' => $paged,
		'pagination_grid' => 'standard',
	);

	$args['extra_query_args'] = array(
		'post_type' => $post_type_array,
		'post_status' => 'publish',
	);	

	if (!empty($terms)){
		$args['extra_query_args']['tax_query'] = array(
			array(
				'taxonomy'		=> $taxonomy,
				'field'			=> 'slug',
				'terms'			=> $terms
			)
		);
	}

	if ( is_date() ){
		$year = $cws_theme_funcs->cws_get_date_part( 'y' );
		$month = $cws_theme_funcs->cws_get_date_part( 'm' );
		$day = $cws_theme_funcs->cws_get_date_part( 'd' );

		if ( !empty( $year ) ){
			$args['extra_query_args']['date_query']['year'] = $year;
		}

		if ( !empty( $month ) ){
			$args['extra_query_args']['date_query']['month'] = $month;
		}

		if ( !empty( $day ) ){
			$args['extra_query_args']['date_query']['day'] = $day;
		}
	}

	$blogtype = sanitize_html_class( $blogtype );

	$news_class = !empty( $blogtype ) ? ( preg_match( '#^\d+$#', $blogtype ) ? "news-pinterest" : "news-$blogtype" ) : "news-medium";

	if ($news_class == "news-pinterest") {
		wp_enqueue_script ('isotope');
	}	

	if ( !$ajax ): // not ajax request
	?>
	<div class="grid_row">
	<?php
	endif;	
		echo cws_blog_output($args);
	if ( !$ajax ): // not ajax request
	?>
	</div>
	<?php
	endif;
?>