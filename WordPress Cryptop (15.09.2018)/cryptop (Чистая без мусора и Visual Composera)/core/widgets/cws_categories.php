<?php
	/**
	 * CWS Banner Widget Class
	 */

class CWS_Categories extends WP_Widget {
	public $fields = array();
	public function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				'value' => '',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),							
			'cats' => array(
				'title' => esc_html__( 'Categories to show', 'cryptop' ),
				'type' => 'taxonomy',
				'taxonomy' => 'category',
				'atts' => 'multiple',
				'source' => array(),
			),
		);
	}
	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-categories', 'description' => esc_html__( 'Add information about yourself', 'cryptop' ) );
		parent::__construct( 'cws-categories', esc_html__( 'CWS Categories', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',			
			'cats' => array(),
		), $instance));
		global $cws_theme_funcs;
		$title = esc_html($title);
 		$out = '';

 		$thumbnail_dims['width'] = 300;

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
		
			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title) . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}
			//Title

			echo "<div class='cws_categories_widget'>";

				foreach ($cats as $id => $slug) {

			 		$term = get_term_by('slug', $slug, 'category');
			 		$term_name = $term->name;
					$term_id = $term->term_id;
					$link = get_category_link($term_id);
					$term_image = get_term_meta( $term_id, 'cws_mb_term' );
					$dummy_image = get_template_directory_uri() . "/img/img_placeholder.png";
					$is_dummy = true;

					ob_start();
						if (!empty($term_image[0]['image']['src'])){
							$is_dummy = false;
							$dims['crop'] = true;
							$img_obj = cws_thumb( $term_image[0]['image']['src'], $thumbnail_dims , true );
						}
						?>
							<article <?php post_class(array( 'item','categories-grid')); ?>>
								<a class='category-block' href="<?php echo esc_url($link);?>">
									<img src='<?php echo esc_url( !$is_dummy ? $img_obj[0] : $dummy_image ); ?>' alt=''>
									<span class='category-label'><?php echo sprintf("%s", $term_name); ?></span>
								</a>
							</article>

						<?php
					$out .= ob_get_clean();
			 	}
			 	
				printf('%s', $out);

			echo "</div>";
		
		echo sprintf('%s',$after_widget);	
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			if ($v == 'on') {
				$v = '1';
			}
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();
		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>