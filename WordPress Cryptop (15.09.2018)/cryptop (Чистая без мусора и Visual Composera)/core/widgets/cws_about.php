<?php
	/**
	 * CWS About Widget Class
	 */

class CWS_About extends WP_Widget {
	public $fields = array();
	public function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				'value' => '',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'avatar' => array(
				'title' => esc_html__( 'Avatar', 'cryptop' ),
				'addrowclasses' => 'wide_picture',
				'type' => 'media',
			),
			'border' => array(
				'title' => esc_html__( 'Add border', 'cryptop' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
			),
			'shape' => array(
				'title' => esc_html__( 'Shape type', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'rectangle' => array( esc_html__( 'Rectangle', 'cryptop' ),  true, 'e:width;e:height' ),
					'round' =>array( esc_html__( 'Round', 'cryptop' ), false,  'e:width;d:height' ),
				),
			),
			'width' => array(
				'type' => 'number',
				'title' => esc_html__( 'Width (px)', 'cryptop' ),
				'addrowclasses' => 'disable',
				'value' => '150',
			),
			'height' => array(
				'type' => 'number',
				'title' => esc_html__( 'Height (px)', 'cryptop' ),
				'addrowclasses' => 'disable',
				'value' => '150',
			),
			'link' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Avatar link', 'cryptop' ),
				'atts' => 'placeholder="'.esc_html__('http://', 'cryptop').'"',
			),
			'grayscale_avatar' => array(
				'title' => esc_html__( 'Grayscale effect', 'cryptop' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
			),
			'name' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Name', 'cryptop' ),
			),
			'position' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Position', 'cryptop' ),
			),
			'description' => array(
				'title' => esc_html__( 'About Me', 'cryptop' ),
				'type' => 'textarea',
				'atts' => 'rows="10" placeholder="'.esc_html__('Enter information about self', 'cryptop').'"',
				'value' => '',
			),
			'signature' => array(
				'title' => esc_html__( 'Signature', 'cryptop' ),
				'addrowclasses' => 'wide_picture',
				'type' => 'media',
			),
		);
	}
	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-about', 'description' => esc_html__( 'Add information about yourself', 'cryptop' ) );
		parent::__construct( 'cws-about', esc_html__( 'CWS About', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'avatar' => '',
			'border' => '0',
			'shape' => 'round',
			'width' => '',
			'height' => '',
			'link' => '',
			'grayscale_avatar' => '0',
			'name' => '',
			'position' => '',
			'description' => '',
			'signature' => '',
		), $instance));
		global $cws_theme_funcs;

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;

		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';

		$title = esc_html($title);

		$avatar_id = !empty($avatar) ? $avatar['id'] : '';

		if ( !empty($width) || !empty($height) ) {
			if ($shape == 'round') $height = $width;
			$thumb_obj = cws_thumb( $avatar_id, array( 'width' => $width, 'height' => $height, 'crop' => true ), false );
			$avatar_src = $thumb_obj[0];
		}

		$signature_src = !empty($signature) ? $signature['src'] : '';

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
			
			if ( !empty( $widget_title_icon ) ){
				echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
			}
			else if (!empty( $title )){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}

		echo "<div class='cws_textwidget_content'>";
			ob_start();
			?>
				<div class="about_me">
					<?php if (!empty($avatar_src)) { ?>
						<div class="user_avatar<?php echo ( (!empty($width) && $shape == 'round') && $width == $height ? ' round' : '');?><?php echo ( $border = '1' ? ' user_avatar_border' : '');?>">
						<?php if (!empty($link)) { ?> <a href="<?php echo esc_url($link) ?>"> <?php } ?>
							<img <?php echo ($grayscale_avatar == '1' ? ' class="grayscale" ': '') ?>src="<?php echo esc_url($avatar_src) ?>" alt="<?php echo (!empty($name) ? $name : '') ?>" />
						<?php if (!empty($link)) { ?> </a> <?php } ?>
						</div>
					<?php } ?>
					<?php echo (!empty($name) ? '<h4 class="user_name">'.$name.'</h4>' : '') ?>
					<?php echo (!empty($position) ? '<h5 class="user_position">'.$position.'</h5>' : '') ?>
					<?php if( (!empty($name) || !empty($position)) && (!empty($description) || !empty($signature_src)) ) { $divider = "custom_divider"; } ?>
					<?php echo (!empty($description) ? '<p class="user_description '.$divider.'">'.esc_html($description).'</p>' : '') ?>
					<?php if (!empty($signature_src)) { ?>
						<div class="user_signature">
							<img src="<?php echo esc_url($signature_src) ?>" alt="Signature" />
						</div>
					<?php } ?>
				</div>
			<?php
			echo ob_get_clean();
		echo "</div>";
		
		echo sprintf('%s',$after_widget);	
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			if ($v == 'on') {
				$v = '1';
			}
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>