<?php
	/**
	 * CWS Banner Widget Class
	 */
	
class CWS_Banner extends WP_Widget {
	public $fields = array();
	public function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				'value' => '',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'img_bg' => array(
				'title' => esc_html__( 'Banner Background Image', 'cryptop' ),
				'addrowclasses' => 'wide_picture',
				'type' => 'media',
			),
			'content_align' => array(
				'title' => esc_html__( 'Content align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),				
			'banner_title' => array(
				'title' => esc_html__( 'Banner title', 'cryptop' ),
				'type' => 'textarea',
				'atts' => 'rows="2" placeholder="'.esc_html__('Enter title', 'cryptop').'"',
				'value' => '',
			),
			'title_desc' => array(
				'title' => esc_html__( 'Title Description', 'cryptop' ),
				'type' => 'textarea',
				'atts' => 'rows="4" placeholder="'.esc_html__('Enter title description', 'cryptop').'"',
				'value' => '',
			),
			'text_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Text color', 'cryptop' ),
				'atts' => 'data-default-color="#000000"',
			),
			'add_overlay' => array(
				'title' => esc_html__( 'Add Overlay', 'cryptop' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'data-options="e:over_color;e:over_opacity;"',
			),
			'over_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Overlay color', 'cryptop' ),
				'atts' => 'data-default-color="#ffffff"',
			),
			'over_opacity' => array(
				'title' => esc_html__( 'Overlay Opacity', 'cryptop' ),
				'type' => 'number',
				'value' => '50',
			),
			'add_button' => array(
				'title' => esc_html__( 'Add Button', 'cryptop' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'data-options="e:button_title;e:button_url;e:new_tab;"',
			),
			'button_title' => array(
				'title' => esc_html__( 'Button Title', 'cryptop' ),
				'type' => 'text',
				'value' => '',
			),
			'button_url' => array(
				'title' => esc_html__( 'Button Url', 'cryptop' ),
				'type' => 'text',
				'value' => '',
			),
			'new_tab' => array(
				'title' => esc_html__( 'Open in New Tab', 'cryptop' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
			),
		);
	}
	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-banner', 'description' => esc_html__( 'Add information about yourself', 'cryptop' ) );
		parent::__construct( 'cws-banner', esc_html__( 'CWS Banner', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'img_bg' => '',
			'content_align' => 'left',
			'banner_title' => '',
			'title_desc' => '',
			'text_color' => '#000000',
			'add_overlay' => false,
			'over_color' => '#ffffff',
			'over_opacity' => '50',
			'add_button' => false,
			'button_title' => '',
			'button_url' => '',
			'new_tab' => false,
		), $instance));
		global $cws_theme_funcs;

		$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

		$title = esc_html($title);

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';

		$button_atts = array(
			'title'				=> $button_title,
			'url'				=> $button_url,
			'new_tab'			=> $new_tab,
			'size'				=> 'regular',
			'font_color'		=> '#fff',
			'font_color_hover'	=> $first_color,
			'background_color'	=> $first_color,
			'add_hover'			=> '1',
			'background_color_hover' => '#fff',
			'border_color'		=> $first_color,
			'border_color_hover' => $first_color,
			'button_type'		 => 'general',
			'border_radius'		=> '4px',
		);
		// $button = $add_button ? cws_sc_button( $button_atts ) : "";
		$button = '';
		$img_bg_id = !empty($img_bg) ? $img_bg['id'] : '';
		$thumb_obj = cws_thumb( $img_bg_id, array( 'width' => 270, 'height' => 270, 'crop' => true ), false );
		$img_bg_src = $thumb_obj[0];
		$over_opacity = (int)$over_opacity / 100;
		$overlay_style = (bool)$add_overlay ? 'style="background:'.(!empty($over_color) ? $over_color : 'trannasparent').';opacity:'.(!empty($over_opacity) ? $over_opacity : '0').';"' : '';

		$image_style = !empty($img_bg_src) ? 'style="background-image:url('.esc_url($img_bg_src).');"' : '';

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);

			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title)  . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title)  . esc_html($title) . $after_title;
			}
			//Title

			echo "<div class='cws_widget_banner' $image_style>";
					echo "<div class='banner_wrapper_overlay' $overlay_style></div><div class='banner_wrapper a-".esc_attr($content_align)."'>";
						echo "<div class='banner_content' style='color:$text_color'>";
							echo !empty( $banner_title ) ? "<h3 class='banner_title'>$banner_title</h3>" : "";
							echo !empty( $title_desc ) ? "<span class='banner_desc'>$title_desc</span>" : "";
						echo "</div>";
						echo "<div class='banner_button'>".($add_button ? $button : '')."</div>";
					echo "</div>";
			echo "</div>";
		
		echo sprintf('%s',$after_widget);	
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			if ($v == 'on') {
				$v = '1';
			}
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>