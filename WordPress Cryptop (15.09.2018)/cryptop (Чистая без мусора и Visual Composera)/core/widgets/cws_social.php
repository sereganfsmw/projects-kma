<?php
	/**
	 * CWS Social Widget Class
	 */

class CWS_Social extends WP_Widget {
	public $fields = array();
	public function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				'value' => '',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),
			'social' => array(
				'type' => 'group',
				'addrowclasses' => 'group sortable box',
				'title' => esc_html__('Social Networks', 'cryptop' ),
				'button_title' => esc_html__('Add new social network', 'cryptop' ),
				'button_icon' => 'fa fa-plus',
				'layout' => array(
					'title' => array(
						'type' => 'text',
						'atts' => 'data-role="title"',
						'title' => esc_html__('Social account title', 'cryptop' ),
					),
					'icon' => array(
						'type' => 'select',
						'source' => 'fa',
						'title' => esc_html__('Icon for this social contact', 'cryptop' )
					),										
					'url' => array(
						'type' => 'text',
						'title' => esc_html__('Url to your account', 'cryptop' ),
					),
					'color'	=> array(
						'title'	=> esc_html__( 'Icon color', 'cryptop' ),
						'atts' => 'data-default-color="#ffffff"',
						'value' => '#ffffff',
						'type'	=> 'text',
					),
					'bg_color'	=> array(
						'title'	=> esc_html__( 'Background color', 'cryptop' ),
						'atts' => 'data-default-color="' . CRYPTOP_HELPER_COLOR . '"',
						'value' => CRYPTOP_HELPER_COLOR,
						'type'	=> 'text',
					),			
					'hover_color'	=> array(
						'title'	=> esc_html__( 'Icon color (Hover)', 'cryptop' ),
						'atts' => 'data-default-color="#ffffff"',
						'value' => '#ffffff',											
						'type'	=> 'text',
					),
					'hover_bg_color'	=> array(
						'title'	=> esc_html__( 'Background color (Hover)', 'cryptop' ),
						'atts' => 'data-default-color="' . CRYPTOP_SECONDARY_COLOR . '"',
						'value' => CRYPTOP_SECONDARY_COLOR,
						'type'	=> 'text',
					),
				)
			),
			'social_icons_shape_type' => array(
				'title' => esc_html__( 'Shape', 'cryptop' ),
				'type' => 'radio',
				'addrowclasses' => 'box',
				'value' => array(
					'none' => array( esc_html__( 'None', 'cryptop' ), 	true, ''),
					'squared' => array( esc_html__( 'Squared', 'cryptop' ), 	false, ''),
					'round' =>array( esc_html__( 'Round', 'cryptop' ), false, ''),
				),
			),
			'social_icons_size' => array(
				'type' => 'select',
				'title' => esc_html__( 'Size', 'cryptop' ),
				'addrowclasses' => 'box',
				'source' => array(
					'1x' => array(esc_html__( 'Mini', 'cryptop' ), false),
					'lg' => array(esc_html__( 'Small', 'cryptop' ), false),
					'2x' => array(esc_html__( 'Medium', 'cryptop' ), true),
					'3x' => array(esc_html__( 'Big', 'cryptop' ), false),
				)
			),
			'social_icons_alignment' => array(
				'type' => 'select',
				'title' => esc_html__( 'Alignment', 'cryptop' ),
				'addrowclasses' => 'box',
				'source' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), false),
					'center' => array( esc_html__( 'Center', 'cryptop' ), true),
					'right' => array( esc_html__( 'Right', 'cryptop' ), false),
				)
			),
		);
	}

	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-social', 'description' => esc_html__( 'Select custom social icons', 'cryptop' ) );
		parent::__construct( 'cws-social', esc_html__( 'CWS Social icons', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'social' => '',
			'social_icons_shape_type' => '',
			'social_icons_size' => '',
			'social_icons_alignment' => '',
		), $instance));
		global $cws_theme_funcs;

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;

		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';

		$title = esc_html($title);

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
			
			if ( !empty( $widget_title_icon ) ){
				echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
			}
			else if (!empty( $title )){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}

		echo "<div class='cws_socialwidget_content'>";

			if(!empty( $social )) {
				ob_start();
				?>
					<div class="cws_social_links position-<?php echo esc_attr($social_icons_alignment) ?> size-<?php echo esc_attr($social_icons_size) ?>">
						<?php foreach ($social as $key => $value) {
							$icon_id = uniqid( "cws_social_icon_" );				
							?>
							<a id="<?php echo esc_attr($icon_id) ?>" href="<?php echo esc_url($value['url']) ?>" class="cws_social_link <?php echo esc_attr($social_icons_shape_type) ?>" title="<?php echo esc_html($value['title']) ?>" target="_blank">
								<i class="social_icon cws_fa <?php echo esc_attr($value['icon']) ?> <?php echo esc_attr((!empty($social_icons_size) ? 'fa-'.$social_icons_size : '')) ?> simple_icon"></i>
							</a>
						<?php

							$social_icons_styles .=
								"
								.cws-widget .cws_social_links #".esc_attr($icon_id)." .social_icon
								{
									".(!empty($value['color']) ? "color:".esc_attr($value['color']).";" : '')."
								}
							";

							if ($social_icons_shape_type != 'none'){
								$social_icons_styles .=
									"
									.cws-widget .cws_social_links #".esc_attr($icon_id).":hover .social_icon
									{
										".(!empty($value['hover_color']) ? "color:".esc_attr($value['hover_color']).";" : '')."
									}
								";
							}

							$social_icons_styles .=
								"
								.cws-widget .cws_social_links #".esc_attr($icon_id)."
								{
									".(!empty($value['bg_color']) ? "background-color:".esc_attr($value['bg_color']).";" : '')."
								}

								.cws-widget .cws_social_links #".esc_attr($icon_id).":before
								{
									".(!empty($value['hover_bg_color']) ? "background-color:".esc_attr($value['hover_bg_color']).";" : '')."
								}		
							";
					} 
			
					Cws_shortcode_css()->enqueue_cws_css($social_icons_styles);

					?>
					</div>
				<?php
				echo ob_get_clean();
			} else {
				echo "<div class='cws_textwidget_content'>No Social Icons found.</div>";
			}

		echo "</div>";

	echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			if ($v == 'on') {
				$v = '1';
			}
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>