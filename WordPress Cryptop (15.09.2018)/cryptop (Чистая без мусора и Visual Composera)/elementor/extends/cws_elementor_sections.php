<?php

namespace Elementor;

class CWS_Elementor_Sections{

	public $widget;

	public function __construct($widget)
	{
		$this->widget = $widget;
	}

	//category
	//post_tag
	//post_format
	public function get_taxonomy(
		$tab = array(
			'Taxonomy' => array(
				'category' => 'Categories'
			)
		)
	)
	{	
		$controls = $this->widget;
		foreach ($tab as $key => $value) {

			$tab_name = strtolower(str_replace(" ", "_", $key));

			//Tab
			$controls->start_controls_section(
				'section_'.$tab_name,
				[
					'label' => esc_html__( $key, 'cryptop' ),
					'tab' => Controls_Manager::TAB_CONTENT,
				]
			);	

			//Fields
			foreach ($value as $taxonomy => $field_caption) {
				$terms = get_terms( $taxonomy ); //Get taxonomy
				$avail_terms = array();
				if ( !is_a( $terms, 'WP_Error' ) ){
					foreach ( $terms as $term ) {
						$avail_terms[$term->name] = $term->slug; //Fill terms array
					}
				}

				$taxonomy_select =
				[
					'label' => esc_html__( $field_caption, 'cryptop' ),
					'type' => Controls_Manager::SELECT2,
					'multiple' => true,
					'options' => $avail_terms
				];

				$controls->add_control(
					$taxonomy.'_list' , $taxonomy_select
				);	
			}

			//-Tab
			$controls->end_controls_section();		

		}
	}

	public function icons($cws_svg = true, $defaults = array('icon_lib' => 'fontawesome', 'icon_fontawesome' => '', 'icon_flaticons' => '', 'icon_svg' => ''))
	{
		$controls = $this->widget;

		$controls->start_controls_section(
			'section_icons',
			[
				'label' => esc_html__( 'Icons', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);	

			$icon_lib =
			[
				'label' => esc_html__( 'Icon library', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'fontawesome' => esc_html__( 'Font Awesome', 'cryptop' ),
					'flaticons' => esc_html__( 'CWS Flaticons', 'cryptop' ),
				]
			];

			if (!empty($defaults['icon_lib'])){
				$icon_lib['default'] = $defaults['icon_lib'];
			}

			if ($cws_svg){
				$icon_lib['options']['svg'] = esc_html__( 'CWS SVG Icons', 'cryptop' );
			}

			$controls->add_control(
				'icon_lib', $icon_lib
			);

			$icon_fontawesome = 
			[
				'label' => esc_html__( 'Font Awesome', 'cryptop' ),
				'type' => Controls_Manager::ICON,
				'condition' => [
					'icon_lib' => 'fontawesome'
				]
			];	

			if (!empty($defaults['icon_fontawesome'])){
				$icon_fontawesome['default'] = $defaults['icon_fontawesome'];
			}

			$controls->add_control(
				'icon_fontawesome', $icon_fontawesome
			);

			//Fill array with icons
			$ficons = cws_get_all_flaticon_icons();
			$fl_icons = [];
			$fl_icons_include = [];

			foreach ($ficons as $key => $value) {
				$fl_icons['flaticon-'.$value] = $value;
				$fl_icons_include[] = 'flaticon-'.$value;
			}

			$icon_flaticons = 
			[
				'label' => esc_html__( 'CWS Flaticons', 'cryptop' ),
				'type' => Controls_Manager::ICON,
				'options' => $fl_icons,
				'include' => $fl_icons_include,			
				'condition' => [
					'icon_lib' => 'flaticons'
				],
			];	

			if (!empty($defaults['icon_flaticons'])){
				$icon_flaticons['default'] = $defaults['icon_flaticons'];
			}

			$controls->add_control(
				'icon_flaticons', $icon_flaticons
			);	

			if ($cws_svg){

				$icon_svg = 
				[
					'label' => esc_html__( 'CWS SVG Icons', 'cryptop' ),
					'type' => 'CWS_SVG',
					'condition' => [
						'icon_lib' => 'svg'
					],	
				];	

				if (!empty($defaults['icon_svg'])){
					$icon_svg['default'] = $defaults['icon_svg'];
				}

				$controls->add_control(
					'icon_svg', $icon_svg
				);
			}	
			
		$controls->end_controls_section();	
	}

	public function carousel()
	{
		$controls = $this->widget;

		$animation_in_arr = [
			'bounceIn' => 'BounceIn',
			'bounceInDown' => 'BounceInDown',
			'bounceInLeft' => 'BounceInLeft',
			'bounceInRight' => 'BounceInRight',
			'bounceInUp' => 'BounceInUp',
			'fadeIn' => 'FadeIn',
			'fadeInDown' => 'FadeInDown',
			'fadeInDownBig' => 'FadeInDownBig',
			'fadeInLeft' => 'FadeInLeft',
			'fadeInLeftBig' => 'FadeInLeftBig',
			'fadeInRight' => 'FadeInRight',
			'fadeInRightBig' => 'FadeInRightBig',
			'fadeInUp' => 'FadeInUp',
			'fadeInUpBig' => 'FadeInUpBig',
			'flipInX' => 'FlipInX',
			'flipInY' => 'FlipInY',
			'lightSpeedIn' => 'LightSpeedIn',
			'rotateIn' => 'RotateIn',
			'rotateInDownLeft' => 'RotateInDownLeft',
			'rotateInDownRight' => 'RotateInDownRight',
			'rotateInUpLeft' => 'RotateInUpLeft',
			'rotateInUpRight' => 'RotateInUpRight',
			'jackInTheBox' => 'JackInTheBox',
			'rollIn' => 'RollIn',
			'zoomIn' => 'ZoomIn',
			'zoomInDown' => 'ZoomInDown',
			'zoomInLeft' => 'ZoomInLeft',
			'zoomInRight' => 'ZoomInRight',
			'zoomInUp' => 'ZoomInUp',
			'slideInDown' => 'SlideInDown',
			'slideInLeft' => 'SlideInLeft',
			'slideInRight' => 'SlideInRight',
			'slideInUp' => 'SlideInUp'
		];

		$animation_out_arr = [
			'bounceOut' => 'BounceOut',
			'bounceOutDown' => 'BounceOutDown',
			'bounceOutLeft' => 'BounceOutLeft',
			'bounceOutRight' => 'BounceOutRight',
			'bounceOutUp' => 'BounceOutUp',
			'fadeOut' => 'FadeOut',
			'fadeOutDown' => 'FadeOutDown',
			'fadeOutDownBig' => 'FadeOutDownBig',
			'fadeOutLeft' => 'FadeOutLeft',
			'fadeOutLeftBig' => 'FadeOutLeftBig',
			'fadeOutRight' => 'FadeOutRight',
			'fadeOutRightBig' => 'FadeOutRightBig',
			'fadeOutUp' => 'FadeOutUp',
			'fadeOutUpBig' => 'FadeOutUpBig',
			'flipOutX' => 'FlipOutX',
			'flipOutY' => 'FlipOutY',
			'lightSpeedOut' => 'LightSpeedOut',
			'rotateOut' => 'RotateOut',
			'rotateOutDownLeft' => 'RotateOutDownLeft',
			'rotateOutDownRight' => 'RotateOutDownRight',
			'rotateOutUpLeft' => 'RotateOutUpLeft',
			'rotateOutUpRight' => 'RotateOutUpRight',
			'rollOut' => 'RollOut',
			'zoomOut' => 'ZoomOut',
			'zoomOutDown' => 'ZoomOutDown',
			'zoomOutLeft' => 'ZoomOutLeft',
			'zoomOutRight' => 'ZoomOutRight',
			'zoomOutUp' => 'ZoomOutUp',
			'slideOutDown' => 'SlideOutDown',
			'slideOutLeft' => 'SlideOutLeft',
			'slideOutRight' => 'SlideOutRight',
			'slideOutUp' => 'SlideOutUp'
		];

		$controls->start_controls_section(
			'section_carousel',
			[
				'label' => esc_html__( 'Carousel', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
				'condition' => [
					'use_carousel' => 'yes'
				],				
			]
		);	

			$slides_to_show = range( 1, 4 );
			$slides_to_show = array_combine( $slides_to_show, $slides_to_show );

			$controls->add_responsive_control(
				'slides_to_show',
				[
					'label' => esc_html__( 'Slides to Show', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '1',
					'options' => $slides_to_show,
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'navigation',
				[
					'label' => esc_html__( 'Navigation', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'both',
					'options' => [
						'both' => esc_html__( 'Arrows and Dots', 'cryptop' ),
						'arrows' => esc_html__( 'Arrows', 'cryptop' ),
						'dots' => esc_html__( 'Dots', 'cryptop' ),
						'none' => esc_html__( 'None', 'cryptop' ),
					],
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'auto_height',
				[
					'label' => esc_html__( 'Auto Height', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',				
				]
			);

			$controls->add_control(
				'item_margins',
				[
					'label' => esc_html__( 'Item margins', 'cryptop' ),
					"description"	=> esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px', ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 10,
					],
				]
			);

			$controls->add_control(
				'item_stage_paddings',
				[
					'label' => esc_html__( 'Stage paddings', 'cryptop' ),
					"description"	=> esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px', ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 0,
					],
				]
			);			

			$controls->add_control(
				'item_center',
				[
					'label' => esc_html__( 'Center item', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',				
				]
			);	

			$controls->add_control(
				'autoplay',
				[
					'label' => esc_html__( 'Autoplay', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',				
				]
			);	

			$controls->add_control(
				'autoplay_speed',
				[
					'label' => esc_html__( 'Autoplay speed', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 100,
					'max' => 10000,
					'step' => 1000,
					'default' => 3000,
					'condition' => [
						'autoplay' => 'yes'
					],
				]
			);

			$controls->add_control(
				'pause_on_hover',
				[
					'label' => esc_html__( 'Pause on Hover', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'condition' => [
						'autoplay' => 'yes'
					],			
				]
			);	

			$controls->add_control(
				'infinite',
				[
					'label' => esc_html__( 'Infinite Loop', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',				
				]
			);	

			$controls->add_control(
				'animation_in',
				[
					'label' => esc_html__( 'Animation (In)', 'cryptop' ),
					'type' => Controls_Manager::SELECT2,
					'default' => 'default',
					'options' => [
						'default' => esc_html__( 'Default', 'cryptop' ),
					] + $animation_in_arr,
					'condition' => [
						'slides_to_show' => '1',
					],
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'animation_out',
				[
					'label' => esc_html__( 'Animation (Out)', 'cryptop' ),
					'type' => Controls_Manager::SELECT2,
					'default' => 'default',
					'options' => [
						'default' => esc_html__( 'Default', 'cryptop' ),
					] + $animation_out_arr,
					'condition' => [
						'slides_to_show' => '1',
					],
					'frontend_available' => true,
				]
			);			

			$controls->add_control(
				'animation_speed',
				[
					'label' => esc_html__( 'Animation Speed', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'default' => 250,
					'frontend_available' => true,
				]
			);

		$controls->end_controls_section();	

		$controls->start_controls_section(
			'section_style_navigation',
			[
				'label' => esc_html__( 'Navigation', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'use_carousel',
									'operator' => '==',
									'value' => 'yes'
								]
							]
						], [
							'relation' => 'or',
							'terms' => [
								[
									'name' => 'navigation',
									'operator' => '==',
									'value' => 'arrows'
								], [
									'name' => 'navigation',
									'operator' => '==',
									'value' => 'dots'
								], [
									'name' => 'navigation',
									'operator' => '==',
									'value' => 'both'
								]
							]
						]
					]
				]				
			]
		);

			$controls->add_control(
				'heading_style_arrows',
				[
					'label' => esc_html__( 'Arrows', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$controls->add_control(
				'arrows_position',
				[
					'label' => esc_html__( 'Position', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'inside',
					'options' => [
						'inside' => esc_html__( 'Inside', 'cryptop' ),
						'outside' => esc_html__( 'Outside', 'cryptop' ),
					],
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$controls->add_control(
				'arrows_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 20,
							'max' => 60,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-prev:before, {{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-next:before' => 'font-size: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$controls->add_control(
				'arrows_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-prev:before, {{WRAPPER}} .elementor-image-carousel-wrapper .slick-slider .slick-next:before' => 'color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$controls->add_control(
				'heading_style_dots',
				[
					'label' => esc_html__( 'Dots', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'dots', 'both' ],
					],
				]
			);

			$controls->add_control(
				'dots_position',
				[
					'label' => esc_html__( 'Position', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'outside',
					'options' => [
						'outside' => esc_html__( 'Outside', 'cryptop' ),
						'inside' => esc_html__( 'Inside', 'cryptop' ),
					],
					'condition' => [
						'navigation' => [ 'dots', 'both' ],
					],
				]
			);

			$controls->add_control(
				'dots_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 5,
							'max' => 10,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-image-carousel-wrapper .elementor-image-carousel .slick-dots li button:before' => 'font-size: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'navigation' => [ 'dots', 'both' ],
					],
				]
			);

			$controls->add_control(
				'dots_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .elementor-image-carousel-wrapper .elementor-image-carousel .slick-dots li button:before' => 'color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'dots', 'both' ],
					],
				]
			);

		$controls->end_controls_section();

	}

	public function advanced_carousel($condition)
	{
		$controls = $this->widget;

		// Section Carousel
		$controls->start_controls_section(
			'section_carousel',
			[
				'label' => esc_html__( 'Carousel settings', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'condition' => $condition,				
			]
		);

			$slides_view = range( 1, 4 );
			$slides_view = array_combine( $slides_view, $slides_view );

			$controls->add_responsive_control(
				'columns_count', //slidesPerView
				[
					'label' => esc_html__( 'Columns to Show', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '1',
					'options' => [
						'auto' => esc_html__( 'Auto', 'cryptop' ),
					] + $slides_view,
					'frontend_available' => true,
				]
			);

			$slides_in_columns = range( 1, 4 );
			$slides_in_columns = array_combine( $slides_in_columns, $slides_in_columns );

			$controls->add_responsive_control(
				'slides_in_columns', //slidesPerColumn
				[
					'label' => esc_html__( 'Slides in 1 column', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '1',
					'options' => $slides_in_columns,
					'frontend_available' => true,
					'condition' => [
						'columns_count!' => 'auto',
					],						
				]
			);		

			$controls->add_control(
				'slides_view', //slidesPerColumnFill
				[
					'label' => esc_html__( 'Slides view', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'column',
					'options' => [
						'column' => esc_html__( 'Column', 'cryptop' ),
						'row' => esc_html__( 'Row', 'cryptop' ),
					],
					'condition' => [
						'columns_count!' => 'auto',
					],								
					'frontend_available' => true,
				]
			);

			$slides_scroll = range( 1, 4 );
			$slides_scroll = array_combine( $slides_scroll, $slides_scroll );

			$controls->add_responsive_control(
				'slides_to_scroll', //slidesPerGroup
				[
					'label' => esc_html__( 'Slides to scroll', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '1',
					'options' => $slides_scroll,
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'slider_direction', //direction
				[
					'label' => esc_html__( 'Slider direction', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'horizontal',
					'options' => [
						'horizontal' => esc_html__( 'Horizontal', 'cryptop' ),
						'vertical' => esc_html__( 'Vertical', 'cryptop' ),
					],
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'auto_height', //autoHeight
				[
					'label' => esc_html__( 'Auto height', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'condition' => [
						'slider_direction!' => 'vertical'
					],						
				]
			);	

			$controls->add_control(
				'navigation', //navigation, pagination
				[
					'label' => esc_html__( 'Navigation', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'both',
					'options' => [
						'both' => esc_html__( 'Arrows and Pagintaion', 'cryptop' ),
						'arrows' => esc_html__( 'Arrows', 'cryptop' ),
						'pagintaion' => esc_html__( 'Pagintaion', 'cryptop' ),
						'none' => esc_html__( 'None', 'cryptop' ),
					],
					'frontend_available' => true,
				]
			);


			$controls->add_control(
				'pagination_style', //pagination.type
				[
					'label' => esc_html__( 'Pagintaion style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'bullets',
					'options' => [
						'bullets' => esc_html__( 'Bullets', 'cryptop' ), //pagination clickable=true
						'fraction' => esc_html__( 'Numbers', 'cryptop' ),
						'progressbar' => esc_html__( 'Progress bar', 'cryptop' ),
						'scrollbar' => esc_html__( 'Scrollbar bar', 'cryptop' ), //scrollbar draggable=true
					],
					'condition' => [
						'navigation' => ['both', 'pagintaion']
					],				
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'dynamic_bullets', //dynamicBullets
				[
					'label' => esc_html__( 'Dynamic Bullets', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'condition' => [
						'navigation' => ['both', 'pagintaion'],
						'pagination_style' => 'bullets',
					],
				]
			);

			$controls->add_control(
				'keyboard_control', //keyboard.enabled
				[
					'label' => esc_html__( 'Keyboard Control', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',		
				]
			);	

			$controls->add_control(
				'mousewheel_control', //mousewheel=true
				[
					'label' => esc_html__( 'Mousewheel Control', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',		
				]
			);				

			$controls->add_control(
				'item_center', //centeredSlides
				[
					'label' => esc_html__( 'Center item', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'condition' => [
						'columns_count!' => ['auto','1'],
					],					
				]
			);

			$controls->add_control(
				'spacing_slides', //spaceBetween
				[
					'label' => esc_html__( 'Spacings between slides', 'cryptop' ),
					"description"	=> esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],					
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 0,
					],
				]
			);

			$controls->add_control(
				'free_mode', //freeMode
				[
					'label' => esc_html__( 'Free Move mode', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',		
				]
			);	

			$controls->add_control(
				'slide_effects', //effect
				[
					'label' => esc_html__( 'Slide effects', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'slide',
					'options' => [
						'slide' => esc_html__( 'Slide', 'cryptop' ),
						'fade' => esc_html__( 'Fade', 'cryptop' ),
						'cube' => esc_html__( 'Cube', 'cryptop' ),
						'coverflow' => esc_html__( 'Coverflow', 'cryptop' ),
						'flip' => esc_html__( 'Flip', 'cryptop' ),
					],
					'frontend_available' => true,
				]
			);

			$controls->add_control(
				'autoplay', //autoplay
				[
					'label' => esc_html__( 'Autoplay', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',				
				]
			);	

			$controls->add_control(
				'autoplay_speed', //delay
				[
					'label' => esc_html__( 'Autoplay speed', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 100,
					'max' => 10000,
					'step' => 1000,
					'default' => 3000,
					'condition' => [
						'autoplay' => 'yes'
					],
				]
			);

			$controls->add_control(
				'pause_on_hover', //autoplay.start() or autoplay.stop()
				[
					'label' => esc_html__( 'Pause on Hover', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
					'condition' => [
						'autoplay' => 'yes'
					],			
				]
			);

			$controls->add_control(
				'parallax', //parallax
				[
					'label' => esc_html__( 'Parallax', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

			$controls->add_control(
				'loop', //loop
				[
					'label' => esc_html__( 'Infinite Loop', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);	

			$controls->add_control(
				'animation_speed', //speed
				[
					'label' => esc_html__( 'Animation Speed', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'default' => 300,
					'frontend_available' => true,
				]
			);

		$controls->end_controls_section();

		//Get condition from params
		$condition_key = array_keys($condition)[0];
		$condition_val = array_values($condition)[0];

		$controls->start_controls_section(
			'section_style_navigation',
			[
				'label' => esc_html__( 'Carousel style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'relation' => 'and',
							'terms' => [
								[
									'name' => $condition_key,
									'operator' => '==',
									'value' => $condition_val
								]
							]
						], [
							'relation' => 'or',
							'terms' => [
								[
									'name' => 'navigation',
									'operator' => '==',
									'value' => 'arrows'
								], [
									'name' => 'navigation',
									'operator' => '==',
									'value' => 'pagintaion'
								], [
									'name' => 'navigation',
									'operator' => '==',
									'value' => 'both'
								]
							]
						]
					]
				]				
			]
		);

			//Arrows
			$controls->add_control(
				'heading_style_arrows',
				[
					'label' => esc_html__( 'Arrows', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$controls->add_control(
				'arrows_position',
				[
					'label' => esc_html__( 'Position', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'outside',
					'options' => [
						'inside' => esc_html__( 'Inside', 'cryptop' ),
						'outside' => esc_html__( 'Outside', 'cryptop' ),
					],
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
					'prefix_class' => 'arrow-position-',
				]
			);

			$controls->add_control(
				'arrows_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 20,
							'max' => 100,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 70,
					],					
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-button-prev, {{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-button-next' => 'font-size: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}; width: calc({{SIZE}}{{UNIT}} / 2);',
					],
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$controls->add_control(
				'arrows_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-button-prev, {{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-button-next' => 'color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			//Bullets
			$controls->add_control(
				'heading_style_bullets',
				[
					'label' => esc_html__( 'Bullets', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'bullets' ],
					],
				]
			);

			$controls->add_control(
				'bullets_squared',
				[
					'label' => esc_html__( 'Squared bullets', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'prefix_class' => 'bullets-',
					'return_value' => 'squared',
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'bullets' ],
					],					
				]
			);

			$controls->add_control(
				'bullets_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 1,
							'max' => 10,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 6,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-pagination .swiper-pagination-bullet' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'bullets' ],
					],
				]
			);

			$controls->add_control(
				'bullets_spacings',
				[
					'label' => esc_html__( 'Spacings', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 1,
							'max' => 10,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 6,
					],					
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-container-horizontal>.swiper-pagination-bullets .swiper-pagination-bullet' => 'margin: 0 {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'bullets' ],
					],
				]
			);			

			$controls->add_control(
				'bullets_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-pagination .swiper-pagination-bullet' => 'background-color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'bullets' ],
					],
				]
			);

			//Numbers
			$controls->add_control(
				'heading_style_fraction',
				[
					'label' => esc_html__( 'Numbers', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'fraction' ],
					],
				]
			);

			$controls->add_control(
				'fraction_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-pagination-fraction' => 'color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'fraction' ],
					],
				]
			);

			//Progress bar
			$controls->add_control(
				'heading_style_progressbar',
				[
					'label' => esc_html__( 'Progress bar', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'progressbar' ],
					],
				]
			);

			$controls->add_control(
				'progressbar_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 1,
							'max' => 10,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 4,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-pagination.swiper-pagination-progressbar' => 'height: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'progressbar' ],
					],
				]
			);

			$controls->add_control(
				'progressbar_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-pagination.swiper-pagination-progressbar .swiper-pagination-progressbar-fill' => 'background-color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'progressbar' ],
					],
				]
			);

			//Scrollbar bar
			$controls->add_control(
				'heading_style_scrollbar',
				[
					'label' => esc_html__( 'Scrollbar bar', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'scrollbar' ],
					],
				]
			);

			$controls->add_control(
				'scrollbar_squared',
				[
					'label' => esc_html__( 'Squared Scrollbar', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'prefix_class' => 'scrollbar-',
					'return_value' => 'squared',
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'scrollbar' ],
					],				
				]
			);

			$controls->add_control(
				'scrollbar_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 1,
							'max' => 10,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 5,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-container-horizontal>.swiper-scrollbar' => 'height: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'scrollbar' ],
					],
				]
			);

			$controls->add_control(
				'scrollbar_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_blog_wrapper .cws_blog_carousel .swiper-scrollbar .swiper-scrollbar-drag' => 'background-color: {{VALUE}};',
					],
					'condition' => [
						'navigation' => [ 'pagintaion', 'both' ],
						'pagination_style' => [ 'scrollbar' ],
					],
				]
			);						
		$controls->end_controls_section();			
	}
}