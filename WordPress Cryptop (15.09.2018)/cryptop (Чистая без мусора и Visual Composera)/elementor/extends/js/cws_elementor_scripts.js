"use strict";
/**********************************
******* CWS ELEMENTOR JS HOOK *****
**********************************/

console.log('789: cws_elementor_script.js');

//Check if Frontend init
jQuery(window).on('elementor/frontend/init', function(){

	//Check if elements render (Each elements Render)
	elementorFrontend.hooks.addAction( 'frontend/element_ready/global', function( $scope  ) {
		// console.info($scope);
	});

	// ------------------------ELEMENTS INIT------------------------
 	//CWS Section (tabs, accordion, carousel, parallax) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/section', function( $scope ) {

		var section_controls = '.elementor-editor-column-settings .elementor-editor-element-duplicate, .elementor-editor-column-settings .elementor-editor-element-add, .elementor-editor-column-settings .elementor-editor-element-remove';

		//Init tabs section
		var section_tabs = $scope.hasClass('elementor-section-tabs');
		if (section_tabs){

			function unwrap_tabs(element){
				element.css('opacity', '0.1');
				if (element.hasClass('cws_tabs_init')){
					var tabs = element.find('.tabs_content .elementor-column');
					tabs.each( function (i, el){
						jQuery(el).unwrap('.tab_item');
					});	
					tabs.unwrap('.tabs_content');
					
					element.find('.tabs_navigation').remove();	
					element.removeClass('cws_tabs_init');			
				}
			}

			function wrap_tabs(element, args){
				element.css('opacity', '0.5');
				var tabs_items = element.children('.elementor-column');

				//Tabs navigation
				element.prepend( "<ul class='tabs_navigation'></ul>" );

				//Tabs content
				tabs_items.each( function (i, el){
					jQuery(el).parent().find('.tabs_navigation').append("<li data-open_tab='tab-"+i+"' class='tab_title'>"+(typeof args.titles[i] !== 'undefined' ? args.titles[i]['title'] : 'Tab #'+(i+1))+"</li>");
					jQuery(el).wrap( "<div data-tab='tab-"+i+"' "+(typeof args.titles[i] !== 'undefined' ? "id='"+args.titles[i]['_id']+"'" : '')+" class='tab_item'></div>" );
				});
				element.find('.tab_item').wrapAll("<div class='tabs_content'></div>");

				element.addClass('cws_tabs_init');
			}

			function reinit_tabs(element, args){
				wrap_tabs(element, args);

				var tabs_navigation = element.find('.tabs_navigation');
				var tabs_content = element.find('.tabs_content');

				//Event
				tabs_navigation.on('click', "li.tab_title:not(.active)", function(){
					jQuery(this).closest('.tabs_navigation').find('.tab_title').removeClass('active');
					jQuery(this).addClass('active');
					var tab = jQuery(this).data('open_tab');
					var content_all = tabs_content.find('.tab_item').removeClass('active');
					var content_this = tabs_content.find('[data-tab="'+tab+'"]').addClass('active');

					if (args.animation == 'none'){
						content_all.hide();
					} else if (args.animation == 'fade'){
						content_all.fadeOut(400);
					} else if (args.animation == 'slide'){
						content_all.slideUp(400);
					}

					if (args.animation == 'none'){
						content_this.show();
					}

					setTimeout(function() {				
						if (args.animation == 'fade'){
							content_this.fadeIn();
						} else if (args.animation == 'slide'){
							content_this.slideDown();
						}						
					}, 400);
				});

				//Set active tab
				if(args.activeTab !== null && typeof args.activeTab !== "object"){
					tabs_navigation.find('li.tab_title').eq(args.activeTab -1).addClass('active');
					tabs_content.find('.tab_item').eq(args.activeTab -1).addClass('active');
				}				

				//SlideUp tabs content (Fix for Elementor)
				setTimeout(function(){
					tabs_content.find('.tab_item:not(.active)').hide();
					element.css('opacity', '1');
				}, 3000);

				//Auto Height
				setTimeout(function(){
					if (args.autoHeight){
						var max_height = 0;
						tabs_content.find('.tab_item').each( function (i, el){
							max_height = jQuery(el).innerHeight() > max_height ? jQuery(el).innerHeight() : max_height;
						}).css('height', max_height+'px');
					}
				}, 100);

				//ReInit on controls click
				element.find(section_controls).on('click', function(e) {
					element.find(section_controls).off();
					unwrap_tabs(element);
					setTimeout(function(){
						reinit_tabs(element, args);
					}, 100);
				});
			}

			var tabs_container = $scope.find('.elementor-cws-tabs');
			var args = tabs_container.data('tabs_args');

			reinit_tabs(tabs_container, args);
		}

		//Init accordion section
		var section_accordion = $scope.hasClass('elementor-section-accordion');
		if (section_accordion){

			function unwrap_accordion(element){
				element.css('opacity', '0.1');
				if (element.hasClass('cws_accordion_init')){
					var accordion_lines = element.find('.accordion-item .accordion-content .elementor-column');

					accordion_lines.each( function (i, el){
						jQuery(el).unwrap('.accordion-content');
						jQuery(el).unwrap('.accordion-item');
					});

					element.find('.accordion-header').remove();
					element.removeClass('cws_accordion_init');
				}				
			}

			function wrap_accordion(element, args){
				var accordion_items = element.children('.elementor-column');

				accordion_items.each( function (i, el){
					jQuery(el).wrap( "<div "+(typeof args.titles[i] !== 'undefined' ? "id='"+args.titles[i]['_id']+"'" : '')+" class='accordion-item'><div class='accordion-content'></div></div>" );
					jQuery(el).closest('.accordion-item').prepend("<div class='accordion-header'>"+(typeof args.titles[i] !== 'undefined' ? args.titles[i]['title'] : 'Accordion #'+(i+1))+"</div>");
				});

				element.addClass('cws_accordion_init');
			}

			function reinit_accordion(element, args){

				wrap_accordion(element, args);			

				//Uncollapsible pointer
				if (args.mode == 'toggle' && !args.collapsible){
					element.addClass('uncollapsible');
				}

				//Open on mouse hover
				var accordion_event = args.mode == 'toggle' && args.openHover ? "mouseover" : "click";

				//Events
				element.find('.accordion-item').on(accordion_event, ".accordion-header", function(){
					if (args.mode == 'accordion'){
						jQuery(this).parent().toggleClass("active");
						jQuery(this).next().slideToggle();
					} else if (args.mode == 'toggle'){
						jQuery(this).closest('.elementor-cws-accordion').find('.accordion-item').not(jQuery(this).parent()).removeClass('active');
						jQuery(this).closest('.elementor-cws-accordion').find('.accordion-item').not(jQuery(this).parent()).find('.accordion-content').slideUp();						
						jQuery(this).parent().toggleClass('active');								
						if (args.collapsible){
							jQuery(this).next().slideToggle();
						} else {								
							jQuery(this).next().slideDown();
						}
					}
				});

				//Set active row
				if(args.activeRow !== null && typeof args.activeRow !== "object"){
					element.find('.accordion-item').eq(args.activeRow -1).addClass('active');
				}

				//Auto Height
				setTimeout(function(){
					if (args.mode == 'toggle' && args.autoHeight){
						var max_height = 0;
						element.find('.accordion-item .accordion-content').each( function (i, el){
							max_height = jQuery(el).innerHeight() > max_height ? jQuery(el).innerHeight() : max_height;
						}).css('height', max_height+'px');
					}
				}, 100);

				//SlideUp accordion (Fix for Elementor)
				setTimeout(function(){
					element.find('.accordion-item:not(.active) .accordion-content').slideUp();
					element.css('opacity', '1');
				}, 3000);	

				//ReInit on controls click
				element.find(section_controls).on('click', function(e) {
					element.find(section_controls).off();
					unwrap_accordion(element);
					setTimeout(function(){
						reinit_accordion(element, args);
					}, 100);
				});	
			}

			var accordion_container = $scope.find('.elementor-cws-accordion');
			var args = accordion_container.data('accordion_args');

			reinit_accordion(accordion_container, args);
		}

		//Init carousel section
		var section_carousel = $scope.hasClass('elementor-section-carousel');
		if (section_carousel){

			function reinit_carousel(row){
				row.find(section_controls).on('click', function(e) {
					row.find(section_controls).off();

					jQuery(row).css('opacity', '0.5');
					jQuery(row).slick('unslick');

					setTimeout(function(){
						jQuery(row).slick(args);
						jQuery(row).css('opacity', '1');
						reinit_carousel(row);
					}, 1000);

				});	
			}
		
			var container = $scope.children('.elementor-container');
			var row = container.children('.elementor-row');
			var args = container.data('carousel_args');

			jQuery(row).slick(args);


jQuery(row).on('afterChange', function(event, slick, currentSlide){
	console.warn(slick);
  	console.log(currentSlide);
  // console.log(currentSlide);
});

			jQuery(row).find('.slick-list').attr('tabindex', 0); //Focus on slider (Keyboard arrows control)

			reinit_carousel(row); //Add Events on columns add/remove
		}

		//Init parallax section
		var patterns = $scope.find('.elementor-background-patterns-cws');
		var parallax_section = patterns.find('.cws_parallax_section');
		cws_parallax_section_init(parallax_section);

	});	

	//CWS Blog (carousel, pagination, isotope) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_blog.default', function( $scope ) {

		var blog_carousel = jQuery($scope).find('.cws_blog_carousel');
		var args = blog_carousel.data('swiper-args');
		var carousel_container = blog_carousel.find('.swiper-container');

		if (blog_carousel.length != 0){
			//Navigation Arrows
			if (args.navigation){
				var prevEl = blog_carousel.find('.swiper-button-prev');
				var nextEl = blog_carousel.find('.swiper-button-next');
				args.navigation = new Object();
				args.navigation.prevEl = prevEl;
				args.navigation.nextEl = nextEl;
			}

			//Direction Vertical
			if (args.direction == 'vertical'){
				var article = blog_carousel.find('article');
				var max_height = 0;
				article.each( function (i, el){
					max_height = jQuery(el).innerHeight() > max_height ? jQuery(el).innerHeight() : max_height;
				});

				carousel_container.css('max-height', max_height+'px');
				args.height = max_height;			
			}

			//Init
			var swiper_carousel = new Swiper (carousel_container, args);

			//Autoplay on mouse hover
			if (args.autoplay && args.autoplayPause){
				blog_carousel.hover(
					//in
					function(event) {
						swiper_carousel.autoplay.stop();
					},
					//out
					function(event) {
						swiper_carousel.autoplay.start();
					}
				);
			}
		}

		//Pagination
		var section = jQuery($scope).find('.cws_blog');
		var wrapper = section.find('.cws_blog_wrapper');

		cws_isotope_init(wrapper);
		cws_pagination_init(section);
	});

	//CWS Portfolio (carousel, pagination, isotope, showcase) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_portfolio.default', function( $scope ) {

		var portfolio_carousel = jQuery($scope).find('.cws_portfolio_carousel');
		var args = portfolio_carousel.data('swiper-args');
		var carousel_container = portfolio_carousel.find('.swiper-container');

		if (portfolio_carousel.length != 0){
			//Navigation Arrows
			if (args.navigation){
				var prevEl = portfolio_carousel.find('.swiper-button-prev');
				var nextEl = portfolio_carousel.find('.swiper-button-next');
				args.navigation = new Object();
				args.navigation.prevEl = prevEl;
				args.navigation.nextEl = nextEl;
			}

			//Direction Vertical
			if (args.direction == 'vertical'){
				var article = portfolio_carousel.find('article');
				var max_height = 0;
				article.each( function (i, el){
					max_height = jQuery(el).innerHeight() > max_height ? jQuery(el).innerHeight() : max_height;
				});

				carousel_container.css('max-height', max_height+'px');
				args.height = max_height;			
			}

			//Init Carousel
			var swiper_carousel = new Swiper (carousel_container, args);

			//Autoplay on mouse hover
			if (args.autoplay && args.autoplayPause){
				portfolio_carousel.hover(
					//in
					function(event) {
						swiper_carousel.autoplay.stop();
					},
					//out
					function(event) {
						swiper_carousel.autoplay.start();
					}
				);
			}
		}
		
		var section = jQuery($scope).find('.cws_portfolio');
		var wrapper = section.find('.cws_portfolio_wrapper');
		var portfolio_showcase = section.hasClass('showcase');

		if (portfolio_showcase){
			cws_portfolio_showcase(section); //Init Showcase
		}

		cws_isotope_init(wrapper); //Init Isotope
		cws_pagination_init(section); //Init Pagination
	});

	//CWS Staff (carousel, pagination, isotope) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_staff.default', function( $scope ) {

		var staff_carousel = jQuery($scope).find('.cws_staff_carousel');
		var args = staff_carousel.data('swiper-args');
		var carousel_container = staff_carousel.find('.swiper-container');

		if (staff_carousel.length != 0){
			//Navigation Arrows
			if (args.navigation){
				var prevEl = staff_carousel.find('.swiper-button-prev');
				var nextEl = staff_carousel.find('.swiper-button-next');
				args.navigation = new Object();
				args.navigation.prevEl = prevEl;
				args.navigation.nextEl = nextEl;
			}

			//Direction Vertical
			if (args.direction == 'vertical'){
				var article = staff_carousel.find('article');
				var max_height = 0;
				article.each( function (i, el){
					max_height = jQuery(el).innerHeight() > max_height ? jQuery(el).innerHeight() : max_height;
				});

				carousel_container.css('max-height', max_height+'px');
				args.height = max_height;			
			}

			//Init Carousel
			var swiper_carousel = new Swiper (carousel_container, args);

			//Autoplay on mouse hover
			if (args.autoplay && args.autoplayPause){
				staff_carousel.hover(
					//in
					function(event) {
						swiper_carousel.autoplay.stop();
					},
					//out
					function(event) {
						swiper_carousel.autoplay.start();
					}
				);
			}
		}
		
		var section = jQuery($scope).find('.cws_staff');
		var wrapper = section.find('.cws_staff_wrapper');

		cws_isotope_init(wrapper); //Init Isotope
		cws_pagination_init(section); //Init Pagination
	});

	//CWS Pie chart (progress circle) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_pie_chart.default', function( $scope ) {
		var pie_char_circle = jQuery($scope).find('.cws_pie_char_circle');
		pie_char_circle.cws_pie_chart();
	});

	//CWS Categories (owlCarousel) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_categories.default', function( $scope ) {
		var categories = jQuery($scope);
		cws_owl_carousel_init(categories)
	});

	//CWS Testimonial (owlCarousel) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_testimonial.default', function( $scope ) {
		var testimonial = jQuery($scope);
		cws_owl_carousel_init(testimonial)
	});

	//CWS Shortcode (owlCarousel) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/shortcode.default', function( $scope ) {
		var shortcode = jQuery($scope);
		cws_owl_carousel_init(shortcode)
	});	

	//CWS Milestone (numbers) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_milestone.default', function( $scope ) {
		var milestone = jQuery($scope).find('.cws_milestone');
		milestone.cws_milestone();
	});

	//CWS Tips (dots) init
	elementorFrontend.hooks.addAction( 'frontend/element_ready/cws_tips.default', function( $scope ) {

		var hotspots = jQuery($scope).find('.cws-hotspots'),
			hotspotsItem = hotspots.find('.hotspot-item.click_trigger');

		hotspotsItem.on('click', function(e) {
			var thisIs = jQuery(this);

			if(thisIs.hasClass('active')){

			} else {
				thisIs.addClass('active');
				e.preventDefault();
				return false;
			}
		});

	});

 	//Check if Columns render
	elementorFrontend.hooks.addAction( 'frontend/element_ready/column', function( $scope ) {
		// var columns_wrap = jQuery($scope).find('.elementor-column-wrap');
	});

});