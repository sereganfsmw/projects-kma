<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


require_once ELEMENTOR_PATH . '/includes/controls/groups/image-size.php';
//CWS Image
class CWS_Elementor_Testimonial extends Widget_Base {
	public function get_name() {
		return 'cws_testimonial';
	}

	public function get_title() {
		return esc_html__( 'CWS Testimonial', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-testimonial';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		//JS enqueue scripts
		wp_enqueue_script( 'isotope' );
		wp_enqueue_script( 'owl_carousel' );
		
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_testimonial',
			[
				'label' => esc_html__( 'Testimonial', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$repeater = new Repeater();
			$repeater->start_controls_tabs( 'tabs_button_style');

				$repeater->add_control(
					'image',
					[
						'label' => esc_html__( 'Choose Image', 'cryptop' ),
						'type' => Controls_Manager::MEDIA,
						'dynamic' => [
							'active' => true,
						],
					]
				);

				$repeater->add_group_control(
					Group_Control_Image_Size::get_type(),
					[
						'name' => 'image', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
						'default' => 'large',
						'separator' => 'none',
					]
				);

				$repeater->add_control(
					'author_name',
					[
					  'label' => esc_html__( 'Author name', 'cryptop' ),
					  'type'  => Controls_Manager::TEXT
					]
				);

				$repeater->add_control(
					'author_quote',
					[
						'label' => esc_html__( 'Author quote', 'cryptop' ),
						'rows' => 10,
						'type' => Controls_Manager::TEXTAREA,
						'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
					]
				);

				$repeater->add_control(
					'author_link',
					[
						'label' => esc_html__( 'Link to', 'cryptop' ),
						'type' => Controls_Manager::URL,
						'dynamic' => [
							'active' => true,
						],
						'placeholder' => esc_html__( 'https://your-link.com', 'cryptop' ),
						'show_label' => false,
					]
				);

			$controls->add_control(
				'testimonials',
				[
					'label'       => esc_html__( 'Testimonials', 'cryptop' ),
					'type'        => Controls_Manager::REPEATER,
					'show_label'  => true,
					'fields'      => array_values( $repeater->get_controls() ),
					'title_field' => '{{{author_name}}}',
				]
			);

			$controls->add_control(
				'columns',
				[
					'label' => esc_html__( 'Columns', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '1',
					'options' => [
						'1' => esc_html__( 'One Column', 'cryptop' ),
						'2' => esc_html__( 'Two Columns', 'cryptop' ),
						'3' => esc_html__( 'Three Columns', 'cryptop' ),
						'4' => esc_html__( 'Four Columns', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'use_carousel',
				[
					'label' => esc_html__( 'Use carousel', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

		$controls->end_controls_section();

		$sections->carousel();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Testimonial', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'testimonials_style',
				[
					'label' => esc_html__( 'Testimonials style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'small',
					'prefix_class' => 'testimonials-style-',
					'options' => [
						'small' => esc_html__( 'Small images', 'cryptop' ),
						'large' => esc_html__( 'Large images', 'cryptop' ),
					],
				]
			);

			$controls->add_responsive_control(
				'title_align',
				[
					'label' => esc_html__( 'Title Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,					
					'options' => [
						'left' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .content_wrapper .title_wrapper' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_responsive_control(
				'quote_align',
				[
					'label' => esc_html__( 'Quote Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,					
					'options' => [
						'left' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .content_wrapper .quote_wrapper' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_responsive_control(
				'thumbnail_align',
				[
					'label' => esc_html__( 'Thumbnail Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,
					'prefix_class' => 'thumbnail-',
					'options' => [
						'left' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
/*					'selectors' => [
						'{{WRAPPER}}' => 'text-align: {{VALUE}};',
					],*/
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .content_wrapper .title_wrapper',
					'label'	=> 'Title typography',
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'quote_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .content_wrapper .quote_wrapper',
					'label'	=> 'Quote typography',
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_testimonial', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
/*	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_testimonial', $this, 'js'));
	}*/

	public function get_link_url( $settings ) {
		if ( 'none' === $settings['link_to'] ) {
			return false;
		}

		if ( 'custom' === $settings['link_to'] ) {
			if ( empty( $settings['link']['url'] ) ) {
				return false;
			}
			return $settings['link'];
		}

		return [
			'url' => $settings['image']['url'],
		];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Testimonial() );