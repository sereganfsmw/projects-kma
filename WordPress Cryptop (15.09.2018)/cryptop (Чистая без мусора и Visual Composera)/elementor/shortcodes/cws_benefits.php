<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Benefits
class CWS_Elementor_Benefits extends Widget_Base {
	public function get_name() {
		return 'cws_benefits';
	}

	public function get_title() {
		return esc_html__( 'CWS Benefits', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-bookmark';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Add extra section
		$sections->icons();

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
					'default' => esc_html__( 'Instant Currency Exchange', 'cryptop' ),
					'label_block' => true,
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .benefits_title',
					'label'	=> 'Title Typography',
					'condition' => [
						'title!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 24,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 24,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 20,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'divider',
				[
					'label' => esc_html__( 'Divider', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'label_block' => true,
					'condition' => [
						'title!' => '',
					],
				]
			);

			$controls->add_control(
				'description',
				[
					'label' => esc_html__( 'Description', 'cryptop' ),
					'rows' => 10,
					'type' => Controls_Manager::TEXTAREA,
					'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
					'default' => esc_html__( 'Nullam lobortis porta justo, id dictum tellus pharetra vitae. Aenean vestibulum odio in nunc dictum elementum. 
						Sed pharetra arcu in nunc tempus, nec vulputate dui molestie. Integer vehicula risus dui, in malesuada nibh', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .benefits_description',
					'label'	=> 'Description Typography',
					'condition' => [
						'description!' => ''
					],
					'exclude' => ['html_tag'],
					'defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 14,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'add_button',
				[
					'label' => esc_html__( 'Add Button', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'button_text',
				[
					'label' => esc_html__( 'Button Text', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Read More', 'cryptop' ),
					'condition' => [
						'add_button' => 'yes'
					],	
				]
			);

			$controls->add_control(
				'button_url',
				[
					'label' => esc_html__( 'Url', 'cryptop' ),
					'type' => Controls_Manager::URL,
					'placeholder' => esc_html__( 'http://', 'cryptop' ),
					'show_external' => true,
					'default' => [
						'url' => '#',
						'is_external' => true,
						'nofollow' => true,
					],
					'condition' => [
						'add_button' => ['yes']
					],
				]
			);

			$controls->add_responsive_control(
				'text_aligning',
				[
					'label' => esc_html__( 'Text Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'center',
					'toggle' => false,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-widget-container' => 'text-align: {{VALUE}};',
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_responsive_control(
				'icon_margins',
				[
					'label' => esc_html__( 'Icon Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 20,
						'left' => 0,
						'isLinked' => false
					],
					'tablet_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 20,
						'left' => 0,
						'isLinked' => false
					],
					'mobile_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 10,
						'left' => 0,
						'isLinked' => false
					],
					'selectors' => [
						'{{WRAPPER}} .benefits_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'conditions' => [
						'relation' => 'or',
						'terms' => [
							[
								'name' => 'icon_fontawesome',
								'operator' => '!=',
								'value' => ''
							], [
								'name' => 'icon_flaticons',
								'operator' => '!=',
								'value' => ''
							], [
								'name' => 'icon_svg',
								'operator' => '!=',
								'value' => ''
							]
						]
					],
				]
			);

			$controls->add_responsive_control(
				'divider_margins',
				[
					'label' => esc_html__( 'Divider Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .benefits_text_wrapper .benefits_divider' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'unit' => 'px',
						'top' => 15,
						'right' => 0,
						'bottom' => 25,
						'left' => 0,
						'isLinked' => false
					],
					'tablet_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
						'isLinked' => false
					],
					'mobile_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
						'isLinked' => false
					],
					'condition' => [
						'divider' => 'yes'
					],
				]
			);

			$controls->add_responsive_control(
				'button_margins',
				[
					'label' => esc_html__( 'Button Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .benefits_button_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'unit' => 'px',
						'top' => 100,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
						'isLinked' => false
					],
					'tablet_default' => [
						'unit' => 'px',
						'top' => 15,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
						'isLinked' => false
					],
					'mobile_default' => [
						'unit' => 'px',
						'top' => 10,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
						'isLinked' => false
					],
					'condition' => [
						'add_button' => 'yes'
					],
				]
			);

			$controls->add_control(
				'customize_colors',
				[
					'label' => esc_html__( 'Customize Colors', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			/*------------------------TITLE POPOVER------------------------*/
			$controls->add_control(
				'title_popover',
				[
					'label' => esc_html__( 'Title Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'title',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'title_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#ffffff',
						'default' => '#ffffff',
						'selectors' => [
							'{{WRAPPER}} .benefits_title' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'title_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000000',
						'default' => '#000000',
						'selectors' => [
							'{{WRAPPER}}:hover .benefits_title' => 'color: {{VALUE}}',
						],			
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			$controls->add_control(
				'divider_color',
				[
					'label' => esc_html__( 'Divider Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $theme_colors_first_color,
					'default' => $theme_colors_first_color,
					'selectors' => [
						'{{WRAPPER}} .benefits_divider i' => 'background-color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'divider',
								'operator' => '==',
								'value' => 'yes',
							],
						],
					],	
				]
			);

			$controls->add_control(
				'description_color',
				[
					'label' => esc_html__( 'Description Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#000000',
					'default' => '#000000',
					'selectors' => [
						'{{WRAPPER}} .benefits_description' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							]
						],
					],			
				]
			);

			$controls->add_control(
				'icon_border_color',
				[
					'label' => esc_html__( 'Icon Border Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $theme_colors_first_color,
					'default' => $theme_colors_first_color,
					'selectors' => [
						'{{WRAPPER}} .benefits_icon_wrapper' => 'border-color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							]
						]
					],	
				]
			);

			$controls->add_control(
				'customize_button',
				[
					'label' => esc_html__( 'Customize Button', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'customize_icon',
				[
					'label' => esc_html__( 'Customize Icon', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_custom_button',
			[
				'label' => esc_html__( 'Custom button', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'customize_button' => ['yes']
				],				
			]
		);

			$controls->start_controls_tabs( 'tabs_button_style');

				$controls->start_controls_tab(
					'tab_button_normal',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),						
					]
				);

					$controls->add_control(
						'button_color',
						[
							'label' => esc_html__( 'Text Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => '#ffffff',
							'default' => '#ffffff',
							'selectors' => [
								'{{WRAPPER}} .benefits_button_wrapper .button' => 'color: {{VALUE}}',
							],					
						]
					);

					$controls->add_control(
						'button_background_color',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => $theme_colors_first_color,
							'default' => $theme_colors_first_color,
							'selectors' => [
								'{{WRAPPER}} .benefits_button_wrapper .button' => 'background-color: {{VALUE}}',
							],					
						]
					);

				$controls->end_controls_tab();

				$controls->start_controls_tab(
					'tab_button_hover',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),						
					]
				);

					$controls->add_control(
						'button_color_hover',
						[
							'label' => esc_html__( 'Text Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => $theme_colors_first_color,
							'default' => $theme_colors_first_color,
							'selectors' => [
								'{{WRAPPER}} .benefits_button_wrapper .button:hover' => 'color: {{VALUE}}',
							],				
						]
					);



					$controls->add_control(
						'button_background_color_hover',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => '#ffffff',
							'default' => '#ffffff',
							'selectors' => [
								'{{WRAPPER}} .benefits_button_wrapper .button:hover' => 'background-color: {{VALUE}}',
							],				
						]
					);

				$controls->end_controls_tab();

			$controls->end_controls_tabs();

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_custom_icon',
			[
				'label' => esc_html__( 'Custom icon', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'customize_icon' => ['yes']
				],				
			]
		);

			$controls->add_control(
				'custom_icon_size',
				[
					'label' => esc_html__( 'Icon size', 'cryptop' ),
					'description' => esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 120,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 20,
					],
					'selectors' => [
						'{{WRAPPER}} .benefits_icon_wrapper .benefits_icon' => 'font-size: {{SIZE}}{{UNIT}};',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_lib',
										'operator' => '!=',
										'value' => 'svg'
									]
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					],
				]
			);

			$controls->add_control(
				'custom_icon_wrapper_size',
				[
					'label' => esc_html__( 'Icon Wrapper size', 'cryptop' ),
					'description' => esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 150,
							'max' => 200,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 80,
					],
					'selectors' => [
						'{{WRAPPER}} .benefits_icon_wrapper' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}}; line-height: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$controls->start_controls_tabs( 'tabs_icons_style');

				$controls->start_controls_tab(
					'tab_icons_normal',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),						
					]
				);

					$controls->add_control(
						'background_color',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#fff',
							'value' => '#fff',
							'selectors' => [
								'{{WRAPPER}} .benefits_icon_wrapper' => 'background-color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'icon_color',
						[
							'label' => esc_html__( 'Icon color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => $theme_colors_first_color,
							'default' => $theme_colors_first_color,
							'selectors' => [
								'{{WRAPPER}} .benefits_icon_wrapper .benefits_icon' => 'color: {{VALUE}}',
								'{{WRAPPER}} .benefits_icon_wrapper i svg' => 'fill: {{VALUE}}',
							],				
						]
					);				

				$controls->end_controls_tab();

				$controls->start_controls_tab(
					'tab_icons_hover',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),						
					]
				);

					$controls->add_control(
						'background_color_hover',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#fff',
							'value' => '#fff',
							'selectors' => [
								'{{WRAPPER}}:hover .benefits_icon_wrapper' => 'background-color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'icon_color_hover',
						[
							'label' => esc_html__( 'Icon color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => $theme_colors_first_color,
							'default' => $theme_colors_first_color,
							'selectors' => [
								'{{WRAPPER}}:hover .benefits_icon_wrapper .benefits_icon' => 'color: {{VALUE}}',
								'{{WRAPPER}}:hover .benefits_icon_wrapper i svg' => 'fill: {{VALUE}}',
							],				
						]
					);					

					$controls->add_control(
						'hover_animation',
						[
							'label' => esc_html__( 'Hover Animation', 'cryptop' ),
							'type' => Controls_Manager::HOVER_ANIMATION,
						]
					);

				$controls->end_controls_tab();

			$controls->end_controls_tabs();

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;

		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_benefits', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {}

	protected function _get_initial_config() {

		global $cws_theme_funcs;

		$config = [
			'widget_type' => $this->get_name(),
			'keywords' => $this->get_keywords(),
			'categories' => $this->get_categories(),
		];

		$out = array_merge( parent::_get_initial_config(), $config );

		//Default paddings for cws-benefits
		$out['controls']['_padding']['default'] = [
			'unit' => 'px',
			'top' => '70',
			'right' => '35',
			'bottom' => '70',
			'left' => '35',
			'isLinked' => false
		];

		return $out;
	}
	
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Benefits() );