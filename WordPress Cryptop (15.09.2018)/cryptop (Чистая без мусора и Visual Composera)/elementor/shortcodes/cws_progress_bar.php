<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Progress Bar
class CWS_Elementor_Progress_bar extends Widget_Base {
	public function get_name() {
		return 'progress';
	}

	public function get_title() {
		return esc_html__( 'CWS Progress Bar', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-skill-bar';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_progress',
			[
				'label' => esc_html__( 'Progress Bar', 'cryptop' ),
			]
		);

		$controls->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
				'default' => esc_html__( 'My Skill', 'cryptop' ),
				'label_block' => true,
			]
		);

		$controls->add_control(
			'progress_type',
			[
				'label' => esc_html__( 'Type', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => esc_html__( 'Default', 'cryptop' ),
					'info' => esc_html__( 'Info', 'cryptop' ),
					'success' => esc_html__( 'Success', 'cryptop' ),
					'warning' => esc_html__( 'Warning', 'cryptop' ),
					'danger' => esc_html__( 'Danger', 'cryptop' ),
				],
			]
		);

		$controls->add_control(
			'percent',
			[
				'label' => esc_html__( 'Percentage', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 50,
					'unit' => '%',
				],
				'label_block' => true,
			]
		);

		$controls->add_control(
			'display_percentage',
			[
				'label' => esc_html__( 'Display Percentage', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'show',
				'options' => [
					'show' => esc_html__( 'Show', 'cryptop' ),
					'hide' => esc_html__( 'Hide', 'cryptop' ),
				],
			]
		);

		$controls->add_control(
			'inner_text',
			[
				'label' => esc_html__( 'Inner Text', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => '',
				'default' => '',
				'label_block' => true,
			]
		);

		$controls->add_control(
			'view',
			[
				'label' => esc_html__( 'View', 'cryptop' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_progress_style',
			[
				'label' => esc_html__( 'Progress Bar', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_group_control(
		CWS_Group_Control_Gradient::get_type(),
		[
			'name' => 'gradient',
			'label' => esc_html__( 'Background gradient', 'cryptop' ),
			'types' => [ 'classic', 'gradient' ],
			'selector' => '{{WRAPPER}} .elementor-progress-bar',
			'defaults' => [
				'background' => 'gradient',
				'color' => $cws_theme_funcs->cws_Hex2RGBA($theme_colors_first_color,0.5),
				'color_stop' => [
					'unit' => '%',
					'size' => 0,
				],
				'color_b' => $theme_colors_first_color,
				'color_b_stop' => [
					'unit' => '%',
					'size' => 100,
				],
				'gradient_type' => 'linear',
				'gradient_angle' => [
					'unit' => 'deg',
					'size' => 90,
				],
				'gradient_position' => 'center center',
			],
		]
	);

		$controls->add_control(
			'bar_bg_color',
			[
				'label' => esc_html__( 'Background Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#E1E9F4',
				'selectors' => [
					'{{WRAPPER}} .elementor-progress-wrapper' => 'background-color: {{VALUE}};',
				],
			]
		);

		$controls->add_control(
			'bar_inline_color',
			[
				'label' => esc_html__( 'Inner Text Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-progress-bar' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_title',
			[
				'label' => esc_html__( 'Title Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Text Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#000',
				'selectors' => [
					'{{WRAPPER}} .elementor-title' => 'color: {{VALUE}};',
				],
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
			]
		);

		$controls->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'typography',
				'selector' => '{{WRAPPER}} .elementor-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_progress_bar', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_progress_bar', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Progress_bar() );