<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Embed
class CWS_Elementor_Embed extends Widget_Base {
	public function get_name() {
		return 'cws_embed';
	}

	public function get_title() {
		return esc_html__( 'CWS Embed', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-object-group';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'url',
				[
					'label' => esc_html__( 'Link', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter URL of embed object', 'cryptop' ),
					'label_block' => true,
				]
			);

			$controls->add_control(
				'width',
				[
					'label' => esc_html__( 'Width', 'cryptop' ),
					'description' => esc_html__( 'In pixels', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'min' => 100,
					'max' => 1920,
					'step' => 100,
					'default' => 1920,
				]
			);

			$controls->add_control(
				'height',
				[
					'label' => esc_html__( 'Height', 'cryptop' ),
					'description' => esc_html__( 'In pixels', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'min' => 100,
					'max' => 1080,
					'step' => 100,
					'default' => 1080,
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_embed', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {}
	
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Embed() );