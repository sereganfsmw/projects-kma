<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Categories
class CWS_Elementor_Categories extends Widget_Base {
	public function get_name() {
		return 'cws_categories';
	}

	public function get_title() {
		return esc_html__( 'CWS Categories', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-tags';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		//JS enqueue scripts
		wp_enqueue_script( 'isotope' );
		wp_enqueue_script( 'owl_carousel' );

		$controls = $this;

		global $cws_theme_funcs;

		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$tax_def = [
			'Taxonomy' => [ //Tab name
				'category' => 'Categories', //Taxonomy => Field caption
			]
		];

		//Add extra section
		$sections->get_taxonomy($tax_def);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'columns',
				[
					'label' => esc_html__( 'Columns', 'cryptop' ),
					'type' => Controls_Manager::SELECT,				
					'default' => '4',
					'options' => [
						'1' => esc_html__( 'One', 'cryptop' ),
						'2' => esc_html__( 'Two', 'cryptop' ),
						'3' => esc_html__( 'Three', 'cryptop' ),
						'4' => esc_html__( 'Four', 'cryptop' ),
					],
					'condition' => [
						'use_carousel!' => 'yes'
					],						
				]
			);

			$controls->add_control(
				'crop',
				[
					'label' => esc_html__( 'Square images', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

			$controls->add_control(
				'count',
				[
					'label' => esc_html__( 'Items Count', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 10,
					'step' => 1,
					'default' => 3
				]
			);

			$controls->add_control(
				'use_carousel',
				[
					'label' => esc_html__( 'Use carousel', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);		

		$controls->end_controls_section();

		$sections->carousel();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Label', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'text_color',
				[
					'label' => esc_html__( 'Text Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => $theme_colors_first_color,
					'selectors' => [
						'{{WRAPPER}} .item .category-label' => 'color:{{VALUE}}',
					],
				]
			);

			$controls->add_control(
				'background_color',
				[
					'label' => esc_html__( 'Background Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#fff',
					'selectors' => [
						'{{WRAPPER}} .item .category-label' => 'background-color: {{VALUE}};',
					],
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_categories', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {}
	
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Categories() );