<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Staff
class CWS_Elementor_Staff extends Widget_Base {
	public function get_name() {
		return 'cws_staff';
	}

	public function get_title() {
		return esc_html__( 'CWS Staff', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-person';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		global $cws_theme_funcs;
		
		$controls = $this;

		//JS scripts
		wp_enqueue_script( 'isotope' );
		wp_enqueue_script( 'imagesloaded' );
		wp_enqueue_script( 'fancybox' );
		wp_enqueue_script ('jquery-shortcode-velocity');
		wp_enqueue_script ('jquery-shortcode-velocity-ui');
		wp_enqueue_script( 'modernizr' );

		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Filter by (Get Taxonomies)
		$post_type = 'cws_staff';

		$taxes = get_object_taxonomies ( $post_type, 'object' );
		$avail_taxes = array(
			'' => esc_html__( 'None', 'cryptop' ),
		);
		foreach ( $taxes as $tax => $tax_obj ){
			$tax_name = isset( $tax_obj->labels->name ) && !empty( $tax_obj->labels->name ) ? $tax_obj->labels->name : $tax;
			$avail_taxes[$tax] = $tax_name;
		}

		$tax_controls = array();
		foreach ( $avail_taxes as $tax => $tax_name ) {
			if ($tax == '') continue;
			$terms = get_terms( $tax );
			$avail_terms = array();
			if ( !is_a( $terms, 'WP_Error' ) ){
				foreach ( $terms as $term ) {
					$avail_terms[$term->slug] = $term->name;
				}
			}

			$tax_controls[$tax] = [
				'label' => esc_html__( $tax_name, 'cryptop' ),
				'type' => Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $avail_terms,
				'condition' => [
					'filter_by' => $tax
				]				
			];		
		}
		//--Filter by (Get Taxonomies)

		$controls->start_controls_section(
			'section_staff_layout',
			[
				'label' => esc_html__( 'Staff', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
			]
		);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
					'default' => esc_html__( 'Staff', 'cryptop' ),
					'label_block' => true,
				]
			);

			$controls->add_control(
				'title_aligning',
				[
					'label' => esc_html__( 'Title Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .widgettitle' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'title_filter_row',
				[
					'label' => esc_html__( 'Title & filter style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'default',
					'options' => [
						'default' => esc_html__( 'Default ', 'cryptop' ),
						'stretch' => esc_html__( 'Stretch', 'cryptop' ),
					],
					'condition' => [
						'display_style' => ['filter', 'filter_with_ajax']
					],						
				]
			);

			$controls->add_control(
				'display_style',
				[
					'label' => esc_html__( 'Layout', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'grid',
					'options' => [
						'grid' => esc_html__( 'Grid', 'cryptop' ),
						'filter' => esc_html__( 'Grid with Filter ', 'cryptop' ),
						'filter_with_ajax' => esc_html__( 'Grid with Filter (Ajax)', 'cryptop' ),
						'carousel' => esc_html__( 'Carousel', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'layout',
				[
					'label' => esc_html__( 'Columns', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'def',
					'options' => [
						'def' => esc_html__( 'Default', 'cryptop' ),
						'1' => esc_html__( 'One Column', 'cryptop' ),
						'2' => esc_html__( 'Two Columns', 'cryptop' ),
						'3' => esc_html__( 'Three Columns', 'cryptop' ),
						'4' => esc_html__( 'Four Columns', 'cryptop' ),
						'5' => esc_html__( 'Five Columns', 'cryptop' ),
					],
					'condition' => [
						'display_style' => ['grid', 'filter', 'filter_with_ajax']
					],						
				]
			);

		$controls->end_controls_section();

		$sections->advanced_carousel(['display_style' => 'carousel']);

		$controls->start_controls_section(
			'section_staff_content',
			[
				'label' => esc_html__( 'Staff', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'content_aligning',
				[
					'label' => esc_html__( 'Content Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .post_info' => 'text-align: {{VALUE}};',
					],
				]
			);

			//Filter by controls
			//Output all filter taxes
			$controls->add_control(
				'filter_by',
				[
					'label' => esc_html__( 'Filter by', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '',
					'options' => $avail_taxes,
				]
			);

			//Output all filter terms
			if (!empty($tax_controls)){
				foreach ( $tax_controls as $control_name => $control_fields ) {
					$controls->add_control(
						$control_name, $control_fields
					);
				}
			}
			//--Filter by controls

			$controls->add_control(
				'items_count',
				[
					'label' => esc_html__( 'Items to display', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 100,
					'step' => 1,
					'default' => get_option( 'posts_per_page' ),
				]
			);

			$controls->add_control(
				'items_per_page',
				[
					'label' => esc_html__( 'Items per Page', 'cryptop' ),
					'description'	=> esc_html__( 'Works on Grid only', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 100,
					'step' => 1,
					'default' => get_option( 'posts_per_page' ),
					'condition' => [
						'display_style!' => ['carousel', 'filter'],
					],
				]
			);			

			$controls->add_control(
				'pagination_grid',
				[
					'label' => esc_html__( 'Pagination', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'multiple' => true,
					'default' => 'load_more',
					'options' => [
						'standard' =>  esc_html__( 'Standard', 'cryptop' ),
						'load_more' =>  esc_html__( 'Load More', 'cryptop' ),
						'load_on_scroll' =>  esc_html__( 'Load on Scroll', 'cryptop' ),
						'ajax' =>  esc_html__( 'Ajax', 'cryptop' ),
					],
					'condition' => [
						'display_style!' => ['carousel', 'filter'],
					],					
				]
			);

			$controls->add_control(
				'pagination_aligning',
				[
					'label' => esc_html__( 'Pagination Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'center',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .pagination' => 'text-align: {{VALUE}};',
						'{{WRAPPER}} .elementor-widget-container .load_more_wrapper' => 'text-align: {{VALUE}};',
					],			        
					'condition' => [
						'display_style!' => ['carousel', 'filter'],
						'pagination_grid!' => 'load_on_scroll'
					],						
				]
			);			

			$controls->add_control(
				'chars_count',
				[
					'label' => esc_html__( 'Content Character Limit', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 1000,
					'step' => 1,
					'default' => '',
				]
			);

			$controls->add_control(
				'show_meta',
				[
					'label' => esc_html__( 'Show meta', 'cryptop' ),
					'type' => Controls_Manager::SELECT2,
					'multiple' => true,
					'default' => ['title', 'deps', 'excerpt', 'experience', 'email', 'biography', 'link_button'],
					'options' => [
						'title' =>  esc_html__( 'Title', 'cryptop' ),
						'deps' =>  esc_html__( 'Departments', 'cryptop' ),
						'poss' =>  esc_html__( 'Positions', 'cryptop' ),
						'excerpt' =>  esc_html__( 'Excerpt', 'cryptop' ),
						'experience' =>  esc_html__( 'Experience', 'cryptop' ),
						'email' =>  esc_html__( 'Email', 'cryptop' ),
						'biography' =>  esc_html__( 'Biography', 'cryptop' ),
						'link_button' =>  esc_html__( 'Link Button', 'cryptop' ),
						'socials' =>  esc_html__( 'Social Links', 'cryptop' ),
					],
				]
			);		

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_staff_style_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'grid_columns_spacings',
				[
					'label' => esc_html__( 'Columns spacings', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],					
					'default' => [
						'size' => 50,
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .cws_icon:before' => 'font-size: {{SIZE}}{{UNIT}}',
					],					
					'label_block' => true,
					'condition' => [
						'display_style!' => ['carousel']
					],						
				]
			);

			$controls->add_control(
				'grid_rows_spacings',
				[
					'label' => esc_html__( 'Rows spacings', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],					
					'default' => [
						'size' => 50,
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .cws_icon:before' => 'font-size: {{SIZE}}{{UNIT}}',
					],					
					'label_block' => true,
					'condition' => [
						'display_style!' => ['carousel']
					],						
				]
			);

			$controls->add_control(
				'view_style',
				[
					'label' => esc_html__( 'View style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'big',
					'options' => [
						'big' => esc_html__( 'Big', 'cryptop' ),
						'small' => esc_html__( 'Small', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'appear_style',
				[
					'label' => esc_html__( 'Appear style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'none' => esc_html__( 'None', 'cryptop' ),
						'callout.bounce' => esc_html__( 'Bounce', 'cryptop' ),
						'callout.shake' => esc_html__( 'Shake', 'cryptop' ),
						'callout.flash' => esc_html__( 'Flash', 'cryptop' ),
						'callout.pulse' => esc_html__( 'Pulse', 'cryptop' ),
						'callout.swing' => esc_html__( 'Swing', 'cryptop' ),
						'callout.tada' => esc_html__( 'Tada', 'cryptop' ),
						'transition.fadeIn' => esc_html__( 'Fade In', 'cryptop' ),
						'transition.flipXIn' => esc_html__( 'Flip X In', 'cryptop' ),
						'transition.flipYIn' => esc_html__( 'Flip Y In', 'cryptop' ),
						'transition.shrinkIn' => esc_html__( 'Shrink In', 'cryptop' ),
						'transition.expandIn' => esc_html__( 'Expand In', 'cryptop' ),
						'transition.grow' => esc_html__( 'Grow', 'cryptop' ),
						'transition.slideUpBigIn' => esc_html__( 'Slide Up', 'cryptop' ),
						'transition.slideDownBigIn' => esc_html__( 'Slide Down', 'cryptop' ),
						'transition.slideLeftBigIn' => esc_html__( 'Slide Left', 'cryptop' ),
						'transition.slideRightBigIn' => esc_html__( 'Slide Right', 'cryptop' ),
						'transition.perspectiveUpIn' => esc_html__( 'Perspective Up', 'cryptop' ),
						'transition.perspectiveDownIn' => esc_html__( 'Perspective Down', 'cryptop' ),
						'transition.perspectiveLeftIn' => esc_html__( 'Perspective Left', 'cryptop' ),
						'transition.perspectiveRightIn' => esc_html__( 'Perspective Right', 'cryptop' ),
					],
					'condition' => [
						'display_style' => ['carousel', 'grid', 'filter', 'filter_with_ajax'],
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_staff_style_hover',
			[
				'label' => esc_html__( 'Hover', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'image_link',
				[
					'label' => esc_html__( 'Make Image Clickable', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'condition' => [
						'view_style' => 'big',
					],
				]
			);			

			$controls->add_group_control(
				CWS_Group_Control_Gradient::get_type(),
				[
					'name' => 'gradient',
					'label' => esc_html__( 'Background gradient', 'cryptop' ),
					'types' => [ 'classic', 'gradient' ],
					'selector' => '{{WRAPPER}} .blog_post .post_media .hover-effect',
					'fields_options' => [
						'background' => [
							'label' => _x( 'Hover overlay', 'Background Control', 'cryptop' ),
						],
					],					
					'defaults' => [
						'background' => 'classic',
						'color' => $cws_theme_funcs->cws_Hex2RGBA($theme_colors_first_color,0.5),
						'color_stop' => [
							'unit' => '%',
							'size' => 0,
						],
						'color_b' => $theme_colors_first_color,
						'color_b_stop' => [
							'unit' => '%',
							'size' => 100,
						],
						'gradient_type' => 'linear',
						'gradient_angle' => [
							'unit' => 'deg',
							'size' => 90,
						],
						'gradient_position' => 'center center',
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_staff_style_title',
			[
				'label' => esc_html__( 'Title', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Text Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#000',
					'selectors' => [
						'{{WRAPPER}} .widgettitle' => 'color: {{VALUE}};',
					],
					'scheme' => [
						'type' => Scheme_Color::get_type(),
						'value' => Scheme_Color::COLOR_1,
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .widgettitle',
					'scheme' => Scheme_Typography::TYPOGRAPHY_3,
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_staff_style_category',
			[
				'label' => esc_html__( 'Category', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'category_color',
				[
					'label' => esc_html__( 'Category Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#000',
					'selectors' => [
						'{{WRAPPER}} .widgettitle' => 'color: {{VALUE}};',
					],
					'scheme' => [
						'type' => Scheme_Color::get_type(),
						'value' => Scheme_Color::COLOR_1,
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'category_typography',
					'selector' => '{{WRAPPER}} .widgettitle',
					'scheme' => Scheme_Typography::TYPOGRAPHY_3,
				]
			);

		$controls->end_controls_section();		
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_staff', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
/*	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_staff', $this, 'js'));
	}*/
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Staff() );