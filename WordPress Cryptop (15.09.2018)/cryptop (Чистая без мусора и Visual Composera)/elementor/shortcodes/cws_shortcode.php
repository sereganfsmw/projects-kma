<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Shortcode
class CWS_Elementor_Shortcode extends Widget_Base {
	public function get_name() {
		return 'shortcode';
	}

	public function get_title() {
		return esc_html__( 'CWS Shortcode', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-shortcode';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {

		//JS enqueue scripts
		wp_enqueue_script( 'owl_carousel' );

		$controls = $this;

		global $cws_theme_funcs;

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'shortcode',
				[
					'label' => esc_html__( 'Enter your shortcode', 'cryptop' ),
					'type' => Controls_Manager::TEXTAREA,
					'dynamic' => [
						'active' => true,
					],
					'placeholder' => '[gallery id="123" size="medium"]',
					'default' => '',
				]
			);

			$controls->add_control(
				'use_carousel',
				[
					'label' => esc_html__( 'Use carousel', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

			$controls->add_control(
				'carousel_type',
				[
					'label' => esc_html__( 'Carousel type', 'cryptop' ),
					'type' => Controls_Manager::SELECT,				
					'default' => 'default',
					'options' => [
						'default' => esc_html__( 'Default', 'cryptop' ),
						'woo' => esc_html__( 'WooCommerce', 'cryptop' ),
						'custom' => esc_html__( 'Custom', 'cryptop' ),
					],
					'condition' => [
						'use_carousel' => 'yes'
					],						
				]
			);

			$controls->add_control(
				'carousel_selector',
				[
					'label' => esc_html__( 'Carousel selector', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( '.selector', 'cryptop' ),
					'title' => esc_html__( 'Selector', 'cryptop' ),
					'condition' => [
						'use_carousel' => 'yes',
						'carousel_type' => 'custom'
					],						
				]
			);

		$controls->end_controls_section();

		$sections->carousel();		
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_shortcode', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {}
	
	public function render_plain_content() {
		echo $this->get_settings( 'shortcode' );
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Shortcode() );