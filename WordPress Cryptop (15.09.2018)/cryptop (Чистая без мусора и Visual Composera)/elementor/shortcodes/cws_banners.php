<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Banners
class CWS_Elementor_Banners extends Widget_Base {
	public function get_name() {
		return 'cws_banners';
	}

	public function get_title() {
		return esc_html__( 'CWS Banners', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-address-card';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_responsive_control(
				'text_alignment',
				[
					'label' => esc_html__( 'Text Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'tablet_default' => 'left',
					'mobile_default' => 'center',
					'toggle' => false,
					'options' => [
						'left' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .cws_banners.cws_module .banners_body' => 'text-align: {{VALUE}};',
						'{{WRAPPER}} .cws_banners.cws_module .banner_button' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'add_divider',
				[
					'label' => esc_html__( 'Add Divider', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'add_button',
				[
					'label' => esc_html__( 'Add Button', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_responsive_control(
				'button_float',
				[
					'label' => esc_html__( 'Button Float', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'row',
					'tablet_default' => 'row',
					'mobile_default' => 'column',
					'toggle' => false,
					'options' => [
						'row-reverse'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'column' => [
							'title' => esc_html__( 'None', 'cryptop' ),
							'icon' => 'fa fa-times',
						],
						'row' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .cws_banners' => 'flex-direction: {{VALUE}}; -webkit-flex-direction: {{VALUE}}; -moz-flex-direction: {{VALUE}}; -ms-flex-direction: {{VALUE}};',
					],
					'condition' => [
						'add_button' => 'yes'
					],
					'render_type' => 'template',
				]
			);

			$controls->add_control(
				'button_text',
				[
					'label' => esc_html__( 'Button Text', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Read More', 'cryptop' ),
					'condition' => [
						'add_button' => 'yes'
					],	
				]
			);

			$controls->add_control(
				'button_url',
				[
					'label' => esc_html__( 'Url', 'cryptop' ),
					'type' => Controls_Manager::URL,
					'placeholder' => esc_html__( 'http://', 'cryptop' ),
					'show_external' => true,
					'default' => [
						'url' => '#',
						'is_external' => true,
						'nofollow' => true,
					],
					'condition' => [
						'add_button' => ['yes']
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_info',
			[
				'label' => esc_html__( 'Main Info', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'title_banners',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Maximum Efficiency', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_banners.cws_module .banners_body .banners_title_wrapper .banners_title',
					'label'	=> esc_html__( 'Title Typography', 'cryptop' ),
					'condition' => [
						'title_banners!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 50,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 40,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 32,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'desc_banners',
				[
					'label' => esc_html__( 'Title description', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Lorem ipsum dolor sit amet, consectetur elit sed eiusmod tempor incididunt ut labore dolore magna aliqua.', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_banners.cws_module .banners_body .banners_desc',
					'label'	=> esc_html__( 'Description Typography', 'cryptop' ),
					'condition' => [
						'desc_banners!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.7,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.6,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.4,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'content',
				[
					'label' => esc_html__( 'Content', 'cryptop' ),
					'type' => Controls_Manager::WYSIWYG,
					'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
				]
			);	

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'discount_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_banners.cws_module .banners_body .discount_price',
					'label'	=> esc_html__( 'Discount Typography', 'cryptop' ),
					'condition' => [
						'discount_banners!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 40,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.4,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],	
				]
			);

			$controls->add_control(
				'discount_banners',
				[
					'label' => esc_html__( 'Discount', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( '', 'cryptop' ),
				]
			);


		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'customize_colors',
				[
					'label' => esc_html__( 'Customize Colors', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'overlay_color',
				[
					'label' => esc_html__( 'Overlay Color on Hover', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $cws_theme_funcs->cws_Hex2RGBA($first_color, 0.5),
					'default' => $cws_theme_funcs->cws_Hex2RGBA($first_color, 0.5),
					'selectors' => [
						'{{WRAPPER}} .elementor-widget-container:before' => 'background-color: {{VALUE}}',
					],
					'condition' => [
						'customize_colors' => 'yes'
					],
				]
			);

			$controls->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Title Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#FFF',
					'default' => '#FFF',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banners_title' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'title_banners',
								'operator' => '!=',
								'value'	=> '',
							], [
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							],
						]
					],
				]
			);

			$controls->add_control(
				'divider_color',
				[
					'label' => esc_html__( 'Divider Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $cws_theme_funcs->cws_Hex2RGBA($first_color, 1),
					'default' => $cws_theme_funcs->cws_Hex2RGBA($first_color, 1),
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner-divider' => 'background-color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'add_divider',
								'operator' => '==',
								'value'	=> 'yes',
							], [
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							],
						]
					],
				]
			);

			$controls->add_control(
				'subtitle_color',
				[
					'label' => esc_html__( 'Subtitle Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => 'rgba(255,255,255, .8)',
					'default' => 'rgba(255,255,255, .8)',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banners_desc' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'desc_banners',
								'operator' => '!=',
								'value'	=> '',
							], [
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							],
						]
					],
				]
			);

			$controls->add_control(
				'content_color',
				[
					'label' => esc_html__( 'Content Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => 'rgba(255,255,255, 1)',
					'default' => 'rgba(255,255,255, 1)',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banners_body' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'content',
								'operator' => '!=',
								'value'	=> '',
							], [
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							],
						]
					],
				]
			);

			$controls->add_control(
				'discount_color',
				[
					'label' => esc_html__( 'Discount Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#ff0000',
					'default' => '#ff0000',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .discount_price' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'discount_banners',
								'operator' => '!=',
								'value'	=> '',
							], [
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							],
						]
					],
				]
			);

			$controls->add_control(
				'use_custom_button_color',
				[
					'label' => esc_html__( 'Customize Button', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'separator' => 'before',
					'condition' => [
						'add_button' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_button_color',
				[
					'label' => esc_html__( 'Font Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#fff',
					'default' => '#fff',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner_button a' => 'color: {{VALUE}};',
					],
					'condition' => [
						'use_custom_button_color' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_button_border',
				[
					'label' => esc_html__( 'Border Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $first_color,
					'default' => $first_color,
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner_button a' => 'border-color: {{VALUE}};',
					],
					'condition' => [
						'use_custom_button_color' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_button_background',
				[
					'label' => esc_html__( 'Background Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $first_color,
					'default' => $first_color,
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner_button a' => 'background-color: {{VALUE}};',
					],
					'condition' => [
						'use_custom_button_color' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_button_hover_color',
				[
					'label' => esc_html__( 'Font Color on Hover', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $first_color,
					'default' => $first_color,
					'separator' => 'before',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner_button a:hover' => 'color: {{VALUE}};',
					],
					'condition' => [
						'use_custom_button_color' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_button_hover_border',
				[
					'label' => esc_html__( 'Border Color on Hover', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#fff',
					'default' => '#fff',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner_button a:hover' => 'border-color: {{VALUE}};',
					],
					'condition' => [
						'use_custom_button_color' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_button_hover_background',
				[
					'label' => esc_html__( 'Background Color on Hover', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#fff',
					'default' => '#fff',
					'selectors' => [
						'{{WRAPPER}} .cws_banners .banner_button a:hover' => 'background-color: {{VALUE}};',
					],
					'condition' => [
						'use_custom_button_color' => 'yes'
					],
				]
			);


			

		$controls->end_controls_section();
		
	}

	protected function _get_initial_config() {
		$config = [
			'widget_type' => $this->get_name(),
			'keywords' => $this->get_keywords(),
			'categories' => $this->get_categories(),
		];

		$out = array_merge( parent::_get_initial_config(), $config );

		//Default background for cws-banner
		$out['controls']['_background_background']['default'] = 'classic';
		$out['controls']['_background_color']['default'] = '#0F1A27';
		$out['controls']['_background_color']['value'] = '#0F1A27';

		//Default paddings for cws-banner
		$out['controls']['_padding']['default'] = [
			'unit' => 'px',
			'top' => '50',
			'right' => '50',
			'bottom' => '50',
			'left' => '50',
			'isLinked' => false
		];

		//Default borders for cws-banner
		$out['controls']['_border_radius']['default'] = [
			'unit' => 'px',
			'top' => '5',
			'right' => '5',
			'bottom' => '5',
			'left' => '5',
			'isLinked' => false
		];

		return $out;
	}

	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_banners', $this, 'php'));
	}
	protected function content_template() {}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Banners() );