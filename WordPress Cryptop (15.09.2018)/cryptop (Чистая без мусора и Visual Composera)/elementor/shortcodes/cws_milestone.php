<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Milestone
class CWS_Elementor_Milestone extends Widget_Base {
	public function get_name() {
		return 'cws_milestone';
	}

	public function get_title() {
		return esc_html__( 'CWS Milestone', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-clock-o';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		
		//JS enqueue scripts
		wp_enqueue_script( 'odometer' );

		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$second_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['second_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Add extra section
		$sections->icons();

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'size',
				[
					'label' => esc_html__( 'Icon Size', 'cryptop' ),
					'type' => Controls_Manager::SELECT,				
					'default' => '3x',
					'options' => [
						'lg' => esc_html__( 'Mini', 'cryptop' ),
						'2x' => esc_html__( 'Small', 'cryptop' ),
						'3x' => esc_html__( 'Medium', 'cryptop' ),
						'4x' => esc_html__( 'Large', 'cryptop' ),
						'5x' => esc_html__( 'Extra Large', 'cryptop' ),
					],
					'conditions' => [
						'relation' => 'or',
						'terms' => [
							[
								'name' => 'icon_fontawesome',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'icon_flaticons',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'icon_svg',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);

			$controls->add_responsive_control(
				'icon_pos',
				[
					'label' => esc_html__( 'Icon Position', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'row',
					'tablet_default' => 'row',
					'mobile_default' => 'column',
			        'options' => [
			            'row'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'column' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'row-reverse' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'conditions' => [
						'relation' => 'or',
						'terms' => [
							[
								'name' => 'icon_fontawesome',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'icon_flaticons',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'icon_svg',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
					'selectors' => [
						'{{WRAPPER}} .cws_milestone .cws_milestone_wrapper' => 'flex-direction: {{VALUE}}; -webkit-flex-direction: {{VALUE}}; -moz-flex-direction: {{VALUE}}; -ms-flex-direction: {{VALUE}};',
					],
				]
			);

			$controls->add_responsive_control(
				'content_alignment',
				[
					'label' => esc_html__( 'Content Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'center',
					'tablet_default' => 'center',
					'mobile_default' => 'center',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .cws_milestone .milestone_wrapper' => 'text-align: {{VALUE}};',
					],
				]
			);

			$info_align_left = '
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;

				-webkit-flex-direction: row-reverse;
					-ms-flex-direction: row-reverse;
						flex-direction: row-reverse;
				
				-webkit-flex-wrap: nowrap;
					-ms-flex-wrap: nowrap;
						flex-wrap: nowrap;

				-webkit-justify-content: flex-end;
					-ms-justify-content: flex-end;
						justify-content: flex-end;

				-webkit-align-items: center;
					-ms-align-items: center;
						align-items: center;
			';
			$info_align_center = '
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;

				-webkit-flex-direction: column;
					-ms-flex-direction: column;
						flex-direction: column;
				
				-webkit-flex-wrap: nowrap;
					-ms-flex-wrap: nowrap;
						flex-wrap: nowrap;

				-webkit-justify-content: center;
					-ms-justify-content: center;
						justify-content: center;

				-webkit-align-items: center;
					-ms-align-items: center;
						align-items: center;
			';
			$info_align_right = '
				display: -webkit-flex;
				display: -ms-flexbox;
				display: flex;

				-webkit-flex-direction: row;
					-ms-flex-direction: row;
						flex-direction: row;
				
				-webkit-flex-wrap: nowrap;
					-ms-flex-wrap: nowrap;
						flex-wrap: nowrap;

				-webkit-justify-content: flex-start;
					-ms-justify-content: flex-start;
						justify-content: flex-start;

				-webkit-align-items: center;
					-ms-align-items: center;
						align-items: center;
			';
			$info_align_left = trim(preg_replace("/\r\n|\r|\s+|\n/", '', $info_align_left));
			$info_align_center = trim(preg_replace("/\r\n|\r|\s+|\n/", '', $info_align_center));
			$info_align_right = trim(preg_replace("/\r\n|\r|\s+|\n/", '', $info_align_right));


			$controls->add_responsive_control(
				'info_alignment',
				[
					'label' => esc_html__( 'Information Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => $info_align_right,
					'tablet_default' => $info_align_right,
					'mobile_default' => $info_align_center,
			        'options' => [
			            $info_align_left => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			                'align' => 'align-left',
			            ],
			            $info_align_center => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			                'align' => 'align-center',
			            ],
			            $info_align_right => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			                'align' => 'align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .cws_milestone .cws_milestone_wrapper .cws_milestone_data' => '{{VALUE}};',
					],
					'render_type' => 'template',
				]
			);

			$controls->add_control(
				'info_vert_alignment',
				[
					'label' => esc_html__( 'Information Vertical Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'flex-end',
			        'options' => [
			            'flex-start'    => [
			                'title' => esc_html__( 'Top', 'cryptop' ),
			                'icon' => 'fa fa-chevron-up',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-chevron-left',
			            ],
			            'flex-end' => [
			                'title' => esc_html__( 'Bottom', 'cryptop' ),
			                'icon' => 'fa fa-chevron-down',
			            ],
			        ],
			        'condition' => [
						'info_alignment!' => $info_align_center
					],
					'selectors' => [
						'{{WRAPPER}} .cws_milestone .cws_milestone_wrapper .cws_milestone_data' => '-webkit-align-items: {{VALUE}}; -moz-align-items: {{VALUE}}; -ms-align-items: {{VALUE}}; align-items: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Title', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_milestone_wrapper .cws_milestone_title',
					'label'	=> esc_html__( 'Title Typography', 'cryptop' ),
					'condition' => [
						'title!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 20,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 20,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'desc',
				[
					'label' => esc_html__( 'Description', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Description', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'desc_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_milestone_wrapper .cws_milestone_desc',
					'label'	=> esc_html__( 'Description Typography', 'cryptop' ),
					'condition' => [
						'desc!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'number',
				[
					'label' => esc_html__( 'Number', 'cryptop' ),
					'title' => esc_html__( 'Integer', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'step' => 1,
					'default' => 356,
				]
			);

			$controls->add_responsive_control(
				'number_size',
				[
					'label' => esc_html__( 'Number Size (px)', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'step' => 1,
					'default' => 94,
					'tablet_default' => 74,
					'mobile_default' => 60,
					'condition' => [
						'number!' => ''
					],
					'selectors' => [
						'{{WRAPPER}} .cws_milestone_wrapper .cws_milestone_number' => 'font-size: {{VALUE}}px',
					],
				]
			);

			$controls->add_control(
				'speed',
				[
					'label' => esc_html__( 'Speed', 'cryptop' ),
					'title' => esc_html__( 'Integer (Millisecond)', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,
					'step' => 100,
					'default' => 2000,
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'custom_size_i',
				[
					'label' => esc_html__( 'Customize Icon Size', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_lib',
									'operator' => '!=',
									'value' => 'svg'
								]
							]
						], [
							'relation' => 'or',
							'terms' => [
								[
									'name' => 'icon_fontawesome',
									'operator' => '!=',
									'value' => ''
								], [
									'name' => 'icon_flaticons',
									'operator' => '!=',
									'value' => ''
								]
							]
						]
					]
				]
				]
			);

			$controls->add_control(
				'size_i',
				[
					'label' => esc_html__( 'Size Icon', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'step' => 1,
					'default' => 15,
					'condition' => [
						'custom_size_i' => 'yes'
					],	
					'selectors' => [
						'{{WRAPPER}} .cws_milestone_icon i:before' => 'font-size: {{VALUE}}px',
					],
				]
			);

			$controls->add_control(
				'custom_color_milestone',
				[
					'label' => esc_html__( 'Custom Colors', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'custom_color_number',
				[
					'label' => esc_html__( 'Milestone Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => $second_color,
					'value' => $second_color,
					'selectors' => [
						'{{WRAPPER}} .cws_milestone_wrapper .cws_milestone_number' => 'color: {{VALUE}}',
					],
					'condition' => [
						'custom_color_milestone' => 'yes'
					],
				]
			);

			$controls->add_control(
				'custom_color_text',
				[
					'label' => esc_html__( 'Info Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => "#FFF",
					'default' => "#FFF",
					'selectors' => [
						'{{WRAPPER}} .cws_milestone_wrapper .cws_milestone_title' => 'color: {{VALUE}}',
						'{{WRAPPER}} .cws_milestone_wrapper .cws_milestone_desc' => 'color: {{VALUE}}',
					],
					'condition' => [
						'custom_color_milestone' => 'yes'
					],
				]
			);

			$controls->add_control(
				'i_color_m',
				[
					'label' => esc_html__( 'Icon Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $second_color,
					'default' => $second_color,
					'selectors' => [
						'{{WRAPPER}} .cws_milestone_icon i' => 'color: {{VALUE}}',
						'{{WRAPPER}} .cws_milestone_icon svg' => 'fill: {{VALUE}}'
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'custom_color_milestone',
										'operator' => '==',
										'value' => 'yes'
									]
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => '',
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => '',
									], [
										'name' => 'icon_svg',
										'operator' => '!=',
										'value' => '',
									],
								],
							]
						]
					]
				]
			);

		$controls->end_controls_section();
	}
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_milestone', $this, 'php'));
	}
	protected function content_template() {}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Milestone() );