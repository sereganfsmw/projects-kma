<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CWS_Group_Control_Gradient extends Group_Control_Base {

	protected static $fields;

	private static $background_types;

	public static function get_type() {
		return 'cws_gradient';
	}

	public static function get_background_types() {
		if ( null === self::$background_types ) {
			self::$background_types = self::init_background_types();
		}

		return self::$background_types;
	}

	private static function init_background_types() {
		return [
			'classic' => [
				'title' => _x( 'Classic', 'Background Control', 'cryptop' ),
				'icon' => 'fa fa-paint-brush',
			],
			'gradient' => [
				'title' => _x( 'Gradient', 'Background Control', 'cryptop' ),
				'icon' => 'fa fa-barcode',
			],
		];
	}

	public function init_fields() {
		$fields = [];

		$fields['background'] = [
			'label' => _x( 'Background Type', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::CHOOSE,
			'label_block' => false,
			'toggle' => false,
			'render_type' => 'ui',
		];

		$fields['color'] = [
			'label' => _x( 'Color', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::COLOR,
			'default' => '',
			'title' => _x( 'Background Color', 'Background Control', 'cryptop' ),
			'selectors' => [
				'{{SELECTOR}}' => 'background-color: {{VALUE}};',
			],
			'condition' => [
				'background' => [ 'classic', 'gradient' ],
			],
		];

		$fields['color_stop'] = [
			'label' => _x( 'Location', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ '%' ],
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['color_b'] = [
			'label' => _x( 'Second Color', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::COLOR,
			'default' => '#f2295b',
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['color_b_stop'] = [
			'label' => _x( 'Location', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ '%' ],
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['gradient_type'] = [
			'label' => _x( 'Type', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'linear' => _x( 'Linear', 'Background Control', 'cryptop' ),
				'radial' => _x( 'Radial', 'Background Control', 'cryptop' ),
			],
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['gradient_angle'] = [
			'label' => _x( 'Angle', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ 'deg' ],
			'range' => [
				'deg' => [
					'step' => 10,
				],
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background-color: transparent; background-image: linear-gradient({{SIZE}}{{UNIT}}, {{color.VALUE}} {{color_stop.SIZE}}{{color_stop.UNIT}}, {{color_b.VALUE}} {{color_b_stop.SIZE}}{{color_b_stop.UNIT}})',
			],
			'condition' => [
				'background' => [ 'gradient' ],
				'gradient_type' => 'linear',
			],
			'of_type' => 'gradient',
		];

		$fields['gradient_position'] = [
			'label' => _x( 'Position', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'center center' => _x( 'Center Center', 'Background Control', 'cryptop' ),
				'center left' => _x( 'Center Left', 'Background Control', 'cryptop' ),
				'center right' => _x( 'Center Right', 'Background Control', 'cryptop' ),
				'top center' => _x( 'Top Center', 'Background Control', 'cryptop' ),
				'top left' => _x( 'Top Left', 'Background Control', 'cryptop' ),
				'top right' => _x( 'Top Right', 'Background Control', 'cryptop' ),
				'bottom center' => _x( 'Bottom Center', 'Background Control', 'cryptop' ),
				'bottom left' => _x( 'Bottom Left', 'Background Control', 'cryptop' ),
				'bottom right' => _x( 'Bottom Right', 'Background Control', 'cryptop' ),
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background-color: transparent; background-image: radial-gradient(at {{VALUE}}, {{color.VALUE}} {{color_stop.SIZE}}{{color_stop.UNIT}}, {{color_b.VALUE}} {{color_b_stop.SIZE}}{{color_b_stop.UNIT}})',
			],
			'condition' => [
				'background' => [ 'gradient' ],
				'gradient_type' => 'radial',
			],
			'of_type' => 'gradient',
		];

		return $fields;
	}

	protected function get_child_default_args() {
		return [
			'types' => [ 'classic', 'gradient' ],
		];
	}

	protected function filter_fields() {
		$fields = parent::filter_fields();

		$args = $this->get_args();

		foreach ( $fields as &$field ) {
			if ( isset( $field['of_type'] ) && ! in_array( $field['of_type'], $args['types'] ) ) {
				unset( $field );
			}
		}

		return $fields;
	}

	protected function prepare_fields( $fields ) {
		$args = $this->get_args();

		$background_types = self::get_background_types();

		$choose_types = [];

		foreach ( $args['types'] as $type ) {
			if ( isset( $background_types[ $type ] ) ) {
				$choose_types[ $type ] = $background_types[ $type ];
			}
		}

		$fields['background']['options'] = $choose_types;

		array_walk(
			$fields, function( &$field, $field_name ) {

				$gradient_field = $this->get_args();

				//Set defaults velues to all fields (CWS)
				if (isset($gradient_field['defaults'])){
					$defaults_arr = $gradient_field['defaults'];
					if (isset($defaults_arr[$field_name])){
						$field['default'] = $defaults_arr[$field_name];
					}
				}			
				//--Set defaults velues to all fields (CWS)		
			}
		);		

		return parent::prepare_fields( $fields );
	}

	protected function get_default_options() {
		return [
			'popover' => false,
			/*'popover' => [
				'starter_name' => 'typography',
				'starter_title' => _x( 'Typography', 'Typography Control', 'cryptop' ),
			],*/				
		];
	}
}