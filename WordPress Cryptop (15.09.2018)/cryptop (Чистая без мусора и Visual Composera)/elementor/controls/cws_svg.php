<?php
namespace Elementor;

class CWS_Elementor_SVG_Control extends Base_Data_Control {

	public function get_type() {
		return 'CWS_SVG';
	}

	public function content_template() {
		$control_uid = $this->get_control_uid();
		$settings = $this->get_settings();

		ob_start();
		?>
		{{data.controlValue}}
		<?php
		$new_value = ob_get_clean();

		$svg = json_decode($new_value, true);

		$svg = json_decode($value, true);
		$svg_content = '';
		if (!empty($svg) && function_exists('cwssvg_shortcode')) {
			$svg_content = cwssvg_shortcode($svg);
		}

		?>
		<div class="<# /*debugger;*/ #> cws_svg">
			<input id="<?php echo $control_uid; ?>" class="elementor-control-area svg_field" type="text" data-setting="{{ data.name }}" />
			<a href='#' class='cws_svg_icon'>Add</a>
			<a href='#' class='cws_svg_icon_remove'>Remove</a>
			<?php print $svg_content; ?>
		</div>
		<?php 

	}

	public function enqueue() {
		wp_enqueue_script('cws_elementor_main_js', get_template_directory_uri() . '/elementor/controls/js/cws_svg.js', array('media'), '1.0', false);
	}

	protected function get_default_settings() {
		return [
			'input_type' => 'text',
			'placeholder' => '',
			'title' => '',
			'dynamic' => [],			
		];
	}

}