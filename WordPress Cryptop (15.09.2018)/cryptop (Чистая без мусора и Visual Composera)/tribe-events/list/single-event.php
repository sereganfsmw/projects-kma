<?php
/**
 * List View Single Event
 * This file contains one event in the list view
 *
 * @package TribeEventsCalendar
 * @version  4.3
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

// Setup an array of venue details for use later in the template
$venue_details = tribe_get_venue_details();

// Venue
$has_venue_address = ( ! empty( $venue_details['address'] ) ) ? ' location' : '';

// Organizer
$organizer = tribe_get_organizer();

?>

<!-- Event Image -->
<?php echo tribe_event_featured_image( null, 'full' ) ?>

<div class="cws-tribe-events-list">
<!-- Event Title -->
<?php do_action( 'tribe_events_before_the_event_title' ) ?>
<h2 class="tribe-events-list-event-title">
	<a class="tribe-event-url" href="<?php echo esc_url( tribe_get_event_link() ); ?>" title="<?php the_title_attribute() ?>" rel="bookmark">
		<?php the_title() ?>
	</a>
</h2>
<?php do_action( 'tribe_events_after_the_event_title' ) ?>
<?php	
	echo '<div class="tribe-events-list-event-date"><div class="day">' . tribe_get_start_date( null, false, 'j' ) . '</div><div class="month">' . tribe_get_start_date( null, false, 'M' ) . ',</div><div class="year">' . tribe_get_start_date( null, false, 'Y' ) . '</div></div>';

?>
<!-- Event Cost -->
<?php if ( tribe_get_cost() ) : ?>
	<div class="tribe-events-event-cost">
		<span><?php echo tribe_get_cost( null, true ); ?></span>
	</div>
<?php endif; ?>

<!-- Event Content -->
<?php do_action( 'tribe_events_before_the_content' ) ?>
<div class="tribe-events-list-event-description tribe-events-content">
	<?php echo tribe_events_get_the_excerpt( null, wp_kses_allowed_html( 'post' ) ); ?>
</div><!-- .tribe-events-list-event-description -->

<!-- Event Meta -->
<?php do_action( 'tribe_events_before_the_meta' ) ?>
<div class="tribe-events-event-meta">
	<div class="author <?php echo esc_attr( $has_venue_address ); ?>">

		<!-- Schedule & Recurrence Details -->
		<div class="tribe-event-schedule-details">
			<?php echo tribe_events_event_schedule_details() ?>
		</div>

		<?php if ( $venue_details ) : ?>
			<!-- Venue Display Info -->
			<div class="tribe-events-venue-details">
				<?php echo implode( ', ', $venue_details ); ?>
				<?php
				if ( tribe_get_map_link() ) {
					echo tribe_get_map_link_html();
				}
				?>
			</div> <!-- .tribe-events-venue-details -->
		<?php endif; ?>

	</div>
</div><!-- .tribe-events-event-meta -->
<?php do_action( 'tribe_events_after_the_meta' ) ?>

<a href="<?php echo esc_url( tribe_get_event_link() ); ?>" class="tribe-events-read-more" rel="bookmark"><?php esc_html_e( 'Joint Event', 'cryptop' ) ?></a>
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.99 40"><path d="M40,19.71a14.09,14.09,0,0,0-6-5.57A13.89,13.89,0,0,0,34.22,6l0-.16L34,5.77a14.26,14.26,0,0,0-8.24.28,14.06,14.06,0,0,0-5.62-6L20,0l-0.15.08a14.06,14.06,0,0,0-5.62,6A14.25,14.25,0,0,0,6,5.77l-0.16,0L5.78,6a13.88,13.88,0,0,0,.29,8.16,14.09,14.09,0,0,0-6,5.57,0.27,0.27,0,0,0,0,.29,0.27,0.27,0,0,0,0,.29,14.09,14.09,0,0,0,6,5.57A13.88,13.88,0,0,0,5.78,34l0,0.16,0.16,0a14.25,14.25,0,0,0,8.24-.29,14.06,14.06,0,0,0,5.62,6L20,40l0.15-.08a14.06,14.06,0,0,0,5.62-6,14.26,14.26,0,0,0,8.24.28l0.16,0,0-.16a13.89,13.89,0,0,0-.29-8.16,14.09,14.09,0,0,0,6-5.57,0.27,0.27,0,0,0,0-.29A0.27,0.27,0,0,0,40,19.71Zm-13.31,8a13.2,13.2,0,0,1-1.18,5.45,13.44,13.44,0,0,1-4.73-3l-0.35-.37a13.91,13.91,0,0,0,2.21-3.46,14.18,14.18,0,0,0,4,.9C26.64,27.42,26.65,27.59,26.65,27.76ZM14.54,33.21a13.21,13.21,0,0,1-1.18-5.45c0-.17,0-0.34,0-0.51a14.16,14.16,0,0,0,4-.9,13.92,13.92,0,0,0,2.21,3.46l-0.35.37A13.45,13.45,0,0,1,14.54,33.21Zm-1.18-21a13.21,13.21,0,0,1,1.18-5.45,13.45,13.45,0,0,1,4.73,3l0.35,0.37a13.92,13.92,0,0,0-2.21,3.46,14.16,14.16,0,0,0-4-.9C13.36,12.58,13.35,12.41,13.35,12.24ZM25.46,6.79a13.21,13.21,0,0,1,1.18,5.45c0,0.17,0,.34,0,0.51a14.18,14.18,0,0,0-4,.9,13.91,13.91,0,0,0-2.21-3.46l0.35-.37A13.45,13.45,0,0,1,25.46,6.79Zm4,13.21a13.5,13.5,0,0,1-3.43,2.19A14,14,0,0,0,24.87,20a14,14,0,0,0,1.2-2.19A13.5,13.5,0,0,1,29.49,20ZM20,29.39a13.35,13.35,0,0,1-2.06-3.24A14.22,14.22,0,0,0,20,25.05a14.2,14.2,0,0,0,2.06,1.09A13.34,13.34,0,0,1,20,29.39Zm0.16-4.92L20,24.36l-0.16.11a13.61,13.61,0,0,1-2.12,1.13A13.38,13.38,0,0,1,17,23.31l0-.19-0.19,0a13.64,13.64,0,0,1-2.31-.68,13.36,13.36,0,0,1,1.14-2.1,0.27,0.27,0,0,0,0-.3,0.27,0.27,0,0,0,0-.3,13.37,13.37,0,0,1-1.14-2.1,13.64,13.64,0,0,1,2.31-.68l0.19,0,0-.19a13.38,13.38,0,0,1,.69-2.29,13.6,13.6,0,0,1,2.12,1.13L20,15.64l0.16-.11a13.59,13.59,0,0,1,2.12-1.13A13.3,13.3,0,0,1,23,16.69l0,0.19,0.19,0a13.66,13.66,0,0,1,2.31.69,13.42,13.42,0,0,1-1.14,2.1,0.27,0.27,0,0,0,0,.3,0.27,0.27,0,0,0,0,.3,13.42,13.42,0,0,1,1.14,2.1,13.67,13.67,0,0,1-2.31.68l-0.19,0,0,0.19a13.32,13.32,0,0,1-.69,2.29A13.62,13.62,0,0,1,20.16,24.47ZM10.51,20a13.49,13.49,0,0,1,3.43-2.19A14,14,0,0,0,15.13,20a14,14,0,0,0-1.2,2.19A13.5,13.5,0,0,1,10.51,20Zm6.66-5.84a14,14,0,0,0-.68,2.22,14.23,14.23,0,0,0-2.24.67,13.26,13.26,0,0,1-.85-3.74A13.59,13.59,0,0,1,17.17,14.16Zm-2.92,8.78a14.22,14.22,0,0,0,2.24.67,14,14,0,0,0,.68,2.22,13.59,13.59,0,0,1-3.77.84A13.26,13.26,0,0,1,14.25,22.94ZM20,10.61a13.34,13.34,0,0,1,2.06,3.24A14.22,14.22,0,0,0,20,14.95a14.19,14.19,0,0,0-2.06-1.09A13.34,13.34,0,0,1,20,10.61Zm2.83,15.22a13.91,13.91,0,0,0,.68-2.22,14.28,14.28,0,0,0,2.24-.67,13.3,13.3,0,0,1,.85,3.74A13.61,13.61,0,0,1,22.83,25.83Zm2.92-8.78a14.28,14.28,0,0,0-2.24-.67,13.91,13.91,0,0,0-.68-2.22,13.61,13.61,0,0,1,3.77-.85A13.3,13.3,0,0,1,25.75,17.06Zm-12,.23a14.08,14.08,0,0,0-3.49,2.19l-0.37-.34a13.31,13.31,0,0,1-3.06-4.68,13.54,13.54,0,0,1,5.5-1.17l0.51,0A13.82,13.82,0,0,0,13.73,17.28Zm-3.49,3.24a14.07,14.07,0,0,0,3.49,2.19,13.81,13.81,0,0,0-.9,4l-0.51,0a13.54,13.54,0,0,1-5.5-1.17,13.31,13.31,0,0,1,3.06-4.68Zm16,2.19a14.07,14.07,0,0,0,3.49-2.19l0.37,0.35a13.31,13.31,0,0,1,3.05,4.68,13.54,13.54,0,0,1-5.5,1.17l-0.51,0A13.86,13.86,0,0,0,26.27,22.72Zm3.49-3.24a14.07,14.07,0,0,0-3.49-2.19,13.86,13.86,0,0,0,.91-4l0.51,0a13.54,13.54,0,0,1,5.5,1.17,13.31,13.31,0,0,1-3.06,4.68Zm3.64-5.58a14.12,14.12,0,0,0-5.72-1.2h0l6.13-6.07A13.31,13.31,0,0,1,33.41,13.89Zm0-7.67-6.13,6.07v0A13.78,13.78,0,0,0,26,6.58,13.67,13.67,0,0,1,33.36,6.22Zm-8.14,0a14,14,0,0,0-4.9,3.16l0,0V0.86A13.47,13.47,0,0,1,25.22,6.25ZM19.71,0.86V9.44l0,0a14,14,0,0,0-4.9-3.16A13.47,13.47,0,0,1,19.71.86ZM14,6.58a13.78,13.78,0,0,0-1.21,5.66v0L6.64,6.22A13.67,13.67,0,0,1,14,6.58Zm-7.75,0,6.13,6.07h0a14.13,14.13,0,0,0-5.72,1.2A13.3,13.3,0,0,1,6.23,6.63ZM0.55,20a13.52,13.52,0,0,1,5.71-5.31,13.88,13.88,0,0,0,3.19,4.85c0.17,0.16.33,0.32,0.5,0.46-0.16.15-.33,0.3-0.5,0.46a13.88,13.88,0,0,0-3.19,4.85A13.52,13.52,0,0,1,.55,20Zm6,6.11a14.13,14.13,0,0,0,5.72,1.2h0L6.23,33.37A13.3,13.3,0,0,1,6.59,26.11Zm0.05,7.68,6.13-6.07v0A13.78,13.78,0,0,0,14,33.42,13.67,13.67,0,0,1,6.64,33.78Zm8.14,0a14,14,0,0,0,4.9-3.16l0,0v8.58A13.47,13.47,0,0,1,14.78,33.75Zm5.51,5.39V30.56l0,0a14,14,0,0,0,4.9,3.16A13.47,13.47,0,0,1,20.29,39.14ZM26,33.42a13.78,13.78,0,0,0,1.21-5.66v0l6.13,6.07A13.67,13.67,0,0,1,26,33.42Zm7.75,0L27.64,27.3h0a14.12,14.12,0,0,0,5.72-1.2A13.31,13.31,0,0,1,33.77,33.37Zm0-8.06a13.88,13.88,0,0,0-3.19-4.85c-0.17-.17-0.34-0.32-0.5-0.46,0.16-.14.33-0.29,0.5-0.46a13.88,13.88,0,0,0,3.19-4.85A13.51,13.51,0,0,1,39.45,20,13.51,13.51,0,0,1,33.74,25.31Z" transform="translate(-0.01)"/></svg>
</div>
<?php
do_action( 'tribe_events_after_the_content' );
