<?php
show_admin_bar( true ); //Remove

# CONSTANTS
define('CRYPTOP_URI', get_template_directory_uri());
define('CRYPTOP_THEME_DIR', get_template_directory());
defined('CRYPTOP_FIRST_COLOR') or define('CRYPTOP_FIRST_COLOR', '#0c51ac');
defined('CRYPTOP_FOOTER_COLOR') or define('CRYPTOP_FOOTER_COLOR', '#fafafa');
defined('CRYPTOP_SECONDARY_COLOR') or define('CRYPTOP_SECONDARY_COLOR', '#fece42');
defined('CRYPTOP_HELPER_COLOR') or define('CRYPTOP_HELPER_COLOR', '#a9a9a9');

defined('IS_FOOTER') or define('IS_FOOTER', 1);

# \CONSTANTS

# TEXT DOMAIN
load_theme_textdomain( 'cryptop' , get_template_directory() .'/languages' );
# \TEXT DOMAIN

global $cws_theme_funcs;
global $cws_theme_default;
//Check if plugin active
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
require_once(get_template_directory() . '/core/cws_blog.php');
require_once(get_template_directory() . '/core/cws_settings.php');
require_once(get_template_directory() . '/core-fw/components.php');
if (is_plugin_active( 'cws-to/cws-to.php' ) && get_option('cryptop')){
	$cws_theme_default = false;
} else {
	$cws_theme_default = true;
}
$cws_theme_funcs = new Cryptop_Funcs();

// CWS PB settings
class Cryptop_Funcs {
	public static $text_domain = 'cryptop';

	protected static $height_to_width_ratio = 0.78;

	protected static $cws_theme_config;

	public static $options;

	protected static $flags = 0;

	public $templates;

	protected static $to_exists = false;

	protected static $blog_thumb_dims = array(
		'large' => array(
			'none' => array(1170, 659),
			'left' => array(870, 490),
			'right' => array(870, 490),
			'both' => array(570, 321),
			),
		'medium' => array(
			'none' => array(570, 321),
			'left' => array(570, 321),
			'right' => array(570, 321),
			'both' => array(570, 321),
			),
		'small' => array(
			'none' => array(370, 208),
			'left' => array(370, 208),
			'right' => array(370, 208),
			'both' => array(370, 208),
			),
		'checkerboard' => array(
			'none' => array(585, 208),
			'left' => array(415, 245),
			'right' => array(415, 245),
			'both' => array(570, 321),
			),
		'1' => array(
			'none' => array(1170, 659),
			'left' => array(870, 490),
			'right' => array(870, 490),
			'both' => array(570, 321),
			),
		'2' => array(
			'none' => array(570, 321),
			'left' => array(420, 237),
			'right' => array(420, 237),
			'both' => array(270, 152),
			),
		'3' => array(
			'none' => array(370, 208),
			'left' => array(270, 152),
			'right' => array(270, 152),
			'both' => array(270, 152),
			),
		'4' => array(
			'none' => array(270, 152),
			'left' => array(270, 152),
			'right' => array(270, 152),
			'both' => array(270, 152),
			),
	);

	const THEME_BEFORE_CE_TITLE = '<div class="ce_title">';
	const THEME_AFTER_CE_TITLE = '</div>';
	const THEME_V_SEP = '<span class="v_sep"></span>';

	public function __construct() {
		$this->header = array(
			'drop_zone_start' => '',
			'drop_zone_end' => '</div><!-- /header_zone -->',
			'before_header' => '',
			'top_bar_box' => '',
			'logo_box' => '',
			'menu_box' => '',
			'title_box' => '',
			'after_header' => '',
		);

		// Check if JS_Composer is active
	/*	if (class_exists('Vc_Manager')) {
			$vc_man = Vc_Manager::getInstance();
			$vc_man->disableUpdater(true);
			if (!isset($_COOKIE['vchideactivationmsg_vc11'])) {
				setcookie('vchideactivationmsg_vc11', WPB_VC_VERSION);
			}
		}*/
		/*	if ( is_plugin_active('js_composer/js_composer.php') ){
			require_once( get_template_directory() . '/vc/cws_vc_config.php' ); // JS_Composer Theme config file
		}*/

		global $wpdb;
		$to_len = (int)$wpdb->get_var( sprintf('SELECT LENGTH(option_value) FROM '.$wpdb->prefix.'options WHERE option_name = "%s"', self::$text_domain) );
		self::$to_exists = $to_len > 0;

		$this->cws_elementor_init();
		$this->assign_constants();
		$this->init();
		$this->cws_customizer_init();
	}

	private function cws_elementor_init() {

		//Elementor Drag&Drop Page builder plugin
		if ( is_plugin_active('elementor/elementor.php') ){
			require_once( get_template_directory() . '/elementor/cws_elementor_config.php' ); // Elementor config file
		}

	}

	private function cws_elementor_options_init() {
		$pid = get_the_id();

		if (!$pid) return;

		$page = Elementor\Plugin::$instance->documents->get( $pid );
		$settings = $page->get_settings();

		$prefixes_arr = array(
			'page_sidebars' => 'MB_general',
			'theme_colors' => 'MB_theme_colors',
			'header' => 'MB_header',
			'logo_box' => 'MB_logo',
			'menu_box' => 'MB_menu',
			'mobile_menu' => 'MB_mobile',
			'sticky_menu' => 'MB_sticky',
			'title_box' => 'MB_title',
			'top_bar_box' => 'MB_top_bar',
			'footer' => 'MB_footer',
			'boxed' => 'MB_boxed',
			'side_panel' => 'MB_side_panel'
		);
		$grouped_settings = array();

		foreach ($settings as $key => $value) {
			$found = false;
			foreach ($prefixes_arr as $k => $v) {

				preg_match("/^".$k."/", $key, $matches);
				if (!empty($matches)){
					$new_key = str_replace($matches[0].'_', '', $key);

					if (isset($settings[$v]) && $settings[$v] == 'yes'){
						$grouped_settings[$k][$new_key] = $value;
						$found = true;				
					}
					continue;
				}
			}

			if (!$found) $grouped_settings[$key] = $value;
		}

		//Metaboxes structure
		$cws_get_metaboxes_structure = Elementor\cws_get_metaboxes_structure($this);
		return $grouped_settings;
	}

	public function cws_g_option() {
		$this->cws_read_options();
		return self::$options;
	}

	public function cws_get_single_settings_field_value($field_name, $field, $parent_field_name, $parrent_field, &$arr_settings, $inner){
		$field_value = '';

		//Fields type
		if (isset($field['type']) && is_string($field['type'])){			
			if ($field['type'] == 'group'){ //Group
				$field_value = isset($field['value']) ? $field['value'] : $field_value;
			} else if ($field['type'] == 'select'){ //Select
				if (isset($field['source']) && is_array($field['source'])){

					if (isset($field['atts'])){
						if (strpos($field['atts'], 'multiple') === false) {
							$multiple = false;
						} else {
							$multiple = true;
						}
					} else {
						$multiple = false;
					}

					if ($multiple){
						$select_arr = array();
					}
					$select_val = '';

						//Get values
						foreach ($field['source'] as $select_key => $select_value) {
							if ($select_value[1] == true){
								if ($multiple){
									$select_arr[] = strval($select_key);
								} else {
									$select_val = strval($select_key);
									continue;
								}
							}
						}

					if ($multiple){
						$field_value = $select_arr;
					} else {
						$field_value = $select_val;
					}

				}		
			} else if ($field['type'] == 'radio'){ //Radio
				if (isset($field['value']) && is_array($field['value'])){
					foreach ($field['value'] as $radio_key => $radio_value) {
						if ($radio_value[1] == true){
							$field_value = strval($radio_key);
							continue;
						}
					}
				}		
			} else if ($field['type'] == 'checkbox') { //Checkbox
				if (isset($field['atts'])){
					if (strpos($field['atts'], 'checked') === false) {
						$field_value = '0';
					} else {
						$field_value = '1';
					}
				} else {
					$field_value = '0';
				}		
			} else if ($field['type'] == 'fields') { //Fields
				if (is_array($field['layout'])){
					$deep = (int)$inner['deep'];
					$deep++;

					//Make tree from keys and parrent keys
					if (!empty($inner['key'])){
						$found = false;
						foreach ($inner['key'] as $k => $v) {
							if ($v == $parent_field_name){
								$found = true;
							}
						}
						if ($found == false) $inner['key'][] = $parent_field_name;

					} else {
						$inner['key'][] = $parent_field_name;					
					}
					//--Make tree from keys and parrent keys
			
					$this->cws_get_settings_field_value($field_name, $field['layout'], $arr_settings, array('depth' => true, 'deep' => $deep, 'key' => $inner['key']));
				}				
			} else if ($field['type'] == 'text') { //Text
				$field_value = isset($field['value']) ? $field['value'] : $field_value;				
			} else if ($field['type'] == 'media') { //Text
				$field_value = isset($field['value']) ? $field['value'] : array('src' => '', 'id' => '');				
			} else if ($field['type'] == 'font') { //Font
				$field_value = isset($field['value']) ? $field['value'] : $field_value;				
			} else if ($field['type'] == 'number') { //Number
				$field_value = isset($field['value']) ? $field['value'] : $field_value;			
			} else if ($field['type'] == 'textarea') { //Textarea
				$field_value = isset($field['value']) ? $field['value'] : $field_value;			
			} else if ($field['type'] == 'margins') { //Margins
				$margins_arr = array();
				foreach ($field['value'] as $margins_key => $margins_value) {
					$margins_arr[$margins_key] = $margins_value['value'];
				}
				$field_value = $margins_arr;	
			} else if ($field['type'] == 'dimensions') { //Dimensions
				$dimensions_arr = array();
				foreach ($field['value'] as $dimensions_key => $dimensions_value) {
					$dimensions_arr[$dimensions_key] = $dimensions_value['value'];
				}
				$field_value = $dimensions_arr;
			}
			else {
				$field_value = isset($field['value']) ? $field['value'] : $field_value;
			}
			//--Fields type

			//Depth
			if ($inner['depth'] == false){

				//First level fields
				if (empty($parent_field_name)){
					if (!isset($arr_settings[$field_name])){
						$arr_settings[$field_name] = $field_value;
					}
				} else {
					if (!isset($arr_settings[$parent_field_name][$field_name])){
						$arr_settings[$parent_field_name][$field_name] = $field_value;
					}						
				}

			} else {

				//Nested fields
				if ($inner['deep'] == 1){
					if (!isset($arr_settings[$inner['key'][0]][$parent_field_name][$field_name])){
						$arr_settings[$inner['key'][0]][$parent_field_name][$field_name] = $field_value;
					}
				} else if ($inner['deep'] == 2){
					if (!isset($arr_settings[$inner['key'][0]][$inner['key'][1]][$parent_field_name][$field_name])){
						$arr_settings[$inner['key'][0]][$inner['key'][1]][$parent_field_name][$field_name] = $field_value;
					}
				} else if ($inner['deep'] == 3){
					if (!isset($arr_settings[$inner['key'][0]][$inner['key'][1]][$inner['key'][2]][$parent_field_name][$field_name])){
						$arr_settings[$inner['key'][0]][$inner['key'][1]][$inner['key'][2]][$parent_field_name][$field_name] = $field_value;
					}
				} else if ($inner['deep'] == 4){
					if (!isset($arr_settings[$inner['key'][0]][$inner['key'][1]][$inner['key'][2]][$inner['key'][3]][$parent_field_name][$field_name])){
						$arr_settings[$inner['key'][0]][$inner['key'][1]][$inner['key'][2]][$inner['key'][3]][$parent_field_name][$field_name] = $field_value;
						var_dump($arr_settings);
					}
				}

			}
			//--Depth

		}
	}

	public function cws_get_settings_field_value($parent_field_name, $parrent_field, &$arr_settings, $inner){
		$types_arr = array(
			'group',
		);

		if (isset($parrent_field['layout']) && is_array($parrent_field['layout']) && !in_array($parrent_field['type'], $types_arr)){
			$arr = $parrent_field['layout'];
		} else if (is_array($parrent_field)){
			$arr = $parrent_field;
		}

		if (isset($arr['type']) && is_string($arr['type'])){
			$this->cws_get_single_settings_field_value($parent_field_name, $parrent_field, '', $parrent_field, $arr_settings, $inner);
		} else {
			foreach ($arr as $field_name => $field) {
				$this->cws_get_single_settings_field_value($field_name, $field, $parent_field_name, $parrent_field, $arr_settings, $inner);
			}			
		}

	}

	public function cws_get_settings_fields($fields, &$settings){

		//Sections
		foreach ($fields as $section_name => $section) {
			if (isset($section['type']) && $section['type'] == 'section'){

				//Tabs
				foreach ($section['layout'] as $tab_name => $tab) {
					if (isset($tab['type']) && $tab['type'] == 'tab'){

						//Fields
						foreach ($tab['layout'] as $field_name => $field) {
							$this->cws_get_settings_field_value($field_name, $field, $settings, array('depth' => false, 'deep' => 0, 'key' => array()));
						} //--Fields

					}
				}
				
			}
		}

	}

	public function cws_get_dafault_options(){
		$cws_settings_structure = cws_get_to_settings();
		$g_components = cws_core_get_base_components();

		//Merge options with %layout%
		if (function_exists('cws_core_build_settings')) {
			cws_core_build_settings($cws_settings_structure, $g_components);
		}

		$settings_fields = array();
		$this->cws_get_settings_fields($cws_settings_structure, $settings_fields);
		return $settings_fields;
	}

	private function cws_read_options() {
		global $wp_query;
		global $cws_theme_default;
		$pid = get_the_id();

		//Get options (Elementor Page Options)
		if (!$cws_theme_default && is_plugin_active('elementor/elementor.php')){
			$elementor_options = $this->cws_elementor_options_init();
		}

		//Get options (Theme Options)
		$theme_options = get_option(self::$text_domain);

		//Get options, if plugin deactivated (Default Options)
		if ($cws_theme_default){
			$theme_options = $this->cws_get_dafault_options();
		}

		if (empty($theme_options)) return;

		$besides_options = is_search();
		if ($pid && ! $besides_options) {

			if (is_elementor() && !empty($elementor_options)){

				$meta = $elementor_options;

				foreach ($theme_options as $key => $value) {
					if (!isset($meta[$key])) {
						$meta[$key] = $value;
					}
				}

			} else {
				//Get options, if Elementor deactivated (MetaBox Options)
				$meta = $this->cws_get_post_meta($pid);
				if (!empty($meta)) {
					$meta = $meta[0];
					foreach ($theme_options as $key => $value) {
						if (!isset($meta[$key])) {
							$meta[$key] = $value;
						}
					}
				} else {
					$meta = $theme_options;
				}
			}

			self::$options = $meta;
		} else {
			self::$options = $theme_options;
		}

	}
	private function assign_constants() {
		self::$cws_theme_config = array(
			'js_path' => get_template_directory_uri() .'/js/',
			'scripts' => array(
				'header' => array(
					//Params ([enable], [path], [dependency], [force activation ENQUEUE])
					//You need to enqueue script in code if it's need, like wp_enqueue_script( 'fancybox' );
					'fancybox' => array(true, 'jquery.fancybox.js', null, true),
					'select2_init' => array(true, 'select2.min.js', null, false),
					'cws_scripts' => array(true, 'scripts.js', null, true),
					'fixed_sidebars' => array($this->cws_get_option('sticky_sidebars') == '1', 'sticky_sidebar.js', null, true),
					'tweenmax' => array(true, 'tweenmax.min.js', null, false),
					'jquery_easing' => array(true, 'jquery.easing.1.3.min.js', null, true),
					'comment-reply' => array(true, null, null, false),
				),
				'footer' => array(
					'owl_carousel' => array(true, 'owl.carousel.js', null, false),
					'pie_chart' => array(true, 'jquery.pie_chart.js', null, false),
					'isotope' => array(true, 'isotope.pkgd.js', null, false),
					'odometer' => array(true, 'odometer.js', null, false),
					'wow' => array(true, 'wow.min.js', null, false),
					'parallax' => array(true, 'parallax.js', null, false),
					'vimeo' => array(true, 'jquery.vimeo.api.min.js', null, false),
					'skrollr' => array(true, 'skrollr.min.js', null, false),
					'modernizr' => array( true, 'modernizr.js', null, false),
					'yt_player_api' => array(true, 'https://www.youtube.com/player_api', null, false),
				),
			),
			'localize_scripts' => array(
				'fancybox' => array(
					'cws_close' => esc_html__( 'Close', 'cryptop' ),
					'cws_next' => esc_html__( 'Next', 'cryptop' ),
					'cws_prev' => esc_html__( 'Prev', 'cryptop' ),
					'cws_error' => esc_html__( 'The requested content cannot be loaded. Please try again later.', 'cryptop' )
				),
			),
			'css_path' => get_template_directory_uri() . '/css/',
			'styles' => array(
				'reset' => 'reset.css',
				'layout' => 'layout.css',
				'cws_font_awesome' => get_template_directory_uri() . '/fonts/font-awesome/font-awesome.css',
				'cwsfi' => array('cws_is_cwsfi', null),
				'fancybox' => 'jquery.fancybox.css',
				'select2_init' => 'select2.css',
				'animate' => 'animate.css',
				'owl_carousel' => 'owl.carousel.css',
			),
			'actions' => array(
				'cws_is_flaticon' => '',
				'cws_is_cwsfi' => '',
			),
			'gfonts' => array('body', 'menu', 'header', 'helper'), // body-font etc from theme options
			'def_char_number' => 155, // cws_blog_get_chars_count
			'char_counts' => array( // keys are columns
				array(
					'double' => 130,
					'single' => 200,
					'' 		 => 300
				), // empty dummy array
				array(
					'double' => 130,
					'single' => 200,
					'' 		 => 300,
				),
				array(
					'double' => 120,
					'single' => 140,
					''			 => 150,
				),
				array(
					'double' => 60,
					'single' => 80,
					''			 => 90,
				),
				array(
					'double' => 50,
					'single' => 70,
					''			 => 100,
				),
			),
			'strings' => array(
				'home' => esc_html__( 'Home','cryptop'), // text for the 'Home' link
				'category' => esc_html__( 'Category "%s"','cryptop' ), // text for a category page
				'search' => esc_html__( 'Search most for ','cryptop' ).(isset($_GET['s']) ? $_GET['s'] : ""), // text for a search results page
				'taxonomy' => esc_html__( 'Archive by %s "%s"', 'cryptop'),
				'tag'	=> esc_html__( 'Posts Tagged "%s"','cryptop' ), // text for a tag page
				'author' => esc_html__( 'Articles Posted by %s','cryptop' ), // text for an author page
				'404' => esc_html__( 'Error 404','cryptop' ),
				'cart' => esc_html__( 'Cart','cryptop' ),
				'checkout' => esc_html__( 'Checkout','cryptop' ),
			),
			'alt_breadcrumbs' => array('yoast_breadcrumb' => array( '<nav class="bread-crumbs">', '</nav>', false)), // alternative breadcrumbs function and its arguments
			'post-formats' => array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ),
			'nav-menus' => array(
				'header-menu' => esc_html__( 'Navigation Menu','cryptop' ),
				'sidebar-menu' => esc_html__( 'SidePanel Menu', 'cryptop'),
				'copyrights-menu' => esc_html__( 'Copyrights Menu', 'cryptop' )
			),
			'widgets' => array(
				'CWS_Text',
				'CWS_Ourteam',
				'CWS_Latest_Posts',
				'CWS_Latest_Comments',
				'CWS_Testimonials',
				'CWS_Portfolio',
				'CWS_Twitter',
				'CWS_Contact',
				'CWS_Social',
				'CWS_About',
				'CWS_Gallery',
				'CWS_Banner',
				'CWS_Categories',
			),
			'category_colors' => array('567dbe', 'be5656', 'be9656', '62be56', 'be56b1', '56bebd'),
			'admin_pages' => array('widgets.php', 'edit-tags.php', 'edit.php', 'term.php', 'user-edit.php', 'profile.php', 'nav-menus.php'), // pages cwsfw should be initialized on
		);
	}

	public function get_theme_config($name) {
		if (isset(self::$cws_theme_config[$name])) {
			return self::$cws_theme_config[$name];
		}
		return null;
	}

	public function cws_customizer_init() {
		if ( is_customize_preview() ) {
			if ( isset( $_POST['wp_customize'] ) && $_POST['wp_customize'] == "on" ) {
				if (strlen($_POST['customized']) > 10) {
					global $cwsfw_settings;
					global $cwsfw_mb_settings;
					$post_values = json_decode( stripslashes_deep( $_POST['customized'] ), true );
					if (isset($post_values['cwsfw_settings'])) {
						$new_options = $post_values['cwsfw_settings'];
						$current_options = get_option('cryptop');
						foreach ($new_options as $key => $value) {

							if (is_array($value)){
								if (!isset($current_options[$key])) {
									$current_options[$key] = array();
								}
								$value = array_merge($current_options[$key], $value );
							}
							$cwsfw_settings[$key] = $value;
						}
					}
					if (isset($post_values['cwsfw_mb_settings'])) {
						$cwsfw_mb_settings = $post_values['cwsfw_mb_settings'];
						$this->cws_meta_vars();
					}
				}
			}
		}
	}

	/* Woo Related functions */
	public function cws_getWooMiniCart() {
		ob_start();
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {	woocommerce_mini_cart(); }
		return ob_get_clean();
	}

	public function cws_getWooMiniIcon() {
		ob_start();
		if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) { ?>
			<a class="woo_icon" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_html_e( 'View your shopping cart','cryptop' ); ?>"><i class='woo_mini-count flaticon-shopcart-icon'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . esc_html( WC()->cart->cart_contents_count ) .'</span>' : '') ?></i></a>
		<?php
		}
		return ob_get_clean();
	}

	/* /Woo Related functions */

	/* Some useful functions */
	public function cws_get_option($name) {
		global $cws_theme_default;
		// !!! this must be in superclass
		$ret = null;
		if (is_customize_preview()) {
			global $cwsfw_settings;
			if (isset($cwsfw_settings[$name])) {
				$ret = $cwsfw_settings[$name];
				if (is_array($ret)) {
				$theme_options = get_option( self::$text_domain );
					if (isset($theme_options[$name])) {
						$to = $theme_options[$name];
							foreach ($ret as $key => $value) {
								$to[$key] = $value;
							}
						$ret = $to;
					}
				}
				return $ret;
			}
		}

		//Get options (Theme Options)
		$theme_options = get_option( self::$text_domain );

		//Get options, if plugin deactivated (Default Options)
		if ($cws_theme_default){
			$theme_options = $this->cws_get_dafault_options();
		}		

		$ret = isset($theme_options[$name]) ? $theme_options[$name] : null;
		$ret = stripslashes_deep( $ret );
		return $ret;
	}

	public function cws_get_meta_option($name = '', $check_first_key = false) {
		$value = isset(self::$options[$name]) ? self::$options[$name] : null;
		while (is_string($value) && '{' === substr($value, 0, 1)) {
			$g_name = substr($value, 1, -1);
			$value = isset(self::$options[$g_name]) ? self::$options[$g_name] : null;
		}
		if ($check_first_key && is_array($value) && !empty($value)) {
			// it's better to set $check_first_key specifically when there's a chance
			// like in case of sidebars processing
			// check if need to replace value with theme option array
			reset($value);
			$first_key = key($value);
			$val = $value[$first_key];
			if (is_string($val) && '{' === substr($val, 0, 1)) {
				$g_name = substr($val, 1, -1);
				$value = isset(self::$options[$g_name]) ? self::$options[$g_name] : null;
			}
		}
		return $value;
	}

	public function cws_get_post_meta($pid, $key = 'cws_mb_post') {
		$ret = get_post_meta($pid, $key);
		if (!empty($ret[0])) {
			$ret = $ret[0];
		}
		if (is_customize_preview()) {
			global $cwsfw_settings;
			global $cwsfw_mb_settings;
			if(!empty($cwsfw_settings)){
				$ret = array_merge($ret, $cwsfw_settings);
			}
			if (!empty($cwsfw_mb_settings) && !empty($ret)) {
				$ret = array_merge($ret, $cwsfw_mb_settings);
			} else if (!empty($cwsfw_mb_settings) && empty($ret)) {
				$ret = $cwsfw_mb_settings;
			}
		}

		$ret = array($ret);

		return $ret;
	}

	// !!! this must be in superclass
	public function echo_ne($condition, $str, $str2 = '') {	echo !empty($condition) ? $str : $str2; }

	// !!! this must be in superclass
	public function echo_if($condition, $str, $str2 = '') {if($condition){echo sprintf("%s", $str);}else{echo sprintf("%s", $str2);}
	}

	public function print_if($condition, $str, $str2 = '') { return $condition ? $str : $str2; }

	public function print_ne($condition, $str, $str2 = '') { return !empty($condition) ? $str : $str2; }

	public function cws_print_search_form($message_title = '', $message = '') {
		ob_start();
		echo shortcode_exists('cws_sc_msg_box') ? do_shortcode( "[cws_sc_msg_box type='info' title='{$message_title}' text='{$message}'][/cws_sc_msg_box]" ) : (!empty($message_title) ? "<h3>{$message_title}</h3><p>{$message}</p>" : '');
		get_search_form();
		$sc_content = ob_get_clean();
		return $sc_content;
	}

	/* END of Some useful functions */

	public function cws_render_sidebars($pid) {
		// !!! this must be in superclass
		$out = '';
		$sb = $this->cws_get_sidebars( $pid );
		$layout_class = $sb && $sb['layout_class'] != 'none' && !empty($sb['layout_class']) ? $sb['layout_class'].'_sidebar' : '';
		$sb1_class = $sb && isset($sb['layout']) && !$sb['layout'] == 'none' ? ($sb['layout'] == 'right' ? 'sb_right' : 'sb_left') : '';
		$sbl = $sb['sbl'];
		if ( $sbl ){
			$out .= '<div class="container">';
			if ( !empty($sb['sb1']) && is_active_sidebar($sb['sb1']) ) {
				$out .= sprintf('<aside class="%s">', sanitize_html_class($sb1_class));
				ob_start();
					dynamic_sidebar( $sb['sb1'] );
				$out .= ob_get_clean();
				$out .= '</aside>';
			}
			if ( !empty($sb['sb2']) && is_active_sidebar($sb['sb2']) ){
				$out .= '<aside class="sb_right">';
				ob_start();
					dynamic_sidebar( $sb['sb2'] );
				$out .= ob_get_clean();
				$out .= '</aside>';
			}
		}
		return array(
			'layout_class' => $layout_class,
			'sb_class' => $sb1_class,
			'content' => $out,
		);
	}

	private function cws_is_woo() {
		global $woocommerce;

		return !empty( $woocommerce ) ? is_woocommerce() || is_product_tag() || is_product_category() || is_account_page() || is_cart() || is_checkout() : false;
	}

	public function cws_is_blog () {
		global  $post;
		$posttype = get_post_type($post );
		return ( ((is_archive()) || (is_author()) || (is_category()) || (is_home()) || (is_single()) || (is_tag())) && ( $posttype == 'post')  ) ? true : false ;
	}	

	public function cws_get_sidebars( $pid = null ) { /*!*/
		// return null;
		$page_type = 'page';
		$sb = null;
		$post_type = get_post_type($pid);

		if ($pid && !is_home() ) {
			switch ($post_type) {
				case 'page':
					$page_type = 'page';
					break;
				case 'attachment':
				case 'post':
				case 'cws_portfolio':
				case 'cws_classes':
				case 'cws_staff':
					$page_type = 'post';
					break;
			}
		}
		if (is_front_page()) {
			//Is is_home() works if selected "latest posts" in Reading
			/* default home page have no ID */
			$page_type = 'home';
		}
		if (is_category() && is_archive()) {
			$page_type = 'blog';
		}

		if (!$sb) {
			$sb = $this->cws_get_meta_option("{$page_type}_sidebars", true);

			if ($sb == 'default'){
				$sb = $this->cws_get_option("{$page_type}_sidebars");
			}
		}

		//Sidebar fix for Elementor
		$sb_enabled = isset($sb['layout']) && $sb['layout'] != 'none';
		$sbl = 0;
		$class = '';
		if ($sb_enabled){			
			if ($sb['layout'] == 'left'){
				$sbl = 1;
				$class = 'single';
				unset($sb['sb2']);
			} else if ($sb['layout'] == 'right') {
				$class = 'single';
				$sbl = 1;
				unset($sb['sb1']);
			} else if ($sb['layout'] == 'both') {
				$class = 'double';
				$sbl = 2;
			}	
		}
		$ret = $sb;

		$ret['layout_class'] = $class;
		$ret['sbl'] = $sbl;
		return $ret;
	}

	public function cws_enqueue_script(){
		$scripts = self::$cws_theme_config['scripts'];
		$localize_scripts = self::$cws_theme_config['localize_scripts'];
		$js_path = self::$cws_theme_config['js_path'];
		wp_enqueue_script("jquery");
		foreach ($scripts as $type => $v) {
			$is_footer = 'footer' === $type;
			foreach ($v as $alias => $value) {
				//foreach
				list($is_load, $path, $dependencies, $enqueue) = $value;

				if ($path) {
					$path = (0 === strrpos($path, 'http')) ? $path : $js_path . $path;
				}

				if (!is_bool($is_load)) {
					$is_load = do_action();
				}

				if ($is_load) {
					if ($enqueue){
						wp_enqueue_script($alias, $path, $dependencies, '1.0', $is_footer);
					} else {
						wp_register_script($alias, $path, $dependencies, '1.0', $is_footer);
					}
				}

				if (isset($localize_scripts[$alias])){
					$js_object_name = 'cws_loc_'.$alias;
					wp_localize_script( $alias, $js_object_name, $localize_scripts[$alias] );
				}
				//foreach
			}
		}

		wp_enqueue_style( '', $this->cws_render_fonts_url() );
	}

	public function cws_enqueue_styles(){
		$styles = self::$cws_theme_config['styles'];
		$css_path = self::$cws_theme_config['css_path'];

		foreach ($styles as $alias => $value) {
			if (is_array($value)) {
				list($is_load, $path) = $value;
				if ($path) {
					$path = (0 === strrpos($path, 'http')) ? $path : $css_path . $path;
				}
			} else {
				$path = (0 === strrpos($value, 'http')) ? $value : $css_path . $value;
				$is_load = true;
			}
			if ($is_load) {
				wp_enqueue_style($alias, $path);
			}
		}

		$this->cws_theme_enqueue_styles();
		$this->cws_add_style();
	}

	private function cws_render_fonts_url() {
		$url = $query_args = '';
		$gfonts = self::$cws_theme_config['gfonts'];
		$fonts_opts = array();
		foreach ($gfonts as $value) {
			$font_value = $this->cws_get_option( $value.'-font' );
			if (isset($font_value)){
				$fonts_opts[] = $font_value;
			}
		}

		if ( !empty( $fonts_opts ) ) {
			$fonts_urls = array( count( $fonts_opts ) );
			$subsets_arr = array();
			$base_url = "//fonts.googleapis.com/css";

			for ( $i = 0; $i < count( $fonts_opts ); $i++ ){
				$fonts_urls[$i] = $fonts_opts[$i]['font-family'];
				$fonts_urls[$i] .= !empty( $fonts_opts[$i]['font-weight'] ) ? ':' . implode( $fonts_opts[$i]['font-weight'], ',' ) : '';
				if(!empty($fonts_opts[$i]['font-sub'])){
					for ( $j = 0; $j < count( $fonts_opts[$i]['font-sub'] ); $j++ ){
						if ( !in_array( $fonts_opts[$i]['font-sub'][$j], $subsets_arr ) ){
							array_push( $subsets_arr, $fonts_opts[$i]['font-sub'][$j] );
						}
					}
				}
			}
			$query_args = array(
				'family'	=> urlencode( implode( $fonts_urls, '|' ) )
			);
			if ( !empty( $subsets_arr ) ) {
				$query_args['subset']	= urlencode( implode( $subsets_arr, ',' ) );
			}

			if ( 'off' !== _x( 'on', 'Google font: on or off', 'cryptop' ) ) {
				$url = add_query_arg( $query_args, $base_url );
			}

		}
		return $url;
	}

	public function cws_wp_title_filter ( $title_text ) {
		$site_name = get_bloginfo( 'name' );
		return is_home() ? $site_name . " | " . get_bloginfo( 'description' ) : $site_name;
	}

	public function cws_custom_nav_menu_item_title ( $title, $item, $args, $depth ) {
		$title =
			(!empty($item->icon) ? "<i class='".esc_attr($item->icon)."'></i> " : '') . //Custom menu fields (icon)
			esc_html($title) .
			($item->tag && !empty($item->tag_text)  ? "<span class='tag_label' style='color:".esc_attr($item->tag_font_color).";background-color:".esc_attr($item->tag_bg_color).";'>".esc_html($item->tag_text)."</span> " : ''); //Custom menu fields (label)

		return $title;
	}

	# UPDATE THEME
	public function cws_check_for_update($transient) {
		if (empty($transient->checked)) { return $transient; }

		$theme_pc = trim($this->cws_get_option('_theme_purchase_code'));
		if (empty($theme_pc)) {
			add_action( 'admin_notices', array($this, 'cws_an_purchase_code') );
		}

		$result = wp_remote_get('http://up.creaws.com/products-updater.php?pc=' . $theme_pc . '&tname=' . self::$text_domain);
		if (!is_wp_error( $result ) ) {
			if (200 == $result['response']['code'] && 0 != strlen($result['body']) ) {
				$resp = json_decode($result['body'], true);
				$h = isset( $resp['h'] ) ? (float) $resp['h'] : 0;
				$theme = wp_get_theme(get_template());
				if (isset($resp['new_version']) && version_compare( $theme->get('Version'), $resp['new_version'], '<' ) ) {
					$transient->response[self::$text_domain] = $resp;
				}
			}
			else{
				unset($transient->response[self::$text_domain]);
			}
		}
		return $transient;
	}

	// an stands for admin notice
	public function cws_an_purchase_code() {
		$cws_theme = wp_get_theme();
		echo "<div class='update-nag'>" . $cws_theme->get('Name') . esc_html__(' theme notice: Please insert your Item Purchase Code in Theme Options to get the latest theme updates!', 'cryptop') .'</div>';
	}
	# \UPDATE THEME

	private function init() {
		global $wp_filesystem;
		global $cws_theme_default;

		if(empty( $wp_filesystem )) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}

		require_once get_template_directory() . '/core/plugins.php';
		require_once(get_template_directory() . '/core/cws_thumb.php');
		include_once(get_template_directory() . '/core/breadcrumbs.php');
		if (function_exists('cws_core_cwsfw_fillMbAttributes')) {
			load_template( trailingslashit( get_template_directory() ) . '/core/scg.php');
			new Cryptop_SCG();
		}

		set_transient('update_themes', 48*3600);

		add_action('after_setup_theme', array($this, 'cws_after_setup_theme') );
		add_action('init', array($this, 'add_excerpts_to_pages') );
		add_filter('nav_menu_item_title', array($this, 'cws_custom_nav_menu_item_title'), 10, 4 ); //Custom menu fields
		add_filter('wp_title', array($this, 'cws_wp_title_filter') );
		add_filter('pre_set_site_transient_update_themes', array($this, 'cws_check_for_update') );
		add_action('admin_enqueue_scripts', array($this, 'cws_admin_init' ) );
		add_filter('the_content', array($this, 'fix_shortcodes_autop') );

		add_filter( 'comment_form_defaults', array($this, 'cws_comment_form_defaults') );

		add_filter('the_content', array($this, 'cws_fix_jetpack_social') );
		add_action('wp_enqueue_scripts', array($this, 'cws_enqueue_script') );

		add_action('wp_enqueue_scripts', array($this, 'cws_enqueue_styles') );

		add_action('wp_enqueue_scripts', array($this, 'cws_enqueue_theme_stylesheet'), 999 );
		add_action('widgets_init', array($this, 'cws_widgets_init') );
		add_filter('body_class', array($this, 'cws_layout_class') );

		add_action('menu_font_hook', array($this, 'cws_menu_font_action') );
		add_action('header_font_hook', array($this, 'cws_header_font_action') );
		add_action('body_font_hook', array($this, 'cws_body_font_action') );
		add_action('body_helper_hook', array($this, 'cws_body_helper_action') );

		add_action('theme_color_hook', array($this, 'cws_theme_color_action'), 1);
		add_action('theme_color_hook', array($this, 'cws_theme_rgba_color'), 1);

		//Custom block styles
		add_action('theme_color_hook', array($this, 'cws_custom_sticky_menu_styles_action'), 2);
		add_action('theme_color_hook', array($this, 'cws_custom_header_styles_action'), 3);
		add_action('theme_color_hook', array($this, 'cws_custom_top_bar_styles_action'), 4);
		add_action('theme_color_hook', array($this, 'cws_custom_logo_box_styles_action'), 5);
		add_action('theme_color_hook', array($this, 'cws_custom_menu_box_styles_action'), 6);
		add_action('theme_color_hook', array($this, 'cws_custom_page_title_styles_action'), 8);
		add_action('theme_color_hook', array($this, 'cws_custom_side_panel_styles_action'), 9);
		add_action('theme_color_hook', array($this, 'cws_custom_footer_styles_action'), 10);
		add_action('theme_color_hook', array($this, 'cws_custom_boxed_layout_styles_action'), 11);
		add_action('theme_color_hook', array($this, 'cws_custom_styles_action'), 12);
		//Custom block styles

		if ($cws_theme_default){
			add_action('theme_color_hook', array($this, 'cws_styles_default'), 13);
		}

		add_action('theme_gradient_hook', array($this, 'cws_theme_gradient_action') );
		add_filter('body_class', array($this, 'cws_gradients_body_class') );
		add_filter('cws_dbl_to_sngl_quotes', array($this, 'cws_dbl_to_sngl_quotes') );

		add_action('wp_enqueue_scripts', array($this, 'cws_js_vars_init') );
		add_action('wp', array($this, 'cws_meta_vars') );
		add_action('template_redirect', array($this, 'cws_ajax_redirect') );
		add_filter('excerpt_length', array($this, 'cws_custom_excerpt_length'), 999 );
		add_action('wp_enqueue_scripts', array($this, 'cws_ajaxurl') );
		add_filter('embed_oembed_html', array($this, 'cws_oembed_wrapper'),10,3);
		add_filter('body_class', array($this, 'cws_loading_body_class') );

		//Filter all widgets output
		add_filter('dynamic_sidebar_params', array( $this, 'cws_filter_dynamic_sidebar_params' ), 9 );
		add_filter('widget_output', array($this, 'cws_filter_widgets'),10,4);
		//Filter all widgets output

		add_filter('post_gallery', array($this, 'cws_custom_gallery'), 10, 2);
		add_filter('get_search_form', array($this, 'cws_custom_search'));

		add_filter('wp_setup_nav_menu_item', array($this, 'cws_add_custom_nav_fields')); // Add custom menu fields to menu
		add_filter('wp_update_nav_menu_item', array($this, 'cws_update_custom_nav_fields'), 10, 3); // Save menu custom fields
		add_filter('wp_edit_nav_menu_walker', array($this, 'cws_edit_walker'), 10, 2); // Edit menu walker

		// Add inline style
		add_filter('cws_print_single_class', array($this, 'cws_print_single_class'));

		/* tinymce related */
		add_filter( 'tiny_mce_before_init', array($this, 'cws_tiny_mce_before_init') );
		add_filter( 'mce_buttons_2', array($this, 'cws_mce_buttons_2') );
		/* /tinymce related */

		// comments
		add_filter('preprocess_comment', array($this, 'cws_comment_post'), '', 1);
		add_filter( 'comment_form_fields',array( $this, 'cws_move_comment_field_to_bottom' ) );

		// Add Svg support
		add_filter('upload_mimes', array($this, 'cc_mime_types'));

		// Check if WPML is active
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
			define('CWS_WPML_ACTIVE', true);
			$GLOBALS['wpml_settings'] = get_option('icl_sitepress_settings');
			global $icl_language_switcher;
		} else {
			define('CWS_WPML_ACTIVE', false);
		}

		define('CWS_WOO_ACTIVE', is_plugin_active('woocommerce/woocommerce.php'));

		if (CWS_WOO_ACTIVE) {
			add_action( 'wp_ajax_woocommerce_remove_from_cart',array( $this, 'cws_woo_ajax_remove_from_cart' ),1000 );
			add_action( 'wp_ajax_nopriv_woocommerce_remove_from_cart', array( $this, 'cws_woo_ajax_remove_from_cart' ),1000 );

			require_once( get_template_directory() . '/woocommerce/wooinit.php' ); // WooCommerce Shop ini file

			add_filter( 'add_to_cart_fragments', array($this, 'cws_woo_header_add_to_cart_fragment') );

			add_filter( 'woocommerce_output_related_products_args', array($this, 'cws_woo_related_products_args') );
			add_action( 'after_setup_theme', array($this, 'cws_theme_woo_setup') );
			add_filter( 'loop_shop_per_page', array( $this, 'loop_products_per_page' ));
		}
	}

	public function loop_products_per_page() {
		return (int) $this->cws_get_option( 'woo_num_products' );
	}

	public function cws_theme_woo_setup(){
			add_theme_support( 'wc-product-gallery-zoom' );
			add_theme_support( 'wc-product-gallery-lightbox' );
			add_theme_support( 'wc-product-gallery-slider' );
	}

	public function cc_mime_types($mimes) {
		$mimes['svg'] = 'image/svg+xml';
		return $mimes;
	}

	public function fix_shortcodes_autop($content){
		$array = array (
			'<p>[' => '[',
			']</p>' => ']',
			']<br />' => ']'
		);

		$content = strtr($content, $array);
		return $content;
	}

	public function add_excerpts_to_pages(){
		add_post_type_support( 'page', 'excerpt' );
	}

	public function cws_fix_jetpack_social( $content ) {
		if ( is_singular( 'post' ) && function_exists( 'sharing_display' ) ) {
	        remove_filter( 'the_content', 'sharing_display', 19 );
	        remove_filter( 'the_excerpt', 'sharing_display', 19 );
			$content = $content . sharing_display();
		}
		return $content;
	}

	public function cws_comment_form_defaults( $defaults ){
		$defaults['title_reply'] = esc_html__('Leave a Comment', 'cryptop' );
		$defaults['title_reply_before'] = '<span id="reply-title" class="h3 comment-reply-title">';
		$defaults['title_reply_after'] = ' <span class="slash-icon">/<i class="fa fa-angle-double-right"></i></span></span>';
	  return $defaults;
	}

	public function cws_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		return $buttons;
	}

	public function cws_blog_get_chars_count( $cols = 0, $pid = null ) {
		$number = self::$cws_theme_config['def_char_number'];
		$pid = $pid ? $pid : get_queried_object_id();
		$sb = $this->cws_get_sidebars( $pid );
		$sb_layout = isset( $sb['sb_layout_class'] ) ? $sb['sb_layout_class'] : '';
		$anums = self::$cws_theme_config['char_counts'];
		if ( $cols < count($anums) ) {
			$number = $anums[$cols][$sb_layout];
		}
		return $number;
	}

	public function cws_tiny_mce_before_init( $settings ) {
		$font_array = $this->cws_get_option( 'header-font' );

		$settings['theme_advanced_blockformats'] = 'p,h1,h2,h3,h4';

		$style_formats = array(
		array( 'title' => 'Title', 'block' => 'div', 'classes' => 'ce_title' ),
		array( 'title' => 'Divider left', 'block' => 'div', 'classes' => 'ce_title ce_title_div_left' ),
		array( 'title' => 'Divider right', 'block' => 'div', 'classes' => 'ce_title ce_title_div_right' ),
		array( 'title' => 'Post highlight', 'block' => 'div', 'classes' => 'post_highlight' ),
		array( 'title' => 'Font-size', 'items' => array(
			array( 'title' => '50px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '50px' , 'line-height' => '1em') ),
			array( 'title' => '40px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '40px' , 'line-height' => '1.2em') ),
			array( 'title' => '30px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '30px' , 'line-height' => '1.4em') ),
			array( 'title' => '20px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '20px' , 'line-height' => '1.6em') ),
			array( 'title' => '16px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '16px' , 'line-height' => '1.75em') ),
			array( 'title' => '14px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em', 'styles' => array( 'font-size' => '14px' , 'line-height' => '1.75em') ),
			)
		),
		array( 'title' => 'margin-top', 'items' => array(
			array( 'title' => '0px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '0' ) ),
			array( 'title' => '10px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '10px' ) ),
			array( 'title' => '15px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '15px' ) ),
			array( 'title' => '20px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '20px' ) ),
			array( 'title' => '25px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '25px' ) ),
			array( 'title' => '30px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '30px' ) ),
			array( 'title' => '40px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '40px' ) ),
			array( 'title' => '50px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '50px' ) ),
			array( 'title' => '60px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-top' => '60px' ) ),
			)
		),
		array( 'title' => 'margin-bottom', 'items' => array(
			array( 'title' => '0px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '0px' ) ),
			array( 'title' => '10px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '10px' ) ),
			array( 'title' => '15px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '15px' ) ),
			array( 'title' => '20px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '20px' ) ),
			array( 'title' => '25px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '25px' ) ),
			array( 'title' => '30px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '30px' ) ),
			array( 'title' => '40px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '40px' ) ),
			array( 'title' => '50px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '50px' ) ),
			array( 'title' => '60px', 'selector' => 'h1,h2,h3,h4,h5,h6,p,span,i,b,strong,em,div', 'styles' => array( 'margin-bottom' => '60px' ) ),
			)
		),
		array( 'title' => 'Underline title', 'items' => array(
			array( 'title' => 'gray line', 'selector' => '.ce_title:not(.und-title.white):not(.und-title.themecolor)', 'classes' => 'und-title gray' ),
			array( 'title' => 'white line', 'selector' => '.ce_title:not(.und-title.themecolor):not(.und-title.gray)', 'classes' => 'und-title white' ),
			array( 'title' => 'theme color line', 'selector' => '.ce_title:not(.und-title.white):not(.und-title.gray)', 'classes' => 'und-title themecolor' ),
			)
		),
		array( 'title' => 'SVG Divider', 'selector' => 'h1,h2,h3,h4,h5,h6', 'classes' => 'div_title' ),
		array( 'title' => 'Borderless image', 'selector' => 'img', 'classes' => 'noborder' ),
		array( 'title' => 'Animation On Hover', 'items' => array(
			array( 'title' => 'To top', 'selector' => 'a,img', 'classes' => 'shadow_image top' ),
			array( 'title' => 'To bottom', 'selector' => 'a,img', 'classes' => 'shadow_image bottom' ),
			)
		),
		array( 'title' => 'Border Radius Image', 'items' => array(
			array( 'title' => '1px', 'selector' => 'img', 'styles' => array( 'border-radius' => '1px' )),
			array( 'title' => '2px', 'selector' => 'img', 'styles' => array( 'border-radius' => '2px' )),
			array( 'title' => '3px', 'selector' => 'img', 'styles' => array( 'border-radius' => '3px' )),
			array( 'title' => '4px', 'selector' => 'img', 'styles' => array( 'border-radius' => '4px' )),
			array( 'title' => '5px', 'selector' => 'img', 'styles' => array( 'border-radius' => '5px' )),
			array( 'title' => '6px', 'selector' => 'img', 'styles' => array( 'border-radius' => '6px' )),
			array( 'title' => '7px', 'selector' => 'img', 'styles' => array( 'border-radius' => '7px' )),
			array( 'title' => '8px', 'selector' => 'img', 'styles' => array( 'border-radius' => '8px' )),
			array( 'title' => '9px', 'selector' => 'img', 'styles' => array( 'border-radius' => '9px' )),
			array( 'title' => '10px', 'selector' => 'img', 'styles' => array( 'border-radius' => '10px' )),
			array( 'title' => '11px', 'selector' => 'img', 'styles' => array( 'border-radius' => '11px' )),
			array( 'title' => '12px', 'selector' => 'img', 'styles' => array( 'border-radius' => '12px' )),
			array( 'title' => '13px', 'selector' => 'img', 'styles' => array( 'border-radius' => '13px' )),
			array( 'title' => '14px', 'selector' => 'img', 'styles' => array( 'border-radius' => '14px' )),
			array( 'title' => '15px', 'selector' => 'img', 'styles' => array( 'border-radius' => '15px' )),
			array( 'title' => '16px', 'selector' => 'img', 'styles' => array( 'border-radius' => '16px' )),
			array( 'title' => '17px', 'selector' => 'img', 'styles' => array( 'border-radius' => '17px' )),
			array( 'title' => '18px', 'selector' => 'img', 'styles' => array( 'border-radius' => '18px' )),
			array( 'title' => '19px', 'selector' => 'img', 'styles' => array( 'border-radius' => '19px' )),
			array( 'title' => '20px', 'selector' => 'img', 'styles' => array( 'border-radius' => '20px' )),

		))
		);
		// Before 3.1 you needed a special trick to send this array to the configuration.
		// See this post history for previous versions.
		$settings['style_formats'] = str_replace( '"', "'", json_encode( $style_formats ) );

		return $settings;
	}

	public function cws_print_single_class($class) {
		$class .= ' page_content';
		$footer = $this->cws_get_meta_option('footer');
		$wide_featured = $this->cws_get_meta_option('wide_featured');

		$class .= isset($footer['fixed']) && $footer['fixed'] == '1' ? ' fixed' : '';
		$class .= isset($wide_featured) && $wide_featured == '1' ? ' wide_featured' : '';
		return $class;
	}

	public function cws_print_metas() {
		$this->echo_if( has_category(), '<div class="post_categories">' . get_the_category_list ( $this::THEME_V_SEP ) . '</div>');
		$this->echo_if( has_tag(), '<div class="post_tags">' . get_the_tag_list (null, $this::THEME_V_SEP, null ) . '</div>');
	}

	public function cws_get_page_meta_var ( $keys ) {
		$p_meta = array();
		if ( isset( $GLOBALS[self::$text_domain . '_page_meta'] ) && !empty($keys) ) {
			$p_meta = $GLOBALS[self::$text_domain . '_page_meta'];
			if ( is_string( $keys ) ) {
				if ( isset( $p_meta[$keys] ) ) {
					return $p_meta[$keys];
				}
			} else if ( is_array( $keys ) ) {
				for ( $i=0; $i < count($keys); $i++ ) {
					if ( isset( $p_meta[$keys[$i]] ) ) {
						if ( $i < count($keys) - 1 ) {
							if ( is_array( $p_meta[$keys[$i]] ) ) {
								$p_meta = $p_meta[$keys[$i]];
							}	else {
								return false;
							}
						}	else {
							return $p_meta[$keys[$i]];
						}
					}	else {
						return false;
					}
				}
			}
		}
		return false;
	}

	public function cws_set_page_meta_var($keys, $value = '') {
		$p_meta = array();
		if (isset($GLOBALS[self::$text_domain . '_page_meta']) && !empty($keys) ) {
			$p_meta = &$GLOBALS[self::$text_domain . '_page_meta'];

			if ( is_string( $keys ) ) {
				if ( isset($p_meta[$keys]) ) {
					$p_meta[$keys] = $value;
					return true;
				}
			} else if ( is_array( $keys ) && !empty( $keys ) ) {
				for ( $i=0; $i < count($keys); $i++ ) {
					if ( isset( $p_meta[$keys[$i]] ) ) {
						if ( $i < count($keys) - 1 ) {
							if ( is_array( $p_meta[$keys[$i]] ) ) {
								$p_meta = &$p_meta[$keys[$i]];
							} else {
								return false;
							}
						}	else {
							$p_meta[$keys[$i]] = $value;
							return true;
						}
					}	else {
						return false;
					}
				}
			}
		}
		return false;
	}

	/* HEDER LOADER */
	public function cws_page_loader() {
		$cws_enable_page_loader = $this->cws_get_meta_option( 'show_loader' );
		if (!empty($cws_enable_page_loader)) {

			$loader_logo = $this->cws_get_option( 'loader_logo' );
			if (isset( $loader_logo['id'] )){
				$logo_get = wp_get_attachment_image_src($loader_logo['id'], 'full');
				$loader_logo['height'] = $logo_get[2];
				$loader_logo['width'] = $logo_get[1];				
			}

			$logo_is_high_dpi = (!empty($loader_logo['logo_is_high_dpi']) ? $loader_logo['logo_is_high_dpi'] : '');

			if ( isset( $loader_logo['src'] ) ) {
				$logo_src = '';
				$logo_class = ' class="loader_logo"';
				$main_logo_height = '';

				if ( isset( $loader_logo['src'] ) && ( ! empty( $loader_logo['src'] ) ) ) {

					if ( $logo_is_high_dpi ) {
						$thumb_obj = cws_thumb( $loader_logo['id'], array( 'width' => 120, 'crop' => false ) );
					} else {
						$thumb_obj = cws_thumb( $loader_logo['id'], array( 'width' => 60, 'height' => 60, 'crop' => false ) );
					}

					$thumb_path_hdpi = !empty($thumb_obj[3]) ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
					$logo_src = $thumb_path_hdpi;

				}
			}
			return '<div id="cws_page_loader_container" class="cws_loader_container">
				<div id="cws_page_loader" class="cws_loader"><div class="inner"></div>'.( (!empty($logo_src)) ? "<img $logo_class $logo_src alt />" : '').'</div>
			</div>';
		}
	}
	/* END HEDER LOADER */

	/* THE HEADER META */
	public function cws_header_meta() {
		?>
			<meta charset="<?php bloginfo( 'charset' ); ?>">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<link rel="profile" href="http://gmpg.org/xfn/11">
			<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
		<?php
			$this->cws_read_options();
	}
	/* END THE HEADER META */

	/* THEME HEADER */
	public function cws_page_header() {
		$pid = get_the_id();

		$font_color_style = $animate_title = '';
		$title_box_use_blur = 0;
		$title_box_slide_down = null;
		$title_box_blur_intensity = $title_box_parallaxify = $bg_header_scalar_x = $bg_header_scalar_y = $bg_header_limit_x = $bg_header_limit_y = $title_box_pattern_image = $title_box_use_blur_style = '';
		$title_box_overlay = $title_box_overlay_gradient = $parallax_opt_arr = array();
		$title_box_parallaxify_atts = $title_box_parallaxify_layer_atts = '';

		$title_box_spacings = $title_box_animate_options = array();
		$title_box_font_color = '';

		//Get metaboxes from page
		$sticky_menu = $this->cws_get_meta_option( 'sticky_menu' );
		extract($sticky_menu, EXTR_PREFIX_ALL, 'sticky_menu');

		$title_box = $this->cws_get_meta_option( 'title_box' );
		extract($title_box, EXTR_PREFIX_ALL, 'title_box');

		$custom_header_bg_color = false;
		$post_type = get_post_type();

		$img_section_atts = $img_section_styles = '';
		$img_section_atts .= ' class="header_bg_img"';

		$header_outside_slider = $this->cws_get_meta_option('header')['outside_slider'] == '1' && !is_single() && !is_archive() || $this->cws_get_option( 'shop_slider_type' ) != 'none' && $this->cws_is_woo();

		$show_header_shop_slider = $this->cws_get_option( 'shop_slider_type' ) != 'none' && $this->cws_is_woo() && is_shop();
		if($show_header_shop_slider){
			if($this->cws_get_option('woo_header_covers_slider')){
				$header_outside_slider = true;
			}else{
				$header_outside_slider = false;
			}
		}

		/***** Boxed Layout *****/
		$boxed_enable = $this->cws_get_option('boxed_layout')['enable'] == '1' || $this->cws_get_meta_option('boxed')['enable'] == '1';
		if ($boxed_enable){	echo '<div class="page_boxed">'; }
		/***** \Boxed Layout *****/

		$top_panel_content = $this->cws_render_top_bar($pid);

		$this->header['top_bar_box'] = $top_panel_content;

		$title_box_customize = isset($title_box_customize) && $title_box_customize == '1';
		$title_box_no_title = isset($title_box_no_title) ? $title_box_no_title : '';
		$woo_customize_title = $this->cws_get_option('woo_customize_title');

		if ($title_box_customize) {
			$is_single_or_archive = is_single() || is_archive();
			$is_show_on_post = is_single() && $this->cws_get_option('title_box')['show_on_posts'] == '1' && !$this->cws_is_woo(); //Call from ThemeOptions
			$is_show_on_archive = is_archive() && $this->cws_get_option('title_box')['show_on_archives'] == '1' && !$this->cws_is_woo(); //Call from ThemeOptions

			if($is_single_or_archive){
				if($this->cws_is_woo()){
					$is_woo_breadcrumbs = false;
				}
				else{
					$is_woo_breadcrumbs = true;
				}

			}else{
				$is_woo_breadcrumbs = true;
			}

			if(!$is_show_on_archive && get_post_type() == 'tribe_events'){
				$is_woo_breadcrumbs = false;
			}
			if ($is_show_on_post && get_post_type() == 'tribe_events') {
				$is_show_on_post = false;
			}

			$is_customized_title = !$is_single_or_archive ^ $is_show_on_post ^ $is_show_on_archive ^ !$is_woo_breadcrumbs;


			$title_box_spacings = $is_customized_title ? $title_box_spacings : array();
			$title_box_animate = $is_customized_title ? $title_box_animate : '';
			$title_box_animate_options = $title_box_animate ? $title_box_animate_options : array();
		}

		if($this->cws_is_woo() && !empty($woo_customize_title)){
			$woo_header_font_color = $this->cws_get_option('woo_header_font_color');
			$title_box_font_color = !empty($woo_header_font_color) ? $woo_header_font_color : $title_box_font_color;
		}

		if($this->cws_is_woo() && !empty($woo_customize_title)){
			$title_box_spacings = $this->cws_get_option('woo_page_title_spacings');
		}

		$page_title_content = $this->cws_build_page_title($title_box_no_title,$title_box_font_color, $title_box_spacings, $title_box_animate_options);

		ob_start();
		echo "<!-- title_box -->";

			$bg_header = false;
			$header_image_exist = false;

			if ($title_box_customize) {
				$title_box_parallaxify = (isset($title_box_effect) && $title_box_effect === 'hover_parallax');
				$title_box_use_blur_style = "-webkit-filter:blur(".esc_attr($title_box_blur_intensity)."px);-moz-filter:blur(".esc_attr($title_box_blur_intensity)."px);-o-filter: blur(".esc_attr($title_box_blur_intensity)."px);-ms-filter:blur(".esc_attr($title_box_blur_intensity)."px);filter: blur(".esc_attr($title_box_blur_intensity)."px);";

				extract($title_box_overlay, EXTR_PREFIX_ALL, 'title_box_overlay');
				if (!empty($title_box_overlay) && $title_box_overlay_type != 'none'){
					$title_box_overlay_opacity = (int) $title_box_overlay_opacity / 100;
				}

				if (isset($title_box_parallax_options)){
					$this->cws_print_parallaxify_atts($title_box_parallax_options, $title_box_parallaxify_atts, $title_box_parallaxify_layer_atts);
				}
			}

			$bg_header_feature = false;
			$bg_header_url = '';

			if (!empty( $title_box_background_image['image']['src'] )){
				$bg_header_url = $title_box_background_image;
				$header_image_exist = true;
			}

			$bg_header_html = '';

			if (
				(
					((is_archive() && $title_box_customize) ? $is_show_on_archive : true ) &&
					((is_single() && (get_post_type() == 'post' && $title_box_customize)) ? $is_show_on_post : true ) &&
					($title_box_customize || isset($meta_title_area) || isset( $bg_header_url ) ) &&
					!(get_post_type() == 'cws_staff')
					/*&& ! (get_post_type() == 'cws_portfolio')*/
					// Uncomment this line if you need to exclude portfolio post
				)
			|| ($this->cws_is_woo())
				){

				if ( ($title_box_customize && ($title_box_overlay_type != 'none' || $title_box_use_pattern == '1') ))
				{
					if (
						( $title_box_use_pattern && !empty( $title_box_pattern_image ) && isset( $title_box_pattern_image['image']['src'] ) && !empty( $title_box_pattern_image['image']['src'] )  )
						|| ( $title_box_overlay_type == 'color' && !empty( $title_box_overlay_color ) )
						|| ( $title_box_overlay_type == 'gradient' )
					)
					{
						$header_image_exist = true;
						$bg_header_html .= "<div class='bg_layer'></div>";
					}
				}
			}

				if ( !empty( $bg_header_url ) ) {
					$header_bg_atts = '';
					foreach ( $title_box_spacings as $key => $value ) {
						switch ($key) {
							case 'top':
							case 'bottom':
							if ( !empty( $value ) ) {
								$header_bg_atts .= " data-$key='".esc_attr($value)."'";
							}
							break;
						}
					}

					$title_box_background_image_style = isset($bg_header_url) && !empty($bg_header_url) ? $this->cws_print_background($title_box_background_image) : '';
					echo "<div class='title_box bg_page_header".(isset($title_box_border['line']) && $title_box_border['line'] == '1' ? ' border_line' : '').( $title_box_slide_down ? ' hide_header' : '')."'".$header_bg_atts.">";

						if ($page_title_content){ printf('%s', $page_title_content); }
						if ($title_box_parallaxify){ printf('<div class="cws_parallax_section" %s>', $title_box_parallaxify_atts); }
							wp_enqueue_script ('parallax');

							if ($title_box_parallaxify) { echo '<div class="layer" data-depth="1.00">'; }
								printf('%s', $bg_header_html);
									echo "<div class='stat_img_cont".
									( (isset($meta_title_area) && $title_box_effect == 'scroll_parallax') || ($title_box_customize && $title_box_effect == 'scroll_parallax') ? ' title has_fixed_background' : '').
									( ((isset($meta_title_area) && $title_box_effect == 'scroll_parallax' && $title_box_scroll_parallax == '1') || ($title_box_customize && $title_box_effect == 'scroll_parallax' && $title_box_scroll_parallax == '1')) ? ' zoom_out' : '').
									"'></div>";
								if ($title_box_parallaxify) {
									echo '</div>';
								echo '</div>';
								}
					echo '</div>';
				}

				if ( (empty( $bg_header_url ) && ($title_box_customize )) || (empty( $bg_header_url ) && !empty($page_title_content) ) ) {
					echo "<div class='title_box bg_page_header".(isset($title_box_border['line']) && $title_box_border['line'] == '1' ? ' border_line' : '').((isset($meta_title_area) && $title_box_slide_down == '1') ? ' hide_header' : '')."'".(!empty($font_color) ? ' style="color:'.esc_attr($font_color).';"' : '').(!empty($custom_header_bg_spacings["top"]) ? " data-top='".esc_attr($custom_header_bg_spacings['top'])."'" : '').(!empty($custom_header_bg_spacings["bottom"]) ? " data-bottom='".esc_attr($custom_header_bg_spacings['bottom'])."'" : '').">";
						if ($page_title_content){ printf('%s', $page_title_content); }
						printf('%s', $bg_header_html);
					echo '</div>';
				}
		echo "<!-- /title_box -->";
		$title_box_content = ob_get_clean();

		ob_start();
			$is_revslider_active = is_plugin_active( 'revslider/revslider.php' );
			$cws_revslider_content = '';
			$slider_shortcode = '';
			$slider_exist = false;
			$slider_error = false;
			//If Home page
			if ( is_front_page() ){
				cws_render_slider_section('page', $slider_exist, $slider_error, $slider_shortcode);
			}
			else if ( is_page() || $this->cws_is_woo() && is_shop() ){
				//If WooCommerce page
				if($this->cws_is_woo() && is_shop()){
					cws_render_slider_section('shop', $slider_exist, $slider_error, $slider_shortcode);
				} else { //If Simple page
					cws_render_image_slider('page', $slider_exist, $slider_error, $slider_shortcode);
				}
			}
			else if ( is_single() ){}
			else if ( is_archive() ){}

		/*
		There we can check various pages types:
			cryptop_is_woo()
			is_page()
			is_home()
			is_front_page()
			is_category()
			is_tag()
			is_archive()
			is_search()
			is_single()
			get_post_type() == 'post'
			get_post_type() == 'cws_staff'
			get_post_type() == 'cws_portfolio'
		*/

		$slider_content = ob_get_clean();

		if(isset($title_box_enable) && $title_box_enable == '1' && !$slider_exist){
			$this->header['title_box'] = $title_box_content;
		} else {
			$this->header['title_box'] = '';
		}

		$bg_header = $header_image_exist || $slider_exist;

		$args = array(
			'slider_content' => $slider_content,
			'bg_header' => $bg_header
		);

		$this->cws_header_menu_and_logo($args);

		wp_add_inline_script('img_loaded', 'window.header_after_slider=false;');

		//Get metaboxes from page
		//Header (General)
		$header = $this->cws_get_meta_option('header');
		extract($header, EXTR_PREFIX_ALL, 'header');

		ob_start();

			?>

			<div class="header_zone"><!-- header_zone -->
				<?php
				if ( $header_customize == '1' && $header_overlay['type'] != 'none' ){
					echo "<div class='header_overlay'></div>";
				}

		$header_zone = ob_get_clean();
		$this->header['drop_zone_start'] = $header_zone;

		$header_outside_slider = $this->cws_get_meta_option('header')['outside_slider'] == '1';

		//Render Header from parts
		ob_start();

		echo '<div class="megamenu_width"><div class="container"></div><div class="container wide_container"></div></div>';
		echo '<div class="header_wrapper'.(($header_outside_slider && $bg_header == true ) ? ' outside_slider' : '').'">';
			print $this->header['before_header'];

			if ( isset( $header_order ) ){
				foreach ($header_order as $key => $value){
					$field = isset($value['val']) ? $value['val'] : (isset($value['field']) ? $value['field'] : '');

					if (($header_outside_slider && $field == 'title_box' )  ) continue;
					print $this->header[$field];
				}
			}
			print $this->header['after_header'];
		echo '</div>';

		if (($header_outside_slider) ) print $this->header['title_box'];

		$header = ob_get_clean();
		printf('%s',$header);

		if (isset($slider_error) && $slider_error){
			$slider_content = "<div class='rev_slider_error'><div class='message'>Revolution Slider Error: Slider $slider_shortcode not found.</div></div>";
		}

		echo (!empty($slider_content) ? $slider_content : '');
	}

	public function cws_build_page_title($no_title, $font_color, $title_box_spacings, $animate_options) {
		$post_type = get_post_type();

		if ($animate_options){
			wp_enqueue_script ('skrollr','jquery');
		}

		$page_title_section_atts = '';
		$page_title_section_class = "page_title".($animate_options ? ' animate_title' : '');
		$page_title_section_class .= $no_title == '1' ? ' no_title' : '';
		$page_title_section_class .= $title_box_spacings ? (!empty($title_box_spacings['top']) || !empty($title_box_spacings['bottom'])) ? ' custom_spacing' : '' : '';
		$page_title_section_atts = !empty( $page_title_section_class ) ? " class='".esc_attr($page_title_section_class)."'" : '';
		$page_title_section_atts .= !empty( $page_title_section_styles ) ? " style='".esc_attr($page_title_section_styles)."'" : '';

		$page_title_container_styles = '';

		foreach ( $title_box_spacings as $key => $value ) {
			if ( !empty( $value ) ) {
				$page_title_container_styles .= "padding-".esc_attr($key).":".esc_attr($value)."px;";
				$page_title_section_atts .= " data-init-".esc_attr($key)."='".esc_attr($value)."'";
			}
		}

		$post_id = get_the_id();
		$post_meta = get_post_meta( $post_id, 'cws_mb_post' );
		$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();
		$apply_color = isset($post_meta['apply_color']) && !empty($post_meta['apply_color']) ? $post_meta['apply_color'] : "";

		$post_title_color = isset($post_meta['post_title_color']) && !empty($post_meta['post_title_color']) ? $post_meta['post_title_color'] : "";
		if($apply_color == 'list_color' ){
			$post_title_color = '';
		}

		$page_title_container_styles = $this->print_ne($page_title_container_styles, ' style="' . esc_attr($page_title_container_styles) . '"');
		$show_breadcrumbs = $this->cws_get_option( 'breadcrumbs' ) == '1';
		$page_title = $this->cws_get_page_title($post_type);

		// Animate options
		$animate_title_options = $animate_container_options = $animate_section_options = '';
		if ($animate_options) {
			foreach ($animate_options as $key => $value) {
				$field = isset($value['value']) ? $value['value'] : (isset($value['selector']) ? $value['selector'] : '');
				$element = isset($value['element']) ? $value['element'] : (isset($value['selector']) ? $value['selector'] : '');

				$data_header = " data-_header-{$field}='{$value['styles']}'";
				switch ($element) {
					case 'title':
						$animate_title_options .= $data_header;
						break;
					case 'container':
						$animate_container_options .= $data_header;
						break;
					case 'section':
						$animate_section_options .= $data_header;
						break;
				}
			}
		}

		$breadcrumbs = '';
		if ( $show_breadcrumbs ){
			$alt_bc = self::$cws_theme_config['alt_breadcrumbs'];
			reset($alt_bc);
			$key = key($alt_bc);
			if (!empty($alt_bc) && function_exists($key)) {
				$breadcrumbs = call_user_func_array($key, $alt_bc[$key]);
			} else {
				ob_start();
				cryptop_dimox_breadcrumbs($animate_title_options);
				$breadcrumbs = ob_get_clean();
			}
		}

		$page_title = esc_html($page_title);

		$title_box = $this->cws_get_meta_option('title_box');

		$breadcrumbs_logo = isset($title_box['breadcrumbs_divider']) ? $title_box['breadcrumbs_divider'] : "";
		$woo_customize_title = $this->cws_get_option('woo_customize_title');
		if($this->cws_is_woo() && !empty($woo_customize_title)){
			$breadcrumbs_logo_divider = $this->cws_get_option('woo_breadcrumbs_divider');
			$breadcrumbs_logo = !empty($breadcrumbs_logo_divider) ? $breadcrumbs_logo_divider : "";
		}

		$logo_exists = false;
		$diver_logo = array();
		if ( !empty( $breadcrumbs_logo['src'] ) ) {
			$logo_exists = true;
			$logo_hw = isset($title_box['breadcrumbs_dimensions']) ? $title_box['breadcrumbs_dimensions'] : "";
			if($this->cws_is_woo() && !empty($woo_customize_title)){
				$logo_hw = $this->cws_get_option('woo_breadcrumbs_dimensions');
			}
			$bfi_args = array();

			if ( is_array( $logo_hw ) ) {
				foreach ( $logo_hw as $key => $value ) {
					if ( ! empty( $value ) ) {
						$bfi_args[ $key ] = $value;
						$bfi_args['crop'] = true;
					}
				}
			}

			$logo_m = isset($title_box['breadcrumbs-margin']) ? $title_box['breadcrumbs-margin'] : "";
			if($this->cws_is_woo() && !empty($woo_customize_title)){
				$logo_m = $this->cws_get_option('woo_breadcrumbs_dimensions');
			}
			if (!empty($logo_hw)){
				foreach ($logo_hw as $key => $value) {
					if ( !empty($value) ){
						$bfi_args[$key] = (int)$value;
						$bfi_args['crop'] = true;
					}
				}
			}

			if(!empty($breadcrumbs_logo['src'])){
				$file_parts = pathinfo($breadcrumbs_logo['src']);

				if($file_parts['extension'] == 'svg'){
					$diver_logo['svg'] = $this->cws_print_svg_html($breadcrumbs_logo, $bfi_args);
				}else{
					$diver_logo['img'] = $this->cws_print_img_html($breadcrumbs_logo, $bfi_args);
				}
			}
			$logo_lr_spacing = $logo_tb_spacing = '';
			if ( is_array( $logo_m ) ) {
				$logo_lr_spacing = $this->cws_print_css_keys($logo_m, 'margin-', 'px');
				$logo_tb_spacing = $this->cws_print_css_keys($logo_m, 'padding-', 'px');
			}
		}

		$out = '';

		if ( !is_front_page() ) {

			$header_center = $this->print_if($title_box['text_center'] == '1', ' header_center');
			if($this->cws_is_woo() && !empty($woo_customize_title)){
				$header_center = $this->print_if($this->cws_get_option('woo_header_center') == '1', ' header_center');
			}
			$out .= '<section' . $page_title_section_atts . $animate_section_options . '>';
			$out .= '<div class="container' . $header_center . '"';
			$out .= $page_title_container_styles . $animate_container_options . '>';
			$font_color = !empty($post_title_color) ? $post_title_color : $font_color;
			$out .= '<div class="title"><h1' . $this->print_ne( $font_color, ' style="color:'.esc_attr($font_color).';"');
			$out .= $animate_title_options . '>';
			if ($logo_exists){

				$spacing = !empty( $logo_lr_spacing ) ? " style='{$logo_lr_spacing}'" : '';
				$out .= sprintf('<span%s class="logo_breadcrumbs svg_lotus">', $spacing);
				if(!empty($diver_logo)){
					foreach ($diver_logo as $key => $value) {
						switch ($key) {
							case 'img' :
							$out .= '<img '. $diver_logo[$key] .' alt />';
							break ;
							case 'svg' :
							$out .= $diver_logo[$key];
							break ;
						}
					}
				}
				$out .= '</span>';
			}
			$out .= esc_html($page_title) . '</h1></div>';
			if(is_page() && !empty($post->post_excerpt)){
				$out .= "<div class='page_excerpt'>".$post->post_excerpt."</div>";
			}
			$out .= $breadcrumbs . '</div></section>';

		}

		return $out;
	}

	public function cws_get_page_title($post_type) {
		$page_title = '';
		if ( is_404() ) {
			$page_title = self::$cws_theme_config['strings']['404'];
		} else if ( is_search() ) {
			$page_title = self::$cws_theme_config['strings']['search'];
		} else if ( is_front_page() ) {
			$page_title = self::$cws_theme_config['strings']['home'];
		} else if ( is_category() ) {
			$cat = get_category( get_query_var( 'cat' ) );
			$cat_name = isset( $cat->name ) ? $cat->name : '';
			$page_title = sprintf( self::$cws_theme_config['strings']['category'], $cat_name );
		} else if ( is_tag() ) {
			$page_title = sprintf( self::$cws_theme_config['strings']['tag'], single_tag_title( '', false ) );
		} else if ( is_day() ) {
			$page_title = get_the_time( get_option('date_format') );
		} else if ( is_month() ) {
			$page_title = get_the_time( 'F, Y' );
		} else if ( is_year() ) {
			$page_title = get_the_time( 'Y' );
		} else if ( has_post_format() && !is_singular() ) {
			$page_title = get_post_format_string( get_post_format() );
		} else if ( is_tax( array( 'cws_portfolio_cat', 'cws_staff_member_department', 'cws_staff_member_position' ) ) ) {
			$tax_slug = get_query_var( 'taxonomy' );
			$term_slug = get_query_var( $tax_slug );
			$tax_obj = get_taxonomy( $tax_slug );
			$term_obj = get_term_by( 'slug', $term_slug, $tax_slug );

			$singular_tax_label = isset( $tax_obj->labels ) && isset( $tax_obj->labels->singular_name ) ? $tax_obj->labels->singular_name : '';
			$term_name = isset( $term_obj->name ) ? $term_obj->name : '';
			$page_title = $singular_tax_label . ' ' . $term_name ;
		} else if ( function_exists ( 'is_shop' ) && is_shop() ) {
		  $page_title = woocommerce_page_title(false);
		} else if ( is_archive() ) {
			$post_type_obj = get_post_type_object( $post_type );
			$post_type_name = isset( $post_type_obj->label ) ? $post_type_obj->label : '';
			$page_title = $post_type_name ;
		} else if ( $this->cws_is_woo() ) {
			if(is_cart()){
				$page_title = self::$cws_theme_config['strings']['cart'];
			}else if(is_checkout()){
				$page_title = self::$cws_theme_config['strings']['checkout'];
			}else{
				$page_title = woocommerce_page_title( false );
			}
		} else if (substr($post_type, 0, 4) === 'cws_' ) {
			$slug_option = substr($post_type, 4) . '_slug'; // if post_type is cws_portfolio, this will turn it into portfolio_slug option name
			$portfolio_slug = $this->cws_get_option( $slug_option );
			$post_type_obj = get_post_type_object( $post_type );
			$post_type_name = $post_type_obj->labels->menu_name;
			$page_title = !empty($portfolio_slug) ? $portfolio_slug : $post_type_name ;
		} else {
			$blog_title = $this->cws_get_option('blog_title');
			$page_title = (!is_page() && !empty($blog_title)) ? $blog_title : get_the_title();
		}
		return $page_title;
	}

	/* Social Links */
	public function cws_render_social_links($social_location = '', $social_place = 'left') {
		$out = '';
		$social = $this->cws_get_option('social'); //Call from ThemeOptions
		$location = isset($social['location']) ? $social['location'] : '';
		$icons = isset($social['icons']) ? $social['icons'] : '';
		$el_atts = '';

		if ((!empty($icons) && !empty($location) && in_array($social_location, $location))) {
			$menu_box_search_place = $this->cws_get_meta_option('menu_box')['search_place'];
			$side_panel_place = $this->cws_get_meta_option('side_panel')['place'];
			$links = null;

			$styles = '';
			foreach ( $icons as $icon ) {
				$icon_id = uniqid( "cws_social_icon_" );
				$title = esc_attr($icon['title']);
				$url = !empty($icon['url']) ? $icon['url'] : '#';
				$links .= "<a id='".esc_attr($icon_id)."' href='".esc_url($url)."' class='cws_social_link ".esc_attr($icon['icon'])."' title='".esc_attr($title)."' target='_blank'></a>";
				$styles .= "
					#top_social_links_wrapper .cws_social_links #".esc_attr($icon_id)."{
						color: ".esc_attr($icon['color']).";
						background-color : ".esc_attr($icon['bg_color']).";
					}

					#top_social_links_wrapper .cws_social_links #".esc_attr($icon_id).":hover{
						color: ".esc_attr($icon['hover_color']).";
						background-color : ".esc_attr($icon['hover_bg_color']).";
					}
				";
			}
			Cws_shortcode_css()->enqueue_cws_css($styles);

			if ($links) {
				$social_class = ($menu_box_search_place == 'top' || $this->cws_get_option('woo_cart_place') == 'top' || $side_panel_place == 'topbar_right') ? 'social-divider' : '';
				$out = "<div ".( !empty( $el_atts ) ? $el_atts : '' )." class='cws_social_links ".esc_attr($social_class)."'>{$links}</div>";
			}
		}
		return $out;
	}
	/* \Social Links */

	//public function

	public function cws_header_menu_and_logo ($args = array() ) {
		extract($args);

		//Get metaboxes from page
		$top_bar_box = $this->cws_get_meta_option( 'top_bar_box' );
		extract($top_bar_box, EXTR_PREFIX_ALL, 'top_bar_box');

		$menu_box = $this->cws_get_meta_option( 'menu_box' );
		extract($menu_box, EXTR_PREFIX_ALL, 'menu_box');

		$sticky_menu = $this->cws_get_meta_option( 'sticky_menu' );
		extract($sticky_menu, EXTR_PREFIX_ALL, 'sticky_menu');

		$logo_box = $this->cws_get_meta_option( 'logo_box' );
		extract($logo_box, EXTR_PREFIX_ALL, 'logo_box');

		$side_panel = $this->cws_get_meta_option( 'side_panel' );
		extract($side_panel, EXTR_PREFIX_ALL, 'side_panel');

		$mobile_menu = $this->cws_get_meta_option( 'mobile_menu' );

		$mobile_menu_class = 'mobile_menu_wrapper';
		$mobile_menu_class .= (!empty($mobile_menu['theme']) ? ' '.esc_attr($mobile_menu['theme']) : '');
		$mobile_menu_class .= (!empty($mobile_menu['enable_on_tablet']) && $mobile_menu['enable_on_tablet'] == '1' ? ' show_on_tablets' : '');

		$woo_mini_cart = $this->cws_getWooMiniCart();
		$woo_mini_icon = $this->cws_getWooMiniIcon();

		if ($mobile_menu['enable_on_tablet']){
			wp_enqueue_script ('modernizr');
		}

		/*** Logo Position ***/
		$header_class = 'site_header';
		$header_class .= $sticky_menu_enable ? ' sticky_enable sticky_'.$sticky_menu_mode : '';
		$header_class .= $sticky_menu_enable && $sticky_menu_shadow ? ' sticky_shadow' : '';

		$header_class .= !empty( $logo_box_position ) ? ' logo-'.esc_attr($logo_box_position) : '';
		$header_class .= !empty( $menu_box_position ) ? ' menu-'.esc_attr($menu_box_position) : '';
		$header_class .= !empty( $logo_box_in_menu ) && $logo_box_in_menu == '1' ? ' logo-in-menu' : ' logo-not-in-menu';
		$header_class .= $menu_box_sandwich ? ' active-sandwich-menu' : ' none-sandwich-menu';

		$social = $this->cws_get_option('social'); //Call from ThemeOptions
		$location = isset($social['location']) ? $social['location'] : '';
		$top_bar_box = $this->cws_get_meta_option( 'top_bar_box' );
		$social_place = $top_bar_box['social_place'];
		if (is_array($location)){
			$header_class .= in_array('top_bar', $location) || in_array('menu', $location) ? ' social-'. esc_attr($social_place) : '';
		} elseif (is_string($location)) {
			$header_class .= $location == 'top_bar' || $location == 'menu' ? ' social-'. esc_attr($social_place) : '';
		}
		/***** \Logo Position *****/

		/***** Menu Position *****/
		global $current_user;
		$menu_locations = get_nav_menu_locations();

		$show_wpml_menu = CWS_WPML_ACTIVE;
		/***** \Menu Position *****/

		$a_logos = array(); // array of main logo, mobile and sticky
		$bfi_args = $bfi_args_sticky = $bfi_args_mobile = array();
		if ($logo_box_enable) {
			ob_start();
				/***** Logo Settings *****/
				// TODO: need to add some filter to get proper logo in case there are more than one option
				$woo_customize_logotype = $this->cws_get_option('woo_customize_logotype');
				if($this->cws_is_woo() && !empty($woo_customize_logotype)){
					$logo_box_default = 'logo_woo';
				}

				$logo_class = '';
				$logo_lr_spacing = $logo_tb_spacing = $main_logo_height = '';

				$logo = isset($this->cws_get_option( 'logo_box' )[$logo_box_default]) ? $this->cws_get_option( 'logo_box' )[$logo_box_default] : ''; //Call from ThemeOptions

				$logo_woo = $this->cws_get_option( 'logo_woo' );
				if ($this->cws_get_option( 'woo_customize_logotype' ) == '1' && $this->cws_is_woo() ) {
					$logo = isset($logo_woo) ? $logo_woo : ''; //Call from ThemeOptions -> WooCommerce
				}

				$mobile_menu_logo = isset($this->cws_get_option( 'mobile_menu' )['logo']) ? $this->cws_get_option( 'mobile_menu' )['logo'] : ''; //Call from ThemeOptions
				$sticky_menu_logo = isset($this->cws_get_option( 'sticky_menu' )['logo']) ? $this->cws_get_option( 'sticky_menu' )['logo'] : ''; //Call from ThemeOptions

				$sticky_menu_dimensions_sticky = isset($this->cws_get_option( 'sticky_menu' )['dimensions_sticky']) ? $this->cws_get_option( 'sticky_menu' )['dimensions_sticky'] : ''; //Call from ThemeOptions
				$mobile_menu_dimensions_mobile = isset($this->cws_get_option( 'mobile_menu' )['dimensions_mobile']) ? $this->cws_get_option( 'mobile_menu' )['dimensions_mobile'] : ''; //Call from ThemeOptions

				$logo_exists = false;
				if ( !empty( $logo['src'] ) ) {
					$logo_exists = true;

					if ( !empty($logo_box_dimensions) && is_array( $logo_box_dimensions ) ) {
						foreach ( $logo_box_dimensions as $key => $value ) {
							if ( ! empty( $value ) ) {
								$bfi_args[ $key ] = $value;
								$bfi_args['crop'] = false;
							}
						}
					}
					if ( !empty($sticky_menu_dimensions_sticky) && is_array( $sticky_menu_dimensions_sticky ) ) {
						foreach ( $sticky_menu_dimensions_sticky as $key => $value ) {
							if ( ! empty( $value ) ) {
								$bfi_args_sticky[ $key ] = $value;
								$bfi_args_sticky['crop'] = false;
							}
						}
					}
					if ( !empty($mobile_menu_dimensions_mobile) && is_array( $mobile_menu_dimensions_mobile ) ) {
						foreach ( $mobile_menu_dimensions_mobile as $key => $value ) {
							if ( ! empty( $value ) ) {
								$bfi_args_mobile[ $key ] = $value;
								$bfi_args_mobile['crop'] = false;
							}
						}
					}

					if(!empty($logo['src'])){
						$file_parts = pathinfo($logo['src']);

						if($file_parts['extension'] == 'svg'){
							$a_logos['logo']['svg'] = $this->cws_print_svg_html($logo, $bfi_args, $main_logo_height);
						}else{
							$a_logos['logo']['img'] = $this->cws_print_img_html($logo, $bfi_args, $main_logo_height);
						}
					}

					$logo_lr_spacing = $logo_tb_spacing = '';
					if ( is_array( $logo_box_spacing ) ) {
						$logo_lr_spacing = $this->cws_print_css_keys($logo_box_spacing, 'margin-', 'px');
						$logo_tb_spacing = $this->cws_print_css_keys($logo_box_spacing, 'padding-', 'px');
					}

					if (!empty($main_logo_height)) {
						$logo_lr_spacing .= "height:{$main_logo_height}px;";
						$main_logo_height = " style='height:{$main_logo_height}px;'";
						$a_logos['logo_h'] = $main_logo_height;
					}
				}

				/***** \Logo Settings *****/
				$logo_sticky_src = array();
				if ( !empty($sticky_menu_logo['src']) ) {
					$file_parts_sticky = pathinfo($sticky_menu_logo['src']);

					if($file_parts_sticky['extension'] == 'svg'){
						$logo_sticky_src['svg'] = $this->cws_print_svg_html($sticky_menu_logo, $bfi_args);
					}else{
						$logo_sticky_src['img'] = $this->cws_print_img_html($sticky_menu_logo['id'], (!empty($bfi_args_sticky) ? $bfi_args_sticky : null));
					}
					$logo_class .= ' custom_sticky_logo';
				}

				$logo_mobile_src = array();
				if ( !empty( $mobile_menu_logo['src']) ) {
					$file_parts_mobile = pathinfo($mobile_menu_logo['src']);
					if($file_parts_mobile['extension'] == 'svg'){
						$logo_mobile_src['svg'] = $this->cws_print_svg_html($mobile_menu_logo, $bfi_args);
					}else{
						$logo_mobile_src['img'] = $this->cws_print_img_html($mobile_menu_logo['id'], (!empty($bfi_args_mobile) ? $bfi_args_mobile : null));
					}
					$logo_class .= ' custom_mobile_logo';
				}
				$a_logos['mobile'] = $logo_mobile_src;
				$a_logos['sticky'] = $logo_sticky_src;

				//Logo box
				$esc_blog_name = esc_html(get_bloginfo('name'));

				$logo_box_class = 'logo_box';
				$logo_box_class .= ' header_logo_part';

				if ( (isset($logo_box_in_menu) && $logo_box_in_menu != '1') ) : ?>
				<!-- logo_box -->
					<div class="<?php echo esc_attr($logo_box_class); ?>">

						<div class="container<?php echo ($logo_box_in_menu != '1' && $logo_box_wide == '1' ? ' wide_container' : ''); ?>">
							<?php

							if (isset($logo_box_in_menu) && $logo_box_in_menu != '1'){
								if ($logo_exists){
									print $this->cws_print_logo_block($sticky_menu_enable, $logo_lr_spacing, $a_logos, $esc_blog_name);
								} else {
								?>
									<h1 class='header_site_title'><?php echo esc_html($esc_blog_name); ?></h1>
								<?php
								}
							}

							?>
						</div>
					</div>
				<?php endif; ?>
				<!-- /logo_box -->
			<?php
			$logo_box = ob_get_clean();
		}

		$this->header['logo_box'] = $logo_box;

		ob_start();

			echo "<div class='header_cont'>"; ?>
				<header <?php echo !empty($header_class) ? "class='".esc_attr($header_class)."'" : ''; ?>><!-- header -->
					<div class="header_container"><!-- header_container -->
						<?php
							$before_header_content = ob_get_clean();
							$this->header['before_header'] = $before_header_content;

							$menu_box_content = '';

							if ($menu_box_enable){

							ob_start();

							$menu_style = '';
							$menu_class = 'menu_box';
							$menu_class .= (isset($menu_box_border['line']) && $menu_box_border['line'] == '1' ? ' border_line' : '');
							$menu_attr = " class='".esc_attr($menu_class)."' ".(!empty($menu_style) ? 'style="'.$menu_style.'"' : '').'';
						?>

						<!-- menu_box -->
						<div<?php echo (!empty( $menu_attr )) ? $menu_attr : ''; ?>>
							<div class="container<?php echo ($menu_box_wide == '1'? ' wide_container' : ''); ?>">
								<?php

									$custom_menu = isset($menu_box_override_menu) && $menu_box_override_menu == '1' ? $menu_box_custom_menu : '';

									if ( !empty($menu_locations['header-menu']) || !empty($custom_menu) ) {
								?>

									<div class="header_nav_part">
										<div class="menu-overlay"></div>
										<nav class="main-nav-container">
											<?php

												/*NEW MENU*/

												// ------> Mobile logo
												echo '<div class="logo_mobile_wrapper">';
													print $this->cws_print_logo_mobile($a_logos);
												echo '</div>';

												// ------> Menu left
												echo '<div class="menu_left_icons">';
													// ------> Side panel
													if ($side_panel_place == 'menu_left' && $side_panel_enable){
														echo "<div class='side_panel_icon_wrapper'>";
															echo "<a href='#' class='side_panel_trigger ".esc_attr($side_panel_place)."'></a>";
														echo "</div>";
													}

													// ------> Menu hamburger
													if ($menu_box_mobile_place == "left"){
														echo "<div class='mobile_menu_hamburger'>";
															echo "<span class='hamburger_icon'></span>";
														echo "</div>";
													}

													// ------> Woo mini-cart
													if ( (class_exists('woocommerce') && $this->cws_get_option('woo_cart_place') == 'left') ){
														echo "<div class='mini-cart'>";
															echo sprintf('%s', $woo_mini_icon);
															echo sprintf('%s', $woo_mini_cart);
														echo "</div>";
													}

													// ------> Search
													if($menu_box_search_place == 'left'){
														echo "<div class='search_icon'></div>";
													}
												echo '</div>';

												// ------> Menu center
												ob_start();

													echo "<div class='menu_box_wrapper'>";

														if (($logo_box_position == "left" || $logo_box_position == "right") && $logo_box_enable && (isset($logo_box_in_menu) && $logo_box_in_menu == '1')) :
														?>
															<div class="menu_logo_part">
																<?php if ($logo_exists) {
																	print $this->cws_print_logo_block($sticky_menu_enable, $logo_lr_spacing, $a_logos, $esc_blog_name);
																}else{ ?>
																	<h1 class='header_site_title'><?php echo esc_html(get_bloginfo( 'name' )) ?></h1>
																<?php } ?>
															</div>

														<?php
														endif;

														wp_nav_menu( array(
															'menu_id'  => 'main_menu',
															'theme_location' => (!empty($custom_menu) ? '' : 'header-menu'),
															'menu' => (!empty($custom_menu) ? $custom_menu : ''),
															'menu_class' => 'main-menu',
															'items_wrap'      => '<div class="'.( $logo_box_position == 'center' && $logo_box_in_menu == '1' && $logo_box_enable ? 'menu-left-part' : 'no-split-menu' ).'"><ul id="%1$s" class="%2$s">%3$s</ul></div>',
															'container' => false,
															'walker' => new Cryptop_Walker_Nav_Menu($this)
														) );
													echo "</div>";

												$menu = ob_get_clean();
												printf('%s', $menu);

												// ------> Menu right
												echo '<div class="menu_right_icons">';
													// ------> Search
													if ($menu_box_search_place == 'right'){
														echo "<div class='search_icon'></div>";
													}

													// ------> Woo mini-cart
													if ( (class_exists('woocommerce') && $this->cws_get_option('woo_cart_place') == 'right') ){
														echo "<div class='mini-cart'>";
															echo sprintf('%s', $woo_mini_icon);
															echo sprintf('%s', $woo_mini_cart);
														echo "</div>";
													}

													// ------> Menu hamburger
													if ($menu_box_mobile_place == "right"){
														echo "<div class='mobile_menu_hamburger'>";
															echo "<span class='hamburger_icon'></span>";
														echo "</div>";
													}

													// ------> Side panel
													if ($side_panel_place == 'menu_right' && $side_panel_enable){
														echo "<div class='side_panel_icon_wrapper'>";
															echo "<a href='#' class='side_panel_trigger ".esc_attr($side_panel_place)."'></a>";
														echo "</div>";
													}
												echo '</div>';

												/*\NEW MENU*/
											?>

										</nav>
									</div>

								<?php
									}
								?>
							</div>
						</div>

							<?php

								$menu_box_content = ob_get_clean();
							}

							$this->header['menu_box'] = $menu_box_content;

							ob_start();
							?>

					</div><!-- header_container -->
				</header><!-- header -->
		<?php
			echo '</div>';

		$after_header_content = ob_get_clean();

		$this->header['after_header'] = $after_header_content;
	}

	public function cws_print_logo_block($is_sticky, $logo_lr_spacing, $lg, $blog_name) {
		extract(shortcode_atts( array(
			'logo' => '',
			'mobile' => '',
			'sticky' => '',
			'logo_h' => '',
		), $lg));

		$spacing = !empty( $logo_lr_spacing ) ? " style='{$logo_lr_spacing}'" : '';
		$out = sprintf( '<a%s class="logo" href="%s">', $spacing, esc_url(home_url()) );
		$logo_box = $this->cws_get_meta_option( 'logo_box' );


		if(!empty($logo)){
			$out .= "<div class='logo_default_wrapper logo_wrapper'>";
			foreach ($logo as $key => $value) {
		    	switch ($key){
			        case 'img' :
			        	$out .= $this->print_if( !empty($logo[$key]), "<img ".$logo[$key]." ".$logo_h." class='logo_default' alt />");
			            break ;
			        case 'svg' :
			            $out .= $logo[$key];
			            break ;
		    	}
		 	}
		 	$out .= "</div>";
		}

		if ($is_sticky){

			if(!empty($sticky) && is_array($sticky) && $logo_box['in_menu'] == '1'){

				$out .= "<div class='logo_sticky_wrapper logo_wrapper'>";
				foreach ($sticky as $key => $value) {
			    	switch ($key){
				        case 'img' :
				            $out .= $this->print_if( !empty($sticky[$key]), "<img ".$sticky[$key]." class='logo_sticky' alt />");
				            break ;
				        case 'svg' :
				        	$out .= "<span class='logo_sticky cws_svg_sticky'>";
				            $out .= $sticky[$key];
				            $out .= "</span>";
				            break ;
			    	}
			 	}
			 	$out .= "</div>";
			}
		}

		$out .= '</a>';
		return $out;
	}

	public function cws_print_logo_mobile($lg) {
		extract(shortcode_atts( array(
			'mobile' => '',
		), $lg));

		$out = sprintf( '<a class="logo" href="%s">', esc_url(home_url()) );

		if(!empty($mobile) && is_array($mobile)){
		 	foreach ($mobile as $key => $value) {
		    	switch ($key){
			        case 'img' :
			            $out .= $this->print_if( !empty($mobile[$key]), "<img ".$mobile[$key]." class='logo_mobile' alt />");
			            break ;
			        case 'svg' :
			        	$out .= "<span class='logo_mobile cws_svg_mobile'>";
			            $out .= $mobile[$key];
			            $out .= "</span>";
			            break ;
		    	}
		 	}
		}

		$out .= '</a>';
		return $out;
	}


	public function cws_get_date_parts () {
		$part_val = array();
		$perm_struct = get_option( 'permalink_structure' );
		if (!empty( $perm_struct )) {
			$part_val = array(
				'year' => get_query_var( 'year' ),
				'monthnum' => get_query_var( 'monthnum' ),
				'day' => get_query_var( 'day' ),
			);
		} else {
			$merge_date = get_query_var( 'm' );
			$match = preg_match( '#(\d{4})?(\d{1,2})?(\d{1,2})?#', $merge_date, $matches );
			$part_val = array(
				'year' => isset( $matches[1] ) ? $matches[1] : '',
				'monthnum' => isset( $matches[2] ) ? $matches[2] : '',
				'day' => isset( $matches[3] ) ? $matches[3] : '',
			);
		}
		return $part_val;
	}
	public function cws_get_date_part ( $part = '' ){
		$part_val = '';
		$pid = get_queried_object_id();
		$perm_struct = get_option( 'permalink_structure' );
		$use_perms = !empty( $perm_struct );
		$merge_date = get_query_var( 'm' );
		$match = preg_match( '#(\d{4})?(\d{1,2})?(\d{1,2})?#', $merge_date, $matches );
		switch ( $part ){
			case 'y':
				$part_val = $use_perms ? get_query_var( 'year' ) : ( isset( $matches[1] ) ? $matches[1] : '' );
				break;
			case 'm':
				$part_val = $use_perms ? get_query_var( 'monthnum' ) : ( isset( $matches[2] ) ? $matches[2] : '' );
				break;
			case 'd':
				$part_val = $use_perms ? get_query_var( 'day' ) : ( isset( $matches[3] ) ? $matches[3] : '' );
				break;
		}
		return $part_val;
	}

	public function cws_render_top_bar($pid) {
		//Get metaboxes from page
		$top_bar_box = $this->cws_get_meta_option( 'top_bar_box' );
		extract($top_bar_box, EXTR_PREFIX_ALL, 'top_bar_box');

		$logo_box = $this->cws_get_meta_option( 'logo_box' );
		extract($logo_box, EXTR_PREFIX_ALL, 'logo_box');

		$menu_box = $this->cws_get_meta_option( 'menu_box' );
		extract($menu_box, EXTR_PREFIX_ALL, 'menu_box');

		$side_panel = $this->cws_get_meta_option( 'side_panel' );
		extract($side_panel, EXTR_PREFIX_ALL, 'side_panel');

		$mobile_menu = $this->cws_get_meta_option( 'mobile_menu' );
		$mobile_menu_class = 'mobile_menu_wrapper';
		$mobile_menu_class .= (!empty($mobile_menu['theme']) ? ' '.esc_attr($mobile_menu['theme']) : '');
		$mobile_menu_class .= (!empty($mobile_menu['enable_on_tablet']) && $mobile_menu['enable_on_tablet'] == '1' ? ' show_on_tablets' : '');

		ob_start();

			/*NEW TOP BAR*/
			if($top_bar_box_enable){

				// ------> Register variables
				$social_links  		= '';
				$woo_mini_cart 		= '';
				$woo_mini_icon		= '';
				$show_wpml_header 	= CWS_WPML_ACTIVE;
				$is_woo_active 		= class_exists('woocommerce');
				$top_bar_box_text 	= isset($top_bar_box_text) ? stripslashes($top_bar_box_text) : '';
				$social_links 		= $this->cws_render_social_links('top_bar', $top_bar_box_social_place);
				if($is_woo_active){
					$woo_mini_cart 	= $this->cws_getWooMiniCart();
					$woo_mini_icon 	= $this->cws_getWooMiniIcon();
				}

				// ------> Render Top-Bar
				if( !empty($social_links) || !empty($top_bar_box_text) || $top_bar_box_language_bar || ($is_woo_active && $this->cws_get_option('woo_cart_place') == 'top')){
					echo "<div class='top_bar_box top_bar_wrapper'>";
						echo "<div class='container" . ( $top_bar_box_wide ? ' wide_container ' : '' ) . "'>";

							echo "<div class='top_bar_icons left_icons'>";
								// ------> Side panel
								if ($side_panel_place == 'topbar_left' && $side_panel_enable){
									echo "<div class='side_panel_icon_wrapper'>";
										echo "<a href='#' class='side_panel_trigger ".esc_attr($side_panel_place)."'></a>";
									echo "</div>";
								}
								// ------> Language switcher
								if ( $show_wpml_header && $top_bar_box_language_bar && $top_bar_box_language_bar_position == 'left') {
									echo "<div class='lang_bar'>";
										do_action( 'icl_language_selector' );
									echo "</div>";
								}
								// ------> Top bar content
								if (!empty( $top_bar_box_text )){
									echo "<div class='top_bar_content'>";
										esc_html_e($top_bar_box_text);
									echo "</div>";
								}
								// ------> Social links
								if ( $top_bar_box_social_place == 'left' && !empty($social_links) ){
									echo "<div class='social_links_wrapper" . ( $top_bar_box_toggle_share ? ' toogle-of' : ' toogle-off' ) . "'>";
										if( $top_bar_box_toggle_share ){
											echo "<i class='social-btn-open-icon'></i><span class='social-btn-open'>Social</span>";
										}
										echo sprintf("%s", $social_links );
									echo "</div>";
								}
							echo "</div>";

							echo "<div class='top_bar_icons right_icons'>";
								// ------> Top bar links
								if (!empty($top_bar_box_content_items) ){
									echo "<div class='top_bar_links_wrapper'>";
										foreach ($top_bar_box_content_items as $key => $value) {
											if( !empty($value['icon']) ){
												$top_bar_box_row_icon = "<i class='".esc_attr($value['icon'])."'></i>";
											}
											if ( !empty($value['url']) ){
												if ( $value['link_type'] != 'link' && !empty($value['link_type']) ){
													echo "<a class='top_bar_box_text' href='".esc_attr($value['link_type']).esc_attr($value['url'])."'>".$top_bar_box_row_icon."<span>".esc_html($value['title'])."</span></a>";
												} elseif ( $value['link_type'] == 'link' ){
													echo "<a class='top_bar_box_text' href='".esc_attr($value['url'])."'>".$top_bar_box_row_icon."<span>".esc_html($value['title'])."</span></a>";
												}
											} else {
												echo "<div class='top_bar_box_text'>".$top_bar_box_row_icon.esc_html($value['title'])."</div>";
											}
										}
									echo "</div>";
								}
								// ------> Social links
								if ( $top_bar_box_social_place == 'right' && !empty($social_links) ){
									echo "<div class='social_links_wrapper" . ( $top_bar_box_toggle_share ? ' toogle-of' : ' toogle-off' ) . "'>";
										echo sprintf("%s", $social_links );
										if( $top_bar_box_toggle_share ){
											echo "<i class='social-btn-open-icon'></i><span class='social-btn-open'>Social</span>";
										}
									echo "</div>";
								}
								// ------> Woo mini-cart
								if ( ($is_woo_active && $this->cws_get_option('woo_cart_place') == 'top') ){
									echo "<div class='mini-cart'>";
										echo sprintf('%s', $woo_mini_icon);
										echo sprintf('%s', $woo_mini_cart);
									echo "</div>";
								}
								// ------> Search
								if($menu_box_search_place == 'top'){
									echo "<div class='top_bar_search'>";
										echo "<div class='row_text_search'>";
											get_search_form();
										echo '</div>';
										echo "<div class='search_icon'></div>";
									echo '</div>';
								}
								// ------> Language switcher
								if ( $show_wpml_header && $top_bar_box_language_bar && $top_bar_box_language_bar_position == 'right') {
									echo "<div class='lang_bar'>";
										do_action( 'icl_language_selector' );
									echo "</div>";
								}
								// ------> Side panel
								if ($side_panel_place == 'topbar_right' && $side_panel_enable){
									echo "<div class='side_panel_icon_wrapper'>";
										echo "<a href='#' class='side_panel_trigger ".esc_attr($side_panel_place)."'></a>";
									echo "</div>";
								}
							echo "</div>";

						echo "</div>";
					echo "</div>";
				}
			}
			/*\\NEW TOP BAR*/

		return ob_get_clean();
	}
	/* /THEME HEADER */

	// Add menu custom fields
	public function cws_add_custom_nav_fields( $menu_item ) {
		$cws_mb_post = get_post_meta( $menu_item->ID, 'cws_mb_post', true );
		if (isset($cws_mb_post['cws_menu'])) {
			foreach ($cws_mb_post['cws_menu'] as $key => $value) {
				$menu_item->$key = $value;
			}
		}
		return $menu_item;
	}

	// Save menu custom fields
	public function cws_update_custom_nav_fields( $menu_id, $menu_post_id, $args ) {
		if(isset($_POST['cws_menu_options'])) {
			parse_str(urldecode($_POST['cws_menu_options']), $parse_array);
			$save_array = array();
			foreach ($parse_array as $k => $value) {
				list($key, $id) = explode('-', $k);
				if ($id == $menu_post_id) {
					$save_array[$key] = $value;
				}
			}
			update_post_meta( $menu_post_id, 'cws_mb_post', array('cws_menu' => $save_array));
		}
	}

	// Edit menu custom fields
	public function cws_edit_walker( $walker,$menu_id ) {
		return 'Walker_Nav_Menu_Edit_Custom';
	}

	// Add inline style
	public function cws_add_style() {
		$out = $this->cws_theme_header_process_fonts();
		$out .= $this->cws_theme_header_process_colors();
		$out .= $this->cws_theme_header_process_blur();
		$out .= $this->cws_theme_loader();

		wp_add_inline_style('cws-theme-options', $out);
	}

	public function cws_Hex2RGB($hex) {
		$hex = str_replace('#', '', $hex);
		$color = '';

		if(strlen($hex) == 3) {
			$color = hexdec(mb_substr($hex, 0, 1)) . ',';
			$color .= hexdec(mb_substr($hex, 1, 1)) . ',';
			$color .= hexdec(mb_substr($hex, 2, 1));
		}
		else if(strlen($hex) == 6) {
			$color = hexdec(mb_substr($hex, 0, 2)) . ',';
			$color .= hexdec(mb_substr($hex, 2, 2)) . ',';
			$color .= hexdec(mb_substr($hex, 4, 2));
		}
		return $color;
	}

	public function cws_Hex2RGBA( $color, $opacity ) {
		$output = '';
		if (!empty($color)){
			//Sanitize $color if "#" is provided
			if (substr($color, 0, 4) === 'rgba') {
				if(!empty($opacity)){
					$rgba_o = str_replace("rgba(", "", $color);
					$rgba_o = explode(",", $rgba_o);
					return "rgba(".$rgba_o[0].",".$rgba_o[1].",".$rgba_o[2].", ".$opacity.")";
				}
				return $color;
			}

		    if ($color[0] == '#' ) {
		    	$color = substr( $color, 1 );
		    }
		    //Check if color has 6 or 3 characters and get values
		    if (strlen($color) == 6) {
		            $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
		    } elseif ( strlen( $color ) == 3 ) {
		            $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
		    } else {
		            return $default;
		    }

		    //Convert hexadec to rgb
		    $rgb =  array_map('hexdec', $hex);

		    //Check if opacity is set(rgba or rgb)
		    if($opacity){
		    	if(abs($opacity) > 1)
		    		$opacity = 1.0;
		    	$output = 'rgba('.implode(",",$rgb).','.$opacity.')';
		    } else {
		    	$output = 'rgb('.implode(",",$rgb).')';
		    }

		    //Return rgb(a) color string
		    return $output;
		}

	}

	public function cws_theme_youtube_api_init (){
		wp_add_inline_script('yt_player_api', '
			var tag = document.createElement("script");
			tag.src = "https://www.youtube.com/player_api";
			var firstScriptTag = document.getElementsByTagName("script")[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
		');
	}

	public function cws_dbl_to_sngl_quotes ( $content ) {
		return preg_replace( "|\"|", "'", $content );
	}

	public function cws_loading_body_class ( $classes ) {
			$classes[] = self::$text_domain.'-new-layout';
		return $classes;
	}

	public function cws_custom_search ( $form ) {
		$form = "
		<form method='get' class='search-form' action=' ".site_url()." ' >
			<div class='search_wrapper'>
				<label><span class='screen-reader-text'>".esc_html__( 'Search for:', 'cryptop' )."</span></label>
				<input type='text' placeholder='".esc_html__( 'Type and press Enter ...', 'cryptop' )."' class='search-field' value='". esc_attr(apply_filters('the_search_query', get_search_query())) ."' name='s'/>
				<input type='submit' class='search-submit' value=' ".esc_html__( 'Search', 'cryptop' )." ' />
			</div>
		</form>";

		return $form;
	}

	public function cws_render_search($block = 'div'){
		ob_start();
			get_search_form();
		$form = ob_get_clean();

		$out = "
			<".esc_attr($block)." class='search_menu_wrap animated'>
				<div class='search_menu_cont'>
					<div class='search_back_button'></div>
					<div class='container'>
					".$form."
					</div>
				</div>
			</".esc_attr($block).">
		";

		return $out;
	}

	// Custom filter function to modify default gallery shortcode output
	public function cws_custom_gallery( $output, $attr ) {

		// Initialize
		global $post, $wp_locale;

		// Gallery instance counter
		static $instance = 0;
		$instance++;

		// Validate the author's orderby attribute
		if ( isset( $attr['orderby'] ) ) {
			$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
			if ( ! $attr['orderby'] ) unset( $attr['orderby'] );
		}

		// Get attributes from shortcode
		extract( shortcode_atts( array(
			'order'      => 'ASC',
			'orderby'    => 'menu_order ID',
			'id'         => $post->ID,
			'itemtag'    => 'div',
			'icontag'    => 'div',
			'captiontag' => 'div',
			'columns'    => 3,
			'size'       => 'thumbnail',
			'include'    => '',
			'exclude'    => ''
		), $attr ) );

		// Initialize
		$id = intval( $id );
		$attachments = array();
		if ( $order == 'RAND' ) $orderby = 'none';

		if ( ! empty( $include ) ) {

			// Include attribute is present
			$include = preg_replace( '/[^0-9,]+/', '', $include );
			$_attachments = get_posts( array( 'include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );

			// Setup attachments array
			foreach ( $_attachments as $key => $val ) {
				$attachments[ $val->ID ] = $_attachments[ $key ];
			}

		} else if ( ! empty( $exclude ) ) {

			// Exclude attribute is present
			$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );

			// Setup attachments array
			$attachments = get_children( array( 'post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
		} else {
			// Setup attachments array
			$attachments = get_children( array( 'post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby ) );
		}

		if ( empty( $attachments ) ) return '';

		// Filter gallery differently for feeds
		if ( is_feed() ) {
			$output = "\n";
			foreach ( $attachments as $att_id => $attachment ) $output .= wp_get_attachment_link( $att_id, $size, true ) . "\n";
			return $output;
		}

		// Filter tags and attributes
		$itemtag = tag_escape( $itemtag );
		$captiontag = tag_escape( $captiontag );
		$columns = intval( $columns );
		$itemwidth = $columns > 0 ? round(100 / $columns, 2) : 100;
		$float = is_rtl() ? 'right' : 'left';
		$selector = "gallery-{$instance}";

		// Filter gallery CSS
		$output = apply_filters( 'gallery_style', "
			<!-- see gallery_shortcode() in wp-includes/media.php -->
			<div id='$selector' class='gallery galleryid-{$id}'>"
		);

		// Iterate through the attachments in this gallery instance
		$i = 0;
		foreach ( $attachments as $id => $attachment ) {

			// Attachment link
			$link = isset( $attr['link'] ) && 'file' == $attr['link'] ? wp_get_attachment_link( $id, $size, false, false ) : wp_get_attachment_link( $id, $size, true, false );

			if ( isset($attr['link']) && $attr['link'] == 'none') {
				$link = preg_replace("/<a[^>]*>([^|]*)<\/a>/", "<div>$1</div>", $link);
			}

			// Start itemtag
			$output .= "<{$itemtag} class='gallery-item' style='float: {$float}; width: {$itemwidth}%;'>";

			// icontag
			$output .= "
			<{$icontag} class='gallery-icon'>
				$link
			</{$icontag}>";

			if ( $captiontag && trim( $attachment->post_excerpt ) ) {

				// captiontag
				$output .= "
				<{$captiontag} class='gallery-caption'>
					" . wptexturize($attachment->post_excerpt) . "
				</{$captiontag}>";

			}

			// End itemtag
			$output .= "</{$itemtag}>";

			// Line breaks by columns set
			if($columns > 0 && ++$i % $columns == 0) $output .= '<br style="clear: both">';

		}

		// End gallery output
		$output .= "</div>";

		return $output;
	}

	//Allows to filter the output of any WordPress widget
	public function cws_filter_dynamic_sidebar_params( $sidebar_params ) {
		if ( is_admin() ) {
			return $sidebar_params;
		}
		global $wp_registered_widgets;
		$current_widget_id = $sidebar_params[0]['widget_id'];
		$wp_registered_widgets[ $current_widget_id ]['original_callback'] = $wp_registered_widgets[ $current_widget_id ]['callback'];
		$wp_registered_widgets[ $current_widget_id ]['callback'] = array( $this, 'cws_display_widget' );
		return $sidebar_params;
	}

	public function cws_display_widget() {
		global $wp_registered_widgets;
		$original_callback_params = func_get_args();
		$widget_id         = $original_callback_params[0]['widget_id'];
		$original_callback = $wp_registered_widgets[ $widget_id ]['original_callback'];
		$wp_registered_widgets[ $widget_id ]['callback'] = $original_callback;
		$widget_id_base = $original_callback[0]->id_base;
		$sidebar_id     = $original_callback_params[0]['id'];
		if ( is_callable( $original_callback ) ) {
			ob_start();
			call_user_func_array( $original_callback, $original_callback_params );
			$widget_output = ob_get_clean();
			/**
			 * Filter the widget's output.
			 *
			 * @param string $widget_output  The widget's output.
			 * @param string $widget_id_base The widget's base ID.
			 * @param string $widget_id      The widget's full ID.
			 * @param string $sidebar_id     The current sidebar ID.
			 */
			echo apply_filters( 'widget_output', $widget_output, $widget_id_base, $widget_id, $sidebar_id );
		}
	}

	public function cws_filter_widgets( $widget_output, $widget_type, $widget_id, $sidebar_id ) {

		if ($widget_type == 'archives' || $widget_type == 'categories' || $widget_type == 'monster'){
			$widget_output = preg_replace('|<\/a>.*\(|', '<span class="post_count"> ( ', $widget_output);
			$widget_output = preg_replace('|\)|', ' )</span></a>', $widget_output);
		}

		if ($widget_type == 'woocommerce_product_categories'){
			$widget_output = preg_replace('|<\/a>.*<span class="count">\(|', '<span class="post_count">( ', $widget_output);
			$widget_output = preg_replace('|\)<\/span>|', ' )</span></a>', $widget_output);
		}

		if ($widget_type == 'recent-posts'){
			$widget_output = preg_replace('|<\/a>|', '', $widget_output);
			$widget_output = preg_replace('|<\/span>|', '</span></a>', $widget_output);
		}

		$widget_output = preg_replace('|cws-widget|', 'cws-widget widget-'.esc_attr($widget_type), $widget_output);
	    return $widget_output;
	}
	// --//Allows to filter the output of any WordPress widget

	public function cws_oembed_wrapper( $html, $url, $args ) {
		return !empty( $html ) ? "<div class='cws_oembed_wrapper'>$html</div>" : '';
	}

	public function cws_custom_excerpt_length( $length ) {
		return 1400;
	}

	public function cws_ajaxurl() {
		wp_localize_script('cws_scripts', 'ajaxurl', array(
			'templateDir' => esc_url( get_template_directory_uri() ),
			'url' => admin_url( 'admin-ajax.php' ),
		));
	}

	public function cws_ajax_redirect() {
		$ajax = isset( $_POST['ajax'] ) ? (bool)$_POST['ajax'] : false;
		if ( $ajax ) {
			$template = isset( $_POST['template'] ) ? $_POST['template'] : '';
			if ( !empty( $template ) ) {
				if ( strpos( $template, '-' ) ) {
					$template_parts = explode( '-', $template );
					if ( count( $template_parts ) == 2 ) {
						get_template_part( $template_parts[0], $template_parts[1] );
					}
					else {
						return;
					}
				}	else {
					get_template_part( $template );
				}
				exit();
			}
		}
		return;
	}

	public function cws_meta_vars() {}

	public function cws_render_gradient ($arrs) {
		$gradient = array(
			'first_color' => (!empty($arrs[ 'first_color' ]) ? $arrs[ 'first_color' ] : ''),
			'second_color' => (!empty($arrs[ 'second_color' ]) ? $arrs[ 'second_color' ] : ''),
			'first_color_opacity' => (!empty($arrs[ 'first_color_opacity' ]) ? $arrs[ 'first_color_opacity' ] : ''),
			'second_color_opacity' => (!empty($arrs[ 'second_color_opacity' ]) ? $arrs[ 'second_color_opacity' ] : ''),
			'type' => (!empty($arrs[ 'type' ]) ? $arrs[ 'type' ] : ''),
			'linear_settings' => (!empty($arrs[ 'linear_settings' ]) ? $arrs[ 'linear_settings' ] : ''),
			'radial_settings' => array(
				'shape_settings' => (!empty($arrs['radial_settings']['shape_settings']) ? $arrs['radial_settings']['shape_settings'] : ''),
				'shape' => (!empty($arrs['radial_settings']['shape']) ? $arrs['radial_settings']['shape'] : ''),
				'size_keyword' => (!empty($arrs['radial_settings']['size_keyword']) ? $arrs['radial_settings']['size_keyword'] : ''),
				'size' => (!empty($arrs['radial_settings']['size']) ? $arrs['radial_settings']['size'] : ''),
				)
		);
		return $gradient;
	}

	public function cws_get_grid_shortcodes() {
		return array( 'cws-row', 'col', 'cws-widget' );
	}

	public function cws_get_special_post_formats() {
		return array( 'aside' );
	}

	public function cws_is_special_post_format() {
		global $post;
		$sp_post_formats = $this->cws_get_special_post_formats();
		if ( isset($post) ) {
			return in_array( get_post_format(), $sp_post_formats );
		} else{
			return false;
		}
	}

	public function cws_post_format_mark() {
		global $post;
		$out = '';
		if ( isset( $post ) ) {
			$pf = get_post_format();
			$post_format_icons = array(
				'aside' => 'bullseye',
				'gallery' =>'bullseye',
				'link' => 'chain',
				'image' => 'image',
				'quote' => 'quote-lef',
				'status' => 'flag',
				'video' => 'video-camer',
				'audio' => 'music',
				'chat' => 'wechat',
			);
			$icon = '';
			if (isset($post_format_icons[$pf])) {
				$icon = $post_format_icons[$pf];
			}
			$out = "<i class='fa fa-$icon'></i> $pf";
		}
		return $out;
	}

	public function cws_strip_grid_shortcodes($text) {
		$shortcodes = function_exists('cryptop_get_grid_shortcodes') ? cryptop_get_grid_shortcodes () : "";
		$find = array();
		if(!empty($shortcodes)){
			foreach ( $shortcodes as $shortcode ) {
				$shortcode = preg_replace( "|-|", "\-", $shortcode );
				$op_tag = "|\[.*" . $shortcode . ".*\]|";
				$cl_tag = "|\[/.*" . $shortcode . ".*\]|";
				array_push( $find, $op_tag, $cl_tag );
			}
		}

		$text = preg_replace( $find, '', $text );
		return $text;
	}

	// Check if WooCommerce is active
	public function cws_woo_ajax_remove_from_cart() {
		global $woocommerce;

		$woocommerce->cart->set_quantity( $_POST['remove_item'], 0 );

		$ver = explode( '.', WC_VERSION );

		if ( $ver[1] == 1 && $ver[2] >= 2 ) :
			$wc_ajax = new WC_AJAX();
			$wc_ajax->get_refreshed_fragments();
		else :
			woocommerce_get_refreshed_fragments();
		endif;

		die();
	}

	public function cws_woo_header_add_to_cart_fragment( $fragments ) {
		ob_start();
		?>
			<i class='woo_mini-count flaticon-shopcart-icon-cryptop'><?php echo ((WC()->cart->cart_contents_count > 0) ?  '<span>' . WC()->cart->cart_contents_count .'</span>' : '') ?></i>
		<?php
		$fragments['.woo_mini-count'] = ob_get_clean();

		ob_start();
		woocommerce_mini_cart();
		$fragments['div.woo_mini_cart'] = ob_get_clean();
		return $fragments;
	}

	public function cws_woo_related_products_args( $args ) {
		$args['posts_per_page'] = $this->cws_get_option( 'woo-resent-num-products' ); // 4 related products
		$args['columns'] = 3; // arranged in 2 columns
		return $args;
	}

	/************** JAVASCRIPT VARIABLES INIT **************/
	public function cws_js_vars_init() {
		//Get metaboxes from page
		$sticky_menu = $this->cws_get_meta_option( 'sticky_menu' );
		if ($sticky_menu) {
			extract($sticky_menu, EXTR_PREFIX_ALL, 'sticky_menu');

			$sticky_menu_enable = $sticky_menu_enable == '1' ? 'true' : 'false';

			$is_user_logged = is_user_logged_in();
			$logged_var = $is_user_logged ? 'true' : 'false';

			$sticky_sidebars = $this->cws_get_option('sticky_sidebars') == '1' ? 'true' : 'false';
			$page_loader = $this->cws_get_option('show_loader') == '1' ? 'true' : 'false';
			$animation_curve_menu = $this->cws_get_option('animation_curve_menu');
			$animation_curve_scrolltop = $this->cws_get_option('animation_curve_scrolltop');
			$animation_curve_speed = $this->cws_get_option('animation_curve_speed');

			//Don't forget boolean value without ''
			wp_add_inline_script('cws_scripts', '
				var is_user_logged = '.esc_js($logged_var).','.
				'sticky_menu_enable = '.esc_js($sticky_menu_enable).','.
				'sticky_menu_mode = "'.esc_js($sticky_menu_mode).'",'.
				'sticky_sidebars = '.esc_js($sticky_sidebars).','.
				'page_loader = '.esc_js($page_loader).','.
				'animation_curve_menu = "'.esc_js($animation_curve_menu).'",'.
				'animation_curve_scrolltop = "'.esc_js($animation_curve_scrolltop).'",'.
				'animation_curve_speed = '.esc_js($animation_curve_speed).','.
				'use_blur = false;');

		}
	}
	/************** \JAVASCRIPT VARIABLES INIT **************/

	/******************** TYPOGRAPHY ********************/
	// MENU FONT HOOK
	private function cws_print_font_css($font_array) {
		$out = '';
		foreach ($font_array as $style=>$v) {
			if ($style != 'font-weight' && $style != 'font-sub' && $style != 'font-type') {
				$out .= !empty($v) ? $style .':'.$v.';' : '';
			}
		}
		return $out;
	}

	private function cws_print_menu_font() {
		ob_start();
		do_action( 'menu_font_hook' );
		return ob_get_clean();
	}

	public function cws_menu_font_action() {
		$out = '';
		$font_array = $this->cws_get_meta_option('menu-font');

		$slider_settings = $this->cws_get_meta_option( 'slider_override' );

		// -----> Styles from Theme Options -> Typography -> Menu
		if (isset($font_array)) {
			$out .= '
			.main-nav-container .menu-item a,
			.main-nav-container .menu-item .cws_megamenu_item_title
			{'
				. esc_attr($this->cws_print_font_css($font_array)) . ';
			}';

			$out .= '
			.main-nav-container .search_icon,
			.main-nav-container .mini-cart a,
			.main-nav-container .side_panel_trigger
			{
				color : '. esc_attr($font_array["color"]) . ';
			}';

			$out .='
			.main-nav-container .hamburger_icon,
			.main-nav-container .hamburger_icon:before,
			.main-nav-container .hamburger_icon:after
			{
				background-color : ' . esc_attr($font_array["color"]) . ';
			}';
		}

		$menu_box_font_color = $this->cws_get_meta_option('menu_box')['font_color'];
		$menu_box_font_color_hover = $this->cws_get_meta_option('menu_box')['font_color_hover'];
		$p_type = get_post_type();

		// -----> Styles from Theme Options / Metaboxes -> Header -> Menu
		$out .= '
		.main-nav-container .main-menu > .menu-item > a,
		.main-nav-container .main-menu > .menu-item > .cws_megamenu_item_title,
		.main-nav-container .search_icon,
		.main-nav-container .mini-cart a,
		.main-nav-container .side_panel_trigger
		{
			color : '. esc_attr($menu_box_font_color) . ';
		}';

		$out .= '
		.main-nav-container .main-menu > .menu-item > a:hover,
		.main-nav-container .main-menu > .menu-item > .cws_megamenu_item_title:hover,
		.main-nav-container .main-menu > .menu-item.current_page_ancestor > a,
		.main-nav-container .main-menu > .menu-item.current_page_ancestor > span,
		.main-nav-container .search_icon:hover,
		.main-nav-container .mini-cart a:hover,
		.main-nav-container .side_panel_trigger:hover
		{
			color : '. esc_attr($menu_box_font_color_hover) . ';
		}';

		$out .= '
		.main-nav-container .hamburger_icon,
		.main-nav-container .hamburger_icon:before,
		.main-nav-container .hamburger_icon:after
		{
			background-color : '. esc_attr($menu_box_font_color) . ';
		}';

		$out .= '
		.main-nav-container .mobile_menu_hamburger:hover .hamburger_icon,
		.main-nav-container .mobile_menu_hamburger:hover .hamburger_icon:before,
		.main-nav-container .mobile_menu_hamburger:hover .hamburger_icon:after
		{
			background-color : '. esc_attr($menu_box_font_color_hover) . ';
		}';

		// -----> Styles from Theme Options -> Header -> Header
		if ($this->cws_get_meta_option( 'header' )['customize'] == '1' ) {
			$menu_box_font_color = $this->cws_get_meta_option('header')['override_menu_color'];
			$menu_box_font_color_hover = $this->cws_get_meta_option('header')['override_menu_color_hover'];

			$out .= '
			.header_zone .main-nav-container .main-menu > .menu-item > a,
			.header_zone .main-nav-container .main-menu > .menu-item > .cws_megamenu_item_title,
			.header_zone .main-nav-container .search_icon,
			.header_zone .main-nav-container .mini-cart a,
			.header_zone .main-nav-container .side_panel_trigger
			{
				color : '. esc_attr($menu_box_font_color) . ';
			}';

			$out .= '
			.header_zone .main-nav-container .main-menu > .menu-item > a:hover,
			.header_zone .main-nav-container .main-menu > .menu-item > .cws_megamenu_item_title:hover,
			.header_zone .main-nav-container .main-menu > .menu-item.current_page_ancestor > a,
			.header_zone .main-nav-container .main-menu > .menu-item.current_page_ancestor > span,
			.header_zone .main-nav-container .search_icon:hover,
			.header_zone .main-nav-container .mini-cart a:hover,
			.header_zone .main-nav-container .side_panel_trigger:hover
			{
				color : '. esc_attr($menu_box_font_color_hover) . ';
			}';

			$out .= '
			.header_zone .main-nav-container .hamburger_icon,
			.header_zone .main-nav-container .hamburger_icon:before,
			.header_zone .main-nav-container .hamburger_icon:after
			{
				background-color : '. esc_attr($menu_box_font_color) . ';
			}';

			$out .= '
			.header_zone .main-nav-container .mobile_menu_hamburger:hover .hamburger_icon,
			.header_zone .main-nav-container .mobile_menu_hamburger:hover .hamburger_icon:before,
			.header_zone .main-nav-container .mobile_menu_hamburger:hover .hamburger_icon:after
			{
				background-color : '. esc_attr($menu_box_font_color_hover) . ';
			}';
		}

		// -----> Styles from Theme Options -> WooCommerce -> Menu
		if ($this->cws_get_option( 'woo_customize_menu' ) == '1' && $this->cws_is_woo() ) {
			$menu_box_font_color = $this->cws_get_option('woo_menu_font_color');
			$menu_box_font_color_hover = $this->cws_get_option('woo_menu_font_hover_color');
			$menu_border_color = $this->cws_get_option('woo_menu_border_color');

			$out .= '
			.woocommerce .main-nav-container .main-menu > .menu-item > a,
			.woocommerce .main-nav-container .main-menu > .menu-item > .cws_megamenu_item_title,
			.woocommerce .main-nav-container .search_icon,
			.woocommerce .main-nav-container .mini-cart a,
			.woocommerce .main-nav-container .side_panel_trigger
			{
				color : '. esc_attr($menu_box_font_color) . ';
			}';

			$out .= '
			.woocommerce .main-nav-container .main-menu > .menu-item > a:hover,
			.woocommerce .main-nav-container .main-menu > .menu-item > .cws_megamenu_item_title:hover,
			.woocommerce .main-nav-container .main-menu > .menu-item.current_page_ancestor > a,
			.woocommerce .main-nav-container .main-menu > .menu-item.current_page_ancestor > span,
			.woocommerce .main-nav-container .search_icon:hover,
			.woocommerce .main-nav-container .mini-cart a:hover,
			.woocommerce .main-nav-container .side_panel_trigger:hover
			{
				color : '. esc_attr($menu_box_font_color_hover) . ';
			}';

			$out .= '
			.woocommerce .main-nav-container .hamburger_icon,
			.woocommerce .main-nav-container .hamburger_icon:before,
			.woocommerce .main-nav-container .hamburger_icon:after
			{
				background : '. esc_attr($menu_box_font_color) . ';
			}';

			$out .= '
			.woocommerce .main-nav-container .mobile_menu_hamburger:hover .hamburger_icon,
			.woocommerce .main-nav-container .mobile_menu_hamburger:hover .hamburger_icon:before,
			.woocommerce .main-nav-container .mobile_menu_hamburger:hover .hamburger_icon:after
			{
				background : '. esc_attr($menu_box_font_color_hover) . ';
			}';

		}
		echo preg_replace('/\s+/',' ', $out);
	}

	// \MENU FONT HOOK

	// HEADER FONT HOOK

	private function cws_print_header_font () {
		ob_start();
		do_action( 'header_font_hook' );
		return ob_get_clean();
	}

	public function cws_header_font_action () {
		$out = '';
		$font_array = $this->cws_get_option('header-font');

		if (isset($font_array)) {
			$out .=  "
				.news .ce_title a.link_post,
				.cws_portfolio_single_wrapper .post_info.outside .title_part a,
				.gallery-icon + .gallery-caption,
				.vc_general.vc_tta.vc_tta-tabs .vc_tta-tab .vc_tta-title-text,
				.cta_subtitle,
				.cta_title,
				.grid_row.single_related .carousel_nav span,
				.cta_desc_subtitle,
				.tribe-nav-label,
				.cta_offer + .cta_banner .cws_cta_banner .cws_banner_title,
				.cta_offer + .cta_banner .cws_cta_banner .cws_banner_price,
				.cta_offer + .cta_banner .cws_cta_banner .cws_banner_desc,
				.cws_pricing_plan .pricing_plan_price .price,
				form.wpcf7-form > div:not(.wpcf7-response-output)>p,
				.page_title .page_excerpt,
				.page_content > main .grid_row.cws_tribe_events #tribe-bar-form label,
				form.wpcf7-form > div:not(.wpcf7-response-output)>label,
				#tribe-events-footer .tribe-events-sub-nav .tribe-events-nav-next a, #tribe-events-header .tribe-events-sub-nav .tribe-events-nav-next a,
				#tribe-events-footer .tribe-events-sub-nav li a, #tribe-events-header .tribe-events-sub-nav li a
				{
					font-family: ". esc_attr($font_array['font-family']) .";
				}";
			$out .= '
				.ce_title, figcaption .title_info h3, .cryptop-new-layout .cws-widget .widget-title,.woo_product_post_title.posts_grid_post_title, .comments-area .comment-reply-title,
				.woocommerce .product_title.entry-title, .page_title.customized .title h1, .bg_page_header .title h1, .widgettitle, .page_title .title
				{'
				. esc_attr($this->cws_print_font_css($font_array)) .
				'}';
			$out .= '
				.testimonial .author figcaption,
				.testimonial .quote .quote_link:hover,
				.pagination a,
				.widget-title,
				.ce_toggle.alt .accordion_title:hover,
				.pricing_table_column .price_section,
				.comments-area .comments_title,
				.comments-area .comment-meta,
				.comments-area .comment-reply-title,
				.comments-area .comment-respond .comment-form input:not([type=\'submit\']),
				.comments-area .comment-respond .comment-form textarea,
				.page_title .bread-crumbs,
				.benefits_container .cws_textwidget_content .link a:hover,
				.cws_portfolio_fw .title,
				.cws_portfolio_fw .cats a:hover,
				.msg_404,
				.cws_portfolio_single_wrapper .post_info.outside .title_part a,
				.nav_post_links .sub_title
				{
					color:' . esc_attr($font_array['color']) . ';
				}';
				$out .=  "h1, h2, h3, h4, h5, h6
				{
					font-family: ". esc_attr($font_array['font-family']) .";
					color: ". esc_attr($font_array['color']) .";
				}";
				$out .= '.posts_grid.cws_portfolio_posts_grid h2.widgettitle,
						.cws_portfolio_nav li .title_nav_portfolio
					{
						font-size:' . esc_attr($font_array['font-size']) . ';
					}';

		}
		echo preg_replace('/\s+/',' ', $out);
	}

	// \HEADER FONT HOOK

	// BODY FONT HOOK

	private function cws_print_body_font () {
		ob_start();
		do_action( 'body_font_hook' );
		return ob_get_clean();
	}

	public function cws_body_font_action () {
		$out = '';
		$font_array = $this->cws_get_option('body-font');
		if (isset($font_array)) {
			$out .= 'body
				{
				'. esc_attr($this->cws_print_font_css($font_array)) .
				'}';
			$out .= '.news .ce_title a,
					.tribe-this-week-events-widget .tribe-this-week-widget-horizontal .entry-title,.tribe-this-week-events-widget  .tribe-this-week-widget-horizontal .entry-title a,
					form.wpcf7-form > div:not(.wpcf7-response-output)>p span,
					.main-nav-container .sub-menu .cws_megamenu_item .widgettitle,
				form.wpcf7-form > div:not(.wpcf7-response-output)>label span,
				.tribe-events-schedule h2,
				.cryptop-new-layout .tooltipster-light .tooltipster-content
					{
						font-family:'. esc_attr($font_array['font-family']) . ';
					}';
			$out .= '.cws-widget ul li>a,
					.comments-area .comments_nav.carousel_nav_panel a,
					.cws_img_navigation.carousel_nav_panel a,
					.cws_portfolio_fw .cats a,
					.cws_portfolio .categories a,
					.row_bg .ce_accordion.alt .accordion_title,
					.row_bg .ce_toggle .accordion_title,
					.mini-cart .woo_mini_cart,
					.lang_bar ul li ul li a,
					.thumb_staff_posts_title a,
					.tribe-this-week-widget-wrapper .tribe-this-week-widget-day .duration, .tribe-this-week-widget-wrapper .tribe-this-week-widget-day .tribe-venue,
					.thumb_staff_posts_title,
					#mc_embed_signup input,
					.mc4wp-form .mc4wp-form-fields input,
					form.wpcf7-form > div:not(.wpcf7-response-output) .select2-selection--single .select2-selection__rendered,.cws-widget #wp-calendar tbody td a:hover,
					.tribe-mini-calendar .tribe-events-has-events div[id*="daynum-"] a:hover,
					.cws-widget #wp-calendar td:hover, .cws-widget #wp-calendar tfoot td#prev:hover a:before,
					form.wpcf7-form > div:not(.wpcf7-response-output) .select2-selection--single .select2-selection__arrow b,
					.main-nav-container .sub-menu .cws_megamenu_item .widgettitle,
					.vc_general.vc_tta.vc_tta-tabs .vc_tta-tabs-list .vc_tta-tab, .tabs.wc-tabs li,
					#tribe-events-content .tribe-events-calendar div[id*=tribe-events-event-] h3.tribe-events-month-event-title,
					.tribe-events-calendar td.tribe-events-past div[id*=tribe-events-daynum-],
					.tribe-events-calendar td.tribe-events-present div[id*=tribe-events-daynum-],
					.tribe-events-calendar td.tribe-events-past div[id*=tribe-events-daynum-]>a,
					#tribe-events-content .tribe-events-calendar div[id*=tribe-events-event-] h3.tribe-events-month-event-title a,
					.tribe-events-calendar td div[id*=tribe-events-daynum-] > a,
					.posts_grid .portfolio_item.under_img .cws_portfolio_posts_grid_post_content
					{
						color:' . esc_attr($font_array['color']) . ';
					}';
			$out .= '.mini-cart .woo_mini_cart,
					body input,body  textarea
					{
						font-size:' . esc_attr($font_array['font-size']) . ';
					}';
			$out .= 'body input,body  textarea
					{
						line-height:' . esc_attr($font_array['line-height']) . ';
					}';
			$out .= 'abbr
					{
						border-bottom-color:' . esc_attr($font_array['color']) . ';
					}';
			$fs_match = preg_match( '#(\d+)(.*)#', $font_array['font-size'], $fs_matches );
			$lh_match = preg_match( '#(\d+)(.*)#', $font_array['line-height'], $lh_matches );
			if ( $fs_match && $lh_match ) {
				$fs_number = (int)$fs_matches[1];
				$fs_units = $fs_matches[2];
				$lh_number = (int)$lh_matches[1];
				$lh_units = $lh_matches[2];
				$out .= "
				.dropcap{
					font-size:" . esc_attr($fs_number * 2 . $fs_units).";
					line-height:" . esc_attr($lh_number * 2 . $lh_units).";
					width:" . esc_attr($lh_number * 2 . $lh_units).";
				}";
			}
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	// \BODY FONT HOOK

	private function cws_print_helper_font () {
		ob_start();
		do_action( 'body_helper_hook' );
		return ob_get_clean();
	}

	// HELPER FONT HOOK
	public function cws_body_helper_action() {
		$out = '';
		$font_array = $this->cws_get_option('helper-font');

		if (isset($font_array)) {
			$out .= '
				.category-images .grid .item .category-wrapper .category-label-wrapper .category-label,
				.page_footer.instagram_feed #sb_instagram .sbi_follow_btn a
				{
					font-family:'. esc_attr($font_array['font-family']) . ';
				}

				.testimonial .quote_wrap,
				.post_format_quote_media_wrapper .cws_module .content-quote,
				.single .news .quote-wrap .quote .text
				{
					'. esc_attr($this->cws_print_font_css($font_array)) .';
				}
				';
		}

		echo preg_replace('/\s+/',' ', $out);
	}
	// \HELPER FONT HOOK

	public function cws_process_fonts() {
		$out = $this->cws_print_menu_font();
		$out .= $this->cws_print_header_font();
		$out .= $this->cws_print_body_font();
		$out .= $this->cws_print_helper_font();
		return $out;
	}

//****************************** CWS PRINT FUNCTIONS ******************************
	public function cws_print_gradient( $settings, $selectors = '',  $use_extra_rules = false) {
		extract( shortcode_atts( array(
			'c1' => CRYPTOP_FIRST_COLOR,
			'c2' => CRYPTOP_SECONDARY_COLOR,
			'op1' => '100',
			'op2' => '100',
			'type' => 'linear',
			'linear' => array(),
			'radial' => array(),
			'custom_css' => '',
		), $settings));

		if (!empty($custom_css)) return $custom_css;

		$c1 = $this->cws_Hex2RGBA($c1,(int)$op1/100);
		$c2 = $this->cws_Hex2RGBA($c2,(int)$op2/100);

		$out = '';
		$rules = '';
		switch ($type) {
			case 'linear':
				$angle = isset($linear['angle']) ? $linear['angle'] : 0;
				$rules .= "background:-webkit-linear-gradient(".esc_attr($angle)."deg, ".esc_attr($c1).", ".esc_attr($c2).");";
				$rules .= "background:-o-linear-gradient(".esc_attr($angle)."deg, ".esc_attr($c1).", ".esc_attr($c2).");";
				$rules .= "background:-moz-linear-gradient(".esc_attr($angle)."deg, ".esc_attr($c1).", ".esc_attr($c2).");";
				$rules .= "background:linear-gradient(".esc_attr($angle)."deg, ".esc_attr($c1).", ".esc_attr($c2).");";
				break;
			case 'radial':
				extract( shortcode_atts( array(
					'shape_type' => 'simple',
					'shape' => 'ellipse',
					'keyword' => 'farthest-corner',
					'size' => ''
				), $radial));

				switch ($shape_type) {
					case 'simple':
						$rules .= "background:-webkit-radial-gradient(".esc_attr($shape)." ".esc_attr($c1).", ".esc_attr($c2).");";
						$rules .= "background:-o-radial-gradient(".esc_attr($shape)." ".esc_attr($c1).", ".esc_attr($c2).");";
						$rules .= "background:-moz-radial-gradient(".esc_attr($shape)." ".esc_attr($c1).", ".esc_attr($c2).");";
						$rules .= "background:radial-gradient(".esc_attr($shape)." ".esc_attr($c1).", ".esc_attr($c2).");";
						break;
					case 'exteneded':
						$rules .= "background:-webkit-radial-gradient( ".esc_attr($size)." ".esc_attr($size_keyword)." ".esc_attr($c1).", ".esc_attr($c2).");";
						$rules .= "background:-o-radial-gradient( ".esc_attr($size)." ".esc_attr($size_keyword)." ".esc_attr($c1).", ".esc_attr($c2).");";
						$rules .= "background:-moz-radial-gradient( ".esc_attr($size)." ".esc_attr($size_keyword)." ".esc_attr($c1).", ".esc_attr($c2).");";
						$rules .= "background:radial-gradient(".esc_attr($size_keyword)." at ".esc_attr($size)." ".esc_attr($c1).", ".esc_attr($c2).");";
						break;
			}
				break;
		}

		if ( !empty( $rules ) ) {
			$printf_rules = !empty($selectors) ? '%s{%s}' : '%s%s';
			$out .= sprintf($printf_rules, $selectors, $rules);
			if ( $use_extra_rules ) {
				$border_extra_rules = 'border-color:transparent;-moz-background-clip:border;-webkit-background-clip: border;background-clip:border-box;-moz-background-origin:border;-webkit-background-origin:border;background-origin:border-box;background-repeat:no-repeat;';
				$transition_extra_rules = '-webkit-transition-property:background,color,border-color,opacity;-webkit-transition-duration:0s,0s,0s,0.6s;-o-transition-property:background,color,border-color,opacity;-o-transition-duration:0s,0s,0s,0.6s;-moz-transition-property:background,color,border-color,opacity;-moz-transition-duration:0s,0s,0s,0.6s;transition-property:background,color,border-color,opacity;transition-duration:0s,0s,0s,0.6s;';
				$out .= sprintf($printf_rules, $selectors, $border_extra_rules);
				$out .= sprintf($printf_rules, $selectors, 'color: #fff !important;');
				$selectors_wth_pseudo = str_replace( ':hover', '', $selectors );
				$out .= sprintf($printf_rules, $selectors_wth_pseudo, $transition_extra_rules);
			}
		}
		return $out;
	}

	public function cws_print_paddings($paddings = array()) {
		if ($paddings && is_array($paddings)) {
			$out = '';
			foreach ( $paddings as $key => $value ){
				if ( !empty( $value ) || $value == '0' ){
					$out .= "padding-".esc_attr($key). ": " . esc_attr($value) . "px;";
				}
			}
		}
		return $out;
	}

	public function cws_print_margins($margins = array()) {
		if ($margins && is_array($margins)) {
			$out = '';
			foreach ( $margins as $key => $value ){
				if ( !empty( $value ) || $value == '0' ){
					$out .= "margin-".esc_attr($key). ": " . esc_attr($value) . "px;";
				}
			}
		}
		return $out;
	}

	public function cws_print_background($props = null) {
		if ($props && is_array($props)) {
			$out = '';
			foreach ($props as $key => $value) {
				if ('image' === $key) {
					$out .= 'background-'.esc_attr($key).':';
					$out .= sprintf('url(%s);', esc_url($value['src']));
				}
			}
			$out .=  $this->cws_print_css_keys($props, 'background-');
		}
		return $out;
	}

	public function cws_print_border($box) {
		$out = '';
		if ( !empty($box) && !empty($box['border_box']) ){
			$width_type_color = sprintf('%spx %s %s', $box['width'], $box['type'], $box['color']);
			foreach ($box['border_box'] as $key => $value) {
				$out .= "border-".esc_attr($value).":".esc_attr($width_type_color).";";
			}
		}
		return $out;
	}

	public function cws_print_overlay($o){
		$bg_styles = '';
		if(!empty($o)){
			extract($o);
		}

		switch ($type) {
			case 'gradient':
				$bg_styles = $this->cws_print_gradient($gradient);
				$bg_styles .= "opacity:".esc_attr((int)$opacity/100).";";
				break;
			case 'color':
				$bg_styles = $this->cws_print_rgba('background-color', $color, $opacity);
				break;
		}
		return $bg_styles;
	}

	public function cws_print_rgba($prefix, $color, $op = 100){
		return sprintf('%s:rgba(%s,%s);', $prefix, $this->cws_Hex2RGB($color), (int)$op/100);
	}

	// prints associative array keys with prefix and value
	public function cws_print_keys($a, $prefix = '') {
		$out = '';
		if (is_array($a)) {
			foreach ($a as $key => $value) {
				if (!is_array($key) && !empty($value)) {
					$out .= $prefix . $key . '="' . $value . '" ';
				}
			}
		}
		return trim($out);
	}

	public function cws_print_css_keys($a, $prefix = '', $suffix = '') {
		$out = '';
		if (is_array($a)) {
			foreach ($a as $key => $value) {
				if (!is_array($a[$key]) && !empty($value)) {
					if ('position' === $key) {
						$out .= $prefix . $key . ':' . $this->cws_print9positions($value) . $suffix . ';';
					} else {
						$out .= $prefix . $key . ':' . $value . $suffix . ';';
					}
				}
			}
		}
		return trim($out);
	}

	public function cws_print_parallaxify_atts($opts, &$atts, &$layer_atts) {
		if (!empty($opts)) {
			$atts .= $this->cws_print_keys($opts, 'data-');
			$layer_atts .= 'position:absolute;z-index:1;left:-'.esc_attr($opts['limit-y']).'px;right:-'.esc_attr($opts['limit-y']).'px;top:-'.esc_attr($opts['limit-x']).'px;bottom:-'.esc_attr($opts['limit-x']).'px;';
		}
	}

	public function cws_print_border_box($box) {
		$out = $type_color = '';
		if ( !empty($box) && !empty($box['border']) ){
			if(isset($box['border_type']) && isset($box['border_color'])){
				$type_color = sprintf('%s %s', $box['border_type'], $box['border_color']);
			}

			if(isset($box['border']) && is_array($box['border'])){
				foreach ($box['border'] as $key => $value) {
					$out .= "border-{$value}:1px {$type_color};";
				}
			}
		}
		return $out;
	}

	public function cws_print_img_html($img, $img_args, &$img_height = null) {
		$src = '';
		$img_h = 0;


		if ($img && !is_array($img) ) {
			$attach = wp_get_attachment_image_src( $img, 'full' );
			if ($attach) {
				list($src, $width, $height) = $attach;
				$img = array('src'=> $src, 'width' => $width, 'height' => $height, 'is_high_dpi' => '1', 'id' => $img);
			} else {
				return $src;
			}
		} else if ($img && !isset($img['is_high_dpi'] ) ) {
			$img['is_high_dpi'] = '1';
		} else if (empty($img['width']) && empty($img['height'])) {
			$attach = wp_get_attachment_image_src( $img['id'], 'full' );
			if ($attach) {
				list($src, $width, $height) = $attach;
				$img['width'] = $width;
				$img['height'] = $height;
			}
		}

		$is_high_dpi = (isset($img['is_high_dpi']) && $img['is_high_dpi'] == '1');

		if ( $is_high_dpi ) {
			if ( empty($img_args['width']) && empty($img_args['height']) ) {
				if (isset($img['width']) && isset($img['height'])) {

					$img_args = array(
						'width' => floor( (int) $img['width'] / 2 ),
						'height' => floor( (int) $img['height'] / 2 ),
						'crop' => true
					);
				}
			}

			$thumb_obj = cws_thumb( isset($img['id']) ? $img['id'] : $img['src'], $img_args );

			if ($thumb_obj) {
				$img_h = !empty($img_args["height"]) ? $img_args["height"] : '';
				$thumb_path_hdpi = !empty($thumb_obj[3]) ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
				$src = $thumb_path_hdpi;
			}
		} else {
			if ( empty($img_args['width']) && empty($img_args['height']) ) {
				if (isset($img['width']) && isset($img['height'])) {
					$img_args = array(
						'width' => floor( (int) $img['width'] ),
						'height' => floor( (int) $img['height'] ),
						'crop' => true
					);
				}
			}
			$thumb_obj = cws_thumb( isset($img['id']) ? $img['id'] : $img['src'], $img_args );

			if ($thumb_obj) {
				$img_h = !empty($img_args["height"]) ? $img_args["height"] : '';
				$thumb_path_hdpi = !empty($thumb_obj[3]) ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
				$src = $thumb_path_hdpi;
			}
		}

		if ($img_height) {
			$img_height = $img_h;
		}
		return $src;
	}
	public function cws_print_svg_html($img, $img_args, &$img_height = null) {

		$svg = '';
		if ( !empty($img_args['width']) && !empty($img_args['height']) ) {
			$svg .= "<span class='cws_logotype_svg' style='width:{$img_args['width']}px;height:{$img_args['height']}px'>";
		}
		if(!empty($img['src'])){
			global $wp_filesystem;
			WP_Filesystem();
			$upload_dir = wp_upload_dir();
			$file_parts = pathinfo($img['src']);
			$dir = str_replace($upload_dir['baseurl'], "", $file_parts['dirname']);
			$dir = $wp_filesystem->find_folder($upload_dir['basedir'] . $dir);
		    $file = trailingslashit($dir) . $file_parts['basename'];
		    if($wp_filesystem->exists($file)){
		    	$svg .= $wp_filesystem->get_contents($file);
		    }
		}

		if ( !empty($img_args['width']) && !empty($img_args['height']) ) {
			$svg .= "</span>";
		}
		return $svg;
	}

	public function cws_print9positions($pos){
		$bg_pos = '';
		for ($i=0; $i<2;$i++) {
			$c = $pos[$i];
			switch ($c) {
				case 'l':
					$bg_pos .= ' left';
					break;
				case 'r':
					$bg_pos .= ' right';
					break;
				case 'c':
					$bg_pos .= ' center';
					break;
				case 'b':
					$bg_pos .= ' bottom';
					break;
				case 't':
					$bg_pos .= ' top';
					break;
			}
		}
		return trim($bg_pos);
	}
//****************************** //CWS PRINT FUNCTIONS ******************************

	public function cws_process_blur() {
		//Get metaboxes from page
		$title_box = $this->cws_get_meta_option( 'title_box' );
		if ($title_box) {
			extract($title_box, EXTR_PREFIX_ALL, 'title_box');

			if (isset($title_box_use_blur) && $title_box_use_blur == '1'){
				$out = '.pic.blured img.blured-img,
						.item .pic_alt .img_cont>img.blured-img,
						.pic .img_cont>img.blured-img,
						.cws-widget .post_item .post_thumb:hover img,
						.cws_img_frame:hover img,
						.cws-widget .cws_item_thumb .pic .blured-img{
							-webkit-filter: blur('.esc_attr($title_box_blur_intensity).'px);
								-moz-filter: blur('.esc_attr($title_box_blur_intensity).'px);
								-o-filter: blur('.esc_attr($title_box_blur_intensity).'px);
								-ms-filter: blur('.esc_attr($title_box_blur_intensity).'px);
								filter: blur('.esc_attr($title_box_blur_intensity).'px);
						}';
				return $out;
			}

		}
	}

	public function cws_loader() {
		$cws_loader = $this->cws_get_option('overlay_loader_color');
		if(!empty($cws_loader)){
			$out = '#cws_page_loader .inner:before{
				    background-image: -webkit-linear-gradient(top, '.$cws_loader.', '.$cws_loader.');
	    			background-image: -moz-linear-gradient(top, '.$cws_loader.', '.$cws_loader.');
	   				background-image: linear-gradient(to bottom, '.$cws_loader.', '.$cws_loader.');
			}';
			$out .= '#cws_page_loader .inner:after{
				    background-image: -webkit-linear-gradient(top, #ffffff, '.$cws_loader.');
	    			background-image: -moz-linear-gradient(top, #ffffff, '.$cws_loader.');
	   				background-image: linear-gradient(to bottom, #ffffff, '.$cws_loader.');
			}';
			return $out;
		}

	}

	/* THEME FOOTER */
	public function cws_page_footer (){
		//Get metaboxes from page
		$footer = $this->cws_get_meta_option( 'footer' );
		extract($footer, EXTR_PREFIX_ALL, 'footer');

		$instagram_feed_content = '';

		if ($footer_instagram_feed == '1') {
			$instagram_feed_content = do_shortcode($footer_instagram_feed_shortcode);
		}

		$footer_class = 'page_footer';
		$footer_class .= (isset($footer_border['line']) && $footer_border['line'] == '1' ? ' border_line' : '');
		$footer_class .= empty( $footer_sidebar ) || !is_active_sidebar( $footer_sidebar ) ? ' empty_footer' : '';
		$footer_class .= $footer_instagram_feed == '1' ? (($footer_instagram_feed_full_width == '1' ) ? ' instagram_feed instagram_feed_full_width' : ' instagram_feed') : '';
		$footer_class .= $footer_fixed == '1' ? ' footer_fixed' : '';

		if ($footer_instagram_feed == '1' || (!empty( $footer_sidebar ) && is_active_sidebar( $footer_sidebar )) ){

			echo "<footer class='".esc_attr($footer_class)."'>";

				if ($footer_overlay['type'] != 'none'){
					echo "<div class='bg_layer'></div>";
				}

				if ( !empty($footer_pattern_image['image']['src']) ) {
					echo "<div class='footer-pattern'></div>";
				}

				if ($footer_instagram_feed == '1' && $footer_instagram_feed_full_width == '1') { printf('%s', $instagram_feed_content); }
				echo "<div class='container".($footer_wide == '1' ? ' wide_container' : '')."'>";
					echo "<div class='footer_container col-".esc_attr($footer_layout)."'>";
						if ($footer_instagram_feed == '1' && $footer_instagram_feed_full_width == '0') { printf('%s', $instagram_feed_content); }

						$GLOBALS['footer_is_rendered'] = true;
						if ( !empty( $footer_sidebar ) && is_active_sidebar( $footer_sidebar ) ) {
							dynamic_sidebar( $footer_sidebar );
						}
						unset( $GLOBALS['footer_is_rendered'] );
					echo '</div>';
				echo '</div>';
			echo "</footer>";
		}

		$footer_copyrights_text = stripslashes($footer_copyrights_text);
		$social_links = '';
		$social_links = $this->cws_render_social_links('copyrights');
		$show_wpml_footer = CWS_WPML_ACTIVE;

		$flags = '';
		if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
			global $wpml_language_switcher;
			$slot = $wpml_language_switcher->get_slot( 'statics', 'footer' );
			$template = $slot->get_model();
			$flags = $slot->is_enabled();
		}

		ob_start();
		if ( !empty( $footer_copyrights_text ) ) {
			echo "<div class='copyrights' >";
				echo wp_kses( $footer_copyrights_text, array(
					'a' 	=> array(
						'href'	=> array(),
						'title' => array()
					),
					'span' 	=> array(),
					'p' 	=> array(),
					'i' 	=> array(
						'class'	=> array()
					),
					'br' 	=> array(),
					'center' 	=> array(),
					'em' 	=> array(),
					'strong'=> array(),
				));
			echo "</div>";
		}

		$menu_locations = get_nav_menu_locations();
		$custom_menu = isset($footer_override_menu) && $footer_override_menu == '1' && !empty($footer_custom_menu) ? $footer_custom_menu : '';

		if ( !empty($menu_locations['copyrights-menu']) && $footer_menu_enable ) {
		?>
			<div class="footer_nav_part <?php echo !empty($footer_menu_position) ? 'menu-' . esc_attr($footer_menu_position) : ''; ?>">
				<nav class="footer-nav-container">
				<?php
					ob_start();
						wp_nav_menu( array(
							'theme_location' => (!empty($custom_menu) ? '' : 'copyrights-menu'),
							'menu' => (!empty($custom_menu) ? $custom_menu : ''),
							'menu_class' => 'main-menu copyrights-menu',
							'container' => false,
							'walker' => new Cryptop_Walker_Nav_Copyright_Menu($this)
						) );
					echo ob_get_clean();
				?>
				</nav>
			</div>
		<?php
		}

		if ( !empty( $social_links ) || !empty( $show_wpml_footer ) ) {
			echo "<div class='copyrights_panel'>";
				echo "<div class='copyrights_panel_wrapper'>";
					echo !empty( $social_links ) ? $social_links : '';
					$class_wpml = '';
					if(isset($template['template']) && !empty($template['template'])){
						if($template['template'] == 'wpml-legacy-vertical-list'){
							$class_wpml = 'wpml_language_switch lang_bar '. esc_attr($template['template']);
						}
						else{
							$class_wpml = 'wpml_language_switch horizontal_bar '.esc_attr($template['template']);
						}
					} else{
						$class_wpml = 'lang_bar';
					}
					ob_start();
						do_action( 'wpml_footer_language_selector');
					$wpml_footer_result = ob_get_clean();

					if ( $show_wpml_footer && !empty($flags) && !empty($wpml_footer_result) ) : ?>
						<div class="<?php echo esc_attr($class_wpml);?>">
							<?php
							printf('%s', $wpml_footer_result);
							?>
						</div>
					<?php endif;
				echo '</div>';
			echo '</div>';
		}
		$copyrights_content = ob_get_clean();

		$copyrights_area_class = 'copyrights_area';
		$copyrights_area_class .= $footer_fixed == '1' ? ' footer_fixed' : '';

		if ( !empty( $copyrights_content ) ) {
			echo "<div class='".esc_attr($copyrights_area_class)."'>";
				echo "<div class='container'>";
					echo "<div class='copyrights_container a-".esc_attr($footer_text_alignment)."'>";
						printf('%s', $copyrights_content);
					echo '</div>';
				echo '</div>';
			echo '</div>';
		}

		echo ($this->cws_get_option('boxed_layout')['enable'] == '1' || $this->cws_get_meta_option('boxed')['enable'] == '1') ? '</div>' : '';
	}
	/* END THEME FOOTER */

	/******************** \TYPOGRAPHY ********************/

	public function cws_layout_class ($classes=array()) {
		global $cws_theme_default;

		if ($cws_theme_default){
			array_push( $classes, 'cws_default' );
		}

		$mobile_menu = $this->cws_get_meta_option( 'mobile_menu' );
		if (!empty($mobile_menu['enable_on_tablet']) && $mobile_menu['enable_on_tablet'] == '1'){
			array_push( $classes, 'show_mobile_menu' );
		}

		//Boxed layout
		$boxed_enable = $this->cws_get_option('boxed_layout')['enable'] == '1' || $this->cws_get_meta_option('boxed')['enable'] == '1';
		if ($boxed_enable) {
			array_push( $classes, 'override_boxed_layout' );
		} else {
			array_push( $classes, 'wide' );
		}

		return $classes;
	}

	public function cws_widgets_init() {
		$sidebars = $this->cws_get_option('sidebars');
		$sidebars = isset($sidebars) ? $sidebars :
		array(
			array('title' => 'Footer'),
			array('title' => 'Blog Right'),
			array('title' => 'Page Right'),
		); //Check default

		if (!empty($sidebars) && function_exists('register_sidebars')) {
			foreach ($sidebars as $sb) {
				if ($sb && !empty($sb['title'])) {
					register_sidebar( array(
						'name' => $sb['title'],
						'id' => strtolower(preg_replace("/[^a-z0-9\-]+/i", "_", esc_html($sb['title']) )),
						//Uncomment this line to add divider to widgets
						// 'before_widget' => '<div class="cws-widget"><span class="cws-widget-circle"><span class="cws-widget-innter-circle"></span></span>',
						'before_widget' => '<div class="cws-widget">',
						'after_widget' => '</div>',
						'before_title' => '<div class="widget-title"><div class="inherit-wt">',
						'after_title' => '</div></div>',
					));
				}
			}
		}
	}

	public function cws_theme_reset_styles() {
		wp_enqueue_style('reset', CRYPTOP_URI . '/css/reset.css');
	}

	public function cws_theme_enqueue_styles() {

		if((is_admin() && !is_shortcode_preview()) || 'wp-login.php' == basename(esc_attr($_SERVER['PHP_SELF'])) ) {
			return;
		}
		wp_register_style( 'cws-theme-options', false, array('cws_main') );
		wp_enqueue_style( 'cws-theme-options' );
		$styles =	array(
			'layout' => 'layout.css',
			'font-awesome' => 'font-awesome.css',
			'fancybox' => 'jquery.fancybox.css',
			'select2_init' => 'select2.css',
			'animate' => 'animate.css',
		);

		foreach($styles as $key=>$sc){
			wp_enqueue_style( $key, CRYPTOP_URI . '/css/' . $sc);
		}

		$cwsfi = get_option('cwsfi');
		if (!empty($cwsfi) && isset($cwsfi['css'])) {
			wp_enqueue_style( 'cwsfi-css', $cwsfi['css']);
		} else {
			wp_enqueue_style( 'cws_flaticon', CRYPTOP_URI . '/fonts/flaticon/flaticon.css' );
		};

		wp_enqueue_style( 'cws_iconpack', CRYPTOP_URI . '/fonts/cws-iconpack/flaticon.css' );

		$is_custom_color = $this->cws_get_option('is-custom-color');
		if ($is_custom_color != '1') {
			$style = $this->cws_get_option('stylesheet');
			if (!empty($style)) {
				wp_enqueue_style( 'style-color', CRYPTOP_URI . '/css/' . $style . '.css' );
			}
		}
		wp_enqueue_style( 'cws_main', CRYPTOP_URI . '/css/main.css' );
	}

	public function cws_enqueue_theme_stylesheet () {
		wp_enqueue_style( 'style', get_stylesheet_uri() );
	}

	public function cws_admin_init( $hook ) {
		$this->cws_read_options();
		wp_enqueue_style('admin-css', CRYPTOP_URI . '/core/css/mb-post-styles.css' );
		wp_enqueue_style('wp-color-picker');
		wp_enqueue_script('custom-admin', CRYPTOP_URI . '/core/js/custom-admin.js', array( 'jquery' ) );

		$cwsfi = get_option('cwsfi');
		if (!empty($cwsfi) && isset($cwsfi['css'])) {
			wp_enqueue_style( 'cwsfi-css', $cwsfi['css']);
		} else {
			wp_enqueue_style( 'flaticon', CRYPTOP_URI . '/fonts/flaticon/flaticon.css' );
		}
/*
		if (('toplevel_page_Cryptop' == $hook) || ('toplevel_page_CryptopChildTheme' == $hook)) {
			wp_enqueue_style( 'cws-redux-style' , CRYPTOP_URI . '/core/css/cws-redux-style.css' );
		}*/
	}

	public function cws_register_fonts () {
		cws_render_fonts_url ();
		$gf_url = esc_url( cws_render_fonts_url () );
		wp_enqueue_style( '', $gf_url );
	}

	private function cws_register_widgets( $cws_widgets ) {
		foreach ($cws_widgets as $w) {
			$php = get_template_directory() . '/core/widgets/' . strtolower($w) . '.php';
			if (file_exists($php)) {
				require_once $php;
				register_widget($w);
			}
		}
	}

	public function cws_after_setup_theme() {
		add_theme_support( 'post-thumbnails' );
		add_theme_support( 'automatic-feed-links' );
		add_theme_support(' widgets ');
		add_theme_support( 'title-tag' );
		add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );
		add_theme_support( 'post-formats', self::$cws_theme_config['post-formats'] );

		$nav_menus = self::$cws_theme_config['nav-menus'];
		register_nav_menus($nav_menus);

		add_theme_support( 'woocommerce' );
		add_theme_support( 'custom-background', array('default-color' => '616262') );

		$this->cws_register_widgets( self::$cws_theme_config['widgets'] );

		$user = wp_get_current_user();
		$user_nav_adv_options = get_user_option( 'managenav-menuscolumnshidden', get_current_user_id() );
		if ( is_array($user_nav_adv_options) ) {
			$css_key = array_search('css-classes', $user_nav_adv_options);
			if (false !== $css_key) {
				unset($user_nav_adv_options[$css_key]);
				update_user_option($user->ID, 'managenav-menuscolumnshidden', $user_nav_adv_options,	true);
			}
		}

		add_editor_style();
	}

	// THEME COLOR HOOK

	private function cws_print_theme_color() {
		ob_start();
		do_action( 'theme_color_hook' );
		return ob_get_clean();
	}

	public function cws_theme_color_action() {
		$out = '';

		$theme_colors_first_color = $this->cws_get_meta_option( 'theme_colors' )['first_color'];
		$theme_colors_second_color = $this->cws_get_meta_option( 'theme_colors' )['second_color'];
		$theme_colors_third_color = $this->cws_get_meta_option( 'theme_colors' )['third_color'];

		global $wp_filesystem;
		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}
		$file = get_template_directory() . '/css/theme-color.css';
		if ( $wp_filesystem->exists($file) ) {
			$file = $wp_filesystem->get_contents( $file );
				$colors = array();
				$colors[0] = '|#theme-color-1#|';
				$colors[1] = '|#theme-color-2#|';
				$colors[2] = '|#theme-color-3#|';
				$replace = array();
				$replace[0] = $this->cws_Hex2RGB($theme_colors_first_color);
				$replace[1] = $this->cws_Hex2RGB($theme_colors_second_color);
				$replace[2] = $this->cws_Hex2RGB($theme_colors_third_color);
				$new_css = preg_replace($colors, $replace, $file);
				$out .= $new_css;
		}

		$result = preg_replace('/\s+/',' ', $out);
		/*
		$result .= "
		.item .date.new_style:hover .date-cont
		{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_second_color,0.5)).";
		}
		.tribe_events_nav li a.active .title_nav_events,
		.cws_classes_nav li a.active .title_nav_classes{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_second_color,0.3)).";
		}
		.cws_portfolio_nav li a.active .title_nav_portfolio:after,
		.cws_staff_nav li a.active .title_nav_staff:after,
		.tribe_events_nav li a.active .title_nav_events:after,
		.cws_classes_nav li a.active .title_nav_classes:after{
			border-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_second_color,0.3))." transparent transparent transparent;
		}
		.cws_staff_post .post_social_links.cws_staff_post_social_links a,
		.post_social_links_classes a{
			border:3px solid ".esc_attr($this->cws_Hex2RGBA($theme_colors_second_color,0.3)).";
		}
		.cws_staff_post .post_social_links.cws_staff_post_social_links a,
		.post_social_links_classes a{
			color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_second_color,0.3)).";
		}
		.cws_pricing_plan  .pricing_plan_price_wrapper{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.85)).";
		}
		.header_cont .menu .menu-item.current-menu-item>a{
			color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,1)).";
		}
		.tabs_classes li{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.65)).";
		}
		.wrap_title .price_single_classes
		{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.7)).";
		}
		.staff_classes_single .staff_post_wrapper
		{
			border-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.2)).";
		}
		.single_classes_divider.separator-line,
		.post_info > hr
		{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.2)).";
		}
		.grid_row.single_related .carousel_nav span
		{
			border-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.3)).";
			color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.3)).";
		}
		.cws-widget form input{
			border-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		.cws-widget form label:before{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		.has-post-thumbnail .post_format_quote_media_wrapper .cws_module .quote_bg_c
		{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,1)).";
		}
		.single .news .has_thumbnail .quote-wrap .quote_bg_c,.quote_bg_c,.single_staff_wrapper .post_terms a{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,1)).";
		}
		.single_svg_divider svg
		{
			fill: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.2)).";
		}
		.link_post_src:after,.link_bg:after,.tribe-events-list .tribe-events-event-cost span{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.7)).";
		}
		.sl-icon:before,
		.info span.post_author a,
		.post_post_info > .post_meta .social_share a,
		.tribe-events-schedule h2,
		.single-tribe_events .tribe-events-schedule .recurringinfo, .single-tribe_events .tribe-events-schedule .tribe-events-cost, .single-tribe_events .tribe-events-schedule .tribe-events-divider,
		.event-is-recurring,
		.tribe-related-event-info,
		.news.single .item > .post_meta .social_share a
		{
			color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.6)).";
		}
		.comment_info_header .button-content.reply:after{
			border-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		.cws_portfolio_container, .video .cover_img,.hoverdir .cws_portfolio_container,
		.news .media_part:hover .hover-effect,
		.media_part.link_post .hover-effect,
		.cws_staff_post.posts_grid_post:hover .add_btn .cws_staff_photo:after,
		.blog_post .post_media .hover-effect,.has-post-thumbnail .post_format_quote_media_wrapper .cws_module:hover .quote_bg_c{
			background-color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.8)).";
		}
		.select2-container .select2-selection--single{
			border:1px solid ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		.wrap_desc_info *:before,.single_classes .post_time_meta:before,.single_classes .post_destinations_meta:before{
			color:".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		aside .cws-widget:before,
		aside .cws-widget:after,aside .cws-widget + .cws-widget:after{
			background:".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.25)).";
		}
		table.shop_table.woocommerce-checkout-review-order-table>tfoot{
			border-top-color:".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.2)).";
		}
		.st55,.st54,.tribe-events-list svg{
			fill:".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.2)).";
		}
		.flxmap-container,
		#wpgmza_map{
			outline: 2px solid ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		.tribe-events-list .type-tribe_events .cws-tribe-events-list:before,
		.tribe-events-list .type-tribe_events .cws-tribe-events-list:after{
			background: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.2)).";
		}
		#tribe-events-content .tribe-event-duration:before, #tribe-events-footer .tribe-events-sub-nav li a, #tribe-events-header .tribe-events-sub-nav li a, #tribe-events-footer .tribe-events-sub-nav .tribe-events-nav-next a, #tribe-events-header .tribe-events-sub-nav .tribe-events-nav-next a{
			color: ".esc_attr($this->cws_Hex2RGBA($theme_colors_first_color,0.5)).";
		}
		";
		*/

		echo sprintf("%s", $result);
	}

	public function cws_theme_rgba_color () {
		$out = '';
		$font_array = $this->cws_get_meta_option( 'theme_colors' )['first_color'];
		$rgb_color = $this->cws_Hex2RGB( $font_array );
		$rgb_color = esc_attr($rgb_color);

		$lighter_rgba_color = $rgb_color .  ",1";
		echo preg_replace('/\s+/',' ', $out);
	}

//****************************** CWS CUSTOM STYLES FUNCTIONS ******************************
	public function cws_custom_sticky_menu_styles_action () {
		$sticky_menu_border = '';
		//Get metaboxes from page
		$sticky_menu = $this->cws_get_meta_option( 'sticky_menu' );
		if ($sticky_menu) {
			extract($sticky_menu, EXTR_PREFIX_ALL, 'sticky_menu');

			$sticky_menu_style = sprintf('rgba(%s,%s)', $this->cws_Hex2RGB( $sticky_menu_background_color ), $sticky_menu_background_opacity / 100);
			$sticky_menu_span_style = sprintf('rgba(%s,%s)', $this->cws_Hex2RGB( $sticky_menu_background_color ), 1);
			$sticky_menu_border_style = $this->cws_print_border($sticky_menu_border);

		ob_start();

			if ( $sticky_menu_enable == '1' ){
				?>
				.header_cont .sticky_enable.sticky_active .menu_box{
					background-color: <?php echo esc_attr($sticky_menu_style) ?> !important;
					<?php echo esc_attr($sticky_menu_border_style) ?>
				}

				.header_cont .sticky_enable.sticky_active .menu_box .main-nav-container .menu-item > a span{
					background-color: <?php echo esc_attr($sticky_menu_span_style) ?>;
				}

				.header_cont .sticky_enable.sticky_active .menu_box .menu_left_icons .woo_mini_cart>ul,
				.header_cont .sticky_enable.sticky_active .menu_box .menu_right_icons .woo_mini_cart>ul
				{
					overflow: auto;
					height: 400px;
				}

				.header_cont .sticky_enable.sticky_active .menu_box .mobile_menu_hamburger .hamburger_icon
				{
					background-color: <?php echo esc_attr($sticky_menu_font_color) ?>;
				}

				.header_cont .sticky_enable.sticky_active .menu_box .main-menu > .menu-item>.cws_megamenu_item_title,
				.header_cont .sticky_enable.sticky_active .menu_box .main-menu > .menu-item>a,
				.header_cont .sticky_enable.sticky_active .menu_box .menu_left_icons a,
				.header_cont .sticky_enable.sticky_active .menu_box .menu_right_icons a,
				.header_cont .sticky_enable.sticky_active .menu_box .side_panel_icon,
				.header_cont .sticky_enable.sticky_active .menu_box .mini-cart a,
				.header_cont .sticky_enable.sticky_active .menu_box .mini-cart,
				.header_cont .sticky_enable.sticky_active .menu_box .search_icon
				{
					color: <?php echo esc_attr($sticky_menu_font_color) ?>;
				}
				<?php
			}

		echo ob_get_clean();
		}
	}

	public function cws_custom_header_styles_action () {
		//Get metaboxes from page
		$header_styles = '';
		$header = $this->cws_get_meta_option( 'header' );
		if ($header) {
			extract($header, EXTR_PREFIX_ALL, 'header');

			if ( isset($header_spacings) ){
				foreach ( $header_spacings as $key => $value ){
					if ( !empty( $value ) || $value == '0' ){
						$header_styles .= "padding-".esc_attr($key) . ": " . esc_attr($value) . "px;";
					}
				}
			}

			$header_styles .= isset($header_background_image['image']['src']) && !empty($header_background_image['image']['src']) ? $this->cws_print_background($header_background_image) : '';
			$header_overlay_styles = isset($header_overlay) ? "style='".esc_attr($this->cws_print_overlay($header_overlay))."'" : '';

			$styles = '';
			if ( isset( $header_order ) ) {
				$count = count($header_order);
				foreach ($header_order as $key => $value){
					if ( !empty($value) && !in_array( $value['val'], array('drop_zone_start', 'drop_zone_end', 'before_header', 'after_header') ) ){
						$z_index = $count-$key;
						$styles .= esc_attr(".header_wrapper .".esc_attr($value['val'])."{z-index:".esc_attr($z_index).";} ");
					}
				}
			}
			ob_start();
			print $styles;

			if (isset($header_background_image['image']['src']) && !empty($header_background_image['image']['src']) && $header_customize == '1'){
				?>
				.header_zone{
					<?php echo esc_attr($header_styles) ?>;
				}
				<?php
			}

			if ($header_customize == '1' && $header_overlay['type'] != 'none'){
				?>
				.header_overlay{
					<?php echo esc_attr($header_overlay_styles) ?>;
				}
				<?php
			}

			echo ob_get_clean();
		}
	}

	public function cws_custom_top_bar_styles_action () {
		$top_bar_box_style = '';
		//Get metaboxes from page
		$top_bar_box = $this->cws_get_meta_option( 'top_bar_box' );

		if ($top_bar_box) {
			extract($top_bar_box, EXTR_PREFIX_ALL, 'top_bar_box');

			$top_bar_box_style = 'background-color:'.sprintf('rgba(%s,%s);', $this->cws_Hex2RGB( $top_bar_box_background_color ), $top_bar_box_background_opacity / 100);
			$top_bar_box_style .= isset($top_bar_box_border) ? $this->cws_print_border($top_bar_box_border) : '';
			$top_bar_box_padding = '';

			if ( isset($top_bar_box_spacings) ){
				foreach ( $top_bar_box_spacings as $key => $value ){
					if ( !empty( $value ) || $value == '0' ){
						$top_bar_box_padding .= "padding-".esc_attr($key). ": " . esc_attr($value) . "px;";
					}
				}
			}

			ob_start();

			//Fix top bar menu triangle after
			if ( isset($top_bar_box_spacings['bottom']) ){

				//WPML dropdown fix
				echo ".wpml-ls-legacy-dropdown .wpml-ls-current-language .wpml-ls-sub-menu{padding-top: ".esc_attr($top_bar_box_spacings['bottom'])."px;}";
			}
			

			?>
				.top_bar_icons{
					<?php echo esc_attr($top_bar_box_padding); ?>
				}

				.top_bar_wrapper{
					<?php echo esc_attr($top_bar_box_style); ?>
				}

				.top_bar_wrapper .container .top_bar_search.show-search .row_text_search:before,
				.top_bar_wrapper .container .top_bar_icons.right_icons .social_links_wrapper.toogle-of .cws_social_links:after{
					background: -webkit-linear-gradient(to left, rgb(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>) 85%, rgba(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>, 0.1));
					background: -o-linear-gradient(to left, rgb(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>) 85%, rgba(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>, 0.1));
					background: linear-gradient(to left, rgb(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>) 85%, rgba(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>, 0.1));
				}

				.top_bar_wrapper .container .top_bar_icons.left_icons .social_links_wrapper.toogle-of .cws_social_links:after{
					background: -webkit-linear-gradient(to right, rgb(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>) 85%, rgba(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>, 0.1));
					background: -o-linear-gradient(to right, rgb(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>) 85%, rgba(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>, 0.1));
					background: linear-gradient(to right, rgb(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>) 85%, rgba(<?php esc_attr_e($this->cws_Hex2RGB( $top_bar_box_background_color )) ?>, 0.1));
				}

				.top_bar_content,
				.top_bar_wrapper .container a,
				.top_bar_wrapper .container .search_icon,
				.top_bar_wrapper .container .top_bar_icons .top_bar_links_wrapper .top_bar_box_text i,
				.top_bar_wrapper .container .social_links_wrapper .social-btn-open,
				.top_bar_wrapper .container .social_links_wrapper .social-btn-open-icon{
					color: <?php echo esc_attr($top_bar_box_font_color) ?>;
				}

				.top_bar_wrapper .container a:hover,
				.top_bar_wrapper .top_bar_search.show-search .search_icon,
				.top_bar_wrapper .container .top_bar_icons .search_icon:hover,
				.top_bar_wrapper .container .social_links_wrapper .social-btn-open.active,
				.top_bar_wrapper .container .social_links_wrapper:hover .social-btn-open{
					color: <?php echo esc_attr($top_bar_box_hover_font_color) ?>;
				}

				<?php if ($this->cws_get_meta_option('header')['customize'] == '1') :
					$top_bar_override_font_color = $this->cws_get_meta_option('header')['override_topbar_color'];
					$top_bar_override_font_color_hover = $this->cws_get_meta_option('header')['override_topbar_color_hover'];
				?>

					.header_zone .top_bar_content,
					.header_zone .top_bar_wrapper .container a,
					.header_zone .top_bar_wrapper .container .search_icon,
					.header_zone .top_bar_wrapper .container .top_bar_icons .top_bar_links_wrapper .top_bar_box_text i,
					.header_zone .top_bar_wrapper .container .social_links_wrapper .social-btn-open,
					.header_zone .top_bar_wrapper .container .social_links_wrapper .social-btn-open-icon{
						color: <?php echo esc_attr($top_bar_override_font_color) ?>;
					}

					.header_zone .top_bar_wrapper .container a:hover,
					.header_zone .top_bar_wrapper .top_bar_search.show-search .search_icon,
					.header_zone .top_bar_wrapper .container .top_bar_icons .search_icon:hover,
					.header_zone .top_bar_wrapper .container .social_links_wrapper .social-btn-open.active,
					.header_zone .top_bar_wrapper .container .social_links_wrapper:hover .social-btn-open{
						color: <?php echo esc_attr($top_bar_override_font_color_hover) ?>;
					}

				<?php endif;

			echo ob_get_clean();
		}

	}

	public function cws_custom_logo_box_styles_action(){
		//Get metaboxes from page
		$logo_box = $this->cws_get_meta_option( 'logo_box' );
		if ($logo_box) {
			extract($logo_box, EXTR_PREFIX_ALL, 'logo_box');

			$logo_style = '';
			$logo_lr_spacing = $logo_tb_spacing = '';
			$logo_style .= $this->cws_print_css_keys($logo_box_spacing, 'padding-', 'px');
			$logo_style .= isset($logo_box_border) ? $this->cws_print_border($logo_box_border) : '';

			$logo_m = $logo_box_spacing;
			if ( is_array( $logo_m ) ) {
				$logo_lr_spacing = $this->cws_print_css_keys($logo_m, 'margin-', 'px');
				$logo_tb_spacing = $this->cws_print_css_keys($logo_m, 'padding-', 'px');
			}

			$logo_box_overlay_styles = isset($logo_box_overlay) ? $this->cws_print_overlay($logo_box_overlay) : '';

			ob_start();
			?>

			.header_container .logo_box{
				<?php echo esc_attr($logo_style) ?>
			}

			.header_wrapper .logo_box{
				<?php echo esc_attr($logo_box_overlay_styles) ?>
			}

			<?php
			echo ob_get_clean();

		}
	}

	public function cws_custom_menu_box_styles_action () {
		//Get metaboxes from page
		$menu_box = $this->cws_get_meta_option( 'menu_box' );
		if ($menu_box) {
			extract($menu_box, EXTR_PREFIX_ALL, 'menu_box');

			$menu_style_margin = $this->cws_print_css_keys($menu_box_margin, 'padding-', 'px');
			$menu_style_border = isset($menu_box_border) ? $menu_box_border['color'] : '';
			$menu_style_background = $this->cws_print_rgba('background-color', $menu_box_background_color, $menu_box_background_opacity);
			$menu_style = '';
			$menu_style .= $menu_style_background;
			$menu_style .= isset($menu_box_border) ? $this->cws_print_border($menu_box_border) : '';

			ob_start();

			?>

			.header_container .menu_box {
				<?php echo esc_attr($menu_style) ?>
			}

			.header_container .menu_box .main-nav-container{
				<?php echo esc_attr($menu_style_margin) ?>
			}

			<?php
				if ($this->cws_get_option( 'woo_customize_menu' ) == '1' && $this->cws_is_woo() ) {
					if(!empty($this->cws_get_option('woo_menu_border_color'))){
						$menu_border_color = $this->cws_get_option('woo_menu_border_color');

						echo "
							.woocommerce .header_container .menu_box{
								border-color: ".esc_attr($menu_border_color)."
							}
						";
					}


					if($this->cws_get_option('show_menu_bg_color')){
						$menu_background_color = (!empty($this->cws_get_option('woo_menu_bg_color')) ? $this->cws_get_option('woo_menu_bg_color') : '#ffffff');
						$menu_background_opacity = (!empty($this->cws_get_option('woo_menu_opacity')) ? $this->cws_get_option('woo_menu_opacity') : 0);

						$menu_style_background = $this->cws_print_rgba('background-color', $menu_background_color, $menu_background_opacity);

						echo "
							.woocommerce .header_container .menu_box{
								".esc_attr($menu_style_background)."
							}
						";
					}
				}

			echo ob_get_clean();

		}
	}

	public function cws_custom_page_title_styles_action () {
		$title_area_font_color = '';
		$bg_layer_styles = '';
		$stat_img_cont_styles = '';
		//Get metaboxes from page
		$title_box = $this->cws_get_meta_option( 'title_box' );
		if ($title_box)
		{
			extract($title_box, EXTR_PREFIX_ALL, 'title_box');

			if ($title_box_customize == '1'){
				if (is_single() && $this->cws_get_option('show_on_posts') == '1'){ //Call from ThemeOptions
					$title_area_font_color = $title_box_font_color;
				} else if (is_archive() && $this->cws_get_option('show_on_archives') == '1'){ //Call from ThemeOptions
					$title_area_font_color = $title_box_font_color;
				} else{
					$title_area_font_color = $title_box_font_color;
				}
			}

			$title_box_style = isset($title_box_border) ? $this->cws_print_border($title_box_border) : '';

			$title_box_parallaxify = isset($title_box_effect) && $title_box_effect === 'hover_parallax';
			if (isset($title_box_use_blur) && $title_box_use_blur){
				$title_box_use_blur_style = "-webkit-filter:blur(".esc_attr($title_box_blur_intensity)."px);-moz-filter:blur(".esc_attr($title_box_blur_intensity)."px);-o-filter: blur(".esc_attr($title_box_blur_intensity)."px);-ms-filter:blur(".esc_attr($title_box_blur_intensity)."px);filter: blur(".esc_attr($title_box_blur_intensity)."px);";
			}

			if (isset($title_box_overlay)){
				$bg_layer_styles .= isset($title_box_overlay) ? $this->cws_print_overlay($title_box_overlay) : '';
			}

			if (isset($title_box_parallax_options)){
				$this->cws_print_parallaxify_atts($title_box_parallax_options, $title_box_parallaxify_atts, $title_box_parallaxify_layer_atts);
			}

			if ( $title_box_use_pattern == '1' ) {
				$bg_layer_styles .= isset($title_box_pattern_image['image']['src']) && !empty($title_box_pattern_image['image']['src']) ? $this->cws_print_background($title_box_pattern_image) : '';
			}

			ob_start();

			if ($title_box_customize && !empty($title_area_font_color)) {

				if ( !empty($title_box_background_image['image']['src']) ) {

					$title_box_background_image_style = $this->cws_print_background($title_box_background_image);
					$stat_img_cont_styles .= ($title_box_use_blur == 1 ? esc_attr($title_box_use_blur_style) : '').
											($title_box_parallaxify ? esc_attr($title_box_parallaxify_layer_atts) : '').
											(!empty($title_box_background_image) ? esc_attr($title_box_background_image_style) : "" ).
											( (isset($meta_title_area) && $title_box_effect == 'scroll_parallax' && $title_box_scroll_parallax == '1') || ($title_box_customize && $title_box_effect == 'scroll_parallax' && $title_box_scroll_parallax == '1') ? 'background-size:'.esc_attr($bg_header_width."px auto;") : '');

					?>
						.bg_page_header .stat_img_cont{
							<?php echo esc_attr($stat_img_cont_styles) ?>
						}
					
		<?php	} ?>

					.bg_page_header .bg_layer{
						<?php echo esc_attr($bg_layer_styles) ?>
					}

					.title_box{
						color: <?php echo esc_attr($title_area_font_color) ?>;
					}

					.bg_page_header .title h1,
					.page_title .bread-crumbs
					{
						color: <?php echo esc_attr($title_area_font_color) ?>;
					}

		<?php	} ?>

				.bg_page_header{
					<?php echo esc_attr($title_box_style) ?>;
				}
			<?php

			echo ob_get_clean();

		}
	}

	public function cws_custom_side_panel_styles_action(){
		$side_panel_bg = '';
		$side_panel_aside = '';
		$side_panel_wrapper = '';
		//Get metaboxes from page
		$side_panel = $this->cws_get_meta_option( 'side_panel' );
		if ($side_panel){
			extract($side_panel, EXTR_PREFIX_ALL, 'side_panel');

			ob_start();
			if ($side_panel_enable) {
				$bg_image = $this->cws_get_option( 'side_panel' )['bg_' . $side_panel_theme]; //Call from ThemeOptions
				$bg_src = isset($bg_image) ? $bg_image['src'] : '';
				$bg_size = $side_panel_bg_size;
				$bg_opacity = (int) $side_panel_bg_opacity / 100;
				$bg_pos = $this->cws_print9positions($side_panel_bg_position);

				$side_panel_overlay_styles = isset($side_panel_overlay) ? esc_attr($this->cws_print_overlay($side_panel_overlay)) : '';
				$side_panel_bg .= 'background-size:'.esc_attr($bg_size).';background-image: url('.esc_url($bg_src).'); background-position: '.esc_attr($bg_pos).';';
				$side_panel_aside .= 'background-color: rgba('.esc_attr($this->cws_Hex2RGB( $side_panel_bg_color )).','.esc_attr($bg_opacity).');';
				$side_panel_wrapper .= 'text-align:'.esc_attr($side_panel_logo_position);

				?>
					aside.side_panel,
					aside.side_panel .cws-widget .widget-title,
					aside.side_panel .cws-widget ul > li:not(.wpml-ls-item) a,
					aside.side_panel .cws-widget ul > li:not(.wpml-ls-item) a:hover,
					aside.side_panel .cws-widget .menu .menu-item:hover>.opener,
					aside.side_panel .cws-widget .menu .menu-item.current-menu-ancestor>.opener,
					aside.side_panel .cws-widget .menu .menu-item.current-menu-item>.opener,
					aside.side_panel .menu .menu-item:hover>a,
					aside.side_panel .menu .menu-item.current-menu-ancestor>a,
					aside.side_panel .menu .menu-item.current-menu-item>a,
					aside.side_panel  .cws-widget .ourteam_item_title a,
					aside.side_panel  .cws-widget .ourteam_item_position a,
					.side_panel_container .side_panel_bottom p,
					.side_panel_container .side_panel_bottom i,
					.side_panel_container .side_panel_bottom .cws_social_links,
					aside.side_panel .cws-widget .parent_archive .widget_archive_opener:before,
					aside.side_panel .cws-widget .menu-item-has-children .opener:before
					{
						color: <?php echo esc_attr($side_panel_bg_font_color) ?>;
					}

					.side_panel_container .side_panel_bottom .cws_social_links .cws_social_link
					{
						color: <?php echo esc_attr($side_panel_bg_font_color) ?> !important;
					}

					aside.side_panel .mobile_menu_hamburger span::before,
					aside.side_panel .mobile_menu_hamburger span::after,
					aside.side_panel .mobile_menu_hamburger:hover span::before,
					aside.side_panel .mobile_menu_hamburger:hover span::after,
					aside.side_panel .cws-widget ul > li:before,
					aside.side_panel .owl-pagination .owl-page.active:before
					{
						background-color: <?php echo esc_attr($side_panel_bg_font_color) ?>;
					}

					aside.side_panel .owl-pagination .owl-page
					{
						-webkit-box-shadow: 0px 0px 0px 1px <?php echo esc_attr($side_panel_bg_font_color) ?>;
						-moz-box-shadow: 0px 0px 0px 1px <?php echo esc_attr($side_panel_bg_font_color) ?>;
						box-shadow: 0px 0px 0px 1px <?php echo esc_attr($side_panel_bg_font_color) ?>;
					}

					.side_panel_wrapper{
						<?php echo esc_attr($side_panel_wrapper) ?>
					}

					aside.side_panel{
						<?php echo esc_attr($side_panel_aside) ?>
					}

					body .side_panel_bg{
						<?php echo esc_attr($side_panel_bg) ?>
					}

					body .side_panel_overlay{
						<?php echo esc_attr($side_panel_overlay_styles) ?>
					}

				<?php
			}
			echo ob_get_clean();

		}
	}

	public function cws_custom_footer_styles_action() {
		//Get metaboxes from page
		$footer = $this->cws_get_meta_option( 'footer' );
		if ($footer) {
			extract($footer, EXTR_PREFIX_ALL, 'footer');

			$footer_style = '';
			$footer_layer_style = '';
			$footer_pattern_style = '';
			$footer_style .= isset($footer_border) ? $this->cws_print_border($footer_border) : '';
			$footer_style .= isset($footer_background_image['image']['src']) && !empty($footer_background_image['image']['src']) ? $this->cws_print_background($footer_background_image) : '';

			if ( isset($footer_spacings) ){
				$footer_style .= $this->cws_print_paddings($footer_spacings);
			}
			$footer_style .= 'background-color:'.esc_attr($footer_background_color).';';
			$footer_layer_style .= $this->cws_print_overlay($footer_overlay);


			if (isset($footer_pattern_image['image']['src']) && !empty($footer_pattern_image['image']['src'])){
				$footer_pattern_style .= $this->cws_print_background($footer_pattern_image);
			}

			ob_start();
			?>

				.page_footer .footer-pattern{
					<?php echo esc_attr($footer_pattern_style) ?>
				}

				.page_footer{
					<?php echo esc_attr($footer_style) ?>
				}

				.page_footer .bg_layer{
					<?php echo esc_attr($footer_layer_style) ?>
				}

				.page_footer .footer_container .cws-widget h3,
				.page_footer .footer_container .cws-widget .widget-title,
				.page_footer .footer_container .cws-widget .widget-title span
				{
					color: <?php echo esc_attr($footer_title_color) ?>;
				}

				.page_footer,
				.page_footer .footer_container .cws-widget .cws_social_links a:not(.fill_icon) .cws_fa,
				.footer_container .cws-widget ul li>a,
				.footer_container .cws-widget .about_me .user_name,
				.footer_container .cws-widget .about_me .user_position,
				.footer_container .cws-widget .about_me .user_description,
				.footer_container .cws-widget .post_item .post_content,
				.footer_container .cws-widget .post_item .quote_author a,
				.footer_container .cws-widget .post_item .post_date,
				.footer_container .cws-widget .post_item .post_comments a,
				.footer_container .cws-widget .cws_tweets .tweet_content,
				.footer_container .cws-widget .recentcomments,
				.footer_container .cws-widget .information_group
				{
					color: <?php echo esc_attr($footer_font_color) ?>;
				}

				.footer_container .cws-widget .tagcloud a:hover
				{
					background-color: <?php echo esc_attr($footer_font_color) ?>;
				}

				.page_footer .footer_container .owl-pagination .owl-page {
						-webkit-box-shadow: 0px 0px 0px 1px <?php echo esc_attr($footer_font_color) ?>;
						-moz-box-shadow: 0px 0px 0px 1px <?php echo esc_attr($footer_font_color) ?>;
						box-shadow: 0px 0px 0px 1px <?php echo esc_attr($footer_font_color) ?>;
				}

				.page_footer .footer_container .owl-pagination .owl-page.active:before {
					background-color: <?php echo esc_attr($footer_font_color) ?>;
				}

				.copyrights_area{
					color: <?php echo esc_attr($footer_copyrights_font_color) ?>;
					background-color: <?php echo esc_attr($footer_copyrights_background_color) ?>;
				}

				.footer-nav-container .copyrights-menu .menu-item a{
					color: <?php echo esc_attr($footer_copyrights_font_color) ?>;
				}

				.footer-nav-container .copyrights-menu .menu-item.current-menu-item > a,
				.footer-nav-container .copyrights-menu .menu-item a:hover,
				.copyrights_panel .cws_social_links .cws_social_link:hover{
					color: <?php echo esc_attr($footer_copyrights_hover_color) ?>;
				}

				.copyrights_panel_wrapper .wpml_language_switch.lang_bar:after{
					background-color: <?php echo esc_attr($footer_copyrights_font_color) ?>;
				}

			<?php
			echo ob_get_clean();

		}
	}

	public function cws_custom_boxed_layout_styles_action() {
		$boxed_style = '';
		//Get metaboxes from page
		$boxed = $this->cws_get_meta_option( 'boxed' );
		if (isset($boxed)){
			extract($boxed, EXTR_PREFIX_ALL, 'boxed');
			ob_start();
			if ($this->cws_get_option('boxed_layout')['enable'] == '1' || $this->cws_get_meta_option('boxed')['enable'] == '1') {
				$bb = $boxed_background_image;
				if ( !empty($bb['image']['src']) ){
					$atts = $this->cws_print_css_keys($bb, 'background-');
					$atts .= 'background-image:url('.esc_url($bb['image']['src']).');';
					$atts .= 'background-color:#ffffff;';
					?>
						body.override_boxed_layout{
							<?php print esc_attr($atts); ?>;
						}
					<?php
				}

				$boxed_style .= $this->cws_print_overlay($boxed_overlay);

				?>
				body .body_overlay{
					<?php echo esc_attr($boxed_style) ?>
				}
				<?php

			}
			echo ob_get_clean();
		}
	}

	public function cws_custom_styles_action(){
		$page_spacing = $this->cws_get_meta_option('page_spacing');
		$page_content_style = '';
		if ( isset($page_spacing) ){
			foreach ( $page_spacing as $key => $value ){
				if ( !empty( $value ) || $value == '0' ){
					$page_content_style .= "padding-".esc_attr($key). ": " . esc_attr($value) . "px;";
				}
			}

			ob_start();
			?>
				#main .page_content{
					<?php echo esc_attr($page_content_style) ?>
				}

			<?php
			echo ob_get_clean();
		}
	}

	public function cws_styles_default(){
		ob_start();
		?>

			.cws_default .page_title.default_page_title .bg_layer{
				background-image: url(<?php echo esc_url(CRYPTOP_URI); ?>/img/default/header-splash.png);
			}

			.cws_default .page_title.default_page_title .stat_img_cont{
				background-image: url(<?php echo esc_url(CRYPTOP_URI); ?>/img/default/title-bg.jpg);
			}			

		<?php 
		echo ob_get_clean();
	}

//****************************** //CWS CUSTOM STYLES FUNCTIONS ******************************

	private function cws_print_theme_gradient () {
		ob_start();
		do_action( 'theme_gradient_hook' );
		return ob_get_clean();
	}

	public function cws_theme_gradient_action () {
		$out = '';
		$use_gradients = $this->cws_get_option('use_gradients');
		if ( $use_gradients ) {
			$gradient_settings = $this->cws_get_option( 'gradient_settings' );
			require_once( get_template_directory() . "/css/gradient_selectors.php" );
			if ( function_exists( "get_gradient_selectors" ) ) {
				$gradient_selectors = get_gradient_selectors();
				$out .= $this->cws_print_gradient( array(
					'settings' => $gradient_settings,
					'selectors' => $gradient_selectors,
					'use_extra_rules' => true
				));
			}
		}
		echo preg_replace('/\s+/',' ', $out);
	}

	public function cws_gradients_body_class ( $classes ) {
		$use_gradients = $this->cws_get_option('use_gradients');
		if ( $use_gradients ) {
			$classes[] = "cws_gradients";
		}
		return $classes;
	}

	public function cws_process_colors() {
		$out = $this->cws_print_theme_color();
		return preg_replace('/\s+/',' ', $out);
	}

	// \  COLOR HOOK

	public function cws_theme_header_process_fonts (){
		return $this->cws_process_fonts();
	}

	public function cws_theme_header_process_colors (){
		return $this->cws_process_colors();
	}

	public function cws_theme_header_process_blur (){
		return $this->cws_process_blur();
	}

	public function cws_theme_loader (){
		return $this->cws_loader();
	}
	/* END THE HEADER META */

	/* Comments */
	public function cws_comment_nav() {
		// Are there comments to navigate through?
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) {
		?>
		<div class="comments_nav carousel_nav_panel clearfix">
			<?php
				if ( $prev_link = get_previous_comments_link( "<span class='prev'></span><span>" . esc_html__( 'Older Comments', 'cryptop' ) . "</span>" ) ) {
					printf( '<div class="prev_section">%s</div>', esc_html($prev_link) );
				}

				if ( $next_link = get_next_comments_link( "<span>" . esc_html__( 'Newer Comments', 'cryptop' ) . "</span><span class='next'></span>" ) ) {
					printf( '<div class="next_section">%s</div>', esc_html($next_link) );
				}
			?>
		</div><!-- .comment-navigation -->
		<?php
		}
	}

	public function cws_comment_post( $incoming_comment ) {
		$comment = strip_tags($incoming_comment['comment_content']);
		$comment = esc_html($comment);
		$incoming_comment['comment_content'] = $comment;
		return $incoming_comment;
	}
	/* /Comments */


	/* SIDE PANEL */
	public function cws_side_panel() {
		//Get metaboxes from page
		$side_panel = $this->cws_get_meta_option( 'side_panel' );
		extract($side_panel, EXTR_PREFIX_ALL, 'side_panel');

		$side_panel_class = 'side_panel_container';
		$side_panel_class .= ' panel-' . (!empty($side_panel_position) ? esc_attr($side_panel_position) : 'left');
		$side_panel_class .= ' appear-' . (!empty($side_panel_appear) ? esc_attr($side_panel_appear) : 'fade');
		$side_panel_class .= ' logo-' . (!empty($side_panel_logo_position) ? esc_attr($side_panel_logo_position) : 'left');

		if ($side_panel_enable) {
			$logo = $this->cws_get_option( 'side_panel' )['logo_' . $side_panel_theme]; //Call from ThemeOptions

			$logo_src = '';
			if ( !empty( $logo['src'] ) ) {
				$logo_hw = $this->cws_get_option( 'side_panel' )['logo_dimensions']; //Call from ThemeOptions

				$bfi_args = array();
				if (is_array($logo_hw)) {
					foreach ($logo_hw as $key => $value) {
						if ( !empty($value) ){
							$bfi_args[$key] = $value;
							$bfi_args['crop'] = false;
						}
					}
				}

				$main_logo_height = '';
				$logo_src = $this->cws_print_img_html($logo, $bfi_args, $main_logo_height);
			}

			echo '
			<div class="side_panel_overlay"></div>
			<div class="'.$side_panel_class.'">
				<div class="side_panel_bg"></div>
				<aside class="side_panel '.(isset($side_panel_bottom_bar['info_icons']) ? ' bottom_bar' : '').'">
					<div class="side_panel_wrapper close-'.esc_attr($side_panel_close_position).'">
						<img '.$logo_src.' alt />
						<div class="mobile_menu_bar">
							<div class="close_side_panel mobile_menu_hamburger mobile_menu_hamburger--htx deactive">
								<span></span>
							</div>
						</div>
					</div>';

				if (!empty($side_panel_sidebar) && is_active_sidebar( $side_panel_sidebar ) ) {
					dynamic_sidebar($side_panel_sidebar);
				}
				echo '
				</aside>';

				if (isset($side_panel_bottom_bar)){

					if (isset($side_panel_bottom_bar['info_icons'])){
					?>
						<div class="side_panel_bottom">
							<div class="info_icons_rows">
								<?php foreach ($side_panel_bottom_bar['info_icons'] as $key => $value) { ?>
									<p><i class="<?php echo esc_attr($value['icon']) ?>"></i><span><?php echo esc_html($value['title']); ?></span></p>
								<?php }
							echo '</div>';

							$social_links = $this->cws_render_social_links('side_panel');
							echo !empty($social_links) ? $social_links : '';
							?>
						</div>
					<?php
					}

				}
			echo '</div>';
		}
	}
	/* SIDE PANEL */

	/* BODY OVERLAY */
	public function cws_body_overlay() {

		if ($this->cws_get_option('boxed_layout')['enable'] == '1' || $this->cws_get_meta_option('boxed')['enable'] == '1') {
			echo "<div class='body_overlay'></div>";
		}

	}
	/* BODY OVERLAY */

	public function cws_widget_title_icon_rendering( $args = array() ) {
		extract( shortcode_atts(
			array(
			'icon_type' => '',
			'icon_fa' => '',
			'icon_img' => array(),
			'icon_color' => '#fff',
			'icon_bg_type' => 'color',
			'icon_bg_color' => CRYPTOP_FIRST_COLOR,
			'gradient_settings' => array(),
		), $args['icon_opts']));

		if (empty($args['icon_opts'])) return;

		$r = $icon_styles = '';
		if ( $icon_type == 'fa' && !empty( $icon_fa ) ) {
			switch ($icon_bg_type) {
				case 'none':
					$icon_styles .= "border-width: 0px; border-style: solid;";
					break;
				case 'color':
					$icon_styles .= "background-color:".esc_attr($icon_bg_color).";";
					break;
				case 'gradient':
					$icon_styles .= esc_attr( $this->cws_render_gradient_rules($gradient_settings) );
					break;
			}

			$icon_styles .= "color:".esc_attr($icon_color).";";
			$r .= "<i class='".esc_attr($icon_fa)."' style='".esc_attr($icon_styles)."'></i>";
		}	else if ( $icon_type == 'img' && !empty( $icon_img['src'] ) ) {

			$font = $this->cws_get_meta_option( 'body-font' );
			$font_size = isset( $font['font_size'] ) ? preg_replace( 'px', '', $font['font_size'] ) : '15';
			$thumb_size = (int)round( (float)$font_size * 2 );

			$g_img = $this->cws_print_img_html(array('src' => $icon_img['src']), array( 'width' => $thumb_size, 'height' => $thumb_size ));
			$this->cws_echo_ne($g_img, "<img{$g_img} alt/>");
		}
		return $r;
	}

	private function cws_extract_array_prefix($arr, $prefix) {
		$ret = array();
		$pref_len = strlen($prefix);
		foreach ($arr as $key => $value) {
			if (0 === strpos($key, $prefix . '_') ) {
				$ret[mb_substr($key, $pref_len+1)] = $value;
			}
		}
		return $ret;
	}

	public function cws_move_comment_field_to_bottom( $fields ) {
		$comment_field = $fields['comment'];
		unset( $fields['comment'] );
		$fields['comment'] = $comment_field;
		return $fields;
	}

}
/* end of Theme's Class */

// =====================FUNCTIONS=====================
function cws_class($classes = array()){
	$out = implode(' ', array_diff( $classes, [''] ));
	return $out;
}

//CWS Render image slider
function cws_render_image_slider($page_type = 'page', &$slider_exist = false, &$slider_error = false, &$slider_shortcode = ''){
	global $cws_theme_funcs;
	$slider_settings = $cws_theme_funcs->cws_get_meta_option( 'slider_override' );

	if ($page_type == 'page'){
		$slider_shortcode = isset($cws_theme_funcs->cws_get_option( 'home_options' )['home_slider_shortcode']) ? $cws_theme_funcs->cws_get_option( 'home_options' )['home_slider_shortcode'] : '';
	} else if ($page_type == 'shop'){
		$slider_shortcode = isset($cws_theme_funcs->cws_get_option( 'shop_options' )['shop_slider_shortcode']) ? $cws_theme_funcs->cws_get_option( 'shop_options' )['shop_slider_shortcode'] : '';
	}

	if ( is_page() && $slider_settings['is_override'] == '1' ){
		$slider_shortcode = $slider_settings['slider_shortcode'];
	}

	//Check if shortcode is rendered
	$slider_plugin_exist = true;
	$slider_shortcode = htmlspecialchars_decode($slider_shortcode, ENT_QUOTES);
	// $slider_shortcode = htmlspecialchars_decode($slider_shortcode);
	$shortcode_output = do_shortcode($slider_shortcode);
	$slider_plugin_exist = !($shortcode_output === $slider_shortcode); //Check shortcode & output not the same

	if (isset($slider_settings['is_wide']) && $slider_settings['is_wide'] !== '1') {
		$shortcode_output = "<div class='slider_bg'><div class='container'>{$shortcode_output}</div></div>";
	}

	if ($slider_plugin_exist){
		print $shortcode_output;
	}

	$slider_error = strpos($shortcode_output, 'Revolution Slider Error') ? true : false;
	$slider_exist = !$slider_error && $slider_plugin_exist;
}

//CWS Render slider section
function cws_render_slider_section($page_type = 'page', &$slider_exist = false, &$slider_error = false, &$slider_shortcode = ''){
	global $cws_theme_funcs;
	if ($page_type == 'page'){
		$slider_type = $cws_theme_funcs->cws_get_option( 'home_options' )['home_slider_type'];
	} else if ($page_type == 'shop'){
		$slider_type = $cws_theme_funcs->cws_get_option( 'shop_options' )['shop_slider_type'];
	}
	switch( $slider_type ){
		case 'img-slider':
			cws_render_image_slider($page_type, $slider_exist, $slider_error);
			break;
		case 'video-slider':
			if ($page_type == 'page'){
				$video_slider_settings = isset($cws_theme_funcs->cws_get_option('home_options')['home_video_slider']) ? $cws_theme_funcs->cws_get_option('home_options')['home_video_slider'] : '';
			} else if ($page_type == 'shop'){
				$video_slider_settings = isset($cws_theme_funcs->cws_get_option('shop_options')['shop_video_slider']) ? $cws_theme_funcs->cws_get_option('shop_options')['shop_video_slider'] : '';
			}

			if (empty($video_slider_settings)) return;

			$slider_switch = isset($video_slider_settings[ 'slider_switch' ]) ? $video_slider_settings[ 'slider_switch' ] : '';
			$slider_shortcode = isset($video_slider_settings[ 'slider_shortcode' ]) ? $video_slider_settings[ 'slider_shortcode' ] : '';
			$video_type = isset($video_slider_settings[ 'video_type' ]) ? $video_slider_settings[ 'video_type' ] : '';
			$set_video_header_height = isset($video_slider_settings[ 'set_video_header_height' ]) ? $video_slider_settings[ 'set_video_header_height' ] : '';
			$video_header_height = isset($video_slider_settings[ 'video_header_height' ]) ? $video_slider_settings[ 'video_header_height' ] : '';
			$sh_source = isset($video_slider_settings[ 'sh_source' ]) ? $video_slider_settings[ 'sh_source' ] : '';
			$youtube_source = isset($video_slider_settings[ 'youtube_source' ]) ? $video_slider_settings[ 'youtube_source' ] : '';
			$vimeo_source = isset($video_slider_settings[ 'vimeo_source' ]) ? $video_slider_settings[ 'vimeo_source' ] : '';
			$color_overlay_type = isset($video_slider_settings[ 'color_overlay_type' ]) ? $video_slider_settings[ 'color_overlay_type' ] : '';
			$overlay_color = isset($video_slider_settings[ 'overlay_color' ]) ? $video_slider_settings[ 'overlay_color' ] : '';
			$color_overlay_opacity = isset($video_slider_settings[ 'color_overlay_opacity' ]) ? $video_slider_settings[ 'color_overlay_opacity' ] : '';
			$use_pattern = isset($video_slider_settings[ 'use_pattern' ]) ? $video_slider_settings[ 'use_pattern' ] : '';
			$pattern_image = isset($video_slider_settings[ 'pattern_image' ]) ? $video_slider_settings[ 'pattern_image' ] : '';
			$gradient_video_set = isset($video_slider_settings["slider_gradient_settings"]) ? $video_slider_settings["slider_gradient_settings"] : '';

			$gradient_settings = $cws_theme_funcs->cws_render_gradient($gradient_video_set);
			$video_header_height = $set_video_header_height == "1" ? $video_header_height : false;

			$sh_source = isset( $sh_source['src'] ) && !empty( $sh_source['src'] ) ? $sh_source['src'] : '';
			$color_overlay_opacity = (int)$color_overlay_opacity / 100;
			$has_video_src = false;
			$header_video_atts = '';
			$header_video_class = "fs_video_bg";
			$header_video_styles = '';
			$header_video_html = '';
			$uniqid = uniqid( 'video-' );
			$uniqid_esc = esc_attr( $uniqid );
			switch ( $video_type ){
				case 'self_hosted':
					if ( !empty( $sh_source ) ){
						$has_video_src = true;
						$header_video_class .= " cws_self_hosted_video";
						$header_video_html .= "<video class='self_hosted_video' src='".esc_url($sh_source)."' autoplay='autoplay' loop='loop' muted='muted'></video>";
					}
					break;
				case 'youtube':
					if ( !empty( $youtube_source ) ){
						wp_enqueue_script ('cws_YT_bg');
						$has_video_src = true;
						$header_video_class .= " cws_Yt_video_bg loading";
						$header_video_atts .= " data-video-source='".esc_url($youtube_source)."' data-video-id='".esc_attr($uniqid)."'";
						$header_video_html .= "<div id='".esc_attr($uniqid_esc)."'></div>";
					}
					break;
				case 'vimeo':
					if ( !empty( $vimeo_source ) ){
						wp_enqueue_script ('vimeo');
						$has_video_src = true;
						$header_video_class .= " cws_Vimeo_video_bg";
						$header_video_atts .= " data-video-source='".esc_url($vimeo_source)."' data-video-id='".esc_attr($uniqid)."'";
						$header_video_html .= "<iframe id='".esc_attr($uniqid_esc)."' src='" . esc_url($vimeo_source) . "?api=1&player_id=".esc_attr($uniqid)."' frameborder='0'></iframe>";
					}
					break;
			}
			if ( $has_video_src ){
				if ( $use_pattern && !empty( $pattern_image ) && isset( $pattern_image['url'] ) && !empty( $pattern_image['url'] ) ){
					$pattern_img_src = $pattern_image['url'];
					$header_video_html .= "<div class='bg_layer' style='background-image:url(" . esc_url($pattern_img_src) . ")'></div>";
				}
				if ( $color_overlay_type == 'color' && !empty( $overlay_color ) ){
					$header_video_html .= "<div class='bg_layer' style='background-color:" . esc_attr($overlay_color) . ";" . ( !empty( $color_overlay_opacity ) ? "opacity:".esc_attr($color_overlay_opacity).";" : '' ) . "'></div>";
				}
				else if ( $color_overlay_type == 'gradient' ){
					$gradient_rules = $cws_theme_funcs->cws_print_gradient( $gradient_settings );
					$header_video_html .= "<div class='bg_layer' style='".esc_attr($gradient_rules)."" . ( !empty( $color_overlay_opacity ) ? "opacity:".esc_attr($color_overlay_opacity).";" : '' ) . "'></div>";
				}
			}

			$header_video_atts .= !empty( $header_video_class ) ? " class='" . trim( $header_video_class ) . "'" : '';
			$header_video_atts .= !empty( $header_video_styles ) ? " style='". esc_attr($header_video_styles) ."'" : '';


			if ( !empty( $slider_shortcode ) && $has_video_src && $slider_switch == 1 ){
				$shortcode_output = do_shortcode($slider_shortcode);
				$slider_error = strpos($shortcode_output, 'Revolution Slider Error') ? true : false;
				$slider_plugin_exist = !($shortcode_output === $slider_shortcode);
				$slider_exist = !$slider_error && $slider_plugin_exist;

				echo "<div class='fs_video_slider'>";
					echo  do_shortcode( $slider_shortcode );
					echo '<div ' . $header_video_atts . '>';
						printf('%s', $header_video_html);
					echo '</div>';
				echo '</div>';
			} elseif ( $has_video_src && $slider_switch == 0 ) {
				$slider_exist = true;
				$header_video_fs_view = $video_header_height == false ? 'header_video_fs_view' : '';
				$video_height_coef = $video_header_height == false ? '' : " data-wrapper-height='".esc_attr(960 / $video_header_height)."'";
				$video_header_height = $video_header_height == false ? '' : "style='height:" . esc_attr($video_header_height) ."px'";
				echo "<div class='fs_video_slider ". sanitize_html_class( $header_video_fs_view ) ."' " . $video_header_height . " ". $video_height_coef .">";
				echo '<div ' . $header_video_atts . '>';
					printf('%s', $header_video_html);
				echo '</div>';
				echo '</div>';
			} elseif ( ! empty( $slider_shortcode ) && $slider_switch == 1 && ! $has_video_src ) {
				$shortcode_output = do_shortcode($slider_shortcode);
				$slider_error = strpos($shortcode_output, 'Revolution Slider Error') ? true : false;
				$slider_plugin_exist = !($shortcode_output === $slider_shortcode);
				$slider_exist = !$slider_error && $slider_plugin_exist;

				echo  do_shortcode( $slider_shortcode );
			} else {
				$slider_exist = true;
				if ( $has_video_src ){
					echo "<div class='fs_video_slider'></div>";
				}
			}

			break;
		case 'stat-image':
			if ($page_type == 'page'){
				$static_image = isset($cws_theme_funcs->cws_get_option('home_options')['home_static_image']) ? $cws_theme_funcs->cws_get_option('home_options')['home_static_image'] : '';
			} else if ($page_type == 'shop'){
				$static_image = isset($cws_theme_funcs->cws_get_option('shop_options')['shop_static_image']) ? $cws_theme_funcs->cws_get_option('shop_options')['shop_static_image'] : '';
			}

			if (empty($static_image)) return;

			$set_img_header_height = $static_image['set_static_image_height'];
			$img_header_height = $static_image[ 'static_image_height' ];

			$color_overlay_type = '';
			$overlay_color = '';
			$color_overlay_opacity = '';
			$gradient_settings = array();

			if ($static_image[ 'static_customize_colors' ] == "1"){
				$color_overlay_type = $static_image[ 'img_header_color_overlay_type' ];
				$overlay_color = $static_image[ 'img_header_overlay_color' ];
				$color_overlay_opacity = $static_image[ 'img_header_color_overlay_opacity' ];
				$color_overlay_opacity = (int)$color_overlay_opacity / 100;
				$gradient_settings = $cws_theme_funcs->cws_render_gradient( $static_image["img_header_gradient_settings"] );
			}

			$use_pattern = $static_image[ 'img_header_use_pattern' ];
			$pattern_image = $static_image[ 'img_header_pattern_image' ];

			$img_header_height = $set_img_header_height == "1" ? $img_header_height : false;

			$parallax_header_opt = $static_image['img_header_parallax_options'];

			$img_header_parallaxify = $static_image["img_header_parallaxify"];

			if ($img_header_parallaxify == '1'){
				$img_header_scalar_x = $parallax_header_opt["img_header_scalar_x"];
				$img_header_scalar_y = $parallax_header_opt["img_header_scalar_y"];
				$img_header_limit_x = $parallax_header_opt["img_header_limit_x"];
				$img_header_limit_y = $parallax_header_opt["img_header_limit_y"];

				$img_header_parallaxify_atts = ' data-scalar-x="'.esc_attr($img_header_scalar_x).'" data-scalar-y="'.esc_attr($img_header_scalar_y).'" data-limit-y="'.esc_attr($img_header_limit_y).'" data-limit-x="'.esc_attr($img_header_limit_x).'"';
				$img_header_parallaxify_layer_atts = 'position: absolute; z-index: 1; left: -'.esc_attr($img_header_limit_y).'px; right: -'.esc_attr($img_header_limit_y).'px; top: -'.esc_attr($img_header_limit_x).'px; bottom: -'.esc_attr($img_header_limit_x).'px;';
			}

			$image_options = $static_image["static_image"];

			$default_img = false;
			$override_img = false;
			$img_url = '';

			$header_img_html = '';

			if ( isset( $pattern_image['src'] ) ){
				if ( $use_pattern && !empty( $pattern_image ) && isset( $pattern_image['src'] ) && !empty( $pattern_image['src'] ) ){
					$pattern_img_src = $pattern_image['src'];
					$header_img_html .= "<div class='bg_layer' style='background-image:url(" . esc_url($pattern_img_src) . ");".($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '')."'></div>";
				}
				if ( $color_overlay_type == 'color' && !empty( $overlay_color ) ){
					$header_img_html .= "<div class='bg_layer' style='background-color:" . esc_attr($overlay_color) . ";" . ( !empty( $color_overlay_opacity ) ? "opacity:".esc_attr($color_overlay_opacity).";" : '' ) . ";".($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '')."'></div>";
				}
				else if ( $color_overlay_type == 'gradient' && !empty( $gradient_settings ) ){
					$gradient_rules = $cws_theme_funcs->cws_print_gradient( $gradient_settings );
					$header_img_html .= "<div class='bg_layer' style='".esc_attr($gradient_rules) . ( !empty( $color_overlay_opacity ) ? "opacity:".esc_attr($color_overlay_opacity).";" : '' ) . ";".($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '')."'></div>";
				}
			}

			if ( isset( $image_options['src'] ) ){
				$slider_exist = true;
				$header_img_fs_view = $img_header_height== false ? 'header_video_fs_view' : '';
				$header_img_height_coef = $img_header_height == false ? '' : " data-wrapper-height='".esc_attr(960 / $img_header_height)."'";
				$img_header_height = $img_header_height == false ? '' : "style='height:" . esc_attr($img_header_height) ."px'";

				printf("<div class='fs_img_header %s' %s %s>", sanitize_html_class( $header_img_fs_view ), $img_header_height, $header_img_height_coef);

				if ($img_header_parallaxify) { printf('<div class="cws_parallax_section" %s>', $img_header_parallaxify_atts); }
					wp_enqueue_script ('parallax');
					if ($img_header_parallaxify) { echo '<div class="layer" data-depth="1.00">'; }
						printf('%s', $header_img_html);
						echo "<div class='stat_img_cont' style='". ($img_header_parallaxify ? $img_header_parallaxify_layer_atts : '') ."background-image: url(".esc_url($image_options['src']).");background-size: cover;background-position: center center;'></div>";
					if ($img_header_parallaxify){
						echo "</div>";
					echo "</div>";
					}
				echo '</div>';
			}
		break;
		default:
	}
}

//Determinate
function cws_is_single($id = ''){

	if ( is_plugin_active('elementor/elementor.php') ){

		if (!empty($id)){

			if (is_elementor($id)){ //Is back-end or front-end Elementor page
				$out = false;
			} else {
				$out = true; //Is Single post
			}

		} else {

			if (is_elementor()){ //Is back-end or front-end Elementor page
				$out = false;
			} else {
				$out = true; //Is Single post
			}

		}

	} else {

		if (!empty($id)){
			$out = is_single($id);
		} else {
			$out = is_single();
		}

	}

	return $out;
}

//Front-end
function cws_is_front_end_page(){
	return is_page() && is_elementor();
}

//Back-end
function cws_is_back_end_page(){
	return is_single() && is_elementor();
}

if(!function_exists('cws_page_links')){
	function cws_page_links(){
		$args = array(
			'before'		   => '',
			'after'			=> '',
			'link_before'	  => '<span>',
			'link_after'	   => '</span>',
			'next_or_number'   => 'number',
			'nextpagelink'	 =>  esc_html__("Next Page",'cryptop'),
			'previouspagelink' => esc_html__("Previous Page",'cryptop'),
			'pagelink'		 => '%',
			'echo'			 => 0
		);
		$pagination = wp_link_pages( $args );
		echo !empty( $pagination ) ? "<div class='pagination'><div class='page_links'>$pagination</div></div>" : '';
	}
}

if(!function_exists('cws_pagination')){
	function cws_pagination ( $paged=1, $max_paged=1, $ajax = true ){
		$is_rtl = is_rtl();

		$pagenum_link = html_entity_decode( get_pagenum_link() );
		$query_args   = array();
		$url_parts	= explode( '?', $pagenum_link );

		if ( isset( $url_parts[1] ) ) {
			wp_parse_str( $url_parts[1], $query_args );
		}

		$permalink_structure = get_option('permalink_structure');

		$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
		$pagenum_link = $permalink_structure ? trailingslashit( $pagenum_link ) . '%_%' : trailingslashit( $pagenum_link ) . '?%_%';
		$pagenum_link = add_query_arg( $query_args, $pagenum_link );

		$format  = $permalink_structure && preg_match( '#^/*index.php#', $permalink_structure ) && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
		$format .= $permalink_structure ? user_trailingslashit( 'page/%#%', 'paged' ) : 'paged=%#%';

		printf('<div class="pagination %s">', ($ajax ? 'ajax' : ''));
		?>
			<div class='page_links'>
			<?php
			$pagination_args = array( 'base' => $pagenum_link,
				'format' => $format,
				'current' => $paged,
				'total' => $max_paged,
				"prev_text" => "<i class='fa fa-angle-" . ( $is_rtl ? "right" : "left" ) . "'></i>",
				"next_text" => "<i class='fa fa-angle-" . ( $is_rtl ? "left" : "right" ) . "'></i>",
				"link_before" => '',
				"link_after" => '',
				"before" => '',
				"after" => '',
				"mid_size" => 2,
			);
			$pagination = paginate_links($pagination_args);
			print $pagination;
			?>
			</div>
		</div>
		<?php

	}
}

//Swiper carousel attr generator
if(!function_exists('swiper_carousel')){
	function swiper_carousel( $fill_atts = array(), $is_carousel = false ){
		$data_attr = '';

		if (!empty($fill_atts)){
			extract( $fill_atts );
		} else {
			return $data_attr;
		}

		if ($is_carousel && !$related_carousel){
			$carousel_atts = array(
				'slidesPerView' => ( $columns_count != 'auto') ? (int)$columns_count : $columns_count,
				'slidesPerColumn' => (int)$slides_in_columns,
				'slidesPerColumnFill' => $slides_view,
				'slidesPerGroup' => (int)$slides_to_scroll,
				'direction' => $slider_direction,
				'autoHeight' => $auto_height == 'yes',
				'keyboard' => $keyboard_control == 'yes',
				'mousewheel' => $mousewheel_control == 'yes',
				'centeredSlides' => $item_center == 'yes',
				'spaceBetween' => $spacing_slides['size'],
				'freeMode' => $free_mode == 'yes',
				'effect' => $slide_effects,
				'autoplay' => ($autoplay == 'yes') ? array('delay' => $autoplay_speed) : false,
				'autoplayPause' => ($autoplay == 'yes' && $pause_on_hover == 'yes'),
				'parallax' => $parallax == 'yes',
				'loop' => $loop == 'yes',
				'speed' => $animation_speed,
			);

			if ($navigation != 'none'){
				//Arrows
				$navigation_arrows = true;

				//Bullets
				$navigation_pagination = array(
					'el' => '.swiper-pagination',
					'type' => $pagination_style,
					'clickable' => true,
					'dynamicBullets' => $dynamic_bullets == 'yes',
				);

				//Scrollbar
				$navigation_scrollbar = array(
					'el' => '.swiper-scrollbar',
					'draggable' => true
				);

				if ($navigation == 'both'){
					$carousel_atts['navigation'] = $navigation_arrows;
					if ($pagination_style == 'scrollbar'){
						$carousel_atts['scrollbar'] = $navigation_scrollbar;
					} else {
						$carousel_atts['pagination'] = $navigation_pagination;
					}
				} else if ($navigation == 'arrows') {
					$carousel_atts['navigation'] = $navigation_arrows;
				} else if ($navigation == 'pagintaion') {
					if ($pagination_style == 'scrollbar'){
						$carousel_atts['scrollbar'] = $navigation_scrollbar;
					} else {
						$carousel_atts['pagination'] = $navigation_pagination;
					}
				}
			}

			$carousel_atts_str = json_encode($carousel_atts);

			$data_attr.= " data-swiper-args='".$carousel_atts_str."'";
		} else if (isset($related_carousel) && $related_carousel){
			$carousel_atts = array(
				'items'			=> (int)$layout,
				'nav'			=> true,
				'autoplay'		=> true,
				'autoplaySpeed'	=> 1000,
				'loop'			=> true,
			);

			$carousel_atts_str = json_encode($carousel_atts);
			$data_attr.= " data-owl-args='".$carousel_atts_str."'";
		}

		return $data_attr;
	}
}

//Filter nav template
if(!function_exists('posts_filters_nav')){
	function posts_filters_nav ( $filter_vals = array(), $title = '' ){
		$out = '';

		$out .= "<nav class='nav cws_filter_nav'>";
			$out .= "<ul class='dots'>";
			$out .= !empty( $title ) ? "<h2 class='widgettitle'>" . esc_html( $title ) . "</h2>" : "";
			$out .= "<li class='cws_post_select_dots circle'></li>";
				$out .= "<li class='dot all'>";
					$out .= "<a href class='nav_item cws_filter_nav_item active' data-nav-val='_all_'>";
						$out .= "<span class='title_nav_filter'>";
							$out .= "<span class='txt_title'>" . esc_html__( 'All', 'cryptop' ) ."</span>";
						$out .= "</span>";
						$out .= "<span class='circle'></span>";
					$out .= "</a>";
				$out .= "</li>";
				foreach ( $filter_vals as $term_slug => $term_name ){
					$out .= "<li class='dot'>";
						$out .= "<a href class='nav_item cws_filter_nav_item' data-nav-filter='." . esc_html( $term_slug ) . "' data-nav-val='" . esc_html( $term_slug ) . "'>";
							$out .= "<span class='title_nav_filter'>";
								$out .= "<span class='txt_title'>" . esc_html( $term_name )."</span>";
							$out .= "</span>";
							$out .= "<span class='circle'></span>";
						$out .= "</a>";
					$out .= "</li>";
				}
			$out .= "</ul>";
			$out .= "<span class='magicline'></span>";
		$out .= "</nav>";

		return $out;
	}
}

if(!function_exists('cws_loader_html')){
	function cws_loader_html ( $args = array() ){
		extract( wp_parse_args( $args, array(
			'holder_id'		=> '',
			'holder_class' 	=> '',
			'loader_id'		=> '',
			'loader_class'	=> ''
		)));
		$holder_class 	.= " cws_loader_holder";
		$loader_class 	.= " cws_loader";
		$holder_id		= esc_attr( $holder_id );
		$holder_class 	= esc_attr( trim( $holder_class ) );
		$loader_id		= esc_attr( $loader_id );
		$loader_class 	= esc_attr( trim( $loader_class ) );
		echo "<div " . ( !empty( $holder_id ) ? " id='$holder_id'" : "" ) . " class='$holder_class'>";
			echo "<div " . ( !empty( $loader_id ) ? " id='$loader_id'" : "" ) . " class='$loader_class'>";
				?>
				<svg width='104px' height='104px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-default"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(0 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(30 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.08333333333333333s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(60 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.16666666666666666s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(90 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.25s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(120 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.3333333333333333s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(150 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.4166666666666667s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(180 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(210 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5833333333333334s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(240 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.6666666666666666s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(270 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.75s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(300 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.8333333333333334s' repeatCount='indefinite'/></rect><rect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(330 50 50) translate(0 -30)'>  <animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.9166666666666666s' repeatCount='indefinite'/></rect></svg>
				<?php
			echo "</div>";
		echo "</div>";
	}
}

if(!function_exists('cws_load_more')){
	function cws_load_more ( $onScroll = false ){
	?>
		<div class='load_more_wrapper'>
			<a class="cws_button cws_load_more<?php echo ($onScroll ? ' load_on_scroll' : '');?>" href="#"><?php esc_html_e( "Load More", 'cryptop' ); ?></a>
		</div>
	<?php
	}
}

//Add inline styles to enqueue
if(!function_exists('Cws_shortcode_css')){
    function Cws_shortcode_css() {
        return Cws_shortcode_css::instance();
    }
}

if ( !class_exists( "Cws_shortcode_css" ) ){
    class Cws_shortcode_css{
        public $settings;
        protected static $instance = null;

        public static function instance() {
            if ( is_null( self::$instance ) ) {
                self::$instance = new self();
            }
            return self::$instance;
        }
        public function enqueue_cws_css( $style ) {
            if(!empty($style)){
                ob_start();
                    printf('%s', $style);
                $css = ob_get_clean();
                $css = apply_filters( 'cws_enqueue_shortcode_css', $css, $style );

                wp_register_style( 'cws-footer', false );
                wp_enqueue_style( 'cws-footer' );
                wp_add_inline_style( 'cws-footer', $css );
            }

        }
    }
}
//Add inline styles to enqueue

/* FA ICONS */
function cws_get_all_fa_icons() {
	$meta = get_option('cws_fa');
	if (!empty($meta) || (time() - $meta['t']) > 3600*7 ) {
		global $wp_filesystem;
		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}
		$file = get_template_directory() . '/fonts/font-awesome/font-awesome.css';
		$fa_content = '';
		if ( $wp_filesystem && $wp_filesystem->exists($file) ) {
			$fa_content = $wp_filesystem->get_contents($file);
			if ( preg_match_all( "/fa-((\w+|-?)+):before/", $fa_content, $matches, PREG_PATTERN_ORDER ) ) {
				return $matches[1];
			}
		}
	} else {
		return $meta['fa'];
	}
}
/* \FA ICONS */

/* FL ICONS */
function cws_get_all_flaticon_icons() {
	$cwsfi = get_option('cwsfi');
	if (!empty($cwsfi) && isset($cwsfi['entries'])) {
		return $cwsfi['entries'];
	} else {
		global $wp_filesystem;
		if( empty( $wp_filesystem ) ) {
			require_once( ABSPATH .'/wp-admin/includes/file.php' );
			WP_Filesystem();
		}
		$file = get_template_directory() . '/fonts/flaticon/flaticon.css';
		$fi_content = '';
		$out = '';
		if ( $wp_filesystem && $wp_filesystem->exists($file) ) {
			$fi_content = $wp_filesystem->get_contents($file);
			if ( preg_match_all( "/flaticon-((\w+|-?)+):before/", $fi_content, $matches, PREG_PATTERN_ORDER ) ){
				return $matches[1];
			}
		}
	}
}
/* \FL ICONS */

/********************************** !!! **********************************/

function cws_twitter_renderer ( $atts, $content = '' ) {
	extract( shortcode_atts( array(
		'in_widget' => false,
		'title' => '',
		'centertitle' => '0',
		'items' => get_option( 'posts_per_page' ),
		'visible' => get_option( 'posts_per_page' ),
		'showdate' => '0'
	), $atts));
	$out = '';
	global $cws_theme_funcs;
	$tw_username = $cws_theme_funcs->cws_get_option( 'tw-username' );
	if ( !is_numeric( $items ) || !is_numeric( $visible ) ) return $out;
	$tweets = cws_getTweets( (int)$items );
	if ( is_string( $tweets ) ) {
		$out .= do_shortcode( "[cws_sc_msg_box title='" . esc_html__( 'Twitter responds:', 'cryptop' ) . "' text='$tweets' is_closable='1'][/cws_sc_msg_box]" );
	}
	else if ( is_array( $tweets ) && isset($tweets['error']) ){
		echo esc_html($tweets['error']);
	}
	else if ( is_array( $tweets ) ) {
		$use_carousel = count( $tweets ) > $visible;
		$section_class = "cws_tweets";
		$section_class .= $use_carousel ? " tweets_carousel" : '';
		$section_class .= $use_carousel && empty( $title ) ? " paginated" : '';
		$out .= !empty( $title ) ? $cws_theme_funcs::THEME_BEFORE_CE_TITLE . "<div" . ( $centertitle ? " style='text-align:center;'" : '' ) . ">".esc_html($title)."</div>" . $cws_theme_funcs::THEME_AFTER_CE_TITLE : '';
		if ( $use_carousel && !$in_widget ) {
			$out .= "<div class='tweets_carousel_header'>";
				$out .= "<a href='http://twitter.com/".esc_attr($tw_username)."' class='follow_us fa fa-twitter' target='_blank'></a>";
			$out .= "</div>";
		}
		$out .= "<div class='".esc_attr($section_class)."'>";
			$out .= "<div class='cws_wrapper'>";
				$carousel_item_closed = false;
				for ( $i=0; $i<count( $tweets ); $i++ ) {
					$tweet = $tweets[$i];
					if ( $use_carousel && ( $i == 0 || $carousel_item_closed ) ) {
						wp_enqueue_script ('owl_carousel');
						$out .= "<div class='item'>";
						$carousel_item_closed = false;
					}
					$tweet_text = isset( $tweet['text'] ) ? $tweet['text'] : '';
					$tweet_entitties = isset( $tweet['entities'] ) ? $tweet['entities'] : array();
					$tweet_urls = isset( $tweet_entitties['urls'] ) && is_array( $tweet_entitties['urls'] ) ? $tweet_entitties['urls'] : array();
					foreach ( $tweet_urls as $tweet_url ) {
						$display_url = isset( $tweet_url['display_url'] ) ? $tweet_url['display_url'] : '';
						$received_url = isset( $tweet_url['url'] ) ? $tweet_url['url'] : '';
						$html_url = "<a href='".esc_url($received_url)."'>".esc_html($display_url)."</a>";
						$tweet_text = substr_replace( $tweet_text, $html_url, strpos( $tweet_text, $received_url ), strlen( $received_url ) );
					}
					$item_content = '';
					$item_content .= !empty( $tweet_text ) ? "<div class='tweet_content'>$tweet_text</div>" : '';
					if ( $showdate ) {
						$tweet_date = isset( $tweet['created_at'] ) ? $tweet['created_at'] : '';
						$tweet_date_formatted = cws_time_elapsed_string( date( "U", strtotime( $tweet_date ) ) );
						$item_content .= "<div class='tweet_date'>$tweet_date_formatted</div>";
					}
					$out .= !empty( $item_content ) ? "<div class='cws_tweet'>$item_content</div>" : '';
					$temp1 = ( $i + 1 ) / (int)$visible;
					if ( $use_carousel && ( $temp1 - floor( $temp1 ) == 0 || $i == count( $tweets ) - 1 ) ) {
						$out .= "</div>";
						$carousel_item_closed = true;
					}
				}
			$out .= "</div>";
		$out .= "</div>";
	}
	return $out;
}

function cws_time_elapsed_string($ptime){
    $etime = time() - $ptime;

    if ($etime < 1)
    {
        return '0 seconds';
    }

    $a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
    $a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    foreach ($a as $secs => $str)
    {
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

function cws_getTweets( $count = 20 ) {
	$res = null;
	global $cws_theme_funcs;

	if ( '0' != $cws_theme_funcs->cws_get_option( 'turn-twitter' ) ) {
		$twitt_name = trim($cws_theme_funcs->cws_get_option( 'tw-username' )) ? trim($cws_theme_funcs->cws_get_option( 'tw-username' )) : 'Creative_WS';
		if (function_exists('getTweets')) {
			$res = getTweets($twitt_name, $count);
		}
	}

	return $res;
}

if ( ! isset( $content_width ) ) $content_width = 1170;

	/* Full width blog */

	function cws_portfolio_loader(){
		ob_start();
		?>
			<div class='portfolio_loader_wraper'>
				<div class='portfolio_loader_container'>
					<svg width='104px' height='104px' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="uil-default"><rect x="0" y="0" width="100" height="100" fill="none" class="bk"></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(0 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(30 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.083s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(60 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.1667s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(90 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.25s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(120 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.33s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(150 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.4166s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(180 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(210 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5833s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(240 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.67s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(270 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.75s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(300 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.83s' repeatCount='indefinite'/></rect><rect x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='#000000' transform='rotate(330 50 50) translate(0 -30)'><animate attributeName='opacity' from='1' to='0' dur='1s' begin='0.9167s' repeatCount='indefinite'/></rect></svg>
				</div>
			</div>
		<?php
		echo ob_get_clean();
	}

/****************** WALKER CUSTOM MENU *********************/
class Walker_Nav_Menu_Edit_Custom extends Walker_Nav_Menu  {
	/**
	 * @see Walker_Nav_Menu::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
	}

	/**
	 * @see Walker_Nav_Menu::end_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference.
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
	}

	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param object $args
	 */
	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		global $_wp_nav_menu_max_depth;

		$_wp_nav_menu_max_depth = $depth > $_wp_nav_menu_max_depth ? $depth : $_wp_nav_menu_max_depth;

		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		ob_start();
		$item_id = esc_attr( $item->ID );
		$removed_args = array(
			'action',
			'customlink-tab',
			'edit-menu-item',
			'menu-item',
			'page-tab',
			'_wpnonce',
		);

		$original_title = '';
		if ( 'taxonomy' == $item->type ) {
			$original_title = get_term_field( 'name', $item->object_id, $item->object, 'raw' );
			if ( is_wp_error( $original_title ) )
				$original_title = false;
		} elseif ( 'post_type' == $item->type ) {
			$original_object = get_post( $item->object_id );
			$original_title = $original_object->post_title;
		}

		$classes = array(
			'menu-item menu-item-depth-' . $depth,
			'menu-item-' . esc_attr( $item->object ),
			'menu-item-edit-' . ( ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? 'active' : 'inactive'),
		);

		$title = $item->title;

		if ( ! empty( $item->_invalid ) ) {
			$classes[] = 'menu-item-invalid';
			/* translators: %s: title of menu item which is invalid */
			$title = sprintf( __( '%s (Invalid)', 'cryptop' ), $item->title );
		} elseif ( isset( $item->post_status ) && 'draft' == $item->post_status ) {
			$classes[] = 'pending';
			/* translators: %s: title of menu item in draft status */
			$title = sprintf( __('%s (Pending)', 'cryptop'), $item->title );
		}

		$title = empty( $item->label ) ? $title : $item->label;

		?>
		<li id="menu-item-<?php echo esc_attr($item_id); ?>" class="<?php echo implode(' ', $classes ); ?>">
			<dl class="menu-item-bar">
				<dt class="menu-item-handle">
					<span class="item-title"><?php echo esc_html( $title ); ?></span>
					<span class="item-controls">
						<span class="spinner"></span>
						<span class="item-type"><?php echo esc_html( $item->type_label ); ?></span>
						<span class="item-order hide-if-js">
							<a href="<?php
								echo esc_url(wp_nonce_url(
									add_query_arg(
										array(
											'action' => 'move-up-menu-item',
											'menu-item' => $item_id,
										),
										remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
									),
									'move-menu_item'
								));
							?>" class="item-move-up"><abbr title="<?php esc_attr_e('Move up', 'cryptop'); ?>">&#8593;</abbr></a>
							|
							<a href="<?php
								echo esc_url(wp_nonce_url(
									add_query_arg(
										array(
											'action' => 'move-down-menu-item',
											'menu-item' => $item_id,
										),
										remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
									),
									'move-menu_item'
								));
							?>" class="item-move-down"><abbr title="<?php esc_attr_e('Move down', 'cryptop'); ?>">&#8595;</abbr></a>
						</span>
						<a class="item-edit" id="edit-<?php echo esc_attr($item_id); ?>" title="<?php esc_attr_e('Edit Menu Item', 'cryptop'); ?>" href="<?php
							echo ( isset( $_GET['edit-menu-item'] ) && $item_id == $_GET['edit-menu-item'] ) ? admin_url( 'nav-menus.php' ) : esc_url(add_query_arg( 'edit-menu-item', $item_id, remove_query_arg( $removed_args, admin_url( 'nav-menus.php#menu-item-settings-' . $item_id ) ) ));
						?>"><?php _e( 'Edit Menu Item', 'cryptop' ); ?></a>
					</span>
				</dt>
			</dl>

			<div class="menu-item-settings wp-clearfix" id="menu-item-settings-<?php echo esc_attr($item_id); ?>">
				<?php if( 'custom' == $item->type ) : ?>
					<p class="field-url description description-wide">
						<label for="edit-menu-item-url-<?php echo esc_attr($item_id); ?>">
							<?php _e( 'URL', 'cryptop' ); ?><br />
							<input type="text" id="edit-menu-item-url-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-url" name="menu-item-url[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->url ); ?>" />
						</label>
					</p>
				<?php endif; ?>
				<p class="description description-thin">
					<label for="edit-menu-item-title-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Navigation Label', 'cryptop' ); ?><br />
						<input type="text" id="edit-menu-item-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-title" name="menu-item-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->title ); ?>" />
					</label>
				</p>
				<p class="description description-thin">
					<label for="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Title Attribute', 'cryptop' ); ?><br />
						<input type="text" id="edit-menu-item-attr-title-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-attr-title" name="menu-item-attr-title[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->post_excerpt ); ?>" />
					</label>
				</p>
				<p class="field-link-target description">
					<label for="edit-menu-item-target-<?php echo esc_attr($item_id); ?>">
						<input type="checkbox" id="edit-menu-item-target-<?php echo esc_attr($item_id); ?>" value="_blank" name="menu-item-target[<?php echo esc_attr($item_id); ?>]"<?php checked( $item->target, '_blank' ); ?> />
						<?php _e( 'Open link in a new window/tab', 'cryptop' ); ?>
					</label>
				</p>
				<p class="field-css-classes description description-thin">
					<label for="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'CSS Classes (optional)', 'cryptop' ); ?><br />
						<input type="text" id="edit-menu-item-classes-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-classes" name="menu-item-classes[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( implode(' ', $item->classes ) ); ?>" />
					</label>
				</p>
				<p class="field-xfn description description-thin">
					<label for="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Link Relationship (XFN)', 'cryptop' ); ?><br />
						<input type="text" id="edit-menu-item-xfn-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-xfn" name="menu-item-xfn[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->xfn ); ?>" />
					</label>
				</p>
				<p class="field-description description description-wide">
					<label for="edit-menu-item-description-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Description', 'cryptop' ); ?><br />
						<textarea id="edit-menu-item-description-<?php echo esc_attr($item_id); ?>" class="widefat edit-menu-item-description" rows="3" cols="20" name="menu-item-description[<?php echo esc_attr($item_id); ?>]"><?php echo esc_html( $item->description ); ?></textarea>
						<span class="description"><?php _e('The description will be displayed in the menu if the current theme supports it.', 'cryptop'); ?></span>
					</label>
				</p>

				<?php
				/* New fields insertion starts here */
				?>

				<p class="field-custom description description-thin description-thin-custom">
					<label for="edit-menu-item-align-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Text alignment', 'cryptop' ); ?><br />
						<select class="widefat" id="edit-menu-item-align<?php echo esc_attr($item_id); ?>" data-item-option data-name="align-<?php echo esc_attr($item_id); ?>">
							<option value="left" <?php if($item->align == "left"){echo 'selected="selected"';} ?>>Left</option>
							<option value="center" <?php if($item->align == "center"){echo 'selected="selected"';} ?>>Center</option>
							<option value="right" <?php if($item->align == "right"){echo 'selected="selected"';} ?>>Right</option>
						</select>
					</label>
				</p>

				<?php
				$icon_data_attr = 'icon-'. esc_attr($item_id);

				$icons = cws_get_all_fa_icons();
				$isIcons = !empty($icons);

				$ficons = cws_get_all_flaticon_icons();
				$isFlatIcons = !empty($ficons);

				$output_icons = '<option value=""></option>';
				?>

				<p class="field-custom description description-thin description-thin-custom">
					<label for="edit-menu-item-icon-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Icon', 'cryptop' ); ?><br />
						<select class="widefat icons-select" id="edit-menu-item-icon<?php echo esc_attr($item_id); ?>" data-item-option data-name="<?php echo esc_attr($icon_data_attr); ?>">
							<?php
							if ($isIcons){
								$output_icons .= '<optgroup label="Font Awesome">';
								foreach ($icons as $icon) {
									$selected = ($item->icon === 'fa fa-' . $icon) ? ' selected' : '';
									$output_icons .= '<option value="fa fa-' . esc_attr($icon) . '" '.esc_attr($selected).'>' . esc_attr($icon) . '</option>';
								}
								$output_icons .= '</optgroup>';
							}

							if ($isFlatIcons){
								$output_icons .= '<optgroup label="Flaticon">';
								foreach ($ficons as $icon) {
									$selected = ($item->icon === 'flaticon-' . $icon) ? ' selected' : '';
									$output_icons .= '<option value="flaticon-' . esc_attr($icon) . '" '.esc_attr($selected).'>' . esc_attr($icon) . '</option>';
								}
								$output_icons .= '</optgroup>';
							}

							printf('%s', $output_icons);
							?>
						</select>
						<br/><?php _e( 'Select icon from list', 'cryptop' ); ?>
					</label>
				</p>

				<p class="field-custom description description-thin">
					<?php
					$value = $item->hide;
					if($value != "") $value = "checked";
					?>
					<label for="edit-menu-item-hide-<?php echo esc_attr($item_id); ?>">
						<input type="checkbox" id="edit-menu-item-hide-<?php echo esc_attr($item_id); ?>" class="code edit-menu-item-custom" data-item-option data-name="hide-<?php echo esc_attr($item_id); ?>" value="hide" <?php echo esc_attr($value); ?> />
						<?php _e( "Don't show in menu", 'cryptop' ); ?>
					</label>
				</p>

				<p class="field-custom description description-thin description-thin-custom">
					<?php
					$value = $item->tag;
					if($value != "") $value = "checked";
					?>
					<label for="edit-menu-item-tag-<?php echo esc_attr($item_id); ?>">
						<input type="checkbox" id="edit-menu-item-tag-<?php echo esc_attr($item_id); ?>" class="code edit-menu-item-custom" data-item-option data-name="tag-<?php echo esc_attr($item_id); ?>" value="tag" <?php echo esc_attr($value); ?> />
						<?php _e( "Show tag label", 'cryptop' ); ?>
					</label>
				</p>

				<p class="field-custom description description-wide description-wide-custom">
					<label for="edit-menu-item-tag_text-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Tag text', 'cryptop' ); ?><br />
						<input type="text" id="edit-menu-item-tag_text-<?php echo esc_attr($item_id); ?>" class="widefat code edit-menu-item-tag_text" data-item-option data-name="tag_text-<?php echo esc_attr($item_id); ?>" value="<?php echo esc_attr( $item->tag_text ); ?>" />
					</label>
				</p>

				<p class="field-custom description description-thin description-thin-custom">
					<label for="edit-menu-item-tag_font_color-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Tag color', 'cryptop' ); ?><br />
						<input type="text" data-default-color="<?php echo esc_attr(CRYPTOP_FIRST_COLOR);?>" id="edit-menu-item-tag_font_color-<?php echo esc_attr($item_id); ?>" class="color_picker widefat code edit-menu-item-tag_font_color" data-item-option data-name="tag_font_color-<?php echo esc_attr($item_id); ?>" value="<?php echo esc_attr( $item->tag_font_color ); ?>" />
					</label>
				</p>

				<p class="field-custom description description-thin description-thin-custom">
					<label for="edit-menu-item-tag_bg_color-<?php echo esc_attr($item_id); ?>">
						<?php _e( 'Tag background', 'cryptop' ); ?><br />
						<input type="text" data-default-color="#ffffff" id="edit-menu-item-tag_bg_color-<?php echo esc_attr($item_id); ?>" class="color_picker widefat code edit-menu-item-tag_bg_color" data-item-option data-name="tag_bg_color-<?php echo esc_attr($item_id); ?>" value="<?php echo esc_attr( $item->tag_bg_color ); ?>" />
					</label>
				</p>

				<?php
				/* New fields insertion ends here */
				?>
				<div class="menu-item-actions description-wide submitbox">
					<?php if( 'custom' != $item->type && $original_title !== false ) : ?>
						<p class="link-to-original">
							<?php printf( __('Original: %s', 'cryptop'), '<a href="' . esc_attr( $item->url ) . '">' . esc_html( $original_title ) . '</a>' ); ?>
						</p>
					<?php endif; ?>
					<a class="item-delete submitdelete deletion" id="delete-<?php echo esc_attr($item_id); ?>" href="<?php
					echo esc_url(wp_nonce_url(
						add_query_arg(
							array(
								'action' => 'delete-menu-item',
								'menu-item' => $item_id,
							),
							remove_query_arg($removed_args, admin_url( 'nav-menus.php' ) )
						),
						'delete-menu_item_' . esc_attr($item_id)
					)); ?>"><?php _e('Remove', 'cryptop'); ?></a> <span class="meta-sep"> | </span> <a class="item-cancel submitcancel" id="cancel-<?php echo esc_attr($item_id); ?>" href="<?php echo esc_url( add_query_arg( array('edit-menu-item' => $item_id, 'cancel' => time()), remove_query_arg( $removed_args, admin_url( 'nav-menus.php' ) ) ) );
						?>#menu-item-settings-<?php echo esc_attr($item_id); ?>"><?php _e('Cancel', 'cryptop'); ?></a>
				</div>

				<input class="menu-item-data-db-id" type="hidden" name="menu-item-db-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr($item_id); ?>" />
				<input class="menu-item-data-object-id" type="hidden" name="menu-item-object-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object_id ); ?>" />
				<input class="menu-item-data-object" type="hidden" name="menu-item-object[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->object ); ?>" />
				<input class="menu-item-data-parent-id" type="hidden" name="menu-item-parent-id[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_item_parent ); ?>" />
				<input class="menu-item-data-position" type="hidden" name="menu-item-position[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->menu_order ); ?>" />
				<input class="menu-item-data-type" type="hidden" name="menu-item-type[<?php echo esc_attr($item_id); ?>]" value="<?php echo esc_attr( $item->type ); ?>" />
			</div><!-- .menu-item-settings-->
			<ul class="menu-item-transport"></ul>
		<?php

		$output .= ob_get_clean();

		}
}
/****************** /WALKER CUSTOM MENU *********************/

/****************** WALKER *********************/
class Cryptop_Walker_Nav_Menu extends Walker {
	private $elements;
	private $elements_counter = 0;
	private $cws_theme_funcs;

	function __construct($a) {
		$this->cws_theme_funcs = $a;
	}

	function walk ($items, $depth) {
		$this->elements = $this->get_number_of_root_elements($items);
		return parent::walk($items, $depth);
	}

	/**
	 * @see Walker::$tree_type
	 * @since 3.0.0
	 * @var string
	 */
	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );

	/**
	 * @see Walker::$db_fields
	 * @since 3.0.0
	 * @todo Decouple this.
	 * @var array
	 */
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	/**
	 * @see Walker::start_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\">";
		$output .= "\n";
	}
	/**
	 * @see Walker::end_lvl()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param int $depth Depth of page. Used for padding.
	 */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}
	/**
	 * @see Walker::start_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Menu item data object.
	 * @param int $depth Depth of menu item. Used for padding.
	 * @param int $current_page Menu item ID.
	 * @param object $args
	 */

	function logo_ini( $indent, $item ) {
		$logo_box = $this->cws_theme_funcs->cws_get_meta_option( 'logo_box' );
		extract($logo_box, EXTR_PREFIX_ALL, 'logo_box');

		$logo_cont = '';

		$logo_lr_spacing = $logo_tb_spacing = $main_logo_height = '';

		$logo = isset($this->cws_theme_funcs->cws_get_option( 'logo_box' )[$logo_box_default]) ? $this->cws_theme_funcs->cws_get_option( 'logo_box' )[$logo_box_default] : ''; //Call from ThemeOptions
		$sticky_menu_logo = isset($this->cws_theme_funcs->cws_get_option( 'sticky_menu' )['logo']) ? $this->cws_theme_funcs->cws_get_option( 'sticky_menu' )['logo'] : ''; //Call from ThemeOptions
		$mobile_menu_logo = isset($this->cws_theme_funcs->cws_get_option( 'mobile_menu' )['logo']) ? $this->cws_theme_funcs->cws_get_option( 'mobile_menu' )['logo'] : ''; //Call from ThemeOptions

		$sticky_menu_dimensions_sticky = isset($this->cws_theme_funcs->cws_get_option( 'sticky_menu' )['dimensions_sticky']) ? $this->cws_theme_funcs->cws_get_option( 'sticky_menu' )['dimensions_sticky'] : ''; //Call from ThemeOptions
		$mobile_menu_dimensions_mobile = isset($this->cws_theme_funcs->cws_get_option( 'mobile_menu' )['dimensions_mobile']) ? $this->cws_theme_funcs->cws_get_option( 'mobile_menu' )['dimensions_mobile'] : ''; //Call from ThemeOptions

		if ( $logo_box_position == 'center' && $logo_box_in_menu == '1' && $logo_box_enable ) {
			if ( !empty($logo['src']) ) {
				$bfi_args = $bfi_args_sticky = $bfi_args_mobile = array();
				if ( is_array( $logo_box_dimensions ) ) {
					foreach ( $logo_box_dimensions as $key => $value ) {
						if ( ! empty( $value ) ) {
							$bfi_args[ $key ] = $value;
							$bfi_args['crop'] = false;
						}
					}
				}
				if ( is_array( $sticky_menu_dimensions_sticky ) ) {
					foreach ( $sticky_menu_dimensions_sticky as $key => $value ) {
						if ( ! empty( $value ) ) {
							$bfi_args_sticky[ $key ] = $value;
							$bfi_args_sticky['crop'] = false;
						}
					}
				}
				if ( is_array( $mobile_menu_dimensions_mobile ) ) {
					foreach ( $mobile_menu_dimensions_mobile as $key => $value ) {
						if ( ! empty( $value ) ) {
							$bfi_args_mobile[ $key ] = $value;
							$bfi_args_mobile['crop'] = false;
						}
					}
				}

				$logo_lr_spacing = $logo_tb_spacing = '';
				if ( is_array( $logo_box_spacing ) ) {
					$logo_lr_spacing = $this->cws_theme_funcs->cws_print_css_keys($logo_box_spacing, 'margin-', 'px');
					$logo_tb_spacing = $this->cws_theme_funcs->cws_print_css_keys($logo_box_spacing, 'padding-', 'px');
				}

				$img_mrg = ! empty( $logo_lr_spacing ) ? "style='".esc_attr( $logo_lr_spacing )."'" : '';

				$logo_src = $this->cws_theme_funcs->cws_print_img_html($logo, $bfi_args, $main_logo_height);


				if(!empty($logo['src'])){
					$logo_default = '<img '. $logo_src .' '. $img_mrg .' class="logo_desktop" />';
				}

				if ( isset($sticky_menu_logo) && !empty( $sticky_menu_logo['src'] ) ) {
					$logo_sticky_src = $this->cws_theme_funcs->cws_print_img_html($sticky_menu_logo['id'], (!empty($bfi_args_sticky) ? $bfi_args_sticky : null));
				}

				if (isset($mobile_menu_logo) && !empty($mobile_menu_logo['src'])) {
					$logo_mobile_src = $this->cws_theme_funcs->cws_print_img_html($mobile_menu_logo['id'], (!empty($bfi_args_mobile) ? $bfi_args_mobile : null));
				}

				$rety = home_url();

				$logo_cont = '
				</ul></div>
					<div class="menu-center-part menu_logo_part">
						<a class="logo" href="'.esc_url($rety).'">';

						if(!empty($logo['src'])){
							$file_parts = pathinfo($logo['src']);

							$logo_cont .= "<div class='logo_default_wrapper logo_wrapper'>";
							if( $file_parts['extension'] == 'svg' ){
								$logo_cont .= $this->cws_theme_funcs->cws_print_svg_html($logo, $bfi_args, $main_logo_height);
							} else {
								$logo_cont .= $logo_default;
							}
							$logo_cont .= "</div>";
						}

						if( !empty($logo_sticky_src) ){
							$file_parts_sticky = pathinfo($logo['src']);

							$logo_cont .= "<div class='logo_sticky_wrapper logo_wrapper'>";
							if( $file_parts_sticky['extension'] != 'svg' ){
								$logo_cont .= ($logo_sticky_src ?  '<img '.$logo_sticky_src." class='logo_sticky' />" : '');
							} else {
								$logo_cont .= $this->cws_theme_funcs->cws_print_svg_html($sticky_menu_logo, $bfi_args);
							}
							$logo_cont .= "</div>";
						}

						$logo_cont .= '
						</a>
					</div>
				<div class="menu-right-part"><ul class="main-menu">';
			} else {
				$logo_cont = '
				</ul></div>
					<div class="menu-center-part menu_logo_part">
						<h1 class="header_site_title">'.esc_html(get_bloginfo( 'name' )).'</h1>
					</div>
				<div class="menu-right-part"><ul class="main-menu">';
			}
		}
		return $logo_cont;
	}

	function site_name_ini( $indent, $item ) {
		$logo_box_position = $this->cws_theme_funcs->cws_get_meta_option( 'logo_box' )['position'];
		if ( $indent == 0 && $logo_box_position == 'center' ) {
			ob_start();
		?>
				</ul>
			</div>
			<div class="menu-center-part menu_logo_part site_name" <?php echo isset( $logo_box_position ) && !empty( $logo_box_position ) && $logo_box_position == 'center' && ! empty( $logo_tb_spacing ) ? " style='".esc_attr($logo_tb_spacing)."'" : ''; ?>>
				<a <?php echo ( ! empty( $logo_lr_spacing ) ? " style='".esc_attr($logo_lr_spacing)."'" : '') ?> class="logo" href="<?php echo esc_url( home_url() ); ?>" >
					<h1 class='header_site_title'><?php echo get_bloginfo( 'name' ); ?></h1>
				</a>
			</div>
			<div class="menu-right-part">
				<ul class="main-menu">
		<?php
			$site_name_cont = ob_get_clean();
		} else {
			$site_name_cont = '';
		}

		return $site_name_cont;
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . sanitize_html_class( $item->ID );

		//Custom menu fields
		if ($item->align != 'left' && isset($item->align)){
			array_push($classes,'link_align_'.$item->align);
		}

		if ($item->hide == 'hide'){
			array_push($classes,'hide_link');
		}
		//Custom menu fields

		if ($item->menu_item_parent=="0") {
			$this->elements_counter += 1;
			if ($this->elements_counter>$this->elements/2){
				array_push($classes,'sub_align_right');
			}
		}

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . $class_names  . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. sanitize_html_class( $item->ID ), $item, $args );
		$id = $id ? ' id="' . $id . '"' : '';

		// logo in cont init;
		if ( $item->menu_item_parent == '0' && $this->elements_counter == floor(($this->elements / 2)+1) ) {
			$logo_container = $this->logo_ini( $indent, $item );
		} else {
			$logo_container = '';
		}

		// Site name in cont init;
		if (isset($this->cws_theme_funcs->cws_get_meta_option('logo_box')['site_name_in_menu']) && $this->cws_theme_funcs->cws_get_meta_option('logo_box')['site_name_in_menu'] == '1'){
			if ( $item->menu_item_parent == '0' && $this->elements_counter == floor(($this->elements / 2)+1) ) {
				$site_name_container = $this->site_name_ini( $indent, $item );
			} else {
				$site_name_container = '';
			}
		}

		$output .= $indent . (!empty($search_and_woo_icon_start) ? $search_and_woo_icon_start : '' ) . $logo_container .(!empty($site_name_container) ? $site_name_container : ''). '<li' . $id . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )	 ? $item->target	 : '';
		$atts['rel']	= ! empty( $item->xfn )		? $item->xfn		: '';
		$atts['href']   = ! empty( $item->url )		? $item->url		: '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}
		$item_output = !empty($args->before) ? $args->before : '';
		$item_output .= '<a'. $attributes .'>';

		$item_output .= ( !empty($args->link_before) ? $args->link_before : "" ) .
			apply_filters( 'nav_menu_item_title', $item->title, $item, $args, $depth ) .
			(is_rtl() ? '&#x200E;' : '') . ( !empty($args->link_after ) ? $args->link_after : "" );

		$item_output .= '</a>';

		if (is_array($item->classes)){
			if ( in_array( 'menu-item-has-children', $item->classes ) ){
				$item_output .= "<i class='button_open'></i>";
			}
		}

		$item_output .= ( !empty($args->after) ? $args->after : '' );

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	/**
	 * @see Walker::end_el()
	 * @since 3.0.0
	 *
	 * @param string $output Passed by reference. Used to append additional content.
	 * @param object $item Page data object. Not used.
	 * @param int $depth Depth of page. Not Used.
	 */


	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n".(!empty($search_and_woo_icon_end) ? $search_and_woo_icon_end : '');
	}
}
/****************** /WALKER *********************/

/****************** TOPBAR WALKER *********************/
class Cryptop_Walker_Nav_Topbar_Menu extends Walker {
	private $elements;
	private $elements_counter = 0;
	private $cws_theme_funcs;

	function __construct($a) {
		$this->cws_theme_funcs = $a;
	}

	function walk ($items, $depth) {
		$this->elements = $this->get_number_of_root_elements($items);
		return parent::walk($items, $depth);
	}

	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\">";
		$output .= "\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . sanitize_html_class( $item->ID );

		if ($item->menu_item_parent=="0") {
			$this->elements_counter += 1;
			if ($this->elements_counter>$this->elements/2){
				array_push($classes,'right');
			}
		}

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . $class_names  . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'top-bar-menu-item-'. sanitize_html_class( $item->ID ), $item, $args );
		$id = $id ? ' id="' . $id . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )	 ? $item->target	 : '';
		$atts['rel']	= ! empty( $item->xfn )		? $item->xfn		: '';
		$atts['href']   = ! empty( $item->url )		? $item->url		: '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = !empty($args->before) ? $args->before : '';
		$item_output .= '<a'. $attributes .'>';

		$item_output .= ( !empty($args->link_before) ? $args->link_before : '' ) . apply_filters( 'the_title', $item->title, $item->ID ) . (is_rtl() ? '&#x200E;' : '') . ( !empty($args->link_after ) ? $args->link_after : '' );
		$item_output .= '</a>';

		$item_output .= ( !empty($args->after) ? $args->after : '' );

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
}

/****************** /TOPBAR WALKER *********************/

/****************** COPYRIGHTS WALKER *********************/
class Cryptop_Walker_Nav_Copyright_Menu extends Walker {
	private $elements;
	private $elements_counter = 0;
	private $cws_theme_funcs;

	function __construct($a) {
		$this->cws_theme_funcs = $a;
	}

	function walk ($items, $depth) {
		$this->elements = $this->get_number_of_root_elements($items);
		return parent::walk($items, $depth);
	}

	var $tree_type = array( 'post_type', 'taxonomy', 'custom' );
	var $db_fields = array( 'parent' => 'menu_item_parent', 'id' => 'db_id' );

	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "\n$indent<ul class=\"sub-menu\">";
		$output .= "\n";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$indent = str_repeat("\t", $depth);
		$output .= "$indent</ul>\n";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

		$class_names = $value = '';

		$classes = empty( $item->classes ) ? array() : (array) $item->classes;
		$classes[] = 'menu-item-' . sanitize_html_class( $item->ID );

		if ($item->menu_item_parent=="0") {
			$this->elements_counter += 1;
			if ($this->elements_counter>$this->elements/2){
				array_push($classes,'right');
			}
		}

		$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
		$class_names = $class_names ? ' class="' . esc_attr($class_names)  . '"' : '';

		$id = apply_filters( 'nav_menu_item_id', 'top-bar-menu-item-'. sanitize_html_class( $item->ID ), $item, $args );
		$id = $id ? ' id="' . $id . '"' : '';

		$output .= $indent . '<li' . $id . $value . $class_names .'>';

		$atts = array();
		$atts['title']  = ! empty( $item->attr_title ) ? $item->attr_title : '';
		$atts['target'] = ! empty( $item->target )	 ? $item->target	 : '';
		$atts['rel']	= ! empty( $item->xfn )		? $item->xfn		: '';
		$atts['href']   = ! empty( $item->url )		? $item->url		: '';

		$atts = apply_filters( 'nav_menu_link_attributes', $atts, $item, $args );

		$attributes = '';
		foreach ( $atts as $attr => $value ) {
			if ( ! empty( $value ) ) {
				$value = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
				$attributes .= ' ' . $attr . '="' . $value . '"';
			}
		}

		$item_output = !empty($args->before) ? $args->before : '';
		$item_output .= '<a'. $attributes .'>';

		$item_output .= ( !empty($args->link_before) ? $args->link_before : '' ) . apply_filters( 'the_title', $item->title, $item->ID ) . (is_rtl() ? '&#x200E;' : '') . ( !empty($args->link_after ) ? $args->link_after : '' );
		$item_output .= '</a>';

		$item_output .= ( !empty($args->after) ? $args->after : '' );

		$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}

	function end_el( &$output, $item, $depth = 0, $args = array() ) {
		$output .= "</li>\n";
	}
}

/****************** /COPYRIGHTS WALKER *********************/

/* Comments */
class CRYPTOP_Walker_Comment extends Walker_Comment {
	// init classwide variables
	var $tree_type = 'comment';
	var $db_fields = array( 'parent' => 'comment_parent', 'id' => 'comment_ID' );
	function __construct() { ?>
		<div class="comment_list">
	<?php }

	/** START_LVL
	 * Starts the list before the CHILD elements are added. Unlike most of the walkers,
	 * the start_lvl function means the start of a nested comment. It applies to the first
	 * new level under the comments that are not replies. Also, it appear that, by default,
	 * WordPress just echos the walk instead of passing it to &$output properly. Go figure.  */
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>
		<div class="comments_children">
	<?php }

	/** END_LVL
	 * Ends the children list of after the elements are added. */
	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$GLOBALS['comment_depth'] = $depth + 1; ?>
		</div><!-- /.children -->

	<?php }

	/** START_EL */
	function start_el( &$output, $comment, $depth = 0, $args = array(), $id = 0 ) {
		$depth++;
		global $cws_theme_funcs;
		$GLOBALS['comment_depth'] = $depth;
		$GLOBALS['comment'] = $comment;
		$parent_class = ( empty( $args['has_children'] ) ? '' : 'parent' );
		$old_version = 0;
		?>

		<div <?php comment_class( $parent_class ); ?> id="comment-<?php comment_ID() ?>">
			<div id="comment-body-<?php comment_ID() ?>" class="comment-body clearfix">
				<div class="comment_info_section">
					<div class="comment_info">
				<div class="avatar_section">
					<?php echo ( $args['avatar_size'] != 0 ? get_avatar( $comment, $args['avatar_size'] ) :'' ); ?>
				</div>

							<?php $reply_args = array(
							'reply_text' => "<i class='reply_icon flaticon-arrows-5'></i>" . esc_html__( 'Reply', 'cryptop' ),
								'depth' => $depth,
								'max_depth' => $args['max_depth']
						); ?>

						<div class="comment-meta comment-meta-data">
							<div class="comment_top_info">
							<cite class="fn n author-name"><?php echo get_comment_author_link(); ?></cite>
								<?php
									echo "<span class='date'>";
										comment_date();
									echo "</span>";
									esc_html_e( ' at', 'cryptop' );
									echo " <span class='time'>";
										comment_time();
									echo "</span>";
									?>
					</div>
						</div><!-- /.comment-meta -->

					<div id="comment-content-<?php comment_ID(); ?>" class="comment-content">
						<?php
						if( !$comment->comment_approved )
						{ ?>
						    <em class="comment-awaiting-moderation"><?php esc_html_e('Your comment is awaiting moderation.', 'cryptop'); ?></em>
						<?php
						} else {
						    if ($comment->comment_type != 'pingback')
						    {
						        comment_text();
						    }
						} ?>
							<div class="comments_buttons">
								<?php
									echo "<div class='button-content reply'>";
									comment_reply_link( array_merge( $args, $reply_args ) );
									echo '</div>';

									edit_comment_link( '(Edit)' );
								?>
							</div>
					</div><!-- /.comment-content -->

					</div>

				</div>
			</div><!-- /.comment-body -->

	<?php }

	function end_el(&$output, $comment, $depth = 0, $args = array() ) { ?>

		</div><!-- /#comment-' . get_comment_ID() . ' -->

	<?php }

	/** DESTRUCTOR
	 * I just using this since we needed to use the constructor to reach the top
	 * of the comments list, just seems to balance out :) */
	function __destruct() { ?>

	</div><!-- /#comment-list -->

	<?php }
}
/* \Comments */

?>