<?php
defined('CRYPTOP_FIRST_COLOR') or define('CRYPTOP_FIRST_COLOR', '#00e7b4');
defined('CRYPTOP_FOOTER_COLOR') or define('CRYPTOP_FOOTER_COLOR', '#fafafa');
defined('CRYPTOP_SECONDARY_COLOR') or define('CRYPTOP_SECONDARY_COLOR', '#ff4a81');
defined('CRYPTOP_HELPER_COLOR') or define('CRYPTOP_HELPER_COLOR', '#a9a9a9');

if (!is_customize_preview()) {
	new Cryptop_Metaboxes();
}

class Cryptop_Metaboxes {
	public $mb_page_layout = array();
	public $mb_staff_layout = array();
	public $mb_portfolio_layout = array();
	public $mb_classes_layout = array();

	public static $instance;

	public function __construct($a = null) {
		$this->mb_page_layout = array(
			'tab0' => array(
				'type' => 'tab',
				'init' => 'open',
				'title' => esc_html__( 'General', 'cws-to' ),
				'layout' => array(
					'MB_general' => array(
						'type' => 'checkbox',
						'title' => esc_html__('Customize', 'cws-to' ),
						'atts' => 'data-options="e:sb_layout;e:is_blog;e:page_sidebars;e:page_spacing;e:slider_override"',
						'addrowclasses' => 'checkbox alt box',
					),
					'page_sidebars' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups',
						'layout' => array(
							'layout' => array(
								'title' => esc_html__('Sidebar Position', 'cws-to' ),
								'type' => 'radio',
								'addrowclasses' => 'grid-col-12',
								'subtype' => 'images',
								'value' => array(
									'{page_sidebars}'=>	array( esc_html__('Default', 'cws-to' ), true, 'd:def--sidebar1;d:def--sidebar2', '/img/default.png' ),
									'left' => array( esc_html__('Left', 'cws-to' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
									'right' => array( esc_html__('Right', 'cws-to' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
									'both' => array( esc_html__('Double', 'cws-to' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
									'none' => array( esc_html__('None', 'cws-to' ), false, 'd:sb1;d:sb2', '/img/none.png' )
								),
							),
							'sb1' => array(
								'title' => esc_html__('Select a sidebar', 'cws-to' ),
								'type' => 'select',
								'addrowclasses' => 'disable box grid-col-6',
								'source' => 'sidebars',
							),
							'sb2' => array(
								'title' => esc_html__('Select right sidebar', 'cws-to' ),
								'type' => 'select',
								'addrowclasses' => 'disable box grid-col-6',
								'source' => 'sidebars',
							),
						),
					),
					'is_blog' => array(
						'type' => 'checkbox',
						'title' => esc_html__('Add Blog posts', 'cws-to' ),
						'atts' => 'data-options="e:blogtype;e:category"',
						'addrowclasses' => 'disable checkbox grid-col-12',
					),
					'blogtype' => array(
						'type' => 'radio',
						'subtype' => 'images',
						'title' => esc_html__('Blog Layout', 'cws-to' ),
						'addrowclasses' => 'disable grid-col-12',
						'value' => array(
							'default'=>	array( esc_html__('Default', 'cws-to' ), false, '', '/img/default.png' ),
							'large' => array( esc_html__('Large', 'cws-to' ), false, '', '/img/large.png' ),
							'medium' => array( esc_html__('Medium', 'cws-to' ), true, '', '/img/medium.png' ),
							'small' => array( esc_html__('Small', 'cws-to' ), false, '', '/img/small.png' ),
							'2' => array(  esc_html__('Two', 'cws-to' ), false, '', '/img/pinterest_2_columns.png'),
							'3' => array( esc_html__('Three', 'cws-to' ), false, '', '/img/pinterest_3_columns.png'),
							'4' => array( esc_html__('Four', 'cws-to' ), false, '', '/img/pinterest_4_columns.png'),
						),
					),
					'category' => array(
						'title' => esc_html__('Category', 'cws-to' ),
						'type' => 'taxonomy',
						'addrowclasses' => 'disable grid-col-12',
						'atts' => 'multiple',
						'taxonomy' => 'category',
						'source' => array(),
					),
					'page_spacing' => array(
						'title' => esc_html__( 'Page Spacings', 'cws-to' ),
						'type' => 'margins',
						'addrowclasses' => 'disable grid-col-12 two-inputs',
						'value' => array(
							'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '70'),
							'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '70'),
						),
					),
					'slider_override' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups',
						'layout' => array(
							'is_override' => array(
								'type' => 'checkbox',
								'title' => esc_html__( 'Add Image Slider', 'cws-to' ),
								'atts' => 'data-options="e:slider_shortcode;e:is_wide;"',
								'addrowclasses' => 'checkbox grid-col-12',
							),
							'slider_shortcode' => array(
								'addrowclasses' => 'disable box grid-col-12',
								'type' => 'text',
								'default' => ''
							),
							'is_wide' => array( // wide_slider
								'type' => 'checkbox',
								'title' => esc_html__( 'Full-Width Slider', 'cws-to' ),
								'atts' => 'checked',
								'addrowclasses' => 'disable checkbox box grid-col-12',
							),
						),
					),
				),
			),
			'tab1' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Styling', 'cws-to' ),
				'layout' => array(
					'MB_styling' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'type' => 'checkbox',
						'atts' => 'data-options="e:theme_colors_first_color;e:theme_colors_second_color;e:theme_colors_third_color;e:theme-fourth-color;e:theme-fifth-color;"',
					),				
					'theme_colors_first_color' => array(
						'title' => esc_html__( 'Main color', 'cws-to' ),
						'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
						'value' => CRYPTOP_FIRST_COLOR,
						'addrowclasses' => 'grid-col-2_5',
						'type' => 'text',
					),
					'theme_colors_second_color' => array(
						'title' => esc_html__( 'Second color', 'cws-to' ),
						'atts' => 'data-default-color="'.CRYPTOP_SECONDARY_COLOR.'"',
						'value' => CRYPTOP_SECONDARY_COLOR,
						'addrowclasses' => 'grid-col-2_5',
						'type' => 'text',
					),
					'theme_colors_third_color' => array(
						'title' => esc_html__( 'Third color', 'cws-to' ),
						'atts' => 'data-default-color="'.CRYPTOP_HELPER_COLOR.'"',
						'value' => CRYPTOP_HELPER_COLOR,
						'addrowclasses' => 'grid-col-2_5',
						'type' => 'text',
					),
				),
			),
			'tab2' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Header', 'cws-to' ),
				'layout' => array(
					'MB_header' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'type' => 'checkbox',
						'atts' => 'data-options="e:header;"',
					),

					'header' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'order' => array(
								'type' => 'group',
								'addrowclasses' => 'group sortable drop grid-col-12',
								'tooltip' => array(
									'title' => esc_html__( 'Header order', 'cws-to' ),
									'content' => esc_html__( 'Drag to reorder and customize your header.', 'cws-to' ),
								),
								'title' => esc_html__('Header order', 'cws-to' ),
								'value' => array(
									array('title' => 'Top Bar','val' => 'top_bar_box'),
									array('title' => 'Header Zone','val' => 'drop_zone_start'),
									array('title' => 'Logo','val' => 'logo_box'),
									array('title' => 'Menu','val' => 'menu_box'),
									array('title' => 'Header Zone','val' => 'drop_zone_end'),
									array('title' => 'Title area','val' => 'title_box'),
								),
								'layout' => array(
									'title' => array(
										'type' => 'text',
										'value' => '',
										'atts' => 'data-role="title"',
										'title' => esc_html__('Sidebar', 'cws-to' ),
									),
									'val' => array(
										'type' => 'text',
									)

								)
							),
							'customize' => array(
								'title' => esc_html__( 'Add an Image/Color or Spacings', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'data-options="e:background_image;e:overlay;e:spacings;e:override_topbar_color;e:override_menu_color;e:override_menu_color_hover;e:override_topbar_color_hover;"',
							),
							'background_image' => array(
								'type' => 'fields',
								'addrowclasses' => 'box grid-col-12 inside-box groups',
								'layout' => '%image_layout%',
							),
							'overlay' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 disable box inside-box groups',
								'layout' => array(
									'type'	=> array(
										'title'		=> esc_html__( 'Add Color overlay', 'cws-to' ),
										'addrowclasses' => 'grid-col-4',
										'type'	=> 'select',
										'source'	=> array(
											'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:opacity;d:color;d:gradient;' ),
											'color' => array( esc_html__( 'Color', 'cws-to' ),  false, 'e:opacity;e:color;d:gradient;' ),
											'gradient' => array( esc_html__('Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient;' )
										),
									),
									'color'	=> array(
										'title'	=> esc_html__( 'Overlay color', 'cws-to' ),
										'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
										'addrowclasses' => 'grid-col-4',
										'value' => CRYPTOP_FIRST_COLOR,
										'type'	=> 'text',
									),
									'opacity' => array(
										'type' => 'number',
										'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
										'placeholder' => esc_html__( 'In percents', 'cws-to' ),
										'value' => '40',
										'addrowclasses' => 'grid-col-4',
									),
									'gradient' => array(
										'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
										'type' => 'fields',
										'addrowclasses' => 'grid-col-12 disable box inside-box groups',
										'layout' => '%gradient_layout%',
									),
								),
							),
							'override_menu_color'	=> array(
								'title'	=> esc_html__( 'Override Menu\'s Font Color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'addrowclasses' => 'grid-col-12 disable',
								'value' => '#ffffff',
								'type'	=> 'text',
							),
							'override_menu_color_hover'	=> array(
								'title'	=> esc_html__( 'Override Menu\'s Font Color on Hover', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'addrowclasses' => 'grid-col-6 disable',
								'value' => '#ffffff',
								'type'	=> 'text',
							),
							'override_topbar_color'	=> array(
								'title'	=> esc_html__( 'Override TopBar\'s Font Color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'addrowclasses' => 'grid-col-12 disable',
								'value' => '#ffffff',
								'type'	=> 'text',
							),
							'override_topbar_color_hover' => array(
								'title'	=> esc_html__( 'Override TopBar\'s Font Color on Hover', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'addrowclasses' => 'grid-col-6 disable',
								'value' => '#ffffff',
								'type'	=> 'text',
							),
							'outside_slider' => array(
								'title' => esc_html__( 'Header overlays slider', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-3',
								'type' => 'checkbox',
							),
							'spacings' => array(
								'title' => esc_html__( 'Add Spacings', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'disable grid-col-4 two-inputs',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => ''),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => ''),
								),
							),						
						),
					),
				),
			),
			'tab3' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Logo', 'cws-to' ),
				'layout' => array(
					'MB_logo' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'atts' => 'data-options="e:logo_box;"',
						'type' => 'checkbox',
					),

					'logo_box' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'title' => esc_html__( 'Logo', 'cws-to' ),
								'addrowclasses' => 'checkbox alt grid-col-12',
								'type' => 'checkbox',
								'atts' => 'checked',
							),													
							'default'	=> array(
								'title'		=> esc_html__( 'Display Logo Variation', 'cws-to' ),
								'addrowclasses' => 'grid-col-12',
								'type'	=> 'select',
								'source'	=> array(
									'dark' => array( esc_html__( 'Dark', 'cws-to' ),  true, '' ),
									'light' => array( esc_html__( 'Light', 'cws-to' ),  false, '' ),
								),
							),
							'dimensions' => array(
								'title' => esc_html__( 'Logo Dimensions', 'cws-to' ),
								'type' => 'dimensions',
								'addrowclasses' => 'grid-col-12',
								'value' => array(
									'width' => array('placeholder' => esc_html__( 'Width', 'cws-to' ), 'value' => ''),
									'height' => array('placeholder' => esc_html__( 'Height', 'cws-to' ), 'value' => ''),
									),
							),
							'spacing' => array(
								'title' => esc_html__( 'Margins (px)', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'grid-col-12',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '12'),
									'left' => array('placeholder' => esc_html__( 'left', 'cws-to' ), 'value' => '0'),
									'right' => array('placeholder' => esc_html__( 'Right', 'cws-to' ), 'value' => '0'),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '12'),
								),
							),							
							'position' => array(
								'title' => esc_html__( 'Position', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-12',
								'value' => array(
									'left' => array( esc_html__('Left', 'cws-to'), true, 'd:site_name_in_menu;e:with_site_name;', '/img/align-left.png' ),
									'center' =>array( esc_html__('Center', 'cws-to'), false, 'e:site_name_in_menu;e:with_site_name;', '/img/align-center.png', ),
									'right' =>array( esc_html__('Right', 'cws-to'), false, 'd:site_name_in_menu;e:with_site_name;', '/img/align-right.png', ),
								),
							),								
							'in_menu' => array(
								'title' => esc_html__( 'Logo in menu box', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'checked data-options="d:wide;d:overlay;d:border;"',
							),		
							'wide' => array(
								'title' => esc_html__( 'Apply Full-Width Container', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
							),													
							'overlay' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => array(
									'type'	=> array(
										'title'		=> esc_html__( 'Add Background Color Overlay to the Logo Area', 'cws-to' ),
										'addrowclasses' => 'grid-col-4',
										'type'	=> 'select',
										'source'	=> array(
											'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:opacity;d:color;d:gradient;' ),
											'color' => array( esc_html__( 'Color', 'cws-to' ),  false, 'e:opacity;e:color;d:gradient;' ),
											'gradient' => array( esc_html__('Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient;' )
										),
									),
									'color'	=> array(
										'title'	=> esc_html__( 'Overlay color', 'cws-to' ),
										'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
										'addrowclasses' => 'grid-col-4',
										'value' => CRYPTOP_FIRST_COLOR,
										'type'	=> 'text',
									),
									'opacity' => array(
										'type' => 'number',
										'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
										'placeholder' => esc_html__( 'In percents', 'cws-to' ),
										'value' => '40',
										'addrowclasses' => 'grid-col-4',
									),
									'gradient' => array(
										'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
										'type' => 'fields',
										'addrowclasses' => 'grid-col-12 disable box inside-box groups',
										'layout' => '%gradient_layout%',
									),
								),
							),
							'border' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => '%border_layout%',
							),
						),
					),

				),
			),
			'tab4' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Menu', 'cws-to' ),
				'layout' => array(
					'MB_menu' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'type' => 'checkbox',
						'atts' => 'data-options="e:menu_box;"',
					),

					'menu_box' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'title' => esc_html__( 'Menu', 'cws-to' ),
								'addrowclasses' => 'checkbox alt grid-col-12',
								'type' => 'checkbox',
								'atts' => 'checked',
							),								
							'position' => array(
								'title' => esc_html__( 'Menu Alignment', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'new_row grid-col-3',
								'value' => array(
									'left' => array( esc_html__( 'Left', 'cws-to' ), 	false, '', '/img/align-left.png' ),
									'center' =>array( esc_html__( 'Center', 'cws-to' ), false, '', '/img/align-center.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), true, '', '/img/align-right.png' ),
								),
							),
							'search_place' => array(
								'title' => esc_html__( 'Search Icon Location', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-3',
								'value' => array(
									'none' => array( esc_html__( 'None', 'cws-to' ), 	false, '', '/img/no_layout.png' ),
									'top' => array( esc_html__( 'Top', 'cws-to' ), 	false, '', '/img/search-social-right.png' ),
									'left' =>array( esc_html__( 'Left', 'cws-to' ), false, '', '/img/search-menu-left.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), true, '', '/img/search-menu-right.png' ),
								),
							),
							'social_place' => array(
								'title' => esc_html__( 'Social Links', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-3',
								'value' => array(
									'left' =>array( esc_html__( 'Left', 'cws-to' ), true, '', '/img/social-left.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false, '', '/img/social-right.png' ),
								),
							),								
							'mobile_place' => array(
								'title' => esc_html__( 'Mobile Menu Location', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-3',
								'value' => array(
									'left' =>array( esc_html__( 'Left', 'cws-to' ), true, '', '/img/hamb-left.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false, '', '/img/hamb-right.png' ),
								),
							),
							'background_color' => array(
								'title' => esc_html__( 'Background color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'value' => '#ffffff',
								'addrowclasses' => 'grid-col-3',
								'type' => 'text',
							),
							'background_opacity' => array(
								'type' => 'number',
								'title' => esc_html__( 'Opacity', 'cws-to' ),
								'placeholder' => esc_html__( 'In percents', 'cws-to' ),
								'addrowclasses' => 'grid-col-3',
								'value' => '100'
							),
							'font_color' => array(
								'type' => 'text',
								'title' => esc_html__( 'Override Font color', 'cws-to' ),
								'atts' => 'data-default-color="#000000"',
								'value' => '#000000',
								'addrowclasses' => 'grid-col-3',
							),
							'font_color_hover' => array(
								'type' => 'text',
								'title' => esc_html__( 'Override Font hover color', 'cws-to' ),
								'atts' => 'data-default-color="'.CRYPTOP_SECONDARY_COLOR.'"',
								'value' => CRYPTOP_SECONDARY_COLOR,
								'addrowclasses' => 'grid-col-3',
							),
							'border' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => '%border_layout%',
							),
							'margin' => array(
								'title' => esc_html__( 'Add Spacings', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'grid-col-12 two-inputs',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '12'),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '12'),
								),
							),
							'sandwich' => array(
								'title' => esc_html__( 'Use mobile menu on desktop PCs', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-3',
								'type' => 'checkbox',
							),
							'wide' => array(
								'title' => esc_html__( 'Apply Full-Width Menu', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-3',
								'type' => 'checkbox',
								'atts' => 'checked',
							),
							'override_menu' => array(
								'type' => 'checkbox',
								'title' => esc_html__( 'Use Custom Menu on this Page', 'cws-to' ),
								'atts' => 'data-options="e:custom_menu;"',
								'addrowclasses' => 'checkbox grid-col-3',
							),	
							'custom_menu' => array(
								'title' => esc_html__('Select a menu', 'cws-to' ),
								'addrowclasses' => 'disable grid-col-12',
								'type' => 'taxonomy',
								'source' => array(),
								'taxonomy' => 'nav_menu',
							),												
						),
					),

				),
			),
			'tab5' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Mobile Menu', 'cws-to' ),
				'layout' => array(
					'MB_mobile_menu' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'type' => 'checkbox',
						'atts' => 'data-options="e:mobile_menu_box;"',
					),

					'mobile_menu_box' => array(
						'type' => 'fields',
						'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
						'layout' => array(

							'theme'	=> array(
								'title'		=> esc_html__( 'Theme', 'cws-to' ),
								'addrowclasses' => 'grid-col-6',
								'type'	=> 'select',
								'source'	=> array(
									'light' => array( esc_html__( 'Light', 'cws-to' ),  true, '' ),
									'dark' => array( esc_html__( 'Dark', 'cws-to' ),  false, '' ),
								),
							),
							'enable_on_tablet' => array(
								'title' => esc_html__( 'Enable Mobile menu on tablets', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-6',
								'atts' => 'checked',
								'type' => 'checkbox',
							),								
							
						),
					),

				),
			),
			'tab6' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Sticky', 'cws-to' ),
				'layout' => array(
					'MB_sticky_menu' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'type' => 'checkbox',
						'atts' => 'data-options="e:sticky_menu;"',
					),

					'sticky_menu' => array(
						'type' => 'fields',
						'addrowclasses' => 'inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'title' => esc_html__( 'Sticky Menu', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12 alt',
								'type' => 'checkbox',
							),
							'mode'	=> array(
								'title'		=> esc_html__( 'Select a Sticky\'s Mode', 'cws-to' ),
								'type'	=> 'select',
								'addrowclasses' => 'grid-col-12',
								'source'	=> array(
									'smart' => array( esc_html__( 'Smart', 'cws-to' ),  true ),
									'simple' => array( esc_html__( 'Simple', 'cws-to' ), false ),
								),
							),							
							'background_color' => array(
								'title' => esc_html__( 'Background color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'value' => '#ffffff',
								'addrowclasses' => 'grid-col-4',
								'type' => 'text',
							),
							'font_color' => array(
								'title' => esc_html__( 'Override Font color', 'cws-to' ),
								'atts' => 'data-default-color="#595959"',
								'value' => '#595959',
								'addrowclasses' => 'grid-col-4',
								'type' => 'text',
							),
							'background_opacity' => array(
								'type' => 'number',
								'title' => esc_html__( 'Opacity', 'cws-to' ),
								'placeholder' => esc_html__( 'In percents', 'cws-to' ),
								'addrowclasses' => 'grid-col-4',
								'value' => '30'
							),
							'border' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => '%border_layout%',
							),
							'shadow' => array(
								'title' => esc_html__( 'Add Shadow', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
							),
						),
					),

				),
			),
			'tab7' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Title', 'cws-to' ),
				'layout' => array(
					'MB_title_box' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'atts' => 'data-options="e:title_box;"',
						'type' => 'checkbox',
					),

					'title_box' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'title' => esc_html__( 'Title Area', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12 alt',
								'atts' => 'checked',
								'type' => 'checkbox',
							),							
							'text_center' => array(
								'title' => esc_html__( 'Center Title & Breadcrumbs', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'checked',
							),
							'no_title' => array(
								'title' => esc_html__( 'Hide Page Title', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
							),							
							'customize' => array(
								'title' => esc_html__( 'Add Background Image/Color and animation', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'checked data-options="e:animate;e:slide_down;e:font_color;e:background_image;e:overlay;e:use_pattern;e:use_blur;e:effect;e:spacings;"',
							),						
							'background_image' => array(
								'type' => 'fields',
								'addrowclasses' => 'disable box grid-col-12 inside-box groups',
								'layout' => '%image_layout%',
							),
							'effect' => array(
								'title' => esc_html__( 'Image style', 'cws-to' ),
								'type' => 'radio',
								'addrowclasses' => 'disable grid-col-6',
								'value' => array(
									'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:parallax_options;d:scroll_parallax;' ),
									'scroll_parallax' => array( esc_html__( 'Scroll Parallax', 'cws-to' ),  false, 'e:scroll_parallax;' ),
									'parallaxify' =>array( esc_html__( 'Parallaxify', 'cws-to' ), false, 'd:scroll_parallax;e:parallax_options;' ),
								),
							),
							'scroll_parallax' => array(
								'title' => esc_html__( 'Add Motion Zoom to Header Image on Page Scroll', 'cws-to' ),
								'addrowclasses' => 'disable checkbox grid-col-12',
								'type' => 'checkbox',
							),
							'parallax_options' => array(
								'type' => 'fields',
								'addrowclasses' => 'disable grid-col-12 box inside-box groups',
								'layout' => '%parallax_layout%',
							),							
							'font_color' => array(
								'title'	=> esc_html__( 'Override Font Color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'value' => '#ffffff',
								'addrowclasses' => 'disable grid-col-12',
								'type'	=> 'text',
							),								
							'overlay' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 disable box inside-box groups',
								'layout' => array(
									'type'	=> array(
										'title'		=> esc_html__( 'Color overlay', 'cws-to' ),
										'addrowclasses' => 'grid-col-4',
										'type'	=> 'select',
										'source'	=> array(
											'none' => array( esc_html__( 'None', 'cws-to' ),  false, 'd:opacity;d:color;d:gradient;' ),
											'color' => array( esc_html__( 'Color', 'cws-to' ),  true, 'e:opacity;e:color;d:gradient;' ),
											'gradient' => array( esc_html__('Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient;' )
										),
									),
									'color'	=> array(
										'title'	=> esc_html__( 'Color Overlay', 'cws-to' ),
										'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
										'addrowclasses' => 'grid-col-4',
										'value' => CRYPTOP_FIRST_COLOR,
										'type'	=> 'text',
									),
									'opacity' => array(
										'type' => 'number',
										'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
										'placeholder' => esc_html__( 'In percents', 'cws-to' ),
										'value' => '100',
										'addrowclasses' => 'grid-col-4',
									),
									'gradient' => array(
										'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
										'type' => 'fields',
										'addrowclasses' => 'grid-col-12 disable box inside-box groups',
										'layout' => '%gradient_layout%',
									),
								),
							),
							'use_pattern' => array(
								'title' => esc_html__( 'Add pattern', 'cws-to' ),
								'addrowclasses' => 'disable checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'data-options="e:pattern_image;"',
							),
							'pattern_image' => array(
								'type' => 'fields',
								'title' => esc_html__( 'Pattern image', 'cws-to' ),
								'addrowclasses' => 'disable box grid-col-12 inside-box groups',
								'layout' => '%image_layout%',
							),
							'breadcrumbs_divider' => array(
								'title' => esc_html__( 'Breadcrumbs Divider', 'cws-to' ),
								'type' => 'media',
								'url-atts' => 'readonly',
								'addrowclasses' => 'grid-col-12',
								'layout' => array(
									'is_high_dpi' => array(
										'title' => esc_html__( 'High-Resolution logo', 'cws-to' ),
										'addrowclasses' => 'checkbox',
										'type' => 'checkbox',
									),
								),
							),
							'breadcrumbs_dimensions' => array(
								'title' => esc_html__( 'Breadcrumbs Divider Dimensions', 'cws-to' ),
								'type' => 'dimensions',
								'addrowclasses' => 'grid-col-12',
								'value' => array(
									'width' => array('placeholder' => esc_html__( 'Width', 'cws-to' ), 'value' => ''),
									'height' => array('placeholder' => esc_html__( 'Height', 'cws-to' ), 'value' => ''),
								),
							),
							'breadcrumbs-margin' => array(
								'title' => esc_html__( 'Margins (px)', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'grid-col-4',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '0'),
									'left' => array('placeholder' => esc_html__( 'left', 'cws-to' ), 'value' => '0'),
									'right' => array('placeholder' => esc_html__( 'Right', 'cws-to' ), 'value' => '0'),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '0'),
									),
							),

							'use_blur' => array(
								'title' => esc_html__( 'Apply blur', 'cws-to' ),
								'addrowclasses' => 'disable checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'data-options="e:blur_intensity;"',
							),
							'blur_intensity' => array(
								'type' => 'number',
								'addrowclasses' => 'disable grid-col-12',
								'title' => esc_html__( 'Intensity', 'cws-to' ),
								'placeholder' => esc_html__( 'In percents', 'cws-to' ),
								'value' => '8'
							),
							'spacings' => array(
								'title' => esc_html__( 'Add Spacings', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'disable two-inputs grid-col-6',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '60'),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '60'),
								),
							),
							'border' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => '%border_layout%',
							),
							'slide_down' => array(
								'title' => esc_html__( 'Slide down Header on Page Load', 'cws-to' ),
								'addrowclasses' => 'disable checkbox grid-col-12',
								'type' => 'checkbox',
							),								
							'animate' => array(
								'title' => esc_html__( 'Add Mouse Scroll Animation', 'cws-to' ),
								'addrowclasses' => 'disable checkbox grid-col-12',
								'type' => 'checkbox',
								'tooltip' => array(
									'title' => esc_html__( 'Documentation', 'cws-to' ),
									'content' => esc_html__( 'Project on https://github.com/Prinzhorn/skrollr <a href="https://github.com/Prinzhorn/skrollr">GitHub</a>', 'cws-to' ),
								),
								'atts' => 'checked data-options="e:animate_options;"',
							),
							'animate_options' => array(
								'type' => 'group',
								'addrowclasses' => 'disable group expander grid-col-12',
								'title' => esc_html__('Animation Steps', 'cws-to' ),
								'button_title' => esc_html__('Add New Step', 'cws-to' ),
								'layout' => array(
									'element'	=> array(
										'title'		=> esc_html__( 'Header Section', 'cws-to' ),
										'type'	=> 'select',
										'addrowclasses' => 'grid-col-3',
										'source'	=> array(
											'title' => array( esc_html__( 'Title & Breadcrumbs', 'cws-to' ),  true, '' ),
											'container' => array( esc_html__( 'Content Width', 'cws-to' ),  false, '' ),
											'section' => array( esc_html__( 'Full Width', 'cws-to' ), false, '' )
										),
									),
									'value' => array(
										'type' => 'number',
										'atts' => 'data-role="title"',
										'value' => '100',
										'addrowclasses' => 'grid-col-3',
										'title' => esc_html__('Top offset (in px)', 'cws-to' ),
									),
									'styles' => array(
										'type' => 'textarea',
										'atts' => 'rows="5"',
										'addrowclasses' => 'grid-col-6',
										'title' => esc_html__('Styles CSS', 'cws-to' ),
									),
								),
							),							
						),
					),

				),
			),
			'tab8' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Top bar', 'cws-to' ),
				'layout' => array(
					'MB_topbar' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'atts' => 'data-options="e:top_bar_box;"',
						'type' => 'checkbox',
					),

					'top_bar_box' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'title' => esc_html__( 'Top Bar', 'cws-to' ),
								'addrowclasses' => 'grid-col-12 checkbox alt',
								'type' => 'checkbox',
							),
							'wide' => array(
								'title' => esc_html__( 'Apply Full-Width Top Bar', 'cws-to' ),
								'addrowclasses' => 'grid-col-12 checkbox',
								'type' => 'checkbox',
							),
							'language_bar' => array(
								'title' => esc_html__( 'Add Language Bar', 'cws-to' ),
								'addrowclasses' => 'grid-col-12 checkbox',
								'atts' => 'checked data-options="e:language_bar_position;"',
								'type' => 'checkbox',
							),
							'language_bar_position' => array(
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'disable grid-col-12',
								'value' => array(
									'left' => array( esc_html__( 'Left', 'cws-to' ), 	true, '', '/img/multilingual-left.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false, '', '/img/multilingual-right.png' ),
								),
							),
							'social_place' => array(
								'title' => esc_html__( 'Social Icons Alignment', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-12',
								'value' => array(
									'left' =>array( esc_html__( 'Left', 'cws-to' ), true, '', '/img/social-left.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false, '', '/img/social-right.png' ),
								),
							),							
							'toggle_share' => array(
								'title' => esc_html__( 'Toggle Social Icons', 'cws-to' ),
								'addrowclasses' => 'grid-col-12 checkbox',
								'atts' => 'checked',
								'type' => 'checkbox',
							),
							'text' => array(
								'title' => esc_html__( 'Content', 'cws-to' ),
								'addrowclasses' => 'grid-col-12 full_row',
								'tooltip' => array(
									'title' => esc_html__( 'Indent Adjusting', 'cws-to' ),
									'content' => esc_html__( 'Adjust Indents by multiple spaces.<br /> Line breaks are working too.', 'cws-to' ),
								),
								'type' => 'textarea',
								'atts' => 'rows="6"',
							),
							'background_color' => array(
								'title' => esc_html__( 'Customize Background', 'cws-to' ),
								'atts' => 'data-default-color="#fafafa"',
								'value' => '#fafafa',
								'addrowclasses' => 'new_row grid-col-3',
								'type' => 'text',
							),
							'font_color' => array(
								'title' => esc_html__( 'Font Color', 'cws-to' ),
								'atts' => 'data-default-color="#999999"',
								'value' => '#999999',
								'addrowclasses' => 'grid-col-3',
								'type' => 'text',
							),
							'hover_font_color' => array(
								'title' => esc_html__( 'Hover Color', 'cws-to' ),
								'atts' => 'data-default-color="#f5881b"',
								'value' => '#f5881b',
								'addrowclasses' => 'grid-col-3',
								'type' => 'text',
							),							
							'background_opacity' => array(
								'type' => 'number',
								'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
								'placeholder' => esc_html__( 'In percents', 'cws-to' ),
								'value' => '100',
								'addrowclasses' => 'grid-col-3',
							),
							'border' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => '%border_layout%',
							),
							'spacings' => array(
								'title' => esc_html__( 'Add Spacings (px)', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'new_row grid-col-4 two-inputs',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '5'),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '5'),
								),
							),	
						),
					),

				),
			),
			'tab9' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Footer', 'cws-to' ),
				'layout' => array(
					'MB_footer' => array(
						'type' => 'checkbox',
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'atts' => 'data-options="e:footer;"',
						'addrowclasses' => 'checkbox alt box',
					),

					'footer' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'menu_enable' => array(
								'title' => esc_html__( 'Footer Menu', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
								'atts' => 'checked data-options="e:override_menu;e:custom_menu;e:menu_position;"',
							),
							'override_menu' => array(
								'type' => 'checkbox',
								'title' => esc_html__( 'Use Custom Menu on this Page', 'cws-to' ),
								'atts' => 'data-options="e:custom_menu;"',
								'addrowclasses' => 'checkbox grid-col-4 disable',
							),
							'custom_menu' => array(
								'title' => esc_html__('Select a menu', 'cws-to' ),
								'addrowclasses' => 'disable grid-col-4',
								'type' => 'taxonomy',
								'source' => array(),
								'taxonomy' => 'nav_menu',
							),	
							'menu_position' => array(
								'title' => esc_html__( 'Menu Alignment', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-4 disable',
								'value' => array(
									'left' => array( esc_html__( 'Left', 'cws-to' ), 	true, '', '/img/align-left.png' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false, '', '/img/align-right.png' ),
								),
							),							
							'fixed' => array(
								'title' => esc_html__( 'Apply Fixed Footer Style', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
							),
							'wide' => array(
								'title' => esc_html__( 'Apply Full-Width Footer', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12',
								'type' => 'checkbox',
							),								
							'layout' => array(
								'type' => 'select',
								'title' => esc_html__( 'Select a layout', 'cws-to' ),
								'addrowclasses' => 'grid-col-6',
								'source' => array(
									'1' => array( esc_html__( '1/1 Column', 'cws-to' ),  false ),
									'2' => array( esc_html__( '2/2 Column', 'cws-to' ), false ),
									'3' => array( esc_html__( '3/3 Column', 'cws-to' ), false ),
									'4' => array( esc_html__( '4/4 Column', 'cws-to' ), false ),
									'two-three' => array( esc_html__( '2/3 + 1/3 Column', 'cws-to' ), false ),
									'one-two' => array( esc_html__( '1/3 + 2/3 Column', 'cws-to' ), false ),
									'one-three' => array( esc_html__( '1/4 + 3/4 Column', 'cws-to' ), false ),
									'one-one-two' => array( esc_html__( '1/4 + 1/4 + 2/4 Column', 'cws-to' ), false ),
									'two-one-one' => array( esc_html__( '2/4 + 1/4 + 1/4 Column', 'cws-to' ), true ),
									'one-two-one' => array( esc_html__( '1/4 + 2/4 + 1/4 Column', 'cws-to' ), false ),
								),
							),
							'sidebar' => array(
								'title' 		=> esc_html__('Select Footer\'s Sidebar Area', 'cws-to' ),
								'type' 			=> 'select',
								'addrowclasses' => 'grid-col-6',
								'source' 		=> 'sidebars',
							),
							'text_alignment' => array(
								'type' => 'select',
								'title' => esc_html__( 'Text Alignment', 'cws-to' ),
								'addrowclasses' => 'grid-col-6',
								'source' => array(
									'left' => array( esc_html__( 'Left', 'cws-to' ), 	false, '' ), 
									'center' => array( esc_html__( 'Center', 'cws-to' ), 	false, '' ),
									'right' => array( esc_html__( 'Right', 'cws-to' ), 	false, '' ), 
								),
							),							
							'copyrights_text' => array(
								'title' => esc_html__( 'Copyrights content', 'cws-to' ),
								'type' => 'textarea',
								'addrowclasses' => 'grid-col-12 full_row',
								'value' => 'Copyright © 2018. All rights reserved.',
								'atts' => 'rows="6"',
							),
							'background_image' => array(
								'type' => 'fields',
								'addrowclasses' => 'box grid-col-12 inside-box groups',
								'layout' => '%image_layout%',
							),
							'background_color'	=> array(
								'title'	=> esc_html__( 'Background Color', 'cws-to' ),
								'atts'	=> 'data-default-color="#1d2326"',
								'value' => '#1d2326',
								'addrowclasses' => 'grid-col-4',
								'type'	=> 'text'
							),
							'title_color' => array(
								'title' => esc_html__( 'Titles Color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'value' => '#ffffff',
								'addrowclasses' => 'grid-col-4',
								'type' => 'text',
							),								
							'font_color' => array(
								'title' => esc_html__( 'Font Color', 'cws-to' ),
								'atts' => 'data-default-color="#a6a6a6"',
								'value' => '#a6a6a6',
								'addrowclasses' => 'grid-col-4',
								'type' => 'text',
							),
							'copyrights_background_color' => array(
								'title'	=> esc_html__( 'Background Color (Copyrights)', 'cws-to' ),
								'atts' => 'data-default-color="#191e21"',
								'value' => '#191e21',
								'addrowclasses' => 'grid-col-4',
								'type'	=> 'text'
							),
							'copyrights_font_color' => array(
								'title' => esc_html__( 'Font color (Copyrights)', 'cws-to' ),
								'atts' => 'data-default-color="#a6a6a6"',
								'value' => '#a6a6a6',
								'addrowclasses' => 'grid-col-4',
								'type' => 'text',
							),
							'copyrights_hover_color' => array(
								'title' => esc_html__( 'Hover color (Copyrights)', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'value' => '#ffffff',
								'addrowclasses' => 'grid-col-4',
								'type' => 'text',
							),			
							'pattern_image' => array(
								'type' => 'fields',
								'title' => esc_html__( 'Pattern Image', 'cws-to' ),
								'addrowclasses' => 'box grid-col-12 inside-box groups',
								'layout' => '%image_layout%',
							),
							'overlay' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => array(
									'type'	=> array(
										'title'		=> esc_html__( 'Color overlay', 'cws-to' ),
										'addrowclasses' => 'grid-col-4',
										'type'	=> 'select',
										'source'	=> array(
											'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:opacity;d:color;d:gradient;' ),
											'color' => array( esc_html__( 'Color', 'cws-to' ),  false, 'e:opacity;e:color;d:gradient;' ),
											'gradient' => array( esc_html__('Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient;' )
										),
									),
									'color'	=> array(
										'title'	=> esc_html__( 'Color', 'cws-to' ),
										'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
										'addrowclasses' => 'grid-col-4',
										'value' => CRYPTOP_FIRST_COLOR,
										'type'	=> 'text',
										'customizer' => array( 'show' => true )
									),
									'opacity' => array(
										'type' => 'number',
										'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
										'placeholder' => esc_html__( 'In percents', 'cws-to' ),
										'value' => '40',
										'addrowclasses' => 'grid-col-4',
									),
									'gradient' => array(
										'type' => 'fields',
										'addrowclasses' => 'grid-col-12 disable box inside-box groups',
										'layout' => '%gradient_layout%',
									),
								),
							),
							'border' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => '%border_layout%',
							),
							'instagram_feed' => array(
								'title' => esc_html__( 'Add Instagram Feed', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-6',
								'type' => 'checkbox',
								'atts' => 'data-options="e:instagram_feed_shortcode;e:instagram_feed_full_width;"',
							),
							'instagram_feed_full_width' => array(
								'title' => esc_html__( 'Apply Full-Width Feed', 'cws-to' ),
								'addrowclasses' => 'disable checkbox grid-col-12',
								'type' => 'checkbox',
							),							
							'instagram_feed_shortcode' => array(
								'title' => esc_html__( 'Instagram Shortcode', 'cws-to' ),
								'addrowclasses' => 'disable grid-col-12 full_row',
								'type' => 'textarea',
								'atts' => 'rows="3"',
								'default' => '',
								'value' => '[instagram-feed cols=8 num=8 imagepadding=0 imagepaddingunit=px showheader=false showbutton=true showfollow=true]'
							),
							'spacings' => array(
								'title' => esc_html__( 'Add Spacings (px)', 'cws-to' ),
								'type' => 'margins',
								'addrowclasses' => 'new_row grid-col-12 two-inputs',
								'value' => array(
									'top' => array('placeholder' => esc_html__( 'Top', 'cws-to' ), 'value' => '10'),
									'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cws-to' ), 'value' => '5'),
								),
							),	

						),
					),

				),
			),
			'tab10' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Layout', 'cws-to' ),
				'layout' => array(
					'MB_boxed' => array(
						'type' => 'checkbox',
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'atts' => 'data-options="e:boxed;"'
					),

					'boxed' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'type' => 'checkbox',
								'title' => esc_html__( 'Apply Boxed Layout', 'cws-to' ),
								'addrowclasses' => 'checkbox grid-col-12 box',
							),
							'background_image' => array(
								'type' => 'fields',
								'addrowclasses' => 'box inside-box groups grid-col-12',
								'layout' => '%image_layout%', 
							),
							'overlay' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => array(
									'type'	=> array(
										'title'		=> esc_html__( 'Color overlay', 'cws-to' ),
										'addrowclasses' => 'grid-col-4',
										'type'	=> 'select',
										'source'	=> array(
											'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:opacity;d:color;d:gradient;' ),
											'color' => array( esc_html__( 'Color', 'cws-to' ),  false, 'e:opacity;e:color;d:gradient;' ),
											'gradient' => array( esc_html__('Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient;' )
										),
									),
									'color'	=> array(
										'title'	=> esc_html__( 'Color', 'cws-to' ),
										'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
										'addrowclasses' => 'grid-col-4',
										'value' => CRYPTOP_FIRST_COLOR,
										'type'	=> 'text',
									),
									'opacity' => array(
										'type' => 'number',
										'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
										'placeholder' => esc_html__( 'In percents', 'cws-to' ),
										'value' => '40',
										'addrowclasses' => 'grid-col-4',
									),
									'gradient' => array(
										'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
										'type' => 'fields',
										'addrowclasses' => 'grid-col-12 disable box inside-box groups',
										'layout' => '%gradient_layout%',
									),
								),
							),
						),
					),

				),
			),
			'tab11' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Sidebar', 'cws-to' ),
				'layout' => array(
					'MB_side_panel' => array(
						'title' => esc_html__( 'Customize', 'cws-to' ),
						'addrowclasses' => 'checkbox alt box',
						'atts' => 'data-options="e:side_panel;"',
						'type' => 'checkbox',
					),

					'side_panel' => array(
						'type' => 'fields',
						'addrowclasses' => 'disable inside-box groups grid-col-12 box',
						'layout' => array(
							'enable' => array(
								'title' => esc_html__( 'Side Panel', 'cws-to' ),
								'addrowclasses' => 'alt checkbox grid-col-12',
								'type' => 'checkbox',
							),	
							// 1st row
							'theme'	=> array(
								'title'		=> esc_html__( 'Color Variation', 'cws-to' ),
								'addrowclasses' => 'grid-col-12',
								'type'	=> 'select',
								'source'	=> array(
									'dark' => array( esc_html__( 'Dark', 'cws-to' ),  true, '' ),
									'light' => array( esc_html__( 'Light', 'cws-to' ),  false, '' ),
								),
							),
							'place' => array(
								'title' => esc_html__( 'Menu Icon Location', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'addrowclasses' => 'grid-col-12',
								'value' => array(
									'topbar_left' =>array( esc_html__( 'TopBar (Left)', 'cws-to' ), false, '', '/img/top-hamb-left.png' ),
									'topbar_right' => array( esc_html__( 'TopBar (Right)', 'cws-to' ), 	false, '', '/img/top-hamb-right.png' ),
									'menu_left' =>array( esc_html__( 'Menu (Left)', 'cws-to' ), true, '', '/img/hamb-left.png' ),
									'menu_right' =>array( esc_html__( 'Menu (Right)', 'cws-to' ), false, '', '/img/hamb-right.png' ),
								),
							),
							'position' => array(
								'title' 			=> esc_html__('Side Panel Position', 'cws-to' ),
								'type' 				=> 'radio',
								'subtype' 			=> 'images',
								'addrowclasses' => 'grid-col-12',
								'value' 			=> array(
									'left' 				=> 	array( esc_html__('Left', 'cws-to' ), true, '',	'/img/left.png' ),
									'right' 			=> 	array( esc_html__('Right', 'cws-to' ), false, '', '/img/right.png' ),
								),
							),								
							'sidebar' => array(
								'title' 		=> esc_html__('Select the Sidebar Area', 'cws-to' ),
								'type' 			=> 'select',
								'addrowclasses' => 'new_row grid-col-12',
								'source' 		=> 'sidebars',
								'value' => 'side_panel',
							),
							'appear'	=> array(
								'title'		=> esc_html__( 'Animation Format', 'cws-to' ),
								'type'	=> 'select',
								'addrowclasses' => 'grid-col-12',
								'source'	=> array(
									'fade' => array( esc_html__( 'Fade', 'cws-to' ),  true ),
									'slide' => array( esc_html__( 'Slide', 'cws-to' ), false ),
									'pull' => array( esc_html__( 'Pull', 'cws-to' ), false ),
								),
							),
							'logo_position' => array(
								'title' => esc_html__( 'Logo position', 'cws-to' ),
								'addrowclasses' => 'grid-col-12',
								'type' => 'radio',
								'value' => array(
									'left' => array( esc_html__( 'Left', 'cws-to' ),  true, '' ),
									'center' =>array( esc_html__( 'Center', 'cws-to' ), false,  '' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false,  '' ),
								),
							),
							'close_position' => array(
								'title' => esc_html__( 'Close Button Position', 'cws-to' ),
								'addrowclasses' => 'grid-col-12',
								'type' => 'radio',
								'value' => array(
									'left' => array( esc_html__( 'Left', 'cws-to' ),  true, '' ),
									'right' =>array( esc_html__( 'Right', 'cws-to' ), false,  '' ),
								),
							),							
							// 5th row
							'bg_size' => array(
								'title' => esc_html__( 'Background size', 'cws-to' ),
								'addrowclasses' => 'grid-col-4',
								'type' => 'radio',
								'value' => array(
									'cover' => array( esc_html__( 'Cover', 'cws-to' ),  true, '' ),
									'contain' =>array( esc_html__( 'Contain', 'cws-to' ), false,  '' ),
								),
							),
							'bg_opacity' => array(
								'type' => 'number',
								'title' => esc_html__( 'Background Opacity', 'cws-to' ),
								'placeholder' => esc_html__( 'In percents', 'cws-to' ),
								'addrowclasses' => 'grid-col-4',
								'value' => '100'
							),
							'bg_position' => array(
								'title' => esc_html__( 'Background Position', 'cws-to' ),
								'addrowclasses' => 'grid-col-4',
								'cols' => 3,
								'type' => 'radio',
								'value' => array(
									'tl'=>	array( '', false ),
									'tc'=>	array( '', false ),
									'tr'=>	array( '', false ),
									'cl'=>	array( '', false ),
									'cc'=>	array( '', true ),
									'cr'=>	array( '', false ),
									'bl'=>	array( '', false ),
									'bc'=>	array( '', false ),
									'br'=>	array( '', false ),
								),
							),
							'bg_color'	=> array(
								'title'	=> esc_html__( 'Background Color', 'cws-to' ),
								'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
								'addrowclasses' => 'grid-col-6',
								'value' => CRYPTOP_FIRST_COLOR,
								'type'	=> 'text',
							),
							'bg_font_color'	=> array(
								'title'	=> esc_html__( 'Font color', 'cws-to' ),
								'atts' => 'data-default-color="#ffffff"',
								'addrowclasses' => 'grid-col-6',
								'value' => '#ffffff',
								'type'	=> 'text',
							),							
							'overlay' => array(
								'type' => 'fields',
								'addrowclasses' => 'grid-col-12 box inside-box groups',
								'layout' => array(
									'type'	=> array(
										'title'		=> esc_html__( 'Overlay color', 'cws-to' ),
										'addrowclasses' => 'grid-col-4',
										'type'	=> 'select',
										'source'	=> array(
											'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:opacity;d:color;d:gradient;' ),
											'color' => array( esc_html__( 'Color', 'cws-to' ),  false, 'e:opacity;e:color;d:gradient;' ),
											'gradient' => array( esc_html__('Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient;' )
										),
									),
									'color'	=> array(
										'title'	=> esc_html__( 'Color', 'cws-to' ),
										'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
										'addrowclasses' => 'grid-col-4',
										'value' => CRYPTOP_FIRST_COLOR,
										'type'	=> 'text',
									),
									'opacity' => array(
										'type' => 'number',
										'title' => esc_html__( 'Opacity (%)', 'cws-to' ),
										'placeholder' => esc_html__( 'In percents', 'cws-to' ),
										'value' => '40',
										'addrowclasses' => 'grid-col-4',
									),
									'gradient' => array(
										'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
										'type' => 'fields',
										'addrowclasses' => 'grid-col-12 disable box inside-box groups',
										'layout' => '%gradient_layout%',
									),
								),
							),
						),
					),

				)
			),
		);		

		$this->mb_staff_layout = array(
			'is_clickable' => array(
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'title' => esc_html__('Show details page', 'cws-to' ),
				'atts' => 'data-options="e:add_btn;"',
			),
			'experience' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Experience', 'cws-to' ),			
			),			
			'email' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Email', 'cws-to' ),			
			),			
			'biography' => array(
				'atts' => 'rows="5"',
				'type' => 'textarea',
				'title' 		=> esc_html__( 'Biography', 'cws-to' ),			
			),
			'read_more_btn' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Read more Button', 'cws-to' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-options="e:read_more_title;"',
			),
			'read_more_title' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Title Button', 'cws-to' ),
				'addrowclasses' => 'disable',				
			),
			//Uncomment this lines to activate Classes
/*			'add_view_btn' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Add Button View Classes', 'cws-to' ),
				'atts' => 'data-options="e:link_to_view;e:title_btn_view;"',
			),
			'title_btn_view' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Title Button', 'cws-to' ),
				'addrowclasses' => 'disable',	
				'value'			=> 'View Classes',			
			),
			'link_to_view' 		=> array(
				'type' => 'select',
				'title' => esc_html__( 'Link To:', 'cws-to' ),
				'source' => array(
					'archive' => array(esc_html__( 'Archive', 'cws-to' ), true, 'd:link_custom_url;'),
					'custom_url' => array(esc_html__( 'Custom Url', 'cws-to' ), false, 'e:link_custom_url;'),
				),
			),
			'link_custom_url' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Custom url', 'cws-to' ),
				'addrowclasses' => 'disable',				
				'default' 		=> ''
			),*/
			'social_group' => array(
				'type' => 'group',
				'addrowclasses' => 'group expander sortable box',
				'title' => esc_html__('Social networks', 'cws-to' ),
				'button_title' => esc_html__('Add new social network', 'cws-to' ),
				'layout' => array(
					'title' => array(
						'type' => 'text',
						'atts' => 'data-role="title"',
						'title' => esc_html__('Social account title', 'cws-to' ),
					),
					'icon' => array(
						'type' => 'select',
						'addrowclasses' => 'fai',
						'source' => 'fa',
						'title' => esc_html__('Select the icon for this social contact', 'cws-to' )
					),
					'url' => array(
						'type' => 'text',
						'title' => esc_html__('Url to your account', 'cws-to' ),
					),
				),
			),
		);

		$this->mb_portfolio_layout = array(
			'tab0' => array(
				'type' => 'tab',
				'init' => 'open grid-col-12',
				'title' => esc_html__( 'General', 'cws-to' ),
				'layout' => array(
					'post_sidebars' => array(
						'title' => esc_html__( 'Page Sidebars Settings', 'cws-to' ),
						'type' => 'fields',
						'addrowclasses' => 'box inside-box groups',
						'layout' => array(
							'layout' => array(
								'title' => esc_html__('Sidebar Position', 'cws-to' ),
								'type' => 'radio',
								'subtype' => 'images',
								'value' => array(
									'{page_sidebars}'=>	array( esc_html__('Default', 'cws-to' ), true, 'd:def--sidebar1;d:def--sidebar2', '/img/default.png' ),
									'left' => 	array( esc_html__('Left', 'cws-to' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
									'right' => 	array( esc_html__('Right', 'cws-to' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
									'both' => 	array( esc_html__('Double', 'cws-to' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
									'none' => 	array( esc_html__('None', 'cws-to' ), false, 'd:sb1;d:sb2', '/img/none.png' )
								),
							),
							'sb1' => array(
								'title' => esc_html__('Select a sidebar', 'cws-to' ),
								'type' => 'select',
								'addrowclasses' => 'disable box',
								'source' => 'sidebars',
							),
							'sb2' => array(
								'title' => esc_html__('Select right sidebar', 'cws-to' ),
								'type' => 'select',
								'addrowclasses' => 'disable box',
								'source' => 'sidebars',
							),
						),
					),
					'full_width' => array(
						'type' => 'checkbox',
						'title' => esc_html__( 'Full Width', 'cws-to' ),
						'atts' => 'data-options="d:decr_pos;"',
					),
					'decr_pos' => array(
						'type' => 'select',
						'title' => esc_html__( 'Project Description', 'cws-to' ),
						'source' => array(
							'bot' => array(esc_html__( 'Bottom', 'cws-to' ), true, 'd:cont_width;'),
							'left' => array(esc_html__( 'Left', 'cws-to' ), false, 'e:cont_width;'),
							'left_s' => array(esc_html__( 'Left + Sticky', 'cws-to' ), false, 'e:cont_width;'),
							'right' => array(esc_html__( 'Right', 'cws-to' ), false, 'e:cont_width;'),
							'right_s' => array(esc_html__( 'Right + Sticky', 'cws-to' ), false, 'e:cont_width;'),
						),
					),
					'cont_width' => array(
						'type' => 'select',
						'title' => esc_html__( 'Content Width', 'cws-to' ),
						'source' => array(
							'25' => array(esc_html__( '1/4', 'cws-to' ), false),
							'33' => array(esc_html__( '1/3', 'cws-to' ), true),
							'50' => array(esc_html__( '1/2', 'cws-to' ), false),
							'66' => array(esc_html__( '2/3', 'cws-to' ), false),
						),
					),
					'portfolio_type' => array(
						'type' => 'select',
						'title' => esc_html__( 'Portfolio Single\'s Format', 'cws-to' ),
						'source' => array(
							'image' => array(esc_html__( 'Featured Image', 'cws-to' ), true, 'd:gallery_type;d:video_type;d:slider_type;d:rev_slider_type;'),
							'gallery' => array(esc_html__( 'Gallery', 'cws-to' ), false, 'e:gallery_type;d:video_type;d:slider_type;d:rev_slider_type;'),
							'slider' => array(esc_html__( 'Slider', 'cws-to' ), false, 'd:gallery_type;d:video_type;e:slider_type;d:rev_slider_type;'),
							'rev_slider' => array(esc_html__( 'External Slider', 'cws-to' ), false, 'd:gallery_type;d:video_type;d:slider_type;e:rev_slider_type;'),
							'video' => array(esc_html__( 'Video', 'cws-to' ), false, 'd:gallery_type;e:video_type;d:slider_type;d:rev_slider_type;'),
							'none' => array(esc_html__( 'None', 'cws-to' ), false, 'd:gallery_type;d:video_type;d:slider_type;d:rev_slider_type;'),
						),
					),
					'gallery_type' => array(
						'type' => 'fields',
						'addrowclasses' => 'box inside-box groups',
						'layout' => array(
							'gallery' => array(
								'title' => esc_html__( 'Add Media', 'cws-to' ),
								'type' => 'gallery'
							),
						),
					),
					'slider_type' => array(
						'type' => 'fields',
						'addrowclasses' => 'box inside-box groups',
						'layout' => array(
							'slider_gall' => array(
								'title' => esc_html__( 'Add Media', 'cws-to' ),
								'type' => 'gallery',
								'addrowclasses' => 'grid-col-3',
							),
							'on_grid' => array(
								'title' => esc_html__( 'Show on Portfolio Grid', 'cws-to' ),
								'type' => 'checkbox',
								'addrowclasses' => 'grid-col-3',
							),
						),
					),
					'rev_slider_type' => array(
						'type' => 'fields',
						'addrowclasses' => 'box inside-box groups',
						'layout' => array(
							'rev_url' => array(
								'title' => esc_html__( 'Add Shortcode', 'cws-to' ),
								'type' => 'text',
							),
						)
					),
					'video_type' => array(
						'type' => 'fields',
						'addrowclasses' => 'box inside-box groups',
						'layout' => array(
							'video_t' => array(
								'type' => 'select',
								'source' => array(
									'youtube' => array(esc_html__( 'YouTube', 'cws-to' ), true, 'e:youtube_t;d:vimeo_t;d:other_t;'),
									'vimeo' => array(esc_html__( 'Vimeo', 'cws-to' ), false, 'd:youtube_t;e:vimeo_t;d:other_t;'),
									'other' => array(esc_html__( 'Other', 'cws-to' ), false, 'd:youtube_t;d:vimeo_t;e:other_t;'),
								),
							),
							'youtube_t' => array(
								'type' => 'fields',
								'addrowclasses' => 'box inside-box groups grid-col-12',
								'layout' => array(
									'url' => array(
										'title' => esc_html__( 'Video ID', 'cws-to' ),
										'type' => 'text',
									),
								),
							),
							'vimeo_t' => array(
								'type' => 'fields',
								'addrowclasses' => 'box inside-box groups grid-col-12',
								'layout' => array(
									'url' => array(
										'title' => esc_html__( 'Video ID', 'cws-to' ),
										'type' => 'text',
									),
								),
							),
							'other_t' => array(
								'type' => 'fields',
								'addrowclasses' => 'box inside-box groups grid-col-12',
								'layout' => array(
									'url' => array(
										'title' => esc_html__( 'Video URL', 'cws-to' ),
										'type' => 'text',
									),
								),
							),
							'popup' => array(
								'title' => esc_html__( 'Show Video in a Popup on Single', 'cws-to' ),
								'type' => 'checkbox',
								'addrowclasses' => 'grid-col-12',
								'atts' => 'data-options="e:img;"',
							),
							'on_grid' => array(
								'title' => esc_html__( 'Show on Portfolio Grid', 'cws-to' ),
								'type' => 'checkbox',
								'addrowclasses' => 'grid-col-12',
								'atts' => 'data-options="e:popup_grid;"',
							),
							'popup_grid' => array(
								'title' => esc_html__( 'Show Video in a Popup on Grid', 'cws-to' ),
								'type' => 'checkbox',
								'addrowclasses' => 'grid-col-12',
							),
						)
					),
				),
			),
			'tab1' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Isotope Layout', 'cws-to' ),
				'layout' => array(
					'isotope_col_count' => array(
						'type' => 'select',
						'title' => esc_html__( 'Columns', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
						'source' => array(
							'1' => array(esc_html__( 'One', 'cws-to' ), true),
							'2' => array(esc_html__( 'Two', 'cws-to' ), false),
							'3' => array(esc_html__( 'Three', 'cws-to' ), false),
							'4' => array(esc_html__( 'Four', 'cws-to' ), false),
						),
					),
					'isotope_line_count' => array(
						'type' => 'select',
						'title' => esc_html__( 'Lines', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
						'source' => array(
							'1' => array(esc_html__( 'One', 'cws-to' ), true),
							'2' => array(esc_html__( 'Two', 'cws-to' ), false),
							'3' => array(esc_html__( 'Three', 'cws-to' ), false),
							'4' => array(esc_html__( 'Four', 'cws-to' ), false),
						),
					),
					'desc' => array(
						'type' => 'lable',
						'addrowclasses' => 'grid-col-12',
						'title' => esc_html__( 'This option is used in the Isotope Portfolio Layout only. The image will take the selected number of Columns/Lines and will be displayed accordingly.', 'cws-to' ),
					),
				),
			),
			'tab2' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Related Items', 'cws-to' ),
				'layout' => array(
					'carousel' => array(
						'title' => esc_html__( 'Display items carousel for this portfolio post', 'cws-to' ),
						'type' => 'checkbox',
						'atts' => 'checked',
						'addrowclasses' => 'checkbox grid-col-12',
					),
					'show_related' => array(
						'title' => esc_html__( 'Show related Items', 'cws-to' ),
						'type' => 'checkbox',
						'atts' => 'checked data-options="e:related_projects_options;e:rpo_title;e:rpo_cols;e:rpo_items_count;e:rpo_categories;"',
						'addrowclasses' => 'alt checkbox grid-col-12',
					),
					'rpo_title' => array(
						'type' => 'text',
						'title' => esc_html__( 'Title', 'cws-to' ),
						'value' => esc_html__( 'Related projects', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
					),
					'rpo_categories' => array(
						'title' => esc_html__( 'Categories', 'cws-to' ),
						'type' => 'taxonomy',
						'atts' => 'multiple',
						'addrowclasses' => 'grid-col-12',
						'taxonomy' => 'cws_portfolio_cat',
						'source' => array(),
					),
					'rpo_cols' => array(
						'type' => 'select',
						'title' => esc_html__( 'Columns', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
						'source' => array(
							'1' => array(esc_html__( 'One', 'cws-to' ), false),
							'2' => array(esc_html__( 'Two', 'cws-to' ), false),
							'3' => array(esc_html__( 'Three', 'cws-to' ), false),
							'4' => array(esc_html__( 'Four', 'cws-to' ), true),
							),
					),
					'rpo_items_count' => array(
						'type' => 'number',
						'title' => esc_html__( 'Number of Related Items', 'cws-to' ),
						'value' => '4',
						'addrowclasses' => 'grid-col-12',
					),
				),

			),
			'tab3' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Hover', 'cws-to' ),
				'layout' => array(
					'enable_hover' => array(
						'title' => esc_html__( 'Enable Hover', 'cws-to' ),
						'type' => 'checkbox',
						'atts' => 'checked data-options="e:link_options;e:link_options_single;e:link_options_fancybox"',
						'addrowclasses' => 'alt checkbox grid-col-12',
					),
					'link_options_fancybox' => array(
						'type' => 'checkbox',
						'title' => esc_html__( 'Open in a popup', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
						'atts' => 'checked'
					),
					'link_options_single' => array(
						'type' => 'checkbox',
						'title' => esc_html__( 'Single Link', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
						'atts' => 'checked data-options="e:link_options_url;"',
					),
					'link_options_url' => array(
						'type' => 'text',
						'title' => esc_html__( 'Add Custom URL', 'cws-to' ),
						'addrowclasses' => 'grid-col-12',
						'default' => ''
					),
				),
			),	
		);

		$this->mb_classes_layout = array(
			'post_sidebars' => array(
				'title' => esc_html__( 'Page Sidebars Settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'box inside-box groups',
				'layout' => array(
					'layout' => array(
						'title' => esc_html__('Sidebar Position', 'cws-to' ),
						'type' => 'radio',
						'subtype' => 'images',
						'value' => array(
							'{page_sidebars}'=>	array( esc_html__('Default', 'cws-to' ), true, 'd:def--sidebar1;d:def--sidebar2', '/img/default.png' ),
							'left' => 	array( esc_html__('Left', 'cws-to' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
							'right' => 	array( esc_html__('Right', 'cws-to' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
							'both' => 	array( esc_html__('Double', 'cws-to' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
							'none' => 	array( esc_html__('None', 'cws-to' ), false, 'd:sb1;d:sb2', '/img/none.png' )
						),
					),
					'sb1' => array(
						'title' => esc_html__('Select a sidebar', 'cws-to' ),
						'type' => 'select',
						'addrowclasses' => 'disable box',
						'source' => 'sidebars',
					),
					'sb2' => array(
						'title' => esc_html__('Select right sidebar', 'cws-to' ),
						'type' => 'select',
						'addrowclasses' => 'disable box',
						'source' => 'sidebars',
					),
				),
			),
			'is_clickable' => array(
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'title' => esc_html__('Show details page', 'cws-to' ),
				'atts' => 'checked data-options="e:link_to;e:add_btn;"',
			),
			'link_to' 		=> array(
				'type' 		=> 'select',
				'title' 	=> esc_html__( 'Link Options', 'cws-to' ),
				'atts' => 'data-options="select:options"',
				'source' 	=> array(
					'none' 			=> array(esc_html__( 'None', 'cws-to' ), false, 'd:link_custom_url;'),
					'post' 			=> array(esc_html__( 'Post', 'cws-to' ), false, 'd:link_custom_url;'),
					'custom_url' 	=> array(esc_html__( 'Custom Url', 'cws-to' ), false, 'e:link_custom_url;' ),
				)
			),
			'link_custom_url' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Custom url', 'cws-to' ),
				'addrowclasses' => 'disable',				
			),	
			'add_btn' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Add Button Link', 'cws-to' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-options="e:title_btn;"',
			),
			'title_btn' => array(
				'type' 			=> 'text',
				'title' 		=> esc_html__( 'Title Link Button', 'cws-to' ),
				'addrowclasses' => 'disable',				
			),

			'our_staff' => array(
				'title' => esc_html__( 'Teacher', 'cws-to' ),
				'type' => 'post_type',
				'atts' => 'multiple',
				'post_type' => 'cws_staff',
				'source' => array(),
			),
			'show_staff' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Show Details Teacher Page', 'cws-to' ),
				'atts' => 'checked',
			),
			'price' => array(
				'type' => 'text',
				'addrowclasses' => 'box',
				'title' => esc_html__( 'Price', 'cws-to' ),
				),
			'date_events' => array(
				'type' => 'text',
				'addrowclasses' => 'box',
				'title' => esc_html__( 'Date Events', 'cws-to' ),
				),			
			'time_events' => array(
				'type' => 'text',
				'title' => esc_html__( 'Time Events', 'cws-to' ),
				),
			'destinations' => array(
				'type' => 'text',
				'addrowclasses' => 'box',
				'title' => esc_html__( 'Destination Events', 'cws-to' ),
				),
			'show_featured' => array(
				'title' => esc_html__( 'Show featured image on single', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked data-options="e:wide_featured;"',
			),
			'wide_featured' => array(
				'title' => esc_html__( 'Wide featured image', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'disable checkbox box',
			),
			'carousel' => array(
				'title' => esc_html__( 'Display items carousel for this post', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked',
			),
			'show_related' => array(
				'title' => esc_html__( 'Show related items', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked data-options="e:rpo_title;e:rpo_cols;e:rpo_items_count"',
			),
			'rpo_title' => array(
				'type' => 'text',
				'addrowclasses' => 'box',
				'title' => esc_html__( 'Title', 'cws-to' ),
				'value' => esc_html__( 'Related items', 'cws-to' )
				),
			'rpo_cols' => array(
				'type' => 'select',
				'title' => esc_html__( 'Columns', 'cws-to' ),
				'addrowclasses' => 'box',
				'source' => array(
					'1' => array(esc_html__( 'one', 'cws-to' ), false),
					'2' => array(esc_html__( 'two', 'cws-to' ), false),
					'3' => array(esc_html__( 'three', 'cws-to' ), false),
					'4' => array(esc_html__( 'four', 'cws-to' ), true),
					),
				),
			'rpo_categories' => array(
				'title' => esc_html__( 'Categories', 'cws-to' ),
				'type' => 'taxonomy',
				'atts' => 'multiple',
				'taxonomy' => 'cws_classes_cat',
				'source' => array(),
			),
			'rpo_items_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Number of items to show', 'cws-to' ),
				'addrowclasses' => 'box',
				'value' => '4'
			),
			'work_days_group' => array(
				'type' => 'group',
				'addrowclasses' => 'group sortable',
				'title' => esc_html__('Working Days', 'cws-to' ),
				'button_title' => esc_html__('Add New Working Days', 'cws-to' ),
				'button_icon' => 'fa fa-plus',
				'layout' => array(
					'title' => array(
						'type' => 'text',
						'atts' => 'data-role="title"',
						'title' => esc_html__('Working Days', 'cws-to' ),
						'value' => "Monday"
					),
					'from' => array(
						'type' => 'text',
						'title' => esc_html__( 'From', 'cws-to' ),
						'value' => ""
					),	
					'to' => array(
						'type' => 'text',
						'title' => esc_html__( 'To', 'cws-to' ),
						'value' => ""
					),		
				)
			),
		);

		if ( is_customize_preview() && $a) { 
			switch (get_post_type($a)) {
				case 'cws_portfolio':
				$this->mb_page_layout = $this->mb_portfolio_layout;
				break;				
				case 'cws_staff':
				$this->mb_page_layout = $this->mb_staff_layout;
				break;				
				case 'cws_classes':
				$this->mb_page_layout = $this->mb_classes_layout;
				break;
			}
		}

		self::$instance = $this;
		$this->init();
	}

	public static function get_instance() {
		return self::$instance;
	}

	private function init() {
		add_action( 'add_meta_boxes', array($this, 'post_addmb') );
		add_action( 'add_meta_boxes_cws_testimonials', array($this, 'testimonials_addmb') );
		add_action( 'add_meta_boxes_cws_portfolio', array($this, 'portfolio_addmb') );
		add_action( 'add_meta_boxes_cws_classes', array($this, 'classes_addmb') );
		add_action( 'add_meta_boxes_cws_staff', array($this, 'staff_addmb') );
		add_action( 'add_meta_boxes_megamenu_item', array($this, 'mgmenu_addmb') );

		add_action( 'save_post', array($this, 'cws_post_metabox_save'), 11, 2 );
	}

	public function testimonials_addmb() {
		add_meta_box( 'cws-post-metabox-id-1', 'CWS Testimonials Options', array($this, 'mb_testimonials_callback'), 'cws_testimonials', 'normal', 'high' );
	}	

	public function classes_addmb() {
		add_meta_box( 'cws-post-metabox-id-1', 'CWS Classes Options', array($this, 'mb_classes_callback'), 'cws_classes', 'normal', 'high' );
	}	

	public function mgmenu_addmb() {
		add_meta_box( 'cws-post-metabox-id-1', 'CWS Megamenu Options', array($this, 'mb_mgmenu_callback'), 'megamenu_item', 'normal', 'high' );
	}

	public function portfolio_addmb() {
		add_meta_box( 'cws-post-metabox-id-1', 'CWS Portfolio Options', array($this, 'mb_portfolio_callback'), 'cws_portfolio', 'normal', 'high' );
	}

	public function staff_addmb() {
		add_meta_box( 'cws-post-metabox-id-1', 'CWS Staff Options', array($this, 'mb_staff_callback'), 'cws_staff', 'normal', 'high' );
	}

	public function post_addmb() {
		add_meta_box( 'cws-post-metabox-id-1', 'CWS Post Options', array($this, 'mb_post_callback'), 'post', 'normal', 'high' );
		add_meta_box( 'cws-post-metabox-id-2', 'Header Image', array($this, 'mb_post_side_callback'), 'post', 'side', 'low' );
		add_meta_box( 'cws-post-metabox-id-3', 'CWS Page Options', array($this, 'mb_page_callback'), 'page', 'normal', 'high' );
	}

	public function mb_staff_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = $this->mb_staff_layout;

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function mb_mgmenu_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'fw_mgmenu' => array(
				'type' => 'checkbox',
				'title' => esc_html__('Disable Full Width Megamenu', 'cws-to' ),
				'atts' => 'data-options="e:mgmenu_width;e:mgmenu_pos"',
				'addrowclasses' => 'grid-col-12',
			),
			'mgmenu_width' => array(
				'title' 		=> esc_html__( 'Set Fixed Width (in px)', 'cws-to' ),
				'type' 			=> 'text',
				'addrowclasses' => 'disable grid-col-12',
				'value'			=> '1170',
			),
			'mgmenu_pos' => array(
				'title' 		=> esc_html__( 'Dropdown Position', 'cws-to' ),
				'type' 			=> 'select',
				'addrowclasses' => 'disable grid-col-12',
				'source' => array(
					'center' => array(esc_html__( 'Center', 'cws-to' ), true),
					'left' => array(esc_html__( 'Left', 'cws-to' ), false),
					'right' => array(esc_html__( 'Right', 'cws-to' ), false),
				),
			),
		);

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function mb_page_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = $this->mb_page_layout;

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function mb_testimonials_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'carousel' => array(
				'title' => esc_html__( 'Display controls', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked',
			),
		);

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function mb_portfolio_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = $this->mb_portfolio_layout;

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function mb_classes_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = $this->mb_classes_layout;

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function mb_post_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr_all = array(
			'gallery' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Gallery', 'cws-to' ),
				'layout' => array(
					'gallery_type' => array(
						'type' => 'select',
						'title' => esc_html__( 'Gallery type', 'cws-to' ),
						'source' => array(
							'slider' => array(esc_html__( 'Slider', 'cws-to' ), true, 'd:grid_cols;'),
							'grid' => array(esc_html__( 'Grid', 'cws-to' ), false, 'e:grid_cols;'),
						),
					),
					'grid_cols' => array(
						'type' => 'select',
						'title' => esc_html__( 'Grid columns', 'cws-to' ),
						'addrowclasses' => 'disable box',
						'source' => array(
							'1' => array(esc_html__( 'one', 'cws-to' ), false),
							'2' => array(esc_html__( 'two', 'cws-to' ), false),
							'3' => array(esc_html__( 'three', 'cws-to' ), false),
							'4' => array(esc_html__( 'four', 'cws-to' ), true),
						),
					),
					'gallery' => array(
						'type' => 'gallery'
					),
				)
			),
			'video' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Video', 'cws-to' ),
				'layout' => array(
					'video' => array(
						'title' => esc_html__( 'Direct URL path of a video file', 'cws-to' ),
						'type' => 'text'
					)
				)
			),
			'audio' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Audio', 'cws-to' ),
				'layout' => array(
					'audio' => array(
						'title' => esc_html__( 'A self-hosted or SoundClod audio file URL', 'cws-to' ),
						'subtitle' => esc_html__( 'Ex.: /wp-content/uploads/audio.mp3 or http://soundcloud.com/...', 'cws-to' ),
						'type' => 'text'
					)
				)
			),
			'link' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Url', 'cws-to' ),
				'layout' => array(
					'link' => array(
						'title' => esc_html__( 'URL', 'cws-to' ),
						'type' => 'text'
					),
					'link_title' => array(
						'title' => esc_html__( 'Title', 'cws-to' ),
						'type' => 'text'
					)
				)
			),
			'quote' => array(
				'type' => 'tab',
				'init' => 'closed',
				'title' => esc_html__( 'Quote', 'cws-to' ),
				'layout' => array(
					'quote_text' => array(
						'subtitle' => esc_html__( 'Enter the quote', 'cws-to' ),
						'atts' => 'rows="5"',
						'type' => 'textarea'
					),
					'quote_author' => array(
						'title' => esc_html__( 'Author', 'cws-to' ),
						'type' => 'text'
					),
				)
			),
			'post_sidebars' => array(
				'title' => esc_html__( 'Sidebars Settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'box inside-box groups grid-col-12',
				'layout' => array(
					'layout' => array(
						'title' => esc_html__('Sidebar Position', 'cws-to' ),
						'type' => 'radio',
						'subtype' => 'images',
						'addrowclasses' => 'grid-col-12',
						'value' => array(
							'{blog_sidebars}'=>	array( esc_html__('Default', 'cws-to' ), true, 'd:sb1;d:sb2', '/img/default.png' ),
							'left' => 	array( esc_html__('Left', 'cws-to' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
							'right' => 	array( esc_html__('Right', 'cws-to' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
							'both' => 	array( esc_html__('Double', 'cws-to' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
							'none' => 	array( esc_html__('None', 'cws-to' ), false, 'd:sb1;d:sb2', '/img/none.png' )
						),
					),
					'sb1' => array(
						'title' => esc_html__('Select a sidebar', 'cws-to' ),
						'type' => 'select',
						'addrowclasses' => 'disable box grid-col-6',
						'source' => 'sidebars',
					),
					'sb2' => array(
						'title' => esc_html__('Select right sidebar', 'cws-to' ),
						'type' => 'select',
						'addrowclasses' => 'disable box grid-col-6',
						'source' => 'sidebars',
					),
				),
			),
			'enable_lightbox' => array(
				'title' => esc_html__( 'Enable lightbox', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked',
			),
			'author_info' => array(
				'title' => esc_html__( 'Show Author info', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
			),			
			'show_featured' => array(
				'title' => esc_html__( 'Show featured image on single post', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked data-options="e:full_width_featured;"',
			),
			'full_width_featured' => array(
				'type' => 'checkbox',
				'title' => esc_html__( 'Full-Width Featured Image', 'cws-to' ),
				'addrowclasses' => 'disable checkbox grid-col-12',
			),
			'show_related' => array(
				'title' => esc_html__( 'Show related items', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'checked data-options="e:rpo"',
			),
			'rpo' => array(
				'title' => esc_html__( 'Related items settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'disable groups',
				'layout' => array(
					'title' => array(
						'type' => 'text',
						'addrowclasses' => 'box grid-col-12',
						'title' => esc_html__( 'Title', 'cws-to' ),
						'value' => esc_html__( 'Related items', 'cws-to' ),
						'addrowclasses' => '',
					),
					'category' => array(
						'title' => esc_html__( 'Categories (Filter)', 'cws-to' ),
						'type' => 'taxonomy',
						'atts' => 'multiple',
						'taxonomy' => 'category',
						'addrowclasses' => 'box grid-col-12',
						'source' => array(),
					),
					'text_length' => array(
						'type' => 'number',
						'title' => esc_html__( 'Text length', 'cws-to' ),
						'addrowclasses' => 'box grid-col-12',
						'value' => '90'
					),
					'cols' => array(
						'type' => 'select',
						'title' => esc_html__( 'Columns', 'cws-to' ),
						'addrowclasses' => 'box grid-col-12',
						'source' => array(
							'1' => array(esc_html__( 'one', 'cws-to' ), false),
							'2' => array(esc_html__( 'two', 'cws-to' ), false),
							'3' => array(esc_html__( 'three', 'cws-to' ), false),
							'4' => array(esc_html__( 'four', 'cws-to' ), true),
							),
						),
					'items_show' => array(
						'type' => 'number',
						'title' => esc_html__( 'Number of items to show', 'cws-to' ),
						'addrowclasses' => 'box grid-col-12',
						'value' => '4'
					),
					'hide_meta' => array(
						'title' => esc_html__( 'Hide', 'cws-to' ),	
						'atts' => 'multiple',
						'type' => 'select',
						'addrowclasses' => 'box grid-col-12',
						'source' => array(
							'title' => array(esc_html__( 'Title', 'cws-to' ), false),
							'cats' => array(esc_html__( 'Categories', 'cws-to' ), false),
							'tags' => array(esc_html__( 'Tags', 'cws-to' ), false),
							'author' => array(esc_html__( 'Author', 'cws-to' ), false),
							'likes' => array(esc_html__( 'Likes', 'cws-to' ), false),
							'date' => array(esc_html__( 'Date', 'cws-to' ), false),
							'comments' => array(esc_html__( 'Comments', 'cws-to' ), false),
							'read_more' => array(esc_html__( 'Read More', 'cws-to' ), false),
							'social' => array(esc_html__( 'Social Icons', 'cws-to' ), false),
							'excerpt' => array(esc_html__( 'Excerpt', 'cws-to' ), false),
						),
					),
				),
			),
			'post_custom_color' => array(
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox grid-col-12',
				'title' => esc_html__( 'Edit Colors', 'cws-to' ),
				'atts' => 'data-options="e:post_title_color;e:post_font_color;e:post_font_sec_color;e:apply_color;e:post_font_meta_color"',
			),
			'apply_color' => array(
				'type' => 'select',
				'title' => esc_html__( 'Apply to', 'cws-to' ),
				'addrowclasses' => 'grid-col-12',
				'source' => array(
					'list_color' => array(esc_html__( 'Blog List', 'cws-to' ), true),
					'single_color' => array(esc_html__( 'Blog Single', 'cws-to' ), false),
					'both_color' => array(esc_html__( 'Both', 'cws-to' ), false),
				),
			),
			'post_title_color' => array(
				'title' 		=> esc_html__( 'Title Color', 'cws-to' ),
				'tooltip' => array(
					'title' => esc_html__( 'Override Title Color', 'cws-to' ),
					'content' => esc_html__( 'Override Title Color', 'cws-to' ),
				),
				'type' 			=> 'text',
				'addrowclasses' => 'disable grid-col-12',
				'atts' 			=> 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
				'value'			=> CRYPTOP_FIRST_COLOR
			),
			'post_font_color' => array(
				'title' 		=> esc_html__( 'Text Color', 'cws-to' ),
				'type' 			=> 'text',
				'addrowclasses' => 'disable grid-col-12',
				'atts' 			=> 'data-default-color="#707273;"',
				'value'			=> '#707273'
			),
			'post_font_sec_color' => array(
				'title' 		=> esc_html__( 'Helper Color', 'cws-to' ),
				'type' 			=> 'text',
				'addrowclasses' => 'disable grid-col-12',
				'atts' 			=> 'data-default-color="' . CRYPTOP_SECONDARY_COLOR . '"',
				'value'			=> CRYPTOP_SECONDARY_COLOR
			),			
			'post_font_meta_color' => array(
				'title' 		=> esc_html__( 'Meta Color', 'cws-to' ),
				'type' 			=> 'text',
				'addrowclasses' => 'disable grid-col-12',
				'atts' 			=> 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
				'value'			=> CRYPTOP_FIRST_COLOR
			),
			'custom_title_overlay' => array(
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox grid-col-12',
				'title' => esc_html__( 'Add Background Overlay', 'cws-to' ),
				'atts' => 'data-options="e:post_color_overlay_type;e:apply_bg_color;"',
			),
			'apply_bg_color' => array(
				'type' => 'select',
				'title' => esc_html__( 'Apply to', 'cws-to' ),
				'addrowclasses' => 'grid-col-12',
				'source' => array(
					'list_color' => array(esc_html__( 'Blog List', 'cws-to' ), true),
					'single_color' => array(esc_html__( 'Blog Single', 'cws-to' ), false),
					'both_color' => array(esc_html__( 'Both', 'cws-to' ), false),
				),
			),
			'post_color_overlay_type'	=> array(
				'title'		=> esc_html__( 'Color overlay', 'cws-to' ),
				'addrowclasses' => 'grid-col-12',
				'type'	=> 'select',
				'source'	=> array(
					'none' => array( esc_html__( 'None', 'cws-to' ),  false, 'd:title_bg_opacity;d:title_overlay;d:post_gradient_settings;' ),
					'color' => array( esc_html__( 'Color', 'cws-to' ),  true, 'e:title_bg_opacity;e:title_overlay;d:post_gradient_settings;' ),
					'gradient' => array( esc_html__( 'Gradient', 'cws-to' ), false, 'e:title_bg_opacity;d:title_overlay;e:post_gradient_settings;' )
					),
				),
			
			'title_overlay' => array(
				'title' => esc_html__( 'Overlay Color', 'cws-to' ),
				'type' => 'text',
				'addrowclasses' => 'grid-col-12 disable',
				'atts' 			=> 'data-default-color="#000000"',
				'value'			=> '#000000'
			),
			'title_bg_opacity' => array(
				'title' 		=> esc_html__( 'Opacity', 'cws-to' ),								
				'type' 			=> 'number',
				'addrowclasses' => 'grid-col-12',
				'atts' 			=> " min='0' max='100'",
				'value'			=> '40'
			),'post_gradient_settings' => array(
				'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'grid-col-12 disable groups',
				'layout' => array(
					'first_color' => array(
						'type' => 'text',
						'title' => esc_html__( 'From', 'cws-to' ),
						'atts' => 'data-default-color=""',
						),
					'second_color' => array(
						'type' => 'text',
						'title' => esc_html__( 'To', 'cws-to' ),
						'atts' => 'data-default-color=""',
						),
					'first_color_opacity' => array(
						'type' => 'number',
						'title' => esc_html__( 'From (Opacity %)', 'cws-to' ),
						'value' => '100',
						),
					'second_color_opacity' => array(
						'type' => 'number',
						'title' => esc_html__( 'To (Opacity %)', 'cws-to' ),
						'value' => '100',
						),
					'type' => array(
						'title' => esc_html__( 'Gradient type', 'cws-to' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array( esc_html__( 'Linear', 'cws-to' ),  true, 'e:linear_settings;d:radial_settings' ),
							'radial' =>array( esc_html__( 'Radial', 'cws-to' ), false,  'd:linear_settings;e:radial_settings' ),
							),
						),
					'linear_settings' => array(
						'title' => esc_html__( 'Linear settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable',
						'layout' => array(
							'angle' => array(
								'type' => 'number',
								'title' => esc_html__( 'Angle', 'cws-to' ),
								'value' => '45',
								),
							)
						),
					'radial_settings' => array(
						'title' => esc_html__( 'Radial settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable',
						'layout' => array(
							'shape_settings' => array(
								'title' => esc_html__( 'Shape', 'cws-to' ),
								'type' => 'radio',
								'value' => array(
									'simple' => array( esc_html__( 'Simple', 'cws-to' ),  true, 'e:shape;d:size;d:size_keyword;' ),
									'extended' =>array( esc_html__( 'Extended', 'cws-to' ), false, 'd:shape;e:size;e:size_keyword;' ),
									),
								),
							'shape' => array(
								'title' => esc_html__( 'Gradient type', 'cws-to' ),
								'type' => 'radio',
								'value' => array(
									'ellipse' => array( esc_html__( 'Ellipse', 'cws-to' ),  true ),
									'circle' =>array( esc_html__( 'Circle', 'cws-to' ), false ),
									),
								),
							'size_keyword' => array(
								'type' => 'select',
								'title' => esc_html__( 'Size keyword', 'cws-to' ),
								'addrowclasses' => 'disable',
								'source' => array(
									'closest-side' => array(esc_html__( 'Closest side', 'cws-to' ), false),
									'farthest-side' => array(esc_html__( 'Farthest side', 'cws-to' ), false),
									'closest-corner' => array(esc_html__( 'Closest corner', 'cws-to' ), false),
									'farthest-corner' => array(esc_html__( 'Farthest corner', 'cws-to' ), true),
									),
								),
							'size' => array(
								'type' => 'text',
								'addrowclasses' => 'disable',
								'title' => esc_html__( 'Size', 'cws-to' ),
								'atts' => 'placeholder="'.esc_html__( 'Two space separated percent values, for example (60% 55%)', 'cws-to' ).'"',
								),
							)
						)
					)
				),	
			'custom_title_spacings' => array(
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox grid-col-12',
				'title' => esc_html__( 'Add Title Spacings', 'cws-to' ),
				'atts' => 'data-options="e:page_title_spacings;"',
			),
			'page_title_spacings' => array(
				'type' => 'margins',
				'addrowclasses' => 'grid-col-4 two-inputs',
				'value' => array(
					'top' => array('placeholder' => esc_html__( 'Top (in px)', 'cws-to' ), 'value' => '60px'),
					'bottom' => array('placeholder' => esc_html__( 'Bottom (in px)', 'cws-to' ), 'value' => '60px'),
				),
			),
		);

		$this->cws_generate_metabox($post, $mb_attr_all);
	}

	public function mb_post_side_callback( $post ) {
		wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );

		$mb_attr = array(
			'post_title_box_image' => array(
				'title' => esc_html__( 'Image', 'cws-to' ),
				'addrowclasses' => 'hide_label',
				'type' => 'media',
			),
		);

		$this->cws_generate_metabox($post, $mb_attr);
	}

	public function cws_generate_metabox($post, $mb_attr){
		$cws_stored_meta = get_post_meta( $post->ID, 'cws_mb_post' );
		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout((!empty($cws_stored_meta) ? $cws_stored_meta[0] : ''), $mb_attr, 'cws_mb_');
		}
	}

	public function cws_post_metabox_save( $post_id, $post ) {
		if ( in_array($post->post_type, array('post', 'page', 'cws_testimonials', 'cws_portfolio', 'cws_staff', 'cws_classes', 'megamenu_item')) ) {
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
				return;

			if ( !isset( $_POST['mb_nonce']) || !wp_verify_nonce($_POST['mb_nonce'], 'cws_mb_nonce') )
				return;

			if ( !current_user_can( 'edit_post', $post->ID ) )
				return;

			$save_array = array();

			foreach($_POST as $key => $value) {
				if (0 === strpos($key, 'cws_mb_')) {
					if ('on' === $value) {
						$value = '1';
					}
					if (is_array($value)) {
						foreach ($value as $k => $val) {
							if (is_array($val)) {
								$this->cws_remove_dummy($val);
								$save_array[substr($key, 7)][$k] = $val;
							} else {
								$save_array[substr($key, 7)][$k] = esc_html($val);
							}
						}
					} else {
						$save_array[substr($key, 7)] = esc_html($value);
					}
				}
			}
			if (!empty($save_array)) {
				update_post_meta($post_id, 'cws_mb_post', $save_array);
			}
		}
	}

	private function cws_remove_dummy(&$val) {
		if (is_array($val)) {
			if (!$this->cws_is_assoc($val)) {
				// check for dummy key and delete it if there are
				if ('!!!dummy!!!' === $val[0]) { array_shift($val); }
			} else {
				foreach ($val as $key => &$value) {
					$this->cws_remove_dummy($value);
				}
			}
		}
	}

	private function cws_is_assoc(array $array) {
		return count(array_filter(array_keys($array), 'is_string')) > 0;
	}
}
?>