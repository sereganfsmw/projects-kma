<?php

//Get defaults values for Portfolio
function cws_portfolio_defaults( $extra_vars = array() ){
	global $cws_theme_funcs;

	$layout = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_layout'] : '1';
	$pagination_grid = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_pagination_grid'] : 'standard';
	$show_meta = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_show_meta'] : array('title', 'excerpt');
	$def_chars_count = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_chars_count'] : '200';
	$def_chars_count = isset( $def_chars_count ) && is_numeric( $def_chars_count ) ? $def_chars_count : '200';	

	$defaults = array(
		'title'						=> 'Portfolio',
		'title_aligning'			=> 'left',
		'title_filter_row'			=> 'default',
		'display_style'				=> 'grid',
		'layout'					=> $layout,
		'full_width'				=> '',

		//Slider (Swiper)
		'columns_count'				=> '1',
		'slides_in_columns'			=> '1',
		'slides_view'				=> 'column',
		'slides_to_scroll'			=> '1',
		'slider_direction'			=> 'horizontal',
		'auto_height'				=> '',
		'navigation'				=> 'both',
		'pagination_style'			=> 'bullets',
		'dynamic_bullets'			=> '',
		'keyboard_control'			=> '',
		'mousewheel_control'		=> '',
		'item_center'				=> '',
		'spacing_slides'			=> '',
		'free_mode'					=> '',
		'slide_effects'				=> 'slide',
		'autoplay'					=> '',
		'autoplay_speed'			=> '',
		'pause_on_hover'			=> 'yes',
		'parallax'					=> '',
		'loop'						=> '',
		'animation_speed'			=> '',
		//--Slider (Swiper)

		'showcase_height'				=> array(
											'unit' => 'px',
											'size' => 600,
										),
		'showcase_style'				=> 'horizontal',
		'showcase_horizontal_spacings'	=> array(
											'unit' => 'px',
											'size' => 10,
										),
		'showcase_vertical_spacings'	=> array(
											'unit' => 'px',
											'size' => 10,
										),
		'showcase_animation'			=> 'sine',
		'showcase_effect'				=> 'none',
		'showcase_speed'				=> 500,
		'showcase_active_size'			=> array(
											'unit' => '%',
											'size' => 50,
										),

		'filter_by'					=> '',

		'items_count'				=> get_option( 'posts_per_page' ),
		'items_per_page'			=> get_option( 'posts_per_page' ),
		'pagination_grid'			=> $pagination_grid,
		'pagination_aligning'		=> 'center',
		'chars_count'				=> $def_chars_count,
		'show_meta'					=> $show_meta,
		'grid_columns_spacings'		=> array(
										'unit' => 'px',
										'size' => 50,
									),
		'grid_rows_spacings'		=> array(
										'unit' => 'px',
										'size' => 50,
									),
		'crop_images'				=> '',
		'masonry'					=> '',
		'portfolio_style'			=> 'normal',
		'info_pos'					=> 'inside_img',
		'add_divider'				=> '',
		'hover_effect'				=> 'area_link',
		'hover_animation'			=> 'hoverdef',
		'appear_style'				=> 'none',
		'title_color'				=> '#000',
		'title_typography'			=> '',
		'category_color'			=> '#000',
		'category_typography'		=> '',

		//Service params
		'extra_query_args'			=> array(),
		'related_items'				=> '',
		'related_carousel'			=> '',
	);

	if (!empty($extra_vars)){
		$defaults = array_merge($defaults, $extra_vars);
	}

	return $defaults;
}

//Fill atts from function to function
function cws_portfolio_fill_atts( $atts = array() ){
	global $cws_theme_funcs;
	extract( $atts );

	$page_id = get_the_id();
	$page_meta = get_post_meta( $page_id, 'cws_mb_post' );
	$page_meta = isset( $page_meta[0] ) ? $page_meta[0] : array();
	$items_count = !empty( $items_count ) ? (int)$items_count : PHP_INT_MAX;

	if ($display_style == 'filter'){
		$items_per_page = $items_count;
	}

	$def_layout = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( 'portfolio_options' )['def_portfolio_layout'] : '1';
	$def_layout = isset( $def_layout ) ? $def_layout : "";
	$layout = ($layout == "def") ? $def_layout : $layout;

	$sb = $cws_theme_funcs ? $cws_theme_funcs->cws_render_sidebars( $page_id ) : '';
	$sb_layout = isset( $sb['layout_class'] ) ? $sb['layout_class'] : '';

	$fill_atts = array_merge($atts,
		array(
			//Service params
			'page_id'			=> $page_id,
			'page_meta'			=> $page_meta,
			'sb_layout'			=> $sb_layout,

			//Override atts
			'items_count'		=> $items_count,
			'items_per_page'	=> $items_per_page,
			'layout'			=> $layout
		)
	);

	//Set GLOBALS vars
	$GLOBALS['cws_portfolio_atts'] = $fill_atts;

	return $fill_atts;
}

function cws_portfolio_output($atts = array()){
	global $cws_theme_funcs;
	$out = "";

	$extra_vars = array();
	if (!empty($atts['filter_by'])){
		$extra_vars[$atts['filter_by']] = '';
	}

	$defaults = cws_portfolio_defaults($extra_vars); //Get defaults
	$atts = shortcode_atts( $defaults, $atts );
	$fill_atts = cws_portfolio_fill_atts($atts); //Fill atts
	extract( $fill_atts );

	$post_type = "cws_portfolio";
	$section_id = uniqid( 'cws_portfolio_grid_' );
	$paged = get_query_var( 'paged' );
	$home_paged = get_query_var('page');
	if(isset($home_paged) && !empty($home_paged)){
		$paged = empty( $home_paged ) ? 1 : get_query_var('page');
	}
	else{
		$paged = empty( $paged ) ? 1 : $paged;
	}

	$not_in = (1 == $paged) ? array() : get_option( 'sticky_posts' );
	$query_args = array(
		'post_type'			=> $post_type,
		'post_status'		=> 'publish',
		'post__not_in'		=> $not_in
	);
	if ( in_array( $display_style, array( 'grid', 'filter', 'filter_with_ajax' ) ) ){
		$query_args['posts_per_page']	= $items_per_page;
		$query_args['paged']			= $paged;
	}
	else {
		$query_args['nopaging']			= true;
		$query_args['posts_per_page']	= -1;
	}

	//Filter by
	$tax = $fill_atts['filter_by'];
	if (!empty($tax)){
		$filter_terms = $fill_atts[$tax];
	}

	if ( !empty( $filter_terms ) ){
		$query_args['tax_query'] = array(
			array(
				'taxonomy'		=> $tax,
				'field'			=> 'slug',
				'terms'			=> $filter_terms
			)
		);
	}
	//--Filter by

	//Get All terms
	$all_terms = array();
	$rewrite_slug = cws_get_slug('portfolio');
	$get_terms = get_terms( 'cws_portfolio_cat' );
	foreach ($get_terms as $key => $value) {
		$all_terms[] = $value->slug;
	}
	//--Get All terms

	//Terms show in Filters bar
	if (empty($filter_terms)){
		$terms = $all_terms;
	} else {
		$terms = $filter_terms;
	}
	//--Terms show in Filters bar

	$query_args['orderby'] 	= "menu_order date title";
	$query_args['order']	= "ASC";

	$query_args = array_merge( $query_args, $extra_query_args );
	$q = new WP_Query( $query_args );
	$found_posts = $q->found_posts;

	$requested_posts = $found_posts > $items_count ? $items_count : $found_posts;
	$max_paged = $found_posts > $items_count ? ceil( $items_count / $items_per_page ) : ceil( $found_posts / $items_per_page );

	$is_carousel = $display_style == 'carousel' && $requested_posts > $layout;
	$is_filter = in_array( $display_style, array( 'filter', 'filter_with_ajax' ) ) && !empty( $terms ) ? true : false;
	$use_pagination = in_array( $display_style, array( 'grid', 'filter', 'filter_with_ajax' ) ) && $max_paged > 1;
	$dynamic_content = $is_filter || $use_pagination;

	//CAROUSEL
	$data_attr = swiper_carousel($fill_atts, $is_carousel);
	//--CAROUSEL

	if ($display_style == "showcase"){
		$showcase_atts = array(
			'active_size'	=> $showcase_active_size['size'],
		);

		$showcase_atts_str = json_encode($showcase_atts);		
		$data_attr.= " data-showcase-args='".$showcase_atts_str."'";
	}

	$section_class = cws_class([
		'cws_portfolio',
		'posts_grid',
		esc_attr($display_style),
		($display_style != "showcase" && $display_style != "carousel" ? "isotope_init" : ""),
		($display_style != "showcase" && $display_style != "carousel" ? "col-".esc_attr($layout) : ""),
		(in_array( $display_style, array( 'filter' )) ? "css_filter" : ""),
		($dynamic_content ? "dynamic_content" : "" ),
		($appear_style !== 'none' ? "appear_style" : "")
	]);

	$wrapper_class = cws_class([
		'cws_portfolio_wrapper',
		($masonry ? " layout-masonry" : (!$is_carousel && $display_style != "showcase" ? "layout-isotope" : "") )
	]);

	if ($crop_images == '1' || $dynamic_content) {
		$isotope_style = " isotope";
	} else if ($masonry == '1') {
		$isotope_style = " isotope masonry";
	}

	$page_id = isset($GLOBALS['cws_portfolio_atts']['page_id']) ? $GLOBALS['cws_portfolio_atts']['page_id'] : get_the_id();

	$container_class = cws_class([
		$dynamic_content ? "cws_pagination" : '',
		($is_carousel ? "cws_portfolio_carousel" : "cws_portfolio_grid" . ( ( in_array( $layout, array( "2", "3", "4", "5" ) ) || $dynamic_content ) && !cws_is_single($page_id) ? $isotope_style : "" )),
		($related_carousel ? 'related_portfolio_carousel cws_owl_carousel' : ''),
		($display_style == "showcase" ? "showcase_wrapper showcase-".esc_attr($showcase_style)." showcase-anim-".esc_attr($showcase_animation).($showcase_effect != 'none' ? " showcase-effect-".esc_attr($showcase_effect) : '') : '')
	]);

ob_start ();

	echo "<section id='".esc_attr($section_id)."' class='".(!empty($section_class) ? $section_class : "")."'>";
		$filter_vals = array();
		if ( isset($related_carousel) && $related_carousel ){
			echo "<div class='widget_header clearfix'>";
				echo !empty( $title ) ? "<h2 class='widgettitle'>" . esc_html( $title ) . "</h2>" : "";
			echo "</div>";
		} else if ( $is_filter && count( $terms ) > 1 ){

			echo $title_filter_row == 'default' ? "<div class='container'>" : '';
			foreach ( $terms as $term ) {
				if ( empty( $term ) ) continue;
				$term_obj = get_term_by( 'slug', $term, 'cws_portfolio_cat' );
				if ( empty( $term_obj ) ) continue;

				$term_name = $term_obj->name;
				$filter_vals[$term_obj->slug] = $term_name;
			}

			if ( $filter_vals > 1 ){
				wp_enqueue_script( 'tweenmax' );
				echo posts_filters_nav ( $filter_vals, $title);
			}

			echo $title_filter_row == 'default' ? "</div>" : '';
		}
		else {
			echo !empty( $title ) ? "<h2 class='widgettitle'>" . esc_html( $title ). "</h2>" : "";
		}		
		echo "<div class='".(!empty($wrapper_class) ? $wrapper_class : "")."'>";
			echo "<div class='".(!empty($container_class) ? $container_class : "")."'" . (!empty($data_attr) ? $data_attr : "") . ">";
				if (!$is_carousel && $display_style != "showcase" && !cws_is_single($page_id)) {
					echo "<div class='grid-sizer'></div>"; //For Isotope grid
				}

				if ($is_carousel){
					echo "<div class='swiper-container'>";
						echo "<div class='swiper-wrapper'>";
				}				
				
				cws_portfolio_posts($q);

				if ($is_carousel){
						echo "</div>"; //swiper-wrapper

						if ($navigation == 'both' || $navigation == 'pagintaion'){
							if ($pagination_style == 'scrollbar'){
								echo "<div class='swiper-scrollbar'></div>";
							} else {
								echo "<div class='swiper-pagination'></div>";
							}
						}

					echo "</div>"; //swiper-container

					if ($navigation == 'both' || $navigation == 'arrows'){
						echo "<div class='swiper-button-prev'></div>";
						echo "<div class='swiper-button-next'></div>";
					}
				}	
				unset($GLOBALS['cws_portfolio_atts']);
			echo "</div>";
			if ( $dynamic_content ){
				cws_loader_html();
			}
		if ( isset($related_carousel) && $related_carousel ){
			echo "<div class='carousel_nav'>";
				echo "<span class='prev'>";
					if(!empty($related_items)){
						esc_html_e("Previous", 'cryptop');
					}
				echo "</span>";
				echo "<span class='next'>";
					if(!empty($related_items)){
						esc_html_e("Next", 'cryptop');
					}
				echo "</span>";
			echo "</div>";
		}			
		echo "</div>";

		if ( $use_pagination ){
			if ( $pagination_grid == 'load_more' ){
				echo cws_load_more ();
			}
			else if ($pagination_grid == 'load_on_scroll'){
				echo cws_load_more (true);
			}				
			else if ($pagination_grid == 'ajax'){
				echo cws_pagination($paged, $max_paged, true);
			}
			else if ($pagination_grid == 'standard'){
				echo cws_pagination($paged, $max_paged, false);
			}
		}
		if ( $dynamic_content ){

			//AJAX data
			$ajax_data = array_merge($fill_atts,
				array(
					//Service params
					'post_type'			=> 'cws_portfolio',
					'section_id'		=> $section_id,
					'max_paged'			=> $max_paged,
					'page'				=> $paged,
					'filter_vals'		=> $filter_vals,
				)
			);

			$ajax_data_str = json_encode( $ajax_data );
			echo "<form id='".esc_attr($section_id)."_data' class='ajax_data_form cws_portfolio_ajax_data_form'>";
				echo "<input type='hidden' id='".esc_attr($section_id)."_ajax_data' class='ajax_data cws_portfolio_ajax_data' name='".esc_attr($section_id)."_ajax_data' value='".$ajax_data_str."' />";
			echo "</form>";	
		}
	echo "</section>";
	$out = ob_get_clean();
	return $out;
}

function cws_portfolio_posts ( $q = null ){
	if ( !isset( $q ) ) return;
	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();
	extract( $grid_atts );

	$paged = $q->query_vars['paged'];
	if ( $paged == 0 && $items_count < $q->post_count ){
		$post_count = $items_count;
	}
	else
	{
		$ppp = $q->query_vars['posts_per_page'];
		$posts_left = $items_count - ( $paged - 1 ) * $ppp;
		$post_count = $posts_left < $ppp ? $posts_left : $q->post_count;
	}
	if ( $q->have_posts() ):
		ob_start();
		while( $q->have_posts() && $q->current_post < $post_count - 1 ):
			$q->the_post();
			cws_portfolio_article ();
		endwhile;
		wp_reset_postdata();
		ob_end_flush();
	endif;
}

function cws_portfolio_article (){
	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();
	extract( $grid_atts );

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();	
	$portfolio_type = isset( $post_meta['portfolio_type'] ) ? $post_meta['portfolio_type'] : '';
	$video_type = isset( $post_meta['video_type'] ) ? $post_meta['video_type'] : '';	
	$slider_type = isset( $post_meta['slider_type'] ) ? $post_meta['slider_type'] : '';	
	$popup = isset( $post_meta['video_type']['popup'] ) ? $post_meta['video_type']['popup'] : '';
	$popup = $popup == 'true' ? true : false;
	$enable_hover = isset( $post_meta['enable_hover'] ) ? $post_meta['enable_hover'] : false;
	$video_on = ($portfolio_type == 'video' && $video_type['on_grid'] == 1 && !$popup);
	$slider_on = ($portfolio_type == 'slider' && $slider_type['on_grid'] == 1);
	//--Meta vars

	$hover_none = ($hover_animation == 'none');
	$hover_none_link = ($hover_animation == 'none_link');
	$is_carousel = $display_style == 'carousel';

	//CSS Filter by class
	$name_slug = array();
	if($display_style == 'filter'){
		foreach (get_the_terms($pid, 'cws_portfolio_cat') as $key => $value) {
			$name_slug[] = $value->slug ;
		}
		$name_slug[] = 'tax_portfolio';
	}
	//--CSS Filter by class

	$article_class = cws_class(array_merge([
		'item',
		'portfolio_item',
		'clearfix',
		($is_carousel ? 'swiper-slide' : ''),
		($info_pos == 'under_img' ? "under_img" : ""),
		(!empty($add_divider) ? "add_divider" : ""),
		($enable_hover == 0 ? "hover_none" : ""),
		(( ($display_style == 'grid' || $display_style == 'filter' ||  $display_style == 'filter_with_ajax' ) && !empty($hover_animation) ) ? $hover_animation : '')
	], $name_slug ));

	$crop_images = ($crop_images == 'yes');
	$masonry = ($masonry == 'yes');
	
	$thumbnail_props = has_post_thumbnail() ? wp_get_attachment_image_src(get_post_thumbnail_id( ),'full') : array();
	$thumbnail = !empty( $thumbnail_props ) ? $thumbnail_props[0] : '';
	$real_thumbnail_dims = array();
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[1] ) ) $real_thumbnail_dims['width'] = $thumbnail_props[1];
	if ( !empty(  $thumbnail_props ) && isset( $thumbnail_props[2] ) ) $real_thumbnail_dims['height'] = $thumbnail_props[2];
	
	$thumbnail_dims = cws_portfolio_thumb_dims( false, $real_thumbnail_dims );

	$isotope = array('crop_images' => $crop_images , 'masonry' => $masonry , 'full_width' => $full_width, 'sb_layout' => $sb_layout , 'columns' => $layout, 'portfolio_style' => $portfolio_style );
	if ($masonry){
		extract(cws_portfolio_masonry($thumbnail, $thumbnail_dims, $post_meta, $isotope));
	}

	$data_col = !empty($isotope_col_count) ? "data-masonry-col='".esc_attr($isotope_col_count)."'" : "";
	$data_line = !empty($isotope_line_count) ? "data-masonry-line='".esc_attr($isotope_line_count)."'" : "";
	$data_anim = '';

	if ($display_style == 'grid' || $display_style == 'filter' ||  $display_style == 'filter_with_ajax' || $display_style == 'showcase') {
		$data_anim = ($appear_style !== 'none') ? "data-item-anim='".esc_attr($appear_style)."'" : ""; 
	}
	$uniq_pid = uniqid( "portfolio_post_" );

	$pid = get_the_id();
	if ( empty( $show_meta ) || empty( $show_meta[0] ) ){
		if ( has_post_thumbnail( $pid ) ){
			echo "<article id='".esc_attr($uniq_pid)."' class='".(!empty($article_class) ? $article_class : "")."' $data_col $data_line $data_anim>";
				echo "<div class='item_content'>";	
					cws_portfolio_media();
				echo "</div>";
			echo "</article>";
		}		
	}
	else {
		echo "<article id='".esc_attr($uniq_pid)."' class='".(!empty($article_class) ? $article_class : "")."' $data_col $data_line $data_anim>";
			echo "<div class='item_content'>";
				cws_portfolio_media();
				if ($info_pos == 'inside_img'){
					if ($hover_none_link && $enable_hover == 1) {
						cws_portfolio_hover();
					} else if (!$video_on && !$hover_none && $enable_hover == 1 && !$slider_on) {
						echo "<div class='cws_portfolio_container'>";
							cws_portfolio_hover();
							echo "<div class='cws_portfolio_description inside_image'>";
								cws_portfolio_title();
								cws_portfolio_terms();
								cws_portfolio_content();
							echo "</div>";
						echo "</div>";
						
					}
				} else if ($info_pos == 'under_img') {
					echo "<div class='cws_portfolio_description under_image'>";
						cws_portfolio_title();
						cws_portfolio_terms();
						cws_portfolio_content();
					echo "</div>";
				}
				if ( $display_style == 'showcase' ) {
					echo "<div class='side_load'>";
						echo "<div class='load_bg'></div>";
						echo "<div class='load_wrap'>";
							cws_portfolio_title();
						echo "</div>";
					echo "</div>";
				}
			echo "</div>";
		echo "</article>";
	}
}

function cws_portfolio_media (){
	global $cws_theme_funcs;

	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();
	extract( $grid_atts );

	$pid = get_the_id();
	$permalink = get_the_permalink( $pid );
	$post_url = esc_url($permalink);

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$portfolio_type = isset( $post_meta['portfolio_type'] ) ? $post_meta['portfolio_type'] : '';
	$slider_type = isset( $post_meta['slider_type'] ) ? $post_meta['slider_type'] : '';
	$video_type = isset( $post_meta['video_type'] ) ? $post_meta['video_type'] : '';
	$enable_hover = isset( $post_meta['enable_hover'] ) ? $post_meta['enable_hover'] : false;
	$custom_url = isset( $post_meta['link_options_url'] ) ? $post_meta['link_options_url'] : "";
	$fancybox = isset( $post_meta['link_options_fancybox'] ) ? $post_meta['link_options_fancybox'] : false;
	$video_t = isset( $post_meta['video_type']['video_t'] ) ? $post_meta['video_type']['video_t'] : '';
	$video = isset( $post_meta['video_type'][$video_t . '_t']['url'] ) ? $post_meta['video_type'][$video_t . '_t']['url'] : '';
	$video_img = isset( $post_meta['video_type']['img'] ) ? $post_meta['video_type']['img'] : '';
	$popup_grid = isset( $post_meta['video_type']['popup_grid'] ) ? $post_meta['video_type']['popup_grid'] : '';
	$popup_grid = (bool)$popup_grid;	
	$popup = isset( $post_meta['video_type']['popup'] ) ? $post_meta['video_type']['popup'] : '';
	$popup = (bool)$popup;
	//--Meta vars

	$crop_images = ($crop_images == 'yes') ? true : false;
	$thumbnail_props = has_post_thumbnail() ? wp_get_attachment_image_src(get_post_thumbnail_id( ),'full') : array();
	$thumbnail = !empty( $thumbnail_props ) ? $thumbnail_props[0] : '';
	$real_thumbnail_dims = array();
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[1] ) && !$crop_images) $real_thumbnail_dims['width'] = $thumbnail_props[1];
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[2] ) && !$crop_images) $real_thumbnail_dims['height'] = $thumbnail_props[2];

	$thumbnail_dims = cws_portfolio_thumb_dims( false, $real_thumbnail_dims );

	if (!empty($thumbnail)) {
		if($crop_images){
			$thumbnail_dims['crop'] = array(
				$cws_theme_funcs->cws_get_option( "crop_x" ),
				$cws_theme_funcs->cws_get_option( "crop_y" )
			);
			if ($thumbnail_dims['height'] < $thumbnail_dims['width']){
				$thumbnail_dims['width'] = $thumbnail_dims['height'];
			}
		}

		$thumb_obj = cws_thumb( get_post_thumbnail_id(), $thumbnail_dims, false );
	}
	$thumb_url = isset( $thumb_obj[0] ) ? esc_url($thumb_obj[0]) : "";
	$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;

	$link_fancy = "";
	$link_fancy .= $fancybox ? "class='fancy fa flaticon-magnifying-glass' href='".esc_url($thumbnail)."'" : "";

	$video_on = $portfolio_type == 'video' && $video_type['on_grid'] == 1 && !$popup;
	$slider_on = $portfolio_type == 'slider' && $slider_type['on_grid'] == 1;
	$hover_none = ($hover_animation == 'none');
	$hover_none_link = ($hover_animation == 'none_link');
	$link_url = "";
	$link_url .= $custom_url ? $custom_url : $post_url;

	$page_url = getUrl();

	echo "<div class='cws_portfolio_media post_media'>";
		if ($portfolio_type == 'slider' && $slider_type['on_grid'] == 1) {
			if ($display_style == 'showcase') {
				echo "<div class='pic'>";
					if ( $retina_thumb ) {
						echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
					}
					else{
						echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
					}
					echo "<a href='".esc_url($permalink)."' class='links' rel='".esc_attr($pid)."' data-url-reload='".esc_url($page_url)."'></a>";	
				echo "</div>";
				if ($enable_hover == 1 && !$hover_none) {
					echo "<div class='cws_portfolio_container'>";
						cws_portfolio_hover ();
						if ($info_pos == 'inside_img') {
							echo "<div class='desc_img'>";
								cws_portfolio_title ();
								cws_portfolio_terms ();
								cws_portfolio_content ();
							echo "</div>";
						}
					echo "</div>";
				}
			} else if ( !empty( $slider_type ) ) {
				$match = preg_match_all("/\d+/",$slider_type['slider_gall'],$images);
				if ($match){
					$images = $images[0];
					$image_srcs = array();
					foreach ( $images as $image ) {
						$image_temp = array();
						$image_src = wp_get_attachment_image_src($image,'full');
						if ($image_src){
							$image_temp = array('url' => $image_src[0], 'id' => $image);
							array_push( $image_srcs, $image_temp );										
						}
					}
					$thumb_media = $some_media = (count( $image_srcs ) > 0) ? true : false;
					$carousel = (count($image_srcs) > 1) ? true : false;
					$gallery_id = uniqid( 'cws-gallery-' );
					echo $carousel ? "<a class='gallery_post_carousel_nav carousel_nav prev'><span></span></a>
										<a class='gallery_post_carousel_nav carousel_nav next'><span></span></a>" : "";
					if ($enable_hover == 1 && !$hover_none && !$hover_none_link) {
						echo "<div class='cws_portfolio_container'>";
							cws_portfolio_hover ();
							if ($info_pos == 'inside_img') {
								echo "<div class='cws_portfolio_description inside_image'>";
									cws_portfolio_title ();
									cws_portfolio_terms ();
									cws_portfolio_content ();
								echo "</div>";
							}
						echo "</div>";
					}

					$carousel_atts = array(
						'nav'			=> true
					);
					$carousel_atts_str = json_encode($carousel_atts);
					$data_attr = " data-owl-args='".$carousel_atts_str."'";	
									
					echo  $carousel ? "<div class='gallery_post_carousel cws_owl_carousel' ".$data_attr.">" : '';
					foreach ( $image_srcs as $image_src ) {
						$img_obj = cws_thumb( $image_src['id'], $thumbnail_dims , false );
						$img_url = isset( $img_obj[0] ) ? esc_url( $img_obj[0] ) : "";
						$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;

						if ( !empty($img_url) ) {
							echo "<div class='pic'>";
							if ( $retina_thumb ) {
								echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
							}
							else{
								echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
							}
							echo "</div>";
						}
					}
					echo  $carousel ? "</div>" : '';
				}
			}
		} else if ($portfolio_type == 'video' && $video_type['on_grid'] == 1) {
			if ($display_style == 'showcase') {
				echo "<div class='pic'>";
					if ( $retina_thumb ) {
						echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
					}
					else{
						echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
					}
					echo "<a href='".esc_url($permalink)."' class='links' rel='".esc_attr($pid)."' data-url-reload='".esc_url($page_url)."'></a>";	
				echo "</div>";
				if ($enable_hover == 1 && !$hover_none && !$hover_none_link) {
					echo "<div class='cws_portfolio_container'>";
						cws_portfolio_hover ();
						if ($info_pos == 'inside_img') {
							echo "<div class='cws_portfolio_description inside_image'>";
							cws_portfolio_title ();
							cws_portfolio_terms ();
							cws_portfolio_content ();
							echo "</div>";
						}
					echo "</div>";
				}
			} else {
				global $wp_filesystem;
				if ( !empty($video_img['src']) ) {
					$video_img = !empty($video_img['src']) ? $video_img['src'] : '';
					$img_obj = cws_thumb( $video_img, $thumbnail_dims , false );
					$img_url = isset( $img_obj[0] ) ? esc_url( $img_obj[0] ) : "";
				}
				preg_match('@[^/]*$@', $video, $video_link);
				$clear_url = array("?", "&amp", "watchv=");
				$video_id = str_replace($clear_url, '', preg_replace('/[^?][a-z]*=\w+/', '', $video_link[0]));
				$video_url = str_replace('watch?v=', '', $video_link[0]);
				if ($video_t == 'youtube') {
					$thumbnail_img = "http://img.youtube.com/vi/".esc_attr($video_id)."/maxresdefault.jpg";
					$link = "http://www.youtube.com/embed/";
				} else if ($video_t == 'vimeo') {
					$json = json_decode($wp_filesystem->get_contents("https://vimeo.com/api/oembed.json?url=".$video));
					$vimeo_id = !empty($json->video_id) ? $json->video_id : '';
					$thumbnail_img = !empty($json->thumbnail_url) ? $json->thumbnail_url : '';
					$link = "https://player.vimeo.com/video/";
					$video_url = ($video_id != $vimeo_id ? str_replace($video_id, $vimeo_id, $video_url) : $video_url);
				}
				$embed_link = $link.esc_attr($video_url) . ($video_id != $video_url ? '&amp;' : '?') . "autoplay=1";
				$img_url = !empty($thumb_url) ? $thumb_url : $thumbnail_img;
				if (!$popup_grid) {
				 	echo "<div class='video'>";
				}
				if ($popup_grid) {
					echo "<div class='cover_img'>";
						if ( $retina_thumb ) {
							echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
						}
						else{
							echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
						}
						if ($enable_hover == 1 && !$hover_none && !$hover_none_link) {
							echo "<div class='cws_portfolio_container'>";
								$links_html = "<a href='$embed_link' class='links video fa fa-play-circle-o fancy fancybox.iframe'></a>";
								cws_portfolio_hover ($embed_link, $links_html);
								if ($info_pos == 'inside_img') {
									echo "<div class='cws_portfolio_description inside_image'>";
										cws_portfolio_title();
										cws_portfolio_terms();
										cws_portfolio_content();
									echo "</div>";
								}
							echo "</div>";
						}
					echo "</div>";
				} else {
					echo apply_filters('the_content',"[embed width='" . $thumbnail_dims['width'] . "']" .($video_t == 'youtube' ? 'https://youtu.be/'.$video_id : $video ) . "[/embed]");
				}
				if (!$popup_grid) {
				 	echo "</div>";
				}
			}
		} else {
			echo "<div class='pic'>";
				if ( $retina_thumb ) {
					echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
				}
				else{
					echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
				}
				if ( $display_style == 'showcase' ) {
					echo "<a href='".esc_url($permalink)."' class='links' rel='".esc_attr($pid)."' data-url-reload='".esc_url($page_url)."'></a>";
				} 
			echo "</div>";
		}
		if ($hover_none_link && $enable_hover == 1) {
			cws_portfolio_hover();
		} else if ($info_pos == 'under_img' && $enable_hover == 1 && !$video_on && !$hover_none && !$slider_on) {
			echo "<div class='cws_portfolio_container'>";
				cws_portfolio_hover ();
			echo "</div>";
		}
	echo "</div>";
}

function cws_portfolio_hover ( $video_link = '', $links_html = '' ){
	$pid = get_the_id ();
	$post_url = esc_url(get_the_permalink());

	$page_id = isset($GLOBALS['cws_portfolio_atts']['page_id']) ? $GLOBALS['cws_portfolio_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();	
	extract( $grid_atts );

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$custom_url = isset( $post_meta['link_options_url'] ) ? $post_meta['link_options_url'] : "";
	$link_options_fancybox = isset( $post_meta['link_options_fancybox'] ) ? $post_meta['link_options_fancybox'] : false;
	$link_options_single = isset( $post_meta['link_options_single'] ) ? $post_meta['link_options_single'] : false;
	$enable_hover = isset( $post_meta['enable_hover'] ) ? $post_meta['enable_hover'] : false;
	//--Meta vars

	$single = cws_is_single($page_id);
	$link_url = $custom_url ? $custom_url : $post_url;
	$thumbnail_props = has_post_thumbnail() ? wp_get_attachment_image_src(get_post_thumbnail_id(), 'full') : array();
	$thumbnail = !empty( $thumbnail_props ) ? $thumbnail_props[0] : '';
	$thumbnail = !empty($video_link) ? $video_link : $thumbnail;
	$hover_effect = !empty($hover_effect) ? $hover_effect : array();
	$single = $hover_effect == 'single_link';
	$popup = $hover_effect == 'popup_link';
	$area = $hover_effect == 'area_link';

	if($single){
		$single = $link_options_single == 1 ? true : false;
	}

	if (!$popup) {
		if($single){
			$popup = $link_options_fancybox == 1 ? true : false;
		}
	}

	$hover_none = ($hover_animation == 'none');
	$hover_none_link = ($hover_animation == 'none_link');
	if ($display_style != 'showcase') {
		if (!$hover_none && !$hover_none_link) {
			echo "<div class='links_wrap'>";
			if ($single || $popup) {
				if ( $single && $link_options_single == 1 ){
					echo "<a href='".esc_url($link_url)."' class='cws_fa flaticon-link-symbol links'></a>";
				} 
				if(!empty($links_html)){
					echo $links_html;
				}
				if ( $popup && $link_options_fancybox == 1 ){
					echo "<a href='" . esc_url($thumbnail) . "' class='cws_fa flaticon-arrows-1 links fancy" . (!empty($video_link) ? ' fancybox.iframe' : '') . "'></a>";			
				} 				
			}
			
			echo "</div>";
			if ( $area ){
				echo "<a href='" . esc_url(!empty($video_link) ? $video_link : $link_url) . "' class='links area" . (!empty($video_link) ? ' fancy fancybox.iframe' : '') . "'></a>";
			} 
			echo "<div class='hover-effect'></div>";
		} else if ($hover_none_link) {
			echo "<a href='".esc_url($link_url)."' class='links area'></a>";
		}
	}
}

function cws_portfolio_title(){
	$pid = get_the_id ();

	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();	
	extract( $grid_atts );

	if ( in_array( 'title', $show_meta ) ){
		$title = get_the_title();
		$permalink = get_the_permalink();
		echo !empty( $title ) ?	"<h3 class='cws_portfolio_title post_title'><a href='".esc_url($permalink)."'>" . $title . "</a></h3>" : "";	
	}
	
}
function cws_portfolio_content(){
	$pid = get_the_id ();
	$post = get_post( $pid );

	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();
	extract( $grid_atts );

	$out = "";
	if ( in_array( 'excerpt', $show_meta ) ){
		$chars_count = !empty($chars_count) ? $chars_count : 0;
		$out = !empty( $post->post_excerpt ) ? $post->post_excerpt : $post->post_content;
		$out = trim( preg_replace( "/[\s]{2,}/", " ", strip_shortcodes( strip_tags( $out ) ) ) );
		$out = wptexturize( $out );
		if (!empty($chars_count)){
			$out = substr( $out, 0, $chars_count );
		}
		echo !empty( $out ) ? "<div class='cws_portfolio_content post_content'>$out</div>" : "";
	}
}
function cws_portfolio_terms(){
	$pid = get_the_id ();

	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();	
	extract( $grid_atts );	

	if ( in_array( 'cats', $show_meta ) ){
		$p_category_terms = wp_get_post_terms( $pid, 'cws_portfolio_cat' );
		$p_cats = "";
		for ( $i=0; $i < count( $p_category_terms ); $i++ ){
			$p_category_term = $p_category_terms[$i];
			$p_cat_permalink = get_term_link( $p_category_term->term_id, 'cws_portfolio_cat' );
			$p_cat_name = $p_category_term->name;
			$p_cats .= "<a href='".esc_url($p_cat_permalink)."'>".esc_html($p_cat_name)."</a>";
			$p_cats .= $i < count( $p_category_terms ) - 1 ? esc_html__( ",&#x20;", 'cws-essentials' ) : "";
		}	
		echo !empty($p_cats) ? "<div class='cws_portfolio_terms post_terms'>&#x20;".$p_cats."</div>" : "";
	}	
}

// =====================Portfolio Single=====================
function cws_portfolio_single_article ($pid = null){
	$pid = !empty($pid) ? $pid : get_the_id ();

	//Meta vars
	$post_meta = get_post_meta( $pid, 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$full_width = isset( $post_meta['full_width'] ) ? $post_meta['full_width'] : false;

	$p_type = isset( $post_meta['p_type'] ) ? $post_meta['p_type'] : '';
	$gallery_type = isset( $post_meta['gallery_type'] ) ? $post_meta['gallery_type'] : '';
	$slider_type = isset( $post_meta['slider_type'] ) ? $post_meta['slider_type'] : '';
	$rev_slider_type = isset( $post_meta['rev_slider_type'] ) ? $post_meta['rev_slider_type'] : '';
	$video_type = isset( $post_meta['video_type'] ) ? $post_meta['video_type'] : '';
	$decr_pos = isset( $post_meta['decr_pos'] ) ? $post_meta['decr_pos'] : '';
	$cont_width = isset( $post_meta['cont_width'] ) ? $post_meta['cont_width'] : '';
	//--Meta vars

	switch ($cont_width) {
		case '25':
			$media_width = '75';
			break;
		case '33':
			$media_width = '66';
			break;
		case '50':
			$media_width = '50';
			break;
		case '66':
			$media_width = '33';
			break;
	}

	//Classes
	$article_class = cws_class([
		'item',
		'portfolio_item',
		'clearfix',
		'post_single',
		(!empty($p_type) ? $p_type : ''),
		(!empty($decr_pos) ? $decr_pos : ''),
		(!empty($decr_pos) && $decr_pos !== 'bot' && !$full_width ? 'flex_col' : ''),
		($decr_pos == 'left' || $decr_pos == 'left_s' ? 'reverse' : '')
	]);

	$description_class = cws_class([
		'cws_portfolio_description',
		'single_description',
		($decr_pos == 'left_s' || $decr_pos == 'right_s' ? 'sticky_desc' : '')
	]);
	//--Classes

	//Post Parts
	ob_start();
		cws_portfolio_single_media($pid);
	$media_part = ob_get_clean();

	ob_start();
	echo "<div class='".esc_attr($description_class)."'>";
		cws_portfolio_single_title($pid);
		cws_portfolio_single_terms($pid);
		cws_portfolio_single_content($pid);
	echo "</div>";
	$description_part = ob_get_clean();
	//--Post Parts

	echo "<article id='cws_portfolio_post_".esc_attr($pid)."' class='" . esc_attr($article_class) . "'>";

		if (!$full_width) {
			if ($decr_pos == 'bot') {
				echo !empty($media_part) ? $media_part : '';
				echo !empty($description_part) ? $description_part : '';
			} else {
				echo "<div class='single_col single_col_" . (!empty($media_width) ? esc_attr($media_width) : '') . "'>";
					echo !empty($media_part) ? $media_part : '';
				echo "</div>";
				echo "<div class='single_col single_col_" . esc_attr($cont_width) . "'>";
					echo !empty($description_part) ? $description_part : '';
				echo "</div>";
			}
		} else {
			echo sprintf("%s", $description_part);
		}

		cws_page_links();
	echo "</article>";	
}

function cws_portfolio_single_media ($pid_ajax = null , $ajax_width = ''){
	$post_url = esc_url(get_the_permalink($pid_ajax));

	//Real image dimensions
	$thumbnail_props = has_post_thumbnail( $pid_ajax ) ? wp_get_attachment_image_src(get_post_thumbnail_id( $pid_ajax ),'full') : array();
	$thumbnail = !empty( $thumbnail_props ) ? $thumbnail_props[0] : '';
	$real_thumbnail_dims = array();
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[1] ) ) $real_thumbnail_dims['width'] = $thumbnail_props[1];
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[2] ) ) $real_thumbnail_dims['height'] = $thumbnail_props[2];
	//Real image dimensions

	$thumbnail_dims = cws_portfolio_thumb_dims( false, $real_thumbnail_dims );
	$crop_thumb = isset( $thumbnail_dims['width'] ) && $thumbnail_dims['width'] > 0;

	//Meta vars
	$post_meta = get_post_meta( ($pid_ajax ? $pid_ajax : get_the_ID()), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$portfolio_type = isset( $post_meta['portfolio_type'] ) ? $post_meta['portfolio_type'] : '';
	$full_width = isset( $post_meta['full_width'] ) ? $post_meta['full_width'] : false;
	$enable_hover = isset( $post_meta['enable_hover'] ) ? $post_meta['enable_hover'] : false;
	$custom_url = isset( $post_meta['link_options_url'] ) ? $post_meta['link_options_url'] : "";
	$fancybox = isset( $post_meta['link_options_fancybox'] ) ? $post_meta['link_options_fancybox'] : false;
	$video_t = isset( $post_meta['video_type']['video_t'] ) ? $post_meta['video_type']['video_t'] : '';
	$video = isset( $post_meta['video_type'][$video_t . '_t']['url'] ) ? $post_meta['video_type'][$video_t . '_t']['url'] : '';
	$video_img = isset( $post_meta['video_type']['img'] ) ? $post_meta['video_type']['img'] : '';
	$popup = isset( $post_meta['video_type']['popup'] ) ? $post_meta['video_type']['popup'] : '';
	$popup = (bool)$popup;
	//--Meta vars

	if (!empty($ajax_width)){
		$thumbnail_dims['width'] = $ajax_width;
		$thumbnail_dims['height'] = 0;
	}

	if (!empty($thumbnail)){
		$thumb_obj = cws_thumb( get_post_thumbnail_id( $pid_ajax ), $thumbnail_dims, $thumbnail );
	}
	$thumb_url = isset( $thumb_obj[0] ) ? esc_url($thumb_obj[0]) : "";
	$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;

	$link_atts = "";
	$link_url = $custom_url ? $custom_url : $thumbnail;
	$link_icon = $fancybox ? ( $custom_url ? 'magic' : 'plus' ) : 'share';
	$link_class = $fancybox ? "fancy fa fa-".esc_attr($link_icon) : "fa fa-".esc_attr($link_icon);
	$link_atts .= !empty( $link_class ) ? " class='".esc_attr($link_class)."'" : "";
	$link_atts .= !empty( $link_url ) ? " href='".esc_url($link_url)."'" : "";
	$link_atts .= !$fancybox ? " target='_blank'" : "";
	$link_atts .= $fancybox && $custom_url ? " data-fancybox-type='iframe'" : "";
		echo "<div class='cws_portfolio_media post_media single_media'>";
				switch ($portfolio_type) {
					case 'image':
						if ( !empty( $thumb_url ) ){
							echo "<div class='pic" . ( !$enable_hover || ( !$crop_thumb && !$custom_url ) ? " wth_hover" : "" ) . "'>";
							if ( $retina_thumb ) {
								echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
							}
							else {
								echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
							}
							echo "</div>";
						}
						break;
					case 'slider':
						$slider_type = isset( $post_meta['slider_type'] ) ? $post_meta['slider_type'] : '';
						if ( !empty( $slider_type) ) {
							$match = preg_match_all("/\d+/",$slider_type['slider_gall'],$images);
							if ($match){
								$images = $images[0];
								$image_srcs = array();
								foreach ( $images as $image ) {
									$image_temp = array();
									$image_src = wp_get_attachment_image_src($image,'full');
									if ($image_src){
										$image_temp = array('url' => $image_src[0], 'id' => $image);
										array_push( $image_srcs, $image_temp );										
									}
								}
								$thumb_media = $some_media = count( $image_srcs ) > 0 ? true : false;
								$carousel = count($image_srcs) > 1 ? true : false;
								$gallery_id = uniqid( 'cws-gallery-' );

								$carousel_atts = array(
									'nav'			=> true
								);
								$carousel_atts_str = json_encode($carousel_atts);
								$data_attr = " data-owl-args='".$carousel_atts_str."'";

								echo $carousel ? "<a class='gallery_post_carousel_nav carousel_nav prev'><span></span></a>
													<a class='gallery_post_carousel_nav carousel_nav next'><span></span></a>
													<div class='gallery_post_carousel cws_owl_carousel' ".$data_attr.">" : '';
								foreach ( $image_srcs as $image_src ) {
									$img_obj = cws_thumb( $image_src['id'], $thumbnail_dims , false );
									$img_url = isset( $img_obj[0] ) ? esc_url( $img_obj[0] ) : "";
									$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;

									if ( !empty($img_url) ) {
										echo "<div class='pic'>";
										if ( $retina_thumb ) {
											echo "<img src='".esc_url($img_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
										}
										else{
											echo "<img src='".esc_url($img_url)."' data-no-retina alt />";
										}
										echo "</div>";
									}
								}
								echo  $carousel ? "</div>" : '';
							}
						}
						break;
					case 'rev_slider':
						get_template_part( 'slider' );
						break;
					case 'gallery':
						portfolio_filter_gallery();
						remove_all_filters('post_gallery', 10);
						echo do_shortcode($post_meta['gallery_type']['gallery']);
						remove_filter('post_gallery', 'single_gallery', 11, 3);
						break;
					case 'video':
						global $wp_filesystem;
						preg_match('@[^/]*$@', $video, $video_link);
						$clear_url = array("?", "&amp", "watchv=");
						$video_id = str_replace($clear_url, '', preg_replace('/[^?][a-z]*=\w+/', '', $video_link[0]));
						$video_url = str_replace('watch?v=', '', $video_link[0]);
						if ($video_t == 'youtube') {
							$thumbnail_img = "http://img.youtube.com/vi/".esc_attr($video_id)."/maxresdefault.jpg";
							$link = "http://www.youtube.com/embed/";
						} else if ($video_t == 'vimeo') {
							$json = json_decode($wp_filesystem->get_contents("https://vimeo.com/api/oembed.json?url=".$video));
							$vimeo_id = !empty($json->video_id) ? $json->video_id : '';
							$thumbnail_img = !empty($json->thumbnail_url) ? $json->thumbnail_url : '';
							$link = "https://player.vimeo.com/video/";
							$video_url = ($video_id != $vimeo_id ? str_replace($video_id, $vimeo_id, $video_url) : $video_url);
						}
						$embed_link = $link.esc_attr($video_url) . ($video_id != $video_url ? '&amp;' : '?') . "autoplay=1";
						$img_url = !empty($thumb_url) ? $thumb_url : $thumbnail_img;
						echo "<div class='video'>";
						if ($popup) {
							echo "<div class='cover_img'>";
								if ( $retina_thumb ) {
									echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
								} else {
									echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
								}
								echo "<div class='cws_portfolio_container'>";
									echo "<div class='hover-effect'></div>";
									echo "<a href='".esc_url($embed_link)."' class='links video fa fa-play-circle-o fancy fancybox.iframe'></a>";
									echo "<a class='links area fancy fancybox.iframe' href='".esc_url($embed_link)."'></a>";
								echo "</div>";
							echo "</div>";
						} else {
							if (!empty($pid_ajax)){
								global $wp_embed; 
								$wp_embed->post_ID = $pid_ajax; 
								echo $wp_embed->run_shortcode( apply_filters('the_content',"[embed width='" . $thumbnail_dims['width'] . "']" .($video_t == 'youtube' ? 'https://youtu.be/'.$video_id : $video ) . "[/embed]") );
							} else {
								echo apply_filters('the_content',"[embed width='" . $thumbnail_dims['width'] . "']" .($video_t == 'youtube' ? 'https://youtu.be/'.$video_id : $video ) . "[/embed]");
							}
						}
						echo "</div>";
						break;
				}
		echo "</div>";
	$GLOBALS['cws_portfolio_single_post_floated_media'] = !empty( $thumb_url ) && !$crop_thumb;
}
function cws_portfolio_single_title ($pid = null){
	$title = get_the_title($pid);
	echo !empty( $title ) ?	"<h3 class='cws_portfolio_title post_title single_title'>" . $title . "</h3>" : "";
}
function cws_portfolio_single_content ($pid = null){
	$content =  apply_filters('the_content', get_post_field('post_content', $pid));
	if ( !empty( $content ) ){
		echo "<div class='cws_portfolio_content post_content single_content'>";
			echo $content;
		echo "</div>";
	}
}
function cws_portfolio_single_terms ($pid = null){
	$pid = !empty($pid) ? $pid : get_the_id ();
	$p_category_terms = wp_get_post_terms( $pid, 'cws_portfolio_cat' );
	$p_cats = "";
	for ( $i=0; $i < count( $p_category_terms ); $i++ ){
		$p_category_term = $p_category_terms[$i];
		$p_cat_permalink = get_term_link( $p_category_term->term_id, 'cws_portfolio_cat' );
		$p_cat_name = $p_category_term->name;
		$p_cats .= "<a href='".esc_url($p_cat_permalink)."'>".esc_html($p_cat_name)."</a>";
		$p_cats .= $i < count( $p_category_terms ) - 1 ? esc_html__( ",&#x20;", 'cws-essentials' ) : "";
	}	
	echo !empty($p_cats) ? "<div class='cws_portfolio_terms post_terms single_terms'>&#x20;".$p_cats."</div>" : "";	
}
// =====================//Portfolio Single=====================

function cws_portfolio_masonry($featured_img_url = '', $dims_from_columns, $p_meta = '', $custom_layout_arr = false){
	global $cws_theme_funcs;
	$full_width = isset($custom_layout_arr['$full_width']) ? $custom_layout_arr['$full_width'] : false;

	$isotope_col_count = (isset($p_meta['isotope_col_count'])) ? intval($p_meta['isotope_col_count']) : 1;
	$isotope_line_count = (isset($p_meta['isotope_line_count'])) ? intval($p_meta['isotope_line_count']) : 1;

	$img_width = $dims_from_columns['width'];
	$img_height = $dims_from_columns['height'];

	$columns = intval($custom_layout_arr['columns']);
	if (!empty( $featured_img_url ) && $custom_layout_arr['crop_images']){
		//Make square dimensions
		if ($img_width <= $img_height){
			$dims['width'] = $img_width;
			$dims['height'] = $img_width;
		} elseif ($img_height <= $img_width) {
			$dims['width'] = $img_height;
			$dims['height'] = $img_height;
		}
		$dims['crop'] = array(
			$cws_theme_funcs->cws_get_option( "crop_x" ),
			$cws_theme_funcs->cws_get_option( "crop_y" )
		);	
	} else {
		$dims['width'] = 0;
		$dims['height'] = 0;
	}

	if (!empty( $featured_img_url ) && $custom_layout_arr['crop_images'] == false && $custom_layout_arr['masonry']) {
		$img_width = $dims_from_columns['width'];
		$img_height = $dims_from_columns['height'];	
		$pd = 30;
		$col_paddings = ($pd * $isotope_col_count);
		$line_paddings = ($pd * $isotope_line_count);
		if ($custom_layout_arr['portfolio_style'] == 'wide_style') {
			$dims['width'] = ($img_width * $isotope_col_count) + $col_paddings;
			$dims['height'] = ($img_width * $isotope_line_count) + $line_paddings;
		} else{
			switch ($isotope_col_count) {
				case '1':
					$dims['width'] = $img_width * $isotope_col_count;
					break;
				case '2':
					if ( $columns < $isotope_col_count ) {
						$isotope_col_count = $columns;
					}
					$dims['width'] = ($img_width * $isotope_col_count) + $pd;
					break;
				case '3':
					$case = $isotope_col_count - 1;
					if ( $columns < $isotope_col_count ) {
						$isotope_col_count = $columns;
						$case = $isotope_col_count - 1;
					}
					$dims['width'] = ($img_width * $isotope_col_count) + $pd * $case;
					break;
				case '4':
					$case = $isotope_col_count - 1;
					if ( $columns < $isotope_col_count ) {
						$isotope_col_count = $columns;
						$case = $isotope_col_count - 1;
					}
					$dims['width'] = ($img_width * $isotope_col_count) + $pd * $case;
					break;
			}
			if($full_width){
				switch ($custom_layout_arr['columns']) {
					case '1':
						$img_width = 1053;
						break;
					case '2':
						$img_width = 518;
						break;
					case '3':
						$img_width = 340;
						break;
					case '4':
						$img_width = 250;
						break;
					case '5':
						$img_width = 197;
						break;
					
					default:
						break;
				}
			} else {
				switch ($custom_layout_arr['columns']) {
					case '1':
						if ( empty( $custom_layout_arr['sb_layout'] ) ){
							$img_width = 658;	
						}
						else if ( $custom_layout_arr['sb_layout'] === "double" ){
							$img_width = 320;
						}
						break;
					case '2':
						if ( empty( $custom_layout_arr['sb_layout'] ) ){
							$img_width = 320;	
						}
						else if ( $custom_layout_arr['sb_layout'] === "single" ){
							$img_width = 268;
						}
						else if ( $custom_layout_arr['sb_layout'] === "double" ){
							$img_width = 152;
						}
						break;
					case '3':
						if ( empty( $custom_layout_arr['sb_layout'] ) ){
							$img_width = 208;
						}
						else if ( $custom_layout_arr['sb_layout'] === "single" ){
							$img_width = 152;
						}
						else if ( $custom_layout_arr['sb_layout'] === "double" ){
							$img_width = 96;
						}
						break;
					case '4':
						if ( empty( $custom_layout_arr['sb_layout'] ) ){
							$img_width = 150;
						}
						else if ( $custom_layout_arr['sb_layout'] === "single" ){
							$img_width = 110;
						}
						else if ( $custom_layout_arr['sb_layout'] === "double" ){
							$img_width = 68;
						}
						break;
					case '5':
						$img_width = 118;
						if ( empty( $custom_layout_arr['sb_layout'] ) ){
							$img_width = 150;
						}
						else if ( $custom_layout_arr['sb_layout'] === "single" ){
							$img_width = 84;
						}
						else if ( $custom_layout_arr['sb_layout'] === "double" ){
							$img_width = 51;
						}
						break;
				}		
			}
			switch ($isotope_line_count) {
				case '1':
					$dims['height'] = $img_width * $isotope_line_count;
					break;
				case '2':
					if ( $columns < $isotope_line_count ) {
						$isotope_line_count = $columns;
					}
					$dims['height'] = ($img_width * $isotope_line_count) + $pd;
					break;
				case '3':
					$case = $isotope_line_count - 1;
					if ( $columns < $isotope_line_count ) {
						$isotope_line_count = $columns;
						$case = $isotope_line_count - 1;
					}
					$dims['height'] = ($img_width * $isotope_line_count) + $pd * $case;
					break;
				case '4':
					$case = $isotope_line_count - 1;
					if ( $columns < $isotope_line_count ) {
						$isotope_line_count = $columns;
						$case = $isotope_line_count - 1;
					}
					$dims['height'] = ($img_width * $isotope_line_count) + $pd * $case;
					break;
			}
		}
		$dims['crop'] = array(
			$cws_theme_funcs->cws_get_option( "crop_x" ),
			$cws_theme_funcs->cws_get_option( "crop_y" )
		);
	}

	if (!empty( $featured_img_url ) && $custom_layout_arr['crop_images'] && $custom_layout_arr['masonry']) {
		$img_width = $dims_from_columns['width'];
		$img_height = $dims_from_columns['height'];	
		$pd = 30;
		$col_paddings = ($pd * $isotope_col_count);
		$line_paddings = ($pd * $isotope_line_count);
		if ($custom_layout_arr['portfolio_style'] == 'wide_style') {
			$dims['width'] = ($img_width * $isotope_col_count) + $col_paddings;
			$dims['height'] = ($img_width * $isotope_line_count) + $line_paddings;
		} else {	
			switch ($isotope_col_count) {
				case '1':
					$dims['width'] = $img_width * $isotope_col_count;
					break;
				case '2':
					if ( $columns < $isotope_col_count ) {
						$isotope_col_count = $columns;
					}
					$dims['width'] = ($img_width * $isotope_col_count) + $pd;
					break;
				case '3':
					$case = $isotope_col_count - 1;
					if ( $columns < $isotope_col_count ) {
						$isotope_col_count = $columns;
						$case = $isotope_col_count - 1;
					}
					$dims['width'] = ($img_width * $isotope_col_count) + $pd * $case;
					break;
				case '4':
					$case = $isotope_col_count - 1;
					if ( $columns < $isotope_col_count ) {
						$isotope_col_count = $columns;
						$case = $isotope_col_count - 1;
					}
					$dims['width'] = ($img_width * $isotope_col_count) + $pd * $case;
					break;
			}
			switch ($isotope_line_count) {
				case '1':
					$dims['height'] = $img_width * $isotope_line_count;
					break;
				case '2':
					if ( $columns < $isotope_line_count ) {
						$isotope_line_count = $columns;
					}
					$dims['height'] = ($img_width * $isotope_line_count) + $pd;
					break;
				case '3':
					$case = $isotope_line_count - 1;
					if ( $columns < $isotope_line_count ) {
						$isotope_line_count = $columns;
						$case = $isotope_line_count - 1;
					}
					$dims['height'] = ($img_width * $isotope_line_count) + $pd * $case;
					break;
				case '4':
					$case = $isotope_line_count - 1;
					if ( $columns < $isotope_line_count ) {
						$isotope_line_count = $columns;
						$case = $isotope_line_count - 1;
					}
					$dims['height'] = ($img_width * $isotope_line_count) + $pd * $case;
					break;
			}
		}

		$dims['crop'] = array(
			$cws_theme_funcs->cws_get_option( "crop_x" ),
			$cws_theme_funcs->cws_get_option( "crop_y" )
		);
	}
	return array(
		'thumbnail_dims' => $dims,
		'isotope_col_count' => $isotope_col_count,
		'isotope_line_count' => $isotope_line_count
	);
}
function portfolio_filter_gallery(){
	return add_filter( 'post_gallery', 'single_gallery', 11, 3 );
}

function single_gallery( $output = null, $atts = null, $instance = null) {
	$page_id = isset($GLOBALS['cws_portfolio_atts']['page_id']) ? $GLOBALS['cws_portfolio_atts']['page_id'] : get_the_id();

	$single_ajax_atts = isset( $GLOBALS['cws_single_ajax_portfolio_atts'] ) ? $GLOBALS['cws_single_ajax_portfolio_atts'] : array();
	$single = cws_is_single($page_id);
	if ( $single && !empty($single_ajax_atts) ){
		extract( $single_ajax_atts );
	}

	$pid = !empty($pid) ? $pid : get_the_id();

	//Meta vars
	$p_meta = get_post_meta( $pid, 'cws_mb_post' );
	$p_meta = isset( $p_meta[0] ) ? $p_meta[0] : array();

	$full_width = $p_meta['full_width'];
	//--Meta vars

	$atts = array_merge(array('columns' => 3), $atts);
	$columns = $atts['columns'];
	$columns = preg_replace( '/[^0-9,]+/', '', $columns );
	$columns = intval($columns);

	$resolution = $full_width ? 1920 : 1170;
	$crop_width = $resolution / $columns;
	$img_data = array('width' => $crop_width );

	$itemwidth = $columns > 0 ?  round((100 / $columns) , 2) : 100;
	$images = explode(',', $atts['ids']);
	$gallery_id = uniqid( 'cws-portfolio-gallery-' );

	$gallery_style = "
		#".esc_attr($gallery_id)." {
			margin: auto;
		}
	    #".esc_attr($gallery_id)." .gallery-item {
			margin-top: 10px;
			text-align: center;
			width: ".esc_attr($itemwidth)."%;
		}
		#".esc_attr($gallery_id)." .gallery-caption {
			margin-left: 0;
		}
	";

	Cws_shortcode_css()->enqueue_cws_css($gallery_style);

	$i = 0;
	$return = '';
	
	$return .= "<div id='".esc_attr($gallery_id)."' class='single_gallery'>";
	foreach ($images as $key => $value) {
		if($key == 0){
			$value = str_replace("&quot;", "", $value); 
		}
		if (!empty($value)){
			$image_attributes = wp_get_attachment_image_src($value, 'full');
			$real_thumbnail_dims = array();
			if ( !empty( $image_attributes ) && isset( $image_attributes[1] ) ) $real_thumbnail_dims['width'] = $image_attributes[1];
			if ( !empty(  $image_attributes ) && isset( $image_attributes[2] ) ) $real_thumbnail_dims['height'] = $image_attributes[2];
			$thumb_obj = cws_thumb($value, $img_data, false);

			if ($thumb_obj){
				$thumb_path_hdpi = isset( $thumb_obj[3] ) ? " src='". esc_url($thumb_obj[0]) ."' data-at2x='" . esc_attr($thumb_obj[3]) ."'" : " src='". esc_url($thumb_obj[0]) . "' data-no-retina";
				$src = $thumb_path_hdpi;
				$return .= '
					<div class="gallery-item col_' . esc_attr($columns) . '">
						<a class="fancy" data-gallery="gallery" data-fancybox-group="'.esc_attr($gallery_id).'" href="'.esc_url($image_attributes[0]).'">
							<img '.$src.' alt="">
						</a>
					</div>
				';
				$i++;
			}
		}
	}
	$return .= '</div>';
	return $return;
}

function cws_portfolio_thumb_dims ( $eq_thumb_height = false, $real_dims = array() ) {
	global $cws_theme_funcs;
	$page_id = isset($GLOBALS['cws_portfolio_atts']['page_id']) ? $GLOBALS['cws_portfolio_atts']['page_id'] : get_the_id();

	//Atts
	$grid_atts = isset( $GLOBALS['cws_portfolio_atts'] ) ? $GLOBALS['cws_portfolio_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_portfolio_atts'] ) ? $GLOBALS['cws_single_portfolio_atts'] : array();
	$single_ajax_atts = isset( $GLOBALS['cws_single_ajax_portfolio_atts'] ) ? $GLOBALS['cws_single_ajax_portfolio_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
		extract( $single_ajax_atts );
	}
	else {
		extract( $grid_atts );
	}

	$pid = !empty($pid) ? $pid : get_the_id();

	//Meta vars
	$post_meta = get_post_meta( $pid, 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$single_fw = isset( $post_meta['full_width'] ) ? $post_meta['full_width'] : false;
	//--Meta vars	

	$layout = !empty($layout) ? $layout : '1';
	
	$dims = array( 'width' => 0, 'height' => 0 );

	if ($single){
		if ( empty( $sb_layout ) ){
			if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) ) ) ){
				if ($single_fw) {
					$dims['width'] = 1920;
				} else{
					$dims['width'] = 1170;
				}
			}
		}
		else if ( $sb_layout === "single" ){
			if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 870 ) ) || $eq_thumb_height ){
				$dims['width'] = 870;
			}
		}
		else if ( $sb_layout === "double" ){
			if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 570 ) ) || $eq_thumb_height ){
				$dims['width'] = 570;
			}
		}
	} else if ($full_width){
		switch ($layout){
			case "1":	
				$dims['width'] = 1920;
				// if (!isset($real_dims['height'])){
					$dims['height'] = 1080;
				// }		
				break;
			case '2':
				$dims['width'] = 921;
				// if (!isset($real_dims['height'])){
					$dims['height'] = 208;
				// }		
				break;
			case '3':
				$dims['width'] = 604;
				// if (!isset($real_dims['height'])){
					$dims['height'] = 208;
				// }		
				break;
			case '4':
				$dims['width'] = 445;
				// if (!isset($real_dims['height'])){
					$dims['height'] = 152;
				// }
				break;
			case '5':
				$dims['width'] = 351;
				// if (!isset($real_dims['height'])){
					$dims['height'] = 152;
				// }
				break;
		}
	} else if ($display_style == 'showcase') {
		$dims['width'] = 1920;
		// if (!isset($real_dims['height'])){
			$dims['height'] = 1080;
		// }
	} else {
		switch ($layout){
			case "1":
				if ( empty( $sb_layout ) ){
					$dims['width'] = 1170;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 979;
					// }	
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 770;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 644;
					// }		
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 570;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 310;
					// }	
				}
				break;
			case '2':
				if ( empty( $sb_layout ) ){
					$dims['width'] = 570;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 477;
					// }	
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 472;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 310;
					// }	
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 270;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 142;
					// }		
				}
				break;
			case '3':
				if ( empty( $sb_layout ) ){
					$dims['width'] = 370;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 370;
					// }	
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 270;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 197;
					// }	
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 170;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 103;
					// }		
				}			
				break;
			case '4':
				if ( empty( $sb_layout ) ){
					$dims['width'] = 270;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 226;
					// }
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 195;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 142;
					// }		
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 120;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 58;
					// }	
				}			
				break;			
			case '5':
				if ( empty( $sb_layout ) ){
					$dims['width'] = 210;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 226;
					// }
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 150;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 142;
					// }		
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 90;
					// if (!isset($real_dims['height'])){
						$dims['height'] = 58;
					// }	
				}			
				break;			
		}
	}

	if (isset($portfolio_style) && ($portfolio_style == 'wide_style' && !empty($crop_images) && !$full_width)) {
		$dims['width'] = $dims['width'] + 30;
		$dims['height'] = $dims['width'] + 30;
	}
	return $dims;
}
function cws_portfolio_chars_count ( $cols = null ){
	$number = 155;
	switch ( $cols ){
		case '1':
			$number = 300;
			break;
		case '2':
			$number = 130;
			break;
		case '3':
			$number = 70;
			break;
		case '4':
			$number = 55;
			break;
	}
	return $number;
}

function getUrl() {
	$url  = isset($_SERVER["HTTPS"]) && @( $_SERVER["HTTPS"] != 'on' ) ? 'http://'.$_SERVER["SERVER_NAME"] :  'https://'.$_SERVER["SERVER_NAME"];
	$url .= ( $_SERVER["SERVER_PORT"] != 80 ) ? ":".$_SERVER["SERVER_PORT"] : "";
	$url .= $_SERVER["REQUEST_URI"];
	return $url;
} 

?>