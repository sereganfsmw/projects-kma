<?php
global $cws_theme_funcs;

$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

extract( shortcode_atts( array(
	"icon_lib"						=> '',
	"icon_fontawesome"				=> '',
	"icon_flaticons"				=> '',
	"icon_svg"						=> '',
	"title" 						=> '',
	"title_typography_html_tag" 	=> '',
	"subtitle" 						=> '',
	"description" 					=> '',
	"text_aligning" 				=> '',
	"icon_aligning" 				=> '',
	"side_line_divider" 			=> '',
	"side_line_divider_tablet" 		=> '',
	"side_line_divider_mobile" 		=> '',
	"bottom_line_divider" 			=> '',
	"bottom_line_divider_tablet" 	=> '',
	"bottom_line_divider_mobile" 	=> '',
	"image_divider" 				=> '',
	"image_divider_tablet" 			=> '',
	"image_divider_mobile" 			=> '',
	"img" 							=> '',
	"icon_margins" 					=> '',
	"title_margins" 				=> '',
	"description_margins" 			=> '',
	"customize_colors" 				=> '',
	"title_color" 					=> '',
	"title_color_hover" 			=> '',
	"subtitle_color" 				=> '',
	"subtitle_color_hover" 			=> '',
	"description_color" 			=> '',
	"description_color_hover" 		=> '',
	"customize_icon" 				=> '',
	"icon_shape" 					=> '',
	"custom_icon_size" 				=> '',
	"background_color" 				=> '',
	"icon_color" 					=> '',
	"background_color_hover" 		=> '',
	"icon_color_hover" 				=> '',
	"hover_animation" 				=> '',
), $settings ) );

$out = $icon = "";

if ($icon_lib == 'fontawesome'){
	$icon = $icon_fontawesome;
} elseif ($icon_lib == 'flaticons') {
	$icon = $icon_flaticons;
} elseif ($icon_lib == 'svg') {
	$icon = $icon_svg;
}

$dividerClass = '';
if($side_line_divider == 'yes'){
	$dividerClass .= ' desktop_side_divider';
}
if($side_line_divider_tablet == 'yes'){
	$dividerClass .= ' tablet_side_divider';
}
if($side_line_divider_mobile == 'yes'){
	$dividerClass .= ' mobile_side_divider';
}
if($image_divider == 'yes'){
	$dividerClass .= ' desktop_image_divider';
}
if($image_divider_tablet == 'yes'){
	$dividerClass .= ' tablet_image_divider';
}
if($image_divider_mobile == 'yes'){
	$dividerClass .= ' mobile_image_divider';
}

if ( empty( $title ) && empty( $description ) && empty( $icon )) return $out;

$module_id = uniqid( "cws_services_" );
$tag = $icon_lib == 'svg' ? "span" : "i";
$classes = "cws_icon " . ($icon_lib != 'svg' ? " $icon" : '');

$tag_atts = $tag;
$tag_atts .= " class='".esc_attr($classes)."'";

$out .= "<div id='".esc_attr($module_id)."' class='cws_service_item ".esc_attr($dividerClass)." shape_".esc_attr($icon_shape)." icon_align_".esc_attr($icon_aligning)." ".($customize_icon == 'yes' ? 'custom_icon' : '')."'>";

	$out .= "<div class='cws_left_divider'></div>";

		if (!empty($icon)){
			$out .= "<span class='cws_service_icon_wrapper'>";
				$out .= "<span class='cws_service_icon_container".(!empty($hover_animation) ? ' elementor-animation-'.esc_attr($hover_animation) : '')."'>";
					$out .= "<$tag_atts>";
					if($icon_lib == 'svg'){
						$svg_icon = json_decode(str_replace("``", "\"", $icon), true);
						$out .= function_exists('cwssvg_shortcode') ? cwssvg_shortcode($svg_icon) : "";			
					}
					$out .= "</$tag>";
				$out .= "</span>";

				if ($image_divider == 'yes' && $icon_aligning =='none'){
					$out .= "<span class='cws_service_divider'><img src=".$img['url']." /></span>";
				}

			$out .= "</span>";
		}

		if ( !empty( $title ) || !empty( $description ) ){
			$out .= "<div class='cws_service_info'>";
				if (!empty( $title )){
					$out .= "<".esc_attr($title_typography_html_tag)." class='cws_service_title'>".(!empty($subtitle) ? "<p class='cws_service_subtitle'>".esc_html($subtitle)."</p>" : '')." ".esc_html($title)."</".esc_attr($title_typography_html_tag).">";
				}

				if (!empty( $description )){
					$out .= "<div class='cws_service_desc'>";
						$out .= $description;
					$out .= "</div>";
				}
			$out .= "</div>";
		}

	$out .= "<div class='cws_right_divider'></div>";

$out .= "</div>";

echo sprintf("%s", $out);