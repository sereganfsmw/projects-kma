<?php
global $cws_theme_funcs;

//Colors
$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

extract( shortcode_atts( array(
	"icon_lib"					=> '',
	"icon_fontawesome"			=> '',
	"icon_flaticons"			=> '',
	"icon_svg"					=> '',
	'title'						=> '',
	'title_typography'			=> '',
	'desc'						=> '',
	'desc_typography'			=> '',
	'size'						=> '',
	'icon_pos'					=> '',
	'number'					=> '',
	'speed'						=> '',
	'content_alignment'			=> '',
	'info_alignment'			=> '',
	'info_alignment_tablet'		=> '',
	'info_alignment_mobile'		=> '',
	'info_vert_alignment'		=> '',
	'custom_size_i'				=> '',
	'size_i'					=> '',
	'custom_color_milestone'	=> '',
	'custom_color_number'		=> '',
	'custom_color_text'			=> '',
	'i_color_m'					=> '',

	'title_typography_html_tag'	=> '',
	'desc_typography_html_tag'	=> '',
), $settings ) );

$component = $render->get_stack();

$component = $component['controls'];

$info_alignment = trim(preg_replace("/\r\n|\r|\s+|\n/", '', $info_alignment));
$info_alignment_tablet = trim(preg_replace("/\r\n|\r|\s+|\n/", '', $info_alignment_tablet));
$info_alignment_mobile = trim(preg_replace("/\r\n|\r|\s+|\n/", '', $info_alignment_mobile));

$info_alignment_desktop = $component['info_alignment']['options'][$info_alignment]['align'];
$info_alignment_tablet = $component['info_alignment_tablet']['options'][$info_alignment_tablet]['align'];
$info_alignment_mobile = $component['info_alignment_mobile']['options'][$info_alignment_mobile]['align'];

$out = $styles = $icon = $icon_html = "";

if ($icon_lib == 'fontawesome'){
	$icon = $icon_fontawesome;
} elseif ($icon_lib == 'flaticons') {
	$icon = $icon_flaticons;
} elseif ($icon_lib == 'svg') {
	$icon = $icon_svg;
}

$module_id = uniqid( "cws_milestone_" );

$classes = "cws_milestone cws_module";
$classes .= !empty( $el_class ) ? " ".esc_attr($el_class) : "" ;

$out .= "<div id='".esc_attr($module_id)."' class='".esc_attr($classes)."'>";
	!empty( $styles ) ? Cws_shortcode_css()->enqueue_cws_css($styles) : "";
	$out .= !empty($milestone_head) ? $milestone_head : "";
	$out .= "<div class='milestone_wrapper'>";
	$out .= "<div class='cws_milestome_main'>";
	$out .= "<div class='cws_milestone_wrapper" . ( !empty( $text_alignment ) ? " a-".esc_attr($text_alignment) : "" ) . "'>";
		if(!empty($icon)){
			$out .= "<div class='cws_milestome_icon_wrapper'>";
				$out .= "<div class='cws_milestone_icon".(!empty( $size ) ? " cws_icon_".esc_attr($size) : " cws_icon_3x")."'>";
					$out .= "<".($icon_lib == 'svg' ? 'span' : "i class='".esc_attr($icon)."'").">";
						if($icon_lib == 'svg'){
							$svg_icon = json_decode(str_replace("``", "\"", $icon), true);
							$out .= function_exists('cwssvg_shortcode') ? cwssvg_shortcode($svg_icon) : "";			
						}	
					$out .= "</".($icon_lib == 'svg' ? 'span' : 'i').">";
				$out .= "</div>";
			$out .= "</div>";
		}

		$out .= "<div class='cws_milestone_data'>";
			$out .= "<div class='cws_milestone_number'" . ( !empty( $speed ) && is_numeric( $speed ) ? " data-speed='".esc_attr($speed)."'" : "" ) . ">".esc_html($number)."</div>";

			if( !empty($title) || !empty($desc) ){
				$out .= "<div class='cws_milestome_info ".$info_alignment_desktop."_desktop ".$info_alignment_tablet."_tablet ".$info_alignment_mobile."_mobile'>";

					$out .= !empty( $title ) ? "<".$title_typography_html_tag." class='cws_milestone_title'>".esc_html($title)."</".$title_typography_html_tag.">" : "";
					if(!empty($desc)){
						$out .= "<".$desc_typography_html_tag." class='cws_milestone_desc'>";
							$out .= esc_html($desc);
						$out .= "</".$desc_typography_html_tag.">";
					}

				$out .= "</div>";
			}

		$out .= "</div>";

	$out .= "</div>";
	$out .= "</div>";
	$out .= "</div>";
$out .= "</div>";

echo sprintf("%s", $out);