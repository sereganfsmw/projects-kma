<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//Settings
$title = 				($type == 'php') ? 	$settings['title'] :				'{{{settings.title}}}';
$progress_type = 		($type == 'php') ? 	$settings['progress_type'] :		'{{settings.progress_type}}';

$percent = 				($type == 'php') ? 	$settings['percent'] :				'{{{settings.percent}}}';
$percent_size = 		($type == 'php') ? 	$settings['percent']['size'] :		'{{{settings.percent.size}}}';

$display_percentage = 	($type == 'php') ? 	$settings['display_percentage'] :	'{{{settings.display_percentage}}}';
$inner_text = 			($type == 'php') ? 	$settings['inner_text'] :			'{{{settings.inner_text}}}';
$view = 				($type == 'php') ? 	$settings['view'] :					'{{{settings.view}}}';
$bar_color = 			($type == 'php') ? 	$settings['bar_color'] :			'{{{settings.bar_color}}}';
$bar_bg_color = 		($type == 'php') ? 	$settings['bar_bg_color'] :			'{{{settings.bar_bg_color}}}';
$bar_inline_color = 	($type == 'php') ?	$settings['bar_inline_color'] :		'{{{settings.bar_inline_color}}}';
$section_title = 		($type == 'php') ? 	$settings['section_title'] :		'{{{settings.section_title}}}';
$title_color = 			($type == 'php') ? 	$settings['title_color'] :			'{{{settings.title_color}}}';
$typography = 			($type == 'php') ? 	$settings['typography'] :			'{{{settings.typography}}}';

//=======================RENDER TYPE=======================
$js_settings = '';
//-----------PHP-----------
if ($type == 'php'){


	$render->add_render_attribute( 'wrapper', [
		'class' => 'elementor-progress-wrapper',
		'role' => 'progressbar',
		'aria-valuemin' => '0',
		'aria-valuemax' => '100',
		'aria-valuenow' => $percent_size,
		'aria-valuetext' => $inner_text
	] );

	if ( ! empty( $progress_type ) ) {
		$render->add_render_attribute( 'wrapper', 'class', 'progress-' . $progress_type );
	}

	$render->add_render_attribute( 'progress_bar', [
		'class' => 'elementor-progress-bar',
		'data-max' => $percent_size,
	] );


//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
elseif ($type == 'js') {


	$js_settings = "
		<#
		view.addRenderAttribute( 'wrapper', {
			'class': [ 'elementor-progress-wrapper', 'progress-' + ".js_var($progress_type)." ],
			'role': 'progressbar',
			'aria-valuemin': '0',
			'aria-valuemax': '100',
			'aria-valuenow': ".js_var($percent_size).",
			'aria-valuetext': ".js_var($inner_text)."
		} );
		view.addInlineEditingAttributes( 'wrapper' );

		view.addRenderAttribute( 'progress_bar', {
			'class': 'elementor-progress-bar',
			'data-max': ".js_var($percent_size)."
		} );	
		#>
	";


}
//-----------/JS (BACKBONE)-----------

//Render attr
$attr_wrapper = 		($type == 'php') ? $render->get_render_attribute_string( 'wrapper' ) : 		"{{{ view.getRenderAttributeString( 'wrapper' ) }}}";
$attr_progress_bar =	($type == 'php') ? $render->get_render_attribute_string( 'progress_bar' ) :	"{{{ view.getRenderAttributeString( 'progress_bar' ) }}}";


$module_id = uniqid( "cws_progress_bar_" );
$out = "";

ob_start();
	print $js_settings;

		if ($type == 'php'){
			if ( ! empty( $title ) ) {	
				echo "<span class='elementor-title'>".$title."</span>";
			}
		} elseif ($type == 'js') {
			echo "<# if ( ".js_var($title)." ) { #>";
				echo "<span class='elementor-title'>".$title."</span>";
			echo "<# } #>";
		}
		?>

		<div <?php echo $attr_wrapper; ?>>
			<div <?php echo $attr_progress_bar; ?>>
				<span class="elementor-progress-text"><?php echo $inner_text; ?></span>
				<?php 

				if ($type == 'php'){
					if ( 'hide' !== $display_percentage ) {
						echo "<span class='elementor-progress-percentage'>".$percent_size."%</span>";
					}
				} elseif ($type == 'js') {
					echo "<# if ( 'hide' !== ".js_var($display_percentage)." ) { #>";
						echo "<span class='elementor-progress-percentage'>".$percent_size."%</span>";
					echo "<# } #>";
				}

				?>

			</div>
		</div>
	<?php
$out .= ob_get_clean();

echo sprintf("%s", $out);