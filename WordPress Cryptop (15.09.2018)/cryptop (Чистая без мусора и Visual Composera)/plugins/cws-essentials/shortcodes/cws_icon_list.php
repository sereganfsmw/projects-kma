<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//=======================RENDER TYPE=======================
$out = '';
ob_start();
//-----------PHP-----------
if ($type == 'php'){

		$render->add_render_attribute( 'icon_list', 'class', 'elementor-icon-list-items' );
		$render->add_render_attribute( 'list_item', 'class', 'elementor-icon-list-item' );

		if ( 'inline' === $settings['view'] ) {
			$render->add_render_attribute( 'icon_list', 'class', 'elementor-inline-items' );
			$render->add_render_attribute( 'list_item', 'class', 'elementor-inline-item' );
		}
		?>
		<ul <?php echo $render->get_render_attribute_string( 'icon_list' ); ?>>
			<?php
			foreach ( $settings['icon_list'] as $index => $item ) :
				$repeater_setting_key = $extra_params[$index];
				?>
				<li class="elementor-icon-list-item" >
					<?php
					if ( ! empty( $item['link']['url'] ) ) {
						$link_key = 'link_' . $index;

						$render->add_render_attribute( $link_key, 'href', $item['link']['url'] );

						if ( $item['link']['is_external'] ) {
							$render->add_render_attribute( $link_key, 'target', '_blank' );
						}

						if ( $item['link']['nofollow'] ) {
							$render->add_render_attribute( $link_key, 'rel', 'nofollow' );
						}

						echo '<a ' . $render->get_render_attribute_string( $link_key ) . '>';
					}

					if ( ! empty( $item['icon'] ) ) :
						?>
						<span class="elementor-icon-list-icon">
							<i class="<?php echo esc_attr( $item['icon'] ); ?>" aria-hidden="true"></i>
						</span>
					<?php endif; ?>
					<span <?php echo $render->get_render_attribute_string( $repeater_setting_key ); ?>><?php echo $item['text']; ?></span>
					<?php if ( ! empty( $item['link']['url'] ) ) : ?>
						</a>
					<?php endif; ?>
				</li>
				<?php
			endforeach;
			?>
		</ul>
		<?php
//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
else if ($type == 'js') {
	?>
	<#
		view.addRenderAttribute( 'icon_list', 'class', 'elementor-icon-list-items' );
		view.addRenderAttribute( 'list_item', 'class', 'elementor-icon-list-item' );

		if ( 'inline' == settings.view ) {
			view.addRenderAttribute( 'icon_list', 'class', 'elementor-inline-items' );
			view.addRenderAttribute( 'list_item', 'class', 'elementor-inline-item' );
		}
	#>
	<# if ( settings.icon_list ) { #>
		<ul {{{ view.getRenderAttributeString( 'icon_list' ) }}}>
		<# _.each( settings.icon_list, function( item, index ) {

				var iconTextKey = view.getRepeaterSettingKey( 'text', 'icon_list', index );

				view.addRenderAttribute( iconTextKey, 'class', 'elementor-icon-list-text' );

				#>

				<li {{{ view.getRenderAttributeString( 'list_item' ) }}}>
					<# if ( item.link && item.link.url ) { #>
						<a href="{{ item.link.url }}">
					<# } #>
					<# if ( item.icon ) { #>
					<span class="elementor-icon-list-icon">
						<i class="{{ item.icon }}" aria-hidden="true"></i>
					</span>
					<# } #>
					<span {{{ view.getRenderAttributeString( iconTextKey ) }}}>{{{ item.text }}}</span>
					<# if ( item.link && item.link.url ) { #>
						</a>
					<# } #>
				</li>
			<#
			} ); #>
		</ul>
	<#	} #>

	<?php
}
//-----------/JS (BACKBONE)-----------
$out = ob_get_clean();

echo sprintf("%s", $out);