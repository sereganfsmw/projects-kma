<?php

$shortcode = $settings['shortcode'];
$shortcode = do_shortcode( shortcode_unautop( $shortcode ) );

$render->add_render_attribute( 'wrapper', 'class', 'elementor-shortcode' );

//Carousel process
if ($settings['use_carousel'] == 'yes'){

	$responsive = array(
		0 => array(
			'items' => (!empty($settings['slides_to_show_mobile']) ? (int)$settings['slides_to_show_mobile'] : 1)
		),
		767 => array(
			'items' => (!empty($settings['slides_to_show_tablet']) ? (int)$settings['slides_to_show_tablet'] : 1)
		),
		1199 => array(
			'items' => (!empty($settings['slides_to_show']) ? (int)$settings['slides_to_show'] : 1)
		),
	);

	$carousel_args = array(
		'items' => (int)$settings['slides_to_show'],
		'responsiveClass' => true,
		'responsive' => $responsive,
		'cols' => (int)$settings['columns'],
		'nav' => ($settings['navigation'] == 'both' || $settings['navigation'] == 'arrows'),
		'dots' => ($settings['navigation'] == 'both' || $settings['navigation'] == 'dots'),
		'autoheight' => ($settings['auto_height'] == 'yes'),
		'autoplay' => ($settings['autoplay'] == 'yes'),
		'autoplaySpeed' => ($settings['autoplay'] == 'yes' ? (int)($settings['autoplay_speed']) : false),
		'autoplayHoverPause' => ($settings['pause_on_hover'] == 'yes'),
		'margin' => $settings['item_margins']['size'],
		'stagePadding' => $settings['item_stage_paddings']['size'],
		'center' => ($settings['item_center'] == 'yes'),
		'loop' => ($settings['infinite'] == 'yes'),
		'animateIn' => ($settings['animation_in'] != 'default' ? $settings['animation_in'] : false),
		'animateOut' => ($settings['animation_out'] != 'default' ? $settings['animation_out'] : false),
		'smartSpeed' => (!empty($settings['animation_speed']) ? (int)(esc_attr($settings['animation_speed'])) : 250),
	);

	$render->add_render_attribute( 'wrapper', 'data-owl-args', json_encode($carousel_args) );

	$render->add_render_attribute( 'wrapper', [
		'class' => ['shortcode_carousel', 'cws_owl_carousel'],
	] );

	if ( $settings['carousel_type'] == 'woo' ){
		$render->add_render_attribute( 'wrapper', 'data-owl-selector', '.products' );
	} else if ($settings['carousel_type'] == 'custom') {
		$render->add_render_attribute( 'wrapper', 'data-owl-selector', esc_attr($settings['carousel_selector']) );
	}	
}
//--Carousel process

$out = "";
$out .= "<div ".$render->get_render_attribute_string( 'wrapper' ).">";
	$out .= $shortcode;
$out .= "</div>";

echo sprintf("%s", $out);