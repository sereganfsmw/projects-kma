<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

extract( shortcode_atts( array(
	"tips_img" 				=> '',
	"tips_list" 			=> array(),
	"tips_style" 			=> '',
	"show_tips" 			=> '',
	"tips_color" 			=> '',
	"hotspot_color" 		=> '',
	"hotspot_opacity" 		=> '',
	"pulse_animation" 		=> '',
	"pulse_color_start" 	=> '',
	"pulse_color_end" 		=> '',
	"show_tips"				=> '',
	"tooltip_trigger" 		=> '',
	"tips_animation" 		=> '',
), $settings ) );

//=======================RENDER TYPE=======================
$out = '';

//-----------PHP-----------
if ($type == 'php'){
	if (empty($tips_img['url'])) return $out;

	ob_start();
	?>
	
	<style>
		@-webkit-keyframes cws_pulse{
			0% {
				-webkit-box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_start); ?>;
						box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_start); ?>;
			}
			70% {
				-webkit-box-shadow: 0 0 0 10px <?php echo esc_attr($pulse_color_end); ?>;
						box-shadow: 0 0 0 10px <?php echo esc_attr($pulse_color_end); ?>;
			}
			100% {
				-webkit-box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_end); ?>;
						box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_end); ?>;
			}
		}
		@-moz-keyframes cws_pulse{
			0% {
				-webkit-box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_start); ?>;
						box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_start); ?>;
			}
			70% {
				-webkit-box-shadow: 0 0 0 10px <?php echo esc_attr($pulse_color_end); ?>;
						box-shadow: 0 0 0 10px <?php echo esc_attr($pulse_color_end); ?>;
			}
			100% {
				-webkit-box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_end); ?>;
						box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_end); ?>;
			}
		}	
		@keyframes cws_pulse{
			0% {
				-webkit-box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_start); ?>;
						box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_start); ?>;
			}
			70% {
				-webkit-box-shadow: 0 0 0 10px <?php echo esc_attr($pulse_color_end); ?>;
						box-shadow: 0 0 0 10px <?php echo esc_attr($pulse_color_end); ?>;
			}
			100% {
				-webkit-box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_end); ?>;
						box-shadow: 0 0 0 0 <?php echo esc_attr($pulse_color_end); ?>;
			}
		}	
	</style>

	<?php
	$out .= ob_get_clean();

	$out .= "<div class='cwstooltip-wrapper'>";
		$out .= "<img src='".esc_url($tips_img['url'])."'>";
		$out .= "<div class='cws-hotspots'>";
		$count = 0;

		foreach ($tips_list as $key => $value) {
			$count ++;
			$tipsStyle = ' ';

			if($value['tips_style'] == 'icon'){
				$tipsStyle = ' tips-icon';
			} else if($value['tips_style'] == 'dots'){
				$tipsStyle = ' tips-dots';
			} else if($value['tips_style'] == 'number'){
				$tipsStyle = ' tips-number';
			}

			$tooltip_align = '';
			if (!empty($value['vertical_align']) || !empty($value['horizontal_align'])){
				if (!empty($value['vertical_align'])){
					$tooltip_align = $value['vertical_align'];	
				}

				if (!empty($value['horizontal_align'])){
					$tooltip_align = $value['horizontal_align'];
				}

				if (!empty($value['vertical_align']) && !empty($value['horizontal_align'])){
					$tooltip_align = $value['vertical_align'] . '-' . $value['horizontal_align'];
				}
			} else {
				$tooltip_align = 'top';
			}

			$item_id = 'elementor-repeater-item-'.$value['_id'];
			
			$out .= "<a target='_blank' href='".(!empty($value['link']['url']) ? esc_url($value['link']['url']) : '#')."' class='".esc_attr($item_id)." hotspot-item".esc_attr($tipsStyle)."".($pulse_animation ? ' pulse-active' : '')."".($show_tips == 'yes' ? ' show-desc' : '')." ".esc_attr($tooltip_trigger)."_trigger'>";
				if($value['tips_style'] == 'icon'){
					$out .= "<span class='tip'><i class='".esc_attr($value['icon'])."'></i></span>";
				} else if($value['tips_style'] == 'dots'){
					$out .= "<span class='tip'><i></i></span>";
				} else{
					$out .= "<span class='tip'><i>".esc_html($count)."</i></span>";
				}

				$out .= "<div class='cws-tooltip ".esc_attr($tooltip_align)."'>";
					$out .= "<span class='cws-tooptip-title'>".esc_html($value['title'])."</span>";
					$out .= "<span class='cws-tooptip-content'>".esc_html($value['text'])."</span>";
				$out .= "</div>";
			$out .= "</a>";
		}

		$out .= "</div>";
	$out .= "</div>";

//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
else if ($type == 'js') {
	ob_start();
	?>

		<style>
			@-webkit-keyframes cws_pulse{
				0% {
					-webkit-box-shadow: 0 0 0 0 {{{settings.pulse_color_start}}};
							box-shadow: 0 0 0 0 {{{settings.pulse_color_start}}};
				}
				70% {
					-webkit-box-shadow: 0 0 0 10px {{{settings.pulse_color_end}}};
							box-shadow: 0 0 0 10px {{{settings.pulse_color_end}}};
				}
				100% {
					-webkit-box-shadow: 0 0 0 0 {{{settings.pulse_color_end}}};
							box-shadow: 0 0 0 0 {{{settings.pulse_color_end}}};
				}
			}
			@-moz-keyframes cws_pulse{
				0% {
					-webkit-box-shadow: 0 0 0 0 {{{settings.pulse_color_start}}};
							box-shadow: 0 0 0 0 {{{settings.pulse_color_start}}};
				}
				70% {
					-webkit-box-shadow: 0 0 0 10px {{{settings.pulse_color_end}}};
							box-shadow: 0 0 0 10px {{{settings.pulse_color_end}}};
				}
				100% {
					-webkit-box-shadow: 0 0 0 0 {{{settings.pulse_color_end}}};
							box-shadow: 0 0 0 0 {{{settings.pulse_color_end}}};
				}
			}	
			@keyframes cws_pulse{
				0% {
					-webkit-box-shadow: 0 0 0 0 {{{settings.pulse_color_start}}};
							box-shadow: 0 0 0 0 {{{settings.pulse_color_start}}};
				}
				70% {
					-webkit-box-shadow: 0 0 0 10px {{{settings.pulse_color_end}}};
							box-shadow: 0 0 0 10px {{{settings.pulse_color_end}}};
				}
				100% {
					-webkit-box-shadow: 0 0 0 0 {{{settings.pulse_color_end}}};
							box-shadow: 0 0 0 0 {{{settings.pulse_color_end}}};
				}
			}	
		</style>

		<div class="cwstooltip-wrapper">
			<img src="{{{settings.tips_img.url}}}">
			<div class="cws-hotspots">
			<#
				var count = 0;
				_.each( settings.tips_list, function( item, index ) {
					count++;
					var tipsStyle = ' ';

					if (item.tips_style == 'icon'){
						tipsStyle = ' tips-icon';
					} else if (item.tips_style == 'dots'){
						tipsStyle = ' tips-dots';
					} else if (item.tips_style == 'number'){
						tipsStyle = ' tips-number';
					} 

					var tooltip_align = '';

					if (item.vertical_align || item.horizontal_align){
						if (item.vertical_align){
							tooltip_align = item.vertical_align;
						}

						if (item.horizontal_align){
							tooltip_align = item.horizontal_align;
						}

						if (item.vertical_align && item.horizontal_align){
							tooltip_align = item.vertical_align + '-' + item.horizontal_align;
						}

					} else {
						tooltip_align = 'top';
					}

					var item_id = 'elementor-repeater-item-'+item['_id'];
			#>

				<a target="_blank" href="{{{(item.link.url) ? item.link.url : '#'}}}" class="{{{item_id}}} hotspot-item{{{tipsStyle}}} {{{(settings.pulse_animation == 'yes') ? ' pulse-active' : ''}}} {{{(settings.show_tips == 'yes') ? ' show-desc' : ''}}} {{{settings.tooltip_trigger}}}_trigger">
					<# if (item.tips_style == 'icon'){ #>
						<span class='tip'><i class='{{{item.icon}}}'></i></span>
					<# } else if (item.tips_style == 'dots'){ #>
						<span class='tip'><i></i></span>
					<# } else if (item.tips_style == 'number') { #>
						<span class='tip'><i>{{{count}}}</i></span>
					<# } #>
				
					<div class='cws-tooltip {{{tooltip_align}}}'>
						<span class='cws-tooptip-title'>{{{item.title}}}</span>
						<span class='cws-tooptip-content'>{{{item.text}}}</span>
					</div>
				</a>

			<# }); #>
			</div>
		</div>

	<?php
	$out = ob_get_clean();
}
//-----------/JS (BACKBONE)-----------


echo sprintf("%s", $out);