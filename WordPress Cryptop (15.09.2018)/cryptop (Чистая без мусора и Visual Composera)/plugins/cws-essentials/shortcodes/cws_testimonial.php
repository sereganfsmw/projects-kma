<?php
	global $cws_theme_funcs;

	$section_id = uniqid( 'cws_testimonials_' );

	$out = $grid_class = "";

	if (empty($settings['testimonials'])) return;

	if ($settings['testimonials_style'] == 'small'){
		$thumbnail_dims = array(
			'crop' => true,
			'width' => 200,
			'height' => 200
		);
	} else {
		$thumbnail_dims = array(
			'crop' => true,
			'width' => 200,
			'height' => 400
		);
	}

	$grid_class .= $settings['use_carousel'] == 'yes' ? ' testimonials_carousel cws_owl_carousel' : 'cws_grid'; 

	$render->add_render_attribute( 'wrapper', [
		'class' => ['layout-'.esc_attr($settings['columns']), 'grid', $grid_class],
	] );

	//Carousel process
	if ($settings['use_carousel'] == 'yes'){

		$responsive = array(
			0 => array(
				'items' => (!empty($settings['slides_to_show_mobile']) ? (int)$settings['slides_to_show_mobile'] : 1)
			),
			767 => array(
				'items' => (!empty($settings['slides_to_show_tablet']) ? (int)$settings['slides_to_show_tablet'] : 1)
			),
			1199 => array(
				'items' => (!empty($settings['slides_to_show']) ? (int)$settings['slides_to_show'] : 1)
			),
		);

		$carousel_args = array(
			'items' => (int)$settings['slides_to_show'],
			'responsiveClass' => true,
			'responsive' => $responsive,
			'cols' => (int)$settings['columns'],
			'nav' => ($settings['navigation'] == 'both' || $settings['navigation'] == 'arrows'),
			'dots' => ($settings['navigation'] == 'both' || $settings['navigation'] == 'dots'),
			'autoheight' => ($settings['auto_height'] == 'yes'),
			'autoplay' => ($settings['autoplay'] == 'yes'),
			'autoplaySpeed' => ($settings['autoplay'] == 'yes' ? (int)($settings['autoplay_speed']) : false),
			'autoplayHoverPause' => ($settings['pause_on_hover'] == 'yes'),
			'margin' => $settings['item_margins']['size'],
			'stagePadding' => $settings['item_stage_paddings']['size'],
			'center' => ($settings['item_center'] == 'yes'),
			'loop' => ($settings['infinite'] == 'yes'),
			'animateIn' => ($settings['animation_in'] != 'default' ? $settings['animation_in'] : false),
			'animateOut' => ($settings['animation_out'] != 'default' ? $settings['animation_out'] : false),
			'smartSpeed' => (!empty($settings['animation_speed']) ? (int)(esc_attr($settings['animation_speed'])) : 250),
		);

		$render->add_render_attribute( 'wrapper', 'data-owl-args', json_encode($carousel_args) );
	}
	//--Carousel process

	$out .= "<section id='".esc_attr($section_id)."' class='cws_testimonials'>";
		$out .= "<div class='cws_wrapper'>";
			//Carousel process
			$out .=	"<div ".$render->get_render_attribute_string( 'wrapper' ).">";
			//--Carousel process
				foreach ($settings['testimonials'] as $key => $value) {
					if (!empty($value['image']['url'])){
						$img_obj = cws_thumb( $value['image']['id'], $thumbnail_dims , true );
					}

					$author_name = !empty($value['author_link']['url']) ? "<a href='".esc_url($value['author_link']['url'])."'>".esc_html($value['author_name'])."</a>" : esc_html($value['author_name']);
					$out .= "<article class='item'>";
						$out .= "<div class='thumbnail_wrapper'><img src='".esc_url($img_obj[0])."' alt=''></div>";
						
						$out .= "<div class='content_wrapper'>";
							$out .= "<div class='title_wrapper'>";
								$out .= $author_name;
							$out .= "</div>";

							$out .= "<div class='quote_wrapper'>";
								$out .= esc_html($value['author_quote']);
							$out .= "</div>";
						$out .= "</div>";
					$out .= "</article>";
				}

			$out .= "</div>";
		$out .= "</div>";

	$out .= "</section>";

echo sprintf("%s", $out);