<?php
global $cws_theme_funcs;

$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

extract( shortcode_atts( array(
	'title' 									=> '',
	'title_typography' 							=> array(),
	'title_typography_html_tag' 				=> '',
	'subtitle' 									=> '',
	'subtitles_typography' 						=> array(),
	'subtitles_typography_html_tag' 			=> '',
	'add_extra_button' 							=> '',
	'extra_button_title' 						=> '',
	'extra_button_url' 							=> '',
	'price_subtitle' 							=> '',
	'price' 									=> '',
	'price_typography' 							=> array(),
	'price_typography_html_tag' 				=> '',
	'price_currency' 							=> '',
	'price_description' 						=> '',
	'content_subtitle' 							=> '',
	'content' 									=> '',
	'align' 									=> '',
	'button_text' 								=> '',
	'button_url' 								=> '',
	'section_style' 							=> '',
	'customize_colors' 							=> '',
	'box_shadow_color' 							=> '',
	'title_popover' 							=> '',
	'title_color' 								=> '',
	'title_color_hover' 						=> '',
	'subtitle_popover' 							=> '',
	'subtitle_color' 							=> '',
	'subtitle_color_hover' 						=> '',
	'price_popover' 							=> '',
	'price_color' 								=> '',
	'price_color_hover' 						=> '',
	'description_popover' 						=> '',
	'description_color' 						=> '',
	'description_color_hover' 					=> '',
	'content_popover' 							=> '',
	'content_color' 							=> '',
	'content_color_hover' 						=> '',
	'customize_buttons' 						=> '',
	'extra_button_color' 						=> '',
	'extra_button_color_font' 					=> '',
	'extra_button_color_font_hover' 			=> '',
	'extra_button_color_background' 			=> '',
	'extra_button_color_background_hover' 		=> '',
	'button_color' 								=> '',
	'button_color_font' 						=> '',
	'button_color_font_hover' 					=> '',
	'button_color_font_block_hover' 			=> '',
	'button_color_background' 					=> '',
	'button_color_background_hover' 			=> '',
	'button_color_background_block_hover' 		=> '',
), $settings ) );

$title 	= wp_kses( $title, array(
	'span'		=> array(),
	'mark' 		=> array(),
	'b'			=> array(),
	'strong'	=> array(),
	'br'		=> array()
), $title );

$out = "";
$section_id = uniqid( 'cws_pricing_plan' );

$section_class = "cws_pricing_plan_wrapper cws_module";
$section_class .= !empty( $gifts_cards_html ) && !empty( $button ) ? " cws_flex_column_sb" : "";


$out .= "<div id='".$section_id."' class='".$section_class."'>";
	$out .= "<div class='cws_pricing_plan_item'>";

		//Pricing plan headers
		if( !empty($subtitle) || !empty($title) ){
			$out .= "<div class='cws_pricing_plan_headers'>";
				if( !empty($subtitle) ){
					$out .= "<".$subtitles_typography_html_tag." class='cws_pricing_plan_subtitle'>".$subtitle."</".$subtitles_typography_html_tag.">";
				}
				if( !empty($title) ){
					$out .= "<".$title_typography_html_tag." class='cws_pricing_plan_title'>".$title."</".$title_typography_html_tag.">";
				}
			$out .= "</div>";
		}

		//Pricing plan extra button
		if( $add_extra_button == 'yes' ){
			$out .= "<a href='".esc_url($button_url['url'])."' class='cws_pricing_plan_extra_button'>".$extra_button_title."</a>";
		}

		//Pricing plan price section
		if( !empty($price_subtitle) || !empty($price) || !empty($price_description) ){
			$out .= "<div class='cws_pricing_plan_price_wrapper'>";
				if( !empty($price_subtitle) ){
					$out .= "<".$subtitles_typography_html_tag." class='cws_pricing_plan_subtitle'>".$price_subtitle."</".$subtitles_typography_html_tag.">";
				}
				if( !empty($price) ){
					!empty( $price_currency ) ? $currency = "<i class='cws_pricing_plan_price_currency'>".$price_currency."</i>" : "";
					$out .= "<p class='cws_pricing_plan_price'>".$price.$currency."</p>";
				}
				if( !empty($price_description) ){
					$out .= "<p class='cws_pricing_plan_price_description'>".$price_description."</p>";
				}
			$out .= "</div>";
		}

		//Pricing plan extra content section
		if( !empty($content_subtitle) || !empty($content) ){
			$out .= "<div class='cws_pricing_plan_content_wrapper'>";
				if( !empty($content_subtitle) ){
					$out .= "<p class='cws_pricing_plan_subtitle'>".$content_subtitle."</p>";
				}
				if( !empty($content) ){
					$out .= "<p class='cws_pricing_plan_content'>".$content."</p>";
				}
			$out .= "</div>";	
		}

		//Pricing plan main button
		$out .= "<a href='".esc_url($button_url['url'])."' class='cws_pricing_plan_main_button button'>".$button_text."</a>";
		
	$out .= "</div>";
$out .= "</div>";

echo sprintf("%s", $out);