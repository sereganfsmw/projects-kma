<?php
global $cws_theme_funcs;

//Colors
$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

extract( shortcode_atts( array(
	'text_alignment'					=> '',
	'add_divider'						=> '',
	'add_button'						=> '',
	'button_float'						=> '',
	'button_float_tablet'				=> '',
	'button_float_mobile'				=> '',
	'button_text'						=> '',
	'button_url'						=> array(),
	'title_banners'						=> '',
	'title_typography'					=> '',
	'desc_banners'						=> '',
	'description_typography'			=> '',
	'discount_banners'					=> '',
	'discount_typography'				=> '',
	'content'							=> '',
	'customize_colors'					=> '',
	'overlay_color'						=> '',
	'title_color'						=> '',
	'divider_color'						=> '',
	'subtitle_color'					=> '',
	'content_color'						=> '',
	'custom_button_color'				=> '',
	'custom_button_background'			=> '',
	'custom_button_hover_color'			=> '',
	'custom_button_hover_background'	=> '',
), $settings ) );

$title 	= wp_kses( $title, array(
	'span'		=> array(),
	'mark' 		=> array(),
	'b'			=> array(),
	'strong'	=> array(),
	'br'		=> array()
	), $title
);

$out = "";
$section_id = uniqid( 'cws_banners_' );

$hasDivider = '';
if ( $add_divider != false ){
	$hasDivider = "has_divider";
}

ob_start();
	echo !empty( $title_banners ) ? ("<div class='banners_title_wrapper ". $hasDivider ." '><p class='banners_title'>".esc_html($title_banners)."</p>". ($add_divider != false ? "<i class='banner-divider'></i>" : "")."</div>") : "";
	echo !empty( $desc_banners ) ? "<p class='banners_desc'>".esc_html($desc_banners)."</p>" : "";
	$content = apply_filters( 'the_content', $content );
	echo !empty( $content ) ? "<div class='banners_content'>$content</div>" : "";
$banners_body = ob_get_clean();

ob_start();
	if ( !empty( $banners_body ) ){
		echo "<div class='banners_body'>";
			echo sprintf("%s", $banners_body);
			echo (!empty($discount_banners)) ? "<p class='discount_price'>".esc_html($discount_banners)."</p>" : ""; 	
		echo "</div>";
		if ( $add_button && !empty( $button_text ) && !empty( $button_url['url'] ) ){
			echo "<div class='banner_button'><a href='".esc_url($button_url['url'])."' class='button'" . ( $button_url['is_external'] == '1' ? " target='_blank'" : "" ) . ">".esc_html($button_text)."</a></div>";
		}
	}
$banners = ob_get_clean();

$section_class = "cws_banners cws_module";
$section_class = "cws_banners cws_module desktop_float-".$button_float." tablet_float-".$button_float_tablet." mobile_float-".$button_float_mobile . (!empty($add_button) ? " has_button" : "" ) . "";
$section_class .= !empty( $el_class ) ? " $el_class" : "";

$out .= "<div id='".esc_attr($section_id)."' class='".esc_attr($section_class)."'>";
	$out .= !empty($banners) ? $banners : "";
$out .= "</div>";

echo sprintf("%s", $out);