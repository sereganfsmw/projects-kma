<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//Settings
$button_type =						($type == 'php') ? $settings['button_type'] : 					'{{{settings.button_type}}}';
$text =								($type == 'php') ? $settings['text'] : 							'{{{settings.text}}}';

$link =								($type == 'php') ? $settings['link'] : 							'{{{settings.link}}}';
$link_url =							($type == 'php') ? $settings['link']['url'] : 					'{{{settings.link.url}}}';
$link_is_external =					($type == 'php') ? $settings['link']['is_external'] : 			'{{{settings.link.is_external}}}';
$link_nofollow =					($type == 'php') ? $settings['link']['nofollow'] : 				'{{{settings.link.nofollow}}}';

// $align =							($type == 'php') ? $settings['align'] : 						'{{{settings.align}}}';
$size =								($type == 'php') ? $settings['size'] : 							'{{{settings.size}}}';
$icon =								($type == 'php') ? $settings['icon'] : 							'{{{settings.icon}}}';
$icon_align =						($type == 'php') ? $settings['icon_align'] : 					'{{{settings.icon_align}}}';
$view =								($type == 'php') ? $settings['view'] : 							'{{{settings.view}}}';
$hover_animation =					($type == 'php') ? $settings['hover_animation'] : 				'{{{settings.hover_animation}}}';

//=======================RENDER TYPE=======================
$js_settings = '';
//-----------PHP-----------
if ($type == 'php'){


	$render->add_render_attribute( 'wrapper', 'class', 'elementor-button-wrapper' );

	if ( ! empty( $link_url ) ) {
		$render->add_render_attribute( 'button', 'href', $link_url );
		$render->add_render_attribute( 'button', 'class', 'elementor-button-link' );

		if ( $link_is_external ) {
			$render->add_render_attribute( 'button', 'target', '_blank' );
		}

		if ( $link_nofollow ) {
			$render->add_render_attribute( 'button', 'rel', 'nofollow' );
		}
	}

	$render->add_render_attribute( 'button', 'class', 'elementor-button' );

	if ( ! empty( $size ) ) {
		$render->add_render_attribute( 'button', 'class', 'elementor-size-' . $size );
	}

	if ( $hover_animation ) {
		$render->add_render_attribute( 'button', 'class', 'elementor-animation-' . $hover_animation );
	}

	$render->add_render_attribute( 'content-wrapper', 'class', 'elementor-button-content-wrapper' );
	$render->add_render_attribute( 'icon-align', 'class', 'elementor-align-icon-' . $icon_align );
	$render->add_render_attribute( 'icon-align', 'class', 'elementor-button-icon' );

	$render->add_render_attribute( 'text', 'class', 'elementor-button-text' );


//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
elseif ($type == 'js') {


	$js_settings = "
		<#

		view.addRenderAttribute( 'wrapper', 'class', 'elementor-button-wrapper' );

		view.addRenderAttribute( 'button', {
			'class': [ 'elementor-button', 'elementor-size-' + ".js_var($size).", 'elementor-animation-' + ".js_var($hover_animation)." ],
			'href': ".js_var($link_url)."
		} );

		view.addRenderAttribute( 'content-wrapper', 'class', 'elementor-button-content-wrapper' );

		view.addRenderAttribute( 'icon-align', {
			'class': [ 'elementor-button-icon', 'elementor-align-icon-' + ".js_var($icon_align)." ]
		} );

		view.addRenderAttribute( 'text', 'class', 'elementor-button-text' );

		view.addInlineEditingAttributes( 'text', 'none' );
		#>
	";


}
//-----------/JS (BACKBONE)-----------

//Render attr
$attr_wrapper = 		($type == 'php') ? $render->get_render_attribute_string( 'wrapper' ) 			: "{{{ view.getRenderAttributeString( 'wrapper' ) }}}";
$attr_button = 			($type == 'php') ? $render->get_render_attribute_string( 'button' ) 			: "{{{ view.getRenderAttributeString( 'button' ) }}}";
$attr_content_wrapper = ($type == 'php') ? $render->get_render_attribute_string( 'content-wrapper' ) 	: "{{{ view.getRenderAttributeString( 'content-wrapper' ) }}}";
$attr_icon_align = 		($type == 'php') ? $render->get_render_attribute_string( 'icon-align' ) 		: "{{{ view.getRenderAttributeString( 'icon-align' ) }}}";
$attr_text = 			($type == 'php') ? $render->get_render_attribute_string( 'text' ) 				: "{{{ view.getRenderAttributeString( 'text' ) }}}";


$module_id = uniqid( "cws_button_" );
$out = "";

ob_start();
?>
	<span <?php echo $attr_icon_align; ?>>
		<i class="<?php echo $icon; ?>" aria-hidden="true"></i>
	</span>
<?php				
$icon_html = ob_get_clean();

ob_start();
	print $js_settings;

	?>
	<div <?php echo $attr_wrapper; ?>>
		<a <?php echo $attr_button; ?>>
			<span <?php echo $attr_content_wrapper; ?>>
				<?php 
					if ($type == 'php'){
						if ( ! empty( $icon ) ) {
							echo $icon_html;
						}
					} elseif ($type == 'js') {
						echo "<# if ( ".js_var($icon)." ) { #>";
							echo $icon_html;
						echo "<# } #>";
					}
				?>
				<span <?php echo $attr_text; ?>><?php echo $text; ?></span>
			</span>
		</a>
	</div>
	<?php
$out .= ob_get_clean();

echo sprintf("%s", $out);