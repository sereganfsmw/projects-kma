<?php
function cwsfw_get_sections() {
	$l_components = cwsfw_get_local_components();
	$g_components = array();
	if (function_exists('cws_core_get_base_components')) {
		$g_components = cws_core_get_base_components();
		$g_components = cws_core_merge_components($g_components, $l_components);
	}

	$settings = cws_get_to_settings();

	//Show field if WPML plugin active
	if ( is_plugin_active('sitepress-multilingual-cms/sitepress.php') )  {
		$settings['general_setting']['layout']['menu_cont']['layout']['menu_box']['layout']['language_bar'] = array(
			'title' => esc_html__( 'Add Language Bar to menu', 'cryptop' ),
			'addrowclasses' => 'checkbox grid-col-6',
			'type' => 'checkbox',
		);
	}

	//Show TAB if WooCommerce plugin active
	if ( is_plugin_active('woocommerce/woocommerce.php') )  {
		$settings['woo_options'] = array(
			'type'		=> 'section',
			'title'		=> esc_html__( 'WooCommerce', 'cryptop' ),
			'icon'		=> array('fa', 'shopping-cart'),
			'layout'	=> array(
				'woo_options' => array(
					'type' 	=> 'tab',
					'init'	=> 'open',
					'icon' 	=> array('fa', 'arrow-circle-o-up'),
					'title' => esc_html__( 'Woocommerce', 'cryptop' ),
					'layout' => array(
						'woo_cart_enable'	=> array(
							'title'			=> esc_html__( 'Show WooCommerce Cart', 'cryptop' ),
							'type'			=> 'checkbox',
							'addrowclasses'	=> 'checkbox alt grid-col-12',
							'atts' => 'checked data-options="e:woo_cart_place;"',
						),
						'woo_cart_place' => array(
							'title' => esc_html__( 'WooCommerce Cart position', 'cryptop' ),
							'type' => 'radio',
							'subtype' => 'images',
							'addrowclasses' => 'disable grid-col-12',
							'value' => array(
								'top' =>array( esc_html__( 'TopBar', 'cryptop' ), false, '', '/img/woo-cart-top-right.png' ),
								'left' =>array( esc_html__( 'Menu (Left)', 'cryptop' ), false, '', '/img/woo-cart-menu-left.png' ),
								'right' =>array( esc_html__( 'Menu (Right)', 'cryptop' ), true, '', '/img/woo-cart-menu-right.png' ),
							),
						),
						'woo_sb_layout' => array(
							'title' => esc_html__('Sidebar Position', 'cryptop' ),
							'type' => 'radio',
							'subtype' => 'images',
							'addrowclasses' => 'grid-col-12',
							'value' => array(
								'left' => 	array( esc_html__('Left', 'cryptop' ), false, 'e:woo_sidebar;',	'/img/left.png' ),
								'right' => 	array( esc_html__('Right', 'cryptop' ), true, 'e:woo_sidebar;', '/img/right.png' ),
								'none' => 	array( esc_html__('None', 'cryptop' ), false, 'd:woo_sidebar;', '/img/none.png' )
							),
						),
						'woo_sidebar' => array(
							'title' => esc_html__('Select a sidebar', 'cryptop' ),
							'type' => 'select',
							'addrowclasses' => 'disable grid-col-12',
							'source' => 'sidebars',
						),	
						'woo_sb_layout_single' => array(
							'title' => esc_html__('Sidebar Position Single', 'cryptop' ),
							'type' => 'radio',
							'subtype' => 'images',
							'addrowclasses' => 'grid-col-12',
							'value' => array(
								'left' => 	array( esc_html__('Left', 'cryptop' ), false, 'e:woo_sidebar_single;',	'/img/left.png' ),
								'right' => 	array( esc_html__('Right', 'cryptop' ), false, 'e:woo_sidebar_single;', '/img/right.png' ),
								'none' => 	array( esc_html__('None', 'cryptop' ), true, 'd:woo_sidebar_single;', '/img/none.png' )
							),
						),					
						'woo_sidebar_single' => array(
							'title' => esc_html__('Select a Single sidebar', 'cryptop' ),
							'type' => 'select',
							'addrowclasses' => 'disable grid-col-12',
							'source' => 'sidebars',
						),
						'woo_columns' => array(
							'type' => 'select',
							'title' => esc_html__( 'Columns layout', 'cryptop' ),
							'addrowclasses' => 'grid-col-6',
							'source' => array(
								'2' => array('Two Columns',false, ''),
								'3' => array('Three Columns',true, ''),
								'4' => array('Four Columns',false, '')
							),
						),
						'woo_num_products'	=> array(
							'title'			=> esc_html__( 'Products per page', 'cryptop' ),
							'type'			=> 'number',
							'addrowclasses' => 'grid-col-6',
							'value'			=> get_option( 'posts_per_page' )
						),
						'woo_related_columns' => array(
							'type' => 'select',
							'title' => esc_html__( 'Columns layout (Related)', 'cryptop' ),
							'addrowclasses' => 'grid-col-6',
							'source' => array(
								'2' => array('Two Columns',false, ''),
								'3' => array('Three Columns',true, ''),
								'4' => array('Four Columns',false, '')
							),
						),						
						'woo_related_num_products'	=> array(
							'title'			=> esc_html__( 'Products per page (Related)', 'cryptop' ),
							'type'			=> 'number',
							'addrowclasses' => 'grid-col-6',
							'value'			=> get_option( 'posts_per_page' )
						),
						'shop_slider_type' => array(
							'title' => esc_html__('Slider', 'cryptop' ),
							'type' => 'radio',
							'addrowclasses' => 'grid-col-12',
							'value' => array(
								'none' => 	array( esc_html__('None', 'cryptop' ), true, 'd:shop_slider_shortcode;d:shop_video_slider;d:shop_static_image' ),
								'img-slider'=>	array( esc_html__('Image Slider', 'cryptop' ), false, 'e:shop_slider_shortcode;d:shop_video_slider;d:shop_static_image' ),
								'video-slider' => 	array( esc_html__('Video Slider', 'cryptop' ), false, 'd:shop_slider_shortcode;e:shop_video_slider;d:shop_static_image' ),
								'stat-img-slider' => 	array( esc_html__('Static image', 'cryptop' ), false, 'd:shop_slider_shortcode;d:shop_video_slider;e:shop_static_image' ),
							),
						),
						'shop_slider_shortcode' => array(
							'title' => esc_html__( 'Slider shortcode', 'cryptop' ),
							'addrowclasses' => 'disable grid-col-12',
							'type' => 'text',
							'value' => '[rev_slider shoppage]',
						),
						'shop_video_slider' => array(
							'title' => esc_html__( 'Video Slider Setting', 'cryptop' ),
							'type' => 'fields',
							'addrowclasses' => 'disable groups',
							'layout' => '%video_slider_layout%',
						),// end of video-section
						'shop_static_image' => array(
							'title' => esc_html__( 'Static image Slider Setting', 'cryptop' ),
							'type' => 'fields',
							'addrowclasses' => 'groups',
							'layout' => '%static_image_layout%',
						),// end of static img slider-section
					)
				),
				'woo_menu_options' => array(
					'type' 	=> 'tab',
					'icon' 	=> array('fa', 'arrow-circle-o-up'),
					'title' => esc_html__( 'Menu', 'cryptop' ),
					'layout' => array(
						'woo_customize_menu'	=> array(
							'title'	=> esc_html__( 'Customize WooCommerce Menu', 'cryptop' ),
							'type'	=> 'checkbox',
							'addrowclasses' => 'checkbox alt grid-col-12',
							'atts' => 'data-options="e:show_menu_bg_color;e:woo_menu_font_color;e:woo_menu_font_hover_color;e:woo_menu_border_color;e:woo_header_covers_slider"',		
						),							
						'show_menu_bg_color' => array(
							'title' => esc_html__( 'Add Background Color', 'cryptop' ),
							'addrowclasses' => 'checkbox disable',
							'type' => 'checkbox',
							'atts' => 'data-options="e:woo_menu_opacity;e:woo_menu_bg_color"',
						),
						'woo_menu_opacity' => array(
							'title' 		=> esc_html__( 'Opacity', 'cryptop' ),
							'tooltip' => array(
								'title' => esc_html__( 'Menu Opacity', 'cryptop' ),
								'content' => esc_html__( 'This option will apply a transparent header when set to 0. Options available from 0 to 100', 'cryptop' ),
							),								
							'type' 			=> 'number',
							'addrowclasses' => 'grid-col-6 disable',
							'atts' 			=> " min='0' max='100'",
							'value'			=> '100'
						),
						'woo_menu_bg_color' => array(
							'title' 		=> esc_html__( 'Background Color', 'cryptop' ),
							'tooltip' => array(
								'title' => esc_html__( 'Background Color', 'cryptop' ),
								'content' => esc_html__( 'Change the background color of the menu and logo area.', 'cryptop' ),
							),							
							'type' 			=> 'text',
							'addrowclasses' => 'grid-col-6 disable',
							'atts' 			=> 'data-default-color="#f8f8f8"',
							'value'			=> '#f8f8f8'
						),
						'woo_menu_font_color' => array(
							'title' 		=> esc_html__( 'Override Font Color', 'cryptop' ),
							'tooltip' => array(
								'title' => esc_html__( 'Override Font Color', 'cryptop' ),
								'content' => esc_html__( 'This color is applied to the main menu only, sub-menu items will use the color which is set in Typography section.<br /> This option is very useful when menu and logo covers title area or slider.', 'cryptop' ),
							),							
							'type' 			=> 'text',
							'addrowclasses' => 'grid-col-6 disable',
							'atts' 			=> 'data-default-color="#fff;"',
							'value'			=> '#fff'
						),
						'woo_menu_font_hover_color' => array(
							'title' 		=> esc_html__( 'Override Font Hover Color', 'cryptop' ),
							'tooltip' => array(
								'title' => esc_html__( 'Override Font Color', 'cryptop' ),
								'content' => esc_html__( 'This color is applied to the menu items on hover in main menu only, sub-menu items will use the color which is set in Typography section.<br /> This option is very useful when menu and logo covers title area or slider.', 'cryptop' ),
							),							
							'type' 			=> 'text',
							'addrowclasses' => 'grid-col-6 disable',
							'atts' 			=> 'data-default-color="'.CRYPTOP_FIRST_COLOR.'"',
							'value'			=> CRYPTOP_FIRST_COLOR
						),					
						'woo_menu_border_color' => array(
							'title' 		=> esc_html__( 'Override Border Color', 'cryptop' ),
							'tooltip' => array(
								'title' => esc_html__( 'Override Border Color', 'cryptop' ),
								'content' => esc_html__( 'This color is applied to the main menu only, sub-menu items will use the color which is set in Typography section.<br /> This option is very useful when menu and logo covers title area or slider.', 'cryptop' ),
							),							
							'type' 			=> 'text',
							'addrowclasses' => 'grid-col-12 disable',
							'atts' 			=> 'data-default-color="#fff;"',
							'value'			=> '#fff'
						),
						'woo_header_covers_slider' => array(
							'title' => esc_html__( 'Header Overlays Slider', 'cryptop' ),
							'tooltip' => array(
								'title' => esc_html__( 'Menu Overlays Slider', 'cryptop' ),
								'content' => esc_html__( 'This option will force the menu and logo sections to overlay the title area. <br> It is useful when using transparent menu.', 'cryptop' ),
							),							
							'type' => 'checkbox',
							'addrowclasses' => 'checkbox grid-col-12 disable'
						),		
						'woo_customize_logotype'	=> array(
							'title'	=> esc_html__( 'Customize WooCommerce Logotype', 'cryptop' ),
							'type'	=> 'checkbox',
							'addrowclasses' => 'checkbox alt grid-col-12',
							'atts' => 'data-options="e:logo_woo"',		
						),
						'logo_woo' => array(
							'title' => esc_html__( 'Logotype Woocommerce', 'cryptop' ),
							'type' => 'media',
							'url-atts' => 'readonly',
							'addrowclasses' => 'grid-col-12 disable',
							'layout' => array(
								'is_high_dpi' => array(
									'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
									'addrowclasses' => 'checkbox',
									'type' => 'checkbox',
								),
							),
						),
					)
				),
				'woo_title_options' => array(
					'type' 	=> 'tab',
					'icon' 	=> array('fa', 'arrow-circle-o-up'),
					'title' => esc_html__( 'Title Area', 'cryptop' ),
					'layout' => array(
						'woo_customize_title'	=> array(
							'title'	=> esc_html__( 'Customize WooCommerce Title Area', 'cryptop' ),
							'type'	=> 'checkbox',
							'atts' => 'data-options="e:woo_hide_title"',
							'addrowclasses' => 'checkbox alt grid-col-12'		
						),	
						'woo_hide_title'	=> array(
							'title'	=> esc_html__( 'Switch on/off the title area', 'cryptop' ),
							'type'	=> 'checkbox',
							'atts' => 'data-options="e:woo_page_title_spacings;e:woo_default_header_image;e:woo_header_font_color;e:woo_color_overlay_type;e:woo_header_center;e:woo_breadcrumbs_divider;e:woo_breadcrumbs_dimensions;e:woo_breadcrumbs-margin"',
							'addrowclasses' => 'checkbox alt grid-col-12 disable'		
						),	
						'woo_page_title_spacings' => array(
							'title' => esc_html__( 'Add Spacings (px)', 'cryptop' ),
							'type' => 'margins',
							'value' => array(
								'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '60'),
								'left' => array('placeholder' => esc_html__( 'left', 'cryptop' ), 'value' => '0'),
								'right' => array('placeholder' => esc_html__( 'Right', 'cryptop' ), 'value' => '0'),
								'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '60'),
							),
							'addrowclasses' => 'grid-col-6 disable'
						),
						'woo_default_header_image'	=> array(
							'title'	=> esc_html__( 'Add Background Image', 'cryptop' ),
							'addrowclasses' => 'grid-col-6 disable',
							'type'	=> 'media'
						),
						'woo_header_font_color' => array(
							'title' 			=> esc_html__( 'Override Font Color', 'cryptop' ),
							'atts' 				=> 'data-default-color=""',
							'type' 				=> 'text',
							'addrowclasses' 	=> 'grid-col-12 disable',
							'value'				=> 	""
						),
						'woo_color_overlay_type'	=> array(
							'title'		=> esc_html__( 'Color Overlay', 'cryptop' ),
							'addrowclasses' => 'grid-col-12 disable',
							'type'	=> 'select',
							'source'	=> array(
								'none' => array( esc_html__( 'None', 'cryptop' ),  true, 'd:woo_color_overlay_opacity;d:woo_overlay_color;d:woo_gradient_settings;' ),
								'color' => array( esc_html__( 'Color', 'cryptop' ),  false, 'e:woo_color_overlay_opacity;e:woo_overlay_color;d:woo_gradient_settings;' ),
								'gradient' => array( esc_html__( 'Gradient', 'cryptop' ), false, 'e:woo_color_overlay_opacity;d:woo_overlay_color;e:woo_gradient_settings;' )
								),
						),
						'woo_color_overlay_opacity' => array(
							'type' => 'number',
							'addrowclasses' => 'disable grid-col-12',
							'title' => esc_html__( 'Opacity', 'cryptop' ),
							'placeholder' => esc_html__( 'In percents', 'cryptop' ),
							'value' => '40'
						),
						'woo_overlay_color'	=> array(
							'title'	=> esc_html__( 'Overlay color', 'cryptop' ),
							'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
							'addrowclasses' => 'disable grid-col-12',
							'value' => CRYPTOP_FIRST_COLOR,
							'type'	=> 'text'
						),
						'woo_gradient_settings' => array(
							'title' => esc_html__( 'Gradient Settings', 'cryptop' ),
							'type' => 'fields',
							'addrowclasses' => 'disable box inside-box groups grid-col-12',
							'layout' => array(
								'first_color' => array(
									'type' => 'text',
									'title' => esc_html__( 'From', 'cryptop' ),
									'atts' => 'data-default-color=""',
								),
								'second_color' => array(
									'type' => 'text',
									'title' => esc_html__( 'To', 'cryptop' ),
									'atts' => 'data-default-color=""',
								),
								'first_color_opacity' => array(
									'type' => 'number',
									'title' => esc_html__( 'From (Opacity %)', 'cryptop' ),
									'value' => '100',
								),
								'second_color_opacity' => array(
									'type' => 'number',
									'title' => esc_html__( 'To (Opacity %)', 'cryptop' ),
									'value' => '100',
								),
								'type' => array(
									'title' => esc_html__( 'Gradient type', 'cryptop' ),
									'type' => 'radio',
									'value' => array(
										'linear' => array( esc_html__( 'Linear', 'cryptop' ),  true, 'e:linear_settings;d:radial_settings' ),
										'radial' =>array( esc_html__( 'Radial', 'cryptop' ), false,  'd:linear_settings;e:radial_settings' ),
									),
								),
								'linear_settings' => array(
									'title' => esc_html__( 'Linear settings', 'cryptop'  ),
									'type' => 'fields',
									'addrowclasses' => 'disable',
									'layout' => array(
										'angle' => array(
											'type' => 'number',
											'title' => esc_html__( 'Angle', 'cryptop' ),
											'value' => '45',
										),
									)
								),
								'radial_settings' => array(
									'title' => esc_html__( 'Radial settings', 'cryptop'  ),
									'type' => 'fields',
									'addrowclasses' => 'disable',
									'layout' => array(
										'shape_settings' => array(
											'title' => esc_html__( 'Shape', 'cryptop' ),
											'type' => 'radio',
											'value' => array(
												'simple' => array( esc_html__( 'Simple', 'cryptop' ),  true, 'e:shape;d:size;d:size_keyword;' ),
												'extended' =>array( esc_html__( 'Extended', 'cryptop' ), false, 'd:shape;e:size;e:size_keyword;' ),
											),
										),
										'shape' => array(
											'title' => esc_html__( 'Gradient type', 'cryptop' ),
											'type' => 'radio',
											'value' => array(
												'ellipse' => array( esc_html__( 'Ellipse', 'cryptop' ),  true ),
												'circle' =>array( esc_html__( 'Circle', 'cryptop' ), false ),
											),
										),
										'size_keyword' => array(
											'type' => 'select',
											'title' => esc_html__( 'Size keyword', 'cryptop' ),
											'addrowclasses' => 'disable',
											'source' => array(
												'closest-side' => array(esc_html__( 'Closest side', 'cryptop' ), false),
												'farthest-side' => array(esc_html__( 'Farthest side', 'cryptop' ), false),
												'closest-corner' => array(esc_html__( 'Closest corner', 'cryptop' ), false),
												'farthest-corner' => array(esc_html__( 'Farthest corner', 'cryptop' ), true),
											),
										),
										'size' => array(
											'type' => 'text',
											'addrowclasses' => 'disable',
											'title' => esc_html__( 'Size', 'cryptop' ),
											'atts' => 'placeholder="'.esc_html__( 'Two space separated percent values, for example (60% 55%)', 'cryptop' ).'"',
										),
									)
								),
							)
						),
						'woo_header_center' => array(
							'title' => esc_html__( 'Center Title & Breadcrumbs', 'cryptop' ),
							'addrowclasses' => 'grid-col-6 disable',
							'type' => 'checkbox',
						),
						'woo_breadcrumbs_divider' => array(
							'title' => esc_html__( 'Breadcrumbs Divider', 'cryptop' ),
							'type' => 'media',
							'url-atts' => 'readonly',
							'addrowclasses' => 'grid-col-12',
							'layout' => array(
								'is_high_dpi' => array(
									'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
									'addrowclasses' => 'checkbox',
									'type' => 'checkbox',
								),
							),
						),
						'woo_breadcrumbs_dimensions' => array(
							'title' => esc_html__( 'Breadcrumbs Divider Dimensions', 'cryptop' ),
							'type' => 'dimensions',
							'addrowclasses' => 'disable grid-col-12',
							'value' => array(
								'width' => array('placeholder' => esc_html__( 'Width', 'cryptop' ), 'value' => ''),
								'height' => array('placeholder' => esc_html__( 'Height', 'cryptop' ), 'value' => ''),
								),
						),
						'woo_breadcrumbs-margin' => array(
							'title' => esc_html__( 'Margins (px)', 'cryptop' ),
							'type' => 'margins',
							'addrowclasses' => 'disable grid-col-4',
							'value' => array(
								'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '0'),
								'left' => array('placeholder' => esc_html__( 'left', 'cryptop' ), 'value' => '0'),
								'right' => array('placeholder' => esc_html__( 'Right', 'cryptop' ), 'value' => '0'),
								'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '0'),
							),
						),
					)
				)
			)
		);
	}
	if (function_exists('cws_core_build_settings')) {
		cws_core_build_settings($settings, $g_components);
	}
	return $settings;
}

/*
	here local or overrided components can be added/changed
*/
function cwsfw_get_local_components() {
	return array();
}
?>