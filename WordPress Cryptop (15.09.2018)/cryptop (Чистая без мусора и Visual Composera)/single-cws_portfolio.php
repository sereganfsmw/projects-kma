<?php
	get_header();

	$pid = get_queried_object_id();
	$title = get_the_title();

	global $cws_theme_funcs;
	if ($cws_theme_funcs){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$fixed_header = $cws_theme_funcs->cws_get_meta_option( 'fixed_header' );
		$class = $sb['layout_class'].' '. $sb['sb_class'].' '.$fixed_header;
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);
	}

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$full_width = isset( $post_meta['full_width'] ) ? $post_meta['full_width'] : false;
	$carousel = isset( $post_meta['carousel'] ) ? $post_meta['carousel'] : false;
	$show_related = isset( $post_meta['show_related'] ) ? $post_meta['show_related'] : false;
	$rpo_title = isset( $post_meta['rpo_title'] ) ? esc_html( $post_meta['rpo_title'] ) : "";
	$rpo_items_count = isset( $post_meta['rpo_items_count'] ) ? esc_textarea( $post_meta['rpo_items_count'] ) : esc_textarea( get_option( "posts_per_page" ) );
	$rpo_cols = isset( $post_meta['rpo_cols'] ) ? esc_textarea( $post_meta['rpo_cols'] ) : 4;
	$rpo_categories = isset( $post_meta['rpo_categories'] ) ? $post_meta['rpo_categories'] : '';
	//--Meta vars

	if(is_array($rpo_categories)){
		if(empty($rpo_categories[0])){
			unset($rpo_categories[0]);
		}
	}

	$page_class = '';
	$page_class .= (isset($sb) ? $sb['sb_class'] : 'page_content');
	$page_class .= $full_width ? ' full_width_featured' : '';

	printf("<div class='%s'>", $page_class);
	echo (isset($sb['content']) && !empty($sb['content'])) ? $sb['content'] : '';

	$cats = get_the_terms( $pid, 'cws_portfolio_cat' );
	$cats = $cats ? $cats : array(); 
	$cat_slugs = array();
	foreach( $cats as $cat ){
		$cat_slugs[] = $cat->slug;
	}
	$has_cats = !empty( $cat_slugs );

	$related_posts = array();
	$has_related = false;

	if ( $has_cats ){
		$query_args = array(
			'post_type' => 'cws_portfolio',
		);
		$query_args['tax_query'] = array( array(
			'taxonomy' => 'cws_portfolio_cat',
			'field' => 'slug',
			'terms' => $cat_slugs
		));
		$query_args["post__not_in"] = array( $pid );
		if ( $rpo_items_count ){
			$query_args['posts_per_page'] = $rpo_items_count;
		}
		$q = new WP_Query( $query_args );
		$related_posts = $q->posts;
		if ( count( $related_posts ) > 0 ){
			$has_related = true;
		}
	}

	$use_related_carousel = ($carousel == 1) && $has_related;
	$show_related_items  = ($show_related == 1) && $has_related;

	//Classes
	$section_class = "cws_portfolio single";
	$section_class .= $use_related_carousel ? " related" : "";

	//Post Parts
	ob_start();
		echo "<div class='full_width_media'>";
			cws_portfolio_single_media();
		echo "</div>";
	$media_part = ob_get_clean();
	//--Post Parts
	?>
	<main>
		<?php
		if ( $full_width ) {
			echo sprintf("%s", $media_part);
		}
		?>
		<div class="grid_row single_portfolio">
			<section class="<?php echo esc_attr($section_class) ?>">
				<div class="cws_portfolio_single_wrapper"> 
					<?php

					$GLOBALS['cws_single_portfolio_atts'] = array(
						'sb_layout' => $sb['layout_class'],
					);
						while ( have_posts() ) : the_post();
							cws_portfolio_single_article();
						endwhile;
						wp_reset_postdata();
						unset( $GLOBALS['cws_single_portfolio_atts'] );
					?>
				</div>
				<?php
					if ( $use_related_carousel ){
						$related_ids = array();
						$previous = '';

						if ( wp_get_referer() )
						{
							$previous = wp_get_referer();
						}

						foreach ( $related_posts as $related_post ) {
							$related_ids[] = $related_post->ID;
						}
						array_unshift( $related_ids, $pid );
						$ajax_data = array(
							'current' => $pid,
							'initial' => $pid,
							'related_ids' => $related_ids
						);
						echo "<input type='hidden' id='cws_portfolio_single_ajax_data' value='" . esc_attr(json_encode( $ajax_data ) ) . "' />";
						?>
						<div class='ajax_carousel_nav clearfix'>
							<div class='prev_section'>
								<div class='prev'>
									<div class="wrap">
										<span><?php esc_html_e( 'Prev' , 'cryptop' ); ?></span>
									</div>
									<i class="fa fa-long-arrow-left"></i>
								</div>
							</div>
							<div class='next_section'>
								<div class='next'>
									<div class="wrap">
										<span><?php esc_html_e( 'Next' , 'cryptop' ); ?></span>
									</div>
									<i class="fa fa-long-arrow-right"></i>
								</div>
							</div>
						</div>
						<?php
					}
				?>
			</section>
		</div>
		<div class="grid_row related_portfolio">
			<?php
				if ( $show_related ){
					$terms = wp_get_post_terms( $pid, 'cws_portfolio_cat' );
					$term_slugs = array();
					for ( $i=0; $i < count( $terms ); $i++ ){
						$term = $terms[$i];
						$term_slug = $term->slug;
						array_push( $term_slugs, $term_slug );
					}
					$term_slugs = implode( ",", $term_slugs );
					if ( !empty( $term_slugs ) ){
						$rp_args = array(
							'title'							=> $rpo_title,
							'post_type'						=> 'cws_portfolio',
							'items_count'					=> $rpo_items_count,
							'display_style'					=> 'carousel',
							'layout'						=> $rpo_cols,
							'info_pos'						=> 'inside_img',
							'filter_by'						=> 'cws_portfolio_cat',
							'related_items'					=> $show_related_items,
							'related_carousel'				=> $use_related_carousel,
							'cws_portfolio_cat'				=> !empty($rpo_categories) ? $rpo_categories : $term_slugs,
							'show_meta'						=> $cws_theme_funcs->cws_get_meta_option( 'def_portfolio_show_meta' ),
							'addl_query_args'				=> array(
								'post__not_in'					=> array( $pid ),
							),
						);

						wp_enqueue_script( 'isotope' );
						wp_enqueue_script( 'imagesloaded' );
						wp_enqueue_script( 'fancybox' );
						wp_enqueue_script( 'owl_carousel' );

						$related_projects = cws_portfolio_output( $rp_args );
						if ( !empty( $related_projects ) ){
							echo sprintf("%s", $related_projects);
						}
					}
				}
			?>
		</div>
	</main>
	<?php echo isset($sb['content']) && !empty($sb['content']) ? '</div>' : ''; ?>
</div>

<?php
	get_footer();
?>