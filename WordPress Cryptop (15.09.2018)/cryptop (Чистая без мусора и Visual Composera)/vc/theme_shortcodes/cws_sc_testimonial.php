<?php
	// Map Shortcode in Visual Composer
	global $cws_theme_funcs;
	vc_map( array(
		"name"				=> esc_html__( 'Custom Testimonials', 'cryptop' ),
		"base"				=> "cws_sc_vc_testimonial",
		'category'			=> "By CWS",
		"icon"     			=> "cws_icon",
		"weight"			=> 80,
		"params"			=> array(
			array(
                'type' => 'param_group',
                'heading' => esc_html__( 'Quotes', 'cryptop' ),
                'param_name' => 'values',
                'description' => esc_html__( 'Enter values for graph - thumbnail, quote, author name and author status.', 'cryptop' ),
                'value' => urlencode( json_encode( array(
                    array(
                        'thumbnail' 		=> '',
                        'quote' 			=> '',
                        'url' 				=> '',
                        'author_name' 		=> 'John Doe',
                        'author_status' 	=> '',
                    ),
                ) ) ),
                'params' => array(
		            array(
						"type"			=> "attach_image",
						"heading"		=> esc_html__( 'Thumbnail', 'cryptop' ),
						"param_name"	=> "thumbnail",
					),
					array(
						"type"			=> "textarea",
						"heading"		=> esc_html__( 'Quote', 'cryptop' ),
						"param_name"	=> "quote",
					),
					array(
						"type"			=> "vc_link",
						"heading"		=> esc_html__( 'Link', 'cryptop' ),
						"param_name"	=> "url",
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( 'Author Name', 'cryptop' ),
						"param_name"	=> "author_name",
                        'admin_label' 	=> true,
					),
					array(
						"type"			=> "textfield",
						"heading"		=> esc_html__( 'Author Status', 'cryptop' ),
						"param_name"	=> "author_status",
					),
                ),
            ),
			array(
                "type"          => "dropdown",
                "heading"       => esc_html__( 'Testimonials Grid', 'cryptop' ),
                "param_name"    => "item_grid",
                "value"         => array(
                    esc_html__( 'One Column', 'cryptop' )    => '1',
                    esc_html__( 'Two Columns', 'cryptop' )   => '2',
                    esc_html__( 'Three Columns', 'cryptop' ) => '3',
                ),              
            ),           
            array(
                "type"          => "checkbox",
                "param_name"    => "use_carousel",
                "std"           => true,
                "value"         => array( esc_html__( 'Use Carousel', 'cryptop' ) => true )
            ),
            array(
                "type"          => "checkbox",
                "param_name"    => "border",
                "std"           => true,
                "value"         => array( esc_html__( 'Border', 'cryptop' ) => true ),
                "dependency"	=> array(
					"element"	=> "use_carousel",
					"not_empty"	=> true
				),                
            ),             
            array(
                "type"          => "checkbox",
                "param_name"    => "autoplay",
                "value"         => array( esc_html__( 'Autoplay', 'cryptop' ) => true ),
                "dependency"	=> array(
					"element"	=> "use_carousel",
					"not_empty"	=> true
				),
            ),
			array(
				"type"			=> "textfield",
				"heading"		=> esc_html__( 'Autoplay Speed', 'cryptop' ),
				"param_name"	=> "autoplay_speed",
                "dependency"	=> array(
					"element"	=> "autoplay",
					"not_empty"	=> true
				),
				"value" 		=> "3000"
			),
			array(
				"type"			=> "css_editor",
				"param_name"	=> "custom_styles",
				"group"			=> esc_html__( "Styling", 'cryptop' )
			),			
			array(
				"type"			=> "checkbox",
				"param_name"	=> "customize_colors",
				"group"			=> esc_html__( "Styling", 'cryptop' ),
				"value"			=> array( esc_html__( 'Customize Colors', 'cryptop' ) => true )
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Author Color', 'cryptop' ),
				"param_name"	=> "custom_author_color",
				"group"			=> esc_html__( "Styling", 'cryptop' ),
				"dependency"	=> array(
					"element"	=> "customize_colors",
					"not_empty"	=> true
				),
				"value"			=> '#ffffff'
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Status Color', 'cryptop' ),
				"param_name"	=> "custom_status_color",
				"group"			=> esc_html__( "Styling", 'cryptop' ),
				"dependency"	=> array(
					"element"	=> "customize_colors",
					"not_empty"	=> true
				),
				"value"			=> '#ffffff'
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Quote Color', 'cryptop' ),
				"param_name"	=> "custom_qoute_color",
				"group"			=> esc_html__( "Styling", 'cryptop' ),
				"dependency"	=> array(
					"element"	=> "customize_colors",
					"not_empty"	=> true
				),
				"value"			=> '#ffffff'
			),			
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Overlay Color', 'cryptop' ),
				"param_name"	=> "custom_overlay_color",
				"group"			=> esc_html__( "Styling", 'cryptop' ),
				"dependency"	=> array(
					"element"	=> "customize_colors",
					"not_empty"	=> true
				),
				"value"			=> ''
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Controls Color', 'cryptop' ),
				"param_name"	=> "custom_controls_color",
				"group"			=> esc_html__( "Styling", 'cryptop' ),
				"dependency"	=> array(
					"element"	=> "customize_colors",
					"not_empty"	=> true
				),
				"value"			=> '#ffffff'
			),			
			array(
				"type"				=> "textfield",
				"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
				"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
				"param_name"		=> "el_class",
				"value"				=> ""
			)
		)
	));

	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Testimonial extends WPBakeryShortCode {
	    }
	}
