<?php
	global $cws_theme_funcs;
	$def_chars_count = $cws_theme_funcs->cws_get_option( 'def_chars_count' );
	$def_chars_count = isset( $def_chars_count ) && is_numeric( $def_chars_count ) ? $def_chars_count : '';
	$post_type = "cws_classes";
	$params = array(
		array(
			"type"			=> "textfield",
			"admin_label"	=> true,
			"heading"		=> esc_html__( 'Title', 'cryptop' ),
			"param_name"	=> "title",
			"value"			=> "" 
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Title Alignment', 'cryptop' ),
			"param_name"	=> "title_align",
			"value"			=> array(
				esc_html__( "Left", 'cryptop' ) 	=> 'left',
				esc_html__( "Right", 'cryptop' )	=> 'right',
				esc_html__( "Center", 'cryptop' )	=> 'center'
			)		
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Layout', 'cryptop' ),
			"param_name"	=> "display_style",
			"value"			=> array(
								esc_html__( 'Grid', 'cryptop' ) => 'grid',
								esc_html__( 'Grid with Filter', 'cryptop' ) => 'filter',
								esc_html__( 'Carousel', 'cryptop' ) => 'carousel'
							)
		),		
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Class Variant', 'cryptop' ),
			"param_name"	=> "display_screen",
			"value"			=> array(
								esc_html__( 'Coach Avatar Bellow Content', 'cryptop' ) => 'style_1',
								esc_html__( 'Coach Avatar Above Content', 'cryptop' ) => 'style_2',
								esc_html__( 'Small Coach Avatar Bellow Content + Date', 'cryptop' ) => 'style_3'
							)
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Columns', 'cryptop' ),
			"param_name"	=> "layout",
			"value"			=> array(
				esc_html__( 'Default', 'cryptop' ) => 'def',
				esc_html__( 'One Column', 'cryptop' ) => '1',
				esc_html__( 'Two Columns', 'cryptop' ) => '2',
				esc_html__( 'Three Columns', 'cryptop' ) => '3',
				esc_html__( 'Four Columns', 'cryptop' ) => '4'
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'crop_images',
			'value'			=> array(
				esc_html__( 'Crop Images', 'cryptop' ) => true
			)
		),
	);
	$taxes = get_object_taxonomies ( 'cws_classes', 'object' );
	$avail_taxes = array(
		esc_html__( 'None', 'cryptop' )	=> '',
		esc_html__( 'Staff', 'cryptop' )	=> 'staff',
	);
	foreach ( $taxes as $tax => $tax_obj ){
		$tax_name = isset( $tax_obj->labels->name ) && !empty( $tax_obj->labels->name ) ? $tax_obj->labels->name : $tax;
		$avail_taxes[$tax_name] = $tax;
	}

	array_push( $params, array(
		"type"				=> "dropdown",
		"heading"			=> esc_html__( 'Filter by', 'cryptop' ),
		"param_name"		=> "tax",
		"value"				=> $avail_taxes
	));
	foreach ( $avail_taxes as $tax_name => $tax ) {
		if ($tax == 'staff'){
			$custom_post_type = 'cws_staff';
			global $wpdb;
    		$results = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type LIKE %s and post_status = 'publish'", $custom_post_type ) );
    		$titles_arr = array();
		    foreach( $results as $index => $post ) {
		    	$post_title = $post->post_title;
		        $titles_arr[$post_title] =  $post->ID;
		    }
			array_push( $params, array(
				"type"			=> "cws_dropdown",
				"multiple"		=> "true",
				"heading"		=> esc_html__( 'Titles', 'cryptop' ),
				"param_name"	=> "titles",
				"dependency"	=> array(
									"element"	=> "tax",
									"value"		=> 'staff'
								),
				"value"			=> $titles_arr
			));		
		} else {
			$terms = get_terms( $tax );
			$avail_terms = array(
				''				=> ''
			);
			if ( !is_a( $terms, 'WP_Error' ) ){
				foreach ( $terms as $term ) {
					$avail_terms[$term->name] = $term->slug;
				}
				array_push( $params, array(
					"type"			=> "cws_dropdown",
					"multiple"		=> "true",
					"heading"		=> $tax_name,
					"param_name"	=> "{$tax}_terms",
					"dependency"	=> array(
										"element"	=> "tax",
										"value"		=> $tax
									),
					"value"			=> $avail_terms
				));	
			}
		}			
	}
		array_push( $params, array(
		
			"type"			=> "checkbox",
			"param_name"	=> "customize_colors",
			"value"			=> array( esc_html__( 'Customize Colors', 'cryptop' ) => true )				
		)	
	);
	array_push( $params, array(	
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Title and More Button Color', 'cryptop' ),
			"param_name"	=> "custom_color",
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> $first_color
		)
	);
	array_push( $params, array(			
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Fill Color', 'cryptop' ),
			"param_name"	=> "bg_color",
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> CRYPTOP_FIRST_COLOR
		)
	);		
	array_push( $params, array(			
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Image Hover', 'cryptop' ),
			"param_name"	=> "hover_bg_color",
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> ""
		)
	);		

		array_push( $params, array(	
			"type" => "dropdown",
			"heading" => __("Avatar Border", 'cryptop'),
			"param_name"		=> "bg_hover_color",
			"value" => array(
				__("None", 'cryptop') => "none",
				__("Color", 'cryptop') => "color",
				__("Gradient", 'cryptop') => "gradient",
			),
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
		)
	);
	array_push( $params, array(	
			"type" => "colorpicker",
			"class" => "",
			"heading"		=> esc_html__( 'Color', 'cryptop' ),
			"param_name" => "hover_fill_color",
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'color',

			),
			"value"			=> $first_color
		)
	);
	array_push( $params, array(	
			"type" => "colorpicker",
			"class" => "",
			"heading"		=> esc_html__( 'From', 'cryptop' ),
			"param_name" => "cws_gradient_color_from",
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'gradient',

				),
			"value"			=> $first_color
		)
	);					
	array_push( $params, array(	
			"type" => "colorpicker",
			"class" => "",
			"heading"		=> esc_html__( 'To', 'cryptop' ),
			"param_name" => "cws_gradient_color_to",
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'gradient',

				),
			"value"			=> $first_color
		)
	);
	array_push( $params, array(	
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Type", 'cryptop'),
			"param_name"		=> "cws_gradient_type",
			"value" => array(
				__("Linear", 'cryptop') => "linear",
				__("Radial", 'cryptop') => "radial",
			),
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'gradient',

			),
		)
	);
	array_push( $params, array(	
			"type"			=> "textfield",
			"class" => "",
			"heading"		=> esc_html__( 'Angle', 'cryptop' ),
			"param_name"	=> "cws_gradient_angle",
			"value" => '360',
			"description"	=> esc_html__( 'Degrees: -360 to 360', 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "cws_gradient_type",
				'value' => 'linear',						
				),
		)
	);
	array_push( $params, array(	
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Shape variant", 'cryptop'),
			"param_name"		=> "cws_gradient_shape_variant_type",
			"value" => array(
				__("Simple", 'cryptop') => "simple",
				__("Extended", 'cryptop') => "extended",
				),
			"dependency"	=> array(
				"element"	=> "cws_gradient_type",
				'value' => 'radial',	
				),
		)
	);					
	array_push( $params, array(	
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Shape", 'cryptop'),
			"param_name"		=> "cws_gradient_shape_type",
			"value" => array(
				__("Ellipse", 'cryptop') => "ellipse",
				__("Circle", 'cryptop') => "circle",
				),
			"dependency"	=> array(
				"element"	=> "cws_gradient_shape_variant_type",
				'value' => 'simple',	
				),
		)
	);						
	array_push( $params, array(	
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Size keyword", 'cryptop'),
			"param_name"		=> "cws_gradient_size_keyword_type",
			"value" => array(
				__("Closest side", 'cryptop') => "closest_side",
				__("Farthest side", 'cryptop') => "farthest_side",
				__("Closest corner", 'cryptop') => "closest_corner",
				__("Farthest corner", 'cryptop') => "farthest_corner",
				),
			"dependency"	=> array(
				"element"	=> "cws_gradient_shape_variant_type",
				'value' => 'extended',	
				),
		)
	);						
	array_push( $params, array(	
			"type" => "textfield",
			"class" => "",
			"heading" => __("Size", 'cryptop'),
			"param_name"		=> "cws_gradient_size_type",
			"value" => '60% 55%',
			"description"	=> esc_html__( 'Two space separated percent values, for example (60% 55%)', 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "cws_gradient_shape_variant_type",
				'value' => 'extended',	
				),
		)
	);
	$params2 = array(	
		array(
			'type'			=> 'checkbox',
			'param_name'	=> $post_type . '_hide_meta_override',
			'value'			=> array(
				esc_html__( 'Hide Meta Data', 'cryptop' ) => true
			)
		),		
		array(
			'type'				=> 'cws_dropdown',
			'multiple'			=> "true",
			'heading'			=> esc_html__( 'Hide', 'cryptop' ),
			'param_name'		=> $post_type . '_hide_meta',
			'dependency'		=> array(
				'element'			=> $post_type . '_hide_meta_override',
				'not_empty'			=> true
			),
			'value'				=> array(
				esc_html__( 'None', 'cryptop' )		=> 'none',
				esc_html__( 'Title', 'cryptop' )		=> 'title',
				esc_html__( 'Excerpt', 'cryptop' )		=> 'excerpt',
				esc_html__( 'Categories', 'cryptop' )	=> 'cats',
				esc_html__( 'Teacher', 'cryptop' )	=> 'teach',
				esc_html__( 'Working Days', 'cryptop' )	=> 'working_days',
				esc_html__( 'Time Events', 'cryptop' )	=> 'time_events',
				esc_html__( 'Venue Events', 'cryptop' )	=> 'venue_events',
				esc_html__( 'Read More Button', 'cryptop' )	=> 'read_more',
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'change_btn',
			'value'			=> array(
				esc_html__( 'Change Details Button', 'cryptop' ) => true
			),
		),		
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Title Button', 'cryptop' ),
			"param_name"	=> "title_btn",
			"dependency" 	=> array(
								"element"	=> "change_btn",
								"not_empty"	=> true
							),
			"value"			=> esc_html__( 'Book Class', 'cryptop' ),
		),
	);
	$params = array_merge($params, $params2);
	array_push( $params, array(
		'type'			=> 'textfield',
		'heading'		=> esc_html__( 'Content Character Limit', 'cryptop' ),
		'param_name'	=> 'chars_count',
		'dependency'	=> array(
				'element'	=> 'show_data_override',
				'not_empty'	=> true
		),
		'value'			=> 	$def_chars_count	
	));	
	array_push( $params, array(
		
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Pagination', 'cryptop' ),
			"param_name"	=> "pagination_grid",
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "grid", "filter" )
							),
			"value"			=> array(
				esc_html__( "None", 'cryptop' ) 	=> 'none',
				esc_html__( "Standard", 'cryptop' ) 	=> 'standard',
				esc_html__( "Load More", 'cryptop' )	=> 'load_more',
			)		
		)
	);
	array_push( $params, array(	
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items to display', 'cryptop' ),
			"param_name"	=> "total_items_count",
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		)
	);	
	array_push( $params, array(	
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items per Page', 'cryptop' ),
			"param_name"	=> "items_pp",
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "grid", "filter" )
							),
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		)
	);				
	array_push( $params, array(
		"type"				=> "textfield",
		"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
		"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
		"param_name"		=> "el_class",
		"value"				=> ""
	));
	vc_map( array(
		"name"				=> esc_html__( 'CWS Classes', 'cryptop' ),
		"base"				=> "cws_sc_classes_posts_grid",
		'category'			=> "By CWS",
		"weight"			=> 80,
		"icon"     			=> "cws_icon",		
		"params"			=> $params
	));
	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Classes_Posts_Grid extends WPBakeryShortCode {
	    }
	}
?>