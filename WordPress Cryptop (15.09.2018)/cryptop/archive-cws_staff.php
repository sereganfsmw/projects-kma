<?php

	get_header ();

	$pid = get_queried_object_id();
	global $cws_theme_funcs;
	if ($cws_theme_funcs){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$class = $sb['layout_class'].' '. $sb['sb_class'];
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);
	}

	$taxonomy = get_query_var( 'taxonomy' );
	$terms = get_query_var( $taxonomy );

	$post_type = "cws_staff";
	$posts_per_page = (int)get_option('posts_per_page');
	$ajax = isset( $_POST['ajax'] ) ? (bool)$_POST['ajax'] : false;
	$paged_var = get_query_var( 'paged' );
	$paged = $ajax && isset( $_POST['paged'] ) ? $_POST['paged'] : ( $paged_var ? $paged_var : 1 );

/*	if ( is_date() ){
		$year = $cws_theme_funcs->cws_get_date_part( 'y' );
		$month = $cws_theme_funcs->cws_get_date_part( 'm' );
		$day = $cws_theme_funcs->cws_get_date_part( 'd' );
		if ( !empty( $year ) ){
			$posts_grid_atts['addl_query_args']['year'] = $year;
		}
		if ( !empty( $month ) ){
			$posts_grid_atts['addl_query_args']['monthnum'] = $month;
		}
		if ( !empty( $day ) ){
			$posts_grid_atts['addl_query_args']['day'] = $day;
		}
	}*/

	$args = array(
		'items_per_page' => $posts_per_page,
		'items_count' => PHP_INT_MAX,
		'paged' => $paged,
		'layout' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_layout'],
		'show_meta' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_show_meta'],
		'pagination_grid' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_pagination_grid'],
		'chars_count' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_chars_count'],
	);

	$args['extra_query_args'] = array(
		'post_type' => $post_type,
		'post_status' => 'publish'
	);

	if (!empty($terms)){
		$args['extra_query_args']['tax_query'] = array(
			array(
				'taxonomy'		=> $taxonomy,
				'field'			=> 'slug',
				'terms'			=> $terms
			)
		);
	}
	
	?>
	<div class="<?php echo (isset($sb) ? $sb['sb_class'] : 'page_content'); ?>">
		<?php
			echo (isset($sb['content']) && !empty($sb['content'])) ? $sb['content'] : '';
		?>
		<main>
			<?php if(isset($sb['content']) && !empty($sb['content'])){ 
				echo '<i class="sidebar-tablet-trigger"></i>';
			}

				if ( !$ajax ): // not ajax request
				?>
				<div class="grid_row">
				<?php
				endif;	
					echo cws_staff_output($args);
				if ( !$ajax ): // not ajax request
				?>
				</div>
				<?php
				endif;
				?>			
		</main>
		<?php echo (isset($sb['content']) && !empty($sb['content'])) ? "</div>" : ''; ?>
	</div>
<?php 
get_footer(); 

?>