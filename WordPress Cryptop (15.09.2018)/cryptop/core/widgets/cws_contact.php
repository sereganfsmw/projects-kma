<?php
	/**
	 * CWS Contact Widget Class
	 */

class CWS_Contact extends WP_Widget {
	public $fields = array();
	public function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				'value' => '',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'description_part' => array(
				'title' => esc_html__('Description', 'cryptop' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'data-options="e:logo_description;e:text_description;"',
			),
				'logo_description' => array(
					'title' => esc_html__( 'Logo', 'cryptop' ),
					'type' => 'media',
					'addrowclasses' => 'disable box',
					'layout' => array(
						'logo_is_high_dpi' => array(
							'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
							'addrowclasses' => 'checkbox',
							'type' => 'checkbox',
						),
					),
				),
				'text_description' => array(
					'title' => esc_html__( 'Text', 'cryptop' ),
					'type' => 'textarea',
					'atts' => 'rows="10"',
					'addrowclasses' => 'disable box',
					'value' => '',
				),
				'information_part' => array(
					'title' => esc_html__('Contact info', 'cryptop' ),
					'type' => 'checkbox',
					'addrowclasses' => 'checkbox',
					'atts' => 'data-options="e:information_group;e:icons_color;"',
				),
				'information_group' => array(
					'type' => 'group',
					'addrowclasses' => 'group expander sortable disable box',
					'title' => esc_html__('Information', 'cryptop' ),
					'button_title' => esc_html__('Add new field', 'cryptop' ),
					'layout' => array(
						'title' => array(
							'type' => 'text',
							'atts' => 'data-role="title"',
							'title' => esc_html__('Description', 'cryptop' ),
						),
						'icon' => array(
							'type' => 'select',
							'addrowclasses' => 'fai',
							'source' => 'fa',
							'title' => esc_html__('Icon', 'cryptop' )
						),
					),
				),
				'icons_color' => array(
					'type'      => 'text',
					'title'     => esc_html__( 'Icons color', 'cryptop' ),
					'addrowclasses' => 'disable box',
					'atts' => 'data-default-color="'.CRYPTOP_FIRST_COLOR.'"',
				),
				'social_part' => array(
					'title' => esc_html__('Social links', 'cryptop' ),
					'type' => 'checkbox',
					'addrowclasses' => 'checkbox',
					'atts' => 'data-options="e:icons_count;e:social_icons_shape_type;e:social_icons_size;e:social_icons_alignment;"',
				),
				'icons_count' => array(
					'title' => esc_html__( 'Icons to show', 'cryptop' ),
					'type' => 'number',
					'addrowclasses' => 'disable box',
					'value' => '5'
				),
				'social_icons_shape_type' => array(
					'title' => esc_html__( 'Shape', 'cryptop' ),
					'type' => 'radio',
					'addrowclasses' => 'disable box',
					'value' => array(
						'none' => array( esc_html__( 'None', 'cryptop' ), 	true, 'd:social_icons_hover'),
						'squared' => array( esc_html__( 'Squared', 'cryptop' ), 	false, 'd:social_icons_hover'),
						'round' =>array( esc_html__( 'Round', 'cryptop' ), false, 'd:social_icons_hover'),
					),
				),
				'social_icons_hover' => array(
					'title' => esc_html__( 'Hover effect', 'cryptop' ),
					'addrowclasses' => 'disable box checkbox',
					'type' => 'checkbox',
				),
				'social_icons_size' => array(
					'type' => 'select',
					'title' => esc_html__( 'Size', 'cryptop' ),
					'addrowclasses' => 'disable box',
					'source' => array(
						'1x' => array(esc_html__( 'Mini', 'cryptop' ), false),
						'lg' => array(esc_html__( 'Small', 'cryptop' ), false),
						'2x' => array(esc_html__( 'Medium', 'cryptop' ), true),
						'3x' => array(esc_html__( 'Big', 'cryptop' ), false),
					)
				),
				'social_icons_alignment' => array(
					'type' => 'select',
					'title' => esc_html__( 'Alignment', 'cryptop' ),
					'addrowclasses' => 'disable box',
					'source' => array(
						'left' => array( esc_html__( 'Left', 'cryptop' ), false),
						'center' => array( esc_html__( 'Center', 'cryptop' ), true),
						'right' => array( esc_html__( 'Right', 'cryptop' ), false),
					)
				),
		);
	}

	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-contact', 'description' => esc_html__( 'Add description, information, social links about site', 'cryptop' ) );
		parent::__construct( 'cws-contact', esc_html__( 'CWS Contact info', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'information_part' => '',
			'description_part' => '',
			'social_part' => '',
			'information_group' => '',
			'icons_color' => '',
			'logo_description' => '',
			'text_description' => '',
			'icons_count' => '5',
			'social_icons_shape_type' => '',
			'social_icons_hover' => '',
			'social_icons_size' => '',
			'social_icons_alignment' => '',
		), $instance));
		global $cws_theme_funcs;
		
		$social = $cws_theme_funcs->cws_get_option( 'social' )['icons'];

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;

		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';

		$title = esc_html($title);

		if (isset($logo_description['src']) && !empty($logo_description['src'])) {
			$logo_src = '';

			$logo = $logo_description;
			$logo_is_high_dpi = $logo['logo_is_high_dpi'];
			$logo_image_src = wp_get_attachment_image_src($logo['id'], 'full');
			$logo_height = $logo_image_src[2];
			$logo_width = $logo_image_src[1];

			if ( $logo_is_high_dpi ) {
				$thumb_obj = cws_thumb( $logo['id'],array( 'width' => floor( (int) $logo_width / 2 ), 'crop' => false ),false );
				$thumb_path_hdpi = !empty($thumb_obj[3]) ? " src='". esc_url( $thumb_obj[0] ) ."' data-at2x='" . esc_attr( $thumb_obj[3] ) ."'" : " src='". esc_url( $thumb_obj[0] ) . "' data-no-retina";
				$logo_src = $thumb_path_hdpi;
			} else {
				$logo_src = " src='" . esc_url( $logo['src'] ) . "' data-no-retina";
			}
		}

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
			
			if ( !empty( $widget_title_icon ) ){
				echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
			}
			else if (!empty( $title )){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}

		echo "<div class='cws_textwidget_content'>";

			if ($description_part == '1') {
				ob_start();
				?>
					<div class="text_description">
						<?php
						if (!empty($logo_src)) { ?>

							<div class="logo_description">
								<?php if(!empty($logo_src)){ ?>
									<img <?php echo sprintf('%s', $logo_src); ?> alt />
								<?php } else { ?>
									<h1 class='header_site_title'><?php echo esc_html(get_bloginfo( 'name' )); ?></h1>
								<?php } ?>
	                    	</div>

						<?php } ?>
						<?php echo (!empty( $text_description ) ? '<p>'.esc_html($text_description).'</p>': ''); ?>
					</div>
				<?php
				echo ob_get_clean();
			}

			if ($information_part == '1') {
				if (!empty( $information_group )){
				ob_start();
				?>
					<div class="information_group">
						<?php foreach ($information_group as $key => $value) { ?>
							<p><i style="color: <?php echo esc_attr($icons_color) ?>" class="<?php echo esc_attr($value['icon']) ?>"></i><?php echo esc_html($value['title']); ?></p>
						<?php } ?>
					</div>
				<?php
				echo ob_get_clean();
				} else {
					echo "<div class='cws_textwidget_content'>No info.</div>";
				}
			}

			if ($social_part == '1') {
				$social_icons_styles = '';

				if(!empty( $social )) {
					$i = 0;
					$icons_count = intval($icons_count);
					$hexagon = "<svg class='svg-hexagon' xmlns='http://www.w3.org/2000/svg'><g><path stroke-width='1' stroke-opacity='null' stroke='#e8e8e8' fill-opacity='0' fill='#fff' d='m40.00001,0.63895l38,23.36105l0,42l-38,23.30561l-37.99999,-23.30561c0,-17.50011 0,-25.11111 0,-42l37.99999,-23.36105z'></path></g></svg>";
					ob_start();
					?>
						<div class="cws_social_links position-<?php echo esc_attr($social_icons_alignment) ?> size-<?php echo esc_attr($social_icons_size) ?>">
							<?php foreach ($social as $key => $value) {
								if($i < $icons_count && !empty($value['icon']) ){

								$icon_id = uniqid( "cws_social_icon_" );				
								?>
								<a id="<?php echo esc_attr($icon_id) ?>" href="<?php echo esc_url($value['url']) ?>" class="cws_social_link <?php echo esc_attr($social_icons_shape_type) ?>" title="<?php echo esc_html($value['title']) ?>" target="_blank">
									<i class="social_icon cws_fa <?php echo esc_attr(($social_icons_hover == '1' ? 'hexagon-hover ' : '')) ?>fa <?php echo esc_attr($value['icon']) ?> <?php echo esc_attr((!empty($social_icons_size) ? 'fa-'.$social_icons_size : '')) ?> simple_icon"><?php echo ($social_icons_shape_type == 'hexagon' ? "<span class='container-hexagon'>$hexagon</span>" : '');?></i>
								</a>
							<?php

							$social_icons_styles .=
								"
								.cws-widget .cws_social_links #".esc_attr($icon_id)." .social_icon
								{
									".(!empty($value['color']) ? "color:".esc_attr($value['color']).";" : '')."
								}
							";

							if ($social_icons_shape_type != 'none'){
								$social_icons_styles .=
									"
									.cws-widget .cws_social_links #".esc_attr($icon_id).":hover .social_icon
									{
										".(!empty($value['hover_color']) ? "color:".esc_attr($value['hover_color']).";" : '')."
									}
								";
							}

							$social_icons_styles .=
								"
								.cws-widget .cws_social_links #".esc_attr($icon_id)."
								{
									".(!empty($value['bg_color']) ? "background-color:".esc_attr($value['bg_color']).";" : '')."
								}

								.cws-widget .cws_social_links #".esc_attr($icon_id).":before
								{
									".(!empty($value['hover_bg_color']) ? "background-color:".esc_attr($value['hover_bg_color']).";" : '')."
								}		
							";

							} $i++;
						} 
				
						Cws_shortcode_css()->enqueue_cws_css($social_icons_styles);

						?>
						</div>
					<?php
					echo ob_get_clean();
				} else {
					echo "<div class='cws_textwidget_content'>No Social Icons found.</div>";
				}
			}
		echo "</div>";

	echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			if ($v == 'on') {
				$v = '1';
			}
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>