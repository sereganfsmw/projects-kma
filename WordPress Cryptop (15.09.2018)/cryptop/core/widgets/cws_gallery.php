<?php
	/**
	 * CWS Gallery Widget Class
	 */

class CWS_Gallery extends WP_Widget {
	public $fields = array();
	public function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
				'value' => '',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'gallery' => array(
				'title' => esc_html__( 'Gallery', 'cryptop' ),
				'type' => 'gallery'
			),
			'layout' => array(
				'title' => esc_html__( 'Layout', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'grid' => array( esc_html__( 'Grid', 'cryptop' ),  true, 'd:visible_count;' ),
					'carousel' =>array( esc_html__( 'Carousel', 'cryptop' ), false, 'e:visible_count;e:controls;' ),
				),
			),
			'controls' => array(
				'title' => esc_html__( 'Controls style', 'cryptop' ),
				'type' => 'select',
				'source' => array(
					'dots' => array('Dots', true, 'e:dots_style;'),
					'arrows' => array('Arrows',false, 'd:dots_style;'),
				),
			),	
			'dots_style' => array(
				'title' => esc_html__( 'Controls style', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'none' => array( esc_html__( 'Standart', 'cryptop' ), true),
					'controls_round' => array( esc_html__( 'Round', 'cryptop' )),
					'controls_square' => array( esc_html__( 'Square', 'cryptop' )),
				),
			),
			'spacings' => array(
				'title' => esc_html__( 'Spacings (px)', 'cryptop' ),
				'type' => 'number',
				'value' => '1'
			),
			'columns' => array(
				'title' => esc_html__( 'Columns', 'cryptop' ),
				'type' => 'select',
				'source' => array(
					'1' => array('One Column', true, ''),
					'2' => array('Two Columns',false, ''),
					'3' => array('Three Columns',false, ''),
					'4' => array('Four Columns',false, '')
				),
			),
			'visible_count' => array(
				'title' => esc_html__( 'Items per slide', 'cryptop' ),
				'addrowclasses' => 'disable',
				'type' => 'number',
				'value' => 1
			),

		);
	}
	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-gallery', 'description' => esc_html__( 'Create gallery from media', 'cryptop' ) );
		parent::__construct( 'cws-gallery', esc_html__( 'CWS Gallery', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'gallery' => '',

			'layout' => 'grid',
			'controls' => 'dots',
			'dots_style' => 'controls_round',
			'spacings' => '1',
			'columns' => '1',
			'visible_count' => 1,
		), $instance));
		global $cws_theme_funcs;

		$count = 0;

		$match = preg_match_all("/\d+/",$gallery,$images);
		if ($match) {
			$images = $images[0];
			$image_srcs = array();
			foreach ( $images as $image ) {
				$image_src = wp_get_attachment_image_src($image,'full');
				if ( $image_src ){
					$image_url = $image_src[0];
					array_push( $image_srcs, $image_url );
				}
			}
			$dims['width'] = 270;
			$dims['height'] = 270;
			$dims['crop'] = array(
				$cws_theme_funcs->cws_get_option( 'crop_x' ),
				$cws_theme_funcs->cws_get_option( 'crop_y' )
			);

			$count = count($image_srcs);
		}

		$carousel_mode = ($count > $visible_count) && $layout == 'carousel';
		$counter = 0;

		$use_blur = $cws_theme_funcs->cws_get_option( 'use_blur' );
		$use_blur = isset($use_blur) && !empty($use_blur) && ($use_blur == '1') ? true : false;

		$title = esc_html($title);

		$count = empty( $count ) ? get_option( 'posts_per_page' ) : $count;
		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';

		$gallery_id = esc_attr(uniqid( 'cws-widget-gallery-' ));

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);

			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title)  . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title)  . esc_html($title) . $after_title;
			}
			//Title

		
		if ($match) {
				$gallery_class = '';
				ob_start();
					if ( $carousel_mode ){
						post_class(array( 'widget_carousel','cws_widget_columns', "col-".$columns, ($controls == 'arrows' ? ' nav' : ' dots'), ($dots_style != 'none' ? " ".$controls : '') ));
					} else {
						post_class(array( 'cws_widget_grid','clearfix','cws_widget_columns', "col-".$columns ));
					}
				$gallery_class .= ob_get_clean();

				if ( $carousel_mode ){
					wp_enqueue_script ('owl_carousel');
				}

				echo "<div ".$gallery_class.">";

				$several = count($image_srcs) > 1 ? true : false;

				foreach ( $image_srcs as $image_src ) {

					$img_obj = cws_thumb( $image_src, $dims , true );
					$thumb_url = isset( $img_obj[0] ) ? esc_url( $img_obj[0] ) : "";
					$retina_thumb_url = isset( $img_obj[3] ) ? esc_url($img_obj[3]) : "";

						if ( $carousel_mode && $counter <= 0 ){
							echo "<div class='item'>"; //open carousel item tag
						}				

							echo "<div class='cws_item_thumb' style='padding:".$spacings."px;'>";
								echo "<div class='pic'>";
									echo "<a href='".esc_url($image_src)."' class='fancy'" . ( $several ? " data-fancybox-group='".esc_attr($gallery_id)."'" : "" ) . ">";
										if ( isset($img_obj[3]) ){
											echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb_url)."' alt />";
										}
										else{
											echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
										}
										if ( $use_blur ){
											echo "<img src='".esc_url($thumb_url)."' class='blured-img' alt />";
										}
										echo "<div class='hover-effect'></div>";
									echo "</a>";
								echo "</div>";
							echo "</div>";
			
						if ( $carousel_mode ){
							if ( $counter >= $visible_count-1){
								echo "</div>"; //-close carousel item tag
								$counter = 0;
							} else {
								$counter ++;
							}
						}
				}

				wp_reset_postdata();						
				echo "</div>";

		} else{
			echo do_shortcode( "[cws_sc_msg_box text='" . esc_html__( 'There are no image selected', 'cryptop' ) . "'][/cws_sc_msg_box]" );
		}
		echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			if ($v == 'on') {
				$v = '1';
			}
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>