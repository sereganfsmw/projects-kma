<?php
	/**
	 * Latest Comments Widget Class
	 */

class CWS_Latest_Comments extends WP_Widget {
	function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget Title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Comments count', 'cryptop' ),
				'value' => '3',
			),		
			'visible_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Comments per slide', 'cryptop' ),
				'value' => '3',
			),
			'chars_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Count of chars from comment', 'cryptop' ),
				'value' => '50',
			),
			'text_align' => array(
				'title' => esc_html__( 'Text align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), 	true, 	'' ),
					'center' => array( esc_html__( 'Center', 'cryptop' ), 	false, 	'' ),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,'' ),
				),
			),		
			'show' => array(
				'title' => esc_html__( 'Show', 'cryptop' ),
				'type' => 'select',
				'atts' => 'multiple',
				'source' => array(
					'name' => array( esc_html__( 'Name', 'cryptop' ), true ),
					'position' => array( esc_html__( 'Position', 'cryptop' ), false ),
					'comment' => array( esc_html__( 'Comment', 'cryptop' ), true ),
					'date' => array( esc_html__( 'Date', 'cryptop' ), false ),
				),
			),	
		);
	}

	function __construct() {
		$widget_ops = array( 'classname' => 'widget_cws_recent_comments', 'description' => esc_html__( 'CWS most recent comments', 'cryptop' ) );
		parent::__construct( 'cws-recent-comments', esc_html__( 'CWS Recent Comments', 'cryptop' ), $widget_ops );
	}
	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'count' => get_option( 'posts_per_page' ),			
			'visible_count' => get_option( 'posts_per_page' ),
			'chars_count' => '50',
			'text_align' => 'left',
			'show' => array('name','comment'),
		), $instance));

		global $cws_theme_funcs;

		$show_arr = array();
		if (is_string($show)){
			$show_arr[] = $show;
		} else {
			$show_arr = $show;
		}

		$title = esc_html($title);

		$footer_is_rendered = isset( $GLOBALS['footer_is_rendered'] );

		/* defaults for empty text fields with number values */
		$count = empty( $count ) ? (int)get_option( 'posts_per_page' ) : (int)$count;
		$visible_count = empty( $visible_count ) ? $count : (int)$visible_count;
		$chars_count = empty( $chars_count ) ? 50 : (int)$chars_count;
		/* \defaults for empty text fields with number values */

		$args = array (
			'status' => 'approve',
			'number' => $count
		);
		$comments = get_comments( $args );

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';
		$carousel_mode = $count > $visible_count;
		$counter = 0;

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
		
			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title) . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}
			//Title

			if (!empty( $comments )){
				echo "<div class='widget_wrapper ".(!empty($text_align) ? 'text_'.$text_align : '')."'>"; //widget_wrapper
				$item_class = '';
				if ( $carousel_mode ){
					wp_enqueue_script ('owl_carousel');
					echo "<div class='widget_carousel dots'>";
				} else if ( $footer_is_rendered ){
					echo "<div class='post_items'>";
				}

				$current_comment = 0;
				$comment_count = count($comments);
				foreach( $comments as $comment ){
					$author = get_user_by( 'slug', $comment->comment_author );

					if (!empty($author->first_name) || !empty($author->last_name)){
						$author_name = $author->first_name . (!empty($author->last_name) ? ' '.$author->last_name : '' );
					} else {
						$author_name = $comment->comment_author;
					}

					$author_position = '';
					if (isset($author->ID)){
						$author_meta = get_user_meta($author->ID, 'cws_mb_user' );
						$author_position = (!empty($author_meta[0]['position']) ? $author_meta[0]['position'] : '');
					}

					$content = $comment->comment_content;
					$permalink = get_comment_link( $comment );
					$comment_date = $comment->comment_date;

					$date_format = get_option( 'date_format' );
					$date = date($date_format, strtotime($comment_date));
					$time = date('h:i:s', strtotime($comment_date));

					if ( $carousel_mode && $counter <= 0 ){
						echo "<div class='item ".esc_attr($item_class)."'>"; //open carousel item tag
					}
						echo "<div class='post_item'>"; //post item
							echo "<div class='comment clearfix'>"; //comment

								echo "<div class='icon_wrapper'><i class='comment_icon cwsicon-quote-left'></i></div>";
								
								echo "<div class='comment_wrapper'>"; //comment_wrapper

									//Сomment
									if (in_array('comment',$show_arr)){
										if ( strlen( $content ) > $chars_count ){
												$content = mb_substr( $content, 0, $chars_count );
												$content = wptexturize( $content );
												echo "<div class='comment_content'>$content <a href='".esc_url($permalink)."'>" . esc_html__( "...", 'cryptop' ) . "</a></div>";
										}
										else{
											$content = wptexturize( $content );
											echo "<div class='comment_content'>$content</div>";
										}
									}

									//Name
									if (in_array('name',$show_arr)){
										echo "<div class='author_name'>".esc_html($author_name)."</div>";
									}

									//Position
									if (in_array('position',$show_arr)){
										echo "<div class='author_position'>".esc_html($author_position)."</div>";
									}

									//Date
									if (in_array('date',$show_arr)){
										echo "<div class='comment_date'><i class='cwsicon-clock-icon'></i> ".esc_html($date).', '.$time."</div>";
									}
									
								echo "</div>"; //-comment_wrapper
							echo "</div>"; //-comment
						echo "</div>"; //-post item
					if ( $carousel_mode ){
						if ( $counter >= $visible_count-1 || $current_comment >= $comment_count-1 ){
							echo "</div>"; //-close carousel item tag
							$counter = 0;
						} else {
							$counter ++;
						}
					}
					$current_comment ++;
				}

				wp_reset_postdata();
				if ($carousel_mode || $footer_is_rendered) echo "</div>"; //-widget_carousel || post_items
				echo "</div>"; //-widget_wrapper
			} else{
				echo do_shortcode( "[cws_sc_msg_box text='" . esc_html__( 'There are no comments yet', 'cryptop' ) . "'][/cws_sc_msg_box]" );
			}

		echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>