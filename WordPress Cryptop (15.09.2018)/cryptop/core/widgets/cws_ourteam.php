<?php
	/**
	 * Ourteam Widget Class
	 */

class CWS_Ourteam extends WP_Widget {
	function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget Title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),			
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'layout' => array(
				'title' => esc_html__( 'Layout', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'grid' => array( esc_html__( 'Grid', 'cryptop' ),  false, 'd:visible_count;' ),
					'carousel' =>array( esc_html__( 'Carousel', 'cryptop' ), true, 'e:visible_count;e:controls;' ),
				),
			),
			'controls' => array(
				'title' => esc_html__( 'Controls style', 'cryptop' ),
				'type' => 'select',
				'source' => array(
					'dots' => array('Dots', true, 'e:dots_style;'),
					'arrows' => array('Arrows',false, 'd:dots_style;'),
				),
			),	
			'dots_style' => array(
				'title' => esc_html__( 'Controls style', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'none' => array( esc_html__( 'Standart', 'cryptop' ), true),
					'controls_round' => array( esc_html__( 'Round', 'cryptop' )),
					'controls_square' => array( esc_html__( 'Square', 'cryptop' )),
				),
			),			
			'cats' => array(
				'type' => 'taxonomy',
				'title' => esc_html__( 'Categories', 'cryptop' ),
				'atts' => 'multiple',
				'taxonomy' => 'cws_staff_member_position',
				'source' => array(),
			),
			'spacings' => array(
				'title' => esc_html__( 'Spacings (px)', 'cryptop' ),
				'type' => 'number',
				'value' => '1'
			),
			'columns' => array(
				'title' => esc_html__( 'Columns', 'cryptop' ),
				'type' => 'select',
				'source' => array(
					'1' => array('One Column', true, 'e:show;'),
					'2' => array('Two Columns',false, 'd:show;'),
					'3' => array('Three Columns',false, 'd:show;'),
					'4' => array('Four Columns',false, 'd:show;')
				),
			),
			'photo_style' => array(
				'title' => esc_html__( 'Photo style', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'round' => array( esc_html__( 'Round', 'cryptop' ), true, '' ),
					'square' => array( esc_html__( 'Square', 'cryptop' ), false, '' ),
				),
			),			
			'show' => array(
				'title' => esc_html__( 'Show', 'cryptop' ),
				'type' => 'select',
				'addrowclasses' => 'disable',
				'atts' => 'multiple',
				'source' => array(
					'name' => array('Name', true, 'd:chars_count;'),
					'position' => array('Position',true, 'd:chars_count;'),
					'excerpt' => array('Excerpt',false, 'e:chars_count;'),
				),
			),	
			'chars_count' => array(
				'type' => 'number',
				'addrowclasses' => 'disable',
				'title' => esc_html__( 'Count of chars from post content', 'cryptop' ),
				'value' => '50',
			),			
			'count' => array(
				'title' => esc_html__( 'Items Count', 'cryptop' ),
				'type' => 'number',
				'value' => '3'
			),
			'visible_count' => array(
				'title' => esc_html__( 'Items per slide', 'cryptop' ),
				'addrowclasses' => 'disable',
				'type' => 'number',
				'value' => '1'
			),
		);
	}

	function __construct() {
		$widget_ops = array( 'classname' => 'widget-cws-ourteam', 'description' => esc_html__( 'Ourteam Items', 'cryptop' ) );
		parent::__construct( 'cws-ourteam', esc_html__( 'CWS Ourteam', 'cryptop' ), $widget_ops );
	}

	function widget( $args, $instance ) {

		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'layout' => 'grid',
			'controls' => 'dots',
			'dots_style' => 'controls_round',
			'cats' => array(),
			'spacings' => '1',
			'photo_style' => 'round',
			'show' => array('name','position'),
			'chars_count' => '50',
			'columns' => '3',
			'count' => '3',
			'visible_count' => '1',
		), $instance));

		global $cws_theme_funcs;

		$carousel_mode = ($count > $visible_count) && $layout == 'carousel';
		$counter = 0;

		$use_blur = $cws_theme_funcs->cws_get_option( 'use_blur' );
		$use_blur = isset($use_blur) && !empty($use_blur) && ($use_blur == '1') ? true : false;

		$title = esc_html($title);

		$chars_count = empty( $chars_count ) ? 50 : (int)$chars_count;

		$count = empty( $count ) ? get_option( 'posts_per_page' ) : $count;
		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';

		$query_args = array(
			'post_type' => 'cws_staff',
			'ignore_sticky_posts' => true,
			'post_status' => 'publish',
			'posts_per_page' => $count
		);

		$tax_query = array();
		if ( !empty( $cats ) ){
			$tax_query[] = array(
				'taxonomy' => 'cws_staff_member_position',
				'field' => 'slug',
				'terms' => $cats
			);
		}

		if ( !empty( $tax_query ) ) $query_args['tax_query'] = $tax_query;

		$q = new WP_Query( $query_args );

		$several = $q->post_count > 1 ? true : false;
		$gallery_id = esc_attr(uniqid( 'cws-ourteam-gallery-' ));

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
		
			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title) . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}
			//Title
			
			if ( $q->have_posts() ){

				if ( $carousel_mode ){
					wp_enqueue_script ('owl_carousel');
				?>
					<div <?php post_class(array( 'ourteam_widget','widget_carousel','cws_widget_columns', "col-".$columns, ($controls == 'arrows' ? ' nav' : ' dots'), ($dots_style != 'none' ? " ".$controls : '') )); //ourteam_widget ?>>
				<?php						
				} else{ ?>
					<div <?php post_class(array( 'ourteam_widget','cws_widget_grid','clearfix','cws_widget_columns', "col-".$columns )); //ourteam_widget ?>>
				<?php }

					while ( $q->have_posts() ):
						$q->the_post();
						if ( has_post_thumbnail() ){
							$permalink = esc_url(get_permalink());
							$post_title = get_the_title();
							$pid = get_the_id();
							$cur_post = get_queried_object();

							$positions_array = wp_get_post_terms( $pid, 'cws_staff_member_position' );
							$positions_str = "";

							if (!empty($positions_array)){
								for ( $i=0; $i<count( $positions_array ); $i++ ){
									$position = $positions_array[$i];
									$position_id = $position->term_id;
									$position_permalink = get_term_link( $position_id, 'cws_staff_member_position' );
									$position_name = $position->name;
									$positions_str .= "<a href='$position_permalink'>$position_name</a>";
									$positions_str .= $i < count( $positions_array ) - 1 ? esc_html__( ', ', 'cryptop' ) : "";
								}
							}

							$dims = array();

							$img_url = esc_url(wp_get_attachment_url( get_post_thumbnail_id() ));

							$dims['width'] = 290;
							$dims['height'] = 290;
							$dims['crop'] = array(
								$cws_theme_funcs->cws_get_option( 'crop_x' ),
								$cws_theme_funcs->cws_get_option( 'crop_y' )
							);

							$thumb_obj = cws_thumb( $img_url, $dims, true );
							$thumb_url = esc_url($thumb_obj[0]);
							$retina_thumb_url = esc_url($thumb_obj[3]);

								if ( $carousel_mode && $counter <= 0 ){
									echo "<div class='item".(empty($show) ? ' no_content' : '')."'>"; //open carousel item tag 
								}

								echo "<div class='cws_item'>"; //cws_item
									//Thumb
									echo "<div class='cws_item_thumb ".esc_attr($photo_style)."' style='padding:".esc_attr($spacings)."px;'>";
										echo "<div class='pic'>";
											echo "<a href='".esc_url($img_url)."' class='fancy'" . ( $several ? " data-fancybox-group='".esc_attr($gallery_id)."'" : "" ) . ">";
												if ( isset($thumb_obj[3]) ){
													echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb_url)."' alt />";
												}
												else{
													echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
												}
												if ( $use_blur ){
													echo "<img src='".esc_url($thumb_url)."' class='blured-img' alt />";
												}
												echo "<div class='hover-effect'></div>";
											echo "</a>";
										echo "</div>";								
									echo "</div>";

									//Name
									if (in_array('name', $show)){
										echo "<div class='ourteam_item_title'>";
											echo "<h4><a href='".esc_url($permalink)."'>".esc_html($post_title)."</a></h4>";
										echo "</div>";
									}

									//Position
									if (in_array('position', $show)){
										echo "<div class='ourteam_item_position'>";
											printf('%s',$positions_str);
										echo "</div>";
									}								

									//Excerpt
									if (in_array('excerpt', $show)){
										$content = !empty( $cur_post->post_excerpt ) ? $cur_post->post_excerpt : get_the_content( '' );
										$content = trim( preg_replace( "/[\s]{2,}/", " ", strip_shortcodes( strip_tags( $content ) ) ) );
										$is_content_empty = empty( $content );
										if ( !$is_content_empty && in_array('excerpt', $show)){
											if ( strlen( $content ) > $chars_count ){
													$content = mb_substr( $content, 0, $chars_count );
													$content = wptexturize( $content ); /* apply wp filter */
													echo "<div class='ourteam_item_content'>$content <a href='".esc_url($permalink)."'>" . esc_html__( "...", 'cryptop' ) . "</a></div>";
											}
											else{
												$content = wptexturize( $content ); /* apply wp filter */
												echo "<div class='ourteam_item_content'>$content</div>";
											}
										}
									}
								echo "</div>"; //-cws_item

							if ( $carousel_mode ){
								if ( $counter >= $visible_count-1 || $q->current_post >= $q->post_count-1 ){
									echo "</div>"; //-close carousel item tag
									$counter = 0;
								} else {
									$counter ++;
								}								
							}
						}
					endwhile;
					wp_reset_postdata();						
				echo "</div>"; //--ourteam_widget

			} else{
				echo do_shortcode( "[cws_sc_msg_box text='" . esc_html__( 'There are no posts matching the query', 'cryptop' ) . "'][/cws_sc_msg_box]" );
			}			
		echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}

?>