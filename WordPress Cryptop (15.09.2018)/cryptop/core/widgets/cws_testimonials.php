<?php
	/**
	 * Testimonials Widget Class
	 */

class CWS_Testimonials extends WP_Widget {
	function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget Title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'sel_posts_by' => array(
				'title' => esc_html__( 'Filter by', 'cryptop' ),
				'type' => 'select',
				'source' => array(
					'none' => array('None', true, 'd:categories;d:post_title;d:tags;'),
					'titles' => array('Titles', false, 'd:categories;e:post_title;d:tags;'),
					'cats' => array('Categories', false, 'e:categories;d:post_title;d:tags;'),
					'tags' => array('Tags', false, 'd:categories;d:post_title;e:tags;'),
				),
			),
			'categories' => array(
				'title' => esc_html__( 'Categories', 'cryptop' ),
				'type' => 'taxonomy',
				'addrowclasses' => 'disable',
				'taxonomy' => 'cws_testimonial_department',
				'atts' => 'multiple',
				'source' => array(),
			),
			'tags' => array(
				'title' => esc_html__( 'Tags', 'cryptop' ),
				'type' => 'taxonomy',
				'addrowclasses' => 'disable',
				'taxonomy' => 'cws_testimonial_position',
				'atts' => 'multiple',
				'source' => array(),
			),
			'post_title' => array(
				'title' => esc_html__( 'Posts', 'cryptop' ),
				'type' => 'select',
				'addrowclasses' => 'disable',
				'taxonomy' => 'cws_testimonials',
				'atts' => 'multiple',
				'source' => 'titles cws_testimonials',
			),
			'count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Post count', 'cryptop' ),
				'value' => '3',
			),
			'visible_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Posts per slide', 'cryptop' ),
				'value' => '1',
			),
			'chars_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Count of chars from post content', 'cryptop' ),
				'value' => '50',
				),
			'text_align' => array(
				'title' => esc_html__( 'Text align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), 	true, 	'' ),
					'center' => array( esc_html__( 'Center', 'cryptop' ), 	false, 	'' ),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,'' ),
				),
			),			
			'show' => array(
				'title' => esc_html__( 'Show', 'cryptop' ),
				'type' => 'select',
				'atts' => 'multiple',
				'source' => array(
					'photo' => array( esc_html__( 'Photo', 'cryptop' ), true, '' ),
					'title' => array( esc_html__( 'Titles', 'cryptop' ), true, '' ),
					'content' => array( esc_html__( 'Content', 'cryptop' ), true, '' ),
					'position' => array( esc_html__( 'Position', 'cryptop' ), false, '' ),
					'date' => array( esc_html__( 'Date', 'cryptop' ), false, '' ),
				),
			),	
			'photo_style' => array(
				'title' => esc_html__( 'Photo style', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'round' => array( esc_html__( 'Round', 'cryptop' ), true, '' ),
					'square' => array( esc_html__( 'Square', 'cryptop' ), false, '' ),
					'rectangle' => array( esc_html__( 'Rectangle', 'cryptop' ), false, '' ),
				),
			),
			'show_content' => array(
				'title' => esc_html__( 'Content', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'content' => array( esc_html__( 'Content', 'cryptop' ), true),
					'exerpt' => array( esc_html__( 'Exerpt', 'cryptop' )),
				),
			),
		);
	}
	function __construct() {
		$widget_ops = array( 'classname' => 'widget_cws_testimonials', 'description' => esc_html__( 'CWS testimonials posts', 'cryptop' ) );
		parent::__construct( 'cws-testimonials', esc_html__( 'CWS Testimonials', 'cryptop' ), $widget_ops );
			}
	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'sel_posts_by' => 'none',
			'categories' => array(),
			'tags' => array(),
			'post_title' => array(),
			'count' => '3',
			'visible_count' => '1',
			'chars_count' => '50',
			'text_align' => 'left',
			'show' => array('photo', 'title', 'content'),
			'photo_style' => 'round',
			'show_content' => 'content',
		), $instance));
		global $cws_theme_funcs;
		
		$show_arr = array();
		if (is_string($show)){
			$show_arr[] = $show;
		} else {
			$show_arr = $show;
		}		

		$use_blur = $cws_theme_funcs->cws_get_option( 'use_blur' );

		$title = esc_html($title);

		$footer_is_rendered = isset( $GLOBALS['footer_is_rendered'] );

		/* defaults for empty text fields with number values */
		$count = empty( $count ) ? (int)get_option( 'posts_per_page' ) : (int)$count;
		$visible_count = empty( $visible_count ) ? $count : (int)$visible_count;
		$chars_count = empty( $chars_count ) ? 50 : (int)$chars_count;
		/* \defaults for empty text fields with number values */


		$q_args = array(
			'post_type' => 'cws_testimonial',
			'posts_per_page' => $count,
			'ignore_sticky_posts' => true,
			'post_status' => 'publish',
			'orderby'    => 'menu_order date title',
			'order'      => 'ASC',
		);

		$post_title = is_string($post_title) ? (!empty($post_title) ? explode( ',', $post_title ) : null) : $post_title;
		$categories = is_string($categories) ? (!empty($categories) ? explode( ',', $categories ) : null) : $categories;
		$tags = is_string($tags) ? (!empty($tags) ? explode( ',', $tags ) : null) : $tags;

		$tax_query = array();
		if ( ($sel_posts_by == 'cats') && !empty( $categories )){
			$tax_query[] = array(
				'taxonomy' => 'cws_testimonial_department',
				'field' => 'slug',
				'terms' => $categories
			);
		} else if ( ($sel_posts_by == 'tags') && !empty( $tags )){
			$tax_query[] = array(
				'taxonomy' => 'cws_testimonial_position',
				'field' => 'slug',
				'terms' => $tags
			);
		} else if ( $sel_posts_by == 'titles' && !empty( $post_title ) ) {
			$q_args['post__in'] = $post_title;
		}
		if ( !empty( $tax_query ) ) $q_args['tax_query'] = $tax_query;

		$q = new WP_Query( $q_args );

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';
		$carousel_mode = $count > $visible_count;
		$counter = 0;

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);

			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title) . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}
			//Title

			if ( $q->have_posts() ):
				$wrapper_style = '';
				$wrapper_style .= !empty($text_align) ? ' text_'.$text_align : '';
				$wrapper_style .= !empty($photo_style) ? ' '.$photo_style : '';
				echo "<div class='widget_wrapper".esc_attr($wrapper_style)."'>"; //widget_wrapper
				$blur_class = esc_attr($use_blur ? ' blurred' : '');
				if ( $carousel_mode ){
					wp_enqueue_script ('owl_carousel');
					echo "<div class='widget_carousel testimonials_widget dots'>";
				} else if ( $footer_is_rendered ){
					echo "<div class='post_items'>";
				}
				while ( $q->have_posts() ):
					$q->the_post();
					$pid = get_the_id();
					$post = get_post( $pid );
					$cur_post = get_queried_object();
					$date_format = get_option( 'date_format' );
					$date = esc_html( get_the_time( $date_format ) );
					$permalink = esc_url(get_permalink());

					$meta = get_post_meta( $pid, 'cws_mb_post' );
					$meta = isset( $meta[0] ) ? $meta[0] : array();

					$p_category_terms = wp_get_post_terms( $pid, 'cws_testimonial_department' );
					$p_tag_terms = wp_get_post_terms( $pid, 'cws_testimonial_position' );

					$taxonomy = '';

						//Position part
						if (in_array('position',$show_arr)){
							$p_cats = "";
							for ( $i=0; $i<count( $p_category_terms ); $i++ ){
								$p_category_term = $p_category_terms[$i];
								$p_cat_permalink = get_term_link( $p_category_term->term_id, 'cws_testimonial_department' );
								$p_cat_name = $p_category_term->name;
								$p_cats .= "<a class='testimonials_category' href='$p_cat_permalink'>$p_cat_name</a>";
								$p_cats .= $i < count( $p_category_terms ) - 1 ? esc_html__( ", ", 'cryptop' ) : "";
							}
							$taxonomy = $p_cats;
							$p_tags = "";
							for ( $i=0; $i<count( $p_tag_terms ); $i++ ){
								$p_tag_term = $p_tag_terms[$i];
								$p_tag_permalink = get_term_link( $p_tag_term->term_id, 'cws_testimonial_position' );
								$p_tag_name = $p_tag_term->name;
								$p_tags .= "<a class='testimonials_tags' href='$p_tag_permalink'>$p_tag_name</a>";
								$p_tags .= $i < count( $p_tag_terms ) - 1 ? esc_html__( ", ", 'cryptop' ) : "";
							}
								$p_tags = !empty($p_tags) ? "<i class='round-divider'></i>" . $p_tags : '';
								$taxonomy = $p_cats.''.$p_tags;					
						}

						if ( $carousel_mode && $counter <= 0 ){
							echo "<div class='item $blur_class'>"; //open carousel item tag 
						}
							echo "<div class='post_item $blur_class'>"; //post item
								echo "<div class='post_preview clearfix'>"; //post_preview

									echo "<div class='testimonials_widget_image'>";
									//Image part
									if ( has_post_thumbnail() && in_array('photo',$show_arr)){
										if ($photo_style == 'rectangle'){
											$dims = array( 'width' => 100, 'height' => 200, 'crop' => array('center','top') );
										} else if ($photo_style == 'square' || $photo_style == 'round') {
											$dims = array( 'width' => 100, 'height' => 100, 'crop' => array('center','top') );
										}

										$featured_img_url = wp_get_attachment_url( get_post_thumbnail_id() );
										$thumb_url = $cws_theme_funcs->cws_print_img_html( array('src' => $featured_img_url), $dims );
										echo "<div class='post_thumb'>";
											echo "<a href='$permalink'>";
												echo "<img ".$thumb_url." alt />";
											echo "</a>";
										echo "</div>";
									}
									echo "</div>";

									echo "<div class='testimonials_widget_meta'>"; //widget meta
										//Content part
										if (in_array('content',$show_arr)){
										$content = !empty( $cur_post->post_excerpt ) ? $cur_post->post_excerpt : get_the_content( '' );
										$content = trim( preg_replace( "/[\s]{2,}/", " ", strip_shortcodes( strip_tags( $content ) ) ) );

										$is_content_empty = empty( $content );
										if ( !$is_content_empty){
											if($show_content == 'exerpt'){
												$content = !empty( $post->post_excerpt ) ? $post->post_excerpt : $content;
											}

											if ( strlen( $content ) > $chars_count ){
													$content = mb_substr( $content, 0, $chars_count );
													$content = wptexturize( $content ); /* apply wp filter */
														echo "<div class='post_content'>\"$content\"<a href='$permalink'>" . esc_html__( "...", 'cryptop' ) . "</a></div>";
											}
											else{
												$content = wptexturize( $content ); /* apply wp filter */
													echo "<div class='post_content'>\"$content\"</div>";
												}
											}
										}

										//Title part								
										if (in_array('title',$show_arr)){
										$post_title = esc_html( get_the_title() );
										echo !empty( $post_title ) ? "<div class='post_title'><a href='$permalink'>$post_title</a></div>" : "";
										}

										if(!empty($taxonomy)) { echo "<div class='quote_author'>$taxonomy</div>"; }


										//Date part
										if ( in_array('date',$show_arr) ){
											echo "<div class='post_date'>$date</div>";
										}
									echo "</div>"; //-widget meta

								echo "</div>"; //-post_preview
							echo "</div>"; //-post item
						if ( $carousel_mode ){
							if ( $counter >= $visible_count-1 || $q->current_post >= $q->post_count-1 ){
								echo "</div>";
								$counter = 0;
							} else {
								$counter ++;
							}
						}

				endwhile;
				wp_reset_postdata();

				if ($carousel_mode || $footer_is_rendered) echo "</div>";
				echo "</div>"; //widget_wrapper
			else:
				echo do_shortcode( "[cws_sc_msg_box text='" . esc_html__( 'There are no posts matching the query', 'cryptop' ) . "'][/cws_sc_msg_box]" );
			endif;
		echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>