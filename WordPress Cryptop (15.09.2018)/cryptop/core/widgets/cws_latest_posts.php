<?php
	/**
	 * Latest Posts Widget Class
	 */

class CWS_Latest_Posts extends WP_Widget {
	function init_fields() {
		$this->fields = array(
			'title' => array(
				'title' => esc_html__( 'Widget Title', 'cryptop' ),
				'atts' => 'id="widget-title"',
				'type' => 'text',
			),
			'title_align' => array(
				'title' => esc_html__( 'Title align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), true, 	''),
					'center' => array( esc_html__( 'Center', 'cryptop' ), false, 	''),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,''),
				),
			),	
			'hide_mark' => array(
				'title' => esc_html__( 'Hide mark', 'cryptop' ),
				'type' => 'checkbox',
			),				
			'show_icon_opts' => array(
				'title' => esc_html__( 'Show icon options', 'cryptop' ),
				'type' => 'checkbox',
				'atts' => 'data-options="e:icon_opts"',
			),
			'icon_opts' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable box inside-box groups',
				'layout' => '%icon_options%',
			),

			'cats' => array(
				'title' => esc_html__( 'Post categories', 'cryptop' ),
				'type' => 'taxonomy',
				'taxonomy' => 'category',
				'atts' => 'multiple',
				'source' => array(),
			),
			'exclude' => array(
				'title' => esc_html__( 'Exclude (Post formats)', 'cryptop' ),
				'type' => 'taxonomy',
				'taxonomy' => 'post_format',
				'atts' => 'multiple',
				'source' => array(),
			),
			'count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Post count', 'cryptop' ),
				'value' => '3',
			),		
			'visible_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Posts per slide', 'cryptop' ),
				'value' => '3',
			),
			'chars_count' => array(
				'type' => 'number',
				'title' => esc_html__( 'Count of chars from post content', 'cryptop' ),
				'value' => '50',
			),
			'text_align' => array(
				'title' => esc_html__( 'Text align', 'cryptop' ),
				'type' => 'radio',
				'value' => array(
					'left' => array( esc_html__( 'Left', 'cryptop' ), 	true, 	'' ),
					'center' => array( esc_html__( 'Center', 'cryptop' ), 	false, 	'' ),
					'right' =>array( esc_html__( 'Right', 'cryptop' ), false,'' ),
			),
			),	
			'wide' => array(
				'title' => esc_html__( 'Wide', 'cryptop' ),
				'type' => 'checkbox',
			),			
			'show' => array(
				'title' => esc_html__( 'Show', 'cryptop' ),
				'type' => 'select',
				'atts' => 'multiple',
				'source' => array(
					'image' => array( esc_html__( 'Featured Image', 'cryptop' ), true ),
					'title' => array( esc_html__( 'Titles', 'cryptop' ), true ),
					'content' => array( esc_html__( 'Content', 'cryptop' ), true ),
					'date' => array( esc_html__( 'Date', 'cryptop' ), false ),
					'comments' => array( esc_html__( 'Comments', 'cryptop' ), false ),
			),
			),	
		);
	}
	function get_widget_thumbnail_dims(){
		global $cws_theme_funcs;
		$footer_wide = $cws_theme_funcs->cws_get_meta_option( 'footer' )['wide'];
		$footer_layout = $cws_theme_funcs->cws_get_meta_option( 'footer' )['layout'];

		$width_correction = 0;
		switch ($footer_layout) {
			case '1':
				$width_correction = 20;
				break;				
			case '2':
				$width_correction = 20;
				break;				
			case '3':
				$width_correction = 20;
				break;				
			case '4':
				$width_correction = 20;
				break;				
		}		

		$resolution = $footer_wide == '1' ? 1920 : 1170;
		$width = ($resolution / (int) $footer_layout) - $width_correction;
		$height = $width / 2;

		return array(
			'width' => $width, 
			'height' => $height, 
			'crop' => true
		);
	}
	function __construct() {
		$widget_ops = array( 'classname' => 'widget_cws_recent_posts', 'description' => esc_html__( 'CWS most recent posts', 'cryptop' ) );
		parent::__construct( 'cws-recent-posts', esc_html__( 'CWS Recent Posts', 'cryptop' ), $widget_ops );
	}
	function widget( $args, $instance ) {
		extract( $args );

		extract( shortcode_atts( array(
			'title' => '',
			'title_align' => 'left',
			'hide_mark' => '0',
			'show_icon_opts' => '0',
			'cats' => array(),
			'exclude' => array(),
			'count' => '3',			
			'visible_count' => '3',
			'chars_count' => '50',
			'text_align' => 'left',
			'wide' => '0',
			'show' => array('image','title','content'),
		), $instance));
		global $cws_theme_funcs;

		$show_arr = array();
		if (is_string($show)){
			$show_arr[] = $show;
		} else {
			$show_arr = $show;
		}
		
		$use_blur = $cws_theme_funcs->cws_get_option( 'use_blur' );

		$title = esc_html($title);

		for ( $i=0; $i<count($cats); $i++ ){
			$term_obj = get_term_by( 'slug', $cats[$i], 'category' );
			if ($term_obj){
				$cats[$i] = $term_obj->term_id;
			}
		}

		$footer_is_rendered = isset( $GLOBALS['footer_is_rendered'] );

		/* defaults for empty text fields with number values */
		$count = empty( $count ) ? (int)get_option( 'posts_per_page' ) : (int)$count;
		$visible_count = empty( $visible_count ) ? $count : (int)$visible_count;
		$chars_count = empty( $chars_count ) ? 50 : (int)$chars_count;
		/* \defaults for empty text fields with number values */

		$q_args = array( 'category__in' => $cats, 'posts_per_page' => $count, 'ignore_sticky_posts' => true, 'post_status' => 'publish' );
		$tax_query = array();
		if(!empty($exclude)){
			$tax_query[] = array(
				'taxonomy' => 'post_format',
				'field' => 'slug',
				'terms' => $exclude,
				'operator' => 'NOT IN'
			);
		}
		$q_args['tax_query'] = $tax_query;
		$q = new WP_Query( $q_args );

		$show_icon_opts = ($show_icon_opts === 'on') ? '1' : $show_icon_opts;
		$widget_title_icon = $show_icon_opts === '1' ? $cws_theme_funcs->cws_widget_title_icon_rendering( $instance ) : '';
		$carousel_mode = $count > $visible_count;
		$counter = 0;

		$before_title = str_replace('widget-title', 'widget-title align-'.esc_attr($title_align).($hide_mark == '1' ? ' hide-mark' : '').(!empty( $widget_title_icon) ? ' has_icon' : '' ), $before_title);
		echo sprintf('%s',$before_widget);
		
			//Title
			if ( !empty( $widget_title_icon ) ){
				if ( !empty( $title ) ){
					echo sprintf("%s", $before_title) . "<div class='widget_title_box'><div class='widget_title_icon_section'>$widget_title_icon</div><div class='widget_title_text_section'>$title</div></div>" . $after_title;
				}
				else{
					echo sprintf("%s", $before_title) . $widget_title_icon . $after_title;
				}
			}
			else if ( !empty( $title ) ){
				echo sprintf("%s", $before_title) . esc_html($title) . $after_title;
			}
			//Title

			if ( $q->have_posts() ){
				echo "<div class='widget_wrapper ".(!empty($text_align) ? 'text_'.$text_align : '')."'>"; //widget_wrapper
				$item_class = '';
				$item_class .= $use_blur ? ' blurred' : '';
				if ( $carousel_mode ){
					wp_enqueue_script ('owl_carousel');
					echo "<div class='widget_carousel dots'>";
				} else if ( $footer_is_rendered ){
					echo "<div class='post_items'>";
				}
				while ( $q->have_posts() ):
					$q->the_post();

					//Get meta from post
					$media_meta = $cws_theme_funcs->cws_get_post_meta( get_the_ID(), 'cws_mb_post' );
					$media_meta = isset( $media_meta[0] ) ? $media_meta[0] : array();
					$post_format = get_post_format();
					$video_post = 'video' === $post_format;

					$cur_post = get_queried_object();
					$date_format = get_option( 'date_format' );
					$date = esc_html( get_the_time( $date_format ) );
					$permalink = esc_url(get_permalink());
					$comments_n = get_comments_number();

					if ($wide || $video_post){
						$dims = $this->get_widget_thumbnail_dims();						
					} else {
						$dims = array( 'width' => 70, 'height' => 70, 'crop' => true );
					}

					$post_class = '';
					$post_class .= $wide || $video_post ? ' wide' : ' no_wide';
					$post_class .= has_post_thumbnail() && wp_get_attachment_url( get_post_thumbnail_id()) && in_array('image',$show_arr) ? ' img_exist' : '';
					$post_class .= count($show_arr) == 2 && $show_arr[1] == 'image' ? ' only_img' : '';

					if ( $carousel_mode && $counter <= 0 ){
						echo "<div class='item ".esc_attr($item_class)."'>"; //open carousel item tag
					}
						echo "<div class='post_item".esc_attr($post_class)."'>"; //post item					
							echo "<div class='post_preview clearfix'>"; //post_preview

								//Image part
								if ( (( has_post_thumbnail() && wp_get_attachment_url( get_post_thumbnail_id()) ) || $video_post ) && in_array('image',$show_arr)){
									$featured_img_url = wp_get_attachment_url( get_post_thumbnail_id() );								
									$thumb_url = $cws_theme_funcs->cws_print_img_html( array('src' => $featured_img_url), $dims );

									if ($video_post){
										$video_url = isset( $media_meta['video'] ) ? $media_meta['video'] : '';
										echo apply_filters( 'the_content',"[embed width='".$dims['width']."']" .$video_url.'[/embed]' );										
									} else {
										echo "<div class='post_thumb'>";
											echo "<a href='".esc_url($permalink)."'>";
												echo "<img ".$thumb_url." alt />";
											echo "</a>";
										echo "</div>";								
									}
								}

								//Title part								
								if (in_array('title',$show_arr)){
								$post_title = esc_html( get_the_title() );
									echo !empty( $post_title ) ? "<div class='post_title'><a href='".esc_url($permalink)."'>$post_title</a></div>" : "";									
								}

								//Content part
								if (in_array('content',$show_arr)){
								$content = !empty( $cur_post->post_excerpt ) ? $cur_post->post_excerpt : get_the_content( '' );
								$content = trim( preg_replace( "/[\s]{2,}/", " ", strip_shortcodes( strip_tags( $content ) ) ) );
								$is_content_empty = empty( $content );
									if ( !$is_content_empty && in_array('content',$show_arr)){
									if ( strlen( $content ) > $chars_count ){
											$content = mb_substr( $content, 0, $chars_count );
											$content = wptexturize( $content ); //apply wp filter
												echo "<div class='post_content'>$content <a href='".esc_url($permalink)."'>" . esc_html__( "...", 'cryptop' ) . "</a></div>";
									}
									else{
										$content = wptexturize( $content ); //apply wp filter
										echo "<div class='post_content'>$content</div>";
									}
								}
								}

								//Date part
								if ( in_array('date',$show_arr) ){
									echo "<div class='post_date'>$date</div>";
								}

								//Comments part
								if ((int) $comments_n > 0 && in_array('comments',$show_arr)){
									$permalink .= "#comments";
									echo "<div class='post_comments icon_align_".esc_attr($text_align)."'><a href='".esc_url($permalink)."' title='".esc_attr($comments_n)." comments'>";
									echo "</a></div>";
								}

							echo "</div>"; //-post_preview
						echo "</div>"; //-post item
					if ( $carousel_mode ){
						if ( $counter >= $visible_count-1 || $q->current_post >= $q->post_count-1 ){
							echo "</div>"; //-close carousel item tag
							$counter = 0;
						} else {
							$counter ++;
						}
					}
				endwhile;
				wp_reset_postdata();

				if ($carousel_mode || $footer_is_rendered) echo "</div>"; //-widget_carousel || post_items
				echo "</div>"; //-widget_wrapper			
			} else{
				echo do_shortcode( "[cws_sc_msg_box text='" . esc_html__( 'There are no posts matching the query', 'cryptop' ) . "'][/cws_sc_msg_box]" );
			}
		echo sprintf('%s',$after_widget);
	}

	function update( $new_instance, $old_instance ) {
		$instance = (array)$new_instance;
		foreach ($new_instance as $key => $v) {
			switch ($this->fields[$key]['type']) {
				case 'text':
					$instance[$key] = strip_tags($v);
					break;
			}
		}
		return $instance;
	}

	function form( $instance ) {
		$this->init_fields();

		if (function_exists('cws_core_get_base_components')) {
			$g_components = cws_core_get_base_components();
		}	
		if (!empty($g_components) && function_exists('cws_core_build_settings')) {
			cws_core_build_settings($this->fields, $g_components);
		}

		if (function_exists('cws_core_build_layout') ) {
			echo cws_core_build_layout($instance, $this->fields, 'widget-' . $this->id_base . '[' . $this->number . '][');
		}
	}
}
?>