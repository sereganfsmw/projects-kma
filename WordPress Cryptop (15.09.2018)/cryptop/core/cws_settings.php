<?php

if(!function_exists('cws_get_to_settings')){
	function cws_get_to_settings(){
		$settings = array(
				'general_setting' => array(
					'type' => 'section',
					'title' => esc_html__( 'Header', 'cryptop' ),
					'icon' => array('fa', 'header'),
					// 'active' => true, // true by default
					'layout' => array(
						'general_cont' => array(
							'type' => 'tab',
							'init' => 'open',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Header', 'cryptop' ),
							'layout' => array(

								'header' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'order' => array(
											'type' => 'group',
											'addrowclasses' => 'group sortable drop grid-col-12 no_overflow',
											'tooltip' => array(
												'title' => esc_html__( 'Header Order', 'cryptop' ),
												'content' => esc_html__( 'Drag to reorder and customize the header.', 'cryptop' ),
											),
											'title' => esc_html__('Header order', 'cryptop' ),
											'button_title' => esc_html__('Add new sidebar', 'cryptop' ),
											'value' => array(
												array('title' => 'Top Bar','val' => 'top_bar_box'),
												array('title' => 'Header Zone','val' => 'drop_zone_start'),
												array('title' => 'Logo','val' => 'logo_box'),
												array('title' => 'Menu','val' => 'menu_box'),
												array('title' => 'Header Zone','val' => 'drop_zone_end'),
												array('title' => 'Title area','val' => 'title_box'),
											),
											'layout' => array(
												'title' => array(
													'type' => 'text',
													'value' => '',
													'atts' => 'data-role="title"',
													'title' => esc_html__('Sidebar', 'cryptop' ),
												),
												'val' => array(
													'type' => 'text',
												)
											)
										),
										'outside_slider' => array(
											'title' => esc_html__( 'Header Overlays Slider', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-4',
											'type' => 'checkbox',
										),					
										'customize' => array(
											'title' => esc_html__( 'Customize', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-4',
											'type' => 'checkbox',
											'atts' => 'data-options="e:background_image;e:overlay;e:spacings;e:override_topbar_color;e:override_menu_color;e:override_menu_color_hover;e:override_topbar_color_hover;"',
										),
										'background_image' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 inside-box groups',
											'layout' => '%image_layout%',
										),
										'overlay' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 disable inside-box groups',
											'layout' => array(
												'type'	=> array(
													'title'		=> esc_html__( 'Add Color overlay', 'cryptop' ),
													'addrowclasses' => 'grid-col-4',
													'type'	=> 'select',
													'source'	=> array(
														'none' => array( esc_html__( 'None', 'cryptop' ),  true, 'd:opacity;d:color;d:gradient;' ),
														'color' => array( esc_html__( 'Color', 'cryptop' ),  false, 'e:opacity;e:color;d:gradient;' ),
														'gradient' => array( esc_html__('Gradient', 'cryptop' ), false, 'e:opacity;d:color;e:gradient;' )
													),
												),
												'color'	=> array(
													'title'	=> esc_html__( 'Color', 'cryptop' ),
													'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
													'addrowclasses' => 'grid-col-4',
													'value' => CRYPTOP_FIRST_COLOR,
													'type'	=> 'text',
												),
												'opacity' => array(
													'type' => 'number',
													'title' => esc_html__( 'Opacity (%)', 'cryptop' ),
													'placeholder' => esc_html__( 'In percents', 'cryptop' ),
													'value' => '40',
													'addrowclasses' => 'grid-col-4',
												),
												'gradient' => array(
													'type' => 'fields',
													'addrowclasses' => 'grid-col-12 disable box inside-box groups',
													'layout' => '%gradient_layout%',
												),
											),
										),
										'override_menu_color'	=> array(
											'title'	=> esc_html__( '(Header Zone) Override Menu\'s Font Color', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'addrowclasses' => 'grid-col-6 disable',
											'value' => '#ffffff',
											'type'	=> 'text',
										),
										'override_menu_color_hover'	=> array(
											'title'	=> esc_html__( '(Header Zone) Override Menu\'s Font Color on Hover', 'cryptop' ),
											'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
											'addrowclasses' => 'grid-col-6 disable',
											'value' => CRYPTOP_FIRST_COLOR,
											'type'	=> 'text',
										),
										'override_topbar_color'	=> array(
											'title'	=> esc_html__( '(Header Zone) Override TopBar\'s Font Color', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'addrowclasses' => 'grid-col-6 disable',
											'value' => '#ffffff',
											'type'	=> 'text',
										),
										'override_topbar_color_hover' => array(
											'title'	=> esc_html__( '(Header Zone) Override TopBar\'s Font Color on Hover', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'addrowclasses' => 'grid-col-6 disable',
											'value' => '#ffffff',
											'type'	=> 'text',
										),
										'spacings' => array(
											'title' => esc_html__( 'Add Spacings', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'disable grid-col-4 two-inputs',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => ''),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => ''),
											),
										),
									),
								),

							)
						),
						'logo_cont' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Logo', 'cryptop' ),
							'layout' => array(

								'logo_box' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Logo', 'cryptop' ),
											'addrowclasses' => 'checkbox alt grid-col-12',
											'type' => 'checkbox',
											'atts' => 'checked',
										),															
										'default'	=> array(
											'title'		=> esc_html__( 'Default Logo Variation', 'cryptop' ),
											'addrowclasses' => 'grid-col-12',
											'type'	=> 'select',
											'source'	=> array(
												'dark' => array( esc_html__( 'Dark', 'cryptop' ),  true, '' ),
												'light' => array( esc_html__( 'Light', 'cryptop' ),  false, '' ),
											),
										),
										'dark' => array(
											'title' => esc_html__( 'Dark Logo', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
											'layout' => array(
												'is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'light' => array(
											'title' => esc_html__( 'Light Logo', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
											'layout' => array(
												'is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'dimensions' => array(
											'title' => esc_html__( 'Dimensions', 'cryptop' ),
											'type' => 'dimensions',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'width' => array('placeholder' => esc_html__( 'Width', 'cryptop' ), 'value' => ''),
												'height' => array('placeholder' => esc_html__( 'Height', 'cryptop' ), 'value' => ''),
											),
										),				
																						
										'position' => array(
											'title' => esc_html__( 'Position', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'left' => array( esc_html__('Left', 'cryptop'), true, 'd:site_name_in_menu;e:with_site_name;', '/img/align-left.png' ),
												'center' =>array( esc_html__('Center', 'cryptop'), false, 'e:site_name_in_menu;e:with_site_name;', '/img/align-center.png', ),
												'right' =>array( esc_html__('Right', 'cryptop'), false, 'd:site_name_in_menu;e:with_site_name;', '/img/align-right.png', ),
											),
										),
										'spacing' => array(
											'title' => esc_html__( 'Margins (px)', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '12'),
												'left' => array('placeholder' => esc_html__( 'left', 'cryptop' ), 'value' => '0'),
												'right' => array('placeholder' => esc_html__( 'Right', 'cryptop' ), 'value' => '0'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '12'),
											),
										),														
										'in_menu' => array(
											'title' => esc_html__( 'Logo in menu box', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
											'atts' => 'checked data-options="d:wide;d:overlay;d:border;"',
										),
										'wide' => array(
											'title' => esc_html__( 'Apply Full-Width Container', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
										),

										'overlay' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => array(
												'type'	=> array(
													'title'		=> esc_html__( 'Add Color overlay', 'cryptop' ),
													'addrowclasses' => 'grid-col-4',
													'type'	=> 'select',
													'source'	=> array(
														'none' => array( esc_html__( 'None', 'cryptop' ),  true, 'd:opacity;d:color;d:gradient;' ),
														'color' => array( esc_html__( 'Color', 'cryptop' ),  false, 'e:opacity;e:color;d:gradient;' ),
														'gradient' => array( esc_html__('Gradient', 'cryptop' ), false, 'e:opacity;d:color;e:gradient;' )
													),
												),
												'color'	=> array(
													'title'	=> esc_html__( 'Color', 'cryptop' ),
													'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
													'addrowclasses' => 'grid-col-4',
													'value' => CRYPTOP_FIRST_COLOR,
													'type'	=> 'text',
												),
												'opacity' => array(
													'type' => 'number',
													'title' => esc_html__( 'Opacity (%)', 'cryptop' ),
													'placeholder' => esc_html__( 'In percents', 'cryptop' ),
													'value' => '40',
													'addrowclasses' => 'grid-col-4',
												),
												'gradient' => array(
													'type' => 'fields',
													'addrowclasses' => 'grid-col-12 disable box inside-box groups',
													'layout' => '%gradient_layout%',
												),
											),
										),
										'border' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 inside-box groups',
											'layout' => '%border_layout%',
										),
									),
								),

							)
						),
						'menu_cont' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Menu', 'cryptop' ),
							'layout' => array(

								'menu_box' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Menu', 'cryptop' ),
											'addrowclasses' => 'checkbox alt grid-col-12',
											'type' => 'checkbox',
											'atts' => 'checked',
										),
										'position' => array(
											'title' => esc_html__( 'Menu Alignment', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), 	false, '', '/img/align-left.png' ),
												'center' =>array( esc_html__( 'Center', 'cryptop' ), false, '', '/img/align-center.png' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), true, '', '/img/align-right.png' ),
											),
										),
										'search_place' => array(
											'title' => esc_html__( 'Search Icon Location', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'none' => array( esc_html__( 'None', 'cryptop' ), 	false, '', '/img/no_layout.png' ),
												'top' => array( esc_html__( 'Top', 'cryptop' ), 	false, '', '/img/search-social-right.png' ),
												'left' =>array( esc_html__( 'Left', 'cryptop' ), false, '', '/img/search-menu-left.png' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), true, '', '/img/search-menu-right.png' ),
											),
										),							
										'mobile_place' => array(
											'title' => esc_html__( 'Mobile Menu Location', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'left' =>array( esc_html__( 'Left', 'cryptop' ), true, '', '/img/hamb-left.png' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), false, '', '/img/hamb-right.png' ),
											),
										),
										'background_color' => array(
											'title' => esc_html__( 'Background color', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'value' => '#ffffff',
											'addrowclasses' => 'grid-col-6',
											'type' => 'text',
										),
										'background_opacity' => array(
											'type' => 'number',
											'title' => esc_html__( 'Opacity', 'cryptop' ),
											'placeholder' => esc_html__( 'In percents', 'cryptop' ),
											'addrowclasses' => 'grid-col-6',
											'value' => '100'
										),
										'font_color' => array(
											'type' => 'text',
											'title' => esc_html__( 'Override Font color', 'cryptop' ),
											'atts' => 'data-default-color="#000000"',
											'value' => '#000000',
											'addrowclasses' => 'grid-col-6',
										),
										'font_color_hover' => array(
											'type' => 'text',
											'title' => esc_html__( 'Override Font hover color', 'cryptop' ),
											'atts' => 'data-default-color="'.CRYPTOP_SECONDARY_COLOR.'"',
											'value' => CRYPTOP_SECONDARY_COLOR,
											'addrowclasses' => 'grid-col-6',
										),
										'border' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => '%border_layout%',
										),
										'margin' => array(
											'title' => esc_html__( 'Add Spacings', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'grid-col-12 two-inputs',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '12'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '12'),
											),
										),
										'sandwich' => array(
											'title' => esc_html__( 'Use mobile menu on desktop PCs', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-4',
											'type' => 'checkbox',
										),
										'wide' => array(
											'title' => esc_html__( 'Apply Full-Width Menu', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-4',
											'type' => 'checkbox',
											'atts' => 'checked',
										),						
									),
								),

							)
						),
						'mobile_menu_cont' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Mobile Menu', 'cryptop' ),
							'layout' => array(

								'mobile_menu' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'logo' => array(
											'title' => esc_html__( 'Mobile Logo', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
											'layout' => array(
												'logo_mobile_is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution mobile logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'dimensions_mobile' => array(
											'title' => esc_html__( 'Mobile Logo Dimensions', 'cryptop' ),
											'type' => 'dimensions',
											'addrowclasses' => 'grid-col-6',
											'value' => array(
												'width' => array('placeholder' => esc_html__( 'Width', 'cryptop' ), 'value' => ''),
												'height' => array('placeholder' => esc_html__( 'Height', 'cryptop' ), 'value' => ''),
											),
										),
										'theme'	=> array(
											'title'		=> esc_html__( 'Theme', 'cryptop' ),
											'addrowclasses' => 'grid-col-12',
											'type'	=> 'select',
											'source'	=> array(
												'light' => array( esc_html__( 'Light', 'cryptop' ),  true, '' ),
												'dark' => array( esc_html__( 'Dark', 'cryptop' ),  false, '' ),
											),
										),
										'enable_on_tablet' => array(
											'title' => esc_html__( 'Enable Mobile menu on tablets', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-6',
											'atts' => 'checked',
											'type' => 'checkbox',
										),
									),
								),

							)
						),
						'sticky_menu_cont' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Sticky', 'cryptop' ),
							'layout' => array(

								'sticky_menu' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Sticky menu', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-6 alt',
											'type' => 'checkbox',
										),
										'logo' => array(
											'title' => esc_html__( 'Sticky Logo', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
											'layout' => array(
												'logo_sticky_is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution sticky logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'dimensions_sticky' => array(
											'title' => esc_html__( 'Sticky Logotype Dimensions', 'cryptop' ),
											'type' => 'dimensions',
											'addrowclasses' => 'grid-col-6',
											'value' => array(
												'width' => array('placeholder' => esc_html__( 'Width', 'cryptop' ), 'value' => ''),
												'height' => array('placeholder' => esc_html__( 'Height', 'cryptop' ), 'value' => ''),
											),
										),
										'mode'	=> array(
											'title'		=> esc_html__( 'Select a Sticky\'s Mode', 'cryptop' ),
											'type'	=> 'select',
											'addrowclasses' => 'grid-col-6',
											'source'	=> array(
												'smart' => array( esc_html__( 'Smart', 'cryptop' ),  true ),
												'simple' => array( esc_html__( 'Simple', 'cryptop' ), false ),
											),
										),
										'margin_sticky' => array(
											'title' => esc_html__( 'Sticky Menu Spacings', 'cryptop' ),
											'type' => 'margins',
											'tooltip' => array(
												'title' => esc_html__('Sticky menu spacings', 'cryptop'),
												'content' => esc_html__('These values should not exceed the menu spacings, which are set in Menu\'s section', 'cryptop'),
											),
											'addrowclasses' => 'grid-col-6 two-inputs',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '12'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '12'),
											),
										),			
										'background_color' => array(
											'title' => esc_html__( 'Background color', 'cryptop' ),
											'tooltip' => array(
												'title' => esc_html__( 'Background Color', 'cryptop' ),
												'content' => esc_html__( 'This color is applied to header section including top bar.', 'cryptop' ),
											),
											'atts' => 'data-default-color="#ffffff"',
											'value' => '#ffffff',
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),
										'font_color' => array(
											'title' => esc_html__( 'Override Font color', 'cryptop' ),
											'tooltip' => array(
												'title' => esc_html__( 'Override Font Color', 'cryptop' ),
												'content' => esc_html__( 'This color is applied to main menu items only, submenus will use the color which is set in Typography section.<br /> This option is very useful when transparent menu is set.', 'cryptop' ),
											),
											'atts' => 'data-default-color="#595959"',
											'value' => '#595959',
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),
										'background_opacity' => array(
											'type' => 'number',
											'title' => esc_html__( 'Opacity', 'cryptop' ),
											'tooltip' => array(
												'title' => esc_html__( 'Header Opacity', 'cryptop' ),
												'content' => esc_html__( 'This option will apply the transparent header when set to "0".', 'cryptop' ),
											),
											'placeholder' => esc_html__( 'In percents', 'cryptop' ),
											'addrowclasses' => 'grid-col-4',
											'value' => '30'
										),
										'border' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => '%border_layout%',
										),
										'shadow' => array(
											'title' => esc_html__( 'Add Shadow', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
										),
									),
								),

							)
						),
						'title_area_cont' => array(
							'type' => 'tab',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Title', 'cryptop' ),
							'layout' => array(

								'title_box' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Title area', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12 alt',
											'type' => 'checkbox',
											'atts' => 'checked',
										),							
										'text_center' => array(
											'title' => esc_html__( 'Center Title & Breadcrumbs', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
											'atts' => 'checked',
										),
										'no_title' => array(
											'title' => esc_html__( 'Hide Page Title', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
										),	
										'customize' => array(
											'title' => esc_html__( 'Customize', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
											'atts' => 'checked data-options="e:animate;e:slide_down;e:font_color;e:show_on_posts;e:show_on_archives;e:background_image;e:overlay;e:use_pattern;e:use_blur;e:effect;e:spacings;"',
										),
										'slide_down' => array(
											'title' => esc_html__( 'Slide down Header on Page Load', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-4',
											'type' => 'checkbox',
										),
										'show_on_posts' => array(
											'title' => esc_html__( 'Use Custom Settings on Posts', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-4',
											'atts' => 'checked',
											'type' => 'checkbox',
										),
										'show_on_archives' => array(
											'title' => esc_html__( 'Use Custom Settings on Archives', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-4',
											'type' => 'checkbox',
										),
										'background_image' => array(
											'type' => 'fields',
											'addrowclasses' => 'disable box grid-col-12 inside-box groups',
											'layout' => '%image_layout%',
										),
										'effect' => array(
											'title' => esc_html__( 'Image style', 'cryptop' ),
											'type' => 'radio',
											'addrowclasses' => 'disable grid-col-12',
											'value' => array(
												'none' => array( esc_html__( 'None', 'cryptop' ),  true, 'd:parallax_options;d:scroll_parallax;' ),
												'scroll_parallax' => array( esc_html__( 'Scroll Parallax', 'cryptop' ),  false, 'e:scroll_parallax;' ),
												'hover_parallax' =>array( esc_html__( 'Parallaxify on hover', 'cryptop' ), false, 'd:scroll_parallax;e:parallax_options;' ),
											),
										),
										'scroll_parallax' => array(
											'title' => esc_html__( 'Add Motion Zoom to Header Image on Page Scroll', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-12',
											'type' => 'checkbox',
										),
										'parallax_options' => array(
											'type' => 'fields',
											'addrowclasses' => 'disable grid-col-12 box inside-box groups',
											'layout' => '%parallax_layout%',
										),								
										'font_color' => array(
											'title'	=> esc_html__( 'Override Font Color', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'value' => '#ffffff',
											'addrowclasses' => 'disable grid-col-6',
											'type'	=> 'text',
										),
										'overlay' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 disable box inside-box groups',
											'layout' => array(
												'type'	=> array(
													'title'		=> esc_html__( 'Add Color overlay', 'cryptop' ),
													'addrowclasses' => 'grid-col-4',
													'type'	=> 'select',
													'source'	=> array(
														'none' => array( esc_html__( 'None', 'cryptop' ),  false, 'd:opacity;d:color;d:gradient;' ),
														'color' => array( esc_html__( 'Color', 'cryptop' ),  true, 'e:opacity;e:color;d:gradient;' ),
														'gradient' => array( esc_html__('Gradient', 'cryptop' ), false, 'e:opacity;d:color;e:gradient;' )
													),
												),
												'color'	=> array(
													'title'	=> esc_html__( 'Color', 'cryptop' ),
													'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
													'addrowclasses' => 'grid-col-4',
													'value' => CRYPTOP_FIRST_COLOR,
													'type'	=> 'text',
												),
												'opacity' => array(
													'type' => 'number',
													'title' => esc_html__( 'Opacity (%)', 'cryptop' ),
													'placeholder' => esc_html__( 'In percents', 'cryptop' ),
													'value' => '100',
													'addrowclasses' => 'grid-col-4',
												),
												'gradient' => array(
													'type' => 'fields',
													'addrowclasses' => 'grid-col-12 disable box inside-box groups',
													'layout' => '%gradient_layout%',
												),
											),
										),
										'use_pattern' => array(
											'title' => esc_html__( 'Add pattern', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-12',
											'type' => 'checkbox',
											'atts' => 'data-options="e:pattern_image;"',
										),
										'pattern_image' => array(
											'type' => 'fields',
											'title' => esc_html__( 'Pattern image', 'cryptop' ),
											'addrowclasses' => 'disable box grid-col-12 inside-box groups',
											'layout' => '%image_layout%',
										),	
										'breadcrumbs_divider' => array(
											'title' => esc_html__( 'Breadcrumbs Divider', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-12',
											//You can set default valut just uncomment this line
											// 'value' => array( 'id' => '', 'src' => get_template_directory_uri() . '\img\logo_the8_128x128.png') ,
											'layout' => array(
												'is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'breadcrumbs_dimensions' => array(
											'title' => esc_html__( 'Breadcrumbs Divider Dimensions', 'cryptop' ),
											'type' => 'dimensions',
											'addrowclasses' => 'grid-col-6',
											'value' => array(
												'width' => array('placeholder' => esc_html__( 'Width', 'cryptop' ), 'value' => ''),
												'height' => array('placeholder' => esc_html__( 'Height', 'cryptop' ), 'value' => ''),
											),
										),
										'breadcrumbs-margin' => array(
											'title' => esc_html__( 'Margins (px)', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'grid-col-6',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '0'),
												'left' => array('placeholder' => esc_html__( 'left', 'cryptop' ), 'value' => '0'),
												'right' => array('placeholder' => esc_html__( 'Right', 'cryptop' ), 'value' => '0'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '0'),
												),
										),

										'use_blur' => array(
											'title' => esc_html__( 'Apply blur', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-12',
											'type' => 'checkbox',
											'atts' => 'data-options="e:blur_intensity;"',
										),
										'blur_intensity' => array(
											'type' => 'number',
											'addrowclasses' => 'disable grid-col-12',
											'title' => esc_html__( 'Intensity', 'cryptop' ),
											'placeholder' => esc_html__( 'In percents', 'cryptop' ),
											'value' => '8'
										),
										'animate' => array(
											'title' => esc_html__( 'Add Mouse Scroll Animation', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-12',
											'type' => 'checkbox',
											'tooltip' => array(
												'title' => esc_html__( 'Documentation', 'cryptop' ),
												'content' => esc_html__( 'Project on https://github.com/Prinzhorn/skrollr <a href="https://github.com/Prinzhorn/skrollr">GitHub</a>', 'cryptop' ),
											),
											'atts' => 'checked data-options="e:animate_options;"',
										),
										'animate_options' => array(
											'type' => 'group',
											'addrowclasses' => 'disable group expander grid-col-12',
											'title' => esc_html__('Animation Steps', 'cryptop' ),
											'button_title' => esc_html__('Add New Step', 'cryptop' ),
											'layout' => array(
												'element'	=> array(
													'title'		=> esc_html__( 'Header Section', 'cryptop' ),
													'type'	=> 'select',
													'addrowclasses' => 'grid-col-3',
													'source'	=> array(
														'title' => array( esc_html__( 'Title & Breadcrumbs', 'cryptop' ),  true, '' ),
														'container' => array( esc_html__( 'Content Width', 'cryptop' ),  false, '' ),
														'section' => array( esc_html__( 'Full Width', 'cryptop' ), false, '' )
													),
												),
												'value' => array(
													'type' => 'number',
													'atts' => 'data-role="title"',
													'value' => '100',
													'addrowclasses' => 'grid-col-3',
													'title' => esc_html__('Top offset (in px)', 'cryptop' ),
												),
												'styles' => array(
													'type' => 'textarea',
													'atts' => 'rows="5"',
													'addrowclasses' => 'grid-col-6',
													'title' => esc_html__('Styles CSS', 'cryptop' ),
												),
											),
										),
										'spacings' => array(
											'title' => esc_html__( 'Add Spacings', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'disable two-inputs grid-col-6',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '60'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '60'),
											),
										),
										'border' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => '%border_layout%',
										),
									),
								),

							)
						),
						'top_bar_cont' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Top Bar', 'cryptop' ),
							'layout' => array(

								'top_bar_box' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Top Bar', 'cryptop' ),
											'addrowclasses' => 'grid-col-12 checkbox alt',
											'type' => 'checkbox',
										),
										'wide' => array(
											'title' => esc_html__( 'Apply Full-Width Top Bar', 'cryptop' ),
											'addrowclasses' => 'grid-col-12 checkbox',
											'type' => 'checkbox',
										),							
										'language_bar' => array(
											'title' => esc_html__( 'Add Language Bar', 'cryptop' ),
											'addrowclasses' => 'grid-col-12 checkbox',
											'atts' => 'checked data-options="e:language_bar_position;"',
											'type' => 'checkbox',
										),
										'language_bar_position' => array(
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'disable grid-col-12',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), 	true, '', '/img/multilingual-left.png' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), false, '', '/img/multilingual-right.png' ),
											),
										),
										'social_place' => array(
											'title' => esc_html__( 'Social Icons Alignment', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-12',
											'value' => array(
												'left' =>array( esc_html__( 'Left', 'cryptop' ), true, '', '/img/social-left.png' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), false, '', '/img/social-right.png' ),
											),
										),								
										'toggle_share' => array(
											'title' => esc_html__( 'Toggle Social Icons', 'cryptop' ),
											'addrowclasses' => 'grid-col-12 checkbox',
											'atts' => 'checked',
											'type' => 'checkbox',
										),
										'text' => array(
											'title' => esc_html__( 'Content', 'cryptop' ),
											'addrowclasses' => 'grid-col-12 full_row',
											'tooltip' => array(
												'title' => esc_html__( 'Indent Adjusting', 'cryptop' ),
												'content' => esc_html__( 'Adjust Indents by multiple spaces.<br /> Line breaks are working too.', 'cryptop' ),
											),
											'type' => 'textarea',
											'atts' => 'rows="2"',
										),
										'content_items' => array(
											'type' => 'group',
											'addrowclasses' => 'grid-col-12 group expander sortable box',
											'title' => esc_html__('Top Bar Info', 'councilio' ),
											'button_title' => esc_html__('Add new info row', 'councilio' ),
											'layout' => array(
												'icon' => array(
													'type' => 'select',
													'addrowclasses' => 'grid-col-3 fai',
													'source' => 'fa',
													'title' => esc_html__('Select the icon', 'councilio' )
												),
												'title' => array(
													'type' => 'text',
													'atts' => 'data-role="title"',
													'addrowclasses' => 'grid-col-3',
													'title' => esc_html__('Write main info', 'councilio' ),
												),
												'url' => array(
													'type' => 'text',
													'addrowclasses' => 'grid-col-3',
													'title' => esc_html__('Write URL', 'councilio' ),
												),
												'link_type' => array(
													'type' => 'select',
													'addrowclasses' => 'grid-col-3 fai',
													'source' => array(
														'link' => array( esc_html__( 'Link', 'councilio' ),  true, '' ),
														'mailto:' => array( esc_html__( 'Email', 'councilio' ),  false, '' ),
														'skype:' => array( esc_html__( 'Skype', 'councilio' ),  false, '' ),
														'tel:' => array( esc_html__( 'Phone', 'councilio' ),  false, '' ),
													),
													'title' => esc_html__('Select link type', 'councilio' )
												),
											),
										),
										'background_color' => array(
											'title' => esc_html__( 'Customize Background', 'cryptop' ),
											'atts' => 'data-default-color="'. CRYPTOP_FIRST_COLOR .'"',
											'value' => CRYPTOP_FIRST_COLOR,
											'addrowclasses' => 'new_row grid-col-3',
											'type' => 'text',
										),
										'font_color' => array(
											'title' => esc_html__( 'Font Color', 'cryptop' ),
											'atts' => 'data-default-color="#fff"',
											'value' => '#fff',
											'addrowclasses' => 'grid-col-3',
											'type' => 'text',
										),
										'hover_font_color' => array(
											'title' => esc_html__( 'Hover Color', 'cryptop' ),
											'atts' => 'data-default-color="#f5881b"',
											'value' => '#f5881b',
											'addrowclasses' => 'grid-col-3',
											'type' => 'text',
										),									
										'background_opacity' => array(
											'type' => 'number',
											'title' => esc_html__( 'Opacity (%)', 'cryptop' ),
											'placeholder' => esc_html__( 'In percents', 'cryptop' ),
											'value' => '100',
											'addrowclasses' => 'grid-col-3',
										),
										'border' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => '%border_layout%',
										),
										'spacings' => array(
											'title' => esc_html__( 'Add Spacings (px)', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'new_row grid-col-12 two-inputs',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '5'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '5'),
											),
										),	
									),
								),

							)
						),
						'side_panel_cont' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Sidebar', 'cryptop' ),
							'layout' => array(

								'side_panel' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Side Panel', 'cryptop' ),
											'addrowclasses' => 'alt checkbox grid-col-12',
											'type' => 'checkbox',
										),	
										// 1st row
										'theme'	=> array(
											'title'		=> esc_html__( 'Color Variation', 'cryptop' ),
											'addrowclasses' => 'grid-col-12',
											'type'	=> 'select',
											'source'	=> array(
												'dark' => array( esc_html__( 'Dark', 'cryptop' ),  true, '' ),
												'light' => array( esc_html__( 'Light', 'cryptop' ),  false, '' ),
											),
										),
										'place' => array(
											'title' => esc_html__( 'Menu Icon Location', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-12',
											'value' => array(
												'topbar_left' =>array( esc_html__( 'TopBar (Left)', 'cryptop' ), false, '', '/img/top-hamb-left.png' ),
												'topbar_right' => array( esc_html__( 'TopBar (Right)', 'cryptop' ), 	false, '', '/img/top-hamb-right.png' ),
												'menu_left' =>array( esc_html__( 'Menu (Left)', 'cryptop' ), true, '', '/img/hamb-left.png' ),
												'menu_right' =>array( esc_html__( 'Menu (Right)', 'cryptop' ), false, '', '/img/hamb-right.png' ),
											),
										),
										'position' => array(
											'title' 			=> esc_html__('Side Panel Position', 'cryptop' ),
											'type' 				=> 'radio',
											'subtype' 			=> 'images',
											'addrowclasses' => 'grid-col-12',
											'value' 			=> array(
												'left' 				=> 	array( esc_html__('Left', 'cryptop' ), true, '',	'/img/left.png' ),
												'right' 			=> 	array( esc_html__('Right', 'cryptop' ), false, '', '/img/right.png' ),
											),
										),								
										'sidebar' => array(
											'title' 		=> esc_html__('Select the Sidebar Area', 'cryptop' ),
											'type' 			=> 'select',
											'addrowclasses' => 'new_row grid-col-12',
											'source' 		=> 'sidebars',
											'value' => 'side_panel',
										),
										'appear'	=> array(
											'title'		=> esc_html__( 'Animation Format', 'cryptop' ),
											'type'	=> 'select',
											'addrowclasses' => 'grid-col-12',
											'source'	=> array(
												'fade' => array( esc_html__( 'Fade', 'cryptop' ),  true ),
												'slide' => array( esc_html__( 'Slide', 'cryptop' ), false ),
												'pull' => array( esc_html__( 'Pull', 'cryptop' ), false ),
											),
										),
										'logo_dark' => array(
											'title' => esc_html__( 'Logo (Dark)', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
											'layout' => array(
												'is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'logo_light' => array(
											'title' => esc_html__( 'Logo (Light)', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
											'layout' => array(
												'is_high_dpi' => array(
													'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
													'addrowclasses' => 'checkbox',
													'type' => 'checkbox',
												),
											),
										),
										'logo_position' => array(
											'title' => esc_html__( 'Logo position', 'cryptop' ),
											'addrowclasses' => 'grid-col-12',
											'type' => 'radio',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ),  true, '' ),
												'center' =>array( esc_html__( 'Center', 'cryptop' ), false,  '' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), false,  '' ),
											),
										),
										'logo_dimensions' => array(
											'title' => esc_html__( 'Logo Dimensions', 'cryptop' ),
											'type' => 'dimensions',
											'addrowclasses' => 'grid-col-12',
											'value' => array(
												'width' => array('placeholder' => esc_html__( 'Width', 'cryptop' ), 'value' => ''),
												'height' => array('placeholder' => esc_html__( 'Height', 'cryptop' ), 'value' => ''),
											),
										),
										'close_position' => array(
											'title' => esc_html__( 'Close button position', 'cryptop' ),
											'addrowclasses' => 'grid-col-12',
											'type' => 'radio',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ),  true, '' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), false,  '' ),
											),
										),								
										// 4th row
										'bg_dark' => array(
											'title' => esc_html__( 'Background (Dark)', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
										),
										'bg_light' => array(
											'title' => esc_html__( 'Background (Light)', 'cryptop' ),
											'type' => 'media',
											'url-atts' => 'readonly',
											'addrowclasses' => 'grid-col-6',
										),
										// 5th row
										'bg_size' => array(
											'title' => esc_html__( 'Background size', 'cryptop' ),
											'addrowclasses' => 'grid-col-4',
											'type' => 'radio',
											'value' => array(
												'cover' => array( esc_html__( 'Cover', 'cryptop' ),  true, '' ),
												'contain' =>array( esc_html__( 'Contain', 'cryptop' ), false,  '' ),
											),
										),
										'bg_opacity' => array(
											'type' => 'number',
											'title' => esc_html__( 'Background Opacity', 'cryptop' ),
											'placeholder' => esc_html__( 'In percents', 'cryptop' ),
											'addrowclasses' => 'grid-col-4',
											'value' => '100'
										),
										'bg_position' => array(
											'title' => esc_html__( 'Background Position', 'cryptop' ),
											'addrowclasses' => 'grid-col-4',
											'cols' => 3,
											'type' => 'radio',
											'value' => array(
												'tl'=>	array( '', false ),
												'tc'=>	array( '', false ),
												'tr'=>	array( '', false ),
												'cl'=>	array( '', false ),
												'cc'=>	array( '', true ),
												'cr'=>	array( '', false ),
												'bl'=>	array( '', false ),
												'bc'=>	array( '', false ),
												'br'=>	array( '', false ),
											),
										),
										'bg_color'	=> array(
											'title'	=> esc_html__( 'Background Color', 'cryptop' ),
											'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
											'addrowclasses' => 'grid-col-6',
											'value' => CRYPTOP_FIRST_COLOR,
											'type'	=> 'text',
										),
										'bg_font_color'	=> array(
											'title'	=> esc_html__( 'Font color', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'addrowclasses' => 'grid-col-6',
											'value' => '#ffffff',
											'type'	=> 'text',
										),
										// 6th row
										'overlay' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => array(
												'type'	=> array(
													'title'		=> esc_html__( 'Overlay color', 'cryptop' ),
													'addrowclasses' => 'grid-col-4',
													'type'	=> 'select',
													'source'	=> array(
														'none' => array( esc_html__( 'None', 'cryptop' ),  true, 'd:opacity;d:color;d:gradient;' ),
														'color' => array( esc_html__( 'Color', 'cryptop' ),  false, 'e:opacity;e:color;d:gradient;' ),
														'gradient' => array( esc_html__('Gradient', 'cryptop' ), false, 'e:opacity;d:color;e:gradient;' )
													),
												),
												'color'	=> array(
													'title'	=> esc_html__( 'Color', 'cryptop' ),
													'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
													'addrowclasses' => 'grid-col-4',
													'value' => CRYPTOP_FIRST_COLOR,
													'type'	=> 'text',
												),
												'opacity' => array(
													'type' => 'number',
													'title' => esc_html__( 'Opacity (%)', 'cryptop' ),
													'placeholder' => esc_html__( 'In percents', 'cryptop' ),
													'value' => '40',
													'addrowclasses' => 'grid-col-4',
												),
												'gradient' => array(
													'type' => 'fields',
													'addrowclasses' => 'grid-col-12 disable box inside-box groups',
													'layout' => '%gradient_layout%',
												),
											),
										),
										'bottom_bar' => array(
											'type' => 'fields',
											'addrowclasses' => 'inside-box groups grid-col-12 box',
											'layout' => array(
												'info_icons' => array(
													'type' => 'group',
													'addrowclasses' => 'group sortable grid-col-12 box',
													'title' => esc_html__('Information', 'cryptop' ),
													'button_title' => esc_html__('Add new information row', 'cryptop' ),
													'button_icon' => 'fa fa-plus',
													'layout' => array(
														'title' => array(
															'type' => 'text',
															'atts' => 'data-role="title"',
															'addrowclasses' => 'grid-col-6',
															'title' => esc_html__('Title', 'cryptop' ),
														),
														'icon' => array(
															'type' => 'select',
															'addrowclasses' => 'fai grid-col-6',
															'source' => 'fa',
															'title' => esc_html__('Icon', 'cryptop' )
														),
													)
												),																
											),
										),

									),
								),

							)
						),
					)
				),
				// end of sections
				'footer_options' => array(
					'type' => 'section',
					'title' => esc_html__('Footer', 'cryptop' ),
					'icon' => array('fa', 'list-alt'),
					'layout' => array(
						'footer_cont' => array(
							'type' => 'tab',
							'init' => 'open',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Footer', 'cryptop' ),
							'layout' => array(

								'footer' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'menu_enable' => array(
											'title' => esc_html__( 'Footer Menu', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
											'atts' => 'checked data-options="e:override_menu;e:custom_menu;e:menu_position;"',
										),
										'menu_position' => array(
											'title' => esc_html__( 'Menu Alignment', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-4 disable',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), 	true, '', '/img/align-left.png' ),
												'right' =>array( esc_html__( 'Right', 'cryptop' ), false, '', '/img/align-right.png' ),
											),
										),							
										'fixed' => array(
											'title' => esc_html__( 'Apply Fixed Footer Style', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
										),
										'wide' => array(
											'title' => esc_html__( 'Apply Full-Width Footer', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-12',
											'type' => 'checkbox',
										),								
										'layout' => array(
											'type' => 'select',
											'title' => esc_html__( 'Select a layout', 'cryptop' ),
											'addrowclasses' => 'grid-col-6',
											'source' => array(
												'1' => array( esc_html__( '1/1 Column', 'cryptop' ),  false ),
												'2' => array( esc_html__( '2/2 Column', 'cryptop' ), false ),
												'3' => array( esc_html__( '3/3 Column', 'cryptop' ), false ),
												'4' => array( esc_html__( '4/4 Column', 'cryptop' ), false ),
												'two-three' => array( esc_html__( '2/3 + 1/3 Column', 'cryptop' ), false ),
												'one-two' => array( esc_html__( '1/3 + 2/3 Column', 'cryptop' ), false ),
												'one-three' => array( esc_html__( '1/4 + 3/4 Column', 'cryptop' ), false ),
												'one-one-two' => array( esc_html__( '1/4 + 1/4 + 2/4 Column', 'cryptop' ), false ),
												'two-one-one' => array( esc_html__( '2/4 + 1/4 + 1/4 Column', 'cryptop' ), true ),
												'one-two-one' => array( esc_html__( '1/4 + 2/4 + 1/4 Column', 'cryptop' ), false ),
											),
										),
										'sidebar' => array(
											'title' 		=> esc_html__('Select Footer\'s Sidebar Area', 'cryptop' ),
											'type' 			=> 'select',
											'addrowclasses' => 'grid-col-6',
											'source' 		=> 'sidebars',
										),
										'text_alignment' => array(
											'type' => 'select',
											'title' => esc_html__( 'Text Alignment', 'cryptop' ),
											'addrowclasses' => 'grid-col-6',
											'source' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), 	true, '' ), 
												'center' => array( esc_html__( 'Center', 'cryptop' ), 	false, '' ),
												'right' => array( esc_html__( 'Right', 'cryptop' ), 	false, '' ), 
											),
										),							
										'copyrights_text' => array(
											'title' => esc_html__( 'Copyrights content', 'cryptop' ),
											'type' => 'textarea',
											'addrowclasses' => 'grid-col-12 full_row',
											'value' => 'Copyright © 2018. All rights reserved.',
											'atts' => 'rows="6"',
										),
										'background_image' => array(
											'type' => 'fields',
											'addrowclasses' => 'box grid-col-12 inside-box groups',
											'layout' => '%image_layout%',
										),							
										'background_color'	=> array(
											'title'	=> esc_html__( 'Background Color', 'cryptop' ),
											'atts'	=> 'data-default-color="#1d2326"',
											'value' => '#1d2326',
											'addrowclasses' => 'grid-col-4',
											'type'	=> 'text'
										),
										'title_color' => array(
											'title' => esc_html__( 'Titles Color', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'value' => '#ffffff',
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),								
										'font_color' => array(
											'title' => esc_html__( 'Font Color', 'cryptop' ),
											'atts' => 'data-default-color="#a6a6a6"',
											'value' => '#a6a6a6',
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),
										'copyrights_background_color' => array(
											'title'	=> esc_html__( 'Background Color (Copyrights)', 'cryptop' ),
											'atts' => 'data-default-color="#191e21"',
											'value' => '#191e21',
											'addrowclasses' => 'grid-col-4',
											'type'	=> 'text'
										),
										'copyrights_font_color' => array(
											'title' => esc_html__( 'Font color (Copyrights)', 'cryptop' ),
											'atts' => 'data-default-color="#a6a6a6"',
											'value' => '#a6a6a6',
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),
										'copyrights_hover_color' => array(
											'title' => esc_html__( 'Hover color (Copyrights)', 'cryptop' ),
											'atts' => 'data-default-color="#ffffff"',
											'value' => '#ffffff',
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),				
										'pattern_image' => array(
											'type' => 'fields',
											'title' => esc_html__( 'Pattern Image', 'cryptop' ),
											'addrowclasses' => 'box grid-col-12 inside-box groups',
											'layout' => '%image_layout%',
										),
										'overlay' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => array(
												'type'	=> array(
													'title'		=> esc_html__( 'Color overlay', 'cryptop' ),
													'addrowclasses' => 'grid-col-4',
													'type'	=> 'select',
													'source'	=> array(
														'none' => array( esc_html__( 'None', 'cryptop' ),  true, 'd:opacity;d:color;d:gradient;' ),
														'color' => array( esc_html__( 'Color', 'cryptop' ),  false, 'e:opacity;e:color;d:gradient;' ),
														'gradient' => array( esc_html__('Gradient', 'cryptop' ), false, 'e:opacity;d:color;e:gradient;' )
													),
												),
												'color'	=> array(
													'title'	=> esc_html__( 'Color', 'cryptop' ),
													'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
													'addrowclasses' => 'grid-col-4',
													'value' => CRYPTOP_FIRST_COLOR,
													'type'	=> 'text',
													'customizer' => array( 'show' => true )
												),
												'opacity' => array(
													'type' => 'number',
													'title' => esc_html__( 'Opacity (%)', 'cryptop' ),
													'placeholder' => esc_html__( 'In percents', 'cryptop' ),
													'value' => '40',
													'addrowclasses' => 'grid-col-4',
												),
												'gradient' => array(
													'type' => 'fields',
													'addrowclasses' => 'grid-col-12 disable box inside-box groups',
													'layout' => '%gradient_layout%',
												),
											),
										),
										'border' => array(
											'type' => 'fields',
											'addrowclasses' => 'grid-col-12 box inside-box groups',
											'layout' => '%border_layout%',
										),
										'instagram_feed' => array(
											'title' => esc_html__( 'Add Instagram Feed', 'cryptop' ),
											'addrowclasses' => 'checkbox grid-col-6',
											'type' => 'checkbox',
											'atts' => 'data-options="e:instagram_feed_shortcode;e:instagram_feed_full_width;"',
										),
										'instagram_feed_full_width' => array(
											'title' => esc_html__( 'Apply Full-Width Feed', 'cryptop' ),
											'addrowclasses' => 'disable checkbox grid-col-12',
											'type' => 'checkbox',
										),							
										'instagram_feed_shortcode' => array(
											'title' => esc_html__( 'Instagram Shortcode', 'cryptop' ),
											'addrowclasses' => 'disable grid-col-12 full_row',
											'type' => 'textarea',
											'atts' => 'rows="3"',
											'default' => '',
											'value' => '[instagram-feed cols=8 num=8 imagepadding=0 imagepaddingunit=px showheader=false showbutton=true showfollow=true]'
										),
										'spacings' => array(
											'title' => esc_html__( 'Add Spacings (px)', 'cryptop' ),
											'type' => 'margins',
											'addrowclasses' => 'new_row grid-col-12 two-inputs',
											'value' => array(
												'top' => array('placeholder' => esc_html__( 'Top', 'cryptop' ), 'value' => '10'),
												'bottom' => array('placeholder' => esc_html__( 'Bottom', 'cryptop' ), 'value' => '5'),
											),
										),	

									),
								),
							),
						),	
					)
				),	// end of sections

				'styling_options' => array(
					'type' => 'section',
					'title' => esc_html__('Styling options', 'cryptop' ),
					'icon' => array('fa', 'paint-brush'),
					'layout' => array(
						'styling_cont' => array(
							'type' => 'tab',
							'init' => 'open',
							'icon' => array('fa', 'calendar-plus-o'),
							'title' => esc_html__( 'Theme colors', 'cryptop' ),
							'layout' => array(
								'theme_colors' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'first_color' => array(
											'title' => esc_html__( 'Main color', 'cryptop' ),
											'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
											'value' => CRYPTOP_FIRST_COLOR,
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),						
										'second_color' => array(
											'title' => esc_html__( 'Theme Secondary Color', 'cryptop' ),
											'atts' => 'data-default-color="' . CRYPTOP_SECONDARY_COLOR . '"',
											'value' => CRYPTOP_SECONDARY_COLOR,
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),
										'third_color' => array(
											'title' => esc_html__( 'Helper color', 'cryptop' ),
											'atts' => 'data-default-color="' . CRYPTOP_HELPER_COLOR . '"',
											'value' => CRYPTOP_HELPER_COLOR,
											'addrowclasses' => 'grid-col-4',
											'type' => 'text',
										),
									),
								),
							)
						),
						'layout_options_layout' => array(
							'type' => 'tab',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Layout', 'cryptop' ),
							'layout' => array(
								'boxed_layout' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'enable' => array(
											'title' => esc_html__( 'Boxed Layout', 'cryptop' ),
											'addrowclasses' => 'checkbox alt',
											'type' => 'checkbox',
											'atts' => 'data-options="e:url_background;"',
										),
										'url_background' => array(
											'title' => esc_html__( 'Background Settings', 'cryptop' ),
											'type' => 'info',
											'addrowclasses' => 'disable',
											'icon' => array('fa', 'calendar-plus-o'),
											'value' => '<a href="'.get_admin_url(null, 'customize.php?autofocus[control]=background_image').'" target="_blank">'.esc_html__('Click this link to customize your background settings','cryptop').'</a>',
										),
									),
								),
							)
						)
					),
				),	// end of sections

				'layout_options' => array(
					'type' => 'section',
					'title' => esc_html__('Page layouts', 'cryptop' ),
					'icon' => array('fa', 'columns'),
					'layout'	=> array(
						'layout_options_homepage'	=> array(
							'type' => 'tab',
							'init'	=> 'open',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Home', 'cryptop' ),
							'layout' => array(
								'home_options' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(					
										'home_slider_type' => array(
											'title' => esc_html__('Slider', 'cryptop' ),
											'type' => 'radio',
											'value' => array(
												'none' => 	array( esc_html__('None', 'cryptop' ), true, 'd:home_slider_shortcode;d:home_video_slider;d:home_static_image' ),
												'img-slider'=>	array( esc_html__('Image Slider', 'cryptop' ), false, 'e:home_slider_shortcode;d:home_video_slider;d:home_static_image' ),
												'video-slider' => 	array( esc_html__('Video Slider', 'cryptop' ), false, 'd:home_slider_shortcode;e:home_video_slider;d:home_static_image' ),
												'stat-img-slider' => 	array( esc_html__('Static image', 'cryptop' ), false, 'd:home_slider_shortcode;d:home_video_slider;e:home_static_image' ),
											),
										),
										'home_slider_shortcode' => array(
											'title' => esc_html__( 'Slider shortcode', 'cryptop' ),
											'addrowclasses' => 'disable',
											'type' => 'text',
											'value' => '[rev_slider homepage]',
										),
										'home_video_slider' => array(
											'title' => esc_html__( 'Video Slider Setting', 'cryptop' ),
											'type' => 'fields',
											'addrowclasses' => 'disable groups',
											'layout' => '%video_slider_layout%',
										),// end of video-section
										'home_static_image' => array(
											'title' => esc_html__( 'Static image Slider Setting', 'cryptop' ),
											'type' => 'fields',
											'addrowclasses' => 'groups',
											'layout' => '%static_image_layout%',
										),// end of static img slider-section
									),
								),
								'home_sidebars' => array(
									'title' => esc_html__( 'Home Page Sidebar Layout', 'cryptop' ),
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'layout' => array(
											'title' => esc_html__('Sidebar Position', 'cryptop' ),
											'type' => 'radio',
											'addrowclasses' => 'box inside-box groups grid-col-12',
											'subtype' => 'images',
											'value' => array(
												'left' => 	array( esc_html__('Left', 'cryptop' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
												'right' => 	array( esc_html__('Right', 'cryptop' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
												'both' => 	array( esc_html__('Double', 'cryptop' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
												'none' => 	array( esc_html__('None', 'cryptop' ), true, 'd:sb1;d:sb2', '/img/none.png' )
											),
										),
										'sb1' => array(
											'title' => esc_html__('Select a sidebar', 'cryptop' ),
											'type' => 'select',
											'addrowclasses' => 'grid-col-12 disable box clear',
											'source' => 'sidebars',
										),
										'sb2' => array(
											'title' => esc_html__('Select right sidebar', 'cryptop' ),
											'type' => 'select',
											'addrowclasses' => 'grid-col-12 disable box',
											'source' => 'sidebars',
										),
									),
								),
							)
						),
						'layout_options_page' => array(
							'type' => 'tab',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Page', 'cryptop' ),
							'layout' => array(
								'page_sidebars' => array(
									'title' => esc_html__( 'Page Sidebar Layout', 'cryptop' ),
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'layout' => array(
											'type' => 'radio',
											'subtype' => 'images',
											'value' => array(
												'left' => 	array( esc_html__('Left', 'cryptop' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
												'right' => 	array( esc_html__('Right', 'cryptop' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
												'both' => 	array( esc_html__('Double', 'cryptop' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
												'none' => 	array( esc_html__('None', 'cryptop' ), true, 'd:sb1;d:sb2', '/img/none.png' )
											),
										),
										'sb1' => array(
											'title' => esc_html__('Select a sidebar', 'cryptop' ),
											'type' => 'select',
											'addrowclasses' => 'disable box',
											'source' => 'sidebars',
										),
										'sb2' => array(
											'title' => esc_html__('Select right sidebar', 'cryptop' ),
											'type' => 'select',
											'addrowclasses' => 'disable box',
											'source' => 'sidebars',
										),
									),
								),
							)
						),
						'layout_options_blog' => array(
							'type' => 'tab',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Blog', 'cryptop' ),
							'layout' => array(
								'blog_sidebars' => array(
									'title' => esc_html__( 'Blog Sidebars Settings', 'cryptop' ),
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'layout' => array(
											'title' => esc_html__('Sidebar Position', 'cryptop' ),
											'type' => 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-4',
											'value' => array(
												'left' => 	array( esc_html__('Left', 'cryptop' ), false, 'e:sb1;d:sb2',	'/img/left.png' ),
												'right' => 	array( esc_html__('Right', 'cryptop' ), false, 'e:sb1;d:sb2', '/img/right.png' ),
												'both' => 	array( esc_html__('Double', 'cryptop' ), false, 'e:sb1;e:sb2', '/img/both.png' ),
												'none' => 	array( esc_html__('None', 'cryptop' ), true, 'd:sb1;d:sb2', '/img/none.png' )
											),
										),
										'sb1' => array(
											'title' => esc_html__('Select a sidebar', 'cryptop' ),
											'type' => 'select',
											'addrowclasses' => 'disable grid-col-4',
											'source' => 'sidebars',
										),
										'sb2' => array(
											'title' => esc_html__('Select right sidebar', 'cryptop' ),
											'type' => 'select',
											'addrowclasses' => 'disable grid-col-4',
											'source' => 'sidebars',
										),
									),
								),
								'blog_options' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'blog_meta_align' => array(
											'title' => 'Meta Alignment',
											'type' => 'radio',
											'addrowclasses' => 'grid-col-3',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), true),
												'center' => array( esc_html__( 'Center', 'cryptop' ), false),
												'right' => array( esc_html__( 'Right', 'cryptop' ), false),
											),
										),
										'blog_content_align' => array(
											'title' => 'Content Alignment',
											'addrowclasses' => 'grid-col-3',
											'type' => 'radio',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), true),
												'center' => array( esc_html__( 'Center', 'cryptop' ), false),
												'right' => array( esc_html__( 'Right', 'cryptop' ), false),
											),
										),
										'blog_button_align' => array(
											'title' => 'Button Alignment',
											'type' => 'radio',
											'addrowclasses' => 'grid-col-3',
											'value' => array(
												'left' => array( esc_html__( 'Left', 'cryptop' ), false),
												'center' => array( esc_html__( 'Center', 'cryptop' ), false),
												'right' => array( esc_html__( 'Right', 'cryptop' ), true),
											),
										),						
										'blog_button_name' => array(
											'title' => esc_html__( 'Button Name', 'cryptop' ),
											'type' => 'text',
											'value' => 'Read More',
											'addrowclasses' => 'grid-col-6',	
										),
										'def_blog_chars_count' => array(
											'title' => esc_html__( 'Text Length', 'cryptop' ),
											'type' => 'text',
											'addrowclasses' => 'grid-col-6',
										),						
										'def_blogtype' => array(
											'title'		=> esc_html__( 'Blog Layout', 'cryptop' ),
											'desc'		=> esc_html__( 'Default Blog Layout', 'cryptop' ),
											'type'		=> 'radio',
											'subtype' => 'images',
											'addrowclasses' => 'grid-col-6',
											'value' => array(
												'1' => array( esc_html__('Large', 'cryptop' ), true, '', '/img/large.png'),
												'medium' => array( esc_html__('Medium', 'cryptop' ), false, '', '/img/medium.png'),
												'small' => array( esc_html__('Small', 'cryptop' ), false, '', '/img/small.png'),
												'2' => array( esc_html__('2 Cols', 'cryptop' ), false, '', '/img/pinterest_2_columns.png'),
												'3' => array( esc_html__('3 Cols', 'cryptop' ), false, '', '/img/pinterest_3_columns.png'),
												'4' => array( esc_html__('4 Cols', 'cryptop' ), false, '', '/img/pinterest_4_columns.png'),
											),
										),											
										'crop_related_items' => array(
											'title' => esc_html__( 'Crop Related Items of Single Posts', 'cryptop' ),
											'addrowclasses' => 'new_row checkbox grid-col-12',
											'type' => 'checkbox',
										),
										'def_hide_meta_related_items'	=> array(
											'title'		=> esc_html__( 'Hide meta (Related Items)', 'cryptop' ),
											'type'		=> 'select',
											'atts'		=> 'multiple',
											'addrowclasses' => 'grid-col-12',
											'source'		=> array(
												'' 				=> array( esc_html__( 'None', 'cryptop' ), false),
												'title' 		=> array( esc_html__( 'Title', 'cryptop' ), false),
												'cats' 			=> array( esc_html__( 'Categories', 'cryptop' ), true),
												'tags' 			=> array( esc_html__( 'Tags', 'cryptop' ), true),
												'author' 		=> array( esc_html__( 'Author', 'cryptop' ), true),
												'likes' 		=> array( esc_html__( 'Likes', 'cryptop' ), true),
												'date' 			=> array( esc_html__( 'Date', 'cryptop' ), true),
												'comments' 		=> array( esc_html__( 'Comments', 'cryptop' ), true),
												'read_more' 	=> array( esc_html__( 'Read More', 'cryptop' ), true),
												'social' 		=> array( esc_html__( 'Social Icons', 'cryptop' ), true),
												'excerpt' 		=> array( esc_html__( 'Excerpt', 'cryptop' ), true),
											)
										),
										'blog_aspect_ratio' => array(
											'title' => esc_html__( 'Keep Aspect Ratio', 'cryptop' ),
											'type' => 'checkbox',
											'addrowclasses' => 'checkbox grid-col-12',
										),
									),
								),
								'blog_slug' => array(
									'title' => esc_html__( 'Rename Blog', 'cryptop' ),
									'addrowclasses' => 'requirement grid-col-6',
									'type' 	=> 'text',
									'value'	=> 'Blog'
								),
							)
						),
						'layout_options_portfolio' => array(
							'type' => 'tab',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Portfolio', 'cryptop' ),
							'layout' => array(
								'portfolio_options' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'def_portfolio_layout' => array(
											'title'		=> esc_html__( 'Portfolio Layout', 'cryptop' ),
											'type'		=> 'radio',
											'subtype' => 'images',
											'tooltip' => array(
												'title' => esc_html__( 'Portfolio Layout', 'cryptop' ),
												'content' => esc_html__( 'This option is applied to portfolio archive pages only', 'cryptop' ),
											),
											'value' => array(
												'1' => array( esc_html__('Large', 'cryptop' ), false, '', '/img/large.png'),
												'2' => array( esc_html__('2 Cols', 'cryptop' ), false, '', '/img/pinterest_2_columns.png'),
												'3' => array( esc_html__('3 Cols', 'cryptop' ), false, '', '/img/pinterest_3_columns.png'),
												'4' => array( esc_html__('4 Cols', 'cryptop' ), true, '', '/img/pinterest_4_columns.png'),
											),
										),
										'def_portfolio_display_style' => array(
											'title' => esc_html__( 'Display as', 'cryptop' ),
											'type' => 'select',
											'source' => array(
												'grid' => array('Grid', true), // Title, isselected, data-options
												'filter_with_ajax' => array('Grid with filter(Ajax)', false),
												'filter' => array('Grid with filter', false),
												'carousel' => array('Carousel', false)
											),
										),
										'def_portfolio_show_meta'	=> array(
											'title'		=> esc_html__( 'Show Meta Data', 'cryptop' ),
											'type'		=> 'select',
											'atts'		=> 'multiple',
											'source'		=> array(
												'title'		=> array( esc_html__( 'Title', 'cryptop' ), true ),
												'excerpt'	=> array( esc_html__( 'Excerpt', 'cryptop' ), true ),
												'cats'		=> array( esc_html__( 'Categories', 'cryptop' ), false )
											)
										),								
										'def_portfolio_pagination_grid' => array(
											'title' => esc_html__( 'Pagination style', 'cryptop' ),
											'type' => 'radio',
											'value' => array(
												'standard' => array( 'Standard', true ),
												'load_more' => array( 'Load More', false ),
												'load_on_scroll' => array( 'Load on Scroll', false ),
												'ajax' => array( 'Ajax', false )
											),
										),
										'def_portfolio_chars_count' => array(
											'title' => esc_html__( 'Text Length', 'cryptop' ),
											'type' => 'text',
											'addrowclasses' => 'grid-col-6',
										),
										'def_portfolio_crop_images' => array(
											'title' => esc_html__( 'Crop Images', 'cryptop' ),
											'addrowclasses' => 'new_row checkbox grid-col-12',
											'type' => 'checkbox',
										),										
									),
								),
								'portfolio_slug' => array(
									'title' => esc_html__( 'Portfolio slug', 'cryptop' ),
									'type' => 'text',
									'addrowclasses' => 'requirement grid-col-6',
									'value' => 'portfolio',
								),
							)
						),
						'layout_options_staff' => array(
							'type' => 'tab',
							'icon' => array( 'fa', 'fa-book' ),
							'title' => esc_html__( 'Staff', 'cryptop' ),
							'layout' => array(
								'staff_options' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box main-box',
									'layout' => array(
										'def_staff_layout' => array(
											'title'		=> esc_html__( 'Staff Layout', 'cryptop' ),
											'type'		=> 'radio',
											'subtype' => 'images',
											'tooltip' => array(
												'title' => esc_html__( 'Staff Layout', 'cryptop' ),
												'content' => esc_html__( 'This option is applied to Staff archive pages only', 'cryptop' ),
											),
											'value' => array(
												'1' => array( esc_html__('Large', 'cryptop' ), false, '', '/img/large.png'),
												'2' => array( esc_html__('2 Cols', 'cryptop' ), false, '', '/img/pinterest_2_columns.png'),
												'3' => array( esc_html__('3 Cols', 'cryptop' ), false, '', '/img/pinterest_3_columns.png'),
												'4' => array( esc_html__('4 Cols', 'cryptop' ), true, '', '/img/pinterest_4_columns.png'),
											),
										),
										'def_staff_show_meta'	=> array(
											'title'		=> esc_html__( 'Show Meta Data', 'cryptop' ),
											'type'		=> 'select',
											'atts'		=> 'multiple',
											'source'		=> array(
												'title'			=> array( esc_html__( 'Title', 'cryptop' ), true ),
												'deps'			=> array( esc_html__( 'Departments', 'cryptop' ), true ),
												'poss'			=> array( esc_html__( 'Positions', 'cryptop' ), false ),
												'excerpt'		=> array( esc_html__( 'Excerpt', 'cryptop' ), true ),
												'experience'	=> array( esc_html__( 'Experience', 'cryptop' ), true ),
												'email'			=> array( esc_html__( 'Email', 'cryptop' ), true ),
												'biography'		=> array( esc_html__( 'Biography', 'cryptop' ), true ),
												'link_button'	 => array( esc_html__( 'Link Button', 'cryptop' ), true ),
												'socials'		=> array( esc_html__( 'Social Links', 'cryptop' ), false )
											)
										),
										'def_staff_pagination_grid' => array(
											'title' => esc_html__( 'Pagination style', 'cryptop' ),
											'type' => 'radio',
											'value' => array(
												'standard' => array( 'Standard', true ),
												'load_more' => array( 'Load More', false ),
												'load_on_scroll' => array( 'Load on Scroll', false ),
												'ajax' => array( 'Ajax', false )
											),
										),
										'def_staff_chars_count' => array(
											'title' => esc_html__( 'Text Length', 'cryptop' ),
											'type' => 'text',
											'addrowclasses' => 'grid-col-6',
										),
									),
								),
								'staff_slug' => array(
									'title' => esc_html__( 'Staff slug', 'cryptop' ),
									'type' => 'text',
									'addrowclasses' => 'requirement grid-col-6',
									'value' => 'staff',
								),

							)
						),				
						'layout_options_sidebar_generator' => array(
							'type' => 'tab',
							'customizer' 	=> array( 'show' => false ),
							'icon' => array('fa', 'calendar-plus-o'),
							'title' => esc_html__( 'Sidebars', 'cryptop' ),
							'layout' => array(
								'sidebars' => array(
									'type' => 'group',
									'addrowclasses' => 'group single_field requirement inside-box grid-col-12 box main-box',
									'title' => esc_html__('Sidebar generator', 'cryptop' ),
									'button_title' => esc_html__('Add new sidebar', 'cryptop' ),
									'value' => array(
											array('title' => 'Footer'),
											array('title' => 'Blog Right'),
											array('title' => 'Blog Left'),
											array('title' => 'Page Right'),
											array('title' => 'Page Left'),
											array('title' => 'Side Panel'),
									),
									'layout' => array(
										'title' => array(
											'type' => 'text',
											'value' => 'New Sidebar',
											'atts' => 'data-role="title"',
											'verification' => array (
												'length' => array( array('!0'), esc_html__('Title should not be empty', 'cryptop' )),
											),
											'title' => esc_html__('Sidebar', 'cryptop' ),
										)
									)
								),
								'sticky_sidebars' => array(
									'title' => esc_html__( 'Sticky sidebars', 'cryptop' ),
									'addrowclasses' => 'checkbox alt grid-col-6',
									'atts' => 'checked',
									'type' => 'checkbox',
								)
							)
						),
					)
				),	// end of sections
				'typography_options' => array(
					'type' => 'section',
					'title' => esc_html__('Typography', 'cryptop' ),
					'icon' => array('fa', 'font'),
					'layout' => array(
						'menu_font_options' => array(
							'type' => 'tab',
							'init' => 'open',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Menu', 'cryptop' ),
							'layout' => array(
								'menu-font' => array(
									'title' => esc_html__('Menu Font', 'cryptop' ),
									'type' => 'font',
									'font-color' => true,
									'font-size' => true,
									'font-sub' => true,
									'line-height' => true,
									'value' => array(
										'font-size' => '17px',
										'line-height' => 'initial',
										'color' => '#000000',
										'font-family' => 'Rubik',
										'font-weight' => array( 'regular'),
										'font-sub' => array('latin'),
									)
								)
							)
						),
						'header_font_options' => array(
							'type' => 'tab',
							'icon' => array('fa', 'font'),
							'title' => esc_html__( 'Header', 'cryptop' ),
							'layout' => array(
								'header-font' => array(
									'title' => esc_html__('Header\'s Font', 'cryptop' ),
									'type' => 'font',
									'font-color' => true,
									'font-size' => true,
									'font-sub' => true,
									'line-height' => true,
									'value' => array(
										'font-size' => '54px',
										'line-height' => '45px',
										'color' => '#000000',
										'font-family' => 'Roboto',
										'font-weight' => array( 'regular', '700' ),
										'font-sub' => array('latin'),
									),
								)
							)
						),
						'body_font_options' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Body', 'cryptop' ),
							'layout' => array(
								'body-font' => array(
									'title' => esc_html__('Body Font', 'cryptop' ),
									'type' => 'font',
									'font-color' => true,
									'font-size' => true,
									'font-sub' => true,
									'line-height' => true,
									'value' => array(
										'font-size' => '16px',
										'line-height' => '24px',
										'color' => '#545454',
										'font-family' => 'Roboto',
										'font-weight' => array( 'regular','300' ),
										'font-sub' => array('latin'),
									)
								)
							)
						),
						'helper_font_options' => array(
							'type' => 'tab',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Helper', 'cryptop' ),
							'layout' => array(
								'helper-font' => array(
									'title' => esc_html__('Helper Font', 'cryptop' ),
									'type' => 'font',
									'font-color' => true,
									'font-size' => true,
									'font-sub' => true,
									'line-height' => true,
									'value' => array(
										'font-size' => '26px',
										'line-height' => '36px',
										'color' => '#545454',
										'font-family' => 'Fira Sans',
										'font-weight' => array( 'regular'),
										'font-sub' => array('latin'),
									)
								)
							)
						),

					)
				), // end of sections
				'help_options' => array(
					'type' => 'section',
					'title' => esc_html__('Maintenance & Help', 'cryptop' ),
					'icon' => array('fa', 'life-ring'),
					'layout' => array(
						'maintenance' => array(
							'type' => 'tab',
							'init' => 'open',
							'icon' => array('fa', 'calendar-plus-o'),
							'title' => esc_html__( 'Maintenance', 'cryptop' ),
							'layout' => array(
								'show_loader' => array(
									'title' => esc_html__( 'ShowLoader', 'cryptop' ),
									'addrowclasses' => 'grid-col-12 checkbox alt',
									'type' => 'checkbox',
									'atts' => 'checked data-options="e:loader_logo;e:overlay_loader_color"',
								),
								'loader_logo' => array(
									'title' => esc_html__( 'Loader logo (Square)', 'cryptop' ),
									'type' => 'media',
									'url-atts' => 'readonly',
									'addrowclasses' => 'grid-col-12 disable',
									'layout' => array(
										'logo_is_high_dpi' => array(
											'title' => esc_html__( 'High-Resolution logo', 'cryptop' ),
											'addrowclasses' => 'checkbox',
											'type' => 'checkbox',
										),
									),
								),
								'overlay_loader_color' => array(
									'title' => esc_html__( 'Loader Color', 'cryptop' ),
									'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
									'value' => CRYPTOP_FIRST_COLOR,
									'addrowclasses' => 'disable grid-col-12',
									'type' => 'text',
								),
								'breadcrumbs' => array(
									'title' => esc_html__( 'Show breadcrumbs', 'cryptop' ),
									'addrowclasses' => 'checkbox alt',
									'atts' => 'checked',
									'type' => 'checkbox',
								),
								'blog_author' => array(
									'title' => esc_html__( 'Show post author', 'cryptop' ),
									'addrowclasses' => 'checkbox alt',
									'atts' => 'checked',
									'type' => 'checkbox',
								),
								'_theme_purchase_code' => array(
									'title' => esc_html__( 'Theme purchase code', 'cryptop' ),
									'tooltip' => array(
										'title' => esc_html__( 'Item Purchase Code', 'cryptop' ),
										'content' => esc_html__( 'Fill in this field with your Item Purchase Code in order to get the demo content and further theme updates.<br/> Please note, this code is applied to the theme only, it will not register Revolution Slider or any other plugins.', 'cryptop' ),
									),
									'type' 	=> 'text',
									'value'	=> '',
									'customizer' 	=> array( 'show' => false )
								),
							)
						),
				    'animation' => array(
					     'type' => 'tab',
					     'icon' => array('fa', 'arrow-circle-o-up'),
					     'title' => esc_html__( 'Animation', 'cryptop' ),
					     'layout' => array(
							'animation_curve_menu'	=> array(
								'title'	=> esc_html__( 'Animation (Menu Anchors)', 'cryptop' ),
								'type'	=> 'select',
								'addrowclasses' => 'grid-col-4',
								'source'	=> array(
									'linear' => array( esc_html__( '1. linear', 'cryptop' ), false ),
									'swing' => array( esc_html__( '2. swing', 'cryptop' ), false ),
									'easeInQuad' => array( esc_html__( '3. easeInQuad', 'cryptop' ), false ),
									'easeOutQuad' => array( esc_html__( '4. easeOutQuad', 'cryptop' ), false ),
									'easeInOutQuad' => array( esc_html__( '5. easeInOutQuad', 'cryptop' ), false ),
									'easeInCubic' => array( esc_html__( '6. easeInCubic', 'cryptop' ), false ),
									'easeOutCubic' => array( esc_html__( '7. easeOutCubic', 'cryptop' ), true ),
									'easeInOutCubic' => array( esc_html__( '8. easeInOutCubic', 'cryptop' ), false ),
									'easeInQuart' => array( esc_html__( '9. easeInQuart', 'cryptop' ), false ),
									'easeOutQuart' => array( esc_html__( '10. easeOutQuart', 'cryptop' ), false ),
									'easeInOutQuart' => array( esc_html__( '11. easeInOutQuart', 'cryptop' ), false ),
									'easeInQuint' => array( esc_html__( '12. easeInQuint', 'cryptop' ), false ),
									'easeOutQuint' => array( esc_html__( '13. easeOutQuint', 'cryptop' ), false ),
									'easeInOutQuint' => array( esc_html__( '14. easeInOutQuint', 'cryptop' ), false ),
									'easeInSine' => array( esc_html__( '15. easeInSine', 'cryptop' ), false ),
									'easeOutSine' => array( esc_html__( '16. easeOutSine', 'cryptop' ), false ),
									'easeInOutSine' => array( esc_html__( '17. easeInOutSine', 'cryptop' ), false ),
									'easeInExpo' => array( esc_html__( '18. easeInExpo', 'cryptop' ), false ),
									'easeOutExpo' => array( esc_html__( '19. easeOutExpo', 'cryptop' ), false ),
									'easeInOutExpo' => array( esc_html__( '20. easeInOutExpo', 'cryptop' ), false ),
									'easeInCirc' => array( esc_html__( '21. easeInCirc', 'cryptop' ), false ),
									'easeOutCirc' => array( esc_html__( '22. easeOutCirc', 'cryptop' ), false ),
									'easeInOutCirc' => array( esc_html__( '23. easeInOutCirc', 'cryptop' ), false ),
									'easeInElastic' => array( esc_html__( '24. easeInElastic', 'cryptop' ), false ),
									'easeOutElastic' => array( esc_html__( '25. easeOutElastic', 'cryptop' ), false ),
									'easeInOutElastic' => array( esc_html__( '26. easeInOutElastic', 'cryptop' ), false ),
									'easeInBack' => array( esc_html__( '27. easeInBack', 'cryptop' ), false ),
									'easeOutBack' => array( esc_html__( '28. easeOutBack', 'cryptop' ), false ),
									'easeInOutBack' => array( esc_html__( '29. easeInOutBack', 'cryptop' ), false ),
									'easeInBounce' => array( esc_html__( '30. easeInBounce', 'cryptop' ), false ),
									'easeOutBounce' => array( esc_html__( '31. easeOutBounce', 'cryptop' ), false ),
									'easeInOutBounce' => array( esc_html__( '32. easeInOutBounce', 'cryptop' ), false ),
								),
							),
							'animation_curve_scrolltop'	=> array(
								'title'	=> esc_html__( 'Animation (ScrollTop)', 'cryptop' ),
								'type'	=> 'select',
								'addrowclasses' => 'grid-col-4',
								'source'	=> array(
									'linear' => array( esc_html__( '1. linear', 'cryptop' ), false ),
									'swing' => array( esc_html__( '2. swing', 'cryptop' ), false ),
									'easeInQuad' => array( esc_html__( '3. easeInQuad', 'cryptop' ), false ),
									'easeOutQuad' => array( esc_html__( '4. easeOutQuad', 'cryptop' ), false ),
									'easeInOutQuad' => array( esc_html__( '5. easeInOutQuad', 'cryptop' ), true ),
									'easeInCubic' => array( esc_html__( '6. easeInCubic', 'cryptop' ), false ),
									'easeOutCubic' => array( esc_html__( '7. easeOutCubic', 'cryptop' ), false ),
									'easeInOutCubic' => array( esc_html__( '8. easeInOutCubic', 'cryptop' ), false ),
									'easeInQuart' => array( esc_html__( '9. easeInQuart', 'cryptop' ), false ),
									'easeOutQuart' => array( esc_html__( '10. easeOutQuart', 'cryptop' ), false ),
									'easeInOutQuart' => array( esc_html__( '11. easeInOutQuart', 'cryptop' ), false ),
									'easeInQuint' => array( esc_html__( '12. easeInQuint', 'cryptop' ), false ),
									'easeOutQuint' => array( esc_html__( '13. easeOutQuint', 'cryptop' ), false ),
									'easeInOutQuint' => array( esc_html__( '14. easeInOutQuint', 'cryptop' ), false ),
									'easeInSine' => array( esc_html__( '15. easeInSine', 'cryptop' ), false ),
									'easeOutSine' => array( esc_html__( '16. easeOutSine', 'cryptop' ), false ),
									'easeInOutSine' => array( esc_html__( '17. easeInOutSine', 'cryptop' ), false ),
									'easeInExpo' => array( esc_html__( '18. easeInExpo', 'cryptop' ), false ),
									'easeOutExpo' => array( esc_html__( '19. easeOutExpo', 'cryptop' ), false ),
									'easeInOutExpo' => array( esc_html__( '20. easeInOutExpo', 'cryptop' ), false ),
									'easeInCirc' => array( esc_html__( '21. easeInCirc', 'cryptop' ), false ),
									'easeOutCirc' => array( esc_html__( '22. easeOutCirc', 'cryptop' ), false ),
									'easeInOutCirc' => array( esc_html__( '23. easeInOutCirc', 'cryptop' ), false ),
									'easeInElastic' => array( esc_html__( '24. easeInElastic', 'cryptop' ), false ),
									'easeOutElastic' => array( esc_html__( '25. easeOutElastic', 'cryptop' ), false ),
									'easeInOutElastic' => array( esc_html__( '26. easeInOutElastic', 'cryptop' ), false ),
									'easeInBack' => array( esc_html__( '27. easeInBack', 'cryptop' ), false ),
									'easeOutBack' => array( esc_html__( '28. easeOutBack', 'cryptop' ), false ),
									'easeInOutBack' => array( esc_html__( '29. easeInOutBack', 'cryptop' ), false ),
									'easeInBounce' => array( esc_html__( '30. easeInBounce', 'cryptop' ), false ),
									'easeOutBounce' => array( esc_html__( '31. easeOutBounce', 'cryptop' ), false ),
									'easeInOutBounce' => array( esc_html__( '32. easeInOutBounce', 'cryptop' ), false ),
								),
							),
							'animation_curve_speed' => array(
								'type' => 'number',
								'title' => esc_html__( 'Animation Speed', 'cryptop' ),
								'placeholder' => esc_html__( 'In milliseconds', 'cryptop' ),
								'value' => '300',
								'addrowclasses' => 'grid-col-4',
							),
							'curves' => array(
								'type' => 'info',
								'addrowclasses' => 'grid-col-12',
								'value' => '<img src="'. get_template_directory_uri() . '/img/easing.png" />',
								'title' => esc_html__('Easing curves', 'cryptop' )
							),
					    )
				    ),
						'help' => array(
							'type' => 'tab',
							'icon' => array('fa', 'calendar-plus-o'),
							'title' => esc_html__( 'Help', 'cryptop' ),
							'layout' => array(
								'help' => array(
										 'title' 			=> esc_html__( 'Help', 'cryptop' ),
										 'type' 			=> 'info',
										 'subtype'		=> 'custom',
										 'value' 			=> '<a class="cwsfw_info_button" href="http://cryptop.creaws.com/manual" target="_blank"><i class="fa fa-life-ring"></i>&nbsp;&nbsp;' . esc_html__( 'Online Tutorial', 'cryptop' ) . '</a>&nbsp;&nbsp;<a class="cwsfw_info_button" href="https://www.youtube.com/user/cwsvideotuts/playlists" target="_blank"><i class="fa fa-video-camera"></i>&nbsp;&nbsp;' . esc_html__( 'Video Tutorial', 'cryptop' ) . '</a>',
									),
							)
						),
						'crop' => array(
							'type' => 'tab',
							'icon' => array('fa', 'calendar-plus-o'),
							'title' => esc_html__( 'Crop Images', 'cryptop' ),
							'layout' => array(
								'crop_x' => array(
									'title' => esc_html__( 'Crop X', 'cryptop' ),
									'type' => 'radio',
									'addrowclasses' => 'grid-col-3',
									'value' => array(
										'left' => array( esc_html__( 'Left', 'cryptop' ),  false, '' ),
										'center' => array( esc_html__( 'Center', 'cryptop' ),  true, '' ),
										'right' => array( esc_html__( 'Right', 'cryptop' ),  false, '' ),
									),
								),
								'crop_y' => array(
									'title' => esc_html__( 'Crop Y', 'cryptop' ),
									'type' => 'radio',
									'addrowclasses' => 'grid-col-3',
									'value' => array(
										'top' => array( esc_html__( 'Top', 'cryptop' ),  false, '' ),
										'center' => array( esc_html__( 'Center', 'cryptop' ),  true, '' ),
										'bottom' => array( esc_html__( 'Bottom', 'cryptop' ),  false, '' ),
									),
								),

							)
						),
					)
				),
				
				'social_options' => array(
					'type' => 'section',
					'title' => esc_html__('Social Networks', 'cryptop' ),
					'icon' => array('fa', 'share-alt'),
					'layout' => array(
						'social_cont'	=> array(
							'type' => 'tab',
							'init'	=> 'open',
							'icon' => array('fa', 'arrow-circle-o-up'),
							'title' => esc_html__( 'Social Networks', 'cryptop' ),
							'layout' => array(

								'social' => array(
									'type' => 'fields',
									'addrowclasses' => 'inside-box groups grid-col-12 box',
									'layout' => array(
										'location'	=> array(
											'title'		=> esc_html__( 'Social Icons Location', 'cryptop' ),
											'type'	=> 'select',
											'atts' => 'multiple',
											'addrowclasses' => 'grid-col-12 box',
											'source'	=> array(
												'top_bar' => array( esc_html__( 'Top Bar', 'cryptop' ), false, ''),
												'menu' => array( esc_html__( 'Menu', 'cryptop' ), false, ''),
												'copyrights' => array( esc_html__( 'Copyrights area', 'cryptop' ), false, ''),
												'side_panel' => array( esc_html__( 'Side panel', 'cryptop' ), false, ''),
											),
										),
										'icons' => array(
											'type' => 'group',
											'addrowclasses' => 'group sortable grid-col-12 box',
											'title' => esc_html__('Social Networks', 'cryptop' ),
											'button_title' => esc_html__('Add new social network', 'cryptop' ),
											'button_icon' => 'fa fa-plus',
											'layout' => array(
												'title' => array(
													'type' => 'text',
													'atts' => 'data-role="title"',
													'addrowclasses' => 'grid-col-4',
													'title' => esc_html__('Social account title', 'cryptop' ),
												),
												'icon' => array(
													'type' => 'select',
													'addrowclasses' => 'fai grid-col-4',
													'source' => 'fa',
													'title' => esc_html__('Icon for this social contact', 'cryptop' )
												),										
												'url' => array(
													'type' => 'text',
													'addrowclasses' => 'grid-col-4',
													'title' => esc_html__('Url to your account', 'cryptop' ),
												),
												'color'	=> array(
													'title'	=> esc_html__( 'Icon color', 'cryptop' ),
													'addrowclasses' => 'grid-col-3',
													'atts' => 'data-default-color="#ffffff"',
													'value' => '#ffffff',
													'type'	=> 'text',
												),
												'bg_color'	=> array(
													'title'	=> esc_html__( 'Background color', 'cryptop' ),
													'addrowclasses' => 'grid-col-3',
													'atts' => 'data-default-color="' . CRYPTOP_HELPER_COLOR . '"',
													'value' => CRYPTOP_HELPER_COLOR,
													'type'	=> 'text',
												),			
												'hover_color'	=> array(
													'title'	=> esc_html__( 'Icon color (Hover)', 'cryptop' ),
													'addrowclasses' => 'grid-col-3',
													'atts' => 'data-default-color="#ffffff"',
													'value' => '#ffffff',											
													'type'	=> 'text',
												),
												'hover_bg_color'	=> array(
													'title'	=> esc_html__( 'Background color (Hover)', 'cryptop' ),
													'addrowclasses' => 'grid-col-3',
													'atts' => 'data-default-color="' . CRYPTOP_SECONDARY_COLOR . '"',
													'value' => CRYPTOP_SECONDARY_COLOR,
													'type'	=> 'text',
												),
											)
										),
									),
								),

							)
						),
					)
				), // end of sections
			);

		return $settings;
	}
}
















































?>