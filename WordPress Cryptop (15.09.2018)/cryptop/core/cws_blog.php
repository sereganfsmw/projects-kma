<?php

//Get defaults values for Blog
function cws_blog_defaults( $extra_vars = array() ){
	global $cws_theme_funcs;

	$def_blogtype = $cws_theme_funcs && isset($cws_theme_funcs->cws_get_option( 'blog_options' )['def_blogtype']) ? $cws_theme_funcs->cws_get_option( 'blog_options' )['def_blogtype'] : '1';
	$def_chars_count = $cws_theme_funcs && isset($cws_theme_funcs->cws_get_option( 'blog_options' )['def_blog_chars_count']) ? $cws_theme_funcs->cws_get_option( 'blog_options' )['def_blog_chars_count'] : '200';
	$def_chars_count = isset( $def_chars_count ) && is_numeric( $def_chars_count ) ? $def_chars_count : '200';

	$defaults = array(
		'display_style'				=> 'grid',
		'title'						=> 'Blog',
		'title_aligning'			=> 'left',
		'use_carousel'				=> '',
		'layout'					=> $def_blogtype,
		'full_width'				=> '',
		'checkerboard_aligning'		=> 'top',
		'elements_order'			=> array(
										array(
											'title' => esc_html__( 'Title', 'cryptop' ),
											'field' => esc_html__( 'title', 'cryptop' ),
										),
										array(
											'title' => esc_html__( 'Featured image', 'cryptop' ),
											'field' => esc_html__( 'featured_image', 'cryptop' ),
										),
										array(
											'title' => esc_html__( 'Meta', 'cryptop' ),
											'field' => esc_html__( 'meta', 'cryptop' ),
										),
										array(
											'title' => esc_html__( 'Content', 'cryptop' ),
											'field' => esc_html__( 'content', 'cryptop' ),
										),
										array(
											'title' => esc_html__( 'Read More', 'cryptop' ),
											'field' => esc_html__( 'read_more', 'cryptop' ),
										),																				
									),
		'columns_count'				=> '1',
		'slides_in_columns'			=> '1',
		'slides_view'				=> 'column',
		'slides_to_scroll'			=> '1',
		'slider_direction'			=> 'horizontal',
		'auto_height'				=> '',
		'navigation'				=> 'both',
		'pagination_style'			=> 'bullets',
		'dynamic_bullets'			=> '',
		'keyboard_control'			=> '',
		'mousewheel_control'		=> '',
		'item_center'				=> '',
		'spacing_slides'			=> '',
		'free_mode'					=> '',
		'slide_effects'				=> 'slide',
		'autoplay'					=> '',
		'autoplay_speed'			=> '',
		'pause_on_hover'			=> 'yes',
		'parallax'					=> '',
		'loop'						=> '',
		'animation_speed'			=> '',

		'filter_by'					=> '',
		'category'					=> '',
		'post_tag'					=> '',
		'post_format'				=> '',

		'items_count'				=> PHP_INT_MAX,
		'items_per_page'			=> get_option( 'posts_per_page' ),
		'pagination_grid'			=> 'standard',
		'pagination_aligning'		=> 'center',
		'chars_count'				=> $def_chars_count,
		'hide_meta'					=> '',
		'read_more_title'			=> 'Read More',
		'grid_columns_spacings'		=> array(
										'unit'=> 'px',
										'size' => 50,
									),
		'grid_rows_spacings'		=> array(
										'unit' => 'px',
										'size' => 50,
									),
		'crop_featured'				=> '',
		'date_container'			=> 'outside',
		'appear_style'				=> 'none',
		'link_show'					=> 'color',
		'hover_animation'			=> 'none',
		'read_more_aligning'		=> 'right',
		'read_more_hover_effect'	=> 'style_1',
		'title_color'				=> '#000',
		'title_typography'			=> '',

		//Service params
		'extra_query_args'			=> array(),
		'related_items'				=> '',
		'related_carousel'			=> '',
	);

	if (!empty($extra_vars)){
		$defaults = array_merge($defaults, $extra_vars);
	}

	return $defaults;
}

//Fill atts from function to function
function cws_blog_fill_atts( $atts = array() ){
	global $cws_theme_funcs;

	extract( $atts );

	$page_id = get_the_id();
	$page_meta = get_post_meta( $page_id, 'cws_mb_post' );
	$page_meta = isset( $page_meta[0] ) ? $page_meta[0] : array();

	$items_count = !empty( $items_count ) ? (int)$items_count : PHP_INT_MAX;

	$def_post_layout = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( 'blog_options' )['def_blogtype'] : '1';
	$def_post_layout = isset( $def_post_layout ) ? $def_post_layout : "";
	$layout = $layout == "def" ? $def_post_layout : $layout; 

	$hover_animation = !empty($hover_animation) && $hover_animation != 'none' ? " hover".$hover_animation : " hover1";

	$sb = $cws_theme_funcs ? $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() ) : '';
	$sb_layout = isset( $sb['layout_class'] ) ? $sb['layout_class'] : '';	

	if ($related_items){
		$def_hide_meta_related_items = $cws_theme_funcs && isset($cws_theme_funcs->cws_get_option( 'blog_options' )['def_hide_meta_related_items']) ? $cws_theme_funcs->cws_get_option( 'blog_options' )['def_hide_meta_related_items'] : array();
		if (!empty($def_hide_meta_related_items)){
			$hide_meta = $def_hide_meta_related_items;
		}	
	}

	$fill_atts = array_merge($atts,
		array(
			//Service params
			'page_id'			=> $page_id,
			'page_meta'			=> $page_meta,
			'sb_layout'			=> $sb_layout,
			'display_style'		=> ($atts['use_carousel'] == 'yes' ? 'carousel' : 'grid'),

			//Override atts
			'items_count'		=> $items_count,
			'hover_animation'	=> $hover_animation,
			'hide_meta'			=> $hide_meta,
			'layout'			=> $layout,
		)
	);

	//Set GLOBALS vars
	$GLOBALS['cws_blog_atts'] = $fill_atts;

	return $fill_atts;
}

function cws_blog_output ( $atts = array(), $content = "" ){
	global $cws_theme_funcs;
	$out = "";

	$extra_vars = array();
	if (!empty($atts['filter_by'])){
		$extra_vars[$atts['filter_by']] = '';
	}

	$defaults = cws_blog_defaults($extra_vars); //Get defaults
	$atts = shortcode_atts( $defaults, $atts );
	$fill_atts = cws_blog_fill_atts($atts); //Fill atts
	extract( $fill_atts );

	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();
	$single = cws_is_single($page_id);

	if ($single){
		wp_enqueue_script ('owl_carousel');
		wp_enqueue_script( 'imagesloaded' );
		wp_enqueue_script( 'fancybox' );
	}

	$post_type = "post";
	$section_id = uniqid( 'cws_blog_grid_' );
	$paged = get_query_var( 'paged' );
	$home_paged = get_query_var('page');
	if (isset($home_paged) && !empty($home_paged)){
		$paged = empty( $home_paged ) ? 1 : get_query_var('page');
	} else {
		$paged = empty( $paged ) ? 1 : $paged;
	}

	$not_in = (1 == $paged) ? array() : get_option( 'sticky_posts' );
	$query_args = array(
		'post_type'			=> array( $post_type ),
		'post_status'		=> 'publish',
		'post__not_in'		=> $not_in
	);
	if ( in_array( $display_style, array( 'grid' ) ) ){
		$query_args['posts_per_page']	= $items_per_page;
		$query_args['paged']			= $paged;
	} else {
		$query_args['nopaging']			= true;
		$query_args['posts_per_page']	= -1;
	}

	//Filter by
	$tax = $fill_atts['filter_by'];
	if (!empty($tax)){
		$terms = $fill_atts[$tax];
	}

	if ( !empty( $terms ) ){
		$query_args['tax_query'] = array(
			array(
				'taxonomy'		=> $tax,
				'field'			=> 'slug',
				'terms'			=> $terms
			)
		);
	}

	$query_args = array_merge( $query_args, $extra_query_args );

	$q = new WP_Query( $query_args );
	$found_posts = $q->found_posts;
	$requested_posts = $found_posts > $items_count ? $items_count : $found_posts;
	$max_paged = $found_posts > $items_count ? ceil( $items_count / $items_per_page ) : ceil( $found_posts / $items_per_page );
	$cols = in_array( $layout, array( 'medium', 'small', 'checkerboard', 'fw_img' ) ) ? 1 : (int)$layout;
	$is_carousel = $display_style == 'carousel' && $requested_posts > $cols;
	$use_pagination = in_array( $display_style, array( 'grid' ) ) && $max_paged > 1;

	//CAROUSEL
	$data_attr = swiper_carousel($fill_atts, $is_carousel);
	//--CAROUSEL

	$section_class = cws_class([
		'news',
		'cws_blog',
		'posts_grid',
		(is_numeric( $layout ) ? 'col-'.esc_attr($layout) : esc_attr($layout)),
		esc_attr($display_style),
		($layout == 'checkerboard') ? 'content_'.esc_attr($content_align) : '',
		($appear_style !== 'none' ? "appear_style" : ""),
		$hover_animation
	]);

	$wrapper_class = cws_class([
		'grid',
		($is_carousel || $related_carousel) ? 'cws_blog_carousel' : 'cws_blog_grid',
		(($is_carousel || $related_carousel) && $use_pagination && $pagination_grid != 'standard') ? '' : 'cws_pagination',
		(isset($related_carousel) && $related_carousel) ? 'related_blog_carousel cws_owl_carousel' : '',
		(is_numeric( $layout ) && !$is_carousel) ? 'isotope' : '',
		(!empty($layout)) ? 'layout-'.esc_attr($layout) : 'layout-def'
	]);

	ob_start ();
	echo "<section id='".esc_attr($section_id)."' class='".(!empty($section_class) ? $section_class : "")."'>";
		if ( isset($related_carousel) && $related_carousel ){
			echo "<div class='widget_header clearfix'>";
				echo !empty( $title ) ? "<h2 class='widgettitle'>" . esc_html( $title ) . "</h2>" : "";
			echo "</div>";
		}
		else{
			echo !empty( $title ) ? "<h2 class='widgettitle'>" . esc_html( $title ) . "</h2>" : "";
		}

		echo "<div class='cws_blog_wrapper'>";
			echo "<div ".(!empty($wrapper_class) ? "class='".$wrapper_class."'" : "").(!empty($data_attr) ? $data_attr : "").">";
				if ($is_carousel){
					echo "<div class='swiper-container'>";
						echo "<div class='swiper-wrapper'>";
				}

				cws_blog_posts($q);

				if ($is_carousel){
						echo "</div>"; //swiper-wrapper

						if ($navigation == 'both' || $navigation == 'pagintaion'){
							if ($pagination_style == 'scrollbar'){
								echo "<div class='swiper-scrollbar'></div>";
							} else {
								echo "<div class='swiper-pagination'></div>";
							}
						}

					echo "</div>"; //swiper-container

					if ($navigation == 'both' || $navigation == 'arrows'){
						echo "<div class='swiper-button-prev'></div>";
						echo "<div class='swiper-button-next'></div>";
					}
				}				

				unset( $GLOBALS['cws_blog_atts'] );
				unset( $GLOBALS['display_post'] );
			echo "</div>";
			if ( $use_pagination && $cws_theme_funcs){
				cws_loader_html();
			}
		echo "</div>";

		if ( isset($related_carousel) && $related_carousel ){
			echo "<div class='carousel_nav'>";
				echo "<span class='prev'>";
					if(!empty($related_items)){
						esc_html_e("Previous", 'cryptop');
					}
				echo "</span>";
				echo "<span class='next'>";
					if(!empty($related_items)){
						esc_html_e("Next", 'cryptop');
					}
				echo "</span>";
			echo "</div>";
		}

		if ($cws_theme_funcs){
			if ((!$single && $use_pagination) || (is_archive() && !is_single())){
				if ( $pagination_grid == 'load_more' ){					
					echo cws_load_more ();
				}
				else if($pagination_grid == 'load_on_scroll'){					
					echo cws_load_more (true);
				}				
				else if($pagination_grid == 'ajax'){					
					echo cws_pagination($paged, $max_paged, true);
				}
				else if($pagination_grid == 'standard'){					
					echo cws_pagination($paged, $max_paged, false);
				}

				//AJAX data
				$ajax_data = array_merge($fill_atts,
					array(
						//Service params
						'post_type'			=> 'post',
						'section_id'		=> $section_id,
						'max_paged'			=> $max_paged,
						'page'				=> $paged,
					)
				);

				$ajax_data_str = json_encode( $ajax_data );
				echo "<form id='".esc_attr($section_id)."_data' class='ajax_data_form cws_blog_ajax_data_form'>";
					echo "<input type='hidden' id='".esc_attr($section_id)."_ajax_data' class='ajax_data cws_blog_ajax_data' name='".esc_attr($section_id)."_ajax_data' value='".$ajax_data_str."' />";
				echo "</form>";
			}

		} else {
			echo cws_pagination($paged, $max_paged, false);
		}
		
	echo "</section>";
	$out = ob_get_clean();

	return $out;
}

function cws_blog_special_post_formats (){
	return array( "status" );
}
function cws_is_special_post_format (){
	global $post;
	$sp_post_formats = cws_blog_special_post_formats ();
	if ( isset($post) ){
		return in_array( get_post_format(), $sp_post_formats );
	}
	else{
		return false;
	}
}
function cws_blog_post_format (){
	global $post;
	if ( isset( $post ) ){
		$pf = get_post_format ();
		$icon = "book";
		switch ( $pf ){
			case "aside":
				$icon = "bullseye";
				break;
			case "gallery":
				$icon = "bullseye";
				break;
			case "link":
				$icon = "chain";
				break;
			case "image":
				$icon = "image";
				break;
			case "quote":
				$icon = "quote-left";
				break;
			case "status":
				$icon = "flag";
				break;
			case "video":
				$icon = "video-camera";
				break;
			case "audio":
				$icon = "music";
				break;
			case "chat":
				$icon = "wechat";
				break;
		}
		$out = "<i class='custom-hash'>#</i> $pf";
		return $out;
	}
	else{
		return "";
	}
}

function cws_blog_thumbnail_dims ( $eq_thumb_height = false, $real_dims = array(), $post_format = null ) {
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	//Atts
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_post_atts'] ) ? $GLOBALS['cws_single_post_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	} else {
		extract( $grid_atts );
	}
	$sb_layout = !empty($sb_layout) ? str_replace("_sidebar","", $sb_layout) : $sb_layout;

	$dims = array( 'width' => 0, 'height' => 0 );
	if ( $single && empty($related_items)){
		if ( empty( $sb_layout ) ){
			if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 1170 ) ) || $eq_thumb_height ){
				$dims['width'] = 1170;
				if ( $eq_thumb_height ) $dims['height'] = 659;
			}
		}
		else if ( $sb_layout === "single" ){
			if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 870 ) ) || $eq_thumb_height ){
				$dims['width'] = 870;				
				if ( $eq_thumb_height ) $dims['height'] = 490;
			}
		}
		else if ( $sb_layout === "double" ){
			if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 570 ) ) || $eq_thumb_height ){
				$dims['width'] = 570;
				if ( $eq_thumb_height ) $dims['height'] = 321;
			}
		}
	} else if ($full_width){
		switch ($layout){
			case "1":	
				$dims['width'] = 1920;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 1080;
				}		
				break;
			case '2':
				$dims['width'] = 1000;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 208;
				}		
				break;
			case '3':
				$dims['width'] = 750;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 208;
				}		
				break;
			case '4':
				$dims['width'] = 500;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 152;
				}
				break;
		}
	} 
	else{
		switch ($layout){
			case "1":
				if ( empty( $sb_layout ) ){
					$dims['width'] = 1170;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 659;
					}	
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 870;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 490;
					}	
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 570;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 321;
					}	
				}
				break;			
			case "large":
				if ( empty( $sb_layout ) ){
					$dims['width'] = 1170;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 659;
					}	
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 870;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 490;
					}	
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 570;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 321;
					}	
				}
				break;
			case "fw_img":
				$dims['width'] = 570;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 290;
				}	
				break;
			case "checkerboard":
				$dims['width'] = 585;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 290;
				}	
				break;	
			case "medium":
				$dims['width'] = 570;
				if ( !isset( $real_dims['height'] ) ){
					$dims['height'] = 321;
				}	
				break;
			case "small":		
				$dims['width'] = 420;
				$dims['height'] = 420;
				break;
			case '2':
				if ( empty( $sb_layout ) ){	
					$dims['width'] = 570;
					$dims['height'] = 351;
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 570;
					$dims['height'] = 351;	
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 270;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 152;
					}	
				}
				break;
			case '3': 

				if ( empty( $sb_layout ) ){
					$dims['width'] = 370;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 208;
					}else{
						$dims['height'] = 370;
					}	
				}
				else if ( $sb_layout === "single" ){
					$dims['width'] = 270;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 152;
					}
					else{
						$dims['height'] = 270;
					}	
				}
				else if ( $sb_layout === "double" ){
					$dims['width'] = 270;
					if ( !isset( $real_dims['height'] ) ){
						$dims['height'] = 152;
					}	
					else{
						$dims['height'] = 270;
					}
				}
			
				break;
			case '4':
				$dims['width'] = 270;
				$dims['height'] = 270;
				break;
		}
	}
	return $dims;
}

function cws_blog_posts ( $q = null ){
	if ( !isset( $q ) ) return;
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$paged = $q->query_vars['paged'];
	if ( $paged == 0 && $items_count < $q->post_count ){
		$post_count = $items_count;
	} else {
		$ppp = $q->query_vars['posts_per_page'];
		$posts_left = $items_count - ( $paged - 1 ) * $ppp;
		$post_count = $posts_left < $ppp ? $posts_left : $q->post_count;
	}

	if ( $q->have_posts() ):
		ob_start();
		while( $q->have_posts() && $q->current_post < $post_count - 1 ):
			$q->the_post();
			cws_blog_article ();
		endwhile;
		wp_reset_postdata();
		ob_end_flush();
	endif;				
}

function cws_blog_styles ($uniq_pid){
	$post_id = get_the_id();
	$post_meta = get_post_meta( $post_id, 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();
	$comments_n = get_comments_number();
	$image_from_post = isset($post_meta['post_title_box_image']) ? $post_meta['post_title_box_image'] : '';
	$post_title_color = isset($post_meta['post_title_color']) ? $post_meta['post_title_color'] : '';
	$post_font_meta_color = isset($post_meta['post_font_meta_color']) ? $post_meta['post_font_meta_color'] : '';
	$post_font_color = isset($post_meta['post_font_color']) ? $post_meta['post_font_color'] : '';
	$post_font_sec_color = isset($post_meta['post_font_sec_color']) ? $post_meta['post_font_sec_color'] : '';
	$custom_title_overlay = isset($post_meta['custom_title_overlay']) ? $post_meta['custom_title_overlay'] : '';
	$post_custom_color = isset($post_meta['post_custom_color']) ? $post_meta['post_custom_color'] : '';
	$apply_color = isset($post_meta['apply_color']) ? $post_meta['apply_color'] : '';
	$apply_bg_color = isset($post_meta['apply_bg_color']) ? $post_meta['apply_bg_color'] : '';
	$title_overlay = isset($post_meta['title_overlay']) ? $post_meta['title_overlay'] : '';
	$title_bg_opacity = isset($post_meta['title_bg_opacity']) ? $post_meta['title_bg_opacity'] : '';
	$title_bg_opacity = $title_bg_opacity !== "" ? strval( (int)$title_bg_opacity / 100 ) : "";
	/* styles */
	ob_start();
	if ( $post_custom_color && ( $apply_color == 'list_color' || $apply_color == 'both_color' ) && empty($related_items) ) {
		echo "#{$uniq_pid} .post_title a,
			#{$uniq_pid} .post_title,
			#{$uniq_pid} .post_title a:hover,
			#{$uniq_pid} .post_info .info:hover, 
			#{$uniq_pid} .sl-icon:hover:before, 
			#{$uniq_pid} .info span.post_author a:hover, 
			#{$uniq_pid} .comments_link i:hover, 
			#{$uniq_pid} .comments_link:hover, 
			#{$uniq_pid} .sl-count:hover, 
			#{$uniq_pid} .like:hover, 
			#{$uniq_pid} .post_info > .post_meta .social_share a:hover, 
			#{$uniq_pid} .post_info .info:hover:before{
				color: ".esc_attr($post_title_color).";
			}

			#{$uniq_pid} .post_info .info, 
			#{$uniq_pid} .sl-icon:before, 
			#{$uniq_pid} .info span.post_author a, 
			#{$uniq_pid} .comments_link i, 
			#{$uniq_pid} .comments_link, 
			#{$uniq_pid} .sl-count, 
			#{$uniq_pid} .like, 
			#{$uniq_pid} .post_info > .post_meta .social_share a, 
			#{$uniq_pid} .post_info .info:before{
				color: ".cws_Hex2RGBA($post_title_color,.6).";
			}

			#{$uniq_pid} .post_media .hover-effect{
				background: ".cws_Hex2RGBA($post_title_color,.8).";
			}

			#{$uniq_pid} .post_info > hr{
				background: ".cws_Hex2RGBA($post_title_color,.2).";
			}

			#{$uniq_pid} div.post_tags a,
			#{$uniq_pid} .btn-read-more a,
			#{$uniq_pid} div.post_category a{
				".(!empty($post_font_meta_color) ? "background: ".cws_Hex2RGBA($post_font_meta_color,.6).";" : "background: ".cws_Hex2RGBA($post_title_color,.6).";")."
				
			}

			#{$uniq_pid} div.post_tags a:hover,
			#{$uniq_pid} div.post_category a:hover{
				".(!empty($post_font_meta_color) ? "background: ".esc_attr($post_font_meta_color).";" : "background: ".esc_attr($post_title_color).";")."
			}

			#{$uniq_pid} .btn-read-more a:hover{
				".(!empty($post_font_meta_color) ? "color: ".esc_attr($post_font_meta_color).";" : "color: ".esc_attr($post_title_color).";")."
			}

			#{$uniq_pid} .btn-read-more a.more-link:hover{
				background: transparent;
			}

			#{$uniq_pid} .btn-read-more a:before{
				border-color: ".esc_attr($post_title_color).";
			}

			#{$uniq_pid} .post_content{
				color: ".esc_attr($post_font_color).";
			}

			#{$uniq_pid} .date-content{
				background-color: ".esc_attr($post_font_sec_color).";
			}
			";
	}
	if ( $post_custom_color && ( $apply_color == 'single_color' ) && empty($related_items) ) {
		echo ".page_title_content #page_title{
				color: ".esc_attr($post_title_color).";
			}
			.page_title_content .bread-crumbs{
				color: ".esc_attr($post_font_color).";
			}";
	}
	if ($custom_title_overlay == 1 && ( $apply_bg_color == 'list_color' || $apply_bg_color == 'both_color' ) && empty($related_items) ) {
		echo "#{$uniq_pid}:before{
			background-color: ".esc_attr($title_overlay).";
			opacity: ".esc_attr($title_bg_opacity).";
		}";
	}
	/* \styles */
	$styles = ob_get_clean();

	if ( !empty( $styles ) ){
		Cws_shortcode_css()->enqueue_cws_css($styles);
	}	
}

function cws_blog_article (){
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$uniq_pid = uniqid( "blog_post_" );
	$is_carousel = $display_style == 'carousel';

	cws_blog_styles($uniq_pid);	//Blog showcase

	$data_anim = ($appear_style !== 'none') ? "data-item-anim='".esc_attr($appear_style)."'" : ""; 
	echo "<article id='".esc_attr($uniq_pid)."' ".$data_anim." ";
	post_class(
		array( 'item', 'blog_item', 'post', (is_sticky(get_the_id()) ? 'sticky-post': ''), ($is_carousel ? 'swiper-slide' : ''),
			(
			!((!in_array( 'cats', $hide_meta ) && has_category()) || (!in_array( 'tags', $hide_meta ) && has_tag())) ? 'no_tax' : ''
			)
		)
	);
	echo ">";
		if (!empty($image_from_post) && $layout == 'fw_img') {
			$back_img_src = $image_from_post['src'];
			echo "<div class='back_img' style='background-image: url(".esc_url($back_img_src).");'></div>";
		}
		echo "<div class='post_wrapper clearfix'>";
			$post_bottom_wrapper =
				cws_blog_meta() .
				cws_blog_taxanomy()
			;

			$output = '';
			$count = count($elements_order);
			$featured_image_middle = false;
			foreach ($elements_order as $key => $value) {
				if($key == 0 && $value['field'] != 'featured_image'){
					$output .= "<div class='post_info'>";
				}

				if ($key == 0 && $value['field'] == 'featured_image'){
					$output .= cws_blog_media($uniq_pid);
					$output .= "<div class='post_info'>";

				} else if ($value['field'] == 'featured_image' && ($key != 0 && $key != $count-1)){
					$output .= "</div>";
					$output .= cws_blog_media($uniq_pid);
					$output .= "<div class='post_info'>";
					$featured_image_middle = true;
				} else if ($value['field'] == 'featured_image' && $key == $count-1){
					$output .= "</div>";
					$output .= cws_blog_media($uniq_pid);
					$output .= cws_page_links();
					$output .= cws_blog_social();								
				}

					if ($value['field'] == 'data'){
						$output .= cws_blog_date();
					}

					if ($value['field'] == 'title'){
						$output .= cws_blog_title();
					}

					if ($value['field'] == 'content'){
						$output .= cws_blog_content();
					}

					if ($value['field'] == 'meta'){
						if ( !empty( $post_bottom_wrapper ) ){
							$output .= "<div class='post_bottom_wrapper meta-bottom'>";
								$output .= $post_bottom_wrapper;
							$output .= "</div>";
						}
					}

					if ($value['field'] == 'read_more'){
						$output .= cws_blog_btn_more();
					}		
					


				if ($featured_image_middle && $key == $count-1){
					$output .= cws_page_links();
					$output .= cws_blog_social();
					$output .= "</div>";
				}
			}
			echo sprintf('%s', $output);
		echo "</div>";
	echo "</article>";
}

function cws_blog_chars_count() {
	global $cws_theme_funcs;
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();	
	extract( $grid_atts );

	$number 	= NULL;
	$pid 		= get_queried_object_id();
	$sb 		= $cws_theme_funcs ? $cws_theme_funcs->cws_get_sidebars($pid) : "";
	$sb_layout 	= isset( $sb['layout_class'] ) ? $sb['layout_class'] : '';
	switch ( $layout ) {
		case '1':
		case 'medium':
		case 'small':
			switch ( $sb_layout ) {
				case 'double':
					$number = NULL;
					break;
				case 'single':
					$number = NULL;
					break;
				default:
					$number = NULL;
			}
			break;
		case '2':
			switch ( $sb_layout ) {
				case 'double':
					$number = 55;
					break;
				case 'single':
					$number = 90;
					break;
				default:
					$number = 130;
			}
			break;
		case '3':
			switch ( $sb_layout ) {
				case 'double':
					$number = 60;
					break;
				case 'single':
					$number = 60;
					break;
				default:
					$number = 70;
			}
			break;
	}
	return $number;
}

// ===========================BLOG PARTS===========================
function cws_blog_title(){
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$out = '';
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$title = get_the_title();
	$permalink = get_the_permalink();
	$is_special_post_format = cws_is_special_post_format();

	$single = cws_is_single($page_id);
	if ($single){
		$title_part = $title;
	} else {
		$title_part = "<h3 class='post_title'><a href='".esc_url($permalink)."'>". $title ."</a></h3>";
	}

	if ( !in_array( 'title', $hide_meta )){
		$out.= !$is_special_post_format && !empty( $title_part ) ? '<div>' . $title_part . '</div>' : "";
	}
	return $out;
}

function cws_blog_date(){
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();

	$single = cws_is_single( $page_id );

	$out = '';
	if ($date_container == 'outside' && !in_array( 'date', $hide_meta ) ){
		$date = get_the_time( get_option("date_format") );
		if ( !empty( $date ) ){
			$out.= "<div class='inline_date'>";
				$out.= $date;
			$out.= "</div>";
		}
	}
	return $out;
}

function cws_blog_media (){
	global $cws_theme_funcs;
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );
	
	$out = '';
	$single = cws_is_single( $page_id );
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$post_url = get_the_permalink();
	$post_format = get_post_format( );
	$eq_thumb_height = in_array( $post_format, array( 'gallery' ) );
	$media_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$media_meta = isset( $media_meta[0] ) ? $media_meta[0] : array();

	$thumbnail_props = has_post_thumbnail( ) ? wp_get_attachment_image_src(get_post_thumbnail_id( ),'full') : array();
	$thumbnail = !empty( $thumbnail_props ) ? $thumbnail_props[0] : '';
	$real_thumbnail_dims = array();
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[1] ) ) $real_thumbnail_dims['width'] = $thumbnail_props[1];
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[2] ) ) $real_thumbnail_dims['height'] = $thumbnail_props[2];
	$thumbnail_dims = cws_blog_thumbnail_dims( $eq_thumb_height, $real_thumbnail_dims, $post_format );

	$full_width_featured = isset($media_meta['full_width_featured']) && $media_meta['full_width_featured'] == '1';

	if ($single && $full_width_featured){
		$thumbnail_dims = array( 'width' => 0, 'height' => 0 );
	}

	if (($single && isset($related_items) && $related_items && $cws_theme_funcs->cws_get_option( 'blog_options' )['crop_related_items'] == '1') || $crop_featured == 'yes'){
		$thumbnail_dims['height'] = $thumbnail_dims['width'];
		$thumbnail_dims['crop'] = array(
			$cws_theme_funcs->cws_get_option( "crop_x" ),
			$cws_theme_funcs->cws_get_option( "crop_y" )
		);
	}

	$full_width = isset($GLOBALS['cws_row_atts']) && !empty($GLOBALS['cws_row_atts']) ? $GLOBALS['cws_row_atts'] : "";

	$buf1 = "";
	$thumb_media = false;
	$allow_cut_media = false;
	$some_media = false;

	ob_start(); //Media Content
	switch ($post_format) {
		case 'link':
			$link = isset( $media_meta['link'] ) ? esc_url( $media_meta['link'] ) : "";
			
			$link_title = isset( $media_meta['link_title'] ) ? esc_html( $media_meta['link_title'] ) : "";
			
			if ( !empty($thumbnail) ) {
				$thumb_obj = cws_thumb( get_post_thumbnail_id( ), $thumbnail_dims, false );
				$thumb_url = isset( $thumb_obj[0] ) ? $thumb_obj[0] : "";
				$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;
				?>
				<div class="pic <?php echo !empty( $link ) ? 'link_post' : ''; ?>">
					<?php				
						if ($single){
							if ( $retina_thumb ) {
								echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
							}
							else{
								echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
							}						
						} else {						
							echo "<span class='link_post_src' style='background-image:url(".esc_url($thumb_url).");'></span>";
						}
					?>
					<div class="hover-effect"></div>
					<?php
					if ( !empty( $link ) ){
						if ( !empty( $link_title ) ){
							echo "<span class='post_media_link_title overlay'>".esc_html($link_title)."</span>";
						}
						echo "<a class='post_media_link".($single ? 'single_media_link' : 'grid_media_link')."' href='".esc_url($link)."'></a>";
					}
					else {
						echo "<a class='fancy post_media_link".($single ? 'single_media_link' : 'grid_media_link')."' href='".esc_url($thumbnail)."'></a>";
					}
					?>
				</div>
				<?php
				$thumb_media = true;
			}
			else{
				if ( !empty( $link ) ) {
					if ( !empty( $link_title ) ){
						echo "<div class='pic'>";
							echo "<span class='post_media_link_title'>$link_title</span>";
							echo "<a class='post_media_link".($single ? 'single_media_link' : 'grid_media_link')."' href='$link'></a>";
						echo "</div>";
					}
				}
			}
			break;
		case 'video':
			$video = isset($media_meta[$post_format]) ? $media_meta[$post_format] : "";
			if ( !empty( $video ) ) {
				echo "<div class='video'>" . apply_filters('the_content',"[embed width='" . $thumbnail_dims['width'] . "']" . $video . "[/embed]") . "</div>";
			}
			break;
		case 'audio':
			$audio = isset($media_meta[$post_format]) ? esc_attr( $media_meta[$post_format]) : "";
			$is_soundcloud = is_int( strpos( (string) $audio, 'https://soundcloud' ) );
			if ( !empty( $thumbnail ) && !$is_soundcloud ){
				$thumb_obj = cws_thumb( get_post_thumbnail_id( ), $thumbnail_dims, false );
				$thumb_url = isset( $thumb_obj[0] ) ? esc_url( $thumb_obj[0] ) : "";
				$thumbnail = esc_url($thumbnail);
				$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;
				echo "<div class='pic'>";
					if ( $retina_thumb ) {
						echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
					}
					else{
						echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
					}
				echo "</div>";
				if ( empty( $audio ) ){
					echo "<div class='hover-effect'></div>";
					echo "<a class='fancy post_media_link".($single ? 'single_media_link' : 'grid_media_link')."' href='".esc_url($thumbnail)."'></a>";					
					$thumb_media = true;				
				}					
			}
			if ( !empty( $audio ) ){
				echo "<div class='audio" . ( $is_soundcloud ? " soundcloud" : "" ) . "'>";
					echo apply_filters( 'the_content', $audio );
				echo "</div>";
			}
			break;
		case 'quote':
			$quote = isset( $media_meta['quote_text'] ) ? $media_meta['quote_text'] : '';
			$author_name = isset( $media_meta['quote_author'] ) ? $media_meta['quote_author'] : '';				
			if ( !empty( $quote ) ) {
				echo "<div class='post_format_quote_media_wrapper'>";
					echo cws_vc_shortcode_quote_renderer( array(
						'thumbnail'			=> $thumbnail,
						'quote'				=> $quote,
						'author_name'		=> $author_name,
					));
				echo "</div>";
			}
			break;
		case 'gallery':
			$gallery = isset( $media_meta[$post_format] ) ? $media_meta[$post_format] : "";
			if ( !empty( $gallery ) ) {
				$match = preg_match_all("/\d+/",$gallery,$images);
				if ($match){
					$images = $images[0];
					$image_srcs = array();

					foreach ( $images as $image ) {
						$image_temp = array();
						$image_src = wp_get_attachment_image_src($image,'full');
						if ( $image_src ){
							$image_temp = array('url' => $image_src[0], 'id' => $image);
							array_push( $image_srcs, $image_temp );
						}
					}
					$carousel = count($image_srcs) > 1 ? true : false;
					$gallery_id = uniqid( 'cws-gallery-' );
					
					$carousel_atts = array(
						'nav'			=> true
					);
					$carousel_atts_str = json_encode($carousel_atts);
					$data_attr = " data-owl-args='".$carousel_atts_str."'";

					echo  $carousel ? "<div class='gallery_post_carousel_wrapper'>
										<a class='gallery_post_carousel_nav carousel_nav prev'><span></span></a>
										<a class='gallery_post_carousel_nav carousel_nav next'><span></span></a>
										<div class='gallery_post_carousel cws_owl_carousel' ".$data_attr.">" : '';
					// if ($carousel) wp_enqueue_script( 'owl_carousel' );
					foreach ( $image_srcs as $image_src ) {
						$img_obj = cws_thumb( $image_src['id'], $thumbnail_dims , false );
						$img_url = isset( $img_obj[0] ) ? esc_url( $img_obj[0] ) : "";
						$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;
						?>
						<div class='pic'>
							<?php
							if ( $retina_thumb ) {
								echo "<img src='".esc_url($img_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
							}
							else{
								echo "<img src='".esc_url($img_url)."' data-no-retina alt />";
							}
							echo "<div class='hover-effect'></div>";
							?>
							<?php
								echo !$carousel ? "<div class='hover-effect'></div>" : "";
								echo "<a href='".esc_url($image_src['url'])."'" . ( $carousel ? " data-fancybox-group='$gallery_id'" : "" ) . " class='fancy post_media_link".($single ? 'single_media_link' : 'grid_media_link') . ( $carousel ? " fancy_gallery" : "" ) . "'></a>";
							?>
						</div>
						<?php
					}
					echo  $carousel ? "</div></div>" : '';
				}
			}
			break;
	}
	$buf1 = ob_get_contents();
	/* Date */
	if ($date_container == 'inside' && !empty($thumbnail)){

		$meta_date_arr = array();
		if ( !in_array( 'date', $hide_meta ) ){
			array_push( $meta_date_arr, "<div class='date-content'>" );
			$date = get_the_time( get_option("date_format") );
			if ( !empty( $date ) ){
				$date = explode(" ", $date);
				foreach ($date as $key => $value) {
					array_push( $meta_date_arr, "<span class='date-c'>".$value."</span>" );
				}
			}
			array_push( $meta_date_arr, "</div>" );
		}
		$meta_date = !empty( $meta_date_arr ) ? implode( " ", $meta_date_arr ) : "";
		if ( !empty( $meta_date ) ){
			echo "<div class='meta_date'>";
				printf('%s', $meta_date);
			echo "</div>";		
		}
		
	}

	if ( empty( $buf1 ) && !empty( $thumbnail ) ) {
		$thumb_obj = cws_thumb( get_post_thumbnail_id( ), $thumbnail_dims, false );
		$thumb_url = isset( $thumb_obj[0] ) ? esc_url($thumb_obj[0]) : "";
		$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;

		echo "<div class='pic'>";
			if ( $retina_thumb ) {
				echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
			}
			else{
				echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
			}
			echo "<div class='hover-effect'></div>";

			if (!empty($media_meta['enable_lightbox']) && !empty($link_show) && $link_show == 'popup_link' ){
				echo "<a class='fancy post_media_link ".($single ? 'single_media_link' : 'grid_media_link')."' href='$thumbnail'></a>";
			}
			
			if($link_show == 'dots'){
				echo "<div class='dots_wrapper'><span></span></div>";
			}

			if($link_show == 'single_link'){
				echo "<a class='link_area_post_media post_media_link ".($single ? 'single_media_link' : 'grid_media_link')."' href='".esc_url($post_url)."'></a>";
			}
		echo "</div>";
		$thumb_media = true;
		$allow_cut_media = true;
	}
	$media_content = ob_get_clean(); //Media Content

	$some_media = !empty( $media_content );
	$floated_media = in_array( $layout, array( "medium", "small", 'checkerboard', 'fw_img' ) ) || ( $layout === "2" && empty( $sb_layout ) );	
	$cut_media = ( in_array( $layout, array( "small" ) ) || ( $layout === "2" && empty( $sb_layout ) ) ) && $allow_cut_media;
	$media_classes = array( "post_media", ($single ? 'single_post_media' : 'grid_post_media') );

	ob_start(); //Media Wrapper
	$media_classes_str = implode( " ", $media_classes );
	if ( !empty( $media_content ) ){
		if ( $floated_media ){
			?>
				<div class="floated_media">
					<?php
					printf('<div class="floated_media_wrapper %s %s">', ($single ? 'single_floated_media_wrapper' : 'grid_floated_media_wrapper'), ($cut_media ? "cut_post_post_media" : "") );
						echo "<div class='".esc_attr($media_classes_str)."'>";
							printf('%s', $media_content);
						echo "</div>";
					?>
					</div>
				</div>
			<?php
		}
		else{
			echo "<div class='".esc_attr($media_classes_str)."'>";
				printf('%s', $media_content);
			echo "</div>";			
		}
	}
	if ( $thumb_media ){
		wp_enqueue_script( 'fancybox' );
	}
	$out.= ob_get_clean(); //Media Wrapper

	return $out;
}

function cws_blog_content(){
	global $post, $more, $cws_theme_funcs;
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();
	
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$out = '';
	$permalink = get_the_permalink( get_the_id() );
	$more = 0;
	$is_rtl = is_rtl();
	$chars_count = isset($chars_count) && !empty( $chars_count ) ? $chars_count : cws_blog_chars_count();
	$content = $proc_content = $excerpt = $proc_excerpt = "";
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$author = get_the_author();
	$content = $post->post_content;
	$excerpt = $post->post_excerpt;

	$single = cws_is_single($page_id);

	ob_start();

		if ( $single && empty($grid_atts['related_items'])) {
			if(strpos( (string) $content, '<!--more-->' )){
				$proc_content .= $content;
			} else {
				$proc_content .= get_the_content();
			}
		} else {

			if ( !empty( $excerpt ) ){
				$proc_content = get_the_excerpt();
			}
			else if ( strpos( (string) $content, '<!--more-->' ) ){
				$proc_content = get_the_content( "" );
			}
			else if ( !empty( $content ) && !empty( $chars_count ) ){
				$proc_content = get_the_content( "" );
				$proc_content = trim( preg_replace( '/[\s]{2,}/u', ' ', strip_shortcodes( strip_tags( $proc_content ) ) ) );
				$chars_count = (int)$chars_count;
				$proc_content = mb_substr( $proc_content, 0, $chars_count );
			}
			else {
				$proc_content = get_the_content( "[...]" );		
			}
		}

		if ($cws_theme_funcs){
			print apply_filters( 'the_content', $proc_content );
		} else {
			the_content();				
		}

	$out_content = ob_get_clean();


	if(!in_array( 'excerpt', $hide_meta )){
		$out.= "<div class='post_content clearfix'>";	
			$out.= $out_content;
		$out.= "</div>";
	}

	return $out;
}

function cws_blog_btn_more(){
	global $post;
	global $more;

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();	
	extract( $grid_atts );

	$out = '';
	$more = 0;
	$permalink = get_the_permalink( get_the_id() );		
	$chars_count = isset($chars_count) && !empty( $chars_count ) ? $chars_count : cws_blog_chars_count();
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$content = $proc_content = $excerpt = $proc_excerpt = "";
	$read_more = esc_html($read_more_title);
	$content = $post->post_content;
	$excerpt = $post->post_excerpt;
	$read_more_exists = false;

	if ( !empty( $excerpt ) || !empty( $content ) ){
		$read_more_exists = true;
	}
	else if ( strpos( (string) $content, '<!--more-->' ) ){
		$read_more_exists = true;
	}
	else if ( !empty( $content ) && !empty( $chars_count ) ){
		$proc_content = get_the_content( "" );
		$proc_content = trim( preg_replace( '/[\s]{2,}/u', ' ', strip_shortcodes( strip_tags( $proc_content ) ) ) );
		$chars_count = (int)$chars_count;
		$proc_content = mb_substr( $proc_content, 0, $chars_count );
		$read_more_exists = strlen( $proc_content ) < strlen( $content );
	}

	if ( $read_more_exists && !in_array( 'read_more', $hide_meta ) ){
		$out.= "<div class='post_button clearfix btn-read-more'>";
			$out.= "<a href='".esc_url($permalink)."' class='more-link'>$read_more</a>";
		$out.= "</div>";
	}

	return $out;
}

function cws_blog_meta(){
	$out = '';

	$author = cws_blog_meta_author();
	$likes = function_exists('cws_get_simple_likes_button') ? cws_blog_meta_likes() : '';
	$post_format = cws_blog_meta_post_format();
	$comments = cws_blog_meta_comments();
	$date = cws_blog_meta_date();

	$meta = 
		$author .
		$likes .
		$post_format .
		$comments .
		$date
	;

	if ( !empty( $meta ) ){
		$out.= "<div class='post_meta'>";
			$out.= $meta;
		$out.= "</div>";
	}

	return $out;
}

function cws_blog_meta_author(){
	global $cws_theme_funcs;
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$show_author = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "blog_author" ) : true;

	$author_part = '';
	if ( !in_array( 'author', $hide_meta ) && $show_author){
		$author = get_the_author();
		if ( !empty($author) ){
			ob_start();
				the_author_posts_link();
			$author_link = ob_get_clean();
			$author_part .= "<div class='info'>".esc_html__('by ', 'cryptop')."<span class='post_author'>".$author_link."</span></div>";
		}
	}
	return $author_part;
}

function cws_blog_meta_likes(){
	global $cws_theme_funcs;
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();

	$likes_part = '';
	if ( !in_array( 'likes', $hide_meta ) ){
		if ($cws_theme_funcs){
			$likes_part .= "<div class='like new_style'><i class='flaticon-shopping'></i>".cws_get_simple_likes_button( get_the_ID() )."</div>";
		}
	}
	return $likes_part;
}

function cws_blog_meta_post_format(){
	$special_pf = cws_is_special_post_format();
	$post_format_part = '';
	if ( $special_pf ){
		$post_format_part .= "<span class='pf'>" . cws_blog_post_format() . "</span>";
	}
	return $post_format_part;
}

function cws_blog_meta_comments(){
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$permalink = get_the_permalink( get_the_id() );	
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();

	$comments_part = '';
	if ( !in_array( 'comments', $hide_meta ) ){
		$comments_n = get_comments_number();
		if ( (int) $comments_n > 0 ) {
			$permalink .= "#comments";
			$comments_part .= "<div class='post_comments'>";
			$comments_part .= "<i class='flaticon-communication'><a href='".esc_url($permalink)."' class='comments_link'></i> $comments_n <span> ".esc_html__('comments', 'cryptop')."</span></a>";
			$comments_part .= "</div>";
		}
	}
	return $comments_part;
}

function cws_blog_meta_date(){
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$single = cws_is_single($page_id);
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$date_part = '';
	if ($date_container == 'meta' &&  !in_array( 'date', $hide_meta ) ){
		$date = get_the_time( get_option("date_format") );
		if ( !empty( $date ) ){
			$date_part .= "<div class='post_meta_date'>" . ($single ? "<i class='flaticon-time'></i>" : '') . $date . "</div>";
		}
	}
	return $date_part;
}

function cws_blog_taxanomy(){
	$out = '';

	$category = cws_blog_taxanomy_cats();									
	$tags = cws_blog_taxanomy_tags();

	$taxanomy = 
		$category .
		$tags
	;

	if ( !empty( $taxanomy ) ){
		$out.= "<div class='post_meta post_categories'>";
			$out.= $taxanomy;
		$out.= "</div>";		
	}

	return $out;
}

function cws_blog_taxanomy_cats (){
	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$category_part = '';
	if ( !in_array( 'cats', $hide_meta ) ){
		if ( has_category() ) {
			$category_part .= "<div class='post_category'>";	
				$cats = "";
				if ( has_category() ) {
					ob_start();
					the_category ( ",&nbsp;" );
					$cats .= ob_get_clean();
				}
				if ( !empty( $cats ) ){
					$category_part .= "<div class='post_terms post_cats'>";
						$category_part .= "<i class='flaticon-shapes'></i>";
						$category_part .= $cats;
					$category_part .= "</div>";
				}
			$category_part .= "</div>";
		}
	}
	return $category_part;
}

function cws_blog_taxanomy_tags(){
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$single = cws_is_single($page_id);
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$tags_part = '';
	if ( !in_array( 'tags', $hide_meta ) ){
		if ( has_tag() ) {
			$tags_part .= "<div class='post_tags'>";
				$tags = "";
				if ( has_tag() ) {
					ob_start();
						the_tags ( "", ($single ? "" : ", "), "" );
					$tags .= ob_get_clean();
				}
				if ( !empty( $tags ) ){
					$tags_part .= "<div class='post_terms post_tags'>";
						$tags_part .= "<i class='flaticon-business-1'></i>";
						$tags_part .= $tags;
					$tags_part .= "</div>";
				}				
			$tags_part .= "</div>";
		}
	}
	return $tags_part;
}

function cws_blog_social(){
	$page_id = isset($GLOBALS['cws_blog_atts']['page_id']) ? $GLOBALS['cws_blog_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_blog_atts'] ) ? $GLOBALS['cws_blog_atts'] : array();
	extract( $grid_atts );

	$out = '';
	$single = cws_is_single($page_id);
	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();

	if ( !in_array( 'social', $hide_meta ) ){	
		if ( is_plugin_active('wordpress-social-login/wp-social-login.php') ){
			$out .= "<div class='social_share'>".do_shortcode('[wordpress_social_login]')."</div>";
		}
	}
	return $out;
}
// ===========================//BLOG PARTS===========================

function cws_single_post_output ( $atts = array() ) {
	$fill_atts = cws_blog_fill_atts($atts);
	extract( $fill_atts );

	$hide_meta = isset($hide_meta) && !empty($hide_meta) ? $hide_meta : array();
	$uniq_pid = uniqid( "post_post_" );
	cws_blog_styles($uniq_pid);

	echo "<article id='".esc_attr($uniq_pid)."' ";
		post_class( array( 'item', 'blog_item', 'post') );
	echo ">";

		if (!empty($image_from_post) && $layout == 'fw_img') {
			$back_img_src = $image_from_post['src'];
			echo "<div class='back_img' style='background-image: url(".esc_url($back_img_src).");'></div>";
		}
		echo "<div class='post_wrapper clearfix'>";

			if ($post_meta['show_featured'] == '1' && isset($post_meta['full_width_featured'])){
				if ($post_meta['full_width_featured'] == '0'){
					$cws_blog_media = cws_blog_media();
				}
			} else if ($post_meta['show_featured'] == '1') {
				$cws_blog_media = cws_blog_media();
			}

			$post_bottom_wrapper = 
				cws_blog_meta() .
				cws_blog_taxanomy() .
				(function_exists('cws_get_simple_likes_button') ? cws_blog_meta_likes() : '')
			;

			$output = '';
			$count = count($elements_order);
			$featured_image_middle = false;
			foreach ($elements_order as $key => $value) {
				if($key == 0 && $value['field'] != 'featured_image'){
					$output .= "<div class='post_info'>";
				}

				if ($key == 0 && $value['field'] == 'featured_image'){
					$output .= $cws_blog_media;
					$output .= "<div class='post_info'>";
				} else if ($value['field'] == 'featured_image' && ($key != 0 && $key != $count-1)){
					$output .= "</div>";
					$output .= $cws_blog_media;
					$output .= "<div class='post_info'>";
					$featured_image_middle = true;
				} else if ($value['field'] == 'featured_image' && $key == $count-1){
					$output .= "</div>";
					$output .= $cws_blog_media;
					$output .= cws_page_links();
					$output .= cws_blog_social();								
				}

					if ($value['field'] == 'data'){
						$output .= cws_blog_date();
					}

					if ($value['field'] == 'title'){
						$output .= cws_blog_title();
					}

					if ($value['field'] == 'content'){
						$output .= cws_blog_content();
					}

					if ($value['field'] == 'meta'){
						if ( !empty( $post_bottom_wrapper ) ){
							$output .= "<div class='post_bottom_wrapper meta-bottom'>";
								$output .= $post_bottom_wrapper;
							$output .= "</div>";
						}
					}

					if ($value['field'] == 'read_more'){
						$output .= cws_blog_btn_more();
					}		
					
				if ($featured_image_middle && $key == $count-1){
					$output .= cws_page_links();
					$output .= cws_blog_social();
					$output .= "</div>";
				}
			}
			echo sprintf('%s', $output);

		echo "</div>";
	echo "</article>";
}