<?php
function cws_core_merge_components($g, $l) {
	return array_merge($g, $l);
}

/*
	build resulting array with replaced %name% layouts with their real components
	nothing to return as we work with a reference
*/
function cws_core_build_settings (&$layout, $components) {
	foreach ($layout as $key => &$v) {
		if (isset($v['layout'])) {
			$llayout = $v['layout'];
			if (is_string($llayout) && '%' === substr($llayout, 0, 1)) {
				$name = substr($llayout, 1, -1);
				if (isset($components[$name])) {
					$v['layout'] = $components[$name]; // replace keyword with actual array
				}
			} else {
				/*foreach ($llayout as $key => $vl) {
					if ('%' === substr($key, 0, 1) && '%' === substr($key, -1, 1)) {
						// replacing template with real array
						$name = substr($key, 1, -1);
						var_dump($name);
						if (isset($components[$name])) {
							unset($v['layout'][$key]);
							var_dump($v['layout']);
							$v['layout'] = array_merge($v['layout'], $components[$name]);
							var_dump($v['layout']);
							die;
						}
					}
				}*/
				cws_core_build_settings ($v['layout'], $components);
			}
		}
	}
}

function cws_core_get_base_components() {
	return
	array(
		'icon_options' => array(
			'icon_type' => array(
				'title' => esc_html__( 'Icon type', 'cws-to' ),
				'type' => 'radio',
				'subtype' => 'images',
				'value' => array(
					'fa' => array( esc_html__( 'icon', 'cws-to' ), 	true, 	'e:icon_fa;e:icon_color;e:icon_bg_type;d:icon_img', '/img/align-left.png' ),
					'img' =>array( esc_html__( 'image', 'cws-to' ), false,	'd:icon_fa;d:icon_color;d:icon_bg_type;e:icon_img', '/img/align-right.png' ),
				),
			),
			'icon_fa' => array(
				'title' => esc_html__( 'Font Awesome character', 'cws-to' ),
				'type' => 'select',
				'addrowclasses' => 'disable fai',
				'source' => 'fa',
			),
			'icon_img' => array(
				'title' => esc_html__( 'Custom icon', 'cws-to' ),
				'addrowclasses' => 'disable',
				'type' => 'media',
			),
			'icon_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Icon color', 'cws-to' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-default-color="#ffffff"',
			),
			'icon_bg_type' => array(
				'title' => esc_html__( 'Background type', 'cws-to' ),
				'type' => 'radio',
				'addrowclasses' => 'disable',
				'value' => array(
					'none' => array( esc_html__( 'None', 'cws-to' ), 	true, 	'd:icon_bg_color;d:gradient_settings;' ),
					'color' => array( esc_html__( 'Color', 'cws-to' ), 	true, 	'e:icon_bg_color;d:gradient_settings;' ),
					'gradient' =>array( esc_html__( 'Gradient', 'cws-to' ), false,'d:icon_bg_color;e:gradient_settings;' ),
				),
			),
			'icon_bg_color' => array(
				'type'      => 'text',
				'title'     => esc_html__( 'Icon background color', 'cws-to' ),
				'addrowclasses' => 'disable',
				'atts' => 'data-default-color=""',
			),
			'gradient_settings' => array(
				'type' => 'fields',
				'addrowclasses' => 'disable groups',
				'layout' => array(							
					'c1' => array(
						'type' => 'text',
						'title' => esc_html__( 'From', 'cws-to' ),
						'atts' => 'data-default-color=""',
						'addrowclasses' => 'grid-col-6',
					),
					'op1' => array(
						'type' => 'number',
						'title' => esc_html__( 'From (Opacity %)', 'cws-to' ),
						'value' => '100',
						'addrowclasses' => 'grid-col-6',
					),				
					'c2' => array(
						'type' => 'text',
						'title' => esc_html__( 'To', 'cws-to' ),
						'atts' => 'data-default-color=""',
						'addrowclasses' => 'grid-col-6',
					),
					'op2' => array(
						'type' => 'number',
						'title' => esc_html__( 'To (Opacity %)', 'cws-to' ),
						'value' => '100',
						'addrowclasses' => 'grid-col-6',
					),
					'type' => array(
						'title' => esc_html__( 'Gradient type', 'cws-to' ),
						'type' => 'radio',
						'addrowclasses' => 'grid-col-6',
						'value' => array(
							'linear' => array( esc_html__( 'Linear', 'cws-to' ),  true, 'e:linear;d:radial' ),
							'radial' =>array( esc_html__( 'Radial', 'cws-to' ), false,  'd:linear;e:radial' ),
						),
					),
					'linear' => array(
						'title' => esc_html__( 'Linear settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable grid-col-6',
						'layout' => array(
							'angle' => array(
								'type' => 'number',
								'title' => esc_html__( 'Angle', 'cws-to' ),
								'value' => '45',
							),
						)
					),
					'radial' => array(
						'title' => esc_html__( 'Radial settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable grid-col-12',
						'layout' => array(
							'shape_type' => array(
								'title' => esc_html__( 'Shape', 'cws-to' ),
								'type' => 'radio',
								'addrowclasses' => 'grid-col-4',
								'value' => array(
									'simple' => array( esc_html__( 'Simple', 'cws-to' ),  true, 'e:shape;d:size;d:keyword' ),
									'extended' =>array( esc_html__( 'Extended', 'cws-to' ), false, 'd:shape;e:size;e:keyword' ),
								),
							),
							'shape' => array(
								'title' => esc_html__( 'Gradient type', 'cws-to' ),
								'type' => 'radio',
								'addrowclasses' => 'grid-col-6',
								'value' => array(
									'ellipse' => array( esc_html__( 'Ellipse', 'cws-to' ),  true ),
									'circle' =>array( esc_html__( 'Circle', 'cws-to' ), false ),
								),
							),
							'size' => array(
								'type' => 'text',
								'addrowclasses' => 'disable grid-col-4',
								'title' => esc_html__( 'Size', 'cws-to' ),
								'atts' => 'placeholder="'.esc_html__('Two space separated percent values, for example (60% 55%)', 'cws-to').'"',
							),
							'keyword' => array(
								'type' => 'select',
								'title' => esc_html__( 'Size keyword', 'cws-to' ),
								'addrowclasses' => 'disable grid-col-4',
								'source' => array(
									'closest-side' => array(esc_html__( 'Closest side', 'cws-to' ), false),
									'farthest-side' => array(esc_html__( 'Farthest side', 'cws-to' ), false),
									'closest-corner' => array(esc_html__( 'Closest corner', 'cws-to' ), false),
									'farthest-corner' => array(esc_html__( 'Farthest corner', 'cws-to' ), true),
								),
							),
						)
					)
				),
			),
		),
		'gradient_layout' => array(
			'c1' => array(
				'type' => 'text',
				'title' => esc_html__( 'From', 'cws-to' ),
				'atts' => 'data-default-color=""',
				'addrowclasses' => 'grid-col-3',
			),
			'op1' => array(
				'type' => 'number',
				'title' => esc_html__( 'From (Opacity %)', 'cws-to' ),
				'value' => '100',
				'addrowclasses' => 'grid-col-3',
			),				
			'c2' => array(
				'type' => 'text',
				'title' => esc_html__( 'To', 'cws-to' ),
				'atts' => 'data-default-color=""',
				'addrowclasses' => 'grid-col-3',
			),
			'op2' => array(
				'type' => 'number',
				'title' => esc_html__( 'To (Opacity %)', 'cws-to' ),
				'value' => '100',
				'addrowclasses' => 'grid-col-3',
			),
			'type' => array(
				'title' => esc_html__( 'Gradient type', 'cws-to' ),
				'type' => 'radio',
				'addrowclasses' => 'grid-col-6',
				'value' => array(
					'linear' => array( esc_html__( 'Linear', 'cws-to' ),  true, 'e:linear;d:radial' ),
					'radial' =>array( esc_html__( 'Radial', 'cws-to' ), false,  'd:linear;e:radial' ),
				),
			),
			'linear' => array(
				'title' => esc_html__( 'Linear settings', 'cws-to'  ),
				'type' => 'fields',
				'addrowclasses' => 'disable grid-col-6',
				'layout' => array(
					'angle' => array(
						'type' => 'number',
						'title' => esc_html__( 'Angle', 'cws-to' ),
						'value' => '45',
					),
				)
			),
			'radial' => array(
				'title' => esc_html__( 'Radial settings', 'cws-to'  ),
				'type' => 'fields',
				'addrowclasses' => 'disable grid-col-12',
				'layout' => array(
					'shape_type' => array(
						'title' => esc_html__( 'Shape', 'cws-to' ),
						'type' => 'radio',
						'addrowclasses' => 'grid-col-4',
						'value' => array(
							'simple' => array( esc_html__( 'Simple', 'cws-to' ),  true, 'e:shape;d:size;d:keyword' ),
							'extended' =>array( esc_html__( 'Extended', 'cws-to' ), false, 'd:shape;e:size;e:keyword' ),
						),
					),
					'shape' => array(
						'title' => esc_html__( 'Gradient type', 'cws-to' ),
						'type' => 'radio',
						'addrowclasses' => 'grid-col-6',
						'value' => array(
							'ellipse' => array( esc_html__( 'Ellipse', 'cws-to' ),  true ),
							'circle' =>array( esc_html__( 'Circle', 'cws-to' ), false ),
						),
					),
					'size' => array(
						'type' => 'text',
						'addrowclasses' => 'disable grid-col-4',
						'title' => esc_html__( 'Size', 'cws-to' ),
						'atts' => 'placeholder="'.esc_html__('Two space separated percent values, for example (60% 55%)', 'cws-to').'"',
					),
					'keyword' => array(
						'type' => 'select',
						'title' => esc_html__( 'Size keyword', 'cws-to' ),
						'addrowclasses' => 'disable grid-col-4',
						'source' => array(
							'closest-side' => array(esc_html__( 'Closest side', 'cws-to' ), false),
							'farthest-side' => array(esc_html__( 'Farthest side', 'cws-to' ), false),
							'closest-corner' => array(esc_html__( 'Closest corner', 'cws-to' ), false),
							'farthest-corner' => array(esc_html__( 'Farthest corner', 'cws-to' ), true),
						),
					),
				)
			),
			'custom_css' => array(
				'title' => esc_html__( 'Custom CSS rules', 'cws-to' ),
				'subtitle' => esc_html__( 'Enter styles', 'cws-to' ),
				'atts' => 'rows="10"',
				'type' => 'textarea',
				'addrowclasses' => 'grid-col-12 full_row',
			),
		),
		'overlay_layout' => array(
			'type'	=> array(
				'title'		=> esc_html__( 'Color overlay', 'cws-to' ),
				'addrowclasses' => 'grid-col-4',
				'type' => 'select',
				'source'	=> array(
					'none' => array( esc_html__( 'None', 'cws-to' ),  true, 'd:opacity;d:color;d:gradient' ),
					'color' => array( esc_html__( 'Color', 'cws-to' ),  false, 'e:opacity;e:color;d:gradient' ),
					'gradient' => array( esc_html__( 'Gradient', 'cws-to' ), false, 'e:opacity;d:color;e:gradient' )
				),
			),
			'opacity' => array(
				'type' => 'number',
				'title' => esc_html__( 'Opacity', 'cws-to' ),
				'placeholder' => esc_html__( 'In percents', 'cws-to' ),
				'value' => '40',
				'addrowclasses' => 'disable grid-col-4',
			),
			'color'	=> array(
				'title'	=> esc_html__( 'Overlay color', 'cws-to' ),
				'addrowclasses' => 'disable grid-col-4',
				'atts' => 'data-default-color="#f0f0f0"',
				'value' => '#f0f0f0',
				'type'	=> 'text',
			),
			'gradient' => array(
				'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'disable grid-col-12 groups',
				'layout' => array(
					'c1' => array(
						'type' => 'text',
						'title' => esc_html__( 'From', 'cws-to' ),
						'atts' => 'data-default-color=""',
						'addrowclasses' => 'grid-col-6',
					),
					'op1' => array(
						'type' => 'number',
						'title' => esc_html__( 'From (Opacity %)', 'cws-to' ),
						'value' => '100',
						'addrowclasses' => 'grid-col-6',
					),				
					'c2' => array(
						'type' => 'text',
						'title' => esc_html__( 'To', 'cws-to' ),
						'atts' => 'data-default-color=""',
						'addrowclasses' => 'grid-col-6',
					),
					'op2' => array(
						'type' => 'number',
						'title' => esc_html__( 'To (Opacity %)', 'cws-to' ),
						'value' => '100',
						'addrowclasses' => 'grid-col-6',
					),
					'type' => array(
						'title' => esc_html__( 'Gradient type', 'cws-to' ),
						'type' => 'radio',
						'addrowclasses' => 'grid-col-6',
						'value' => array(
							'linear' => array( esc_html__( 'Linear', 'cws-to' ),  true, 'e:linear;d:radial' ),
							'radial' =>array( esc_html__( 'Radial', 'cws-to' ), false,  'd:linear;e:radial' ),
						),
					),
					'linear' => array(
						'title' => esc_html__( 'Linear settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable grid-col-6',
						'layout' => array(
							'angle' => array(
								'type' => 'number',
								'title' => esc_html__( 'Angle', 'cws-to' ),
								'value' => '45',
							),
						)
					),
					'radial' => array(
						'title' => esc_html__( 'Radial settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable grid-col-12',
						'layout' => array(
							'shape_type' => array(
								'title' => esc_html__( 'Shape', 'cws-to' ),
								'type' => 'radio',
								'addrowclasses' => 'grid-col-4',
								'value' => array(
									'simple' => array( esc_html__( 'Simple', 'cws-to' ),  true, 'e:shape;d:size;d:keyword' ),
									'extended' =>array( esc_html__( 'Extended', 'cws-to' ), false, 'd:shape;e:size;e:keyword' ),
								),
							),
							'shape' => array(
								'title' => esc_html__( 'Gradient type', 'cws-to' ),
								'type' => 'radio',
								'addrowclasses' => 'grid-col-6',
								'value' => array(
									'ellipse' => array( esc_html__( 'Ellipse', 'cws-to' ),  true ),
									'circle' =>array( esc_html__( 'Circle', 'cws-to' ), false ),
								),
							),
							'size' => array(
								'type' => 'text',
								'addrowclasses' => 'disable grid-col-4',
								'title' => esc_html__( 'Size', 'cws-to' ),
								'atts' => 'placeholder="'.esc_html__('Two space separated percent values, for example (60% 55%)', 'cws-to').'"',
							),
							'keyword' => array(
								'type' => 'select',
								'title' => esc_html__( 'Size keyword', 'cws-to' ),
								'addrowclasses' => 'disable grid-col-4',
								'source' => array(
									'closest-side' => array(esc_html__( 'Closest side', 'cws-to' ), false),
									'farthest-side' => array(esc_html__( 'Farthest side', 'cws-to' ), false),
									'closest-corner' => array(esc_html__( 'Closest corner', 'cws-to' ), false),
									'farthest-corner' => array(esc_html__( 'Farthest corner', 'cws-to' ), true),
								),
							),
						)
					),
					'custom_css' => array(
						'title' => esc_html__( 'Custom CSS rules', 'cws-to' ),
						'subtitle' => esc_html__( 'Enter styles', 'cws-to' ),
						'atts' => 'rows="10"',
						'type' => 'textarea',
						'addrowclasses' => 'grid-col-12 full_row',
					),
				),
			),
		),
		'border_layout' => array(
			'border_box'	=> array(
				'title'		=> esc_html__( 'Border', 'cws-to' ),
				'addrowclasses' => 'grid-col-3',
				'type'	=> 'select',
				'atts' => 'multiple data-none="d:width;d:type;d:color;d:line;"',
				'source'	=> array(
					'top' => array( esc_html__( 'Top', 'cws-to' ),  false, 'e:width;e:type;e:color;e:line;' ),
					'bottom' => array( esc_html__( 'Bottom', 'cws-to' ), false, 'e:width;e:type;e:color;e:line;' ),
				),
			),
			'width' => array(
				'title' => esc_html__( 'Width', 'cws-to' ),
				'addrowclasses' => 'disable grid-col-3',
				'type' => 'number',
				'atts' => ' min="1" step="1"',
				'value' => '2'
			),		
			'type'	=> array(
				'title'		=> esc_html__( 'Type', 'cws-to' ),
				'addrowclasses' => 'disable grid-col-3',
				'type'	=> 'select',
				'source'	=> array(
					'dotted' => array( esc_html__( 'Dotted', 'cws-to' ),  false, '' ),
					'dashed' => array( esc_html__( 'Dashed', 'cws-to' ),  false, '' ),
					'solid' => array( esc_html__( 'Solid', 'cws-to' ), true, '' ),
				),
			),
			'color'	=> array(
				'title'	=> esc_html__( 'Color', 'cws-to' ),
				'atts' => 'data-default-color="#f2f2f2"',
				'addrowclasses' => 'disable grid-col-3',
				'value' => '#f2f2f2',
				'type'	=> 'text',
			),
		),
		'parallax_layout' => array(
			'scalar-x' => array(
				'type' => 'number',
				'title' => esc_html__( 'x-axis parallax intensity', 'cws-to' ),
				'placeholder' => esc_html__( 'Integer', 'cws-to' ),
				'addrowclasses' => 'grid-col-6',
				'value' => '2'
			),
			'scalar-y' => array(
				'type' => 'number',
				'title' => esc_html__( 'y-axis parallax intensity', 'cws-to' ),
				'placeholder' => esc_html__( 'Integer', 'cws-to' ),
				'addrowclasses' => 'grid-col-6',
				'value' => '2'
			),
			'limit-x' => array(
				'type' => 'number',
				'title' => esc_html__( 'Maximum x-axis shift', 'cws-to' ),
				'placeholder' => esc_html__( 'Integer', 'cws-to' ),
				'addrowclasses' => 'grid-col-6',
				'value' => '15'
			),
			'limit-y' => array(
				'type' => 'number',
				'title' => esc_html__( 'Maximum y-axis shift', 'cws-to' ),
				'placeholder' => esc_html__( 'Integer', 'cws-to' ),
				'addrowclasses' => 'grid-col-6',
				'value' => '15'
			),
		),
		'image_layout' => array(
			'image' => array(
				'title' => esc_html__( 'Image', 'cws-to' ),
				'addrowclasses' => 'grid-col-2_5',
				'type' => 'media',
			),
			'size' => array(
				'title' => esc_html__( 'Size', 'cws-to' ),
				'addrowclasses' => 'grid-col-2_5',
				'type' => 'radio',
				'value' => array(
					'initial' =>array( esc_html__( 'Initial', 'cws-to' ), true,  '' ),
					'cover' => array( esc_html__( 'Cover', 'cws-to' ),  false, '' ),
					'contain' =>array( esc_html__( 'Contain', 'cws-to' ), false,  '' ),
				),
			),
			'repeat' => array(
				'title' => esc_html__( 'Repeat', 'cws-to' ),
				'addrowclasses' => 'grid-col-2_5',
				'type' => 'radio',
				'value' => array(
					'no-repeat' => array( esc_html__( 'No repeat', 'cws-to' ),  false, '' ),
					'repeat' => array( esc_html__( 'Tile', 'cws-to' ),  true, '' ),
					'repeat-x' => array( esc_html__( 'Tile Horizontally', 'cws-to' ),  false, '' ),
					'repeat-y' =>array( esc_html__( 'Tile Vertically', 'cws-to' ), false,  '' ),
				),
			),
			'attachment' => array(
				'title' => esc_html__( 'Attachment', 'cws-to' ),
				'addrowclasses' => 'grid-col-2_5',
				'type' => 'radio',
				'value' => array(
					'scroll' => array( esc_html__( 'Scroll', 'cws-to' ),  true, '' ),
					'fixed' =>array( esc_html__( 'Fixed', 'cws-to' ), false, '' ),
					'local' =>array( esc_html__( 'Local', 'cws-to' ), false, '' ),
				),
			),
			'position' => array(
				'title' => esc_html__( 'Position', 'cws-to' ),
				'addrowclasses' => 'grid-col-2_5',
				'cols' => 3,
				'type' => 'radio',
				'value' => array(
					'tl'=>	array( '', false ),
					'tc'=>	array( '', false ),
					'tr'=>	array( '', false ),
					'cl'=>	array( '', false ),
					'cc'=>	array( '', true ),
					'cr'=>	array( '', false ),
					'bl'=>	array( '', false ),
					'bc'=>	array( '', false ),
					'br'=>	array( '', false ),
				),
			),
		),
		'video_slider_layout' => array(
			'slider_switch' => array(
				'title' => esc_html__( 'Slider', 'cws-to' ),
				'addrowclasses' => 'checkbox',
				'type' => 'checkbox',
				'atts' => 'data-options="e:slider_shortcode;"',
			),
			'slider_shortcode' => array(
				'title' => esc_html__( 'Slider shortcode', 'cws-to' ),
				'addrowclasses' => 'disable box',
				'type' => 'text',
			),
			'set_video_header_height' => array(
				'title' => esc_html__( 'Set Video height', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'data-options="e:video_header_height"',
			),
			'video_header_height' => array(
				'title' => esc_html__( 'Video height', 'cws-to' ),
				'addrowclasses' => 'disable box',
				'type' => 'number',
				'value' => '600',
			),
			'video_type' => array(
				'title' => esc_html__('Video type', 'cws-to' ),
				'type' => 'radio',
				'value' => array(
					'self_hosted' => 	array( esc_html__('Self-hosted', 'cws-to' ), true, 'e:sh_source;d:youtube_source;d:vimeo_source' ),
					'youtube'=>	array( esc_html__('Youtube clip', 'cws-to' ), false, 'd:sh_source;e:youtube_source;d:vimeo_source' ),
					'vimeo' => 	array( esc_html__('Vimeo clip', 'cws-to' ), false, 'd:sh_source;d:youtube_source;e:vimeo_source' ),
				),
			),
			'sh_source' => array(
				'title' => esc_html__( 'Add video', 'cws-to' ),
				'addrowclasses' => 'box',
				'url-atts' => 'readonly',
				'type' => 'media',
			),
			'youtube_source' => array(
				'title' => esc_html__( 'Youtube video code', 'cws-to' ),
				'addrowclasses' => 'disable box',
				'type' => 'text',
			),
			'vimeo_source' => array(
				'title' => esc_html__( 'Vimeo embed url', 'cws-to' ),
				'addrowclasses' => 'disable box',
				'type' => 'text',
			),
			'color_overlay_type' => array(
				'title' => esc_html__( 'Overlay type', 'cws-to' ),
				'type' => 'select',
				'source' => array(
					'none' => array( esc_html__( 'None', 'cws-to' ), 	true, 'd:overlay_color;d:slider_gradient_settings;d:color_overlay_opacity;'),
					'color' => array( esc_html__( 'Color', 'cws-to' ), 	false, 'e:overlay_color;d:slider_gradient_settings;e:color_overlay_opacity;'),
					'gradient' =>array( esc_html__( 'Gradient', 'cws-to' ), false, 'd:overlay_color;e:slider_gradient_settings;e:color_overlay_opacity;'),
				),
			),
			'overlay_color' => array(
				'title' => esc_html__( 'Overlay Color', 'cws-to' ),
				'atts' => 'data-default-color=""',
				'addrowclasses' => 'box',
				'type' => 'text',
			),
			'color_overlay_opacity' => array(
				'type' => 'number',
				'addrowclasses' => 'box',
				'title' => esc_html__( 'Opacity', 'cws-to' ),
				'placeholder' => esc_html__( 'In percents', 'cws-to' ),
				'value' => '40'
			),
			'slider_gradient_settings' => array(
				'title' => esc_html__( 'Gradient settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'disable box groups',
				'layout' => array(
					'first_color' => array(
						'type' => 'text',
						'title' => esc_html__( 'From', 'cws-to' ),
						'atts' => 'data-default-color=""',
					),
					'second_color' => array(
						'type' => 'text',
						'title' => esc_html__( 'To', 'cws-to' ),
						'atts' => 'data-default-color=""',
					),
					'first_color_opacity' => array(
						'type' => 'number',
						'title' => esc_html__( 'From (Opacity %)', 'cws-to' ),
						'value' => '100',
					),
					'second_color_opacity' => array(
						'type' => 'number',
						'title' => esc_html__( 'To (Opacity %)', 'cws-to' ),
						'value' => '100',
					),
					'type' => array(
						'title' => esc_html__( 'Gradient type', 'cws-to' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array( esc_html__( 'Linear', 'cws-to' ),  true, 'e:linear_settings;d:radial_settings' ),
							'radial' =>array( esc_html__( 'Radial', 'cws-to' ), false,  'd:linear_settings;e:radial_settings' ),
						),
					),
					'linear_settings' => array(
						'title' => esc_html__( 'Linear settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable',
						'layout' => array(
							'angle' => array(
								'type' => 'number',
								'title' => esc_html__( 'Angle', 'cws-to' ),
								'value' => '45',
							),
						)
					),
					'radial_settings' => array(
						'title' => esc_html__( 'Radial settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable',
						'layout' => array(
							'shape_settings' => array(
								'title' => esc_html__( 'Shape', 'cws-to' ),
								'type' => 'radio',
								'value' => array(
									'simple' => array( esc_html__( 'Simple', 'cws-to' ),  true, 'e:shape;d:size;d:size_keyword;' ),
									'extended' =>array( esc_html__( 'Extended', 'cws-to' ), false, 'd:shape;e:size;e:size_keyword;' ),
								),
							),
							'shape' => array(
								'title' => esc_html__( 'Gradient type', 'cws-to' ),
								'type' => 'radio',
								'value' => array(
									'ellipse' => array( esc_html__( 'Ellipse', 'cws-to' ),  true ),
									'circle' =>array( esc_html__( 'Circle', 'cws-to' ), false ),
								),
							),
							'size_keyword' => array(
								'type' => 'select',
								'title' => esc_html__( 'Size keyword', 'cws-to' ),
								'addrowclasses' => 'disable',
								'source' => array(
									'closest-side' => array(esc_html__( 'Closest side', 'cws-to' ), false),
									'farthest-side' => array(esc_html__( 'Farthest side', 'cws-to' ), false),
									'closest-corner' => array(esc_html__( 'Closest corner', 'cws-to' ), false),
									'farthest-corner' => array(esc_html__( 'Farthest corner', 'cws-to' ), true),
								),
							),
							'size' => array(
								'type' => 'text',
								'addrowclasses' => 'disable',
								'title' => esc_html__( 'Size', 'cws-to' ),
								'atts' => 'placeholder="'.esc_html__( 'Two space separated percent values, for example (60% 55%)', 'cws-to' ).'"',
							),
						)
					)

				),
			),
			'use_pattern' => array(
				'title' => esc_html__( 'Use pattern image', 'cws-to' ),
				'type' => 'checkbox',
				'addrowclasses' => 'checkbox',
				'atts' => 'data-options="e:pattern_image"',
			),
			'pattern_image' => array(
				'title' => esc_html__( 'Pattern image', 'cws-to' ),
				'addrowclasses' => 'disable box',
				'url-atts' => 'readonly',
				'type' => 'media',
			),
		),
		'static_image_layout' => array(
			'static_image' => array(
				'title' => esc_html__( 'Static image', 'cws-to' ),
				'type' => 'media',
				'url-atts' => 'readonly',
				'layout' => array(
					'is_high_dpi' => array(
						'title' => esc_html__( 'High-Resolution image', 'cws-to' ),
						'type' => 'checkbox',
						'addrowclasses' => 'checkbox',
					),
				),
			),
			'set_static_image_height' => array(
				'title' => esc_html__( 'Set Image height', 'cws-to' ),
				'addrowclasses' => 'checkbox',
				'type' => 'checkbox',
				'atts' => 'data-options="e:static_image_height;"',
			),
			'static_image_height' => array(
				'title' => esc_html__( 'Static Image Height', 'cws-to' ),
				'addrowclasses' => 'disable box',
				'type' => 'number',
				'default' => '600',
			),
			'static_customize_colors' => array(
				'title' => esc_html__( 'Customize colors', 'cws-to' ),
				'addrowclasses' => 'checkbox',
				'type' => 'checkbox',
				'atts' => 'data-options="e:img_header_color_overlay_type;e:img_header_overlay_color;e:img_header_color_overlay_opacity;"',
			),
			'img_header_color_overlay_type'	=> array(
				'title'		=> esc_html__( 'Color overlay type', 'cws-to' ),
				'type'	=> 'select',
				'addrowclasses' => 'box disable',
				'source'	=> array(
					'color' => array( esc_html__( 'Color', 'cws-to' ),  true, 'e:img_header_overlay_color;d:img_header_gradient_settings;' ),
					'gradient' => array( esc_html__( 'Gradient', 'cws-to' ), false, 'd:img_header_overlay_color;e:img_header_gradient_settings;' )
				),
			),
			'img_header_overlay_color'	=> array(
				'title'	=> esc_html__( 'Overlay color', 'cws-to' ),
				'atts' => 'data-default-color="' . CRYPTOP_FIRST_COLOR . '"',
				'value' => CRYPTOP_FIRST_COLOR,
				'addrowclasses' => 'box disable',
				'type'	=> 'text',
			),
			'img_header_gradient_settings' => array(
				'title' => esc_html__( 'Gradient Settings', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'disable box groups',
				'layout' => array(
					'first_color' => array(
						'type' => 'text',
						'title' => esc_html__( 'From', 'cws-to' ),
						'atts' => 'data-default-color=""',
					),
					'second_color' => array(
						'type' => 'text',
						'title' => esc_html__( 'To', 'cws-to' ),
						'atts' => 'data-default-color=""',
					),
					'first_color_opacity' => array(
						'type' => 'number',
						'title' => esc_html__( 'From (Opacity %)', 'cws-to' ),
						'value' => '100',
					),
					'second_color_opacity' => array(
						'type' => 'number',
						'title' => esc_html__( 'To (Opacity %)', 'cws-to' ),
						'value' => '100',
					),
					'type' => array(
						'title' => esc_html__( 'Gradient type', 'cws-to' ),
						'type' => 'radio',
						'value' => array(
							'linear' => array( esc_html__( 'Linear', 'cws-to' ),  true, 'e:img_header_gradient_linear_settings;d:img_header_gradient_radial_settings' ),
							'radial' =>array( esc_html__( 'Radial', 'cws-to' ), false,  'd:img_header_gradient_linear_settings;e:img_header_gradient_radial_settings' ),
						),
					),
					'linear_settings' => array(
						'title' => esc_html__( 'Linear settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable',
						'layout' => array(
							'angle' => array(
								'type' => 'number',
								'title' => esc_html__( 'Angle', 'cws-to' ),
								'value' => '45',
							),
						)
					),
					'radial_settings' => array(
						'title' => esc_html__( 'Radial settings', 'cws-to'  ),
						'type' => 'fields',
						'addrowclasses' => 'disable',
						'layout' => array(
							'shape_settings' => array(
								'title' => esc_html__( 'Shape', 'cws-to' ),
								'type' => 'radio',
								'value' => array(
									'simple' => array( esc_html__( 'Simple', 'cws-to' ),  true, 'e:img_header_gradient_shape;d:img_header_gradient_size;d:img_header_gradient_size_keyword;' ),
									'extended' =>array( esc_html__( 'Extended', 'cws-to' ), false, 'd:img_header_gradient_shape;e:img_header_gradient_size;e:img_header_gradient_size_keyword;' ),
								),
							),
							'shape' => array(
								'title' => esc_html__( 'Gradient type', 'cws-to' ),
								'type' => 'radio',
								'value' => array(
									'ellipse' => array( esc_html__( 'Ellipse', 'cws-to' ),  true ),
									'circle' =>array( esc_html__( 'Circle', 'cws-to' ), false ),
								),
							),
							'img_header_gradient_size_keyword' => array(
								'type' => 'select',
								'title' => esc_html__( 'Size keyword', 'cws-to' ),
								'addrowclasses' => 'disable',
								'source' => array(
									'closest-side' => array(esc_html__( 'Closest side', 'cws-to' ), false),
									'farthest-side' => array(esc_html__( 'Farthest side', 'cws-to' ), false),
									'closest-corner' => array(esc_html__( 'Closest corner', 'cws-to' ), false),
									'farthest-corner' => array(esc_html__( 'Farthest corner', 'cws-to' ), true),
								),
							),
							'img_header_gradient_size' => array(
								'type' => 'text',
								'addrowclasses' => 'disable',
								'title' => esc_html__( 'Size', 'cws-to' ),
								'atts' => 'placeholder="'.esc_html__( 'Two space separated percent values, for example (60% 55%)', 'cws-to' ).'"',
							),
						)
					)
				)
			),
			'img_header_color_overlay_opacity' => array(
				'type' => 'number',
				'title' => esc_html__( 'Opacity', 'cws-to' ),
				'addrowclasses' => 'box disable',
				'placeholder' => esc_html__( 'In percents', 'cws-to' ),
				'value' => '40'
			),
			'img_header_use_pattern' => array(
				'title' => esc_html__( 'Add pattern', 'cws-to' ),
				'addrowclasses' => 'checkbox',
				'type' => 'checkbox',
				'atts' => 'data-options="e:img_header_pattern_image;"',
			),
			'img_header_pattern_image' => array(
				'title' => esc_html__( 'Pattern image', 'cws-to' ),
				'type' => 'media',
				'addrowclasses' => 'disable box',
				'url-atts' => 'readonly',
			),
			'img_header_parallaxify' => array(
				'title' => esc_html__( 'Parallaxify image', 'cws-to' ),
				'addrowclasses' => 'checkbox',
				'type' => 'checkbox',
				'atts' => 'data-options="e:img_header_parallax_options;"',
			),
			'img_header_parallax_options' => array(
				'title' => esc_html__( 'Parallax options', 'cws-to' ),
				'type' => 'fields',
				'addrowclasses' => 'disable box groups',
				'layout' => array(
					'img_header_scalar-x' => array(
						'type' => 'number',
						'title' => esc_html__( 'x-axis parallax intensity', 'cws-to' ),
						'placeholder' => esc_html__( 'Integer', 'cws-to' ),
						'value' => '2'
					),
					'img_header_scalar-y' => array(
						'type' => 'number',
						'title' => esc_html__( 'y-axis parallax intensity', 'cws-to' ),
						'placeholder' => esc_html__( 'Integer', 'cws-to' ),
						'value' => '2'
					),
					'img_header_limit-x' => array(
						'type' => 'number',
						'title' => esc_html__( 'Maximum x-axis shift', 'cws-to' ),
						'placeholder' => esc_html__( 'Integer', 'cws-to' ),
						'value' => '15'
					),
					'img_header_limit-y' => array(
						'type' => 'number',
						'title' => esc_html__( 'Maximum y-axis shift', 'cws-to' ),
						'placeholder' => esc_html__( 'Integer', 'cws-to' ),
						'value' => '15'
					),
				),
			),
		),



















	);
}
?>
