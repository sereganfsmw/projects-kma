<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Cryptop
 * @since Cryptop 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<?php
		global $cws_theme_funcs;
		global $cws_theme_default;
		if ($cws_theme_funcs){
			$cws_theme_funcs->cws_header_meta();
		} else {
			$cws_theme_default->cws_header_meta();
		}
		wp_head();
	?>
</head>

<body <?php body_class(); ?>>
<?php
if ($cws_theme_funcs){
echo sprintf("%s", $cws_theme_funcs->cws_body_overlay()) ;
echo sprintf("%s", $cws_theme_funcs->cws_side_panel()) ;
}
?>
<!-- body cont -->
<div class='body-cont'>
	<?php
		if ($cws_theme_funcs){
			echo sprintf("%s", $cws_theme_funcs->cws_page_loader()) ;
			echo sprintf("%s", $cws_theme_funcs->cws_page_header()) ;
		} else { ?>


			<div class="header_wrapper">
				<div class="header_cont">
					<header class="site_header logo-left menu-right logo-in-menu none-sandwich-menu"><!-- header -->
						<div class="header_container"><!-- header_container -->

							<div class="header_zone default_header"><!-- header_zone -->							
								<!-- logo_box -->
								<!-- /logo_box -->

								<!-- menu_box -->
								<div class="menu_box default_menu_box">
									<div class="container wide_container">

										<div class="header_logo_part ">
											<h1 class="header_site_title"><a href="<?php echo esc_url( home_url() ); ?>"><?php echo get_bloginfo( 'name' ); ?></a></h1>				
			                			</div>
										<div class="header_nav_part">
											<nav class="main-nav-container">	
											<?php

												ob_start();
													$menu_locations = get_nav_menu_locations();
													if ( isset( $menu_locations['header-menu'] ) && !empty($menu_locations['header-menu'])  ) {
														wp_nav_menu( array(
															'theme_location'  => 'header-menu',
															'menu_class' => 'main-menu',
															'items_wrap'      => '<div class="no-split-menu"><ul id="%1$s" class="%2$s">%3$s</ul></div>',
															'container' => false,
													
														) );
													}
												$menu = ob_get_clean();
												if ( ! empty ( $menu ) ){
														echo sprintf("%s", $menu);
												}

											?>
											</nav>
										</div>
										<div class="menu_right_icons">
											<div class="search_menu">								
	
											</div>											

											<div data-menu="menu_box" class="mobile_menu_switcher mobile_menu_hamburger mobile_menu_hamburger--htx">
												<div class="mobile_menu_hamburger_wrapper">
													<span></span>
													<span></span>
													<span></span>
													<span></span>
													<span></span>
													<span></span>
													<span></span>
													<span></span>
													<span></span>
												</div>
											</div>

										</div>
									</div>
									<div class="search_menu_wrap">
										<div class="search_menu_cont">
											<div class="container">						
												<form method='get' class='search-form' action='<?php echo esc_url(site_url()); ?>' >
													<div class='search_wrapper'>
														<label><span class='screen-reader-text'><?php esc_html_e( 'Search for:', 'cryptop' ); ?></span></label>
														<input type='text' placeholder='<?php esc_html_e( 'Type and press Enter ...', 'cryptop' ); ?>' class='search-field' value='<?php echo esc_attr(apply_filters('the_search_query', get_search_query())); ?>' name='s'/>
														<input type='submit' class='search-submit' value='<?php esc_html_e( 'Search', 'cryptop' ); ?>' />
													</div>
												</form>

												<div class="search_back_button"></div>
											</div>
										</div>
									</div>		
	
									<div class="mobile_menu_wrapper">
										<span class="close_menu"></span>
										<div class="mobile_menu_container">
										<?php
											ob_start();

												wp_nav_menu( array(
													'theme_location'  => 'header-menu',
													'menu_id'  => 'mobile_menu',
													'menu_class' => 'mobile_menu main-menu',
													'container' => false,
												) );

											$mobile_menu = ob_get_clean();
											if ( ! empty ( $mobile_menu ) ){
												echo sprintf("%s", $mobile_menu);
											}

										?>
										<i class="mobile_menu_switcher"></i>
										</div>
									</div>
											
								</div><!-- /menu_box -->
								<?php
									$cws_theme_default->cws_site_header();
								?>
							</div><!-- /header_zone -->			
						</div><!-- header_container -->
					</header><!-- header -->
				</div>
			</div>


	<?php 
		}	?>
	<div id="main" class="site-main">