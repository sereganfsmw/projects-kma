<?php
	get_header ();

	global $cws_theme_funcs;
	global $cws_theme_default;
	
	$pid = get_the_id();
	if ($cws_theme_funcs){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$class = $sb['layout_class'].' '. $sb['sb_class'];
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);

		$post_meta = $cws_theme_funcs->cws_get_post_meta( $pid );
		if(isset($post_meta[0])){
			$post_meta = $post_meta[0];
		} 		
		extract( shortcode_atts( array(
			'enable_lightbox' => '0',
			'show_related' => '0',
			'author_info' => '1',
			'show_featured' => '',
			'full_width_featured' => '',
		), $post_meta) );	

		$fw_featured = $show_featured == '1' && $full_width_featured == '1';	
	}

	$page_class = '';
	$page_class .= (isset($sb) ? $sb['sb_class'] : 'page_content');
	$page_class .= isset($fw_featured) && $fw_featured ? ' full_width_featured' : '';

	printf("<div class='%s'>", $page_class);
	
	if ($cws_theme_funcs){
		
		if ($fw_featured){
			echo cws_blog_media();
		}

		$GLOBALS['cws_single_post_atts'] = array(
			'sb_layout'	=> $sb['layout_class'],
		);	

		echo (isset($sb['content']) && !empty($sb['content'])) ? $sb['content'] : '';

		//Meta vars
		$related_projects_title = isset( $post_meta['rpo']['title'] ) ? $post_meta['rpo']['title'] : '';
		$related_projects_category = isset( $post_meta['rpo']['category'] ) ? $post_meta['rpo']['category'] : '';
		$related_projects_text_length = isset( $post_meta['rpo']['text_length'] ) ? $post_meta['rpo']['text_length'] : '90';
		$related_projects_cols = isset( $post_meta['rpo']['cols'] ) ? (int) $post_meta['rpo']['cols'] : '4';
		$related_projects_count = isset( $post_meta['rpo']['items_show'] ) ? (int) $post_meta['rpo']['items_show'] : '4';
		$related_hide_meta = isset( $post_meta['rpo']['hide_meta'] ) ? $post_meta['rpo']['hide_meta'] : '';
		//--Meta vars
	}

	$query_args = array(
		'post_type' => 'post',
		'ignore_sticky_posts' => true,
		'post_status' => 'publish',
	);
	$query_args["post__not_in"] = array( $pid );
	if ($cws_theme_funcs){
		if ( $related_projects_count ){
			$query_args['posts_per_page'] = $related_projects_count;
		}
	}
	$q = new WP_Query( $query_args );
	$related_posts = $q->posts;
	$has_related = false;
	if ( count( $related_posts ) > 0 ) {
		$has_related = true;
	}

	if ($cws_theme_funcs){
		$show_related_items  = $show_related == 1 && $has_related;
	}

	$section_class = "cws_blog single";

	$query = cws_blog_defaults();
	$query = array_merge($query,
		array(
			//Override atts
			'layout'			=> '1',
			'hide_meta'			=> array('title'),
			'date_container'	=> 'meta',
		)
	);

	?>
	<main>
		<?php if(isset($sb['content']) && !empty($sb['content'])){ 
			echo '<i class="sidebar-tablet-trigger"></i>';
		} ?>
		<div class="grid_row clearfix">
			<section class="<?php echo esc_attr($section_class) ?>">
				<div class="cws_blog_single_wrapper">
					<div class="grid">
						<?php
							while ( have_posts() ):
								the_post();
									cws_single_post_output($query);
							endwhile;
							wp_reset_postdata();
						?>
					</div>
				</div>
			</section>

            <!-- Post navigation -->
            <div class="nav_post_links">
                <?php
                    ob_start();
                    previous_post_link("%link", "%title"); 
                    $prev_link = ob_get_clean();
                    $prev_link = preg_replace( '/<\/a>/', '', $prev_link );
                
                if (!empty($prev_link)){ ?>
                <div class="nav_prev">
                    <div class="nav_wrapper">
                    	<?php printf("%s", $prev_link); ?>
                        <span class="prev_post"></span>
                        <span class="sub_title">Prev</span>
                        </a>
                    </div>
                </div>
				<?php }

                    ob_start();
                    next_post_link("%link", "%title"); 
                    $next_link = ob_get_clean();
                    $next_link = preg_replace( '/<\/a>/', '', $next_link );
                
                if (!empty($next_link)){ ?>
                <div class="nav_next">
                    <div class="nav_wrapper">
                    	<?php printf("%s", $next_link); ?>
                        <span class="next_post"></span>
                        <span class="sub_title">Next</span>
                        </a>
                    </div>
                </div>
                <?php } ?>
            </div>
			
		</div>

		<?php

			//USER INFO BLOCK
			if ($cws_theme_funcs && is_plugin_active( 'cws-essentials/cws-essentials.php' )){
				if ($author_info){
					$author_meta = get_user_meta($post->post_author, 'cws_mb_user' );

					$author_position = (!empty($author_meta[0]['position']) ? $author_meta[0]['position'] : '');
					$author_avatar = (!empty($author_meta[0]['avatar']['src']) ? $author_meta[0]['avatar']['src'] : '');
					$author_social = (!empty($author_meta[0]['social_group']) ? $author_meta[0]['social_group'] : '');
					$author_url = (!empty($author_meta[0]['author_url']) ? $author_meta[0]['author_url'] : '');
					$author_description = get_user_meta($post->post_author, 'description', true);

					$first_name = get_user_meta($post->post_author, 'first_name', true);
					$last_name = get_user_meta($post->post_author, 'last_name', true);

					if (!empty($first_name) || !empty($last_name)){
						$author_name = $first_name . (!empty($last_name) ? ' '.$last_name : '' );
					} else {
						$author_name = get_the_author();
					}

					if(!empty( $author_avatar )) {
						$thumb_obj = cws_thumb( $author_avatar,array('width' => 150, 'height' => 150, 'crop' => false) ,false );
						$thumb_url = isset( $thumb_obj[0] ) ? $thumb_obj[0] : "";
						$retina_thumb = isset( $thumb_obj[3] ) ? $thumb_obj[3] : false;
					
					?>
					<div class="grid_row clearfix">
						<div class="author_info cws_content_top">
						<?php if(!empty( $author_avatar )) { ?>
							<div class='author_pic'>
								<?php
								echo (!empty($author_url) ? "<a href='".esc_url($author_url)."'>" : "" );
								if ( $retina_thumb ) {
									echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
								}
								else{
									echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
								}	
								echo (!empty($author_url) ? "</a>" :"" ) ?>
							</div>
						<?php } ?>
							<div class="author_description grid_col grid_col_12">
								<div class="cols_wrapper">
									<div class="widget_wrapper">
										<div class="ce clearfix">
											<div class="author_title">
												<h3><?php echo (!empty($author_url) ? "<a class='author_link' href='".esc_url($author_url)."'>".$author_name."</a>" : $author_name ) ?></h3>
												<?php echo !empty($author_position) ? " <h4>( ".esc_html($author_position)." )</h4>" : ''; ?>
											</div>
											<p><?php printf("%s", $author_description); ?></p>
											<div>
										<?php
										if(!empty( $author_social )) {
										?>
											<div class="cws_social_links">
												<?php foreach ($author_social as $key => $value) { ?>
													<a href="<?php echo esc_url($value['url']) ?>" class="cws_social_link" title="<?php echo esc_attr($value['title']) ?>" target="_blank">
														<i style="color:<?php echo esc_attr($value['color']); ?>;" class="cws_fa <?php echo esc_attr($value['icon']) ?> fa-2x simple_icon"></i>
													</a>
												<?php } ?>
											</div>
										<?php } ?>
											</div>									
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>				
					<?php
					}
				}
			}
			//--USER INFO BLOCK

			//RELATED POSTS
			if ( $cws_theme_funcs && $show_related_items ){
				$mode = $q->post_count > $related_projects_cols ? '1' : '0';
				
				$pid = get_queried_object_id();
				$related_atts = array(
					'related_items'				=> true,
  					'related_carousel'			=> true,
					'title'						=> $related_projects_title,
					'filter_by'					=> !empty($related_projects_category) ? 'category' : '',
  					'category' 					=> !empty($related_projects_category) ? $related_projects_category : array(),
  					'items_count'				=> $related_projects_count,
  					'layout'					=> $related_projects_cols,
  					'chars_count'				=> $related_projects_text_length,
  					'hide_meta'					=> $related_hide_meta,
  					'link_show'					=> 'popup_link',
  					'extra_query_args'			=> 
	  					array(
							'post__not_in'				=> array( $pid )
						),
				);

				$related_projects_section = cws_blog_output( $related_atts );
				echo !empty( $related_projects_section ) ? "<div class='grid_row single_post related_post'>$related_projects_section</div>" : "";
				unset( $GLOBALS['cws_single_post_atts'] );
			}
			//--RELATED POSTS

			if (is_singular() && comments_open() && get_option( 'thread_comments' ) == '1'){
				wp_enqueue_script('comment-reply');
			}

		?>

		<?php comments_template(); ?>
	</main>
	<?php echo (isset($sb) && !empty($sb['content']) ) ? '</div>' : ''; ?>
</div>

<?php

get_footer ();
?>