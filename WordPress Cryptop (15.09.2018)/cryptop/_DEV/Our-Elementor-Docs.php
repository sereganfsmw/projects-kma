<?php 

//Вложенная проверка для элементов
'conditions' => [
	'relation' => 'and',
	'terms' => [
		[
			'relation' => 'and',			//Доступны 'or' или 'and'
			'terms' => [
				[
					'name' => 'icon_lib',
					'operator' => '!=', 	//Операторы описаны ниже
					'value' => 'svg'
				]
			]
		], [
			'relation' => 'or',
			'terms' => [
				[
					'name' => 'icon_fontawesome',
					'operator' => '!=',
					'value' => ''
				], [
					'name' => 'icon_flaticons',
					'operator' => '!=',
					'value' => ''
				]
			]
		]
	]
]

//Проверки для елементов
'conditions' => [
	'relation' => 'or',						//Доступны 'or' или 'and'
	'terms' => [
		[
			'name' => 'title_banners',
			'operator' => '==',				//Операторы описаны ниже
			'value' => '5',
		], [
			'name' => 'desc_banners',
			'operator' => '==',
			'value' => '7',
		],
	],
],

//Проверки для (вложенных полей)
'conditions' => [
	'relation' => 'or',
	'terms' => [
		[
			'name' => 'desc_banners',
			'operator' => '==',
			'value' => '7',
		], [
			'name' => 'icon_img[url]',      //Вложенное поле
			'operator' => '==',
			'value' => ''
		]
	],
],

switch ( $operator ) {
	case '==':
		return $left_value == $right_value;
	case '!=':
		return $left_value != $right_value;
	case '!==':
		return $left_value !== $right_value;
	case 'in':
		return -1 !== array_search( $left_value, $right_value );
	case '!in':
		return -1 === array_search( $left_value, $right_value );
	case '<':
		return $left_value < $right_value;
	case '<=':
		return $left_value <= $right_value;
	case '>':
		return $left_value > $right_value;
	case '>=':
		return $left_value >= $right_value;
	default:
		return $left_value === $right_value;
}

/*---------------------------------*/

'condition' => [
	'title_banners!' => '',						//Если поле не пустое
	'!title_banners' => '',						//Если поле пустое
	'testimonial_image[url]!' => '',			//Проверить конкретный параметр
],

'prefix_class' => 'elementor%s-align-',  // <------- Добавить префикс в Wrapper над Container (Без перерисовки шаблона)

/*----------------------------------------------------------------------------------*/
//Добавляем классы для контейнера WRAPPER
public function render_content() {		
	add_action('elementor/widget/before_render_content', function($widget, $instance = []){
		if ($widget->get_data('widgetType') == 'cws_icon'){
			// var_dump($widget->get_settings);			
		}
	}, 10, 2);

	do_action( 'elementor/widget/before_render_content', $this );

	//Edit buttons
	if ( Plugin::$instance->editor->is_edit_mode() ) {
		$this->render_edit_tools();
	}

	$classes = '';

	if ($this->get_data('widgetType') == 'cws_icon'){

		if ($this->get_settings('aligning') == 'left'){
			$classes .= ' align_left';
		}

		if ($this->get_settings('aligning') == 'center'){
			$classes .= ' align_center';
		}

		if ($this->get_settings('aligning') == 'right'){
			$classes .= ' align_right';
		}
	}

	echo "<div class='elementor-widget-container".esc_attr($classes)."'>";

		ob_start();
			$skin = $this->get_current_skin();
			if ( $skin ) {
				$skin->set_parent( $this );
				$skin->render();
			} else {
				$this->render();
			}
		$widget_content = ob_get_clean();
		$widget_content = apply_filters( 'elementor/widget/render_content', $widget_content, $this );
		echo sprintf('%s', $widget_content);
		
	echo "</div>";
}

//Переопределяем дефолтные значения в Widget (Виджетах)
protected function _get_initial_config() {
	$config = [
		'widget_type' => $this->get_name(),
		'keywords' => $this->get_keywords(),
		'categories' => $this->get_categories(),
	];

	$out = array_merge( parent::_get_initial_config(), $config );

	//Default background for cws-banner
	$out['controls']['_background_background']['default'] = 'classic';
	$out['controls']['_background_color']['default'] = '#0F1A27';
	$out['controls']['_background_color']['value'] = '#0F1A27';

	//Default paddings for cws-banner
	$out['controls']['_padding']['default'] = [
		'unit' => 'px',
		'top' => '50',
		'right' => '50',
		'bottom' => '50',
		'left' => '50',
		'isLinked' => false
	];

	//Default borders for cws-banner
	$out['controls']['_border_radius']['default'] = [
		'unit' => 'px',
		'top' => '5',
		'right' => '5',
		'bottom' => '5',
		'left' => '5',
		'isLinked' => false
	];

	return $out;
}

//Получаем значение с соседних контролов для текущего
//background_overlay_hover_transition.SIZE
$controls->add_control(
	'border_hover_transition',
	[
		'label' => esc_html__( 'Transition Duration', 'cryptop' ),
		'type' => Controls_Manager::SLIDER,
		'default' => [
			'size' => 0.3,
		],
		'range' => [
			'px' => [
				'max' => 3,
				'step' => 0.1,
			],
		],
		'conditions' => [
			'relation' => 'or',
			'terms' => [
				[
					'name' => 'background_background',
					'operator' => '!==',
					'value' => '',
				], [
					'name' => 'border_border',
					'operator' => '!==',
					'value' => '',
				],
			],
		],
		'selectors' => [
			'{{WRAPPER}}' => 'transition: background {{background_hover_transition.SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
			'{{WRAPPER}} > .elementor-background-overlay' => 'transition: background {{background_overlay_hover_transition.SIZE}}s, border-radius {{SIZE}}s, opacity {{background_overlay_hover_transition.SIZE}}s',
		],
	]
);

/*----------------------------------------------------------------------------------*/

//Все виды контролов с их полными параметрами что есть

//Открывается секция
$controls->start_controls_section(
	'section_general',
	[
		'label' => esc_html__( 'General', 'cryptop' ),
		'tab' => Controls_Manager::TAB_CONTENT,
		'tab' => Controls_Manager::TAB_STYLE,
		'tab' => Controls_Manager::TAB_ADVANCED,
		'tab' => Controls_Manager::TAB_RESPONSIVE,
		'tab' => Controls_Manager::TAB_LAYOUT,
		'tab' => Controls_Manager::TAB_SETTINGS,
	]
);

//Добавляются контролы
	$controls->add_responsive_control( // <----- Респонсив контрол
	$controls->add_control( // <----- Обычный

//Закрывается секция
$controls->end_controls_section();

/*---------------------------------*/

$controls->add_control(
	'style_type',
	[
		'label' => esc_html__( 'Select Style Type', 'cryptop' ),
		'type' => Controls_Manager::SELECT,				
		'default' => 'style1',
		'options' => [
			'style1' => esc_html__( 'Style 1', 'cryptop' ),
			'style2' => esc_html__( 'Style 2', 'cryptop' ),
		],
		'render_type' => 'template', // <----- Перезагружает шаблон
		'render_type' => 'none', // <----- Не отображает изменения
		'render_type' => 'ui', // <----- Рендерит только "selectors"
	]
);

/*---------------------------------*/
//Добавляем всплывающие блоки

$controls->add_control(
	'popover_toggle',
	[
		'label' => esc_html__( 'Box', 'cryptop' ),
		'type' => Controls_Manager::POPOVER_TOGGLE,
		'label_off' => esc_html__( 'Default', 'cryptop' ),
		'label_on' => esc_html__( 'Custom', 'cryptop' ),
		'return_value' => 'yes',
		'condition' => [
			'extra_color' => 'yes',
			'background' => [ 'classic' ],
		],
	]
);

$controls->start_popover();

$controls->add_control...

$controls->end_popover();
/*---------------------------------*/

//HEADING
$controls->add_control(
	'heading_style_dots',
	[
		'label' => esc_html__( 'Bullets', 'cryptop' ),
		'type' => Controls_Manager::HEADING,
		'separator' => 'before',
	]
);

//CHOOSE
$controls->add_responsive_control(
	'button_float',
	[
		'label' => esc_html__( 'Button Float', 'cryptop' ),
		'type' => Controls_Manager::CHOOSE,
		'default' => 'none',
		'toggle' => false,				// <----- Нельзя убрать значение
		'prefix_class' => 'elementor%s-align-',  // <------- Добавить префикс в Wrapper над Container
		'options' => [
			'left'    => [
				'title' => esc_html__( 'Left', 'cryptop' ),
				'icon' => 'fa fa-align-left',
			],
			'none' => [
				'title' => esc_html__( 'None', 'cryptop' ),
				'icon' => 'fa fa-times',
			],
			'right' => [
				'title' => esc_html__( 'Right', 'cryptop' ),
				'icon' => 'fa fa-align-right',
			],
		],
		'condition' => [
			'add_button' => 'yes'
		],
		'tablet_default' => [
			'unit' => '%',
		],
		'mobile_default' => [
			'unit' => '%',
		],
	]
);

//CHOOSE (2 Вариант)
$controls->add_control(
	'button_float',
	[
		'responsive' => true, // <----- RESPONSIVE true
		'label' => esc_html__( 'Button Float', 'cryptop' ),
		'type' => Controls_Manager::CHOOSE,
		'default' => 'none',
		'toggle' => false,				// <----- Нельзя убрать значение
		'options' => [
			'left'    => [
				'title' => esc_html__( 'Left', 'cryptop' ),
				'icon' => 'fa fa-align-left',
			],
			'none' => [
				'title' => esc_html__( 'None', 'cryptop' ),
				'icon' => 'fa fa-times',
			],
			'right' => [
				'title' => esc_html__( 'Right', 'cryptop' ),
				'icon' => 'fa fa-align-right',
			],
		],
		'condition' => [
			'add_button' => 'yes'
		],
	]
);

/*---------------------------------*/

//SELECT
$controls->add_control(
	'style_type',
	[
		'label' => esc_html__( 'Select Style Type', 'cryptop' ),
		'type' => Controls_Manager::SELECT,				
		'default' => 'style1',
		'options' => [
			'style1' => esc_html__( 'Style 1', 'cryptop' ),
			'style2' => esc_html__( 'Style 2', 'cryptop' ),
		],			
	]
);

//SELECT2
$controls->add_control(
	'show_elements',
	[
		'label' => esc_html__( 'Show Elements', 'cryptop' ),
		'type' => Controls_Manager::SELECT2,
		'multiple' => true,
		'options' => [
			'title'  => esc_html__( 'Title', 'cryptop' ),
			'description' => esc_html__( 'Description', 'cryptop' ),
			'button' => esc_html__( 'Button', 'cryptop' ),
		],
		'default' => [ 'title', 'description' ],
	]
);

//IMAGE
$controls->add_control(
	'banners_img',
	[
		'label' => esc_html__( 'Image', 'cryptop' ),
		'type' => Controls_Manager::MEDIA,
		'default' => [
			'url' => Utils::get_placeholder_image_src(),
		],
	]
);

//TEXT
$controls->add_control(
	'title_banners',
	[
		'label' => esc_html__( 'Label', 'cryptop' ),
		'label_block' => true,
		'title' => esc_html__( 'Title', 'cryptop' ),
		'type' => Controls_Manager::TEXT,
		'default' => esc_html__( '25%', 'cryptop' ),
		'description' => esc_html__( 'Description under field', 'cryptop' ),
		'placeholder' => esc_html__( 'Placeholder', 'cryptop' ),
	]
);

//NUMBER
$controls->add_control(
	'size',
	[
		'label' => esc_html__( 'Font Size Title (px)', 'cryptop' ),
		'type' => Controls_Manager::NUMBER,				
		'min' => 1,
		'max' => 100,
		'step' => 1,
		'default' => 45,
		'selectors' => [
			'{{WRAPPER}} .cws_banners.cws_module .banners_body .banners_title' => 'font-size: {{VALUE}}px',
		],
	]
);

//SLIDER
$controls->add_control(
	'width',
	[
		'label' => esc_html__( 'Width', 'cryptop' ),
		"description"	=> esc_html__( 'In Percents', 'cryptop' ),
		'type' => Controls_Manager::SLIDER,
		'size_units' => [ 'px', '%' ],
		'range' => [
			'px' => [
				'min' => 0,
				'max' => 1000,
				'step' => 5,
			],
			'%' => [
				'min' => 0,
				'max' => 100,
			],
		],
		'default' => [
			'unit' => '%',
			'size' => 50,
		],
		'selectors' => [
			'(desktop+){{WRAPPER}} .box' => 'width: {{SIZE}}{{UNIT}};', //Специальный селектор работает только на Desktop
		],
	]
);

//SWITCHER (CHECKBOX)
$controls->add_control(
	'add_divider',
	[
		'label' => esc_html__( 'Add Divider', 'cryptop' ),
		'type' => Controls_Manager::SWITCHER,
		'default' => '',
		'label_on' => esc_html__( 'Hide', 'elementor' ),
		'label_off' => esc_html__( 'Show', 'elementor' ),
		'prefix_class' => 'elementor-',	
	]
);

//URL
$controls->add_control(
	'button_url',
	[
		'label' => esc_html__( 'Url', 'cryptop' ),
		'type' => Controls_Manager::URL,
		'placeholder' => esc_html__( 'http://', 'cryptop' ),
		'show_external' => true,
		'default' => [
			'url' => '',
			'is_external' => true,
			'nofollow' => true,
		],
	]
);

//WYSIWYG
$controls->add_control(
	'content',
	[
		'label' => esc_html__( 'Content', 'cryptop' ),
		'type' => Controls_Manager::WYSIWYG,
		'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
	]
);	

//TEXTAREA
$controls->add_control(
	'content',
	[
		'label' => esc_html__( 'Content', 'cryptop' ),
		'rows' => 10,
		'type' => Controls_Manager::TEXTAREA,
		'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
	]
);	

//COLORPICKER
$controls->add_control(
	'custom_color',
	[
		'label' => esc_html__( 'Font Color', 'cryptop' ),
		'type' => Controls_Manager::COLOR,
		'value' => $first_color,
		'default' => $first_color,
		'selectors' => [
			'{{WRAPPER}} .cws_banners.cws_module .banners_body' => 'color: {{VALUE}}',
		],
		'condition' => [
			'customize_colors' => ['yes']
		],		
	]
);

//CODE
$controls->add_control(
	'custom_styles',
	[
		'label' => esc_html__( 'Custom styles', 'cryptop' ),
		'type' => Controls_Manager::CODE,
		'language' => 'css',
		'rows' => 20,
	]
);

//DIMENSIONS (PADDINGS, MARGIN)
$controls->add_control(
	'margins',
	[
		'label' => esc_html__( 'Margin', 'cryptop' ),
		'type' => Controls_Manager::DIMENSIONS,
		'size_units' => [ 'px', '%', 'em' ],
		'allowed_dimensions' => [ 'top', 'right', 'bottom', 'left' ],
		'selectors' => [
			'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
		],
		'default' => [
			'top' => 50,
			'right' => 0,
			'bottom' => 0,
			'left' => 0
		],
	]
);

//ICON
$controls->add_control(
	'icon_flaticons',
	[
		'label' => esc_html__( 'CWS Flaticons', 'cryptop' ),
		'type' => Controls_Manager::ICON,
		'options' => $fl_icons,
		'include' => $fl_icons_include,
		'condition' => [
			'icon_lib' => 'flaticons'
		],
	]
);	

//TYPOGRAPHY
$controls->add_group_control(
	CWS_Group_Control_Typography::get_type(),
	[
		'name' => 'title_typography',
		'scheme' => Scheme_Typography::TYPOGRAPHY_1,
		'selector' => '{{WRAPPER}} .title_wrapper .widgettitle',
		'label'	=> esc_html__( 'Title Typography', 'cryptop' ),
		'render_type' => 'template', //Рендерить шаблон при изменении атрибутов (кроме тех что применяються стилями)
		'condition' => [
			'title!' => ''
		],
		'defaults' => [	//Дефолтные значения для вложенных полей
			'font_size' => [
				'size' => 50,
				'unit' => 'px'
			],
			'text_transform' => 'none',
			'font_style' => 'normal',
			'font_weight' => '400',
			'text_decoration' => 'none',
			'html_tag' => 'h2',
			'line_height' => [
				'size' => 1,
				'unit' => 'em'
			],
			'letter_spacing' => [
				'size' => 0,
				'unit' => 'px'
			],
		],
		'mobile_defaults' => [
			'font_size' => [
				'size' => 50,
				'unit' => 'px'
			],
			'text_transform' => 'none',
			'font_style' => 'normal',
			'text_decoration' => 'none',
			'html_tag' => 'h2',
			'line_height' => [
				'size' => 1,
				'unit' => 'em'
			],
			'letter_spacing' => [
				'size' => 0,
				'unit' => 'px'
			],
		],
		'tablet_defaults' => [
			'font_size' => [
				'size' => 50,
				'unit' => 'px'
			],
			'text_transform' => 'none',
			'font_style' => 'normal',
			'text_decoration' => 'none',
			'html_tag' => 'h2',
			'line_height' => [
				'size' => 1,
				'unit' => 'em'
			],
			'letter_spacing' => [
				'size' => 0,
				'unit' => 'px'
			],
		],		
		'exclude' => ['html_tag'] //Исключить поле из вывода и обработки
	]
);

//BORDER
$controls->add_group_control(
	CWS_Group_Control_Border::get_type(),
	[
		'name' => 'inner_border',
		'fields_names' => [
			'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
			'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
			'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
		],
		'selector' => '{{WRAPPER}} .cws_service_icon_container',
		'separator' => 'before',
		'fields_options' => [
			'width' => [
				'allowed_dimensions' => [ 'top', 'bottom' ],
			],
		],		
		'defaults' => [
			'border' => 'solid',
			'width' => [
				'top' => 1,
				'right' => 1,
				'bottom' => 1,
				'left' => 1,
				'unit' => 'px'
			],
			'color' => '#000000',
		],				
	]
);

//GRADIENT
$controls->add_group_control(
	CWS_Group_Control_Gradient::get_type(),
	[
		'name' => 'gradient',
		'label' => esc_html__( 'Background gradient', 'cryptop' ),
		'types' => [ 'classic', 'gradient' ],
		'selector' => '{{WRAPPER}} .some_div',
		'defaults' => [
			'background' => 'gradient',
			'color' => '#000000',
			'color_stop' => [
				'unit' => '%',
				'size' => 0,
			],
			'color_b' => '#ff0000',
			'color_b_stop' => [
				'unit' => '%',
				'size' => 100,
			],
			'gradient_type' => 'linear',
			'gradient_angle' => [
				'unit' => 'deg',
				'size' => 180,
			],
			'gradient_position' => 'center center',
		],
	]
);

//BACKGROUND
$controls->add_group_control(
	CWS_Group_Control_Background::get_type(),
	[
		'name' => 'background',
		'types' => [ 'classic', 'gradient', 'video' ],
		'fields_options' => [	//Опции внутрених полей
			'background' => [
				'frontend_available' => true,
			],
			'video_link' => [
				'frontend_available' => true,
			],
		],
	]
);

//REPEATER
//На PHP $item_id = 'elementor-repeater-item-'.$value['_id'];
$controls->add_control(
	'tips_list',
	[
		'label' => '',
		'type' => Controls_Manager::REPEATER,
		'title_field' => '<i class="{{ icon }}" aria-hidden="true"></i> {{{ title }}}',
		//'title_field' => '<i class="{{ social }}"></i> {{{ social.replace( \'fa fa-\', \'\' ).replace( \'-\', \' \' ).replace( /\b\w/g, function( letter ){ return letter.toUpperCase() } ) }}}',
		'default' => [
			[
				'text' => esc_html__( 'List Item #1', 'cryptop' ),
				'icon' => 'fa fa-check',
			],
			[
				'text' => esc_html__( 'List Item #2', 'cryptop' ),
				'icon' => 'fa fa-times',
			],
			[
				'text' => esc_html__( 'List Item #3', 'cryptop' ),
				'icon' => 'fa fa-dot-circle-o',
			],
		],
		'fields' => [
			[
				'name' => 'title',
				'label' => esc_html__( 'Tips title', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'label_block' => true,
				'placeholder' => esc_html__( 'Enter title here', 'cryptop' ),
				'default' => esc_html__( 'Title', 'cryptop' ),
			],					
			[
				'name' => 'x_coord',
				'label' => esc_html__( 'X Cordinates', 'cryptop' ),
				'description'	=> esc_html__( 'In Percents', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ '%' ],
				'label_block' => true,
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}}.hotspot-item' => 'left: {{SIZE}}{{UNIT}}',
				],
				'default' => [
					'unit' => '%',
					'size' => 50,
				],
				'range' => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
			],					
			[
				'name' => 'y_coord',
				'label' => esc_html__( 'Y Cordinates', 'cryptop' ),
				'description'	=> esc_html__( 'In Percents', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ '%' ],
				'label_block' => true,
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}}.hotspot-item' => 'top: {{SIZE}}{{UNIT}}',
				],
				'default' => [
					'unit' => '%',
					'size' => 50,
				],
				'range' => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
			],
			[
				'name' => 'icon',
				'label' => esc_html__( 'Font Awesome', 'cryptop' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'default' => 'fa fa-check',
			],
		],
	]
);

//REPEATER (With Tabs)
//На PHP $item_id = 'elementor-repeater-item-'.$value['_id'];
$repeater = new Repeater();
$repeater->start_controls_tabs( 'tabs_button_style');

	$repeater->start_controls_tab(
		'tab_button_normal',
		[
			'label' => esc_html__( 'Normal', 'cryptop' ),						
		]
	);

		$repeater->add_control(
			'first_title',
			[
			  'label' => esc_html__( 'Title 1', 'cryptop' ),
			  'type'  => Controls_Manager::TEXT
			]
		);

	$repeater->end_controls_tab();

	$repeater->start_controls_tab(
		'tab_button_hover',
		[
			'label' => esc_html__( 'Hover', 'cryptop' ),						
		]
	);

		$repeater->add_control(
			'second_title',
			[
			  'label' => esc_html__( 'Title 2', 'cryptop' ),
			  'type'  => Controls_Manager::TEXT
			]
		);

	$repeater->end_controls_tab();

$repeater->end_controls_tabs();

$controls->add_control(
	'tabs',
	[
		'label'       => esc_html__( 'Tabs', 'cryptop' ),
		'type'        => Controls_Manager::REPEATER,
		'show_label'  => true,
		'default'     => [
			[
				'title' => esc_html__( 'Example Tab', 'cryptop' ),
			]
		],
		'fields'      => array_values( $repeater->get_controls() ),
		'title_field' => '{{{first_title}}}',
	]
);




































































?>