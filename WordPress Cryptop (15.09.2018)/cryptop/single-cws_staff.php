<?php
	get_header();
	$pid = get_queried_object_id();

	wp_enqueue_script ('jquery-shortcode-velocity');
	wp_enqueue_script ('jquery-shortcode-velocity-ui');

	global $cws_theme_funcs;
	if ($cws_theme_funcs){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$fixed_header = $cws_theme_funcs->cws_get_meta_option( 'fixed_header' );
		$class = $sb['layout_class'].' '. $sb['sb_class'];
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);
	}

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();
	//--Meta vars

	$page_class = '';
	$page_class .= (isset($sb) ? $sb['sb_class'] : 'page_content');

	printf("<div class='%s'>", $page_class);
	echo (isset($sb['content']) && !empty($sb['content'])) ? $sb['content'] : '';

	//Classes
	$section_class = "cws_staff single";

	?>
	<main>
		<div class="grid_row single_staff">
			<section class="<?php echo esc_attr($section_class) ?>">
				<div class="cws_staff_single_wrapper"> 
					<?php
						$GLOBALS['cws_single_staff_atts'] = array(
							'sb_layout' => $sb['layout_class'],
							'show_meta' => 	array(
								'title', 'deps', 'poss', 'excerpt', 'experience', 'email', 'biography', 'link_button', 'socials'
							),
						);
						while ( have_posts() ) : the_post();
							cws_staff_single_article();
						endwhile;
						wp_reset_postdata();
						unset( $GLOBALS['cws_single_staff_atts'] );
					?>
				</div>
			</section>
		</div>
	</main>
	<?php echo isset($sb['content']) && !empty($sb['content']) ? '</div>' : ''; ?>
</div>

<?php
	get_footer();
?>