<?php
/**
 * Single Product Thumbnails
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-thumbnails.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.2
 */

defined( 'ABSPATH' ) || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if ( ! function_exists( 'wc_get_gallery_image_html' ) ) {
	return;
}

global $post, $product; 

$attachment_ids = $product->get_gallery_image_ids();

if ( $attachment_ids && has_post_thumbnail() ) { 		

	//CWS ADDON
	$thumb_dims = get_option( 'shop_single_image_size' );
 	$retina_thumb_url = '';
	
	foreach ( $attachment_ids as $attachment_id ) {

		$full_size_image = wp_get_attachment_image_src( $attachment_id, 'full' );
		$thumbnail       = wp_get_attachment_image_src( $attachment_id, 'shop_thumbnail' );
		$attributes      = array(
			'title'                   => get_post_field( 'post_title', $attachment_id ),
			'data-caption'            => get_post_field( 'post_excerpt', $attachment_id ),
			'data-src'                => $full_size_image[0],
			'data-large_image'        => $full_size_image[0],
			'data-large_image_width'  => $full_size_image[1],
			'data-large_image_height' => $full_size_image[2],
		);

 		$html  = '<div data-thumb="' . esc_url( $thumbnail[0] ) . '" class="pic woocommerce-product-gallery__image"><a href="' . esc_url( $full_size_image[0] ) . '">';
 		
 		$image_link    = wp_get_attachment_url( $attachment_id );
 		$image = "";
 		$thumb_obj = cws_thumb( $attachment_id, $thumb_dims );
 		$thumb_url = isset( $thumb_obj[0] ) ? esc_url( $thumb_obj[0] ) : "";

 		$html .= apply_filters( "woocommerce_single_product_image_html", "<img src='".esc_url($thumb_url)."' data-no-retina alt" );
 		foreach ( $attributes as $name => $value ) {
 			$html .= " $name=" . '"' . $value . '"';
 		}
 		$html .= "/>"; 	
 		$html .= '</a></div>';
 		//-----CWS ADDON
		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $attachment_id );
	}
}
