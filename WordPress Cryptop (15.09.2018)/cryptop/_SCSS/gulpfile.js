var gulp		= require('gulp'),
	sass		= require('gulp-sass');

gulp.task('scss', function() {
	return setTimeout(function () {
		return gulp.src('app/scss/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(sass())
		.pipe(gulp.dest('../css'))
	}, 500);
});
		// .pipe(gulp.dest('../../../../../cryp-live.my/wp-content/themes/cryptop/css/'))

gulp.task('watch', function() {
	gulp.watch('app/scss/**/*.scss', ['scss']);
});