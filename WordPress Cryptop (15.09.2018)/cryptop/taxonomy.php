<?php
	get_header ();
	global $cws_theme_funcs;
	if(!empty($cws_theme_funcs)){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$class = $sb['layout_class'].' '. $sb['sb_class'];
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);
	}

	$taxonomy = get_query_var( 'taxonomy' );
	$terms = get_query_var( $taxonomy );
?>
<div class="<?php echo (isset($sb) ? $sb['sb_class'] : 'page_content'); ?>">
	<?php
		echo (isset($sb['content']) && !empty($sb['content'])) ? $sb['content'] : '';

	?>
	<main>
		<div class="grid_row">
			<?php
				switch( $taxonomy ) {
					case "cws_portfolio_cat":
							echo cws_portfolio_output(
								array(
									'display_style' 	=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_display_style'],
									'layout' 			=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_layout'],
									'crop_images' 		=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_crop_images'],						
									'pagination_grid' 	=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_pagination_grid'],
									'items_count' => PHP_INT_MAX,

									'extra_query_args'	=> array(
										'post_type' => 'cws_portfolio',
										'post_status' => 'publish',
										'tax_query' => array(
											array(
												'taxonomy'		=> $taxonomy,
												'field'			=> 'slug',
												'terms'			=> $terms
											)
										)
									)
								)
							);
						break;
					case "cws_staff_member_department":
					case "cws_staff_member_position":
						echo cws_staff_output(
							array(
								'layout' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_layout'],
								'show_meta' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_show_meta'],
								'pagination_grid' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_pagination_grid'],
								'chars_count' => $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_chars_count'],
								'items_count' => PHP_INT_MAX,

								'extra_query_args'	=> array(
									'post_type' => 'cws_staff',
									'post_status' => 'publish',
									'tax_query' => array(
										array(
											'taxonomy'		=> $taxonomy,
											'field'			=> 'slug',
											'terms'			=> $terms
										)
									)
								)
							)
						);
					break;		
				}
			?>
		</div>
	</main>
	<?php echo (isset($sb['content']) && !empty($sb['content'])) ? "</div>" : ''; ?>
</div>

<?php

get_footer ();
?>