<?php

	get_header ();

	$pid = get_queried_object_id();
	global $cws_theme_funcs;
	if ($cws_theme_funcs){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$class = $sb['layout_class'].' '. $sb['sb_class'];
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);
	}

	$taxonomy = get_query_var( 'taxonomy' );
	$terms = get_query_var( $taxonomy );

	$post_type = "cws_portfolio";
	$posts_per_page = (int)get_option('posts_per_page');
	$ajax = isset( $_POST['ajax'] ) ? (bool)$_POST['ajax'] : false;
	$paged_var = get_query_var( 'paged' );
	$paged = $ajax && isset( $_POST['paged'] ) ? $_POST['paged'] : ( $paged_var ? $paged_var : 1 );

/*	if ( is_date() ){
		$year = $cws_theme_funcs->cws_get_date_part( 'y' );
		$month = $cws_theme_funcs->cws_get_date_part( 'm' );
		$day = $cws_theme_funcs->cws_get_date_part( 'd' );
		if ( !empty( $year ) ){
			$posts_grid_atts['addl_query_args']['year'] = $year;
		}
		if ( !empty( $month ) ){
			$posts_grid_atts['addl_query_args']['monthnum'] = $month;
		}
		if ( !empty( $day ) ){
			$posts_grid_atts['addl_query_args']['day'] = $day;
		}
	}*/

/*	if ($posts_grid_atts['display_style'] == 'filter' || $posts_grid_atts['display_style'] =='filter_with_ajax'){
		$i = 0; 
		$name_cats = array();
		foreach (get_categories('taxonomy=cws_portfolio_cat') as $key => $value) { 
			$name_cats[$i]['name'] = $value->name;
			$name_cats[$i]['slug'] = $value->slug;
			$i++;
		}
		$vale = '';
		foreach ($name_cats as $k => $v) {
			$vale .= $v['slug'].',';
		}
		$vale = substr($vale, 0, -1);
		$posts_grid_atts['cws_portfolio_cat_terms'] = $vale;
		$posts_grid_atts['tax'] = 'cws_portfolio_cat';			
	}*/

	$args = array(
		'items_per_page' => $posts_per_page,
		'items_count' => PHP_INT_MAX,
		'paged' => $paged,
		'display_style' 	=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_display_style'],
		'layout' 			=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_layout'],
		'crop_images' 		=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_crop_images'],						
		'pagination_grid' 	=> $cws_theme_funcs->cws_get_option( "portfolio_options" )['def_portfolio_pagination_grid'],
	);

	$args['extra_query_args'] = array(
		'post_type' => $post_type,
		'post_status' => 'publish'
	);

	if (!empty($terms)){
		$args['extra_query_args']['tax_query'] = array(
			array(
				'taxonomy'		=> $taxonomy,
				'field'			=> 'slug',
				'terms'			=> $terms
			)
		);
	}
	
	?>
	<div class="<?php echo (isset($sb) ? $sb['sb_class'] : 'page_content'); ?>">
		<?php
			echo (isset($sb['content']) && !empty($sb['content'])) ? $sb['content'] : '';
		?>
		<main>
			<?php if(isset($sb['content']) && !empty($sb['content'])){ 
				echo '<i class="sidebar-tablet-trigger"></i>';
			}

				if ( !$ajax ): // not ajax request
				?>
				<div class="grid_row">
				<?php
				endif;	
					echo cws_portfolio_output($args);
				if ( !$ajax ): // not ajax request
				?>
				</div>
				<?php
				endif;
				?>			
		</main>
		<?php echo (isset($sb['content']) && !empty($sb['content'])) ? "</div>" : ''; ?>
	</div>
<?php 
get_footer(); 

?>