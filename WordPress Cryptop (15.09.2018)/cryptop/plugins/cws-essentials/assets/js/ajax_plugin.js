"use strict";
// ( function ($){
	cws_hooks_init ();
	var wait_load_posts = false;

	cws_advanced_resize_init ();
	jQuery(window).scroll(function (){
		cws_animation_init();
	});

	window.addEventListener( "load", function (){

		// cws_hoverdir_init();
		// cws_gallery_post_carousel_init();
		cws_fancybox_init();
		// cws_vc_shortcode_init_dot();
		// cws_portfolio_footer_height();
		// cws_render_styles();
		// cws_portfolio_fw();
		cws_portfolio_single_carousel_init();
		// cws_classes_single_carousel_init();
		cws_animation_init();
		

	}, false );

/*	jQuery(document).ready(function (){	
		cws_load_events();
	});*/

	//Gallery Init
	function cws_gallery_post_carousel_init( area ){
		var area = area == undefined ? document : area;
		
		jQuery( ".gallery_post_carousel", area ).each(function (){
			debugger;
			var owl = jQuery(this);
			owl.owlCarousel({
				singleItem: true,
				slideSpeed: 300,
				navigation: false,
				pagination: false
			});
			jQuery(this).parent().find(".carousel_nav.next").click(function (){
				owl.trigger('owl.next');
			});
			jQuery(this).parent().find(".carousel_nav.prev").click(function (){
				owl.trigger('owl.prev');
			});
		});
	}

	//Isotope Init
	function cws_isotope_init (wrapper){
		var isotope_wrapper = wrapper.find('.isotope');
		if (wrapper.hasClass('layout-masonry')){
			isotope_wrapper.each(function(item, value){
				cws_isotope_masonry_init(value);
				jQuery( window ).resize(function() {
					cws_isotope_masonry_init(value);
				});
				if ( typeof jQuery.fn.isotope == 'function' ){
					jQuery(this).isotope({
						itemSelector: ".item",
						percentPosition: true,
						layoutMode: 'masonry',
						masonry: {
							columnWidth: '.grid-sizer',
						}
					});
				}		
			});	
		} else {
			if ( typeof jQuery.fn.isotope == 'function' ){

				setTimeout(function(){
					isotope_wrapper.isotope({
						itemSelector: ".item",
					});
				},0);

			}
		}
	}

	//Massonry init
	function cws_isotope_masonry_init(container){
		var container, size, th, col_count, line_count, width;
		container = jQuery(container);
		size = container.find('.grid-sizer');
		width = container.width();
		console.warn(container);
		console.log(width);
		var grid_num = container.parents('.posts_grid').attr('data-col');
		var col_width = width/grid_num;

		container.find('.item').each(function() {
			th = jQuery(this);
			var thHeight = jQuery(this).find('.under_image_portfolio').outerHeight() + 30;
			col_count = th.data('masonry-col'); 
			line_count = th.data('masonry-line'); 
			var col = Math.ceil(col_width*col_count);
			if(container.hasClass('crop')){
				var line = Math.ceil(col_width*line_count);
			} else {
				var paddings = 30 * grid_num;
				var single_col_width = ((width - paddings) / grid_num);
				var single_col_height = ((single_col_width / 16) * 9) + 30;
				var line = single_col_height * line_count;
			}

			if(line / 2 > col_width){
				line = Math.ceil( line + 0.5);
			}

			//If not mobile viewport
			if (jQuery(document).width() >= 768) {
				th.css('width', col + 'px');
				th.css('height',line + 'px');
				if(th.hasClass('under_img')){
					th.css('height',line +  thHeight + 'px');
				}
			} else {
			    th.css('width', '100%');
			    th.css('height','auto');				
			}
		});
	}	

	//Pagination
	function cws_pagination_init(section){
		section = section[0];
		var ajax_pagination, load_more, filter_nav_items, i, section_id, grid, loader, form, data_field, /*paged_field, filter_field,*/ data, request_data, response_data, response_data_str, page_links, page_link;
		var magicLine, magicLine_exists, magicLine_id, magicLine_instance, magicLine_avail, load_more_pg, load_more_pg_old;

		if ( section == undefined ) return;
		grid = section.getElementsByClassName( 'cws_pagination' );
		if ( !grid.length ) return;
		grid = grid[0];
		loader = section.getElementsByClassName( 'cws_loader_holder' );
		loader = loader.length ? loader[0] : null;
		form = section.getElementsByClassName( 'ajax_data_form' );
		if ( !form.length ) return;
		form = form[0];
		data_field = form.getElementsByClassName( 'ajax_data' );
		if ( !data_field.length ) return;
		data_field = data_field[0];
		data = data_field.value;
		data = JSON.parse( data );
		section_id = data['section_id'];
		request_data = response_data = data;

		ajax_pagination = jQuery(section).find('.pagination.ajax');
		if (ajax_pagination.length){
			cws_pagination_ajax({
				'section'		: section,
				'section_id'	: section_id,
				'grid'			: grid,
				'loader'		: loader,
				'form'			: form,
				'data_field'	: data_field,
				'data'			: data
			});
		}

		load_more = jQuery(section).find('.cws_load_more');
		if (load_more.length){
			cws_pagination_load_more({
				'load_on_scroll': (load_more.hasClass('load_on_scroll') ? true : false),
				'section'		: section,
				'section_id'	: section_id,
				'grid'			: grid,
				'loader'		: loader,
				'form'			: form,
				'data_field'	: data_field,
				'data'			: data
			});
		}

		filter_nav_items = jQuery(section).find('.cws_filter_nav .nav_item');
		if (filter_nav_items.length){
	    	cws_filter({
	    		'section'		: section,
	    		'section_id'	: section_id,
	    		'grid'			: grid,
	    		'loader'		: loader,
	    		'form'			: form,
	    		'data_field'	: data_field,
	    		'data'			: data
			});
		}	

	}

	//Pages
	function cws_pagination_ajax ( args ){
		var i, ajax_pagination, section, section_id, section_offset, grid, loader, form, data_field, data, request_data, response_data, pagination;

		section = args['section'];
		section_id = args['section_id'];
		grid = args['grid'];
		loader = args['loader'];
		form = args['form'];
		data_field = args['data_field'];
		data = request_data = response_data = args['data'];
		section_offset = jQuery( section ).offset().top;

		ajax_pagination = jQuery(section).find('.pagination.ajax');
		if ( !ajax_pagination.length ) return;

		//CLICK
		ajax_pagination.find('a').on( 'click', function ( e ){
			if ( wait_load_posts ) return;

			e.preventDefault();
			var el = e.srcElement ? e.srcElement : e.target;
			
			jQuery(loader).addClass('pagination_action active'); //Show loader

			//Position load circle in page links
			var loadCircleInPage = jQuery(this).offset();
			loadCircleInPage.left = loadCircleInPage.left + jQuery(this).outerWidth() / 2 - 20;
			jQuery(loader).find("svg").offset(loadCircleInPage);

			request_data['req_page_url'] = jQuery(el).is('.page-numbers') ? el.href : jQuery(el).parent()[0].href;
			wait_load_posts = true;
			jQuery.post( ajaxurl, {
				'action'		: 'cws_posts_ajax',
				'data'			: request_data
			}, function ( response, status ){
				var section_offset_top, response_container, page_number_field, old_items, new_items, old_ajax_pagination, old_page_links, new_ajax_pagination, new_page_links, img_loader, page_number, response_data_str;
				response_container = document.createElement( "div" );
				response_container.innerHTML = response;

				new_items = jQuery( ".item", response_container );
				new_items.addClass( "hidden" );
				new_ajax_pagination = jQuery('.pagination.ajax',response_container);
				new_page_links = new_ajax_pagination.find('.page_links');
				page_number_field = jQuery('.cws_page_number',response_container);
				page_number = page_number_field ? page_number_field.val() : "";
				section_offset_top = jQuery( section ).offset().top;
				old_items = jQuery( ".item", grid );
				old_ajax_pagination = jQuery('.pagination.ajax', section);
				old_page_links = old_ajax_pagination.find('.page_links');
				jQuery( grid ).isotope( 'remove', old_items );
				
				//Scroll to section top
				if ( window.scrollY > section_offset_top ){
					jQuery( 'html, body' ).stop().animate({
						scrollTop : section_offset_top
					}, 300);
				}

				jQuery( grid ).append( new_items );
				wait_load_posts = false;
				img_loader = imagesLoaded ( grid );
				img_loader.on( "always", function (){
					//PORTFOLIO
					if (request_data['post_type'] == 'cws_portfolio'){
						jQuery(".cws_portfolio_wrapper.layout-masonry .isotope").each(function(item, value){
							cws_isotope_masonry_init(value);
							jQuery( window ).resize(function() {
								cws_isotope_masonry_init(value);
							});	
						});

						cws_hoverdir_init();
					}

					cws_owl_carousel_init( grid );
					cws_fancybox_init ( grid );

					new_items.removeClass( "hidden" );
					jQuery( grid ).isotope( 'appended', new_items );
					jQuery( grid ).isotope( 'layout' );
				    if (Retina.isRetina()) {
			        	jQuery(window.retina.root).trigger( "load" );
				    }

				    old_page_links.addClass('hiding animated fadeOut');
				    setTimeout( function (){
				    	old_page_links.remove();
				    	new_page_links.addClass('animated fadeIn');

				    	if(new_page_links){
				    		old_ajax_pagination.prepend(new_page_links);
				    	}
				    	
				    	cws_pagination_ajax({
				    		'section'		: section,
				    		'section_id'	: section_id,
				    		'grid'			: grid,
				    		'loader'		: loader,
				    		'form'			: form,
				    		'data_field'	: data_field,
				    		'data'			: data
			    		});

				    }, 300);

					//Page
					response_data['page'] = page_number.length ? page_number : 1;
					response_data_str = JSON.stringify( response_data );
					data_field.value = response_data_str;

					cws_do_action( "cws_dynamic_content", new Array({
						"section" : section
					}));

					jQuery(loader).removeClass('pagination_action active'); //Hide loader

					//Scroll to section top
					if ( window.scrollY > section_offset ){
						jQuery( 'html, body' ).stop().animate({
							scrollTop : section_offset
						}, 300);
					}

				});
			});
		});
	}

	//Load More
	function cws_pagination_load_more ( args ){
		var load_more, load_on_scroll, section, section_id, grid, loader, form, data_field, section_offset, data, request_data, response_data, response_data_str;

		load_on_scroll = args['load_on_scroll'];
		section = args['section'];
		section_id = args['section_id'];
		grid = args['grid'];
		loader = args['loader'];
		form = args['form'];
		data_field = args['data_field'];
		data = request_data = response_data = args['data'];
		section_offset = jQuery( section ).offset().top;

		load_more = jQuery( section ).find('.cws_load_more');
		if ( !load_more.length ) return;

		//Scroll Handler
		var scrollHandler = function(event){
			var st = jQuery(this).scrollTop();
			var wh = jQuery(window).height();
			var scroll_bottom = st + wh;
			var section_offset_bottom = jQuery( section ).offset().top + jQuery( section ).outerHeight();
			if (scroll_bottom >= section_offset_bottom){
				jQuery(load_more).trigger("click");
			}
		}

		if (load_on_scroll) jQuery(window).scroll(scrollHandler); //Add scroll event

		//CLICK
		load_more.on( 'click', function ( e ){
			var page, next_page, max_paged;
			e.preventDefault();
			if ( wait_load_posts ) return;

			jQuery(loader).addClass('load_more_action active'); //Show loader
			if (load_on_scroll) jQuery(loader).addClass('load_more_scroll'); //Show loader scroll

			//Circle loader
			if (load_on_scroll){
				jQuery(loader).find("svg").css({
					left: '50%',
					bottom: 0
				});
			} else {
				var btnLoadMorePos = jQuery(this).offset();
				btnLoadMorePos.left = btnLoadMorePos.left + jQuery(this).outerWidth() / 2 - 20;
				btnLoadMorePos.top = btnLoadMorePos.top + 10;
				jQuery(loader).find("svg").offset(btnLoadMorePos);
			}
			//--Circle loader

			//Load more page
			page = data['page'];
			max_paged = data['max_paged'];
			next_page = page + 1;
			request_data['page'] = next_page;
			if ( next_page >= max_paged ){
				jQuery(window).off("scroll", scrollHandler); //Remove scroll event
				jQuery(load_more).fadeOut( { duration : 300, complete : function (){
					jQuery(load_more).remove();
				}});
			}
			wait_load_posts = true;
			jQuery.post( ajaxurl, {
				'action'		: 'cws_posts_ajax',
				'data'			: request_data
			}, function ( response, status ){
				var response_container, new_items, img_loader;
				response_container = document.createElement( "div" );
				response_container.innerHTML = response;

				new_items = jQuery( ".item", response_container );
				new_items.addClass( "hidden" );

				jQuery( grid ).append( new_items );
				wait_load_posts = false;
				img_loader = imagesLoaded ( grid );
				img_loader.on( "always", function (){
					//PORTFOLIO
					if (request_data['post_type'] == 'cws_portfolio'){
						jQuery(".cws_portfolio_wrapper.layout-masonry .isotope").each(function(item, value){
							cws_isotope_masonry_init(value);
							jQuery( window ).resize(function() {
								cws_isotope_masonry_init(value);
							});	
						});

						cws_hoverdir_init();
					}

					cws_owl_carousel_init( grid );
					cws_fancybox_init ( grid );

					new_items.removeClass( "hidden" );

					jQuery( grid ).isotope( 'appended', new_items );
					jQuery( grid ).isotope( 'layout' );
					if (Retina.isRetina()){
						jQuery(window.retina.root).trigger( "load" );
					}

					//Page
					response_data['page'] = next_page;
					response_data_str = JSON.stringify( response_data );
					data_field.value = response_data_str;

					cws_do_action( "cws_dynamic_content", new Array({
						"section" : section
					}));
				
					jQuery(loader).removeClass('load_more_action active'); //Hide loader
					if (load_on_scroll) jQuery(loader).removeClass('load_more_scroll'); //Hide loader scroll
				});
			});
		});
	}

	//Filter
	function cws_filter ( args ){
		var i, ajax_pagination, section, section_id, section_offset, grid, loader, form, data_field, data, request_data, response_data, pagination, page_links, page_link, response_data_str;
		var magicLine, magicLine_exists, magicLine_id, magicLine_instance, magicLine_avail;

		section = args['section'];
		section = jQuery(section);
		section_id = args['section_id'];
		grid = args['grid'];
		loader = args['loader'];
		form = args['form'];
		data_field = args['data_field'];
		data = request_data = response_data = args['data'];

		//magicLine
		magicLine 			= section.find('.magicline');
		magicLine_exists 	= magicLine.length;
		magicLine_id 		= magicLine_exists ? magicLine[0].id : "";
		magicLine_instance 	= window[magicLine_id + "_instance"] !== undefined ? window[magicLine_id + "_instance"] : null;
		magicLine_avail 	= magicLine_instance !== null;

		//CLICK
		jQuery( '.cws_filter_nav .nav_item', section ).on( 'click', function ( e ){
			if ( wait_load_posts ) return;
			e.preventDefault();
			e.stopPropagation();
			var filter = jQuery(this);
			var filterPos = filter.offset();
			var filter_val = filter.data("nav-val");
			var filter_nav = filter.data("nav-filter");

			request_data['current_filter_val']	= filter_val;
			request_data['page']				= 1;
			filter.closest('.posts_grid').find('a.nav_item').removeClass( "active" )
			filter.addClass( "active" );
			filter.closest('.dot').trigger('click');

			if ( magicLine_avail ){
				magicLine_instance.init_base();
			}

			//CSS Filter
			if(filter.closest('.posts_grid').hasClass('css_filter')){
				var grid_css = filter.closest('.posts_grid').find('.isotope');
				grid_css.isotope({ filter: filter_nav, animationEngine: 'jquery' });
			} else {
				//AJAX Filter
				jQuery(loader).addClass('filter_action active'); //Show loader
				filterPos.left = filterPos.left + filter.width() / 2;
				filterPos.top = filterPos.top - 20;
				jQuery(loader).find("svg").offset(filterPos);
				jQuery(filter).find('.txt_title').css({'opacity' : 0});
				wait_load_posts = true;
				jQuery.post( ajaxurl, {
					'action'		: 'cws_posts_ajax',
					'data'			: request_data
				}, function ( response, status ){
					var response_container, old_items, new_items, old_ajax_pagination, new_ajax_pagination, new_load_more, old_load_more, img_loader;
					response_container = document.createElement( "div" );
					response_container.innerHTML = response;

					new_items = jQuery( ".item", response_container );
					new_items.addClass( "hidden" );
					jQuery(filter).find('.txt_title').css({'opacity' : 1});
					new_ajax_pagination = jQuery('.pagination.ajax', response_container);
					new_load_more = jQuery('.cws_load_more', response_container);
					old_items = jQuery( ".item", grid );
					old_ajax_pagination = jQuery('.pagination.ajax', section);
					old_load_more = jQuery('.cws_load_more', section);

					jQuery( grid ).isotope( 'remove', old_items );
					jQuery( grid ).append( new_items );
					wait_load_posts = false;
					img_loader = imagesLoaded ( grid );
					img_loader.on( "always", function (){
						//PORTFOLIO
						if (request_data['post_type'] == 'cws_portfolio'){
							jQuery(".cws_portfolio_wrapper.layout-masonry .isotope").each(function(item, value){
								cws_isotope_masonry_init(value);
								jQuery( window ).resize(function() {
									cws_isotope_masonry_init(value);
								});	
							});

							cws_hoverdir_init();
						}

						cws_owl_carousel_init( grid );
						cws_fancybox_init ( grid );			

						new_items.removeClass( "hidden" );
						if (jQuery(grid).data('isotope') != undefined){
							jQuery( grid ).isotope( 'appended', new_items );
							jQuery( grid ).isotope( 'layout' );	
						} else {
							jQuery( grid ).append(new_items);
						}

					    if (Retina.isRetina()) {
				        	jQuery(window.retina.root).trigger( "load" );
					    }

					    if ( old_ajax_pagination.length || old_load_more.length ){
						    if (old_ajax_pagination.length) old_ajax_pagination.addClass('hiding animated fadeOut');
						    if (old_load_more.length) old_load_more.addClass('hiding animated fadeOut');

						    setTimeout( function (){
						    	if (old_ajax_pagination.length) old_ajax_pagination.remove();
						    	if (old_load_more.length) old_load_more.remove();

						    	if ( new_ajax_pagination.length || new_load_more.length ){
						    		//Ajax
									if (new_ajax_pagination.length){
										new_ajax_pagination.addClass('animated fadeIn');
										new_ajax_pagination.insertBefore(section.find(form));
									} 

									//Load more
									if (new_load_more.length){
										new_load_more.addClass('animated fadeIn');
										new_load_more.insertBefore(section.find(form));
									}

							    	if(new_ajax_pagination.length){
										cws_pagination_ajax({
											'section'		: section,
											'section_id'	: section_id,
											'grid'			: grid,
											'loader'		: loader,
											'form'			: form,
											'data_field'	: data_field,
											'data'			: data
										});							    		
							    	}
							    	if(new_load_more.length){
										cws_pagination_load_more({
											'load_on_scroll': (jQuery(new_load_more).hasClass('load_on_scroll') ? true : false),
											'section'		: section,
											'section_id'	: section_id,
											'grid'			: grid,
											'loader'		: loader,
											'form'			: form,
											'data_field'	: data_field,
											'data'			: data
										});	
							    	}
				    		
						    	}						    	
						    }, 300);
					    }
					    else{
					    	if ( new_ajax_pagination.length || new_load_more.length ){
									//Ajax
									if (new_ajax_pagination.length){
										new_ajax_pagination.addClass('animated fadeIn');
										new_ajax_pagination.insertBefore(section.find(form));
									} 

									//Load more
									if (new_load_more.length){
										new_load_more.addClass('animated fadeIn');
										new_load_more.insertBefore(section.find(form));
									}

						    	if(new_ajax_pagination.length){
									cws_pagination_ajax({
										'section'		: section,
										'section_id'	: section_id,
										'grid'			: grid,
										'loader'		: loader,
										'form'			: form,
										'data_field'	: data_field,
										'data'			: data
									});							    		
						    	}
						    	if(new_load_more.length){
									cws_pagination_load_more({
										'load_on_scroll': (jQuery(new_load_more).hasClass('load_on_scroll') ? true : false),
										'section'		: section,
										'section_id'	: section_id,
										'grid'			: grid,
										'loader'		: loader,
										'form'			: form,
										'data_field'	: data_field,
										'data'			: data
									});
						    	}

							}	
					    }

					    //Page & Filter
						response_data['current_filter_val']	= filter_val;
						response_data['page']				= 1;
						response_data_str = JSON.stringify( response_data );
						data_field.value = response_data_str;

						cws_do_action( "cws_dynamic_content", new Array({
							"section" : section
						}));
				
						jQuery(loader).removeClass('filter_action active'); //Hide loader

					});
				});
			}
			
		});
	}

/*	function cws_vc_shortcode_init_dot(){
		jQuery(".dot").click(function(){
			var cur=jQuery(this);
			var curM = jQuery(this).find('.circle').position().left;
			var cirM = parseInt(jQuery(this).find('.circle').css('marginLeft'));
			var dest=cur.position().left + curM + cirM;
			var t=0.6;
			TweenMax.to(jQuery(this).siblings(".cws_post_select_dots"),t,{x:dest,ease:Back.easeOut})
		});
		
		jQuery('.dots').each(function(){
			jQuery(this).find(".dot:eq(0)").trigger("click");
		});
	}*/

	//Hover init
	function cws_hoverdir_init(){

		jQuery(".portfolio_item.hoverdir").each(function() {
			jQuery(this).hoverdir({hoverElem: '.cws_portfolio_content_wrap'})
		})
		jQuery(".portfolio_item.hover3d").hover3d({
			selector: ".item_content",
			shine: !1,
			perspective: 1e3,
			invert: !0
		})
	}

	(function (factory) {
		if (typeof define === 'function' && define.amd) {
			define(['jquery'], factory);
		} else if (typeof exports !== 'undefined') {
			module.exports = factory(require('jquery'));
		} else {
			factory(jQuery);
		}
	})(function ($) {
		function Hoverdir(element, options) {
			this.$el = $(element);
			this.options = $.extend(true, {}, this.defaults, options);
			this.isVisible = false;
			this.$hoverElem = this.$el.find(this.options.hoverElem);
			this.transitionProp = 'all ' + this.options.speed + 'ms ' + this.options.easing;
			this.support = this._supportsTransitions();
			this._loadEvents();
		}
		Hoverdir.prototype = {
			defaults: {
				speed: 300,
				easing: 'ease',
				hoverDelay: 0,
				inverse: false,
				hoverElem: 'div'
			},
			constructor: Hoverdir,
			_supportsTransitions: function () {
				if (typeof Modernizr !== 'undefined') {
					return Modernizr.csstransitions;
				} else {
					var b = document.body || document.documentElement,
						s = b.style,
						p = 'transition';
					if (typeof s[p] === 'string') {
						return true;
					}
					var v = ['Moz', 'webkit', 'Webkit', 'Khtml', 'O', 'ms'];
					p = p.charAt(0).toUpperCase() + p.substr(1);

					for (var i = 0; i < v.length; i++) {
						if (typeof s[v[i] + p] === 'string') {
							return true;
						}
					}

					return false;
				}
			},
			_loadEvents: function () {
				this.$el.on('mouseenter.hoverdir mouseleave.hoverdir', $.proxy(function (event) {
					this.direction = this._getDir({x: event.pageX, y: event.pageY});
					if (event.type === 'mouseenter') {
						this._showHover();
					}
					else {
						this._hideHover();
					}
				}, this));
			},
			_showHover: function () {
				var styleCSS = this._getStyle(this.direction);
				if (this.support) {
					this.$hoverElem.css('transition', '');
				}
				this.$hoverElem.hide().css(styleCSS.from);
				clearTimeout(this.tmhover);

				this.tmhover = setTimeout($.proxy(function () {
					this.$hoverElem.show(0, $.proxy(function () {
						if (this.support) {
							this.$hoverElem.css('transition', this.transitionProp);
						}
						this._applyAnimation(styleCSS.to);

					}, this));
				}, this), this.options.hoverDelay);

				this.isVisible = true;
			},
			_hideHover: function () {
				var styleCSS = this._getStyle(this.direction);
				if (this.support) {
					this.$hoverElem.css('transition', this.transitionProp);
				}
				clearTimeout(this.tmhover);
				this._applyAnimation(styleCSS.from);
				this.isVisible = false;
			},
			_getDir: function (coordinates) {
				var w = this.$el.width(),
					h = this.$el.height(),
					x = (coordinates.x - this.$el.offset().left - (w / 2)) * (w > h ? (h / w) : 1),
					y = (coordinates.y - this.$el.offset().top - (h / 2)) * (h > w ? (w / h) : 1),
					direction = Math.round((((Math.atan2(y, x) * (180 / Math.PI)) + 180) / 90) + 3) % 4;
				return direction;
			},
			_getStyle: function (direction) {
				var fromStyle, toStyle,
					slideFromTop = {'left': '0', 'top': '-100%'},
					slideFromBottom = {'left': '0', 'top': '100%'},
					slideFromLeft = {'left': '-100%', 'top': '0'},
					slideFromRight = {'left': '100%', 'top': '0'},
					slideTop = {'top': '0'},
					slideLeft = {'left': '0'};
				switch (direction) {
					case 0:
					case 'top':
						fromStyle = !this.options.inverse ? slideFromTop : slideFromBottom;
						toStyle = slideTop;
						break;
					case 1:
					case 'right':
						fromStyle = !this.options.inverse ? slideFromRight : slideFromLeft;
						toStyle = slideLeft;
						break;
					case 2:
					case 'bottom':
						fromStyle = !this.options.inverse ? slideFromBottom : slideFromTop;
						toStyle = slideTop;
						break;
					case 3:
					case 'left':
						fromStyle = !this.options.inverse ? slideFromLeft : slideFromRight;
						toStyle = slideLeft;
						break;
				}

				return {from: fromStyle, to: toStyle};
			},
			_applyAnimation: function (styleCSS) {
				$.fn.applyStyle = this.support ? $.fn.css : $.fn.animate;
				this.$hoverElem.stop().applyStyle(styleCSS, $.extend(true, [], {duration: this.options.speed}));
			},
			show: function (direction) {
				this.$el.off('mouseenter.hoverdir mouseleave.hoverdir');
				if (!this.isVisible) {
					this.direction = direction || 'top';
					this._showHover();
				}
			},
			hide: function (direction) {
				this.rebuild();
				if (this.isVisible) {
					this.direction = direction || 'bottom';
					this._hideHover();
				}
			},
			setOptions: function (options) {
				this.options = $.extend(true, {}, this.defaults, this.options, options);
			},
			destroy: function () {
				this.$el.off('mouseenter.hoverdir mouseleave.hoverdir');
				this.$el.data('hoverdir', null);
			},
			rebuild: function (options) {
				if (typeof options === 'object') {
					this.setOptions(options);
				}
				this._loadEvents();
			}
		};
		jQuery.fn.hoverdir = function (option, parameter) {
			return this.each(function () {
				var data = jQuery(this).data('hoverdir');
				var options = typeof option === 'object' && option;

				// Initialize hoverdir.
				if (!data) {
					data = new Hoverdir(this, options);
					jQuery(this).data('hoverdir', data);
				}
				if (typeof option === 'string') {
					data[option](parameter);

					if (option === 'destroy') {
						jQuery(this).data('hoverdir', false);
					}
				}
			});
		};
		jQuery.fn.hoverdir.Constructor = Hoverdir;
	});

	(function (factory) {
		if (typeof define === 'function' && define.amd) {
			define(['jquery'], factory);
		} else if (typeof exports !== 'undefined') {
			module.exports = factory(require('jquery'));
		} else {
			factory(jQuery);
		}
	})(function($){
		$.fn.hover3d = function(options){
			var settings = $.extend({
				selector      : null,
				perspective   : 1000,
				sensitivity   : 20,
				invert        : false,
				shine         : false,
				hoverInClass  : "hover-in",
				hoverOutClass : "hover-out",
				hoverClass    : "hover-3d"
			}, options);
			return this.each(function(){
				var $this = $(this),
					$card = $this.find(settings.selector),
					currentX = 0,
					currentY = 0;
				if( settings.shine ){
					$card.append('<div class="shine"></div>');
				}
				var $shine = $(this).find(".shine");
				$this.css({
					perspective: settings.perspective+"px",
					transformStyle: "preserve-3d"
				});
				$card.css({
					perspective: settings.perspective+"px",
					transformStyle: "preserve-3d",
				});
				$shine.css({
					position  : "absolute",
					top       : 0,
					left      : 0,
					bottom    : 0,
					right     : 0,
					transform : 'translateZ(1px)',
					"z-index" : 9
				});
				function enter(event){
					$card.addClass(settings.hoverInClass+" "+settings.hoverClass);
					currentX = currentY = 0;
					setTimeout(function(){
						$card.removeClass(settings.hoverInClass);
					}, 1000);
				}
				function move(event){
					var w      = $card.innerWidth(),
						h      = $card.innerHeight(),
						currentX = Math.round(event.pageX - $card.offset().left),
						currentY = Math.round(event.pageY - $card.offset().top),
						ax 	   = settings.invert ?  ( w / 2 - currentX)/settings.sensitivity : -( w / 2 - currentX)/settings.sensitivity,
						ay     = settings.invert ? -( h / 2 - currentY)/settings.sensitivity :  ( h / 2 - currentY)/settings.sensitivity,
						dx     = currentX - w / 2,
						dy     = currentY - h / 2,
						theta  = Math.atan2(dy, dx),
						angle  = theta * 180 / Math.PI - 90;
					if (angle < 0) {
						angle  = angle + 360;
					}
					$card.css({
						perspective    : settings.perspective+"px",
						transformStyle : "preserve-3d",
						transform      : "rotateY("+ax+"deg) rotateX("+ay+"deg)"
					});
					$shine.css('background', 'linear-gradient(' + angle + 'deg, rgba(255,255,255,' + event.offsetY / h * .5 + ') 0%,rgba(255,255,255,0) 80%)');
				}
				function leave(){
					$card.addClass(settings.hoverOutClass+" "+settings.hoverClass);
					$card.css({
						perspective    : settings.perspective+"px",
						transformStyle : "preserve-3d",
						transform      : "rotateX(0) rotateY(0)"
					});
					setTimeout( function(){
						$card.removeClass(settings.hoverOutClass+" "+settings.hoverClass);
						currentX = currentY = 0;
					}, 1000 );
				}
				$this.on( "mouseenter", function(){
					return enter();
				});
				$this.on( "mousemove", function(event){
					return move(event);
				});
				$this.on( "mouseleave", function(){
					return leave();
				});
			});
		};
	})

	//Appear items animation
	function cws_animation_init() {
		var el = jQuery('section.appear_style article.item, .anim-item');
		if(typeof jQuery.Velocity === 'function' && jQuery.Velocity){
			jQuery.Velocity.RegisterEffect.packagedEffects = {
				"callout.bounce": {
					defaultDuration: 550,
					calls: [[{
						translateY: -30
					}, .25], [{
						translateY: 0
					}, .125], [{
						translateY: -15
					}, .125], [{
						translateY: 0
					}, .25]]
				},
				"callout.shake": {
					defaultDuration: 800,
					calls: [[{
						translateX: -11
					}, .125], [{
						translateX: 11
					}, .125], [{
						translateX: -11
					}, .125], [{
						translateX: 11
					}, .125], [{
						translateX: -11
					}, .125], [{
						translateX: 11
					}, .125], [{
						translateX: -11
					}, .125], [{
						translateX: 0
					}, .125]]
				},
				"callout.flash": {
					defaultDuration: 1100,
					calls: [[{
						opacity: [0, "easeInOutQuad", 1]
					}, .25], [{
						opacity: [1, "easeInOutQuad"]
					}, .25], [{
						opacity: [0, "easeInOutQuad"]
					}, .25], [{
						opacity: [1, "easeInOutQuad"]
					}, .25]]
				},
				"callout.pulse": {
					defaultDuration: 825,
					calls: [[{
						scaleX: 1.1,
						scaleY: 1.1
					}, .5, {
						easing: "easeInExpo"
					}], [{
						scaleX: 1,
						scaleY: 1
					}, .5]]
				},
				"callout.swing": {
					defaultDuration: 950,
					calls: [[{
						rotateZ: 15
					}, .2], [{
						rotateZ: -10
					}, .2], [{
						rotateZ: 5
					}, .2], [{
						rotateZ: -5
					}, .2], [{
						rotateZ: 0
					}, .2]]
				},
				"callout.tada": {
					defaultDuration: 1e3,
					calls: [[{
						scaleX: .9,
						scaleY: .9,
						rotateZ: -3
					}, .1], [{
						scaleX: 1.1,
						scaleY: 1.1,
						rotateZ: 3
					}, .1], [{
						scaleX: 1.1,
						scaleY: 1.1,
						rotateZ: -3
					}, .1], ["reverse", .125], ["reverse", .125], ["reverse", .125], ["reverse", .125], ["reverse", .125], [{
						scaleX: 1,
						scaleY: 1,
						rotateZ: 0
					}, .2]]
				},
				"transition.fadeIn": {
					defaultDuration: 600,
					calls: [[{
						opacity: [1, 0]
					}]]
				},
				"transition.flipXIn": {
					defaultDuration: 600,
					calls: [[{
						opacity: [1, 0],
						transformPerspective: [800, 800],
						rotateY: [0, -30]
					}]],
					reset: {
						transformPerspective: 0
					}
				},
				"transition.flipYIn": {
					defaultDuration: 600,
					calls: [[{
						opacity: [1, 0],
						transformPerspective: [1500, 1500],
						rotateX: [0, -30]
					}]],
					reset: {
						transformPerspective: 0
					}
				},
				"transition.shrinkIn": {
					defaultDuration: 600,
					calls: [[{
						opacity: [1, 0],
						transformOriginX: ["50%", "50%"],
						transformOriginY: ["50%", "50%"],
						scaleX: [1, 1.15],
						scaleY: [1, 1.15],
						translateZ: 0
					}]]
				},
				"transition.expandIn": {
					defaultDuration: 600,
					calls: [[{
						opacity: [1, 0],
						transformOriginX: ["50%", "50%"],
						transformOriginY: ["50%", "50%"],
						scaleX: [1, .9],
						scaleY: [1, .9],
						translateZ: 0
					}]]
				},
				"transition.grow": {
					defaultDuration: 600,
					calls: [[{
						opacity: [1, 0],
						transformOriginX: ["50%", "50%"],
						transformOriginY: ["50%", "50%"],
						scaleX: [1, .2],
						scaleY: [1, .2],
						translateZ: 0
					}]]
				},
				"transition.slideUpBigIn": {
					defaultDuration: 850,
					calls: [[{
						opacity: [1, 0],
						translateY: [0, 75],
						translateZ: 0
					}]]
				},
				"transition.slideDownBigIn": {
					defaultDuration: 850,
					calls: [[{
						opacity: [1, 0],
						translateY: [0, -75],
						translateZ: 0
					}]]
				},
				"transition.slideLeftBigIn": {
					defaultDuration: 800,
					calls: [[{
						opacity: [1, 0],
						translateX: [0, -75],
						translateZ: 0
					}]]
				},
				"transition.slideRightBigIn": {
					defaultDuration: 800,
					calls: [[{
						opacity: [1, 0],
						translateX: [0, 75],
						translateZ: 0
					}]]
				},
				"transition.perspectiveUpIn": {
					defaultDuration: 800,
					calls: [[{
						opacity: [1, 0],
						transformPerspective: [3e3, 3e3],
						transformOriginX: [0, 0],
						transformOriginY: ["100%", "100%"],
						rotateX: [0, -70]
					}]],
					reset: {
						transformPerspective: 0,
						transformOriginX: "50%",
						transformOriginY: "50%"
					}
				},
				"transition.perspectiveDownIn": {
					defaultDuration: 800,
					calls: [[{
						opacity: [1, 0],
						transformPerspective: [3e3, 3e3],
						transformOriginX: [0, 0],
						transformOriginY: [0, 0],
						rotateX: [0, 70]
					}]],
					reset: {
						transformPerspective: 0,
						transformOriginX: "50%",
						transformOriginY: "50%"
					}
				},
				"transition.perspectiveLeftIn": {
					defaultDuration: 800,
					calls: [[{
						opacity: [1, 0],
						transformPerspective: [2e3, 2e3],
						transformOriginX: [0, 0],
						transformOriginY: [0, 0],
						rotateY: [0, -70]
					}]],
					reset: {
						transformPerspective: 0,
						transformOriginX: "50%",
						transformOriginY: "50%"
					}
				},
				"transition.perspectiveRightIn": {
					defaultDuration: 800,
					calls: [[{
						opacity: [1, 0],
						transformPerspective: [2e3, 2e3],
						transformOriginX: ["100%", "100%"],
						transformOriginY: [0, 0],
						rotateY: [0, 70]
					}]],
					reset: {
						transformPerspective: 0,
						transformOriginX: "50%",
						transformOriginY: "50%"
					}
				}
			};

			for (var k in jQuery.Velocity.RegisterEffect.packagedEffects)
				jQuery.Velocity.RegisterEffect(k, jQuery.Velocity.RegisterEffect.packagedEffects[k]);
			el.each(function() {
				if (jQuery(this).is_visible() && !jQuery(this).hasClass('anim_done')){
					jQuery(this).addClass('anim_done');
					var anim = jQuery(this).data('item-anim');
					jQuery(this).velocity(anim);
				}	
			});			
		}
	}

/*	function cws_vc_shortcode_dots_resize(){
		jQuery(".dots").each(function(){
			var cur = jQuery(this).find('a.active').parent();
			if(jQuery(this).find('a.active').parent().find('.circle').length > 0){
				var curM = jQuery(this).find('a.active').parent().find('.circle').position().left;
				var cirM = parseInt(jQuery(this).find('a.active').parent().find('.circle').css('marginLeft'));
				var dest=cur.position().left + curM + cirM;
				var t=0.6;
				TweenMax.to(jQuery(this).find(".cws_post_select_dots"),t,{x:dest,ease:Back.easeOut})					
			}
		
		});
	}*/

	function cws_portfolio_footer_height(){
		var fh, fh2, footer;
		footer = jQuery('#footer');
		fh = footer.outerHeight();
		fh2 = fh * 1.5
		if (!jQuery('section.posts_grid').hasClass('posts_grid_showcase')) {
			if ( ( jQuery(window).width()>992) && footer.hasClass('footer-fixed')){
				footer.addClass('fixed');
				jQuery('body').css('margin-bottom',' ' + fh + 'px');
			} else{
				jQuery('body').css('margin-bottom','0px');
				footer.removeClass('fixed');
			}
			if ( ( jQuery(window).height()<fh2) ){
				jQuery('body').css('margin-bottom','0px');
				footer.removeClass('fixed');
			}
		}
	};

/*	function cws_render_styles(){
		var css = '';
		var head = document.head || document.getElementsByTagName('head')[0];
		var style = document.createElement('style');  
			jQuery('.render_styles').each(function(index, el) {
				var data = '';
				var data = JSON.parse(jQuery(el).data('style'));
				jQuery(el).removeAttr('data-style');
				css += data;
			});

		style.type = 'text/css';
		if (style.styleSheet){
			style.styleSheet.cssText = css;
		} else {
			style.appendChild(document.createTextNode(css));
		}
		head.appendChild(style);
	}*/

	//Showcase Init
	function cws_portfolio_showcase(section) {
		var wrapper = section.find('.showcase_wrapper');
		var showcase_args = wrapper.data('showcase-args');
		var items = wrapper.find('.portfolio_item');
		var items_count = items.length;
		var part_size = (100 - showcase_args.active_size) / (items_count - 1);
		var flex_grow = (100 / part_size) - (items_count - 1);

		items.each(function (i, el){
			jQuery(el).hover(
				//in
				function(event) {
					jQuery(this).css('flex-grow', flex_grow);
				},
				//out
				function(event) {
					jQuery(this).css('flex-grow', '1');
				}
			);	
		});
	}

	//Single Portfolio Carousel Init
	function cws_portfolio_single_carousel_init (){
		jQuery( ".cws_portfolio.single.related" ).each( function (){
			var parent = jQuery(this);
			var grid = jQuery( ".cws_portfolio_single_wrapper", parent );
			var ajax_data_input = jQuery( "#cws_portfolio_single_ajax_data", parent );
			var carousel_nav = jQuery( ".ajax_carousel_nav", parent );
			if ( !carousel_nav.length ) return;
			jQuery( ".prev, .next", carousel_nav ).on( "click", function (){
				var el = jQuery( this );
				var action = el.hasClass( "prev" ) ? "prev" : "next";
				var ajax_data = JSON.parse( ajax_data_input.val() );
				var current = ajax_data['current'];
				var all = ajax_data['related_ids'];
				var next_ind;
				var next;
				for ( var i=0; i<all.length; i++ ){
					if ( all[i] == current ){
						if ( action == "prev" ){
							if ( i <= 0 ){
								next_ind = all.length-1;
							}
							else {
								next_ind = i-1;
							}
						}
						else {
							if ( i >= all.length-1 ){
								next_ind = 0;
							}
							else {
								next_ind = i+1
							}
						}
						break;
					}
				}
				if ( typeof next_ind != "number" || typeof all[next_ind] == undefined ) return;
				next = all[next_ind];
				debugger;
				jQuery('html, body').animate({scrollTop: jQuery("#main").offset().top - 50}, 800);
				jQuery.post( ajaxurl, {
					'action' : 'cws_portfolio_single_ajax',
					'data' : {
						'initial_id' : ajax_data['initial'],
						'requested_id' : next
					}
				}, function ( data, status ){
					debugger;
					var full_width, animation_config, new_post, old_post, new_media_full, old_media_full, hiding_class, showing_class, delay, img_loader;					
					
					//Elements
					old_post = jQuery( ".item" , parent );	
					new_post = jQuery( ".item", jQuery( data ) );
					old_media_full = jQuery('.page_content > main > .full_width_media');
					new_media_full = jQuery('.full_width_media', jQuery( data ) );

					full_width = new_media_full.length != 0;
					if (full_width){
						jQuery('.page_content').addClass('full_width_featured');
					} else {
						jQuery('.page_content').removeClass('full_width_featured');
					}
					
					ajax_data['current'] = next;
					ajax_data_input.attr( "value", JSON.stringify( ajax_data ) );
					
					animation_config = {
						'prev' : {
							'in' : 'fadeInLeft',
							'out' : 'fadeOutRight'
						},
						'next' : {
							'in' : 'fadeInRight',
							'out' : 'fadeOutLeft'
						},
						'delay' : 150
					};
					
					hiding_class = "animated " + animation_config[action]['out'];
					showing_class = "animated " + animation_config[action]['in'];
					delay = animation_config['delay'];

					//Hide new post & media
					new_post.css( "display", "none" );
					new_media_full.css( "display", "none" );
					
					//Append post & media
					grid.append( new_post );
					jQuery('.page_content > main').prepend( new_media_full );

					img_loader = imagesLoaded( grid );
					
					img_loader.on( 'always', function (){
						//Init new post
						cws_owl_carousel_init();
						cws_fancybox_init();
						cws_hoverdir_init();

						old_post.addClass( hiding_class );
						old_media_full.addClass( hiding_class );
						setTimeout( function (){
							//Remove old post & media
							old_post.remove();
							old_media_full.remove();

							//Show new post
							new_post.addClass( showing_class );
							new_post.css( "display", "block" );

							//Show new  media
							new_media_full.addClass( showing_class );
							new_media_full.css( "display", "block" );

						    if (Retina.isRetina()) {
					        	jQuery(window.retina.root).trigger( "load" );
						    }
						   
						}, delay );
					});
				});
			});
		});
	}

/*	function cws_classes_single_carousel_init (){
		jQuery( ".cws_classes.single.related" ).each( function (){
			var parent = jQuery(this);
			var grid = jQuery( ".cws_classes_items", parent );
			var ajax_data_input = jQuery( "#cws_classes_single_ajax_data", parent );
			var carousel_nav = jQuery( ".carousel_nav_panel", parent );
			if ( !carousel_nav.length ) return;
			jQuery( ".prev,.next", carousel_nav ).on( "click", function (){
				var el = jQuery( this );
				var action = el.hasClass( "prev" ) ? "prev" : "next";
				var ajax_data = JSON.parse( ajax_data_input.val() );
				var current = ajax_data['current'];
				var all = ajax_data['related_ids'];
				var next_ind;
				var next;
				for ( var i=0; i<all.length; i++ ){
					if ( all[i] == current ){
						if ( action == "prev" ){
							if ( i <= 0 ){
								next_ind = all.length-1;
							}
							else{
								next_ind = i-1;
							}
						}
						else{
							if ( i >= all.length-1 ){
								next_ind = 0;
							}
							else{
								next_ind = i+1
							}
						}
						break;
					}
				}
				if ( typeof next_ind != "number" || typeof all[next_ind] == undefined ) return;
				next = all[next_ind];
				jQuery.post( ajaxurl, {
					'action' : 'cws_classes_single',
					'data' : {
						'initial_id' : ajax_data['initial'],
						'requested_id' : next
					}
				}, function ( data, status ){
					jQuery('html,body').animate({scrollTop: jQuery("#main").offset().top - 50}, 800);
					var animation_config, old_el, new_el, hiding_class, showing_class, delay, img_loader;
					ajax_data['current'] = next;
					ajax_data_input.attr( "value", JSON.stringify( ajax_data ) );
					animation_config = {
						'prev' : {
							'in' : 'fadeInLeft',
							'out' : 'fadeOutRight'
						},
						'next' : {
							'in' : 'fadeInRight',
							'out' : 'fadeOutLeft'
						},
						'delay' : 150
					};
					old_el = jQuery( ".cws_classes_items .item" , parent );
					new_el = jQuery( ".item", jQuery( data ) );
					hiding_class = "animated " + animation_config[action]['out'];
					showing_class = "animated " + animation_config[action]['in'];
					delay = animation_config['delay'];
					new_el.css( "display", "none" );
					grid.append( new_el );
					img_loader = imagesLoaded( grid );
					img_loader.on( 'always', function (){
						cws_owl_carousel_init( );
						cws_fancybox_init ( );
						cws_hoverdir_init();
						old_el.addClass( hiding_class );
						setTimeout( function (){
							old_el.remove();
							new_el.addClass( showing_class );
							new_el.css( "display", "block" );

						    if (Retina.isRetina()) {
					        	jQuery(window.retina.root).trigger( "load" );
						    }
						}, delay );
					});
				});
			});
		});
	}*/

/*	function cws_load_events(){
		var i, section, loader;
		var sections = document.getElementsByClassName( 'cws_wrapper_events' );
		for ( i = 0; i < sections.length; i++ ){
			section = sections[i];		
			
			loader = section.getElementsByClassName( 'cws_loader_holder' );
			loader = loader.length ? loader[0] : null;		

			if ( loader != null ){
				if ( !cws_has_class( loader, "active" ) ){
					cws_add_class( loader, "active" );
				}
			}
			
			if(window.cws_vc_sh_atts){
				jQuery.ajax({
					type : "post", 
					async: true,
					dataType : "text",
					url : ajaxurl.url,
					data : {
						action: "cws_vc_shortcode_tribe_events_posts_grid",		
						nonce: cws_vc_sh.ajax_nonce,
						data: JSON.parse(cws_vc_sh_atts.cws_events) 
					},
					error: function(resp) {
						console.log(resp);
					},
					success: function(resp) {
						if ( loader != null ){
							if ( cws_has_class( loader, "active" ) ){
								cws_remove_class( loader, "active" );
							}
						}
						if (resp.length) {
							var o_resp = JSON.parse(resp);
							jQuery(section).append(o_resp.result);	
							// setTimeout(cws_vc_shortcode_isotope_init_plugin, 500);
						}
					},
				});			
			}
		}
	}*/

	window.addEventListener( 'resize', function (){
		// cws_vc_shortcode_dots_resize();
		// cws_portfolio_fw();
	}, false )

// }(jQuery));