<?php

//Get defaults values for Staff
function cws_staff_defaults( $extra_vars = array() ){
	global $cws_theme_funcs;

	$layout = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_layout'] : '1';
	$pagination_grid = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_pagination_grid'] : 'standard';
	$show_meta = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_show_meta'] : array('title', 'deps', 'excerpt', 'experience', 'email', 'biography', 'link_button');
	$def_chars_count = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( "staff_options" )['def_staff_chars_count'] : '200';
	$def_chars_count = isset( $def_chars_count ) && is_numeric( $def_chars_count ) ? $def_chars_count : '200';	

	$defaults = array(
		'title'						=> 'Staff',
		'title_aligning'			=> 'left',
		'title_filter_row'			=> 'default',
		'display_style'				=> 'grid',
		'layout'					=> $layout,

		//Slider (Swiper)
		'columns_count'				=> '1',
		'slides_in_columns'			=> '1',
		'slides_view'				=> 'column',
		'slides_to_scroll'			=> '1',
		'slider_direction'			=> 'horizontal',
		'auto_height'				=> '',
		'navigation'				=> 'both',
		'pagination_style'			=> 'bullets',
		'dynamic_bullets'			=> '',
		'keyboard_control'			=> '',
		'mousewheel_control'		=> '',
		'item_center'				=> '',
		'spacing_slides'			=> '',
		'free_mode'					=> '',
		'slide_effects'				=> 'slide',
		'autoplay'					=> '',
		'autoplay_speed'			=> '',
		'pause_on_hover'			=> 'yes',
		'parallax'					=> '',
		'loop'						=> '',
		'animation_speed'			=> '',
		//--Slider (Swiper)

		'filter_by'					=> '',

		'items_count'				=> get_option( 'posts_per_page' ),
		'items_per_page'			=> get_option( 'posts_per_page' ),
		'pagination_grid'			=> $pagination_grid,
		'pagination_aligning'		=> 'center',
		'chars_count'				=> $def_chars_count,
		'show_meta'					=> $show_meta,
		'grid_columns_spacings'		=> array(
										'unit' => 'px',
										'size' => 50,
									),
		'grid_rows_spacings'		=> array(
										'unit' => 'px',
										'size' => 50,
									),
		'view_style'				=> 'big',
		'appear_style'				=> 'none',
		'image_link'				=> '',
		'title_color'				=> '#000',
		'title_typography'			=> '',
		'category_color'			=> '#000',
		'category_typography'		=> '',

		//Service params
		'extra_query_args'			=> array(),
	);

	if (!empty($extra_vars)){
		$defaults = array_merge($defaults, $extra_vars);
	}

	return $defaults;
}

//Fill atts from function to function
function cws_staff_fill_atts( $atts = array() ){
	global $cws_theme_funcs;
	extract( $atts );

	$page_id = get_the_id();
	$page_meta = get_post_meta( $page_id, 'cws_mb_post' );
	$page_meta = isset( $page_meta[0] ) ? $page_meta[0] : array();
	$items_count = !empty( $items_count ) ? (int)$items_count : PHP_INT_MAX;

	if ($display_style == 'filter'){
		$items_per_page = $items_count;
	}

	$def_layout = $cws_theme_funcs ? $cws_theme_funcs->cws_get_option( 'def_staff_layout' ) : '1';
	$def_layout = isset( $def_layout ) ? $def_layout : "";
	$layout = ($layout == "def") ? $def_layout : $layout;

	$sb = $cws_theme_funcs ? $cws_theme_funcs->cws_render_sidebars( $page_id ) : '';
	$sb_layout = isset( $sb['layout_class'] ) ? $sb['layout_class'] : '';

	$fill_atts = array_merge($atts,
		array(
			//Service params
			'page_id'			=> $page_id,
			'page_meta'			=> $page_meta,
			'sb_layout'			=> $sb_layout,

			//Override atts
			'items_count'		=> $items_count,
			'items_per_page'	=> $items_per_page,
			'layout'			=> $layout
		)
	);

	//Set GLOBALS vars
	$GLOBALS['cws_staff_atts'] = $fill_atts;

	return $fill_atts;
}

function cws_staff_output($atts = array()){
	global $cws_theme_funcs;
	$out = "";

	$extra_vars = array();
	if (!empty($atts['filter_by'])){
		$extra_vars[$atts['filter_by']] = '';
	}

	$defaults = cws_staff_defaults($extra_vars); //Get defaults
	$atts = shortcode_atts( $defaults, $atts );
	$fill_atts = cws_staff_fill_atts($atts); //Fill atts
	extract( $fill_atts );

	$post_type = "cws_staff";
	$section_id = uniqid( 'cws_staff_grid_' );
	$paged = get_query_var( 'paged' );
	$home_paged = get_query_var('page');
	if(isset($home_paged) && !empty($home_paged)){
		$paged = empty( $home_paged ) ? 1 : get_query_var('page');
	}
	else{
		$paged = empty( $paged ) ? 1 : $paged;
	}

	$not_in = (1 == $paged) ? array() : get_option( 'sticky_posts' );
	$query_args = array(
		'post_type'			=> $post_type,
		'post_status'		=> 'publish',
		'post__not_in'		=> $not_in
	);
	if ( in_array( $display_style, array( 'grid', 'filter', 'filter_with_ajax' ) ) ){
		$query_args['posts_per_page']	= $items_per_page;
		$query_args['paged']			= $paged;
	}
	else {
		$query_args['nopaging']			= true;
		$query_args['posts_per_page']	= -1;
	}

	//Filter by
	$tax = $fill_atts['filter_by'];
	if (!empty($tax)){
		$filter_terms = $fill_atts[$tax];
	}

	if ( !empty( $filter_terms ) ){
		$query_args['tax_query'] = array(
			array(
				'taxonomy'		=> $tax,
				'field'			=> 'slug',
				'terms'			=> $filter_terms
			)
		);
	}
	//--Filter by

	//Get All terms
	$all_terms = array();
	$rewrite_slug = cws_get_slug('staff');
	$get_terms_by = empty($tax) ? 'cws_staff_member_department' : $tax;
	$get_terms = get_terms( $get_terms_by );
	foreach ($get_terms as $key => $value) {
		$all_terms[] = $value->slug;
	}
	//--Get All terms

	//Terms show in Filters bar
	if (empty($filter_terms)){
		$terms = $all_terms;
	} else {
		$terms = $filter_terms;
	}
	//--Terms show in Filters bar

	$query_args['orderby'] 	= "menu_order date title";
	$query_args['order']	= "ASC";

	$query_args = array_merge( $query_args, $extra_query_args );
	$q = new WP_Query( $query_args );
	$found_posts = $q->found_posts;

	$requested_posts = $found_posts > $items_count ? $items_count : $found_posts;
	$max_paged = $found_posts > $items_count ? ceil( $items_count / $items_per_page ) : ceil( $found_posts / $items_per_page );

	$is_carousel = $display_style == 'carousel' && $requested_posts > $layout;
	$is_filter = in_array( $display_style, array( 'filter', 'filter_with_ajax' ) ) && !empty( $terms ) ? true : false;
	$use_pagination = in_array( $display_style, array( 'grid', 'filter', 'filter_with_ajax' ) ) && $max_paged > 1;
	$dynamic_content = $is_filter || $use_pagination;

	//CAROUSEL
	$data_attr = swiper_carousel($fill_atts, $is_carousel);
	//--CAROUSEL

	$section_class = cws_class([
		'cws_staff',
		'posts_grid',
		esc_attr($display_style),
		($display_style != "carousel" ? "isotope_init" : ""),
		($display_style != "carousel" ? "col-".esc_attr($layout) : ""),
		(in_array( $display_style, array( 'filter' )) ? "css_filter" : ""),
		($dynamic_content ? "dynamic_content" : ""),
		($appear_style !== 'none' ? "appear_style" : "")
	]);

	$wrapper_class = cws_class([
		'cws_staff_wrapper',
		(!$is_carousel ? "layout-isotope" : "")
	]);

	$container_class = cws_class([
		$dynamic_content ? "cws_pagination" : '',
		($is_carousel ? "cws_staff_carousel" : "cws_staff_grid"),
		(( in_array( $layout, array( "2", "3", "4", "5" ) ) || $dynamic_content ) ? 'isotope' : "" ),
	]);

	ob_start ();

		echo "<section id='".esc_attr($section_id)."' class='".(!empty($section_class) ? $section_class : "")."'>";
			$filter_vals = array();
			if ( $is_filter && count( $terms ) > 1 ){

				echo ($title_filter_row == 'default') ? "<div class='container'>" : '';
				foreach ( $terms as $term ) {
					if ( empty( $term ) ) continue;
					$term_obj = get_term_by( 'slug', $term, 'cws_staff_member_department' );
					if ( empty( $term_obj ) ) continue;

					$term_name = $term_obj->name;
					$filter_vals[$term_obj->slug] = $term_name;
				}

				if ( $filter_vals > 1 ){
					wp_enqueue_script( 'tweenmax' );
					echo posts_filters_nav ( $filter_vals, $title);
				}

				echo $title_filter_row == 'default' ? "</div>" : '';
			}
			else {
				echo !empty( $title ) ? "<h2 class='widgettitle'>" . esc_html( $title ). "</h2>" : "";
			}		
			echo "<div class='".(!empty($wrapper_class) ? $wrapper_class : "")."'>";
				echo "<div class='".(!empty($container_class) ? $container_class : "")."'" . (!empty($data_attr) ? $data_attr : "") . ">";
					if (!$is_carousel) {
						echo "<div class='grid-sizer'></div>"; //For Isotope grid
					}

					if ($is_carousel){
						echo "<div class='swiper-container'>";
							echo "<div class='swiper-wrapper'>";
					}				
					
					cws_staff_posts($q);

					if ($is_carousel){
							echo "</div>"; //swiper-wrapper

							if ($navigation == 'both' || $navigation == 'pagintaion'){
								if ($pagination_style == 'scrollbar'){
									echo "<div class='swiper-scrollbar'></div>";
								} else {
									echo "<div class='swiper-pagination'></div>";
								}
							}

						echo "</div>"; //swiper-container

						if ($navigation == 'both' || $navigation == 'arrows'){
							echo "<div class='swiper-button-prev'></div>";
							echo "<div class='swiper-button-next'></div>";
						}
					}	
					unset($GLOBALS['cws_staff_atts']);
				echo "</div>";
				if ( $dynamic_content ){
					cws_loader_html();
				}
			echo "</div>";
			if ( $use_pagination ){
				if ( $pagination_grid == 'load_more' ){
					echo cws_load_more ();
				}
				else if($pagination_grid == 'load_on_scroll'){
					echo cws_load_more (true);
				}				
				else if($pagination_grid == 'ajax'){
					echo cws_pagination($paged, $max_paged, true);
				}
				else if($pagination_grid == 'standard'){
					echo cws_pagination($paged, $max_paged, false);
				}
			}
			if ( $dynamic_content ){

				//AJAX data
				$ajax_data = array_merge($fill_atts,
					array(
						//Service params
						'post_type'			=> 'cws_staff',
						'section_id'		=> $section_id,
						'max_paged'			=> $max_paged,
						'page'				=> $paged,
						'filter_vals'		=> $filter_vals,
					)
				);

				$ajax_data_str = json_encode( $ajax_data );
				echo "<form id='".esc_attr($section_id)."_data' class='ajax_data_form cws_staff_ajax_data_form'>";
					echo "<input type='hidden' id='".esc_attr($section_id)."_ajax_data' class='ajax_data cws_staff_ajax_data' name='".esc_attr($section_id)."_ajax_data' value='".$ajax_data_str."' />";
				echo "</form>";				
			}
		echo "</section>";

	$out = ob_get_clean();
	return $out;
}

function cws_staff_posts ( $q = null ){
	if ( !isset( $q ) ) return;
	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	extract( $grid_atts );

	$paged = $q->query_vars['paged'];
	if ( $paged == 0 && $items_count < $q->post_count ){
		$post_count = $items_count;
	}
	else
	{
		$ppp = $q->query_vars['posts_per_page'];
		$posts_left = $items_count - ( $paged - 1 ) * $ppp;
		$post_count = $posts_left < $ppp ? $posts_left : $q->post_count;
	}
	if ( $q->have_posts() ):
		ob_start();
		while( $q->have_posts() && $q->current_post < $post_count - 1 ):
			$q->the_post();
			cws_staff_article ();
		endwhile;
		wp_reset_postdata();
		ob_end_flush();
	endif;
}

function cws_staff_article (){
	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	extract( $grid_atts );

	$is_carousel = $display_style == 'carousel';
	$uniq_pid = uniqid( "staff_post_" );

	//CSS Filter by class
	$name_slug = array();
	if($display_style == 'filter'){
		foreach (get_the_terms($pid, 'cws_staff_member_department') as $key => $value) {
			$name_slug[] = $value->slug ;
		}
		$name_slug[] = 'tax_staff';
	}
	//--CSS Filter by class

	$article_class = cws_class(array_merge([
		'item',
		'staff_item',
		($is_carousel ? 'swiper-slide' : ''),
		(( ($display_style == 'grid' || $display_style == 'filter' ||  $display_style == 'filter_with_ajax' ) && !empty($hover_animation) ) ? $hover_animation : '')
	], $name_slug ));

	if ($display_style == 'grid' || $display_style == 'filter' ||  $display_style == 'filter_with_ajax') {
		$data_anim = ($appear_style !== 'none') ? "data-item-anim='".esc_attr($appear_style)."'" : "";
	}

	echo "<article id='".esc_attr($uniq_pid)."' class='".(!empty($article_class) ? $article_class : "")."' $data_anim>";
		echo "<div class='item_content'>";
			cws_staff_media();
			if ($view_style == 'big'){
				echo "<div class='cws_staff_container'>";
					echo "<div class='hover-effect'></div>";
					echo "<div class='cws_staff_description big_staff'>";
						cws_staff_title();
						cws_staff_terms();
						cws_staff_content();
						cws_staff_meta();
						cws_staff_social();
						cws_staff_read_more();
					echo "</div>";
				echo "</div>";			
			} else if ($view_style == 'small') {
				echo "<div class='cws_staff_description small_staff'>";
					cws_staff_title();
					cws_staff_terms();
					cws_staff_content();
					cws_staff_meta();
					cws_staff_social();
					cws_staff_read_more();
				echo "</div>";
			}
		echo "</div>";
	echo "</article>";
}

function cws_staff_media (){
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$pid = get_the_ID();
	$permalink = get_the_permalink( $pid );

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_media' : '';

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$is_clickable = isset( $post_meta['is_clickable'] ) ? $post_meta['is_clickable']: false;
	//--Meta vars

	//Real image dimensions
	$thumbnail_props = has_post_thumbnail( get_the_ID() ) ? wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ),'full') : array();
	$thumbnail = !empty( $thumbnail_props ) ? $thumbnail_props[0] : '';
	$real_thumbnail_dims = array();
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[1] ) ) $real_thumbnail_dims['width'] = $thumbnail_props[1];
	if ( !empty( $thumbnail_props ) && isset( $thumbnail_props[2] ) ) $real_thumbnail_dims['height'] = $thumbnail_props[2];
	//Real image dimensions

	$thumbnail_dims = cws_staff_thumb_dims($real_thumbnail_dims);
	$thumb_obj = cws_thumb( get_post_thumbnail_id(), $thumbnail_dims, false );
	$thumb_url = isset($thumb_obj[0]) ? $thumb_obj[0] : "";
	$retina_thumb = isset($thumb_obj[3]) ? $thumb_obj[3] : false;

	echo "<div class='cws_staff_media post_media".esc_attr($single_class)."'>";
		echo "<div class='pic'>";
			echo $is_clickable && !$single ? "<a href='".esc_url($permalink)."' class='staff_link'>" : "";
				if ( $retina_thumb ) {
					echo "<img src='".esc_url($thumb_url)."' data-at2x='".esc_url($retina_thumb)."' alt />";
				} else {
					echo "<img src='".esc_url($thumb_url)."' data-no-retina alt />";
				}
			echo $is_clickable && !$single ? "</a>" : "";
		echo "</div>";
	echo "</div>";
}

function cws_staff_title(){
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_title' : '';

	//If you need to separate (styles) first name, second name
	/*$fword_boundary = strpos( $title, " " );
	$name = $another = "";
	if ( $fword_boundary !== false ){
		$name = substr( $title, 0, strpos( $title, " " ) );
		$another = substr( $title, $fword_boundary );
		echo "<span class='name'>$name</span>$another";
	}*/
	if ( in_array( 'title', $show_meta ) ){
		$title = get_the_title();
		$permalink = get_the_permalink();

		if ($single){
			$out = $title;
		} else {
			$out = "<a href='".esc_url($permalink)."'>" . $title . "</a>";
		}

		echo !empty( $out  ) ?	"<h3 class='cws_staff_title post_title".esc_attr($single_class)."'>".$out."</h3>" : "";	
	}
}

function cws_staff_terms(){
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_terms' : '';

	$out = '';
	if ( in_array( 'deps', $show_meta ) ){
		$deps = cws_post_term_links_str( 'cws_staff_member_department', ', ' );
		if (!empty($deps)){
			$out .= "<div class='cws_staff_member_department'>";
				$out .= $deps;
			$out .= "</div>";
		}
	}

	if ( in_array( 'poss', $show_meta ) ){
		$poss = cws_post_term_links_str( 'cws_staff_member_position', ', ' );
		if (!empty($poss)){
			$out .= "<div class='cws_staff_member_position'>";
				$out .= $poss;
			$out .= "</div>";
		}
	}	

	if (!empty($out)){
		echo "<div class='cws_staff_terms post_terms".esc_attr($single_class)."'>";
			echo sprintf("%s", $out);
		echo "</div>";
	}
}

function cws_staff_content(){
	global $post;

	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_content' : '';

	$out = "";
	if ( in_array( 'excerpt', $show_meta ) ){
		if ($single){
			$out = apply_filters( 'the_content', $post->post_content );
			// $out = get_post_field('post_content', get_the_id()); //Alternative get field value
		} else {
			$chars_count = !empty($chars_count) ? $chars_count : 0;
			$out = !empty( $post->post_excerpt ) ? $post->post_excerpt : $post->post_content;
			$out = trim( preg_replace( "/[\s]{2,}/", " ", strip_shortcodes( strip_tags( $out ) ) ) );
			$out = wptexturize( $out );
			if (!empty($chars_count)){
				$out = substr( $out, 0, $chars_count );
			}		
		}
		echo !empty( $out ) ? "<div class='cws_staff_content post_content".esc_attr($single_class)."'>$out</div>" : "";
	}
}

function cws_staff_meta(){
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_meta' : '';

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();	

	$experience = isset( $post_meta['experience'] ) ? $post_meta['experience']: "";
	$email = isset( $post_meta['email'] ) ? $post_meta['email']: "";
	$biography = isset( $post_meta['biography'] ) ? $post_meta['biography']: "";
	//--Meta vars
	
	$out = '';
	if (in_array( 'experience', $show_meta ) && !empty($experience)){
		$out .= "<div class='experience'><span>".esc_html__('Experience', 'cryptop').":</span><span>".esc_html($experience)."</span></div>";
	}	

	if (in_array( 'email', $show_meta ) && !empty($email)){
		$out .= "<div class='email'><span>".esc_html__('Email', 'cryptop').":</span><a href='mailto:".esc_html($email)."'>".esc_html($email)."</a></div>";
	}

	if (in_array( 'biography', $show_meta ) && !empty($biography)){
		$out .= "<div class='biography'><span>".esc_html__('Biography', 'cryptop').":</span><p>".esc_html($biography)."</p></div>";
	}

	if (!empty($out)){
		echo "<div class='cws_staff_meta post_meta".esc_attr($single_class)."'>";
			echo sprintf("%s", $out);
		echo "</div>";
	}
}

function cws_staff_social(){
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_social' : '';

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();	

	$social_group = isset( $post_meta['social_group'] ) ? $post_meta['social_group']: array();
	//--Meta vars	

	$icons = "";
	foreach ( $social_group as $social ) {
		$title = isset( $social['title'] ) ? $social['title'] : "";
		$icon = isset( $social['icon'] ) ? $social['icon'] : "";
		$url = isset( $social['url'] ) ? $social['url'] : "";
		if ( !empty( $icon ) && !empty( $url ) ){
			$icons .= "<a href='".esc_url($url)."' target='_blank'" . ( !empty( $title ) ? " title='".esc_attr($title)."'" : "" ) . "><i class='".esc_attr($icon)."'></i></a>";
		}
	}
	if ( !empty( $icons ) ){
		echo "<div class='cws_staff_post_social post_social".esc_attr($single_class)."'>";
			echo sprintf("%s", $icons);
		echo "</div>";
	}
}

function cws_staff_read_more(){
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$single_class = $single ? ' single_read_more' : '';
	$permalink = get_the_permalink();

	//Meta vars
	$post_meta = get_post_meta( get_the_ID(), 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();	

	$read_more_btn = isset( $post_meta['read_more_btn'] ) ? $post_meta['read_more_btn']: false;
	$read_more_title = isset( $post_meta['read_more_title'] ) ? $post_meta['read_more_title']: "";
	//--Meta vars

	if(in_array( 'link_button', $show_meta ) && $read_more_btn ){
		echo !empty($add_btn) && !empty($title_button) ?  "<a class='cws_staff_read_more post_read_more".esc_attr($single_class)."' href='".esc_attr($permalink)."'>".esc_html($title_button)."</a>" : "";
	}
}

function cws_staff_thumb_dims ( $real_dims = array() ) {	
	$page_id = isset($GLOBALS['cws_staff_atts']['page_id']) ? $GLOBALS['cws_staff_atts']['page_id'] : get_the_id();

	//Atts	
	$grid_atts = isset( $GLOBALS['cws_staff_atts'] ) ? $GLOBALS['cws_staff_atts'] : array();
	$single_atts = isset( $GLOBALS['cws_single_staff_atts'] ) ? $GLOBALS['cws_single_staff_atts'] : array();

	$resolution = 1170;

	$single = cws_is_single($page_id);
	if ( $single ){
		extract( $single_atts );
	}
	if(!$single || (is_category() || is_tag() || is_archive())){
		extract( $grid_atts );
	}

	$dims = array( 'width' => null, 'height' => null , 'crop' => true);
	if ($single){
		if ( ( empty( $real_dims ) || ( isset( $real_dims['width'] ) && $real_dims['width'] > 1170 ) ) ) {
			$dims['width']	= 1170;
		}
	}
	else{
		$dims['width'] = ($resolution / (int) $layout);
	}

	return $dims;
}

// =====================Portfolio Single=====================
function cws_staff_single_article ($pid = null){
	$pid = !empty($pid) ? $pid : get_the_id ();

	//Meta vars
	$post_meta = get_post_meta( $pid, 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();
	//--Meta vars

	//Classes
	$article_class = cws_class([
		'item',
		'staff_item',
		'post_single',
	]);
	//--Classes

	echo "<article id='cws_staff_post_".esc_attr($pid)."' class='" . esc_attr($article_class) . "'>";
		echo "<div class='item_content'>";
			cws_staff_media();
			echo "<div class='cws_staff_container'>";
				cws_staff_title();
				echo "<div class='cws_staff_description anim-item' data-item-anim='transition.slideLeftBigIn'>";
					cws_staff_terms();
					cws_staff_content();
					cws_staff_meta();
					cws_staff_social();
					cws_staff_read_more();

					cws_page_links();
				echo "</div>";
			echo "</div>";
		echo "</div>";
	echo "</article>";	
}
// =====================//Portfolio Single=====================

?>