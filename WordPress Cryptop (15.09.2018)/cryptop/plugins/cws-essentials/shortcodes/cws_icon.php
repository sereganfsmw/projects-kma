<?php
global $cws_theme_funcs;

$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

//=======================RENDER TYPE=======================
$out = '';

//-----------PHP-----------
if ($type == 'php'){

	$out .= "<div class='icons-wrapper'>";
		foreach ($settings['icons_list'] as $key => $repeator) {

			//Get icon
			if ($repeator['icon_lib'] == 'fontawesome'){
				$icon = $repeator['icon_fa'];
			} elseif ($repeator['icon_lib'] == 'flaticons') {
				$icon = $repeator['icon_fl'];
			} elseif ($repeator['icon_lib'] == 'svg') {
				$icon = $repeator['icon_svg'];
			}

			$item_id = 'elementor-repeater-item-'.$repeator['_id'];
			$out .= "<div class='icon-wrapper ".esc_attr($item_id)."'>";

				$tag = !empty( $repeator['link']['url'] ) ? "a" : ($repeator['icon_lib'] == 'svg' ? "span" : "i");
				$classes = "cws_icon" . ($repeator['icon_lib'] != 'svg' ? " ".$icon : '');
				$tag_atts = $tag == 'a' ? "$tag href='".esc_url($repeator['link']['url'])."'" : $tag;

				$tag_atts .= " class='$classes'";
				$tag_atts .= !empty( $repeator['link']['url'] ) && $repeator['link']['is_external'] == 'on' ? " target='_blank'" : "";
				$tag_atts .= !empty( $repeator['title'] ) ? " title='".esc_attr($repeator['title'])."'" : "";

				$out .= "<div class='icon-container position-".esc_attr($settings['aligning'])." shape-".esc_attr($repeator['icon_shape'])."'>";
					$out .= "<$tag_atts>";
					if($repeator['icon_lib'] == 'svg'){
						$svg_icon = json_decode(str_replace("``", "\"", $icon), true);
						$out .= function_exists('cwssvg_shortcode') ? cwssvg_shortcode($svg_icon) : "";			
					}
					$out .= "</$tag>";
				$out .= "</div>";

			$out .= "</div>";
		}
	$out .= "</div>";

//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
else if ($type == 'js') {

}

echo sprintf("%s", $out);