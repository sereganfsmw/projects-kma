<?php

$out = "";

if (!empty($settings['url'])){
	$out .= apply_filters( "the_content", "[embed" . ( !empty( $settings['width'] ) ? " width='".esc_attr($settings['width'])."'" : "" ) . ( !empty( $settings['height'] ) ? " height='".esc_attr($settings['height'])."'" : "" ) . "]" . esc_url($settings['url']) . "[/embed]" );
}

echo sprintf("%s", $out);