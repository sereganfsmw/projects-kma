<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//Settings
$title = 					($type == 'php') ? $settings['title'] : 					'{{{settings.title}}}';
$progress = 				($type == 'php') ? $settings['progress'] : 					'{{{settings.progress}}}';
$progress_size = 			($type == 'php') ? $settings['progress']['size'] : 			'{{{settings.progress.size}}}';
$duration =					($type == 'php') ? $settings['duration'] : 					'{{{settings.duration}}}';
$circle_width =				($type == 'php') ? $settings['circle_width'] : 				'{{{settings.circle_width}}}';
$custom_title_color =		($type == 'php') ? $settings['custom_title_color'] : 		'{{{settings.custom_title_color}}}';
$custom_bar_color_start =	($type == 'php') ? $settings['custom_bar_color_start'] :	'{{{settings.custom_bar_color_start}}}';
$custom_bar_color_end =		($type == 'php') ? $settings['custom_bar_color_end'] : 		'{{{settings.custom_bar_color_end}}}';
$custom_line_color =		($type == 'php') ? $settings['custom_line_color'] : 		'{{{settings.custom_line_color}}}';
$custom_percents_color =	($type == 'php') ? $settings['custom_percents_color'] : 	'{{{settings.custom_percents_color}}}';

//=======================RENDER TYPE=======================
$js_settings = '';
//-----------PHP-----------
if ($type == 'php'){


	$render->add_render_attribute( 'module', [
		'id' => esc_attr(uniqid( "cws_pie_char_" )),
		'class' => ['cws_pie_char', 'cws_module'],
	] );

	$render->add_render_attribute( 'pie_char_circle', [
		'class' => 'cws_pie_char_circle',
		'data-width' => (int)$circle_width,
		'data-duration' => (int)$duration,
		'data-percent' => (int)$progress_size,
		'data-barColorStart' => $custom_bar_color_start,
		'data-barColorEnd' => $custom_bar_color_end,
		'data-lineColor' => $custom_line_color
	] );


//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
elseif ($type == 'js') {


	$js_settings = "
		<#

		view.addRenderAttribute( 'module', {
			'id': '".esc_attr(uniqid( "cws_pie_char_" ))."',
			'class': [ 'cws_pie_char', 'cws_module' ]
		} );

		view.addRenderAttribute( 'pie_char_circle', {
			'class': [ 'cws_pie_char_circle' ],
			'data-width': ".js_var($circle_width).",
			'data-duration': ".js_var($duration).",
			'data-percent': ".js_var($progress_size).",
			'data-barColorStart': ".js_var($custom_bar_color_start).",
			'data-barColorEnd': ".js_var($custom_bar_color_end).",
			'data-lineColor': ".js_var($custom_line_color)."
		} );

		#>
	";


}
//-----------/JS (BACKBONE)-----------

//Render attr
$attr_module = 			($type == 'php') ? $render->get_render_attribute_string( 'module' ) : 			"{{{ view.getRenderAttributeString( 'module' ) }}}";
$attr_pie_char_circle =	($type == 'php') ? $render->get_render_attribute_string( 'pie_char_circle' ) :	"{{{ view.getRenderAttributeString( 'pie_char_circle' ) }}}";

$module_id = uniqid( "cws_pie_char_" );

$out = "";

	$out .= $js_settings;

	$out .= "<div ".$attr_wrapper.">";
		$out .= "<div class='cws_pie_char_wrapper'>";
			$out .= "<div ".$attr_pie_char_circle.">";
				$out .= "<span class='cws_pie_char_value'></span>";
			$out .= "</div>";
			if ($type == 'php'){
				if ( ! empty( $title ) ) {	
					$out .= "<span class='cws_pie_char_title'>".$title."</span>";
				}
			} elseif ($type == 'js') {
				$out .= "<# if ( ".js_var($title)." ) { #>";
					$out .= "<span class='cws_pie_char_title'>".$title."</span>";
				$out .= "<# } #>";
			}
		$out .= "</div>";

	$out .= "</div>";

echo sprintf("%s", $out);