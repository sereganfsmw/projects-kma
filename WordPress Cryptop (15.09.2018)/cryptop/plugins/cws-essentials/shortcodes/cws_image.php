<?php
namespace Elementor;

global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//=======================RENDER TYPE=======================
$out = '';
ob_start();
//-----------PHP-----------
if ($type == 'php'){

	if ( empty( $settings['image']['url'] ) ) {
		return;
	}

	$has_caption = ! empty( $settings['caption'] );

	$render->add_render_attribute( 'wrapper', 'class', 'elementor-image' );

	if ( ! empty( $settings['shape'] ) ) {
		$render->add_render_attribute( 'wrapper', 'class', 'elementor-image-shape-' . $settings['shape'] );
	}

	$link = $render->get_link_url( $settings );

	if ( $link ) {
		$render->add_render_attribute( 'link', [
			'href' => $link['url'],
			'data-elementor-open-lightbox' => $settings['open_lightbox'],
		] );

		if ( Plugin::$instance->editor->is_edit_mode() ) {
			$render->add_render_attribute( 'link', [
				'class' => 'elementor-clickable',
			] );
		}

		if ( ! empty( $link['is_external'] ) ) {
			$render->add_render_attribute( 'link', 'target', '_blank' );
		}

		if ( ! empty( $link['nofollow'] ) ) {
			$render->add_render_attribute( 'link', 'rel', 'nofollow' );
		}
	} ?>
	<div <?php echo $render->get_render_attribute_string( 'wrapper' ); ?>>
		<div class="elementor-image-wrapper">
			<div class="elementor-image-overlay"></div>
				<?php if ( $has_caption ) : ?>
					<figure class="wp-caption">
				<?php endif; ?>
				<?php if ( $link ) : ?>
						<a <?php echo $render->get_render_attribute_string( 'link' ); ?>>
				<?php endif; ?>
					<?php echo Group_Control_Image_Size::get_attachment_image_html( $settings ); ?>
				<?php if ( $link ) : ?>
						</a>
				<?php endif; ?>
				<?php if ( $has_caption ) : ?>
						<figcaption class="widget-image-caption wp-caption-text"><?php echo $settings['caption']; ?></figcaption>
				<?php endif; ?>
				<?php if ( $has_caption ) : ?>
					</figure>
				<?php endif; ?>			
		</div>
	</div>
	<?php

//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
else if ($type == 'js') {

	?>
	<#
	if ( settings.image.url ) {
		var image = {
			id: settings.image.id,
			url: settings.image.url,
			size: settings.image_size,
			dimension: settings.image_custom_dimension,
			model: view.getEditModel()
		};

		var image_url = elementor.imagesManager.getImageUrl( image );

		if ( ! image_url ) {
			return;
		}

		var link_url;

		if ( 'custom' === settings.link_to ) {
			link_url = settings.link.url;
		}

		if ( 'file' === settings.link_to ) {
			link_url = settings.image.url;
		}

		#><div class="elementor-image{{ settings.shape ? ' elementor-image-shape-' + settings.shape : '' }}">
			<div class="elementor-image-wrapper">
				<div class="elementor-image-overlay"></div><#
				var imgClass = '',
					hasCaption = '' !== settings.caption;

				if ( '' !== settings.hover_animation ) {
					imgClass = 'elementor-animation-' + settings.hover_animation;
				}

				if ( hasCaption ) {
					#><figure class="wp-caption"><#
				}

				if ( link_url ) {
						#><a class="elementor-clickable" data-elementor-open-lightbox="{{ settings.open_lightbox }}" href="{{ link_url }}"><#
				}
							#><img src="{{ image_url }}" class="{{ imgClass }}" /><#

				if ( link_url ) {
						#></a><#
				}

				if ( hasCaption ) {
						#><figcaption class="widget-image-caption wp-caption-text">{{{ settings.caption }}}</figcaption><#
				}

				if ( hasCaption ) {
					#></figure><#
				}

				#>			
			</div>
		</div><#
	}
	#>
	<?php

}
//-----------/JS (BACKBONE)-----------
$out = ob_get_clean();

echo sprintf("%s", $out);