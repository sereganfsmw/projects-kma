<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//=======================RENDER TYPE=======================
$js_settings = $icon = '';
//-----------PHP-----------
if ($type == 'php'){

	$size = $settings['size'];
	$img = $settings['icon_img'];
	$img = wp_get_attachment_image_src($img['id'],'full');


	if ($settings['icon_lib'] == 'fontawesome'){
		$icon = $settings['icon_fontawesome'];
	} else if ($settings['icon_lib'] == 'flaticons') {
		$icon = $settings['icon_flaticons'];
	}

	$render->add_render_attribute( 'icon', [
		'class' => [$icon, $size],
	] );

	$render->add_render_attribute( 'extra_line', [
		'class' => [($settings['background_line'] ? 'has_bg' : ''), 'elementor-divider-separator'],
	] );

//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
else if ($type == 'js') {


	$js_settings = "
		<#

		if (settings.icon_lib == 'fontawesome'){
			settings.icon = settings.icon_fontawesome;
		} else if (settings.icon_lib == 'flaticons') {
			settings.icon = settings.icon_flaticons;
		}
		
		settings.img = settings.icon_img.url;

		view.addRenderAttribute( 'icon', {
			'class': [ settings.icon, settings.size ],
		} );

		view.addRenderAttribute( 'extra_line', {
			'class': [(settings.background_line ? 'has_bg' : ''), 'elementor-divider-separator'],
		} );

		#>
	";


}
//-----------/JS (BACKBONE)-----------

//Render attr
$attr_icon = ($type == 'php') ? $render->get_render_attribute_string( 'icon' ) : "{{{ view.getRenderAttributeString( 'icon' ) }}}";
$attr_extra_line = ($type == 'php') ? $render->get_render_attribute_string( 'extra_line' ) : "{{{ view.getRenderAttributeString( 'extra_line' ) }}}";

$module_id = uniqid( "cws_divider_" );

$out = "";

	$out .= $js_settings;

	$out .= "<div id='".esc_attr($module_id)."' class='elementor-divider'>";
		$out .= "<span ".$attr_extra_line.">";
			$out .= "<span class='left-divider-part'></span>";
			if ($type == 'php'){
				if( !empty( $img ) ) {
					$out .= "<span class='icon_image' style='background-image:url(".$img[0].")'></span>";
				} else {
					if ( ! empty( $icon ) ) {	
						$out .= "<span class='icon_wrapper'><i ".$attr_icon."></i></span>";
					}
				}
			} else if ($type == 'js') {
				$out .= "<# if ( settings.img ) { #>";
					$out .= "<span class='icon_image' style='background-image:url({{{settings.img}}})'></span>";
				$out .= "<# } else { #>";
					$out .= "<# if ( settings.icon ) { #>";
						$out .= "<span class='icon_wrapper'><i ".$attr_icon."></i></span>";
					$out .= "<# } #>";
				$out .= "<# } #>";
			}
			$out .= "<span class='right-divider-part'></span>";
		$out .= "</span>";
	$out .= "</div>";

echo sprintf("%s", $out);