<?php
global $cws_theme_funcs;

//Colors
$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

//=======================RENDER TYPE=======================
$out = '';
ob_start();
//-----------PHP-----------
if ($type == 'php'){

		if ( empty( $settings['alert_title'] ) ) {
			return;
		}

		if ( ! empty( $settings['alert_type'] ) ) {
			$render->add_render_attribute( 'wrapper', 'class', 'elementor-alert elementor-alert-' . $settings['alert_type'] );
		}

		if ($settings['icon_lib'] == 'fontawesome'){
			$icon = $settings['icon_fontawesome'];
		} else if ($settings['icon_lib'] == 'flaticons') {
			$icon = $settings['icon_flaticons'];
		}

		$render->add_render_attribute( 'icon', [
			'class' => [$icon],
		] );

		$render->add_render_attribute( 'wrapper', 'role', 'alert' );

		$render->add_render_attribute( 'alert_title', 'class', 'elementor-alert-title' );

		?>
		<div <?php echo $render->get_render_attribute_string( 'wrapper' ); ?>>

			<?php if ( ! empty( $icon ) ) {	?>
				<span class="icon-wrapper"><i <?php echo $render->get_render_attribute_string( 'icon' ); ?>></i></span>
			<?php }	?>

			<span <?php echo $render->get_render_attribute_string( 'alert_title' ); ?>><?php echo $settings['alert_title']; ?></span>
			<?php if ( ! empty( $settings['alert_description'] ) ) {
				$render->add_render_attribute( 'alert_description', 'class', 'elementor-alert-description' );

				?>
				<span <?php echo $render->get_render_attribute_string( 'alert_description' ); ?>><?php echo $settings['alert_description']; ?></span>
			<?php }
			if ( 'show' === $settings['show_dismiss'] ) { ?>
				<button type="button" class="elementor-alert-dismiss">
					<span class="close-button" aria-hidden="true">&times;</span>
					<span class="elementor-screen-only"><?php esc_html_e( 'Dismiss alert', 'cryptop' ); ?></span>
				</button>
			<?php } ?>
		</div>
		<?php

//-----------/PHP-----------
}
//-----------JS (BACKBONE)-----------
else if ($type == 'js') {

		?>
			<#
			if ( settings.alert_title ) {
			view.addRenderAttribute( {
				alert_title: { class: 'elementor-alert-title' },
				alert_description: { class: 'elementor-alert-description' }
			} );

			if (settings.icon_lib == 'fontawesome'){
				settings.icon = settings.icon_fontawesome;
			} else if (settings.icon_lib == 'flaticons') {
				settings.icon = settings.icon_flaticons;
			}	

			view.addRenderAttribute( 'icon', {
				'class': [ settings.icon, settings.size ],
			} );			

			view.addInlineEditingAttributes( 'alert_title', 'none' );
			view.addInlineEditingAttributes( 'alert_description' );
			#>
			<div class="elementor-alert elementor-alert-{{ settings.alert_type }}" role="alert">

				<# if ( settings.icon ) { #>
					<span class="icon-wrapper"><i {{{ view.getRenderAttributeString( 'icon' ) }}}></i></span>
				<# } #>

				<span {{{ view.getRenderAttributeString( 'alert_title' ) }}}>{{{ settings.alert_title }}}</span>
				<span {{{ view.getRenderAttributeString( 'alert_description' ) }}}>{{{ settings.alert_description }}}</span>
				<# if ( 'show' === settings.show_dismiss ) { #>
					<button type="button" class="elementor-alert-dismiss">
						<span class="close-button" aria-hidden="true">&times;</span>
					</button>
				<# } #>
			</div>
		<# } #>
		<?php	

}
//-----------/JS (BACKBONE)-----------
$out = ob_get_clean();

echo sprintf("%s", $out);