<?php
global $cws_theme_funcs;

$section_id = uniqid( 'cws_categories_grid_' );

$out = $grid_class = "";
$counter = 0;

$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
$sb_layout = isset( $sb['layout_class'] ) ? $sb['layout_class'] : '';

$GLOBALS['cws_blog_atts'] = array(
	'layout'			=> $settings['columns'],
	'sb_layout'			=> $sb_layout,
);
$thumbnail_dims = cws_blog_thumbnail_dims();

if ((bool)$settings['crop']) {
	$thumbnail_dims['crop'] = true;
	$thumbnail_dims['width'] = $thumbnail_dims['height'];
	$image_object = wp_get_image_editor(get_template_directory_uri() . "/img/img_placeholder.png");

	if(!is_wp_error($image_object)){
		$image_object->resize($thumbnail_dims['width'], $thumbnail_dims['height'], true);
		$image_object->save(get_template_directory() . "/img/img_placeholder_crop.png");
	}
}
unset( $GLOBALS['cws_blog_atts'] );

$cat_terms = $settings['category_list'];

if (empty($cat_terms)) {
	$cat_terms = get_terms( 'category', array(
	    'fields' => 'id=>slug', //all, ids, names, id=>name, id=>slug
	));
}

$grid_class .= $settings['use_carousel'] == 'yes' ? ' category_carousel cws_owl_carousel' : ''; 

$render->add_render_attribute( 'wrapper', [
	'class' => ['cws_grid', "layout-".esc_attr($settings['columns']), 'grid', $grid_class],
] );

//Carousel process
if ($settings['use_carousel'] == 'yes'){

	$responsive = array(
		0 => array(
			'items' => (!empty($settings['slides_to_show_mobile']) ? (int)$settings['slides_to_show_mobile'] : 1)
		),
		767 => array(
			'items' => (!empty($settings['slides_to_show_tablet']) ? (int)$settings['slides_to_show_tablet'] : 1)
		),
		1199 => array(
			'items' => (!empty($settings['slides_to_show']) ? (int)$settings['slides_to_show'] : 1)
		),
	);

	$carousel_args = array(
		'items' => (int)$settings['slides_to_show'],
		'responsiveClass' => true,
		'responsive' => $responsive,
		'cols' => (int)$settings['columns'],
		'nav' => ($settings['navigation'] == 'both' || $settings['navigation'] == 'arrows'),
		'dots' => ($settings['navigation'] == 'both' || $settings['navigation'] == 'dots'),
		'autoheight' => ($settings['auto_height'] == 'yes'),
		'autoplay' => ($settings['autoplay'] == 'yes'),
		'autoplaySpeed' => ($settings['autoplay'] == 'yes' ? (int)($settings['autoplay_speed']) : false),
		'autoplayHoverPause' => ($settings['pause_on_hover'] == 'yes'),
		'margin' => $settings['item_margins']['size'],
		'stagePadding' => $settings['item_stage_paddings']['size'],
		'center' => ($settings['item_center'] == 'yes'),
		'loop' => ($settings['infinite'] == 'yes'),
		'animateIn' => ($settings['animation_in'] != 'default' ? $settings['animation_in'] : false),
		'animateOut' => ($settings['animation_out'] != 'default' ? $settings['animation_out'] : false),
		'smartSpeed' => (!empty($settings['animation_speed']) ? (int)(esc_attr($settings['animation_speed'])) : 250),
	);

	$render->add_render_attribute( 'wrapper', 'data-owl-args', json_encode($carousel_args) );
}
//--Carousel process

$out .= "<section id='".esc_attr($section_id)."' class='cws_categories'>";
$out .= "<div class='cws_wrapper'>";

//Carousel process
$out .=	"<div ".$render->get_render_attribute_string( 'wrapper' ).">";
//--Carousel process

		foreach ($cat_terms as $id => $slug) {
				if ($counter >= $settings['count']) break;

				$term = get_term_by('slug', $slug, 'category');
				$term_name = $term->name;
				$term_id = $term->term_id;
				$link = get_category_link($term_id);
				$term_image = get_term_meta($term_id, 'cws_mb_term' );
				if ((bool)$settings['crop']) {
					$dummy_image = get_template_directory_uri() . "/img/img_placeholder_crop.png";
				} else {
					$dummy_image = get_template_directory_uri() . "/img/img_placeholder.png";
				}
				$is_dummy = true;

				ob_start();
					if (!empty($term_image[0]['image']['src'])){
						$is_dummy = false;
						$dims['crop'] = true;
						$img_obj = cws_thumb( $term_image[0]['image']['src'], $thumbnail_dims , true );
					}
					?>
						<article <?php post_class(array( 'item','categories-grid')); ?>>
							<a class='category-block' href="<?php echo esc_url($link);?>">
								<img src='<?php echo esc_url( !$is_dummy ? $img_obj[0] : $dummy_image ); ?>' alt=''>
								<span class='category-label'><?php echo sprintf("%s", $term_name); ?></span>
							</a>
						</article>

					<?php
				$out .= ob_get_clean();
			$counter++;
		}
		$out .= "</div>";
	$out .= "</div>";

$out .= "</section>";

echo sprintf("%s", $out);