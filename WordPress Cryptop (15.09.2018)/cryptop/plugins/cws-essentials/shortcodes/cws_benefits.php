<?php
global $cws_theme_funcs;

$first_color = $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];

extract( shortcode_atts( array(
	"icon_lib"							=> '',
	"icon_fontawesome"					=> '',
	"icon_flaticons"					=> '',
	"icon_svg"							=> '',

	"title"								=> '',
	"title_typography"					=> '',
	"title_typography_html_tag"			=> '',
	"divider"							=> '',
	"description"						=> '',
	"description_typography"			=> '',
	"add_button"						=> '',
	"button_text"						=> '',
	"button_url"						=> '',
	"text_aligning"						=> '',
	"icon_margins"						=> '',
	"divider_margins"					=> '',
	"button_margins"					=> '',
	"customize_colors"					=> '',
	"title_color"						=> '',
	"title_color_hover"					=> '',
	"divider_color"						=> '',
	"divider_color_hover"				=> '',
	"description_color"					=> '',
	"description_color_hover"			=> '',
	"customize_button"					=> '',
	"customize_icon"					=> '',
	"button_color"						=> '',
	"button_background_color"			=> '',
	"button_color_hover"				=> '',
	"button_background_color_hover"		=> '',
	"custom_icon_size" 					=> '',
	"background_color" 					=> '',
	"icon_color" 						=> '',
	"background_color_hover" 			=> '',
	"icon_color_hover" 					=> '',
	"hover_animation" 					=> '',
), $settings ) );

$out = $icon = "";

if ($icon_lib == 'fontawesome'){
	$icon = $icon_fontawesome;
} elseif ($icon_lib == 'flaticons') {
	$icon = $icon_flaticons;
} elseif ($icon_lib == 'svg') {
	$icon = $icon_svg;
}

if ( empty( $title ) && empty( $description ) && empty( $icon )) return $out;

$module_id = uniqid( "cws_benefits_" );

$out .= "<div id='".esc_attr($module_id)."' class='cws_benefits_item'>";

	//Benefints icon
	if (!empty($icon)){
		$out .= "<div class='benefits_icon_wrapper'>";
			if ($icon_lib != 'svg') {
				$out .= "<i class='benefits_icon ".$icon."'></i>";
			} else {
				$svg_icon = json_decode(str_replace("``", "\"", $icon), true);
				$out .= function_exists('cwssvg_shortcode') ? cwssvg_shortcode($svg_icon) : "";
			}
		$out .= "</div>";	
	}

	//Benefits info part
	if (!empty($title) || !empty($description)){
		$out .= "<div class='benefits_text_wrapper'>";
			if (!empty($title)){
				$out .= "<".$title_typography_html_tag." class='benefits_title inner_title'>".esc_html($title)."</".$title_typography_html_tag.">";
			}
			if ($divider == 'yes'){
				$out .= "<div class='benefits_divider'><i></i></div>";
			}
			if (!empty($description)){
				$out .= "<div class='benefits_description'>".esc_html($description)."</div>";
			}
		$out .= "</div>";
	}

	//Benefits button
	if ($add_button == 'yes'){
		$out .= "<div class='benefits_button_wrapper'>";
			$out .= "<a class='button' href='".(!empty($button_url) ? esc_attr($button_url) : '#')."'>".esc_html($button_text)."</a>";
		$out .= "</div>";
	}


$out .= "</div>";

echo sprintf("%s", $out);