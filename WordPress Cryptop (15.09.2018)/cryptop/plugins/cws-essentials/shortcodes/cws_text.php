<?php
global $cws_theme_funcs;

//Colors
$body_font_options = $cws_theme_funcs->cws_get_option( 'body-font' );
$body_font_color = esc_attr( $body_font_options['color'] );
$subtitle_font_options = $cws_theme_funcs->cws_get_option( 'subtitle_font' );
$subtitle_font_color = esc_attr( $subtitle_font_options['color'] );
$heading_font_options = $cws_theme_funcs->cws_get_option( 'header-font' );
$heading_font_color = esc_attr( $heading_font_options['color'] );
$theme_colors_third_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['third_color'] );

extract( shortcode_atts( array(
	'icon_lib'					=> '',
	'icon_fontawesome'			=> '',
	'icon_flaticons'			=> '',
	'icon_svg'					=> '',
	'title'						=> '',
	'title_typography'			=> '',
	'add_divider'				=> '',
	'subtitle'					=> '',
	'subtitle_typography'		=> '',
	'text_alignment'			=> '',
	'text_alignment_mobile'		=> '',
	'text_alignment_tablet'		=> '',
	'icon_position'				=> '',
	'size'						=> '',
	'content'					=> '',
	'content_typography'		=> '',
	'customize_icon_size'		=> '',
	'size_i'					=> '',
	'customize_margins'			=> '',
	'icon_margins'				=> array(),
	'title_margins'				=> array(),
	'subtitle_margins'			=> array(),
	'content_margins'			=> array(),
	'customize_colors'			=> false,
	'custom_font_color'			=> '',
	'custom_title_color'		=> '',
	'custom_divider_color'		=> '',
	'custom_subtitle_color'		=> '',
	'custom_icon_color'			=> '',

	'title_typography_html_tag' 	=> '',
	'subtitle_typography_html_tag' 	=> '',
), $settings ) );

$custom_css_class = "";

$title = wp_kses( $title, array(
	"b"			=> array(),
	"strong"	=> array(),
	"mark"		=> array(),
	"br"		=> array()
));

$module_id = uniqid( "cws_textmodule_" );
$out = $titles = $icon_html = $icon = "";

if ($icon_lib == 'fontawesome'){
	$icon = $icon_fontawesome;
} elseif ($icon_lib == 'flaticons') {
	$icon = $icon_flaticons;
} elseif ($icon_lib == 'svg') {
	$icon = $icon_svg;
}

$titles .= !empty( $title ) ? "<".$title_typography_html_tag." class='widgettitle'>".esc_html( $title )."</".$title_typography_html_tag.">" : "";
$subtitles = !empty( $subtitle ) ? "<".$subtitle_typography_html_tag." class='widgetsubtitle'>" . esc_html( $subtitle ) . "</".$subtitle_typography_html_tag.">" : "";

if($icon_lib == 'svg'){
	$svg_icon = json_decode(str_replace("``", "\"", $icon), true);
	$icon_html .= function_exists('cwssvg_shortcode') ? cwssvg_shortcode($svg_icon) : "";	
} else {
	$icon_html .= !empty( $icon ) ? "<i class='cws_service_icon cws_icon_".esc_attr($size)." cws_textmodule_icon " . esc_attr( $icon ) ."'></i>" : "";
}

ob_start();

if ( !empty( $icon_fontawesome ) || !empty( $icon_flaticons ) || !empty( $icon_svg ) ) {
	$hasIcon = 'has_icon';
}

$class = cws_class([
	'cws_textmodule',
	'cws_module',
	( !empty( $icon_fontawesome ) || !empty( $icon_flaticons ) || !empty( $icon_svg ) ) ? 'has_icon' : ''
]);

if ( !empty( $titles ) || !empty( $content ) || !empty( $icon ) || !empty( $subtitles ) ){
	echo "<div class=".esc_attr($class)."' id='".esc_attr($module_id)."'>";

		echo "<div class='cws_textmodule_wrapper'>";				
			
			if ( !empty( $titles ) || !empty( $content ) ){
				echo "<div class='cws_textmodule_text'>";
					if ( !empty($titles) || !empty($subtitles) ){
						echo "<div class='cws_textmodule_content_wrapper" . ( !empty( $text_alignment ) ? " desktop_text-align-".esc_attr($text_alignment) : "" ) . ( !empty( $text_alignment_mobile ) ? " mobile_text-align-".esc_attr($text_alignment_mobile) : "" ) . ( !empty( $text_alignment_tablet ) ? " tablet_text-align-".esc_attr($text_alignment_tablet) : "" ) . "'>";
						
							echo $subtitles;

							if( !empty($titles) ){
								echo "<div class='title_wrapper'>";
									echo "<div class='flex-title'>";
										if ( !empty( $icon ) ){
											echo "<div class='cws_textmodule_icon_wrapper'>";
												echo $icon_html;
											echo "</div>";
										}
										echo $titles;
									echo "</div>";
								echo "</div>";
							}

						if( $add_divider == 'yes' ){
							echo "<div class='divider-wrapper'>";
								echo "<i class='text-divider'></i>";
							echo "</div>";
						}
					}
						if ( !empty( $content ) ){
							echo "<div class='cws_textmodule_content'>";
								echo $content;
							echo "</div>";
						}

						echo "</div>";

				echo "</div>";
			}

		echo "</div>";

	echo "</div>";
}
$out .= ob_get_clean();
echo sprintf("%s", $out);