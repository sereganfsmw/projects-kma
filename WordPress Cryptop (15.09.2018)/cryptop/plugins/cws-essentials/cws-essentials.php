<?php
/*
Plugin Name: CWS Essentials
Plugin URI:  http://creaws.com
Description: Internal use for creaws/cwsthemes themes only.
Author:      Creative Web Solutions
Author URI:  http://creaws.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Domain Path: /languages
Text Domain: cws-essentials
Version: 1.0.0
*/

//Load textdomain
add_action( 'plugins_loaded', 'cws_load_textdomain_essentials' );
function cws_load_textdomain_essentials() {
	load_plugin_textdomain( 'cws-essentials', false, basename( dirname( __FILE__ ) ) . '/languages' );
}
//--Load textdomain

//Check if plugin active
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

if (!defined('CWS_SHORTCODES_PLUGIN_NAME'))
	define('CWS_SHORTCODES_PLUGIN_NAME', trim(dirname(plugin_basename(__FILE__)), '/'));

if (!defined('CWS_SHORTCODES_PLUGIN_DIR'))
	define('CWS_SHORTCODES_PLUGIN_DIR', WP_PLUGIN_DIR . '/' . CWS_SHORTCODES_PLUGIN_NAME);


if (!defined('CWS_SHORTCODES_PLUGIN_URL'))
	define('CWS_SHORTCODES_PLUGIN_URL', WP_PLUGIN_URL . '/' . CWS_SHORTCODES_PLUGIN_NAME);

$theme = wp_get_theme();
if ($theme->get( 'Template' )) {
	if ( ! defined( 'THEME_SLUG' ) ) {
  		define('THEME_SLUG', $theme->get( 'Template' ));
  	}
} else {
	if ( ! defined( 'THEME_SLUG' ) ) {
  		define('THEME_SLUG', $theme->get( 'TextDomain' ));
  	}
}

global $cws_essentials_funcs;

$cws_essentials_funcs = new CWS_Essentials();

class CWS_Essentials{
	public function render_shortcodes( $template, $render, $type = 'php', $extra_params = array() ) {

		if ($type == 'php'){
			//Get all settings
			$settings = $render->get_settings();
		}

		if (!is_readable(CWS_SHORTCODES_PLUGIN_DIR . "/shortcodes/" . $template . ".php")) return;

		ob_start();
			require (CWS_SHORTCODES_PLUGIN_DIR . '/shortcodes/' . $template . ".php");
		$output = ob_get_clean();

		return $output;
	}
}

function js_var($variable){
	return preg_replace('/{+|}+/', '', $variable);
}

//Get custom post types slugs
function cws_get_slug($slug) {
	$new_slug = '';
	global $cws_theme_funcs;
	if(!empty($cws_theme_funcs)){
		$new_slug = $cws_theme_funcs->cws_get_option($slug.'_slug');
	}
	$new_slug = !empty( $new_slug ) ? $new_slug : $slug;

	return sanitize_title($new_slug);
}

//Regenerate permalinks, if slug (blog / portfolio / staff) changed
add_action( "init", "cws_rewrite_slug", 11 );

function cws_rewrite_slug() {
	$cws_rewrite_slug = get_option('cws_rewrite_slug');
	if ($cws_rewrite_slug){
		flush_rewrite_rules();
		update_option('cws_rewrite_slug', false);
	}
}
//-------------------------

//Add thumbnail image to posts
function add_post_thumb_name ($columns) {
	$columns = array_slice($columns, 0, 1, true) +
				array('post_thumbnail' => __('Thumbnails', 'cws-essentials')) +
				array_slice($columns, 1, NULL, true);
	return $columns;
}
add_filter('manage_post_posts_columns', 'add_post_thumb_name');

function add_post_thumb ($column, $id) {
	if ('post_thumbnail' === $column) {
		echo the_post_thumbnail('thumbnail');
	}
}
add_action('manage_post_posts_custom_column', 'add_post_thumb', 5, 2);
//Add thumbnail image to posts

/*Term images*/
//If you need to initialize then term edit
// add_action( 'edit_term', 'cws_show_extra_term_fields' );
add_action( 'admin_init', 'cws_taxonomy_image_init');

/*Users extra profile fields*/
add_action( 'show_user_profile', 'cws_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'cws_show_extra_profile_fields' );
add_action( 'personal_options_update', 'cws_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'cws_save_extra_profile_fields' );

function cws_taxonomy_image_init(){
    $taxonomies = get_taxonomies();

    add_filter('manage_edit-category_columns', 'cws_category_columns');
    add_filter('manage_category_custom_column', 'cws_category_columns_fields', 10, 3);

    foreach ((array) $taxonomies as $taxonomy) {
    	if ($taxonomy == 'category'){
        	cws_add_custom_column_fields($taxonomy);
    	}
    }
}

function cws_add_custom_column_fields($taxonomy)
{
    add_action($taxonomy."_add_form_fields", 'cws_show_extra_term_fields');
    add_action($taxonomy."_edit_form_fields", 'cws_show_extra_term_fields');

	add_action("created_".$taxonomy, 'cws_save_extra_term_fields' );
	add_action("edited_".$taxonomy, 'cws_save_extra_term_fields' );

    //Add custom columns
    add_filter("manage_edit-".$taxonomy."_columns", 'cws_category_columns');
    add_filter("manage_".$taxonomy."_custom_column", 'cws_category_columns_fields', 10, 3);
}

function cws_category_columns($columns)
{
    $columns['image'] = esc_html__( 'Image', 'cws-essentials' );
    return $columns;
}

function cws_category_columns_fields($deprecated, $column_name, $term_id)
{
	$term_image = get_term_meta( $term_id, 'cws_mb_term' );
    if (!empty($term_image)){
		echo "<img class='term_table_img' src='".$term_image[0]['image']['src']."' alt=''>";
    }
}

// Extra term fields
function cws_show_extra_term_fields( $term_id ) {
	global $pagenow;

	$mb_attr = array(	
		'image' => array(
			'title' => esc_html__( 'Image', 'cws-essentials' ),
			'subtitle' => esc_html__( 'Upload your photo here.', 'cws-essentials' ),
			'addrowclasses' => 'hide_label wide_picture box grid-col-12 category_image',
			'type' => 'media',
		),
	);

	if ( in_array($pagenow,array('edit-tags.php','term.php')) ){
		echo '<h3>Additional categories information (CWS Themes)</h3>';
		echo '<div id="cws-post-metabox-id-1">';
			echo '<div class="inside" data-w="0">';
			wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );
			if (gettype ($term_id) == 'object'){
				$cws_stored_meta = get_term_meta( $term_id->term_id, 'cws_mb_term' );
			}
			if (function_exists('cws_core_cwsfw_fillMbAttributes') ) {
				if (!empty($cws_stored_meta[0])) {
					cws_core_cwsfw_fillMbAttributes($cws_stored_meta[0], $mb_attr);
				}
				echo cws_core_cwsfw_print_layout($mb_attr, 'cws_mb_');
			}
			echo "</div>";
		echo "</div>";
	}
}

function cws_save_extra_term_fields( $term_id) {
	$save_array = array();

	foreach($_POST as $key => $value) {
		if (0 === strpos($key, 'cws_mb_')) {
			if ('on' === $value) {
				$value = '1';
			}
			if (is_array($value)) {
				foreach ($value as $k => $val) {
					if (is_array($val)) {
						$save_array[substr($key, 7)][$k] = $val;
					} else {
						$save_array[substr($key, 7)][$k] = esc_html($val);
					}
				}
			} else {
				$save_array[substr($key, 7)] = esc_html($value);
			}
		}
	}
	if (!empty($save_array)) {
		update_term_meta( $term_id, 'cws_mb_term', $save_array );
	}
	return;
}

// Extra user fields
function cws_show_extra_profile_fields( $user ) {
	$mb_attr = array(
		'position' => array(
			'type' => 'text',
			'title' => esc_html__('Position', 'cws-essentials' ),
			'addrowclasses' => 'box grid-col-12',
		),		
		'avatar' => array(
			'title' => esc_html__( 'Avatar', 'cws-essentials' ),
			'subtitle' => esc_html__( 'Upload your photo here.', 'cws-essentials' ),
			'addrowclasses' => 'hide_label wide_picture box grid-col-12',
			'type' => 'media',
		),
		'social_group' => array(
			'type' => 'group',
			'addrowclasses' => 'group expander sortable box grid-col-12',
			'title' => esc_html__('Social networks', 'cws-essentials' ),
			'button_title' => esc_html__('Add new social network', 'cws-essentials' ),
			'button_icon' => 'fa fa-plus',
			'button_class' => 'button button-primary',
			'layout' => array(
				'title' => array(
					'type' => 'text',
					'atts' => 'data-role="title"',
					'addrowclasses' => 'grid-col-2',
					'title' => esc_html__('Social account title', 'cws-essentials' ),
				),
				'icon' => array(
					'type' => 'select',
					'addrowclasses' => 'fai grid-col-3',
					'source' => 'fa',
					'title' => esc_html__('Icon for this social contact', 'cws-essentials' )
				),
				'color'	=> array(
					'title'	=> esc_html__( 'Icon color', 'cws-essentials' ),
					'atts' => 'data-default-color="#595959"',
					'addrowclasses' => 'grid-col-3',
					'value' => '#595959',
					'type'	=> 'text',
				),										
				'url' => array(
					'type' => 'text',
					'addrowclasses' => 'grid-col-3',
					'title' => esc_html__('Url to your account', 'cws-essentials' ),
				)
			),
		),
		'author_url' => array(
			'type' => 'text',
			'title' => esc_html__('Author page URL', 'cws-essentials' ),
			'addrowclasses' => 'box grid-col-12',
		),			
	);

	echo '<h3>Additional profile information (CWS Themes)</h3>';
	echo '<div id="cws-post-metabox-id-1" class="postbox">';
		echo '<div class="inside" data-w="0">';
			wp_nonce_field( 'cws_mb_nonce', 'mb_nonce' );
			$cws_stored_meta = get_user_meta( $user->ID, 'cws_mb_user' );

			if (function_exists('cws_core_build_layout') ) {
				echo cws_core_build_layout((!empty($cws_stored_meta) ? $cws_stored_meta[0] : ''), $mb_attr, 'cws_mb_');
			}

		echo "</div>";
	echo "</div>";
}

function cws_save_extra_profile_fields( $user_id ) {
	if ( !current_user_can( 'edit_user', $user_id ) ) return false;

	$save_array = array();

	foreach($_POST as $key => $value) {
		if (0 === strpos($key, 'cws_mb_')) {
			if ('on' === $value) {
				$value = '1';
			}
			if (is_array($value)) {
				foreach ($value as $k => $val) {
					if (is_array($val)) {
						$save_array[substr($key, 7)][$k] = $val;
					} else {
						$save_array[substr($key, 7)][$k] = esc_html($val);
					}
				}
			} else {
				$save_array[substr($key, 7)] = esc_html($value);
			}
		}
	}
	if (!empty($save_array)) {
		update_user_meta( $user_id, 'cws_mb_user', $save_array );

	}
}

/*------------------------------------
-------------- PORTFOLIO -------------
------------------------------------*/
add_action( "init", "register_cws_portfolio_cat", 1 );
add_action( "init", "register_cws_portfolio", 2 );

function register_cws_portfolio_cat(){
	$rewrite_slug = cws_get_slug('portfolio');

	register_taxonomy( 'cws_portfolio_cat', 'cws_portfolio', array(
		'hierarchical' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => $rewrite_slug . '_cat' )
	));
}

function register_cws_portfolio (){
	$rewrite_slug = cws_get_slug('portfolio');

	$labels = array(
		'name' => esc_html__( 'Portfolio items', 'cws-essentials' ),
		'singular_name' => esc_html__( 'Portfolio item', 'cws-essentials' ),
		'menu_name' => esc_html__( 'Portfolio', 'cws-essentials' ),
		'add_new' => esc_html__( 'Add New', 'cws-essentials' ),
		'add_new_item' => esc_html__( 'Add New Portfolio Item', 'cws-essentials' ),
		'edit_item' => esc_html__('Edit Portfolio Item', 'cws-essentials' ),
		'new_item' => esc_html__( 'New Portfolio Item', 'cws-essentials' ),
		'view_item' => esc_html__( 'View Portfolio Item', 'cws-essentials' ),
		'search_items' => esc_html__( 'Search Portfolio Item', 'cws-essentials' ),
		'not_found' => esc_html__( 'No Portfolio Items found', 'cws-essentials' ),
		'not_found_in_trash' => esc_html__( 'No Portfolio Items found in Trash', 'cws-essentials' ),
		'parent_item_colon' => '',
	);

	register_post_type( 'cws_portfolio', array(
		'label' => esc_html__( 'Portfolio items', 'cws-essentials' ),
		'labels' => $labels,
		'public' => true,
		'rewrite' => array( 'slug' => $rewrite_slug ),
		'capability_type' => 'post',
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'page-attributes',
			'thumbnail'
			),
		'menu_position' => 23,
		'menu_icon' => 'dashicons-format-gallery',
		'taxonomies' => array( 'cws_portfolio_cat' ),
		'has_archive' => true
	));
}

//Add thumbnail image to portfolio posts
function add_cws_portfolio_thumb_name ($columns) {
	$columns = array_slice($columns, 0, 1, true) +
				array('cws_portfolio_thumbnail' => __('Thumbnails', 'cws-essentials')) +
				array_slice($columns, 1, NULL, true);
	return $columns;
}
add_filter('manage_cws_portfolio_posts_columns', 'add_cws_portfolio_thumb_name');

function add_cws_portfolio_thumb ($column, $id) {
	if ('cws_portfolio_thumbnail' === $column) {
		echo the_post_thumbnail('thumbnail');
	}
}
add_action('manage_cws_portfolio_posts_custom_column', 'add_cws_portfolio_thumb', 5, 2);
//Add thumbnail image to portfolio posts

/*------------------------------------
---------------- STAFF ---------------
------------------------------------*/

add_action( "init", "register_cws_staff_department", 3 );
add_action( "init", "register_cws_staff_position", 4 );
add_action( "init", "register_cws_staff", 5 );

function register_cws_staff (){
	$rewrite_slug = cws_get_slug('staff');

	$labels = array(
		'name' => esc_html__( 'Staff members', 'cws-essentials' ),
		'singular_name' => esc_html__( 'Staff member', 'cws-essentials' ),
		'menu_name' => esc_html__( 'Our team', 'cws-essentials' ),
		'all_items' => esc_html__( 'All', 'cws-essentials' ),
		'add_new' => esc_html__( 'Add new', 'cws-essentials' ),
		'add_new_item' => esc_html__( 'Add New Staff Member', 'cws-essentials' ),
		'edit_item' => esc_html__('Edit Staff Member\'s info', 'cws-essentials' ),
		'new_item' => esc_html__( 'New Staff Member', 'cws-essentials' ),
		'view_item' => esc_html__( 'View Staff Member\'s info', 'cws-essentials' ),
		'search_items' => esc_html__( 'Find Staff Member', 'cws-essentials' ),
		'not_found' => esc_html__( 'No Staff Members found', 'cws-essentials' ),
		'not_found_in_trash' => esc_html__( 'No Staff Members found in Trash', 'cws-essentials' ),
		'parent_item_colon' => '',
	);

	register_post_type( 'cws_staff', array(
		'label' => esc_html__( 'Staff members', 'cws-essentials' ),
		'labels' => $labels,
		'public' => true,
		'rewrite' => array( 'slug' => $rewrite_slug ),
		'capability_type' => 'post',
		'supports' => array(
			'title',
			'editor',
			'excerpt',
			'page-attributes',
			'thumbnail'
			),
		'menu_position' => 24,
		'menu_icon' => 'dashicons-groups',
		'taxonomies' => array( 'cws_staff_member_position' ),
		'has_archive' => true
	));
}

function register_cws_staff_department(){
	$rewrite_slug = cws_get_slug('staff');

	$labels = array(
		'name' => esc_html__( 'Departments', 'cws-essentials' ),
		'singular_name' => esc_html__( 'Staff department', 'cws-essentials' ),
		'all_items' => esc_html__( 'All Staff departments', 'cws-essentials' ),
		'edit_item' => esc_html__( 'Edit Staff department', 'cws-essentials' ),
		'view_item' => esc_html__( 'View Staff department', 'cws-essentials' ),
		'update_item' => esc_html__( 'Update Staff department', 'cws-essentials' ),
		'add_new_item' => esc_html__( 'Add Staff department', 'cws-essentials' ),
		'new_item_name' => esc_html__( 'New Staff department name', 'cws-essentials' ),
		'parent_item' => esc_html__( 'Parent Staff department', 'cws-essentials' ),
		'parent_item_colon' => esc_html__( 'Parent Staff department:', 'cws-essentials' ),
		'search_items' => esc_html__( 'Search Staff departments', 'cws-essentials' ),
		'popular_items' => esc_html__( 'Popular Staff departments', 'cws-essentials' ),
		'separate_items_width_commas' => esc_html__( 'Separate with commas', 'cws-essentials' ),
		'add_or_remove_items' => esc_html__( 'Add or Remove Staff departments', 'cws-essentials' ),
		'choose_from_most_used' => esc_html__( 'Choose from the most used Staff departments', 'cws-essentials' ),
		'not_found' => esc_html__( 'No Staff departments found', 'cws-essentials' )
	);
	register_taxonomy( 'cws_staff_member_department', 'cws_staff', array(
		'labels' => $labels,
		'hierarchical' => true,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => $rewrite_slug . '_cat' )
	));
}

function register_cws_staff_position(){
	$rewrite_slug = cws_get_slug('staff');

	$labels = array(
		'name' => esc_html__( 'Positions', 'cws-essentials' ),
		'singular_name' => esc_html__( 'Staff Member position', 'cws-essentials' ),
		'all_items' => esc_html__( 'All Staff Member positions', 'cws-essentials' ),
		'edit_item' => esc_html__( 'Edit Staff Member position', 'cws-essentials' ),
		'view_item' => esc_html__( 'View Staff Member position', 'cws-essentials' ),
		'update_item' => esc_html__( 'Update Staff Member position', 'cws-essentials' ),
		'add_new_item' => esc_html__( 'Add Staff Member position', 'cws-essentials' ),
		'new_item_name' => esc_html__( 'New Staff Member position name', 'cws-essentials' ),
		'search_items' => esc_html__( 'Search Staff Member positions', 'cws-essentials' ),
		'popular_items' => esc_html__( 'Popular Staff Member positions', 'cws-essentials' ),
		'separate_items_width_commas' => esc_html__( 'Separate with commas', 'cws-essentials' ),
		'add_or_remove_items' => esc_html__( 'Add or Remove Staff Member positions', 'cws-essentials' ),
		'choose_from_most_used' => esc_html__( 'Choose from the most used Staff Member positions', 'cws-essentials' ),
		'not_found' => esc_html__( 'No Staff Member positions found', 'cws-essentials' )
	);
	register_taxonomy( 'cws_staff_member_position', 'cws_staff', array(
		'labels' => $labels,
		'show_admin_column' => true,
		'rewrite' => array( 'slug' => $rewrite_slug . '_tag' ),
		'show_tagcloud' => false
	));
}
// =====================================================================================================================================================

function add_order_column( $columns ) {
  $columns['menu_order'] = "Order";
  return $columns;
}
add_action('manage_edit-cws_staff_columns', 'add_order_column');
add_action('manage_edit-cws_portfolio_columns', 'add_order_column');

/**
* show custom order column values
*/
function show_order_column($name){
  global $post;
  switch ($name) {
    case 'menu_order':
      $order = $post->menu_order;
      echo $order;
      break;
   default:
      break;
   }
}

//Add thumbnail image to staff posts
function add_cws_staff_thumb_name ($columns) {
	$columns = array_slice($columns, 0, 1, true) +
				array('cws_staff_thumbnail' => __('Thumbnails', 'cws-essentials')) +
				array_slice($columns, 1, NULL, true);
	return $columns;
}
add_filter('manage_cws_staff_posts_columns', 'add_cws_staff_thumb_name');

function add_cws_staff_thumb ($column, $id) {
	if ('cws_staff_thumbnail' === $column) {
		echo the_post_thumbnail('thumbnail');
	}
}
add_action('manage_cws_staff_posts_custom_column', 'add_cws_staff_thumb', 5, 2);
//Add thumbnail image to staff posts

add_action('manage_cws_staff_posts_custom_column','show_order_column');
add_action('manage_cws_portfolio_posts_custom_column','show_order_column');
add_action('manage_cws_testimonial_posts_custom_column','show_order_column');

/**
* make column sortable
*/
function order_column_register_sortable( $columns ){
	$new_columns = array(
		"menu_order" 	=> "menu_order",
		"date"			=> "date",
		"title"			=> "title"
	);
	return $new_columns;
}
add_filter('manage_edit-cws_staff_sortable_columns','order_column_register_sortable');
add_filter('manage_edit-cws_portfolio_sortable_columns','order_column_register_sortable');


/****************** POSTS AJAX *******************/
function cws_posts_ajax(){
	//Fill atts
	if ($_POST['data']['post_type'] == 'post'){
		$fill_atts = cws_blog_fill_atts($_POST['data']);
	} else if ($_POST['data']['post_type'] == 'cws_portfolio') {
		$fill_atts = cws_portfolio_fill_atts($_POST['data']);
	} else if ($_POST['data']['post_type'] == 'cws_staff') {
		$fill_atts = cws_staff_fill_atts($_POST['data']);
	}
	extract( $fill_atts );

	$paged = $page;
	if ( !empty( $req_page_url ) ){
		$match = preg_match( "#paged?(=|/)(\d+)#", $req_page_url, $matches );
		$paged = $match ? $matches[2] : '1';								// if page parameter absent show first page
	}

	$not_in = ( 1 == $paged ) ? array() : get_option( 'sticky_posts' );
	$query_args = array(
		'post_type' => array( $post_type ),
		'post_status' => 'publish',
		'post__not_in' => $not_in
	);

	$query_args['posts_per_page']	= $items_per_page;
	$query_args['paged']			= $paged;

	//Filter by
	$tax = $fill_atts['filter_by'];
	$terms = $fill_atts[$tax];

	if ( !empty( $current_filter_val ) && $current_filter_val != '_all_'){
		$terms = array( $current_filter_val );
	}

	if (!empty($current_filter_val) && empty($tax)){
		if ($post_type == 'cws_portfolio'){
			$tax = 'cws_portfolio_cat';
		} else if ($post_type == 'cws_staff') {
			$tax = 'cws_staff_member_department';
		}
	}
	
	if ( !empty( $terms ) ){
		$query_args['tax_query'] = array(
			array(
				'taxonomy'		=> $tax,
				'field'			=> 'slug',
				'terms'			=> $terms
			)
		);
	}

	if ( in_array( $post_type, array( "cws_portfolio", "cws_staff", "cws_testimonial", "tribe_events", "cws_classes" ) ) ){
		$query_args['orderby'] 	= "menu_order date title";
		$query_args['order']	= "ASC";
	}

	if (!empty($extra_query_args)){
		$query_args = array_merge( $query_args, $extra_query_args );
	}

	$q = new WP_Query( $query_args );
	$found_posts = $q->found_posts;

	$requested_posts = $found_posts > $items_count ? $items_count : $found_posts;
	$max_paged = $found_posts > $items_count ? ceil( $items_count / $items_per_page ) : ceil( $found_posts / $items_per_page );

	$use_pagination = in_array( $display_style, array( 'grid', 'filter', 'filter_with_ajax' ) ) && $max_paged > 1;

	if ($post_type == 'post'){
		cws_blog_posts($q);
	} else if ($post_type == 'cws_portfolio') {
		cws_portfolio_posts($q);
	} else if ($post_type == 'cws_staff') {
		cws_staff_posts($q);
	}

	if ( $use_pagination ){
		if ( $pagination_grid == 'load_more' ){
			echo cws_load_more ();
		}
		else if($pagination_grid == 'load_on_scroll'){
			echo cws_load_more (true);
		}				
		else if($pagination_grid == 'ajax'){
			echo cws_pagination($paged, $max_paged, true);
		}
	}

	if ($post_type == 'post'){
		unset( $GLOBALS['cws_blog_atts'] );
	} else if ($post_type == 'cws_portfolio') {
		unset( $GLOBALS['cws_portfolio_atts'] );
	} else if ($post_type == 'cws_staff') {
		unset( $GLOBALS['cws_staff_atts'] );
	}

	echo "<input type='hidden' id='".esc_attr($section_id)."_page_number' name='".esc_attr($section_id)."_page_number' class='cws_page_number' value='".esc_attr($paged)."' />";
	wp_die();
}
add_action( 'wp_ajax_cws_posts_ajax', 'cws_posts_ajax' );
add_action( 'wp_ajax_nopriv_cws_posts_ajax', 'cws_posts_ajax' );
/****************** \POSTS AJAX *******************/

function cws_portfolio_single_ajax(){
	$data = isset( $_POST['data'] ) ? $_POST['data'] : array();
	extract( shortcode_atts( array(
		'initial_id' => '',
		'requested_id' => ''
	), $data));

	if ( empty( $initial_id ) || empty( $requested_id ) ) die();

	$pid = $requested_id;

	//Meta vars
	$post_meta = get_post_meta( $pid, 'cws_mb_post' );
	$post_meta = isset( $post_meta[0] ) ? $post_meta[0] : array();

	$full_width = isset( $post_meta['full_width'] ) ? $post_meta['full_width'] : "";
	//--Meta vars

	$GLOBALS['cws_single_ajax_portfolio_atts'] = array(
		'pid' => $pid,
	);

	//Post Parts
	ob_start();
		echo $full_width ? "<div class='full_width_media'>" : '';
			cws_portfolio_single_media($pid);
		echo $full_width ? "</div>" : '';
	$media_part = ob_get_clean();
	//--Post Parts

	if ( $full_width ) {
		echo "<div class='cws_ajax_response_media_full'>";
			echo sprintf("%s", $media_part);
		echo "</div>";
	}

	echo "<div class='cws_ajax_response'>";
		cws_portfolio_single_article($pid);
	echo "</div>";

	unset( $GLOBALS['cws_single_ajax_portfolio_atts'] );
	die();
}
add_action( "wp_ajax_cws_portfolio_single_ajax", "cws_portfolio_single_ajax" );
add_action( "wp_ajax_nopriv_cws_portfolio_single_ajax", "cws_portfolio_single_ajax" );

if(!function_exists('essentials_register_scripts')){
	function essentials_register_scripts (){
		$js_path = plugin_dir_url( __FILE__ ) . 'assets/js/';

		$common_scripts = array(	
			'jquery-ajax-shortcode'			=> array('ajax_plugin.js', true),
			'jquery-shortcode-velocity'		=> array('velocity.min.js', false),
			'jquery-shortcode-velocity-ui'	=> array('velocity.ui.min.js', false)
		);

		foreach ($common_scripts as $alias => $value) {

			list($path, $enqueue) = $value;
			if ($path) {
				$path = (0 === strrpos($path, 'http')) ? $path : $js_path . $path;
			}

			if ($enqueue){
				wp_enqueue_script( $alias, $path, array( 'jquery' ), '', true );
			} else {
				wp_register_script( $alias, $path, array( 'jquery' ), '', true );
			}

		}

		wp_localize_script('jquery-ajax-shortcode', 'cws_vc_sh', array(
			'ajax_nonce' => wp_create_nonce('cws_vc_sh_nonce'),
		));	
	}	
}
add_action( 'wp_enqueue_scripts', 'essentials_register_scripts' );

if(!function_exists('cws_post_term_links_str')){
	function cws_post_term_links_str ( $tax = "", $delim = "" ){
		$pid = get_the_id();
		$terms_arr = wp_get_post_terms( $pid, $tax );
		$terms = "";
		if ( is_wp_error( $terms_arr ) ){
			return $terms;
		}
		for( $i = 0; $i < count( $terms_arr ); $i++ ){
			$term_obj	= $terms_arr[$i];
			$term_slug	= $term_obj->slug;
			$term_name	= $term_obj->name;
			$term_link	= get_term_link( $term_slug, $tax );
			$terms		.= "<a class='post_term' href='".esc_url($term_link)."'>".esc_html($term_name)."</a>" . ( $i < ( count( $terms_arr ) - 1 ) ? $delim : "" );
		}
		return $terms;
	}
}

require_once( CWS_SHORTCODES_PLUGIN_DIR . '/templates-modules/cws_portfolio.php' );
require_once( CWS_SHORTCODES_PLUGIN_DIR . '/templates-modules/cws_staff.php' );

function cws_sc_dropcap ( $atts = array(), $content = "" ){
	return "<span class='dropcap'>$content</span>";
}
add_shortcode( 'cws_sc_dropcap', 'cws_sc_dropcap' );

function cws_sc_mark ( $atts = array(), $content = "" ){
	global $cws_theme_funcs;
	$first_color = $cws_theme_funcs ? $cws_theme_funcs->cws_get_meta_option( 'theme_color' )['first_color'] : "";
	extract( shortcode_atts( array(
		'font_color'	=> '#fff',
		'bg_color'		=> $first_color
	), $atts));
	return "<mark style='color: ".esc_attr($font_color).";background-color: ".esc_attr($bg_color).";'>$content</mark>";
}
add_shortcode( 'cws_sc_mark', 'cws_sc_mark' );



function cws_vc_shortcode_sc_twitter ( $atts = array(), $content = "" ){
	global $cws_theme_funcs;
	$first_color = $cws_theme_funcs ? $cws_theme_funcs->cws_get_meta_option( 'theme_color' )['first_color'] : "";
	$font_options = $cws_theme_funcs ? $cws_theme_funcs->cws_get_meta_option( 'body_font' ) : "";
	$font_color 			= $font_options['color'];
	extract( shortcode_atts( array(
		"icon_lib"					=> "",
		'number' 					=> 4,
		'visible_number' 			=> 2,
		"customize_colors"			=> false,
		"custom_font_color"			=> $font_color,
		"custom_featured_color"		=> $first_color,
		"el_class"					=> ""
 	), $atts));
	$icon_lib 				= esc_attr( $icon_lib );
	$icon 					= function_exists('cws_ext_vc_sc_get_icon') ? cws_ext_vc_sc_get_icon( $atts ) : "";
	$icon 					= esc_attr( $icon );
  	$number 				= esc_textarea( $number );
  	$visible_number 		= esc_textarea( $visible_number );
  	$customize_colors 		= (bool)$customize_colors;
  	$custom_font_color 	  	= esc_attr( $custom_font_color );
  	$custom_featured_color 	= esc_attr( $custom_featured_color );
  	$el_class				= esc_attr( $el_class );
 	$number = empty( $number ) ? 4 : (int)$number;
 	$visible_number = empty( $visible_number ) ? 2 : (int)$visible_number;
	$classes = "cws_twitter cws_vc_shortcode_module";
	$classes .= !empty( $el_class ) ? " $el_class" : "";
	$classes = trim( $classes );
	$id = uniqid( "cws_twitter_" );

	$number = (int)$number;
	$visible_number = (int)$visible_number;
	$visible_number = $visible_number == 0 ? $number : $visible_number;
	$retrieved_tweets_number = 0;
	$is_plugin_installed = function_exists( 'getTweets' );
	$tweets = $is_plugin_installed ? getTweets( $number ) : array();
	$retrieved_tweets_number = count( $tweets );
	$is_carousel = $retrieved_tweets_number > $visible_number;
	if ( $is_carousel ){
		wp_enqueue_script( 'owl_carousel' );
	}
	$tweets_received = false;
	ob_start();
	if ( !empty( $tweets ) ){
		if ( isset( $tweets['error'] ) && !empty( $tweets['error'] ) ){
			echo do_shortcode( "[cws_sc_msg_box title='" . esc_html__( 'error', 'cws_vc_shortcode' ) . "' type='error']" . esc_html( $tweets['error'] ) . "[/cws_sc_msg_box]" );
		}
		else{
			if ( $is_carousel ){
				echo "<ul class='cws_tweets widget_carousel bullets_nav'>";
				$groups_count = ceil( $retrieved_tweets_number / $visible_number );
				for ( $i = 0; $i < $groups_count; $i++ ){
					echo "<li class='cws_tweets_group'>";
						echo "<ul>";
						for( $j = $i * $visible_number; ( ( $j < ( $i + 1 ) * $visible_number ) && ( $j < $retrieved_tweets_number ) ); $j++ ){
							$tweet = $tweets[$j];
							$tweet_text = $tweet['text'];
							$tweet_date = $tweet['created_at'];
							echo "<li class='tweet'>";
								echo "<div class='text'>";
									echo esc_html( $tweet_text );
								echo "</div>";
								echo "<div class='date'>";
									echo esc_html( date( "Y-m-d H:i:s", strtotime( $tweet_date ) ) );
								echo "</div>";	
							echo "</li>";						
						}
						echo "</ul>";
					echo "</li>";
				}
				echo "</ul>";
			}
			else{
				echo "<ul class='cws_tweets'>";
					foreach ( $tweets as $tweet ) {
						echo "<li class='tweet'>";
							$tweet_text = $tweet['text'];
							$tweet_date = $tweet['created_at'];
							echo "<div class='text'>";
								echo esc_html( $tweet_text );
							echo "</div>";
							echo "<div class='date'>";
								echo esc_html( date( "Y-m-d H:i:s", strtotime( $tweet_date ) ) );
							echo "</div>";
						echo "</li>";
					}
				echo "</ul>";
			}
			$tweets_received = true;
		}
	}
	else{
		if ( !$is_plugin_installed ){
			echo do_shortcode( "[cws_sc_msg_box title='" . esc_html__( 'Plugin not installed', 'cws_vc_shortcode' ) . "' type='warn']" . esc_html__( 'Please install and activate required plugin ', 'cws_vc_shortcode' ) . "<a href='https://ru.wordpress.org/plugins/oauth-twitter-feed-for-developers/'>" . esc_html__( "oAuth Twitter Feed for Developers", 'cws_vc_shortcode' ) . "</a>[/cws_sc_msg_box]" );
		}
	}
	$twitter_response = ob_get_clean();


	ob_start();
	if ( $customize_colors ){
		echo "
		#{$id}{
			color: {$custom_font_color};
		}
		#{$id} .owl-pagination .owl-page{
			background-color: {$custom_font_color};
		}
		#{$id} .owl-pagination .owl-page{
			border-color: {$custom_font_color};
		}
		#{$id} .cws_twitter_icon,
			#{$id} .tweet .date{
			color: {$custom_featured_color};
		}
		#{$id} .cws_twitter_icon,
		#{$id} .owl-pagination .owl-page.active{
			border-color: {$custom_featured_color};
		}
		#{$id} .owl-pagination .owl-page.active{
			background-color: transparent;
		}
		";
	}
	$styles = ob_get_clean();

	ob_start();
	if ( $tweets_received ){
		!empty( $styles ) ? Cws_shortcode_css()->enqueue_cws_css($styles) : "";
		echo "<div id='$id' class='$classes'>";
			if ( !empty( $icon ) ){
				echo "<div class='cws_twitter_icon'>";
					echo "<i class='$icon'></i>";
				echo "</div>";
			}
			echo $twitter_response;
		echo "</div>";
	}
	else{
		echo $twitter_response;
	}
	$out = ob_get_clean();
	return $out;
}
add_shortcode( 'cws_sc_twitter', 'cws_vc_shortcode_sc_twitter' );

/**
 * Processes like/unlike
 * @since    0.5
 */
add_action( 'wp_enqueue_scripts', 'cws_vc_shortcode_sl_enqueue_scripts' );
function cws_vc_shortcode_sl_enqueue_scripts() {
	wp_enqueue_script( 'simple-likes-public-js', CWS_SHORTCODES_PLUGIN_URL . '/assets/js/simple-likes-public.js', array( 'jquery' ), '0.5', false );
	wp_localize_script( 'simple-likes-public-js', 'simpleLikes', array(
		'ajaxurl' => admin_url( 'admin-ajax.php' ),
		'like' => esc_html__( 'Like', 'cws-essentials' ),
		'unlike' => esc_html__( 'Unlike', 'cws-essentials' )
	)); 
}

add_action( 'wp_ajax_nopriv_cws_vc_shortcode_process_simple_like', 'cws_vc_shortcode_process_simple_like' );
add_action( 'wp_ajax_cws_vc_shortcode_process_simple_like', 'cws_vc_shortcode_process_simple_like' );
function cws_vc_shortcode_process_simple_like() {
	// Security
	$nonce = isset( $_REQUEST['nonce'] ) ? sanitize_text_field( $_REQUEST['nonce'] ) : 0;
	if ( !wp_verify_nonce( $nonce, 'simple-likes-nonce' ) ) {
		exit( __( 'Not permitted', 'cws-essentials' ) );
	}
	// Test if javascript is disabled
	$disabled = ( isset( $_REQUEST['disabled'] ) && $_REQUEST['disabled'] == true ) ? true : false;
	// Test if this is a comment
	$is_comment = ( isset( $_REQUEST['is_comment'] ) && $_REQUEST['is_comment'] == 1 ) ? 1 : 0;
	// Base variables
	$post_id = ( isset( $_REQUEST['post_id'] ) && is_numeric( $_REQUEST['post_id'] ) ) ? $_REQUEST['post_id'] : '';
	$result = array();
	$post_users = NULL;
	$like_count = 0;
	// Get plugin options
	if ( $post_id != '' ) {
		$count = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_comment_like_count", true ) : get_post_meta( $post_id, "_post_like_count", true ); // like count
		$count = ( isset( $count ) && is_numeric( $count ) ) ? $count : 0;
		if ( !cws_vc_shortcode_already_liked( $post_id, $is_comment ) ) { // Like the post
			if ( is_user_logged_in() ) { // user is logged in
				$user_id = get_current_user_id();
				$post_users = cws_vc_shortcode_post_user_likes( $user_id, $post_id, $is_comment );
				if ( $is_comment == 1 ) {
					// Update User & Comment
					$user_like_count = get_user_option( "_comment_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					update_user_option( $user_id, "_comment_like_count", ++$user_like_count );
					if ( $post_users ) {
						update_comment_meta( $post_id, "_user_comment_liked", $post_users );
					}
				} else {
					// Update User & Post
					$user_like_count = get_user_option( "_user_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					update_user_option( $user_id, "_user_like_count", ++$user_like_count );
					if ( $post_users ) {
						update_post_meta( $post_id, "_user_liked", $post_users );
					}
				}
			} else { // user is anonymous
				$user_ip = cws_vc_shortcode_sl_get_ip();
				$post_users = cws_vc_shortcode_post_ip_likes( $user_ip, $post_id, $is_comment );
				// Update Post
				if ( $post_users ) {
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_IP", $post_users );
					} else { 
						update_post_meta( $post_id, "_user_IP", $post_users );
					}
				}
			}
			$like_count = ++$count;
			$response['status'] = "liked";
			$response['icon'] = cws_vc_shortcode_get_liked_icon();
		} else { // Unlike the post
			if ( is_user_logged_in() ) { // user is logged in
				$user_id = get_current_user_id();
				$post_users = cws_vc_shortcode_post_user_likes( $user_id, $post_id, $is_comment );
				// Update User
				if ( $is_comment == 1 ) {
					$user_like_count = get_user_option( "_comment_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					if ( $user_like_count > 0 ) {
						update_user_option( $user_id, "_comment_like_count", --$user_like_count );
					}
				} else {
					$user_like_count = get_user_option( "_user_like_count", $user_id );
					$user_like_count =  ( isset( $user_like_count ) && is_numeric( $user_like_count ) ) ? $user_like_count : 0;
					if ( $user_like_count > 0 ) {
						update_user_option( $user_id, '_user_like_count', --$user_like_count );
					}
				}
				// Update Post
				if ( $post_users ) {	
					$uid_key = array_search( $user_id, $post_users );
					unset( $post_users[$uid_key] );
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_liked", $post_users );
					} else { 
						update_post_meta( $post_id, "_user_liked", $post_users );
					}
				}
			} else { // user is anonymous
				$user_ip = cws_vc_shortcode_sl_get_ip();
				$post_users = cws_vc_shortcode_post_ip_likes( $user_ip, $post_id, $is_comment );
				// Update Post
				if ( $post_users ) {
					$uip_key = array_search( $user_ip, $post_users );
					unset( $post_users[$uip_key] );
					if ( $is_comment == 1 ) {
						update_comment_meta( $post_id, "_user_comment_IP", $post_users );
					} else { 
						update_post_meta( $post_id, "_user_IP", $post_users );
					}
				}
			}
			$like_count = ( $count > 0 ) ? --$count : 0; // Prevent negative number
			$response['status'] = "unliked";
			$response['icon'] = cws_vc_shortcode_get_unliked_icon();
		}
		if ( $is_comment == 1 ) {
			update_comment_meta( $post_id, "_comment_like_count", $like_count );
			update_comment_meta( $post_id, "_comment_like_modified", date( 'Y-m-d H:i:s' ) );
		} else { 
			update_post_meta( $post_id, "_post_like_count", $like_count );
			update_post_meta( $post_id, "_post_like_modified", date( 'Y-m-d H:i:s' ) );
		}
		$response['count'] = get_like_count( $like_count );
		$response['testing'] = $is_comment;
		if ( $disabled == true ) {
			if ( $is_comment == 1 ) {
				wp_redirect( get_permalink( get_the_ID() ) );
				exit();
			} else {
				wp_redirect( get_permalink( $post_id ) );
				exit();
			}
		} else {
			wp_send_json( $response );
		}
	}
}

function cws_vc_shortcode_already_liked( $post_id, $is_comment ) {
	$post_users = NULL;
	$user_id = NULL;
	if ( is_user_logged_in() ) { // user is logged in
		$user_id = get_current_user_id();
		$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_liked" ) : get_post_meta( $post_id, "_user_liked" );
		if ( count( $post_meta_users ) != 0 ) {
			$post_users = $post_meta_users[0];
		}
	} else { // user is anonymous
		$user_id = cws_vc_shortcode_sl_get_ip();
		$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_IP" ) : get_post_meta( $post_id, "_user_IP" ); 
		if ( count( $post_meta_users ) != 0 ) { // meta exists, set up values
			$post_users = $post_meta_users[0];
		}
	}
	if ( is_array( $post_users ) && in_array( $user_id, $post_users ) ) {
		return true;
	} else {
		return false;
	}
}

function cws_vc_shortcode_get_simple_likes_button( $post_id, $is_comment = NULL ) {
	$is_comment = ( NULL == $is_comment ) ? 0 : 1;
	$output = '';
	$nonce = wp_create_nonce( 'simple-likes-nonce' ); // Security
	if ( $is_comment == 1 ) {
		$post_id_class = esc_attr( ' sl-comment-button-' . $post_id );
		$comment_class = esc_attr( ' sl-comment' );
		$like_count = get_comment_meta( $post_id, "_comment_like_count", true );
		$like_count = ( isset( $like_count ) && is_numeric( $like_count ) ) ? $like_count : 0;
	} else {
		$post_id_class = esc_attr( ' sl-button-' . $post_id );
		$comment_class = esc_attr( '' );
		$like_count = get_post_meta( $post_id, "_post_like_count", true );
		$like_count = ( isset( $like_count ) && is_numeric( $like_count ) ) ? $like_count : 0;
	}
	$count = get_like_count( $like_count );
	$icon_empty = cws_vc_shortcode_get_unliked_icon();
	$icon_full = cws_vc_shortcode_get_liked_icon();
	// Loader
	$loader = '<span class="sl-loader"></span>';
	// Liked/Unliked Variables
	if ( cws_vc_shortcode_already_liked( $post_id, $is_comment ) ) {
		$class = esc_attr( ' liked' );
		$title = __( 'Unlike', 'cws-essentials' );
		$icon = $icon_full;
	} else {
		$class = '';
		$title = __( 'Like', 'cws-essentials' );
		$icon = $icon_empty;
	}
	$output = '<span class="sl-wrapper"><a href="' . admin_url( 'admin-ajax.php?action=cws_vc_shortcode_process_simple_like' . '&post_id=' . $post_id . '&nonce=' . $nonce . '&is_comment=' . $is_comment . '&disabled=true' ) . '" class="sl-button' . $post_id_class . $class . $comment_class . '" data-nonce="' . $nonce . '" data-post-id="' . $post_id . '" data-iscomment="' . $is_comment . '" title="' . $title . '">' . $icon . $count . '</a>' . $loader . '</span>';
	return $output;
} 

add_shortcode( 'jmliker', 'cws_vc_shortcode_sl_shortcode' );
function cws_vc_shortcode_sl_shortcode() {
	return cws_vc_shortcode_get_simple_likes_button( get_the_ID(), 0 );
}

function cws_vc_shortcode_post_user_likes( $user_id, $post_id, $is_comment ) {
	$post_users = '';
	$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_liked" ) : get_post_meta( $post_id, "_user_liked" );
	if ( count( $post_meta_users ) != 0 ) {
		$post_users = $post_meta_users[0];
	}
	if ( !is_array( $post_users ) ) {
		$post_users = array();
	}
	if ( !in_array( $user_id, $post_users ) ) {
		$post_users['user-' . $user_id] = $user_id;
	}
	return $post_users;
}

function cws_vc_shortcode_post_ip_likes( $user_ip, $post_id, $is_comment ) {
	$post_users = '';
	$post_meta_users = ( $is_comment == 1 ) ? get_comment_meta( $post_id, "_user_comment_IP" ) : get_post_meta( $post_id, "_user_IP" );
	// Retrieve post information
	if ( count( $post_meta_users ) != 0 ) {
		$post_users = $post_meta_users[0];
	}
	if ( !is_array( $post_users ) ) {
		$post_users = array();
	}
	if ( !in_array( $user_ip, $post_users ) ) {
		$post_users['ip-' . $user_ip] = $user_ip;
	}
	return $post_users;
}

function cws_vc_shortcode_sl_get_ip() {
	if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) && ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
		$ip = $_SERVER['HTTP_CLIENT_IP'];
	} elseif ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) && ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
		$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
	} else {
		$ip = ( isset( $_SERVER['REMOTE_ADDR'] ) ) ? $_SERVER['REMOTE_ADDR'] : '0.0.0.0';
	}
	$ip = filter_var( $ip, FILTER_VALIDATE_IP );
	$ip = ( $ip === false ) ? '0.0.0.0' : $ip;
	return $ip;
}

function cws_vc_shortcode_get_liked_icon() {
	/* If already using Font Awesome with your theme, replace svg with: <i class="fa fa-heart"></i> */
	$icon = '<span class="sl-icon unliked"></span>';
	return $icon;
}

function cws_vc_shortcode_get_unliked_icon() {
	/* If already using Font Awesome with your theme, replace svg with: <i class="fa fa-heart-o"></i> */
	$icon = '<span class="sl-icon liked"></span>';
	return $icon;
}

function cws_vc_shortcode_sl_format_count( $number ) {
	$precision = 2;
	if ( $number >= 1000 && $number < 1000000 ) {
		$formatted = number_format( $number/1000, $precision ).'K';
	} else if ( $number >= 1000000 && $number < 1000000000 ) {
		$formatted = number_format( $number/1000000, $precision ).'M';
	} else if ( $number >= 1000000000 ) {
		$formatted = number_format( $number/1000000000, $precision ).'B';
	} else {
		$formatted = $number; // Number is less than 1000
	}
	$formatted = str_replace( '.00', '', $formatted );
	return $formatted;
}

function get_like_count( $like_count ) {
	$like_text = __( '0', 'cws-essentials' );
	if ( is_numeric( $like_count ) && $like_count > 0 ) { 
		$number = cws_vc_shortcode_sl_format_count( $like_count );
	} else {
		$number = $like_text;
	}
	$count = '<span class="sl-count">' . $number . ' '. esc_html__('likes', 'cws-essentials'). '</span>';
	return $count;
}

// User Profile List
add_action( 'show_user_profile', 'cws_vc_shortcode_show_user_likes' );
add_action( 'edit_user_profile', 'cws_vc_shortcode_show_user_likes' );
function cws_vc_shortcode_show_user_likes( $user ) { ?>        
	<table class="form-table">
		<tr>
			<th><label for="user_likes"><?php _e( 'You Like:', 'cws-essentials' ); ?></label></th>
			<td>
				<?php
				$types = get_post_types( array( 'public' => true ) );
				$args = array(
					'numberposts' => -1,
					'post_type' => $types,
					'meta_query' => array (
						array (
							'key' => '_user_liked',
							'value' => $user->ID,
							'compare' => 'LIKE'
							)
						) );		
				$sep = '';
				$like_query = new WP_Query( $args );
				if ( $like_query->have_posts() ) : ?>
					<p>
						<?php while ( $like_query->have_posts() ) : $like_query->the_post(); 
						echo sprintf('%s', $sep); ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
						<?php
						$sep = ' &middot; ';
						endwhile; 
						?>
					</p>
				<?php else : ?>
				<p><?php _e( 'You do not like anything yet.', 'cws-essentials' ); ?></p>
				<?php 
				endif; 
				wp_reset_postdata(); 
				?>
			</td>
		</tr>
	</table>
<?php }

?>