<?php
	$paged = !empty($_POST['paged']) ? (int)$_POST['paged'] : (!empty($_GET['paged']) ? (int)$_GET['paged'] : ( get_query_var("paged") ? get_query_var("paged") : 1 ) );
	$posts_per_page = (int)get_option('posts_per_page');
	$search_terms = get_query_var( 'search_terms' );

	get_header();
	global $cws_theme_funcs;
	global $cws_theme_default;

	$pid = get_queried_object_id();
	if ($cws_theme_funcs){
		$sb = $cws_theme_funcs->cws_render_sidebars( get_queried_object_id() );
		$fixed_header = $cws_theme_funcs->cws_get_meta_option( 'fixed_header' );
		$class = $sb['layout_class'].' '. $sb['sb_class'];
		$sb['sb_class'] = apply_filters('cws_print_single_class', $class);
	}
?>
<div class="page_content search_results <?php echo (isset($sb['sb_class']) ? $sb['sb_class'] : ''); ?>">
	<?php
	echo (isset($sb['content']) ? $sb['content'] : '<div class="container">');
	$blogtype = !empty($cws_theme_funcs) ? $cws_theme_funcs->cws_get_meta_option('blogtype') : "";
	$news_class = !empty( $blogtype ) ? ( preg_match( '#^\d+$#', $blogtype ) ? "news-pinterest" : "news-$blogtype" ) : "news-medium";
	$grid_class = $news_class == "news-pinterest" ? "grid-$blogtype isotope" : "";

	?>
	<main>
		<div class="grid_row clearfix">
			<?php
			global $wp_query;
			$total_post_count = $wp_query->found_posts;
			$max_paged = ceil( $total_post_count / $posts_per_page );
			if ( 0 === strlen($wp_query->query_vars['s'] ) ){
				$message_title = esc_html__( 'Empty search string', 'cryptop' );
				$message = esc_html__( 'Please, enter some characters to search field', 'cryptop' );
				if(!empty($cws_theme_funcs)){
					print $cws_theme_funcs->cws_print_search_form($message_title, $message);
				}else{
					print $cws_theme_default->cws_print_search_form($message_title, $message);
				}
				
			} else {

				if(have_posts()){
					?>
					<section class="news <?php print $news_class; ?>">
						<div class='cws_wrapper'>
							<div class="grid <?php print $grid_class; ?>">
							<?php
								wp_enqueue_script ('isotope');
								$use_pagination = $max_paged > 1;
									while( have_posts() ) : the_post();
										$content = get_the_content();
										$content = preg_replace( '/\[.*?(\"title\":\"(.*?)\").*?\]/', '$2', $content );
										$content = preg_replace( '/\[.*?(|title=\"(.*?)\".*?)\]/', '$2', $content );
										$content = strip_tags( $content );
										$content = preg_replace( '|\s+|', ' ', $content );
										$title = get_the_title();

										$cont = '';
										$bFound = false;
										$contlen = strlen( $content );
										foreach ($search_terms as $term) {
											$pos = 0;
											$term_len = strlen($term);
											do {
												if ( $contlen <= $pos ) {
													break;
												}
												$pos = stripos( $content, $term, $pos );
												if ( $pos ) {
													$start = ($pos > 50) ? $pos - 50 : 0;
													$temp = substr( $content, $start, $term_len + 100 );
													$cont .= ! empty( $temp ) ? $temp . ' ... ' : '';
													$pos += $term_len + 50;
												}
											} while ($pos);
										}

										if (strlen($cont) > 0) {
											$bFound = true;
										}
										else {
											$cont = mb_substr( $content, 0, $contlen < 100 ? $contlen : 100 );
											if ( $contlen > 100 ) {
												$cont .= '...';
											}
											$bFound = true;
										}
										$pattern = "#\[[^\]]+\]#";
										$replace = "";
										$cont = preg_replace($pattern, $replace, $cont);
										$cont = preg_replace('/('.implode('|', $search_terms) .')/iu', '<mark>\0</mark>', $cont);
										$permalink = esc_url( get_the_permalink() );
										$button_word = esc_html__( 'Read More', 'cryptop' );
										$title = get_the_title();
										$title = preg_replace( '/('.implode( '|', $search_terms ) .')/iu', '<mark>\0</mark>', $title );
										echo "<article class='item small'>";
											echo !empty( $title ) ? (!empty($cws_theme_funcs) ? $cws_theme_funcs::THEME_BEFORE_CE_TITLE : $cws_theme_default::THEME_BEFORE_CE_TITLE) . "<span><a href='$permalink'>$title</a></span>" . (!empty($cws_theme_funcs) ? $cws_theme_funcs::THEME_AFTER_CE_TITLE : $cws_theme_default::THEME_AFTER_CE_TITLE) : "";

											echo "<div class='post_content'>" . apply_filters( 'the_content', $cont ) . "</div>";

											echo "<div class='post_bottom_wrapper meta-bottom'>";

											// ================AUTHOR PART================
											$author = get_the_author();
											ob_start();
											the_author_posts_link();
											$author_link = ob_get_clean();

											if ( !empty($author) ) {
												echo "<div class='info'>";
												ob_start();
												echo !empty($author) ? (esc_html__('by ', 'cryptop'))."<span class='post_author'>$author_link</span>" : '';
												$author_part = ob_get_clean();
												echo sprintf("%s", $author_part);
												echo '</div>';
											}
													// ================/AUTHOR PART================	

													echo '<div class="post_meta post_info post_post_meta posts_grid_post_meta">';

											// ================COMMENTS PART================
											$comments_n = get_comments_number();
											if ( (int) $comments_n > 0 ) {
												$permalink .= "#comments";
															echo "<div class='post_comments'><i class='flaticon-communication'></i>";
												$comments_part = "<a href='$permalink'>$comments_n <span> ".esc_html__('comments', 'cryptop')."</span></a>";
															echo $comments_part;
															echo "</div>";
											}
											// ================/COMMENTS PART================

													echo '</div>';

											// ================META PART================
												echo "<div class='post_meta post_categories'>";			
													if ( has_category() ) {
														echo "<div class='post_category'>";						
															$cats = "";
															if ( has_category() ) {
																ob_start();
																the_category ( ", " );
																$cats .= ob_get_clean();
															}
															if ( !empty( $cats ) ){
																echo "<div class='post_terms post_meta post_post_terms posts_grid_post_terms'>";
																		echo "<i class='flaticon-shapes'></i>";
																	printf("%s", $cats);
																echo "</div>";
															}					
														echo '</div>';
													}
											
													if ( has_tag() ) {
														echo "<div class='post_tags'>";							
															$tags = "";
															if ( has_tag() ) {
																ob_start();
																the_tags ( "", ", ", "" );
																$tags .= ob_get_clean();
															}
															if ( !empty( $tags ) ){
																echo "<div class='post_terms post_meta post_post_terms posts_grid_post_terms'>";
																		echo "<i class='flaticon-business-1'></i>";
																	printf("%s", $tags);
																echo "</div>";
															}
														echo '</div>';
													}				
												echo '</div>';	

													echo "<div class='post_button clearfix btn-read-more'>";
														echo "<a href='$permalink' class='more-link'>$button_word</a>";
													echo "</div>";

											echo '</div>';


										echo "</article>";
									endwhile;
									wp_reset_postdata();
								?>
							</div>
						</div>
					</section>
					<?php
					if ( $use_pagination ) {
						cws_pagination($paged, $max_paged);
					}
				}
				else {
					$message_title = esc_html__( 'No search Results', 'cryptop' );
					$message = esc_html__( 'There are no posts matching your query', 'cryptop' );
					if(!empty($cws_theme_funcs)){
						print $cws_theme_funcs->cws_print_search_form($message_title, $message);
					}else{
						print $cws_theme_default->cws_print_search_form($message_title, $message);
					}
				}
			}
			?>
		</div>
	</main>
	<?php echo (isset($sb['content']) ? $sb['content'] : '</div>'); ?>
</div>

<?php

get_footer ();
?>