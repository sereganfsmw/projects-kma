<?php
	// Map Shortcode in Visual Composer
	global $cws_theme_funcs;
	vc_map( array(
		"name"				=> esc_html__( 'Text Widget', 'cryptop' ),
		"base"				=> "cws_sc_widget_text",
		'category'			=> "CWS Widgets",
		"icon"     			=> "cws_icon",
		"weight"			=> 80,
		"params"			=> array(
			array(
				"type"			=> "textfield",
				"admin_label"	=> true,
				"heading"		=> esc_html__( 'Title', 'cryptop' ),
				"param_name"	=> "title",
			),
			array(
				"type"			=> "iconpicker",
				"heading"		=> esc_html__( 'Widget Icon', 'cryptop' ),
				"param_name"	=> "icon",
			),
			array(
				"type"			=> "textarea",
				"heading"		=> esc_html__( 'Widget Content', 'cryptop' ),
				"param_name"	=> "text"
			),
			array(
				"type"			=> "checkbox",
				"param_name"	=> "add_custom_color",
				"value"			=> array( esc_html__( 'Add Custom Color', 'cryptop' ) => true )
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Custom Color', 'cryptop' ),
				"param_name"	=> "color",
				"dependency"	=> array(
					"element"		=> "add_custom_color",
					"not_empty"		=> true
				)
			),
			array(
				"type"				=> "textfield",
				"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
				"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
				"param_name"		=> "el_class",
				"value"				=> ""
			)
		)
	));

	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Widget_Text extends WPBakeryShortCode {
	    }
	}
?>