<?php
	global $cws_theme_funcs;
	$params = array(
		array(
			"type"			=> "textfield",
			"admin_label"	=> true,
			"heading"		=> esc_html__( 'Title', 'cryptop' ),
			"param_name"	=> "title",
			"value"			=> ""
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Title Alignment', 'cryptop' ),
			"param_name"	=> "title_align",
			"value"			=> array(
				esc_html__( "Left", 'cryptop' ) 	=> 'left',
				esc_html__( "Right", 'cryptop' )	=> 'right',
				esc_html__( "Center", 'cryptop' )	=> 'center'
			)		
		),
		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__( 'Title & filter style', 'cryptop' ),
			'param_name'	=> 'title_filter_row',
			"dependency" 	=> array(
								"element"	=> 'display_style',
								"value"		=> array( "filter","filter_with_ajax" )
			),			
			'value'			=> array(
					esc_html__( 'Stretch', 'cryptop' ) => 'stretch',
					esc_html__( 'Default', 'cryptop' ) => 'default',
				),
		),			
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Layout', 'cryptop' ),
			"param_name"	=> "display_style",
			"value"			=> array(
								esc_html__( 'Grid', 'cryptop' ) => 'grid',
								esc_html__( 'Grid with Filter', 'cryptop' ) => 'filter',
								esc_html__( 'Grid with Filter(Ajax)', 'cryptop' ) => 'filter_with_ajax',
								esc_html__( 'Carousel', 'cryptop' ) => 'carousel',
								esc_html__( 'Showcase', 'cryptop' ) => 'showcase',
							)
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Columns', 'cryptop' ),
			"param_name"	=> "layout",
			"value"			=> array(
				esc_html__( 'Default', 'cryptop' ) => 'def',
				esc_html__( 'One Column', 'cryptop' ) => '1',
				esc_html__( 'Two Columns', 'cryptop' ) => '2',
				esc_html__( 'Three Columns', 'cryptop' ) => '3',
				esc_html__( 'Four Columns', 'cryptop' ) => '4',
				esc_html__( 'Five Columns', 'cryptop' ) => '5'
			)
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "crop_images",
			"dependency" 	=> array(
								"element"	=> 'layout',
								"value"		=> array( "def","2","3","4","5" )
							),
			'value'			=> array(
				esc_html__( 'Crop images', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "masonry",
			"dependency" 	=> array(
								"element"	=> 'crop_images',
								'not_empty'	=> true
							),
			'value'			=> array(
				esc_html__( 'Masonry', 'cryptop' ) => true
			)
		),
	);
	$taxes = get_object_taxonomies ( 'cws_portfolio', 'object' );
	$avail_taxes = array(
		esc_html__( 'None', 'cryptop' )	=> '',
		esc_html__( 'Titles', 'cryptop' )	=> 'title',
	);
	foreach ( $taxes as $tax => $tax_obj ){
		$tax_name = isset( $tax_obj->labels->name ) && !empty( $tax_obj->labels->name ) ? $tax_obj->labels->name : $tax;
		$avail_taxes[$tax_name] = $tax;
	}
	array_push( $params, array(
		"type"				=> "dropdown",
		"heading"			=> esc_html__( 'Filter by', 'cryptop' ),
		"param_name"		=> "tax",
		"value"				=> $avail_taxes
	));
	foreach ( $avail_taxes as $tax_name => $tax ) {
		if ($tax == 'title'){
			$custom_post_type = 'cws_portfolio';
			global $wpdb;
    		$results = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type LIKE %s and post_status = 'publish'", $custom_post_type ) );
    		$titles_arr = array();
		    foreach( $results as $index => $post ) {
		    	$post_title = $post->post_title;
		        $titles_arr[$post_title] =  $post->ID;
		    }
			array_push( $params, array(
				"type"			=> "cws_dropdown",
				"multiple"		=> "true",
				"heading"		=> esc_html__( 'Titles', 'cryptop' ),
				"param_name"	=> "titles",
				"dependency"	=> array(
									"element"	=> "tax",
									"value"		=> 'title'
								),
				"value"			=> $titles_arr
			));		
		} else {
			$terms = get_terms( $tax );
			$avail_terms =  array();
			$hierarchy = _get_term_hierarchy($tax);
			if ( !is_a( $terms, 'WP_Error' ) ){
				foreach($terms as $term) {
					if(isset($term)){
						if($term->parent) {
							continue;
						} 			
						$avail_terms[] = $term->name;  
						if(isset($hierarchy[$term->term_id])) {	
							$children = _get_term_children($term->term_id, $terms, $tax);										
							foreach($children as $child) {
								$child = get_term($child, $tax);
								$ancestors = get_ancestors( $child->term_id, $child->taxonomy );
								$depth = $ancestors = count($ancestors);
								if($child->count > 0){
									if($depth <= $ancestors){							
										$avail_terms[] =  str_repeat("-", $depth) . ' ('.$term->name.') '.$child->slug;
									}
								}
							}
						}					
					}				
				}

			}

			array_push( $params, array(
				"type"			=> "cws_dropdown",
				"multiple"		=> "true",
				"heading"		=> $tax_name,
				"param_name"	=> "{$tax}_terms",
				"dependency"	=> array(
									"element"	=> "tax",
									"value"		=> $tax
								),
				"value"			=> $avail_terms, 
			));	
		} 		
	}
	$params2 = array(
		array(
			"type"			=> "checkbox",
			"param_name"	=> "en_isotope",
			"dependency" 	=> array(
					"element"	=> 'display_style',
					"value"		=> 'grid'
				),
			'value'			=> array(
				esc_html__( 'Use Isotope', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "carousel_auto",
			"dependency" 	=> array(
								"element"	=> 'display_style',
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'AutoPlay Carousel', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "carousel_pagination",
			"dependency" 	=> array(
								"element"	=> 'display_style',
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'Pagination', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Hover', 'cryptop' ),
			"param_name"	=> "anim_style",
			"value"			=> array(
								esc_html__( 'Default Hover', 'cryptop' ) => 'hoverdef',
								esc_html__( 'Solid Color with Zoomed Border', 'cryptop' ) => 'hoverbi',
								esc_html__( 'Solid Color with Border', 'cryptop' ) => 'hoverbi2',
								esc_html__( 'Mouse Follow', 'cryptop' ) => 'hoverdir',
								// esc_html__( '3D Hover', 'cryptop' ) => 'hover3d',
								esc_html__( 'Zoom Slow Motion', 'cryptop' ) => 'hoverzs',
								esc_html__( 'Zoom With Rotation', 'cryptop' ) => 'hoversr',
								esc_html__( 'Zoom With Blur', 'cryptop' ) => 'hoverzb',
								esc_html__( 'No Hover With Link', 'cryptop' ) => 'hover_none_link',
								esc_html__( 'No Hover', 'cryptop' ) => 'hover_none',
							),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency" 	=> array(
					"element"	=> 'display_style',
					"value"		=> array( "carousel" ,"grid" , "filter", "filter_with_ajax" )
				),
		),
		array(
			'type'				=> 'cws_dropdown',
			'multiple'			=> "true",
			'heading'			=> esc_html__( 'Show', 'cryptop' ),
			'param_name'		=> 'link_show',					
			"dependency" 	=> array(
					"element"	=> 'anim_style',
					"value"		=> array( "hoverdef" ,"hoverdir" , "hoverbi", "hoverbi2", "hoverzs", "hoversr", "hoverzb" )
				),
			'value'				=> array(
				esc_html__( 'Make Image Clickable', 'cryptop' )	=> 'area_link',
				esc_html__( 'Show Image PoPup Icon', 'cryptop' )	=> 'popup_link',
				esc_html__( 'Show Project Details Icon', 'cryptop' )	=> 'single_link',
			)
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "en_hover_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Edit Fill Color', 'cryptop' ) => true
			),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency" 	=> array(
					"element"	=> 'anim_style',
					"value"		=> array( "hoverdef" ,"hoverdir" , "hoverbi", "hoverbi2", "hoverzs", "hoversr", "hoverzb" )
				),
		),		
		array(
			"type"			=> "colorpicker",
			"param_name"	=> "hover_color",
			"dependency"	=> array(
				"element"	=> "en_hover_color",
				"not_empty"	=> true
			),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"value"			=> "rgba(0,0,0,0.7)"
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "en_title_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Edit Title Color', 'cryptop' ) => true
			),
			"dependency" 	=> array(
					"element"	=> 'anim_style',
					"value"		=> array( "hoverdef" ,"hoverdir" , "hoverbi", "hoverbi2", "hoverzs", "hoversr", "hoverzb" )
				),
		),
		array(
			"type"			=> "colorpicker",
			"param_name"	=> "title_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "en_title_color",
				"not_empty"	=> true
			),
			"value"			=> "#ffffff"
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "en_cat_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Edit Category Color', 'cryptop' ) => true
			),
			"dependency" 	=> array(
					"element"	=> 'anim_style',
					"value"		=> array( "hoverdef" ,"hoverdir" , "hoverbi", "hoverbi2", "hoverzs", "hoversr", "hoverzb" )
				),
		),
		array(
			"type"			=> "colorpicker",
			"param_name"	=> "cat_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "en_cat_color",
				"not_empty"	=> true
			),
			"value"			=> ""
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Animation', 'cryptop' ),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"param_name"	=> "appear_style",
			"value"			=> array(
								esc_html__( 'None', 'cryptop' ) => 'none',
								esc_html__( 'Bounce', 'cryptop' ) => 'callout.bounce',
								esc_html__( 'Shake', 'cryptop' ) => 'callout.shake',
								esc_html__( 'Flash', 'cryptop' ) => 'callout.flash',
								esc_html__( 'Pulse', 'cryptop' ) => 'callout.pulse',
								esc_html__( 'Swing', 'cryptop' ) => 'callout.swing',
								esc_html__( 'Tada', 'cryptop' ) => 'callout.tada',
								esc_html__( 'Fade In', 'cryptop' ) => 'transition.fadeIn',
								esc_html__( 'Flip X In', 'cryptop' ) => 'transition.flipXIn',
								esc_html__( 'Flip Y In', 'cryptop' ) => 'transition.flipYIn',
								esc_html__( 'Shrink In', 'cryptop' ) => 'transition.shrinkIn',
								esc_html__( 'Expand In', 'cryptop' ) => 'transition.expandIn',
								esc_html__( 'Grow', 'cryptop' ) => 'transition.grow',
								esc_html__( 'Slide Up', 'cryptop' ) => 'transition.slideUpBigIn',
								esc_html__( 'Slide Down', 'cryptop' ) => 'transition.slideDownBigIn',
								esc_html__( 'Slide Left', 'cryptop' ) => 'transition.slideLeftBigIn',
								esc_html__( 'Slide Right', 'cryptop' ) => 'transition.slideRightBigIn',
								esc_html__( 'Perspective Up', 'cryptop' ) => 'transition.perspectiveUpIn',
								esc_html__( 'Perspective Down', 'cryptop' ) => 'transition.perspectiveDownIn',
								esc_html__( 'Perspective Left', 'cryptop' ) => 'transition.perspectiveLeftIn',
								esc_html__( 'Perspective Right', 'cryptop' ) => 'transition.perspectiveRightIn',
							),
			"dependency" 	=> array(
					"element"	=> 'display_style',
					"value"		=> array( "carousel" ,"grid" , "filter", "filter_with_ajax" )
				),
		),

		array(
			'type'			=> 'dropdown',
			'heading'		=> esc_html__( 'Show Title and Description', 'cryptop' ),
			'param_name'	=> 'info_pos',
			'value'			=> array(
					esc_html__( 'On Image Hover', 'cryptop' ) => 'inside_img',
					esc_html__( 'Under Image', 'cryptop' ) => 'under_img',
				),
			"dependency" 	=> array(
								"element"	=> 'layout',
								"value"		=> array( "def","1","2","3","4","5" )
				),
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "add_divider",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Add Divider', 'cryptop' ) => true
			),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency" 	=> array(
					"element"	=> 'info_pos',
					"value"		=> "under_img"
				),
		),		
		array(
			"type"			=> "checkbox",
			"param_name"	=> "customize_carousel",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Customize Carousel', 'cryptop' ) => true
			),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency" 	=> array(
					"element"	=> 'display_style',
					"value"		=> "carousel"
				),
		),
		array(
			"type"			=> "colorpicker",
			"param_name"	=> "pagination_carousel",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"heading"		=> esc_html__( 'Pagination Color', 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "customize_carousel",
				"not_empty"	=> true
			),
			"value"			=> ""
		),		
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Info Alignment', 'cryptop' ),
			"param_name"	=> "info_align",
			"value"			=> array(
					esc_html__( 'Center', 'cryptop' ) 	=> 'center',
					esc_html__( 'Left', 'cryptop' )	=> 'left',
					esc_html__( 'Right', 'cryptop' ) 	=> 'right',
							),
			"dependency" 	=> array(
								"element"	=> 'info_pos',
								"value"		=> 'under_img'
				),
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Shape', 'cryptop' ),
			"param_name"	=> "portfolio_style",
			"value"			=> array(
					esc_html__( 'Square', 'cryptop' ) => 'def_style',
					esc_html__( 'Square with no spacing', 'cryptop' ) => 'wide_style',
							),
			"dependency" 	=> array(
								"element"	=> 'info_pos',
								"value"		=> 'inside_img'
				),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
		),
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items to display', 'cryptop' ),
			"param_name"	=> "total_items_count",
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		),
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items per Page', 'cryptop' ),
			"param_name"	=> "items_pp",
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "grid", "filter", "filter_with_ajax" )
							),
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'cws_portfolio_show_data_override',
			'value'			=> array(
				esc_html__( 'Show Meta Data', 'cryptop' ) => true
			)
		),		
		array(
			'type'				=> 'cws_dropdown',
			'multiple'			=> "true",
			'param_name'		=> 'cws_portfolio_data_to_show',
			'dependency'		=> array(
				'element'			=> 'cws_portfolio_show_data_override',
				'not_empty'			=> true
			),
			'value'				=> array(
				esc_html__( 'None', 'cryptop' )			=> '',
				esc_html__( 'Title', 'cryptop' )		=> 'title',
				esc_html__( 'Excerpt', 'cryptop' )		=> 'excerpt',
				esc_html__( 'Categories', 'cryptop' )	=> 'cats'
			)
		),	
		array(
			'type'			=> 'textfield',
			'heading'		=> esc_html__( 'Content Character Limit', 'cryptop' ),
			'param_name'	=> 'chars_count',
			'dependency'	=> array(
				'element'		=> 'cws_portfolio_show_data_override',
				'not_empty'		=> true
			),
			'value'			=> 	''	
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Pagination', 'cryptop' ),
			"param_name"	=> "pagination_grid",
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "grid", "filter", "filter_with_ajax" )
							),
			"value"			=> array(
				esc_html__( "Standard", 'cryptop' ) 	=> 'standard_with_ajax',
				esc_html__( "Load More", 'cryptop' )	=> 'load_more',
			)		
		),
			
	);
	$params = array_merge($params, $params2);
	array_push( $params, array(
		"type"				=> "textfield",
		"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
		"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
		"param_name"		=> "el_class",
		"value"				=> ""
	));
	vc_map( array(
		"name"				=> esc_html__( 'CWS Portfolio', 'cryptop' ),
		"base"				=> "cws_sc_portfolio_posts_grid",
		'category'			=> "By CWS",
		"weight"			=> 80,
		"icon"     			=> "cws_icon",		
		"params"			=> $params
	));
	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Portfolio_Posts_Grid extends WPBakeryShortCode {
	    }
	}
?>