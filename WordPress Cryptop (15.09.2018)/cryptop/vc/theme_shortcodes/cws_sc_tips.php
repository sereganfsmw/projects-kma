<?php
	global $cws_theme_funcs;
	$first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );
	// Map Shortcode in Visual Composer
	vc_map( array(
		"name"				=> esc_html__( 'CWS Tips', 'cryptop' ),
		"base"				=> "cws_sc_tips",
		'category'			=> "By CWS",
                "icon"                          => "cws_icon",
		"weight"			=> 80,
        'description' => __( 'Image Tips with tooltip', 'cryptop' ),
        "params" => array(
        	array(
        		"type" => "attach_image",
        		"heading" => __("Image", "cryptop"),
        		"param_name" => "image",
        		"value" => "",
        		"description" => __("Select image from media library.", "cryptop")
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("Resize image to this width", "cryptop"),
        		"param_name" => "width",
        		"value" => __("", 'cryptop'),
        		"description" => __("You can resize image to this width, or keep it to blank to use the original image.", "cryptop")
        		),
        	array(
        		"type" => "textarea_html",
        		"holder" => "div",
        		"heading" => __("Tooltip content, divide each one with [cwstips][/cwstips], please edit in text mode:", "cryptop"),
        		"param_name" => "content",
        		"value" => __("[cwstips]
        			You have to wrap each tooltip block in <strong>cwstips</strong>.
        			[/cwstips]
        			[cwstips]
        			Hello tooltip 2, you can customize the icon color, link, arrow position, tooltip content etc in the backend.
        			[/cwstips]
        			[cwstips]
        			Hello tooltip 3
        			[/cwstips]
        			", "cryptop"), "description" => __("Enter content for each block here. Divide each with [cwstips].", "cryptop") ),
        	array(
        		"type" => "dropdown",
        		"heading" => __("Display which tooltip by default?", "cryptop"),
        		"param_name" => "isdisplayall",
        		'value' => array(__("Display all of them when loaded", "cryptop") => "on", __("Display a specified one (customize it below:)", "cryptop") => "specify", __("Hide them all when loaded", "cryptop") => "off"),
        		'std' => 'off',
        		"description" => __('Default all the tooltips are hidden. Though you can choose to open all of them or a single one when page is loaded.', 'cryptop')
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("Display this tooltip when page loaded:", "cryptop"),
        		"param_name" => "displayednum",
        		"value" => "1",
        		"dependency" => Array('element' => "isdisplayall", 'value' => array('specify')),
        		"description" => __("You can specify to display which tooltip in current image. Default is <strong>1</strong>, which stand for the number 1 tooltip will be opened when page is loaded.", "cryptop")
        		),
        	array(
        		"type" => "dropdown",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Display the tips with?", "cryptop"),
        		"param_name" => "icontype",
        		"value" => array(__("single dot", "cryptop") => "dot", __("number", "cryptop") => "number", __("Font Awesome icon", "cryptop") => "icon"),
        		"description" => __("", "cryptop")
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("Numbers start from", "cryptop"),
        		"param_name" => "startnumber",
        		"value" => "1",
        		"dependency" => Array('element' => "icontype", 'value' => array('number')),
        		"description" => __("Default is start from 1, you can specify other value here, like 4.", "cryptop")
        		),
        	array(
        		"type" => "exploded_textarea",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Font Awesome icon for each tips:", 'cryptop'),
        		"param_name" => "fonticon",
        		"value" => __("fa-hand-o-right,fa-image,fa-coffee,fa-comment", 'cryptop'),
        		"dependency" => Array('element' => "icontype", 'value' => array('icon')),
        		"description" => __("Put the <a href='http://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>Font Awesome icon</a> here, divide with linebreak (Enter).", 'cryptop')
        		),
        	array(
        		"type" => "exploded_textarea",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Each tips icon's position", 'cryptop'),
        		"param_name" => "position",
        		"value" => __("25%|30%,35%|20%,45%|60%,75%|20%", 'cryptop'),
        		"description" => __("Position of each icon in <strong>top|left</strong> format. Please update via dragging the tips icon in the Visual Composer Frontend editor. See a <a href='http://youtu.be/9j1XhIQw9JE' target='_blank'>Youtube video demo</a>.", 'cryptop')
        		),
        	array(
        		"type" => "colorpicker",
        		"holder" => "div",
        		"class" => "",
        		"heading" => __("Global tips icon color", 'cryptop'),
        		"param_name" => "iconbackground",
        		"value" => 'rgba(0,0,0,0.8)',
        		"description" => __("Global color for the tips icon. Or you can specify different color for each icon below.", 'cryptop')
        		),
        	array(
        		"type" => "colorpicker",
        		"holder" => "div",
        		"class" => "",
        		"heading" => __("Hotspot circle dot (or Font Awesome icon) color", 'cryptop'),
        		"param_name" => "circlecolor",
        		"value" => '#FFFFFF',
        		"description" => __("Color for the tips circle dot. Default is white.", 'cryptop')
        		),
        	array(
        		"type" => "exploded_textarea",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Each tips icon's color", 'cryptop'),
        		"param_name" => "color",
        		"value" => __("", 'cryptop'),
        		"description" => __("Color for each icon, you can use the value like #663399 or the name of the color like blue here. Divide each with linebreaks (Enter).", 'cryptop')
        		),
        	array(
        		"type" => "dropdown",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Display pulse animation for the tips icon?", "cryptop"),
        		"param_name" => "ispulse",
        		"value" => array(__("yes", "cryptop") => "yes", __("no", "cryptop") => "no"),
        		"description" => __("", "cryptop")
        		),
        	array(
        		"type" => "dropdown",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Select pulse border color", "cryptop"),
        		"param_name" => "pulsecolor",
        		"value" => array(__("Default", "cryptop") => "pulse-white", __("gray", "cryptop") => "pulse-gray", __("red", "cryptop") => "pulse-red", __("green", "cryptop") => "pulse-green", __("yellow", "cryptop") => "pulse-yellow", __("blue", "cryptop") => "pulse-blue", __("purple", "cryptop") => "pulse-purple"),
        		"dependency" => Array('element' => "ispulse", 'value' => array('yes')),
        		"std" => "pulse-white",
        		"description" => __("You can select the pulse border color here, default is white.", "cryptop")
        		),
        	array(
        		"type" => "exploded_textarea",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Tooltip arrow position for each tips", 'cryptop'),
        		"param_name" => "arrowposition",
        		"value" => __("", 'cryptop'),
        		"description" => __("The arrow position for each tooltip, default is top. The available options are: <strong>top, right, bottom, left, top-right, top-left, bottom-right, bottom-left</strong>. Divide each with linebreaks (Enter)", 'cryptop')
        		),

        	array(
        		"type" => "textfield",
        		"heading" => __("Hotspot icon opacity", "cryptop"),
        		"param_name" => "opacity",
        		"value" => "1",
        		"description" => __("The opacity of each icon, default is 1", "cryptop")
        		),
        	array(
        		"type" => "dropdown",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Tooltip style", "cryptop"),
        		"param_name" => "tooltipstyle",
        		"value" => array(__("shadow", "cryptop") => "shadow", __("light", "cryptop") => "light", __("noir", "cryptop") => "noir", __("punk", "cryptop") => "punk"),
        		"description" => __("", "cryptop")
        		),
        	array(
        		"type" => "dropdown",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Tooltip trigger when user", "cryptop"),
        		"param_name" => "trigger",
        		"value" => array(__("hover", "cryptop") => "hover", __("click", "cryptop") => "click"),
        		"description" => __("Select how to trigger the tooltip.", "cryptop")
        		),
        	array(
        		"type" => "dropdown",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Tooltip animation", "cryptop"),
        		"param_name" => "tooltipanimation",
        		"value" => array(__("grow", "cryptop") => "grow", __("fade", "cryptop") => "fade", __("swing", "cryptop") => "swing", __("slide", "cryptop") => "slide", __("fall", "cryptop") => "fall"),
        		"description" => __("Choose the animation for the tooltip.", "cryptop")
        		),
        	array(
        		"type" => "exploded_textarea",
        		"holder" => "",
        		"class" => "cryptop",
        		"heading" => __("Link for each tips icon", 'cryptop'),
        		"param_name" => "links",
        		"value" => __("", 'cryptop'),
        		"description" => __("Specify link for each icon, divide each with linebreaks (Enter).", 'cryptop')
        		),
        	array(
        		"type" => "dropdown",
        		"heading" => __("How to open the link for the icon?", "cryptop"),
        		"param_name" => "custom_links_target",
        		"description" => __('Select how to open the links', 'cryptop'),
        		'value' => array(__("Same window", "cryptop") => "_self", __("New window", "cryptop") => "_blank")
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("maxWidth of the tooltip", "cryptop"),
        		"param_name" => "maxwidth",
        		"value" => "240",
        		"description" => __("maxWidth for the tooltip, 0 is auto width, you can specify a value here, default is 240.", "cryptop")
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("Container width", "cryptop"),
        		"param_name" => "containerwidth",
        		"value" => "",
        		"description" => __("You can specify the container width here, default is 100%. You can try other value like 80%, it will be align center automatically.", "cryptop")
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("Margin offset", "cryptop"),
        		"param_name" => "marginoffset",
        		"value" => "",
        		"description" => __("The margin offset for the tips icon in small screen. For example <strong>-6px 0 0 -6px</strong> will move the icons upper left for 6px offset in small screen. Leave here to be blank if you do not want it.", "cryptop")
        		),
        	array(
        		"type" => "textfield",
        		"heading" => __("Extra class name for the container", "cryptop"),
        		"param_name" => "extra_class",
        		"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "cryptop")
        		)

        	)
	));

	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Tips extends WPBakeryShortCode {
	    }
	}
?>