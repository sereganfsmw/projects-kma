<?php
	global $cws_theme_funcs;
	$params = array(
		array(
			"type"			=> "textfield",
			"admin_label"	=> true,
			"heading"		=> esc_html__( 'Title', 'cryptop' ),
			"param_name"	=> "title",
			"value"			=> ""
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Title Alignment', 'cryptop' ),
			"param_name"	=> "title_align",
			"value"			=> array(
				esc_html__( "Left", 'cryptop' ) 	=> 'left',
				esc_html__( "Right", 'cryptop' )	=> 'right',
				esc_html__( "Center", 'cryptop' )	=> 'center'
			)		
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Layout', 'cryptop' ),
			"param_name"	=> "display_style",
			"value"			=> array(
								esc_html__( 'Grid', 'cryptop' ) => 'grid',
								/*esc_html__( 'Grid with Filter', 'cryptop' ) => 'filter',*/
								esc_html__( 'Carousel', 'cryptop' ) => 'carousel'
							)
		), 
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Columns', 'cryptop' ),
			"param_name"	=> "layout",
			"value"			=> array(
				esc_html__( 'Default', 'cryptop' ) => 'def',
				esc_html__( 'One Column', 'cryptop' ) => '1',
				esc_html__( 'Two Columns', 'cryptop' ) => '2',
				esc_html__( 'Three Columns', 'cryptop' ) => '3'
			)
		),
	);


	$taxes = get_object_taxonomies ( 'cws_testimonial', 'object' );
	$avail_taxes = array(
		esc_html__( 'None', 'cryptop' )	=> ''
	);
	foreach ( $taxes as $tax => $tax_obj ){
		$tax_name = isset( $tax_obj->labels->name ) && !empty( $tax_obj->labels->name ) ? $tax_obj->labels->name : $tax;
		$avail_taxes[$tax_name] = $tax;
	}
	array_push( $params, array(
		"type"				=> "dropdown",
		"heading"			=> esc_html__( 'Filter by', 'cryptop' ),
		"param_name"		=> "tax",
		"value"				=> $avail_taxes
	));
	foreach ( $avail_taxes as $tax_name => $tax ) {
		$terms = get_terms( $tax );
		$avail_terms = array(
			''				=> ''
		);
		if ( !is_a( $terms, 'WP_Error' ) ){
			foreach ( $terms as $term ) {
				$avail_terms[$term->name] = $term->slug;
			}
		}
		array_push( $params, array(
			"type"			=> "cws_dropdown",
			"multiple"		=> "true",
			"heading"		=> $tax_name,
			"param_name"	=> "{$tax}_terms",
			"dependency"	=> array(
								"element"	=> "tax",
								"value"		=> $tax
							),
			"value"			=> $avail_terms
		));				
	}

	$params2 = array(
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'auto_play_carousel',
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'AutoPlay Carousel', 'cryptop' ) => true
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'bvnvnvbnvbn',
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'Add Navigation Arrows', 'cryptop' ) => true
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'pagination_carousel',
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'Add Navigation Bullets', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "checkbox",
			"param_name"	=> "customize_colors",
			"dependency" 	=> array(
				"element"	=> "display_style",
				"value"		=> array( "carousel" )
				),
			"value"			=> array( esc_html__( 'Customize Colors Carousel', 'cryptop' ) => true )
			),
		array(
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Navigation Color', 'cryptop' ),
			"param_name"	=> "custom_arrow_color",
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
				),
			"value"			=> ""
			),
		array(
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Pagination Color', 'cryptop' ),
			"param_name"	=> "custom_pagination_color",
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
				),
			"value"			=> ""
			),

		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'hide_data_override',
			'value'			=> array(
				esc_html__( 'Hide Meta Data', 'cryptop' ) => true
			)
		),
		array(
			'type'				=> 'cws_dropdown',
			'multiple'			=> "true",
			'heading'			=> esc_html__( 'Hide', 'cryptop' ),
			'param_name'		=> 'data_to_hide',
			'dependency'		=> array(
				'element'			=> 'hide_data_override',
				'not_empty'			=> true
			),
			'value'				=> array(
				esc_html__( 'None', 'cryptop' )			=> '',
				esc_html__( 'Positions', 'cryptop' )	=> 'poss',
				esc_html__( 'Background', 'cryptop' )	=> 'background',
				esc_html__( 'Raiting', 'cryptop' )		=> 'raiting',
				esc_html__( 'Excerpt', 'cryptop' )		=> 'excerpt',
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'change_title',
			'value'			=> array(
				esc_html__( 'Change Details Button', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Title Button', 'cryptop' ),
			"param_name"	=> "title_btn",
			'dependency'		=> array(
				'element'			=> 'change_title',
				'not_empty'			=> true
			),
			"value"			=> esc_html__( 'Read More', 'cryptop' )
		),
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items to display', 'cryptop' ),
			"param_name"	=> "total_items_count",
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		),
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items per Page', 'cryptop' ),
			"param_name"	=> "items_pp",
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "grid", "filter" )
							),
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		),
	);
	$params = array_merge($params, $params2);
	array_push( $params, array(
		"type"				=> "textfield",
		"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
		"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
		"param_name"		=> "el_class",
		"value"				=> ""
	));
	vc_map( array(
		"name"				=> esc_html__( 'CWS Testimonials Grid', 'cryptop' ),
		"base"				=> "cws_sc_testimonial_posts",
		'category'			=> "By CWS",
		"weight"			=> 80,
		"icon"     			=> "cws_icon",
		"params"			=> $params
	));
	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Testimonials_Posts extends WPBakeryShortCode {
	    }
	}
?>