<?php
	// Map Shortcode in Visual Composer
	global $cws_theme_funcs;
	$first_color 			= $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'];
	$font_options 			= $cws_theme_funcs->cws_get_option( 'body-font' );
	$font_color 			= $font_options['color'];
	$icon_params 			= cws_ext_icon_vc_sc_config_params();
	$params 				= cws_ext_merge_arrs( array(
		$icon_params,
		array(
			array(
				"type"			=> "textfield",
				"heading"		=> esc_html__( 'Post Count', 'cryptop' ),
				"param_name"	=> "number",
				"value"			=> "4"
			),
			array(
				"type"			=> "textfield",
				"heading"		=> esc_html__( 'Posts per slide', 'cryptop' ),
				"param_name"	=> "visible_number",
				"value"			=> "2"
			),
			array(
				"type"			=> "checkbox",
				"param_name"	=> "customize_colors",
				"value"			=> array( esc_html__( 'Customize Colors', 'cryptop' ) => true )
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Font Color', 'cryptop' ),
				"param_name"	=> "custom_font_color",
				"dependency"	=> array(
					"element"		=> "customize_colors",
					"not_empty"		=> true
				),
				"value"			=> $font_color
			),
			array(
				"type"			=> "colorpicker",
				"heading"		=> esc_html__( 'Featured Color', 'cryptop' ),
				"param_name"	=> "custom_featured_color",
				"dependency"	=> array(
					"element"		=> "customize_colors",
					"not_empty"		=> true
				),
				"value"			=> $first_color
			),
			array(
				"type"				=> "textfield",
				"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
				"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
				"param_name"		=> "el_class",
				"value"				=> ""
			)
		)
	));
	vc_map( array(
		"name"				=> esc_html__( 'CWS Twitter', 'cryptop' ),
		"base"				=> "cws_sc_twitter",
		'category'			=> "By CWS",
		"icon"     			=> "cws_icon",
		"weight"			=> 80,
		"params"			=> $params
	));

	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Twitter extends WPBakeryShortCode {
	    }
	}
?>