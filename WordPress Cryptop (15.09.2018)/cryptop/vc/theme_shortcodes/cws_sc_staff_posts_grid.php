<?php
	global $cws_theme_funcs;
	$first_color  = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );
	$body_font_options = $cws_theme_funcs->cws_get_option( 'body-font' );
	$body_font_color = esc_attr( $body_font_options['color'] );
	$params = array(
		array(
			"type"			=> "textfield",
			"admin_label"	=> true,
			"heading"		=> esc_html__( 'Title', 'cryptop' ),
			"param_name"	=> "title",
			"value"			=> ""
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Title Alignment', 'cryptop' ),
			"param_name"	=> "title_align",
			"value"			=> array(
				esc_html__( "Left", 'cryptop' ) 	=> 'left',
				esc_html__( "Right", 'cryptop' )	=> 'right',
				esc_html__( "Center", 'cryptop' )	=> 'center'
			)		
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Layout', 'cryptop' ),
			"param_name"	=> "display_style",
			"value"			=> array(
								esc_html__( 'Grid', 'cryptop' ) => 'grid',
								esc_html__( 'Grid with Filter', 'cryptop' ) => 'filter',
								esc_html__( 'Carousel', 'cryptop' ) => 'carousel'
							)
		),
			array(
			'type'			=> 'checkbox',
			'param_name'	=> 'auto_play_carousel',
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'AutoPlay Carousel', 'cryptop' ) => true
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'navigation_carousel',
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'Add Navigation Controls', 'cryptop' ) => true
			)
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'pagination_carousel',
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "carousel" )
							),
			'value'			=> array(
				esc_html__( 'Add Navigation Bullets', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "dropdown",
			"heading"		=> esc_html__( 'Columns', 'cryptop' ),
			"param_name"	=> "layout",
			"value"			=> array(
				esc_html__( 'Default', 'cryptop' ) => 'def',
				esc_html__( 'One Column', 'cryptop' ) => '1',
				esc_html__( 'Two Columns', 'cryptop' ) => '2',
				esc_html__( 'Three Columns', 'cryptop' ) => '3',
				esc_html__( 'Four Columns', 'cryptop' ) => '4',
			)
		),
	);
		$taxes = get_object_taxonomies ( 'cws_staff', 'object' );
		$avail_taxes = array(
			esc_html__( 'None', 'cryptop' )	=> ''
		);
		foreach ( $taxes as $tax => $tax_obj ){
			$tax_name = isset( $tax_obj->labels->name ) && !empty( $tax_obj->labels->name ) ? $tax_obj->labels->name : $tax;
			$avail_taxes[$tax_name] = $tax;
		}
		array_push( $params, array(
			"type"				=> "dropdown",
			"heading"			=> esc_html__( 'Filter by', 'cryptop' ),
			"param_name"		=> "tax",
			"value"				=> $avail_taxes
		));
		foreach ( $avail_taxes as $tax_name => $tax ) {
			$terms = get_terms( $tax );
			$avail_terms = array(
				''				=> ''
			);
			if ( !is_a( $terms, 'WP_Error' ) ){
				foreach ( $terms as $term ) {
					$avail_terms[$term->name] = $term->slug;
				}
			}
			array_push( $params, array(
				"type"			=> "cws_dropdown",
				"multiple"		=> "true",
				"heading"		=> $tax_name,
				"param_name"	=> "{$tax}_terms",
				"dependency"	=> array(
									"element"	=> "tax",
									"value"		=> $tax
								),
				"value"			=> $avail_terms
			));				
		}
	$params2 = array(
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items to display', 'cryptop' ),
			"param_name"	=> "total_items_count",
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		),
		array(
			"type"			=> "textfield",
			"heading"		=> esc_html__( 'Items per Page', 'cryptop' ),
			"param_name"	=> "items_pp",
			"dependency" 	=> array(
								"element"	=> "display_style",
								"value"		=> array( "grid", "filter" )
							),
			"value"			=> esc_html( get_option( 'posts_per_page' ) )
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'change_btn',
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Edit Read More Title', 'cryptop' ) => true
			)
		),		
		array(
			"type"			=> "textfield",
			"param_name"	=> "title_btn",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency" 	=> array(
								"element"	=> "change_btn",
								"not_empty"	=> true
							),
			"value"			=> esc_html__( 'Read More', 'cryptop' ),
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'customize_colors',
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Customize Colors', 'cryptop' ) => true
			)
		),
		array(
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Title Color', 'cryptop' ),
			"param_name"	=> "custom_title_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> ""
		),
		array(
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Font Color', 'cryptop' ),
			"param_name"	=> "custom_font_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> ""
		),		
		array(
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Social Color', 'cryptop' ),
			"param_name"	=> "custom_social_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> ""
		),		
		array(
			"type"			=> "colorpicker",
			"heading"		=> esc_html__( 'Button Color', 'cryptop' ),
			"param_name"	=> "custom_btn_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
			"value"			=> ""
		),
		array(
			"type" => "dropdown",
			"heading" => __("Image Hover", 'cryptop'),
			"param_name"		=> "bg_hover_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"value" => array(
				__("None", 'cryptop') => "none",
				__("Color", 'cryptop') => "color",
				__("Gradient", 'cryptop') => "gradient",
			),
			"dependency"	=> array(
				"element"	=> "customize_colors",
				"not_empty"	=> true
			),
		),
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading"		=> esc_html__( 'Color', 'cryptop' ),
			"param_name" => "hover_fill_color",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'color',

			),
			"value"			=> $first_color
		),
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading"		=> esc_html__( 'From', 'cryptop' ),
			"param_name" => "cws_gradient_color_from",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'gradient',

				),
			"value"			=> $first_color
		),					
		array(
			"type" => "colorpicker",
			"class" => "",
			"heading"		=> esc_html__( 'To', 'cryptop' ),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"param_name" => "cws_gradient_color_to",
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'gradient',

				),
			"value"			=> $first_color
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Type", 'cryptop'),
			"param_name"		=> "cws_gradient_type",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"value" => array(
				__("Linear", 'cryptop') => "linear",
				__("Radial", 'cryptop') => "radial",
			),
			"dependency"	=> array(
				"element"	=> "bg_hover_color",
				'value' => 'gradient',

			),
		),
		array(
			"type"			=> "textfield",
			"class" => "",
			"heading"		=> esc_html__( 'Angle', 'cryptop' ),
			"param_name"	=> "cws_gradient_angle",
			"value" => '360',
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"description"	=> esc_html__( 'Degrees: -360 to 360', 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "cws_gradient_type",
				'value' => 'linear',						
				),
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Shape variant", 'cryptop'),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"param_name"		=> "cws_gradient_shape_variant_type",
			"value" => array(
				__("Simple", 'cryptop') => "simple",
				__("Extended", 'cryptop') => "extended",
				),
			"dependency"	=> array(
				"element"	=> "cws_gradient_type",
				'value' => 'radial',	
				),
		),					
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Shape", 'cryptop'),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"param_name"		=> "cws_gradient_shape_type",
			"value" => array(
				__("Ellipse", 'cryptop') => "ellipse",
				__("Circle", 'cryptop') => "circle",
				),
			"dependency"	=> array(
				"element"	=> "cws_gradient_shape_variant_type",
				'value' => 'simple',	
				),
		),						
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Size keyword", 'cryptop'),
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"param_name"		=> "cws_gradient_size_keyword_type",
			"value" => array(
				__("Closest side", 'cryptop') => "closest_side",
				__("Farthest side", 'cryptop') => "farthest_side",
				__("Closest corner", 'cryptop') => "closest_corner",
				__("Farthest corner", 'cryptop') => "farthest_corner",
				),
			"dependency"	=> array(
				"element"	=> "cws_gradient_shape_variant_type",
				'value' => 'extended',	
				),
		),						
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Size", 'cryptop'),
			"param_name"		=> "cws_gradient_size_type",
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			"value" => '60% 55%',
			"description"	=> esc_html__( 'Two space separated percent values, for example (60% 55%)', 'cryptop' ),
			"dependency"	=> array(
				"element"	=> "cws_gradient_shape_variant_type",
				'value' => 'extended',	
				),
		),
		array(
			'type'			=> 'checkbox',
			'param_name'	=> 'hide_data_override',
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'value'			=> array(
				esc_html__( 'Hide Meta Data', 'cryptop' ) => true
			)
		),
		array(
			'type'				=> 'cws_dropdown',
			'multiple'			=> "true",
			'heading'			=> esc_html__( 'Hide', 'cryptop' ),
			'param_name'		=> 'data_to_hide',
			"group"			=> esc_html__( "Styling", 'cryptop' ),
			'dependency'		=> array(
				'element'			=> 'hide_data_override',
				'not_empty'			=> true
			),
			'value'				=> array(
				esc_html__( 'None', 'cryptop' )			=> '',
				esc_html__( 'Departments', 'cryptop' ) 	=> 'deps',
				esc_html__( 'Positions', 'cryptop' )	=> 'poss',
				esc_html__( 'Excerpt', 'cryptop' )		=> 'excerpt',
				esc_html__( 'Social Links', 'cryptop' )	=> 'socials',
				esc_html__( 'Link Button', 'cryptop' )	=> 'link_button',
				esc_html__( 'Email', 'cryptop' )			=> 'email',
				esc_html__( 'Experience', 'cryptop' )	=> 'experience',
				esc_html__( 'Biography', 'cryptop' )		=> 'biography',
			)
		)				
	);
	$params = array_merge($params, $params2);
	array_push( $params, array(
		"type"				=> "textfield",
		"heading"			=> esc_html__( 'Extra class name', 'cryptop' ),
		"description"		=> esc_html__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'cryptop' ),
		"param_name"		=> "el_class",
		"value"				=> ""
	));
	vc_map( array(
		"name"				=> esc_html__( 'CWS Staff', 'cryptop' ),
		"base"				=> "cws_sc_staff_posts_grid",
		'category'			=> "By CWS",
		"weight"			=> 80,
		"icon"     			=> "cws_icon",		
		"params"			=> $params
	));
	if ( class_exists( 'WPBakeryShortCode' ) ) {
	    class WPBakeryShortCode_CWS_Sc_Staff_Posts_Grid extends WPBakeryShortCode {
	    }
	}
?>