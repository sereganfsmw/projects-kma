"use strict";
/**********************************
******* CWS ELEMENTOR JS HOOK *****
**********************************/

//Check if Frontend init
jQuery(window).on('elementor/frontend/init', function(){

	if ( window.elementor ) {

		//Style header order
		function header_order(){
			var setting_trigger = elementor.panel.$el.find('#elementor-panel-footer-settings');
			setting_trigger.on('click', function(event) {
				setTimeout(function(){
					initTabs();
				}, 1);
			});

			function initTabs(){
				var navigation = elementor.panel.$el.find('.elementor-panel-navigation-tab ,.elementor-panel-heading');
				var panel_elements = elementor.panel.$el.find('#elementor-panel-page-settings-controls');
				var header_order = panel_elements.find('.elementor-control-header_order');

				if (header_order.length){
					var fields = header_order.find('.elementor-repeater-fields');
					fields.each(function(index, el) {
						var text = jQuery(el).find('.elementor-repeater-row-item-title').text();
						if (text == 'Header Zone'){
							jQuery(el).addClass('cws_header_zone')
						}
					});
				}

				navigation.on('click', function(event) {
					navigation.off();
					setTimeout(function(){
						initTabs();
					}, 1);
				});
				
			}

		}
		setTimeout(function(){
			header_order();
		}, 1);
		//--Style header order

		//Fires on element change (drag&drop)
		elementor.hooks.addAction( 'panel/open_editor/widget', function( panel, model, view ) {

			var controls_data = model.attributes.settings.controls;
			var controls_arr = [];

			objectRecursive(controls_data, function(node, value, key, path, depth) {
			    if (typeof value.condition == 'object' || typeof value.conditions == 'object'){
			    	controls_arr.push(key);
			    }
			});

			//Highlight hidden controls
			function highlight(){
				var navigation = panel.$el.find('.elementor-panel-navigation-tab ,.elementor-panel-heading');
				var hidden_controls_current_tab = [];
				jQuery.each(controls_arr, function(i, el) {
					var field = panel.$el.find('.elementor-control-'+el);
					field.addClass('cws_animation_controller');
					if (field.length){
						hidden_controls_current_tab.push(field);
						if (!jQuery(field).hasClass('elementor-hidden-control')){

							jQuery(field).prepend('<span class="cws_not_blink"></span>');
						}

						setTimeout(function() {
							jQuery(field).find('.cws_not_blink').remove();
						}, 600);

						if (!field.hasClass('cws_hidden_control')) {
							field.addClass('cws_hidden_control');
						}		
					}
				});	

				hidden_controls_current_tab = jQuery(hidden_controls_current_tab);

				navigation.on('click', function(event) {
					navigation.off();
					setTimeout(function(){
						highlight();
					}, 1);
				});

			}

			//Init toogle elements background
			highlight();
		});

		//Fires on Section edit
		elementor.hooks.addAction( 'panel/open_editor/section', function( panel, model, view ) {
			console.log(panel);
			console.log(model);
			console.log(view);
		});





		function handleMenuItemColor ( newValue ) {
			debugger;
			console.log( newValue );
			elementor.reloadPreview();
		}

		elementor.settings.page.addChangeCallback( 'logo_box_position', handleMenuItemColor );

	}

});