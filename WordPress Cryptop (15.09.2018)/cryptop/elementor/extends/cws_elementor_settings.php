<?php

namespace Elementor;

function cws_page_metaboxes($cws_theme_funcs){

	//Colors
	$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );	
	$theme_colors_second_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['second_color'] );	
	$theme_colors_third_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['third_color'] );	

	//Sources
	$source_menu = cws_core_cwsfw_get_taxonomy_array('nav_menu');
	$source_sidebars = cws_core_cwsfw_get_sidebars_array();
	$source_category = cws_core_cwsfw_get_taxonomy_array('category');

	$elementor_metaboxes = [
		'sections' => [
			'cws_general' => [
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_general' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'page_sidebars' => [
						'label' => esc_html__( 'Sidebar Position', 'cryptop' ),
						'type' => Controls_Manager::SELECT,				
						'separator' => 'before',
						'default' => 'default',
						'options' => [
							'default' => esc_html__( 'Default', 'cryptop' ),
							'left' => esc_html__( 'Left', 'cryptop' ),
							'right' => esc_html__( 'Right', 'cryptop' ),
							'both' => esc_html__( 'Double', 'cryptop' ),
							'none' => esc_html__( 'None', 'cryptop' ),
						],
						'condition' => [
							'MB_general' => 'yes',
						],
					],
					'sb1' => [
						'label' => esc_html__( 'Left Sidebar', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '',
						'options' => $source_sidebars,
						'condition' => [
							'MB_general' => 'yes',
							'page_sidebars' => ['left','both'],
						],						
					],
					'sb2' => [
						'label' => esc_html__( 'Right Sidebar', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '',
						'options' => $source_sidebars,
						'condition' => [
							'MB_general' => 'yes',
							'page_sidebars' => ['right','both'],
						],
					],
					'page_spacing' => [
						'label' => esc_html__( 'Page Spacings', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'bottom' ],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 70,
							'bottom' => 70,
							'unit' => 'px',
						],
						'condition' => [
							'MB_general' => 'yes',
						],
					],
					'slider_is_override' => [
						'label' => esc_html__( 'Add Image Slider', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_general' => 'yes',
						],							
					],
					'slider_shortcode' => [
						'label' => esc_html__( 'Slider shortcode', 'cryptop' ),
						'label_block' => true,
						'type' => Controls_Manager::TEXT,
						'default' => '[rev_slider alias="Home"]',
						'description' => esc_html__( 'Enter shortcode', 'cryptop' ),
						'placeholder' => '[rev_slider alias="Home]',
						'condition' => [
							'MB_general' => 'yes',
							'slider_is_override' => 'yes',
						],							
					],
					'slider_is_wide' => [
						'label' => esc_html__( 'Full-Width Slider', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => 'yes',
						'condition' => [
							'MB_general' => 'yes',
							'slider_is_override' => 'yes',
						],							
					],
				]
			],
			'cws_theme_colors' => [
				'label' => esc_html__( 'Theme colors', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_theme_colors' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'theme_colors_first_color' => [
						'label' => esc_html__( 'Main color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $theme_colors_first_color,
						'default' => $theme_colors_first_color,
						// 'selector' => '.top_bar_wrapper .container .social_links_wrapper .cws_social_links .cws_social_link',
						// 'selector' => '{{WRAPPER}}',
						'selectors' => [
							'{{WRAPPER}} .top_bar_wrapper .container .social_links_wrapper .cws_social_links .cws_social_link' => 'color: {{VALUE}};',
						],					
						'separator' => 'before',
						'condition' => [
							'MB_theme_colors' => 'yes',
						],	
					],
					'theme_colors_second_color' => [
						'label' => esc_html__( 'Second color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $theme_colors_second_color,
						'default' => $theme_colors_second_color,
						// 'selector' => '{{WRAPPER}} .cws_social_links .cws_social_link a',
						'selectors' => [
							'{{WRAPPER}} .cws_social_links a.cws_social_link' => 'color: {{VALUE}};',
						],							
						'condition' => [
							'MB_theme_colors' => 'yes',
						],	
					],
					'theme_colors_third_color' => [
						'label' => esc_html__( 'Third color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $theme_colors_third_color,
						'default' => $theme_colors_third_color,
						// 'selector' => '{{WRAPPER}} .title_wrapper .widgettitle',
						'selectors' => [
							'{{WRAPPER}} .scroll_block .scroll_to_top:hover' => 'background-color: {{VALUE}};',
						],							
						'condition' => [
							'MB_theme_colors' => 'yes',
						],	
					],
				]
			],
			'cws_header' => [
				'label' => esc_html__( 'Header', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_header' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'header_order' => [
						'label'=> esc_html__( 'Elements order', 'cryptop' ),
						'type'=> Controls_Manager::REPEATER,
						'show_label'=> true,
						'separator' => 'before',
						'default' => [
							[
								'title' => esc_html__( 'Top Bar', 'cryptop' ),
								'field' => esc_html__( 'top_bar_box', 'cryptop' ),
							],
							[
								'title' => esc_html__( 'Header Zone', 'cryptop' ),
								'field' => esc_html__( 'drop_zone_start', 'cryptop' ),
							],						
							[
								'title' => esc_html__( 'Logo', 'cryptop' ),
								'field' => esc_html__( 'logo_box', 'cryptop' ),
							],
							[
								'title' => esc_html__( 'Menu', 'cryptop' ),
								'field' => esc_html__( 'menu_box', 'cryptop' ),
							],
							[
								'title' => esc_html__( 'Header Zone', 'cryptop' ),
								'field' => esc_html__( 'drop_zone_end', 'cryptop' ),
							],
							[
								'title' => esc_html__( 'Title area', 'cryptop' ),
								'field' => esc_html__( 'title_box', 'cryptop' ),
							]																		
						],
						'fields' => [
							[
								'name' => 'title',
								'label' => esc_html__( 'Title', 'cryptop' ),
								'type' => Controls_Manager::TEXT,
								'default' => '',
								'title' => esc_html__( 'Title', 'cryptop' ),
							],
							[
								'name' => 'field',
								'label' => esc_html__( 'Field', 'cryptop' ),
								'type' => Controls_Manager::TEXT,
								'default' => '',
								'title' => esc_html__( 'Field', 'cryptop' ),
							]
						],
						'title_field' => '{{{title}}}',
						'condition' => [
							'MB_header' => 'yes',
						],	
					],
					'header_customize' => [
						'label' => esc_html__( 'Add an Image/Color or Spacings', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_header' => 'yes',
						],			
					],
					'header_pattern_enable' => [
						'label' => esc_html__( 'Add pattern (Overlay)', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'MB_header' => 'yes',
							'header_customize' => 'yes'
						],
					],
					'header_tabs' => [
						'type' => 'tabs',
						'controls' => [
							'header_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_header' => 'yes',
									'header_customize' => 'yes',
								],
								'controls' => [
									'header_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
									],
									'header_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
									],
									'header_pattern_title' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
											'header_pattern_enable' => 'yes',
										],
									],
									'header_pattern' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
											'header_pattern_enable' => 'yes',
										],
									],
									'header_font_title' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
									],
									'header_override_menu_color' => [
										'label' => esc_html__( 'Override Menu\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],	
									],
									'header_override_topbar_color' => [
										'label' => esc_html__( 'Override TopBar\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],	
									],
								],
							], //--header_tab_normal
							'header_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_header' => 'yes',
									'header_customize' => 'yes',
								],
								'controls' => [
									'header_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
									],
									'header_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
									],
									'header_pattern_title_hover' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
											'header_pattern_enable' => 'yes',
										],
									],
									'header_pattern_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
											'header_pattern_enable' => 'yes',
										],
									],
									'header_font_title_hover' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
									],
									'header_override_menu_color_hover' => [
										'label' => esc_html__( 'Override Menu\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],	
									],
									'header_override_topbar_color_hover' => [
										'label' => esc_html__( 'Override TopBar\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],	
									],
									'header_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_header' => 'yes',
											'header_customize' => 'yes',
										],
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--header_tab_hover
						]
					],
					'header_spacing' => [
						'label' => esc_html__( 'Header Spacings', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'bottom' ],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 0,
							'bottom' => 0,
							'unit' => 'px',
						],
						'condition' => [
							'MB_general' => 'yes',
							'header_customize' => 'yes',
						],
					],
					'header_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_header' => 'yes',
							'header_customize' => 'yes',
						],
					],					
					'header_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],						
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_header' => 'yes',
							'header_customize' => 'yes',
						],
					],
					'header_fixed' => [
						'label' => esc_html__( 'Apply Fixed header', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_header' => 'yes',
							'header_customize' => 'yes',
						],
					],
					'header_outside_slider' => [
						'label' => esc_html__( 'Header overlays slider', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'separator' => 'before',
						'condition' => [
							'MB_header' => 'yes',
							'header_customize' => 'yes',
						],
					],
				]
			],
			'cws_logo_box' => [
				'label' => esc_html__( 'Logo', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_logo' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'logo_box_enable' => [
						'label' => esc_html__( 'Logo enable', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => 'yes',
						'separator' => 'before',
						'condition' => [
							'MB_logo' => 'yes',
						],						
					],
					'logo_box_default' => [
						'label' => esc_html__( 'Logo Variation', 'cryptop' ),
						'type' => Controls_Manager::SELECT,				
						'default' => 'dark',
						'options' => [
							'dark' => esc_html__( 'Dark', 'cryptop' ),
							'light' => esc_html__( 'Light', 'cryptop' ),
						],
						'condition' => [
							'MB_logo' => 'yes',
						],						
					],
					'logo_box_dimensions' => [
						'label' => esc_html__( 'Logo Dimensions', 'cryptop' ),
						'type' => Controls_Manager::IMAGE_DIMENSIONS,
						'description' => esc_html__( 'Crop the original logo size to any custom size. Set custom width or height to keep the original size ratio.', 'cryptop' ),
						'default' => [
							'width' => '',
							'height' => '',
						],
						'condition' => [
							'MB_logo' => 'yes',
						],								
					],
					'logo_box_spacing' => [
						'label' => esc_html__( 'Margins', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'right', 'bottom', 'left' ],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
						'default' => [
							'top' => 12,
							'right' => 0,
							'bottom' => 12,
							'left' => 0,
							'unit' => 'px',
						],
						'condition' => [
							'MB_logo' => 'yes',
						],
					],
					'logo_box_position' => [
						'label' => esc_html__( 'Position', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'left',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'center' => [
								'title' => esc_html__( 'Center', 'cryptop' ),
								'icon' => 'fa fa-align-center',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'render_type' => 'template',
					/*	'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],	*/	
						'condition' => [
							'MB_logo' => 'yes'
						],
					],
					'logo_box_in_menu' => [
						'label' => esc_html__( 'Logo in menu box', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_logo' => 'yes',
						],
					],
					'logo_box_tabs' => [
						'type' => 'tabs',
						'controls' => [
							'logo_box_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_logo' => 'yes',
									'logo_box_in_menu' => '',
								],
								'controls' => [
									'logo_box_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_logo' => 'yes',
											'logo_box_in_menu' => '',
										],
									],								
									'logo_box_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_logo' => 'yes',
											'logo_box_in_menu' => '',
										],			
									],
								],
							], //--logo_box_tab_normal
							'logo_box_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_logo' => 'yes',
									'logo_box_in_menu' => '',
								],									
								'controls' => [
									'logo_box_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_logo' => 'yes',
											'logo_box_in_menu' => '',
										],
									],
									'logo_box_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_logo' => 'yes',
											'logo_box_in_menu' => '',
										],			
									],
									'logo_box_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_logo' => 'yes',
											'logo_box_in_menu' => '',
										],										
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--logo_box_tab_hover
						]
					],
					'logo_box_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_logo' => 'yes',
							'logo_box_in_menu' => '',
						],
					],						
					'logo_box_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_logo' => 'yes',
							'logo_box_in_menu' => '',
						],
					],
					'logo_box_wide' => [
						'label' => esc_html__( 'Apply Full-Width Container', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'separator' => 'before',
						'condition' => [
							'MB_logo' => 'yes',
							'logo_box_in_menu' => '',
						],
					],
				]
			],			
			'cws_menu_box' => [
				'label' => esc_html__( 'Menu', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_menu' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'menu_box_enable' => [
						'label' => esc_html__( 'Menu enable', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => 'yes',
						'separator' => 'before',
						'condition' => [
							'MB_menu' => 'yes',
						],
					],
					'menu_box_position' => [
						'label' => esc_html__( 'Menu Alignment', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'center',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'center' => [
								'title' => esc_html__( 'Center', 'cryptop' ),
								'icon' => 'fa fa-align-center',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_menu' => 'yes'
						],
					],
					'menu_box_search_place' => [
						'label' => esc_html__( 'Search Icon Location', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'right',
						'toggle' => false,
						'options' => [
							'none' => [
								'title' => esc_html__( 'None', 'cryptop' ),
								'icon' => 'fa fa-times',
							],
				            'top'    => [
				                'title' => esc_html__( 'Top', 'cryptop' ),
				                'icon' => 'fa fa-chevron-up',
				            ],
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-chevron-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-chevron-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_menu' => 'yes'
						],
					],
					'menu_box_mobile_place' => [
						'label' => esc_html__( 'Mobile Menu Location', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'right',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-chevron-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-chevron-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_menu' => 'yes'
						],
					],
					'menu_box_tabs' => [
						'type' => 'tabs',					
						'controls' => [
							'menu_box_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_menu' => 'yes',
								],									
								'controls' => [
									'menu_box_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_menu' => 'yes',
										],
									],
									'menu_box_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_menu' => 'yes',
										],			
									],
									'menu_box_font_title' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_menu' => 'yes',
										],
									],									
									'menu_box_font_color' => [
										'label' => esc_html__( 'Menu\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],										
										'condition' => [
											'MB_menu' => 'yes',
										],	
									],
								],
							], //--menu_box_tab_normal
							'menu_box_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_menu' => 'yes',
								],									
								'controls' => [
									'menu_box_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_menu' => 'yes',
										],
									],								
									'menu_box_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_menu' => 'yes',
										],			
									],
									'menu_box_font_title_hover' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_menu' => 'yes',
										],
									],									
									'menu_box_font_color_hover' => [
										'label' => esc_html__( 'Menu\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],										
										'condition' => [
											'MB_menu' => 'yes',
										],	
									],
									'menu_box_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_menu' => 'yes',
										],										
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--menu_box_tab_hover
						]
					],
					'menu_box_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_menu' => 'yes',
						],
					],					
					'menu_box_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],						
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_menu' => 'yes',
						],
					],
					'menu_box_spacing' => [
						'label' => esc_html__( 'Menu Spacings', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'bottom' ],
						'separator' => 'before',
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 12,
							'bottom' => 12,
							'unit' => 'px',
						],
						'condition' => [
							'MB_menu' => 'yes',
						],
					],					
					'menu_box_sandwich' => [
						'label' => esc_html__( 'Use mobile menu on desktop PCs', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_menu' => 'yes',
						],
					],
					'menu_box_override_menu' => [
						'label' => esc_html__( 'Use Custom Menu on this Page', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_menu' => 'yes',
						],
					],
					'menu_box_custom_menu' => [
						'label' => esc_html__( 'Select a menu', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '',
						'options' => $source_menu,
						'condition' => [
							'MB_menu' => 'yes',
							'menu_box_override_menu' => 'yes',
						],
					],
					'menu_box_wide' => [
						'label' => esc_html__( 'Apply Full-Width Container', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'separator' => 'before',
						'condition' => [
							'MB_menu' => 'yes',
						],
					],
				]
			],
			'cws_mobile_menu' => [
				'label' => esc_html__( 'Mobile', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_mobile' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'mobile_menu_enable_on_tablet' => [
						'label' => esc_html__( 'Enable Mobile menu on tablets', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => '',
						'condition' => [
							'MB_mobile' => 'yes',
						],
					],
				]
			],
			'cws_sticky_menu' => [
				'label' => esc_html__( 'Sticky menu', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_sticky' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'sticky_menu_enable' => [
						'label' => esc_html__( 'Sticky enable', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => 'yes',
						'separator' => 'before',
						'condition' => [
							'MB_sticky' => 'yes',
						],
					],					
					'sticky_menu_mode' => [
						'label' => esc_html__( 'Select a Sticky\'s Mode', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => 'simple',
						'options' => [
							'smart' => esc_html__( 'Smart', 'cryptop' ),
							'simple' => esc_html__( 'Simple', 'cryptop' ),
						],
						'condition' => [
							'MB_sticky' => 'yes',
						],
					],
					'sticky_menu_tabs' => [
						'type' => 'tabs',					
						'controls' => [
							'sticky_menu_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_sticky' => 'yes',
								],									
								'controls' => [
									'sticky_menu_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_sticky' => 'yes',
										],
									],
									'sticky_menu_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_sticky' => 'yes',
										],			
									],
									'sticky_menu_font_title' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_sticky' => 'yes',
										],
									],									
									'sticky_menu_font_color' => [
										'label' => esc_html__( 'Menu\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],										
										'condition' => [
											'MB_sticky' => 'yes',
										],	
									],
								],
							], //--sticky_menu_tab_normal
							'sticky_menu_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_sticky' => 'yes',
								],									
								'controls' => [
									'sticky_menu_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_sticky' => 'yes',
										],
									],								
									'sticky_menu_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_sticky' => 'yes',
										],			
									],
									'sticky_menu_font_title_hover' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_sticky' => 'yes',
										],
									],									
									'sticky_menu_font_color_hover' => [
										'label' => esc_html__( 'Menu\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],										
										'condition' => [
											'MB_sticky' => 'yes',
										],	
									],
									'sticky_menu_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_sticky' => 'yes',
										],										
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--sticky_menu_tab_hover
						]
					],
					'sticky_menu_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_sticky' => 'yes',
						],
					],					
					'sticky_menu_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],						
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_sticky' => 'yes',
						],
					],
					'sticky_menu_shadow' => [
						'label' => esc_html__( 'Add Shadow', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'condition' => [
							'MB_sticky' => 'yes',
						],
					],						
				]
			],			
			'cws_title_box' => [
				'label' => esc_html__( 'Title', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_title' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],

					'title_box_enable' => [
						'label' => esc_html__( 'Title Area enable', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => 'yes',
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
						],
					],
					'title_box_text_center' => [
						'label' => esc_html__( 'Center Title & Breadcrumbs', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_title' => 'yes',
						],
					],
					'title_box_no_title' => [
						'label' => esc_html__( 'Hide Page Title', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_title' => 'yes',
						],
					],
					'title_box_customize' => [
						'label' => esc_html__( 'Add Background Image/Color and animation', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_title' => 'yes',
						],
					],
					'title_box_pattern_enable' => [
						'label' => esc_html__( 'Add pattern (Overlay)', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],
					'title_box_tabs' => [
						'type' => 'tabs',					
						'controls' => [
							'title_box_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_title' => 'yes',
									'title_box_customize' => 'yes'
								],									
								'controls' => [
									'title_box_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],
									],
									'title_box_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],			
									],
									'title_box_pattern_title' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes',
											'title_box_pattern_enable' => 'yes'
										],
									],
									'title_box_pattern' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes',
											'title_box_pattern_enable' => 'yes'
										],			
									],
									'title_box_font_title' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],
									],									
									'title_box_font_color' => [
										'label' => esc_html__( 'Title\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],										
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],	
									],
								],
							], //--title_box_tab_normal
							'title_box_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_title' => 'yes',
									'title_box_customize' => 'yes'
								],									
								'controls' => [
									'title_box_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],
									],								
									'title_box_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],			
									],
									'title_box_pattern_title_hover' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes',
											'title_box_pattern_enable' => 'yes'
										],
									],
									'title_box_pattern_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes',
											'title_box_pattern_enable' => 'yes'
										],			
									],
									'title_box_font_title_hover' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],
									],									
									'title_box_font_color_hover' => [
										'label' => esc_html__( 'Title\'s Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],										
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],	
									],
									'title_box_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_title' => 'yes',
											'title_box_customize' => 'yes'
										],										
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--title_box_tab_hover
						]
					],
					'title_box_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],					
					'title_box_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],						
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],
					'title_box_spacing' => [
						'label' => esc_html__( 'Title Area Spacings', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'bottom' ],
						'separator' => 'before',
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 60,
							'bottom' => 60,
							'unit' => 'px',
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],
					'title_box_parallax_title' => [
						'label' => esc_html__( 'Parallax', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],
					'title_box_effect' => [
						'label' => esc_html__( 'Parallax', 'cryptop' ),
						'type' => Controls_Manager::SELECT,				
						'default' => 'none',
						'options' => [
							'none' => esc_html__( 'None', 'cryptop' ),
							'scroll_parallax' => esc_html__( 'Scroll Parallax', 'cryptop' ),
							'hover_parallax' => esc_html__( 'Parallaxify on hover', 'cryptop' ),
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],					
					],
					'title_box_scroll_parallax' => [
						'label' => esc_html__( 'Add Motion Zoom to Header Image on Page Scroll', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'scroll_parallax',
						],
					],	
					'title_box_x_scalar' => [
						'label' => esc_html__( 'X-axis parallax intensity', 'cryptop' ),
						'type' => Controls_Manager::NUMBER,				
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 2,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
						],	
					],						
					'title_box_x_limit' => [							
						'label' => esc_html__( 'Maximum x-axis shift', 'cryptop' ),
						'type' => Controls_Manager::NUMBER,				
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 15,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
						],	
					],	
					'title_box_y_scalar' => [							
						'label' => esc_html__( 'Y-axis parallax intensity', 'cryptop' ),
						'type' => Controls_Manager::NUMBER,				
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 2,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
						],	
					],	
					'title_box_y_limit' => [							
						'label' => esc_html__( 'Maximum y-axis shift', 'cryptop' ),
						'type' => Controls_Manager::NUMBER,				
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 15,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
						],	
					],	
					'title_box_invert_parallax' => [							
						'label' => esc_html__( 'Invert hover', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
						],								
					],
					'title_box_x_invert' => [							
						'label' => esc_html__( 'X-axis parallax invert', 'cryptop' ),
						'type' => Controls_Manager::NUMBER,				
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 15,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
							'title_box_invert_parallax' => 'yes',
						],	
					],
					'title_box_y_invert' => [							
						'label' => esc_html__( 'Y-axis parallax invert', 'cryptop' ),
						'type' => Controls_Manager::NUMBER,				
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 15,
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_effect' => 'hover_parallax',
							'title_box_invert_parallax' => 'yes',
						],	
					],
					'title_box_breadcrumbs_title' => [
						'label' => esc_html__( 'Breadcrumbs', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],
					'title_box_breadcrumbs_divider' => [
						'label' => esc_html__( 'Breadcrumbs Divider', 'cryptop' ),
						'type' => Controls_Manager::MEDIA,
						'default' => [
							'url' => Utils::get_placeholder_image_src(),
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],			
					],
					'title_box_breadcrumbs_dimensions' => [
						'label' => esc_html__( 'Breadcrumbs Divider Dimensions', 'cryptop' ),
						'type' => Controls_Manager::IMAGE_DIMENSIONS,
						'description' => esc_html__( 'Crop the original divider size to any custom size. Set custom width or height to keep the original size ratio.', 'cryptop' ),
						'default' => [
							'width' => '',
							'height' => '',
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],								
					],					
					'title_box_breadcrumbs_spacing' => [
						'label' => esc_html__( 'Breadcrumbs Divider margin', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'right', 'bottom', 'left' ],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 0,
							'right' => 0,
							'bottom' => 0,
							'left' => 0,
							'unit' => 'px',
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_breadcrumbs_divider!' => '',
						],
					],
					'title_box_blur' => [
						'label' => esc_html__( 'Apply blur', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],	
					'title_box_blur_intensity' => [
						'label' => esc_html__( 'X-axis parallax intensity', 'cryptop' ),			
						'type' => Controls_Manager::SLIDER,
						'default' => [
							'size' => 8,
						],
						'range' => [
							'px' => [
								'min' => 1,
								'max' => 20,
								'step' => 1,
							],
						],
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_blur' => 'yes',
						],	
					],
					'title_box_slide_down' => [
						'label' => esc_html__( 'Slide down Header on Page Load', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes'
						],
					],
					'title_box_animate' => [
						'label' => esc_html__( 'Add Mouse Scroll Animation', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							// 'title_box_animate_options' => 'yes'
						],
					],
					'title_box_animate_options' => [
						'label'=> esc_html__( 'Animation Steps', 'cryptop' ),
						'type'=> Controls_Manager::REPEATER,
						'show_label'=> true,
						'separator' => 'before',
						'default' => [
							[
								'selector' => 'container',
								'offset' => 100,
								'styles' => 'background-color: rgba(255, 17, 0, 0.5);',
							],
							[
								'selector' => 'container',
								'offset' => 300,
								'styles' => 'background-color: rgba(0, 0, 0, 0.5);',
							],																						
						],
						'fields' => [
							[
								'name' => 'selector',
								'label' => esc_html__( 'Parallax', 'cryptop' ),
								'type' => Controls_Manager::SELECT,				
								'separator' => 'before',
								'default' => 'container',
								'options' => [
									'title' => esc_html__( 'Title & Breadcrumbs', 'cryptop' ),
									'container' => esc_html__( 'Content Width', 'cryptop' ),
									'section' => esc_html__( 'Full Width', 'cryptop' ),
								],
							],
							[
								'name' => 'offset',
								'label' => esc_html__( 'Top offset (in px)', 'cryptop' ),
								'type' => Controls_Manager::NUMBER,				
								'min' => 0,
								'max' => 1000,
								'step' => 1,
								'default' => 100,
							],
							[
								'name' => 'styles',
								'label' => esc_html__( 'Styles CSS', 'cryptop' ),
								'rows' => 10,
								'type' => Controls_Manager::TEXTAREA,
								'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
							],
						],
						'title_field' => '{{{selector}}} {{{offset}}}px',
						'condition' => [
							'MB_title' => 'yes',
							'title_box_customize' => 'yes',
							'title_box_animate' => 'yes'
						],
					],
				]
			],
			'cws_top_bar_box' => [
				'label' => esc_html__( 'Top bar', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_top_bar' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'top_bar_box_enable' => [
						'label' => esc_html__( 'Top Bar enable', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => 'yes',
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
					'top_bar_box_language_bar' => [
						'label' => esc_html__( 'Add Language Bar', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
					'top_bar_box_language_bar_position' => [
						'label' => esc_html__( 'Language bar position', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'left',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_top_bar' => 'yes',
							'top_bar_box_language_bar' => 'yes'
						],
					],
					'top_bar_box_social_place' => [
						'label' => esc_html__( 'Social Icons Alignment', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'left',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
					'top_bar_box_toggle_share' => [
						'label' => esc_html__( 'Toggle Social Icons', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
					'top_bar_box_text' => [
						'label' => esc_html__( 'Content', 'cryptop' ),
						'description'	=> esc_html__( 'Adjust Indents by multiple spaces. <br/> Line breaks are working too.', 'cryptop' ),
						'rows' => 10,
						'type' => Controls_Manager::TEXTAREA,
						'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
						'condition' => [
							'MB_top_bar' => 'yes',
						],						
					],
					'top_bar_box_tabs' => [
						'type' => 'tabs',					
						'controls' => [
							'top_bar_box_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_top_bar' => 'yes',
								],									
								'controls' => [
									'top_bar_box_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_top_bar' => 'yes',
										],
									],								
									'top_bar_box_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_top_bar' => 'yes',
										],			
									],
								],
							], //--top_bar_box_tab_normal
							'top_bar_box_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_top_bar' => 'yes',
								],									
								'controls' => [
									'top_bar_box_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_top_bar' => 'yes',
										],
									],
									'top_bar_box_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_top_bar' => 'yes',
										],			
									],
									'top_bar_box_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_top_bar' => 'yes',
										],										
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--top_bar_box_tab_hover
						]
					],
					'top_bar_box_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],						
					'top_bar_box_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
					'top_bar_box_spacing' => [
						'label' => esc_html__( 'Margins', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'bottom' ],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 5,
							'right' => 0,
							'bottom' => 5,
							'left' => 0,
							'unit' => 'px',
						],
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
					'top_bar_box_wide' => [
						'label' => esc_html__( 'Apply Full-Width Container', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'separator' => 'before',
						'condition' => [
							'MB_top_bar' => 'yes',
						],
					],
				]
			],
			'cws_footer' => [
				'label' => esc_html__( 'Footer', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_footer' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'footer_menu_enable' => [
						'label' => esc_html__( 'Footer Menu', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => '',
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_override_menu' => [
						'label' => esc_html__( 'Use Custom Menu on this Page', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_footer' => 'yes',
							'footer_menu_enable' => 'yes',
						],
					],					
					'footer_custom_menu' => [
						'label' => esc_html__( 'Select a menu', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '',
						'options' => $source_menu,
						'condition' => [
							'MB_footer' => 'yes',
							'footer_menu_enable' => 'yes',
							'footer_override_menu' => 'yes',
						],
					],
					'footer_menu_position' => [
						'label' => esc_html__( 'Menu Alignment', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'right',
						'toggle' => false,
						'options' => [
							'left' => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-chevron-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-chevron-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_footer' => 'yes',
							'footer_menu_enable' => 'yes',
						],
					],
					'footer_layout' => [
						'label' => esc_html__( 'Select a layout', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '4',
						'options' => [
							'1' => esc_html__( '1/1 Column', 'cryptop' ),
							'2' => esc_html__( '2/2 Column', 'cryptop' ),
							'3' => esc_html__( '3/3 Column', 'cryptop' ),
							'4' => esc_html__( '4/4 Column', 'cryptop' ),
							'two-three' => esc_html__( '2/3 + 1/3 Column', 'cryptop' ),
							'one-two' => esc_html__( '1/3 + 2/3 Column', 'cryptop' ),
							'one-three' => esc_html__( '1/4 + 3/4 Column', 'cryptop' ),
							'one-one-two' => esc_html__( '1/4 + 1/4 + 2/4 Column', 'cryptop' ),
							'two-one-one' => esc_html__( '2/4 + 1/4 + 1/4 Column', 'cryptop' ),
							'one-two-one' => esc_html__( '1/4 + 2/4 + 1/4 Column', 'cryptop' ),
						],
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_sidebar' => [
						'label' => esc_html__( 'Select Footer\'s Sidebar Area', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '',
						'options' => $source_sidebars,
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_text_alignment' => [
						'label' => esc_html__( 'Text Alignment', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'left',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'center' => [
								'title' => esc_html__( 'Center', 'cryptop' ),
								'icon' => 'fa fa-align-center',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_footer' => 'yes'
						],
					],
					'footer_copyrights_text' => [
						'label' => esc_html__( 'Copyrights content', 'cryptop' ),
						'rows' => 10,
						'default' => esc_html__( 'Copyright © 2018. All rights reserved.', 'cryptop' ),
						'type' => Controls_Manager::TEXTAREA,
						'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_pattern_enable' => [
						'label' => esc_html__( 'Add pattern (Overlay)', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_tabs' => [
						'type' => 'tabs',
						'controls' => [
							'footer_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_footer' => 'yes',
								],
								'controls' => [
									'footer_background_title' => [
										'label' => esc_html__( 'Footer Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_pattern_title' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
											'footer_pattern_enable' => 'yes',
										],
									],
									'footer_pattern' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_footer' => 'yes',
											'footer_pattern_enable' => 'yes',
										],
									],
									'footer_font_title' => [
										'label' => esc_html__( 'Colors', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_font_color' => [
										'label' => esc_html__( 'Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_footer' => 'yes',
										],	
									],
									'footer_title_color' => [
										'label' => esc_html__( 'Title Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_footer' => 'yes',
										],	
									],
									'footer_copyrights_color' => [
										'label' => esc_html__( 'Copyrights Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_footer' => 'yes',
										],	
									],
									'footer_background_copyrights_title' => [
										'label' => esc_html__( 'Copyrights background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_background_copyrights' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_footer' => 'yes',
										],			
									],
								],
							], //--footer_tab_normal
							'footer_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_footer' => 'yes',
								],
								'controls' => [
									'footer_background_title_hover' => [
										'label' => esc_html__( 'Footer Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_pattern_title_hover' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
											'footer_pattern_enable' => 'yes',
										],
									],
									'footer_pattern_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_footer' => 'yes',
											'footer_pattern_enable' => 'yes',
										],
									],
									'footer_font_title_hover' => [
										'label' => esc_html__( 'Colors', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_font_color_hover' => [
										'label' => esc_html__( 'Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_footer' => 'yes',
										],	
									],
									'footer_title_color_hover' => [
										'label' => esc_html__( 'Title Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_footer' => 'yes',
										],	
									],
									'footer_copyrights_color_hover' => [
										'label' => esc_html__( 'Copyrights Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_footer' => 'yes',
										],	
									],
									'footer_background_copyrights_title_hover' => [
										'label' => esc_html__( 'Copyrights background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_footer' => 'yes',
										],
									],
									'footer_background_copyrights_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Gradient::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_footer' => 'yes',
										],			
									],
									'footer_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_footer' => 'yes',
										],
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--footer_tab_hover
						]
					],
					'footer_border_title' => [
						'label' => esc_html__( 'Border', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_footer' => 'yes',
						],
					],						
					'footer_border' => [
						'type' => 'group',
						'group' => CWS_Group_Control_Border::get_type(),
						'fields_names' => [
							'border' => _x( 'Border Type', 'Border Control', 'cryptop' ),
							'width' => _x( 'Border Width', 'Border Control', 'cryptop' ),
							'color' => _x( 'Border Color', 'Border Control', 'cryptop' ),
						],
						'selector' => '{{WRAPPER}} .cws_service_icon_container',
						'fields_options' => [
							'width' => [
								'allowed_dimensions' => [ 'top', 'bottom' ],
							],
						],
						'defaults' => [
							'border' => '',
							'width' => [
								'top' => 0,
								'right' => 0,
								'bottom' => 0,
								'left' => 0,
								'unit' => 'px'
							],
							'color' => '#000000',
						],
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_instagram_feed_title' => [
						'label' => esc_html__( 'Instagram Feed', 'cryptop' ),
						'type' => Controls_Manager::HEADING,
						'separator' => 'before',
						'condition' => [
							'MB_footer' => 'yes',
						],
					],	
					'footer_instagram_feed' => [
						'label' => esc_html__( 'Add Instagram Feed', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_instagram_feed_full_width' => [
						'label' => esc_html__( 'Apply Full-Width (Instagram Feed)', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
						'condition' => [
							'MB_footer' => 'yes',
							'footer_instagram_feed' => 'yes',
						],
					],					
					'footer_instagram_feed_shortcode' => [
						'label' => esc_html__( 'Instagram Shortcode', 'cryptop' ),
						'rows' => 10,
						'default' => '[instagram-feed cols=8 num=8 imagepadding=0 imagepaddingunit=px showheader=false showbutton=true showfollow=true]',
						'type' => Controls_Manager::TEXTAREA,
						'placeholder' => esc_html__( 'Type Shortcode here', 'cryptop' ),
						'condition' => [
							'MB_footer' => 'yes',
							'footer_instagram_feed' => 'yes',
						],
					],					
					'footer_fixed' => [
						'label' => esc_html__( 'Apply Fixed Footer Style', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => '',
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
					'footer_spacing' => [
						'label' => esc_html__( 'Spacings', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'size_units' => [ 'px', '%' ],
						'allowed_dimensions' => [ 'top', 'bottom' ],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{BOTTOM}}{{UNIT}};',
						],
						'default' => [
							'top' => 5,
							'right' => 0,
							'bottom' => 5,
							'left' => 0,
							'unit' => 'px',
						],
						'condition' => [
							'MB_footer' => 'yes',
						],
					],					
					'footer_wide' => [
						'label' => esc_html__( 'Apply Full-Width Container', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => '',
						'condition' => [
							'MB_footer' => 'yes',
						],
					],
				]
			],
			'cws_boxed' => [
				'label' => esc_html__( 'Layout', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_boxed' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'boxed_enable' => [
						'label' => esc_html__( 'Apply Boxed Layout', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => 'yes',
						'condition' => [
							'MB_boxed' => 'yes',
						],
					],
					'boxed_pattern_enable' => [
						'label' => esc_html__( 'Add pattern (Overlay)', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'MB_boxed' => 'yes',
						],
					],
					'boxed_tabs' => [
						'type' => 'tabs',
						'controls' => [
							'boxed_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_boxed' => 'yes',
								],
								'controls' => [
									'boxed_background_title' => [
										'label' => esc_html__( 'Boxed Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_boxed' => 'yes',
										],
									],
									'boxed_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_boxed' => 'yes',
										],
									],
									'boxed_pattern_title' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_boxed' => 'yes',
											'boxed_pattern_enable' => 'yes',
										],
									],
									'boxed_pattern' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_boxed' => 'yes',
											'boxed_pattern_enable' => 'yes',
										],
									],
								],
							], //--boxed_tab_normal
							'boxed_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_boxed' => 'yes',
								],
								'controls' => [
									'boxed_background_title_hover' => [
										'label' => esc_html__( 'Boxed Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_boxed' => 'yes',
										],
									],
									'boxed_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_boxed' => 'yes',
										],
									],
									'boxed_pattern_title_hover' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_boxed' => 'yes',
											'boxed_pattern_enable' => 'yes',
										],
									],
									'boxed_pattern_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_boxed' => 'yes',
											'boxed_pattern_enable' => 'yes',
										],
									],								
									'boxed_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_boxed' => 'yes',
										],
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--boxed_tab_hover
						]
					],
				]
			],
			'cws_side_panel' => [
				'label' => esc_html__( 'Sidebar', 'cryptop' ),
				'tab' => 'cws-tab',
				'controls' => [
					'MB_side_panel' => [
						'label' => esc_html__( 'Customize', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'default' => '',
					],
					'side_panel_enable' => [
						'label' => esc_html__( 'Side Panel enable', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'separator' => 'before',
						'default' => '',
						'condition' => [
							'MB_side_panel' => 'yes',
						],
					],
					'side_panel_theme' => [
						'label' => esc_html__( 'Color Variation', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => 'dark',
						'options' => [
							'dark' => esc_html__( 'Dark', 'cryptop' ),
							'light' => esc_html__( 'Light', 'cryptop' ),
						],
						'condition' => [
							'MB_side_panel' => 'yes',
						],
					],
					'side_panel_place' => [
						'label' => esc_html__( 'Menu Icon Location', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'topbar_left',
						'toggle' => false,
						'options' => [
							'topbar_left' => [
								'title' => esc_html__( 'TopBar (Left)', 'cryptop' ),
								'icon' => 'fa fa-chevron-left',
							],
							'menu_left' => [
								'title' => esc_html__( 'Menu (Left)', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'menu_right' => [
								'title' => esc_html__( 'Menu (Right)', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
							'topbar_right' => [
								'title' => esc_html__( 'TopBar (Right)', 'cryptop' ),
								'icon' => 'fa fa-chevron-right',
							],

						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_side_panel' => 'yes'
						],
					],
					'side_panel_position' => [
						'label' => esc_html__( 'Side Panel Position', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'right',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_side_panel' => 'yes'
						],
					],
					'side_panel_sidebar' => [
						'label' => esc_html__( 'Select the Sidebar Area', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => '',
						'options' => $source_sidebars,
						'condition' => [
							'MB_side_panel' => 'yes',
						],						
					],
					'side_panel_appear' => [
						'label' => esc_html__( 'Animation Format', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => 'fade',
						'options' => [
							'fade' => esc_html__( 'Fade', 'cryptop' ),
							'slide' => esc_html__( 'Slide', 'cryptop' ),
							'pull' => esc_html__( 'Pull', 'cryptop' ),
						],
						'condition' => [
							'MB_side_panel' => 'yes',
						],
					],
					'side_panel_logo_position' => [
						'label' => esc_html__( 'Logo position', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'left',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-align-left',
							],
							'center' => [
								'title' => esc_html__( 'Center', 'cryptop' ),
								'icon' => 'fa fa-align-center',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-align-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_side_panel' => 'yes'
						],
					],
					'side_panel_close_position' => [
						'label' => esc_html__( 'Close Button Position', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'default' => 'left',
						'toggle' => false,
						'options' => [
							'left'    => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-chevron-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-chevron-right',
							],
						],
						'selectors' => [
							'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
						],		
						'condition' => [
							'MB_side_panel' => 'yes'
						],
					],					
					'side_panel_pattern_enable' => [
						'label' => esc_html__( 'Add pattern (Overlay)', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'condition' => [
							'MB_side_panel' => 'yes',
						],
					],
					'side_panel_tabs' => [
						'type' => 'tabs',
						'controls' => [
							'side_panel_tab_normal' => [
								'type' => 'tab',
								'label' => esc_html__( 'Normal', 'cryptop' ),
								'condition' => [
									'MB_side_panel' => 'yes',
								],
								'controls' => [
									'side_panel_background_title' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_side_panel' => 'yes',
										],
									],
									'side_panel_background' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_side_panel' => 'yes',
										],
									],
									'side_panel_pattern_title' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_side_panel' => 'yes',
											'side_panel_pattern_enable' => 'yes',
										],
									],
									'side_panel_pattern' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_side_panel' => 'yes',
											'side_panel_pattern_enable' => 'yes',
										],
									],
									'side_panel_font_title' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_side_panel' => 'yes',
										],
									],
									'side_panel_font_color' => [
										'label' => esc_html__( 'Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_side_panel' => 'yes',
										],	
									],
								],
							], //--side_panel_tab_normal
							'side_panel_tab_hover' => [
								'type' => 'tab',
								'label' => esc_html__( 'Hover', 'cryptop' ),
								'condition' => [
									'MB_side_panel' => 'yes',
								],
								'controls' => [
									'side_panel_background_title_hover' => [
										'label' => esc_html__( 'Background', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_side_panel' => 'yes',
										],
									],
									'side_panel_background_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}:hover',
										'condition' => [
											'MB_side_panel' => 'yes',
										],
									],
									'side_panel_pattern_title_hover' => [
										'label' => esc_html__( 'Pattern (Overlay)', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_side_panel' => 'yes',
											'side_panel_pattern_enable' => 'yes',
										],
									],
									'side_panel_pattern_hover' => [
										'type' => 'group',
										'group' => CWS_Group_Control_Background::get_type(),
										'selector' => '{{WRAPPER}}',
										'condition' => [
											'MB_side_panel' => 'yes',
											'side_panel_pattern_enable' => 'yes',
										],
									],
									'side_panel_font_title_hover' => [
										'label' => esc_html__( 'Font color', 'cryptop' ),
										'type' => Controls_Manager::HEADING,
										'separator' => 'before',
										'condition' => [
											'MB_side_panel' => 'yes',
										],
									],
									'side_panel_font_color_hover' => [
										'label' => esc_html__( 'Font Color', 'cryptop' ),
										'type' => Controls_Manager::COLOR,
										'value' => '#ffffff',
										'default' => '#ffffff',
										'selectors' => [
											'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
										],
										'condition' => [
											'MB_side_panel' => 'yes',
										],	
									],
									'side_panel_hover_transition' => [
										'label' => esc_html__( 'Transition Duration', 'cryptop' ),
										'type' => Controls_Manager::SLIDER,
										'separator' => 'before',
										'default' => [
											'size' => 0.3,
										],
										'condition' => [
											'MB_side_panel' => 'yes',
										],
										'range' => [
											'px' => [
												'max' => 3,
												'step' => 0.1,
											],
										],
										'selectors' => [
											'{{WRAPPER}}' => 'transition: background {{SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
										],
									]
								]
							], //--side_panel_tab_hover
						]
					],
				]
			],
		]
	];

	return $elementor_metaboxes;
}

function cws_get_metaboxes_structure ($cws_theme_funcs){
	$elementor_page_metaboxes = cws_page_metaboxes($cws_theme_funcs);
	$metaboxes_structure = array();
	cws_get_fields_name('', $elementor_page_metaboxes, $metaboxes_structure);

	return $metaboxes_structure;
}

function cws_get_fields_name($section_name, $arr_controls, &$arr_fields){

	foreach ($arr_controls as $sections => $section) {

		if (is_string($section_name) && !empty($section_name) && $section_name == 'controls'){

			if (isset($section['type'])){
				if ($section['type'] == 'group'){
					$arr_fields[] = $sections;
				} else if ($section['type'] == 'tabs') {
					cws_get_fields_name($sections, $section, $arr_fields);
				} else if ($section['type'] == 'tab') {
					cws_get_fields_name($sections, $section, $arr_fields);
				} else {
					$arr_fields[] = $sections;
				}
			}
			continue;
		}

		if(is_array($section)){
			cws_get_fields_name($sections, $section, $arr_fields);			
		}
	}

}

//Recursive Controls process
function cws_handle_controls(&$page, $value){
	
	foreach ( $value['controls'] as $controls_name => $controls_setting ) {

		if (isset($controls_setting['type']) && $controls_setting['type'] == 'group'){

			$controls_setting['name'] = $controls_name;

			$page->add_group_control(
				$controls_setting['group'],
				array_filter($controls_setting, function($k, $v) {
					return !in_array($v, array('type', 'group')); //Exclude keys from array
				}, ARRAY_FILTER_USE_BOTH)
			);

		} else if (isset($controls_setting['type']) && $controls_setting['type'] == 'tabs') {

			$page->start_controls_tabs( $controls_name );

				cws_handle_controls($page, $controls_setting); //Self call

			$page->end_controls_tabs();

		} else if (isset($controls_setting['type']) && $controls_setting['type'] == 'tab') {

			$page->start_controls_tab(
				$controls_name,
				array_filter($controls_setting, function($k, $v) {
				return !in_array($v, array('type', 'controls')); //Exclude keys from array
				}, ARRAY_FILTER_USE_BOTH)
			);
				
				cws_handle_controls($page, $controls_setting); //Self call

			$page->end_controls_tab();

		} else {							
			$page->add_control(
				$controls_name, $controls_setting
			);
		}

	}
}

function cws_page_settings_controls($page) {
	global $cws_theme_funcs;

	//Register CWS Tab
	Controls_Manager::add_tab( 'cws-tab', esc_html__( 'CWS Themes', 'cryptop' ) );

	//Get all metabox fields
	$elementor_metaboxes = cws_page_metaboxes($cws_theme_funcs);

/*	$page->start_controls_section(
		'section_icons',
		[
			'label' => esc_html__( 'Icons', 'cryptop' ),
			'tab' => 'cws-tab',
		]
	);

		$page->add_control(
			'c27_header_heading',
			[
				'label' => esc_html__( 'Header Options', 'cryptop' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$page->add_control(
			'MB_header',
			[
				'label' => esc_html__( 'Hide Header? lolo4ka', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

	$page->end_controls_section();*/

	if (!empty($elementor_metaboxes)){
		foreach ( $elementor_metaboxes as $section_name => $section_setting ) {

			foreach ($section_setting  as $key => $value) {

				//Section
				$page->start_controls_section(
					$key,
					array_filter($value, function($k, $v) {
						return !in_array($v, array('controls')); //Exclude keys from array
					}, ARRAY_FILTER_USE_BOTH)
				);

				//Controls
				cws_handle_controls($page, $value);

			$page->end_controls_section();
			}

		}
	}

}