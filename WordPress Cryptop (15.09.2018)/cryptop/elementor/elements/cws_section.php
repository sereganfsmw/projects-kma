<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Section
class CWS_Elementor_Section extends Element_Section {

	public function get_title() {
		return esc_html__( 'CWS Section', 'cryptop' );
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}	

	protected static function get_default_edit_tools() {
		$section_label = __( 'Section', 'elementor' );

		return [
			'add' => [
				/* translators: %s: Section label */
				'title' => sprintf( __( 'Add %s', 'elementor' ), $section_label ),
				'icon' => 'plus',
			],
			'edit' => [
				/* translators: %s: Section label */
				'title' => sprintf( __( 'Edit %s', 'elementor' ), $section_label ),
				'icon' => 'section',
			],
			'remove' => [
				/* translators: %s: Section label */
				'title' => sprintf( __( 'Delete %s', 'elementor' ), $section_label ),
				'icon' => 'close',
			],
		];
	}

	protected function _register_controls() {
		//==========================CWS EXTRA FIELDS==========================
		wp_enqueue_script ('jquery-slick');
		wp_enqueue_script ('parallax');
		wp_enqueue_script ('jquery_easing');

		$cws_animation_arr = [
			'linear' => 'linear',
			'ease' => 'ease',
			'ease-in' => 'ease-in',
			'ease-out' => 'ease-out',
			'ease-in-out' => 'ease-in-out',
		];

		$cws_jQuery_animation_arr = [
			'ease' => 'ease',
			'linear' => 'linear',
			'swing' => 'swing',
			'easeInQuad' => 'easeInQuad',
			'easeOutQuad' => 'easeOutQuad',
			'easeInOutQuad' => 'easeInOutQuad',
			'easeInCubic' => 'easeInCubic',
			'easeOutCubic' => 'easeOutCubic',
			'easeInOutCubic' => 'easeInOutCubic',
			'easeInQuart' => 'easeInQuart',
			'easeOutQuart' => 'easeOutQuart',
			'easeInOutQuart' => 'easeInOutQuart',
			'easeInQuint' => 'easeInQuint',
			'easeOutQuint' => 'easeOutQuint',
			'easeInOutQuint' => 'easeInOutQuint',
			'easeInSine' => 'easeInSine',
			'easeOutSine' => 'easeOutSine',
			'easeInOutSine' => 'easeInOutSine',
			'easeInExpo' => 'easeInExpo',
			'easeOutExpo' => 'easeOutExpo',
			'easeInOutExpo' => 'easeInOutExpo',
			'easeInCirc' => 'easeInCirc',
			'easeOutCirc' => 'easeOutCirc',
			'easeInOutCirc' => 'easeInOutCirc',
			'easeInElastic' => 'easeInElastic',
			'easeOutElastic' => 'easeOutElastic',
			'easeInOutElastic' => 'easeInOutElastic',
			'easeInBack' => 'easeInBack',
			'easeOutBack' => 'easeOutBack',
			'easeInOutBack' => 'easeInOutBack',
			'easeInBounce' => 'easeInBounce',
			'easeOutBounce' => 'easeOutBounce',
			'easeInOutBounce' => 'easeInOutBounce',
		];
		//==========================//CWS EXTRA FIELDS==========================

		$this->start_controls_section(
			'section_layout',
			[
				'label' => __( 'Layout', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
			]
		);

		$this->add_control(
			'stretch_section',
			[
				'label' => __( 'Stretch Section', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'return_value' => 'section-stretched',
				'prefix_class' => 'elementor-',
				'hide_in_inner' => true,
				'description' => __( 'Stretch the section to the full width of the page using JS.', 'cryptop' ) . sprintf( ' <a href="%1$s" target="_blank">%2$s</a>', 'https://go.elementor.com/stretch-section/', __( 'Learn more.', 'cryptop' ) ),
				'render_type' => 'none',
				'frontend_available' => true,
			]
		);

		//==========================//CWS EXTRA FIELDS==========================
		$this->add_control(
			'cws_carousel_section',
			[
				'label' => esc_html__( 'Use Carousel (CWS)', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'return_value' => 'section-carousel',
				'prefix_class' => 'elementor-',
				'render_type' => 'template',
				'description' => esc_html__( 'To make section carousel the page using JS. You can change slides using the keyboard arrows (Slick slider)', 'cryptop' ) . sprintf( ' <a href="%1$s" target="_blank">%2$s</a>', 'http://kenwheeler.github.io/slick/', esc_html__( 'Learn more.', 'cryptop' ) ),
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'cws_accordion_section',
							'operator' => '!==',
							'value' => 'section-accordion',
						], [
							'name' => 'cws_tabs_section',
							'operator' => '!==',
							'value' => 'section-tabs',
						],
					],
				],					
			]
		);		

		$this->add_control(
			'cws_accordion_section',
			[
				'label' => esc_html__( 'Use Accordion (CWS)', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'return_value' => 'section-accordion',
				'prefix_class' => 'elementor-',
				'render_type' => 'template',
				'description' => esc_html__( 'To make section accordion the page using JS', 'cryptop' ),
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'cws_carousel_section',
							'operator' => '!==',
							'value' => 'section-carousel',
						], [
							'name' => 'cws_tabs_section',
							'operator' => '!==',
							'value' => 'section-tabs',
						],
					],
				],				
			]
		);

		$this->add_control(
			'cws_tabs_section',
			[
				'label' => esc_html__( 'Use Tabs (CWS)', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'return_value' => 'section-tabs',
				'prefix_class' => 'elementor-',
				'render_type' => 'template',
				'description' => esc_html__( 'To make tabs from section the page using JS', 'cryptop' ),
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'cws_carousel_section',
							'operator' => '!==',
							'value' => 'section-carousel',
						], [
							'name' => 'cws_accordion_section',
							'operator' => '!==',
							'value' => 'section-accordion',
						],
					],
				],				
			]
		);
		//==========================//CWS EXTRA FIELDS==========================	

		$this->add_control(
			'layout',
			[
				'label' => __( 'Content Width', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'boxed',
				'options' => [
					'boxed' => __( 'Boxed', 'cryptop' ),
					'full_width' => __( 'Full Width', 'cryptop' ),
				],
				'prefix_class' => 'elementor-section-',
			]
		);

		$this->add_control(
			'content_width',
			[
				'label' => __( 'Content Width', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 500,
						'max' => 1600,
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-container' => 'max-width: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'layout' => [ 'boxed' ],
				],
				'show_label' => false,
				'separator' => 'none',
			]
		);

		$this->add_control(
			'gap',
			[
				'label' => __( 'Columns Gap', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'cryptop' ),
					'no' => __( 'No Gap', 'cryptop' ),
					'narrow' => __( 'Narrow', 'cryptop' ),
					'extended' => __( 'Extended', 'cryptop' ),
					'wide' => __( 'Wide', 'cryptop' ),
					'wider' => __( 'Wider', 'cryptop' ),
				],
			]
		);

		$this->add_control(
			'height',
			[
				'label' => __( 'Height', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'cryptop' ),
					'full' => __( 'Fit To Screen', 'cryptop' ),
					'min-height' => __( 'Min Height', 'cryptop' ),
				],
				'prefix_class' => 'elementor-section-height-',
				'hide_in_inner' => true,
			]
		);

		$this->add_responsive_control(
			'custom_height',
			[
				'label' => __( 'Minimum Height', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 400,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1440,
					],
					'vh' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'size_units' => [ 'px', 'vh' ],
				'selectors' => [
					'{{WRAPPER}} > .elementor-container' => 'min-height: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'height' => [ 'min-height' ],
				],
				'hide_in_inner' => true,
			]
		);

		$this->add_control(
			'height_inner',
			[
				'label' => __( 'Height', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'default',
				'options' => [
					'default' => __( 'Default', 'cryptop' ),
					'min-height' => __( 'Min Height', 'cryptop' ),
				],
				'prefix_class' => 'elementor-section-height-',
				'hide_in_top' => true,
			]
		);

		$this->add_responsive_control(
			'custom_height_inner',
			[
				'label' => __( 'Minimum Height', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 400,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 1440,
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-container' => 'min-height: {{SIZE}}{{UNIT}};',
				],
				'condition' => [
					'height_inner' => [ 'min-height' ],
				],
				'hide_in_top' => true,
			]
		);

		$this->add_control(
			'column_position',
			[
				'label' => __( 'Column Position', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'middle',
				'options' => [
					'stretch' => __( 'Stretch', 'cryptop' ),
					'top' => __( 'Top', 'cryptop' ),
					'middle' => __( 'Middle', 'cryptop' ),
					'bottom' => __( 'Bottom', 'cryptop' ),
				],
				'prefix_class' => 'elementor-section-items-',
				'condition' => [
					'height' => [ 'full', 'min-height' ],
				],
			]
		);

		$this->add_control(
			'content_position',
			[
				'label' => __( 'Content Position', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'' => __( 'Default', 'cryptop' ),
					'top' => __( 'Top', 'cryptop' ),
					'middle' => __( 'Middle', 'cryptop' ),
					'bottom' => __( 'Bottom', 'cryptop' ),
				],
				'prefix_class' => 'elementor-section-content-',
			]
		);

		$possible_tags = [
			'div',
			'header',
			'footer',
			'main',
			'article',
			'section',
			'aside',
			'nav',
		];

		$options = [
			'' => __( 'Default', 'cryptop' ),
		] + array_combine( $possible_tags, $possible_tags );

		$this->add_control(
			'html_tag',
			[
				'label' => __( 'HTML Tag', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'options' => $options,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'structure',
			[
				'label' => __( 'Structure', 'cryptop' ),
				'type' => Controls_Manager::STRUCTURE,
				'default' => '10',
				'render_type' => 'none',
			]
		);

		$this->end_controls_section();

		//==========================CWS EXTRA FIELDS==========================
		// Section Carousel
		$this->start_controls_section(
			'section_carousel',
			[
				'label' => esc_html__( 'Carousel settings (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'condition' => [
					'cws_carousel_section' => 'section-carousel',
				],				
			]
		);

		$cws_slides_columns = range( 1, 4 );
		$cws_slides_columns = array_combine( $cws_slides_columns, $cws_slides_columns );

		$cws_slides_rows = range( 1, 2 );
		$cws_slides_rows = array_combine( $cws_slides_rows, $cws_slides_rows );

		$this->add_responsive_control(
			'cws_slides_columns',
			[
				'label' => esc_html__( 'Slides to Show (Columns)', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => '1',
				'options' => $cws_slides_columns,
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'cws_slides_rows',
			[
				'label' => esc_html__( 'Slides to Show (Rows)', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => '1',
				'options' => $cws_slides_rows,
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'cws_slide_mode',
			[
				'label' => esc_html__( 'Slide mode', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'slide',
				'options' => [
					'fade' => esc_html__( 'Fade', 'cryptop' ),
					'slide' => esc_html__( 'Slide', 'cryptop' ),
				],
				'condition' => [
					'cws_slides_columns' => '1',
				],				
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'cws_navigation',
			[
				'label' => esc_html__( 'Navigation', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'both',
				'options' => [
					'both' => esc_html__( 'Arrows and Dots', 'cryptop' ),
					'arrows' => esc_html__( 'Arrows', 'cryptop' ),
					'dots' => esc_html__( 'Dots', 'cryptop' ),
					'none' => esc_html__( 'None', 'cryptop' ),
				],
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'cws_spacing_slides',
			[
				'label' => esc_html__( 'Spacings between slides', 'cryptop' ),
				"description"	=> esc_html__( 'In Pixels', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],	
				'selectors' => [
					'{{WRAPPER}} .slick-slider .slick-track .elementor-widget-wrap' => 'margin: 0 calc({{SIZE}}{{UNIT}} / 2);',
					'{{WRAPPER}} .slick-slider .slick-slide .elementor-editor-column-settings' => 'left: calc({{SIZE}}{{UNIT}} / 2);',
				],						
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 0,
				],
			]
		);

		$this->add_control(
			'cws_item_center',
			[
				'label' => esc_html__( 'Center item', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$this->add_control(
			'cws_item_side_paddings',
			[
				'label' => esc_html__( 'Side padding (Center item)', 'cryptop' ),
				"description"	=> esc_html__( 'In Pixels', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'condition' => [
					'cws_item_center' => 'yes'
				],				
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 50,
				],
			]
		);

		$this->add_control(
			'cws_disable_side_paddings',
			[
				'label' => esc_html__( 'Disable paddings', 'cryptop' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => '',
				'options' => [
					'-'    => [
						'title' => esc_html__( 'Left', 'cryptop' ),
						'icon' => 'fa fa-align-left',
					],
					'+' => [
						'title' => esc_html__( 'Right', 'cryptop' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} .slick-slider .slick-track' => 'left: calc({{VALUE}}{{cws_item_side_paddings.SIZE}}{{cws_item_side_paddings.UNIT}} {{VALUE}} calc({{cws_spacing_slides.SIZE}}{{cws_spacing_slides.UNIT}} / 2));',
				],	
				'condition' => [
					'cws_item_center' => 'yes',
				],
			]
		);			

		$this->add_control(
			'cws_autoplay',
			[
				'label' => esc_html__( 'Autoplay', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',				
			]
		);	

		$this->add_control(
			'cws_autoplay_speed',
			[
				'label' => esc_html__( 'Autoplay speed', 'cryptop' ),
				'type' => Controls_Manager::NUMBER,				
				'min' => 100,
				'max' => 10000,
				'step' => 1000,
				'default' => 3000,
				'condition' => [
					'cws_autoplay' => 'yes'
				],
			]
		);

		$this->add_control(
			'cws_pause_on_hover',
			[
				'label' => esc_html__( 'Pause on Hover', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
				'condition' => [
					'cws_autoplay' => 'yes'
				],			
			]
		);	

		$this->add_control(
			'cws_swipe',
			[
				'label' => esc_html__( 'Swipe', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',				
			]
		);	

		$this->add_control(
			'cws_infinite',
			[
				'label' => esc_html__( 'Infinite Loop', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',		
			]
		);	

		$this->add_control(
			'cws_animation',
			[
				'label' => esc_html__( 'Animation', 'cryptop' ),
				'type' => Controls_Manager::SELECT2,
				'default' => 'ease',
				'options' => $cws_animation_arr,
				'condition' => [
					'cws_slides_columns' => '1',
				],
				'frontend_available' => true,
			]
		);		

		$this->add_control(
			'cws_animation_speed',
			[
				'label' => esc_html__( 'Animation Speed', 'cryptop' ),
				'type' => Controls_Manager::NUMBER,
				'default' => 300,
				'frontend_available' => true,
			]
		);


		$this->end_controls_section();

		$this->start_controls_section(
			'section_style_navigation',
			[
				'label' => esc_html__( 'Carousel Navigation (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'cws_carousel_section',
									'operator' => '==',
									'value' => 'section-carousel'
								]
							]
						], [
							'relation' => 'or',
							'terms' => [
								[
									'name' => 'cws_navigation',
									'operator' => '==',
									'value' => 'arrows'
								], [
									'name' => 'cws_navigation',
									'operator' => '==',
									'value' => 'dots'
								], [
									'name' => 'cws_navigation',
									'operator' => '==',
									'value' => 'both'
								]
							]
						]
					]
				]				
			]
		);

			$this->add_control(
				'cws_heading_style_arrows',
				[
					'label' => esc_html__( 'Arrows', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'cws_navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_arrows_position',
				[
					'label' => esc_html__( 'Position', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'outside',
					'options' => [
						'inside' => esc_html__( 'Inside', 'cryptop' ),
						'outside' => esc_html__( 'Outside', 'cryptop' ),
					],
					'condition' => [
						'cws_navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_arrows_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 10,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 35,
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-slick-slider .slick-next:before, {{WRAPPER}} .elementor-slick-slider .slick-prev:before' => 'font-size: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'cws_navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_arrows_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .elementor-slick-slider .slick-next:before, {{WRAPPER}} .elementor-slick-slider .slick-prev:before' => 'color: {{VALUE}};',
					],
					'condition' => [
						'cws_navigation' => [ 'arrows', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_heading_style_dots',
				[
					'label' => esc_html__( 'Dots', 'cryptop' ),
					'type' => Controls_Manager::HEADING,
					'separator' => 'before',
					'condition' => [
						'cws_navigation' => [ 'dots', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_dots_position',
				[
					'label' => esc_html__( 'Position', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'outside',
					'options' => [
						'outside' => esc_html__( 'Outside', 'cryptop' ),
						'inside' => esc_html__( 'Inside', 'cryptop' ),
					],
					'condition' => [
						'cws_navigation' => [ 'dots', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_dots_size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 1,
							'max' => 15,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 6,
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-slick-slider ul.slick-dots li button:before' => 'font-size: {{SIZE}}{{UNIT}};',
					],
					'condition' => [
						'cws_navigation' => [ 'dots', 'both' ],
					],
				]
			);

			$this->add_control(
				'cws_dots_color',
				[
					'label' => esc_html__( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .elementor-slick-slider ul.slick-dots li button:before' => 'color: {{VALUE}};',
					],
					'condition' => [
						'cws_navigation' => [ 'dots', 'both' ],
					],
				]
			);

		$this->end_controls_section();

		// Section Accordion
		$this->start_controls_section(
			'section_accordion_settings',
			[
				'label' => esc_html__( 'Accordion settings (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'condition' => [
					'cws_accordion_section' => 'section-accordion',
				],				
			]
		);

			$this->add_control(
				'cws_accordion_description',
				[
					'type' => Controls_Manager::RAW_HTML,
					'content_classes' => 'elementor-control-field-description cws-elementor-info-text',
					'raw' => esc_html__( 'Change some settings to see the new accordions titles', 'cryptop' ),
					'separator' => 'none',
				]
			);

			//REPEATER
			$repeater = new Repeater();
			$repeater->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => '',
					'title' => esc_html__( 'Title', 'cryptop' ),
				]
			);

			$this->add_control(
				'cws_accordion_items',
				[
					'label'       => esc_html__( 'Accordion items', 'cryptop' ),
					'type'        => Controls_Manager::REPEATER,
					'show_label'  => true,
					'render_type' => 'ui',
					'default' => [
						[
							'title' => esc_html__( 'Accordion #1', 'cryptop' ),
						],
					],
					'fields'      => array_values( $repeater->get_controls() ),
					'title_field' => '{{{title}}}',
				]
			);
			//---REPEATER

			$this->add_control(
				'cws_accordion_mode',
				[
					'label' => esc_html__( 'Mode', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'accordion',
					'options' => [
						'accordion' => esc_html__( 'Accordion', 'cryptop' ),
						'toggle' => esc_html__( 'Toggle', 'cryptop' ),
					],
				]
			);

			$cws_accordion_active = range( 1, 10 );
			$cws_accordion_active = array_combine( $cws_accordion_active, $cws_accordion_active );

			$this->add_control(
				'cws_accordion_active',
				[
					'label' => esc_html__( 'Active', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '',
					'options' => [
						'' => esc_html__( 'None', 'cryptop' ),
					] + $cws_accordion_active,
					'frontend_available' => true,
				]
			);

			$this->add_control(
				'cws_accordion_collapsible',
				[
					'label' => esc_html__( 'Collapsible', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'condition' => [
						'cws_accordion_mode' => 'toggle',
					],
					'default' => 'yes',
				]
			);	

			$this->add_control(
				'cws_accordion_auto_height',
				[
					'label' => esc_html__( 'Auto Height', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'condition' => [
						'cws_accordion_mode' => 'toggle',
					],								
				]
			);

			$this->add_control(
				'cws_accordion_hover',
				[
					'label' => esc_html__( 'Open on hover', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',	
					'condition' => [
						'cws_accordion_mode' => 'toggle',
					],								
				]
			);			

		$this->end_controls_section();

		// Section Accordion (Style)
		$this->start_controls_section(
			'section_accordion_styles',
			[
				'label' => esc_html__( 'Accordion styles (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'condition' => [
					'cws_accordion_section' => 'section-accordion',
				],				
			]
		);

			$this->add_control(
				'cws_accordion_border_enable',
				[
					'label' => esc_html__( 'Accordion border', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);	


			$this->add_control(
				'cws_accordion_border',
				[
					'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'solid',
					'options' => [
						'' => esc_html__( 'None', 'cryptop' ),
						'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
						'double' => _x( 'Double', 'Border Control', 'cryptop' ),
						'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
						'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
					],
					'condition' => [
						'cws_accordion_border_enable' => 'yes',
					],							
					'selectors' => [
						'{{WRAPPER}} .cws_accordion_init' => 'border-style: {{VALUE}};',
					],	
				]
			);

			$this->add_control(
				'cws_accordion_border_width',
				[
					'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'selectors' => [
						'{{WRAPPER}} .cws_accordion_init' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'top' => 1,
						'right' => 1,
						'bottom' => 1,
						'left' => 1
					],					
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'cws_accordion_border',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'cws_accordion_border_enable',
								'operator' => '==',
								'value' => 'yes',
							],
						],
					],					
				]
			);					

			$this->add_control(
				'cws_accordion_border_color',
				[
					'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_accordion_init' => 'border-color: {{VALUE}};',
					],
					'default' => '#dde0e7',
					'value' => '#dde0e7',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'cws_accordion_border',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'cws_accordion_border_enable',
								'operator' => '==',
								'value' => 'yes',
							],
						],
					],
				]
			);

			$this->start_controls_tabs( 'cws_accordion_style');

				$this->start_controls_tab(
					'cws_tab_normal_accordion',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),						
					]
				);

					$this->add_control(
						'cws_accordion_heading_color',
						[
							'label' => esc_html__( 'Heading Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
			                'scheme' => [
			                    'type' => Scheme_Color::get_type(),
			                    'value' => Scheme_Color::COLOR_1,
			                ],
			                'default' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item .accordion-header' => 'color: {{VALUE}};',
							],
							'separator' => 'none',
						]
					);

					$this->add_control(
						'cws_accordion_background_color',
						[
							'label' => esc_html__( 'Heading Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item .accordion-header' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_accordion_active_heading_color',
						[
							'label' => esc_html__( 'Heading Color (Active Row)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item.active .accordion-header' => 'color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_accordion_active_background_color',
						[
							'label' => esc_html__( 'Background Color (Active Row)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#F6F7F9',
							'value' => '#F6F7F9',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item.active .accordion-header' => 'background-color: {{VALUE}};',
							],
						]
					);	

				$this->end_controls_tab();

				$this->start_controls_tab(
					'cws_tab_hover_accordion',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),						
					]
				);

					$this->add_control(
						'cws_accordion_heading_color_hover',
						[
							'label' => esc_html__( 'Heading Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item .accordion-header:hover' => 'color: {{VALUE}};',
							],
							'separator' => 'none',
						]
					);

					$this->add_control(
						'cws_accordion_background_color_hover',
						[
							'label' => esc_html__( 'Heading Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#F6F7F9',
							'value' => '#F6F7F9',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item .accordion-header:hover' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_accordion_active_heading_color_hover',
						[
							'label' => esc_html__( 'Heading Color (Active Row)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item.active .accordion-header:hover' => 'color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_accordion_active_background_color_hover',
						[
							'label' => esc_html__( 'Background Color (Active Row)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#F6F7F9',
							'value' => '#F6F7F9',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-accordion > .accordion-item.active .accordion-header:hover' => 'background-color: {{VALUE}};',
							],
						]
					);					

				$this->end_controls_tab();

			$this->end_controls_tabs();

			$this->add_control(
				'cws_accordion_margins',
				[
					'label' => esc_html__( 'Content Paddings', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px'],
					'selectors' => [
						'{{WRAPPER}} .elementor-cws-accordion > .accordion-item .accordion-content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'top' => 20,
						'right' => 20,
						'bottom' => 20,
						'left' => 20
					],
				]
			);

			$this->add_control(
				'cws_accordion_content_background_color',
				[
					'label' => esc_html__( 'Content Background Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#ffffff',
					'value' => '#ffffff',
					'selectors' => [
						'{{WRAPPER}} .cws_accordion_init' => 'background-color: {{VALUE}};',
					],
				]
			);

		$this->end_controls_section();

		// Section Tabs
		$this->start_controls_section(
			'section_tabs',
			[
				'label' => esc_html__( 'Tabs settings (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'condition' => [
					'cws_tabs_section' => 'section-tabs',
				],				
			]
		);

			$this->add_control(
				'cws_tabs_description',
				[
					'type' => Controls_Manager::RAW_HTML,
					'content_classes' => 'elementor-control-field-description cws-elementor-info-text',
					'raw' => esc_html__( 'Change some settings to see the new tabs titles', 'cryptop' ),
					'separator' => 'none',
				]
			);

			//REPEATER
			$repeater = new Repeater();
			$repeater->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => '',
					'title' => esc_html__( 'Title', 'cryptop' ),
				]
			);

			$this->add_control(
				'cws_tabs_titles',
				[
					'label'       => esc_html__( 'Tabs titles', 'cryptop' ),
					'type'        => Controls_Manager::REPEATER,
					'show_label'  => true,
					'render_type' => 'ui',
					'default' => [
						[
							'title' => esc_html__( 'Tab #1', 'cryptop' ),
						],
					],
					'fields'      => array_values( $repeater->get_controls() ),
					'title_field' => '{{{title}}}',
				]
			);
			//---REPEATER

			$this->add_control(
				'cws_tabs_mode',
				[
					'label' => esc_html__( 'Mode', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'horizontal',
					'options' => [
						'horizontal' => esc_html__( 'Horizontal', 'cryptop' ),
						'vertical' => esc_html__( 'Vertical', 'cryptop' ),
					],
				]
			);

			$this->add_control(
				'cws_tabs_animation',
				[
					'label' => esc_html__( 'Tab animation', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'fade',
					'options' => [
						'none' => esc_html__( 'None', 'cryptop' ),
						'fade' => esc_html__( 'Fade', 'cryptop' ),
						'slide' => esc_html__( 'Slide', 'cryptop' ),
					],
				]
			);			

			$cws_tabs_active = range( 1, 10 );
			$cws_tabs_active = array_combine( $cws_tabs_active, $cws_tabs_active );

			$this->add_control(
				'cws_tabs_active',
				[
					'label' => esc_html__( 'Active', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '1',
					'options' => $cws_tabs_active,
					'frontend_available' => true,
				]
			);

			$this->add_control(
				'cws_tabs_auto_height',
				[
					'label' => esc_html__( 'Auto Height', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',	
					'condition' => [
						'cws_tabs_animation!' => 'slide',
					],												
				]
			);

		$this->end_controls_section();

		// Section Tabs (Style)
		$this->start_controls_section(
			'section_tabs_styles',
			[
				'label' => esc_html__( 'Tabs styles (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
				'condition' => [
					'cws_tabs_section' => 'section-tabs',
				],				
			]
		);

			$this->add_control(
				'cws_tabs_border_enable',
				[
					'label' => esc_html__( 'Tab border', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);	


			$this->add_control(
				'cws_tabs_border',
				[
					'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'solid',
					'options' => [
						'' => esc_html__( 'None', 'cryptop' ),
						'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
						'double' => _x( 'Double', 'Border Control', 'cryptop' ),
						'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
						'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
					],
					'condition' => [
						'cws_tabs_border_enable' => 'yes',
					],							
					'selectors' => [
						'{{WRAPPER}} .cws_tabs_init' => 'border-style: {{VALUE}};',
					],	
				]
			);

			$this->add_control(
				'cws_tabs_border_width',
				[
					'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'selectors' => [
						'{{WRAPPER}} .cws_tabs_init' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'top' => 1,
						'right' => 1,
						'bottom' => 1,
						'left' => 1
					],						
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'cws_tabs_border',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'cws_tabs_border_enable',
								'operator' => '==',
								'value' => 'yes',
							],
						],
					],					
				]
			);					

			$this->add_control(
				'cws_tabs_border_color',
				[
					'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'selectors' => [
						'{{WRAPPER}} .cws_tabs_init' => 'border-color: {{VALUE}};',
					],
					'default' => '#dde0e7',
					'value' => '#dde0e7',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'cws_tabs_border',
								'operator' => '!=',
								'value' => '',
							], [
								'name' => 'cws_tabs_border_enable',
								'operator' => '==',
								'value' => 'yes',
							],
						],
					],
				]
			);

			$this->start_controls_tabs( 'cws_tabs_style');

				$this->start_controls_tab(
					'cws_tab_normal_tabs',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),						
					]
				);

					$this->add_control(
						'cws_tabs_heading_color',
						[
							'label' => esc_html__( 'Title Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
			                'scheme' => [
			                    'type' => Scheme_Color::get_type(),
			                    'value' => Scheme_Color::COLOR_1,
			                ],
			                'default' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title' => 'color: {{VALUE}};',
							],
							'separator' => 'none',
						]
					);

					$this->add_control(
						'cws_tabs_background_color',
						[
							'label' => esc_html__( 'Title Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_tabs_active_heading_color',
						[
							'label' => esc_html__( 'Title Color (Active Tab)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title.active' => 'color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_tabs_active_background_color',
						[
							'label' => esc_html__( 'Title Background Color (Active Tab)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#F6F7F9',
							'value' => '#F6F7F9',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title.active' => 'background-color: {{VALUE}};',
							],
						]
					);	

				$this->end_controls_tab();

				$this->start_controls_tab(
					'cws_tab_hover_tabs',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),						
					]
				);

					$this->add_control(
						'cws_tabs_heading_color_hover',
						[
							'label' => esc_html__( 'Title Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title:hover' => 'color: {{VALUE}};',
							],
							'separator' => 'none',
						]
					);

					$this->add_control(
						'cws_tabs_background_color_hover',
						[
							'label' => esc_html__( 'Title Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#F6F7F9',
							'value' => '#F6F7F9',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title:hover' => 'background-color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_tabs_active_heading_color_hover',
						[
							'label' => esc_html__( 'Title Color (Active Tab)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '',
							'value' => '',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title.active:hover' => 'color: {{VALUE}};',
							],
						]
					);

					$this->add_control(
						'cws_tabs_active_background_color_hover',
						[
							'label' => esc_html__( 'Title Background Color (Active Tab)', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#F6F7F9',
							'value' => '#F6F7F9',
							'selectors' => [
								'{{WRAPPER}} .elementor-cws-tabs .tabs_navigation .tab_title.active:hover' => 'background-color: {{VALUE}};',
							],
						]
					);					

				$this->end_controls_tab();

			$this->end_controls_tabs();

			$this->add_control(
				'cws_tabs_margins',
				[
					'label' => esc_html__( 'Content Paddings', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px'],
					'selectors' => [
						'{{WRAPPER}} .elementor-cws-tabs .tabs_content' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'top' => 20,
						'right' => 20,
						'bottom' => 20,
						'left' => 20
					],
				]
			);

			$this->add_control(
				'cws_tabs_content_background_color',
				[
					'label' => esc_html__( 'Content Background Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#ffffff',
					'value' => '#ffffff',
					'selectors' => [
						'{{WRAPPER}} .cws_tabs_init' => 'background-color: {{VALUE}};',
					],
				]
			);

		$this->end_controls_section();
		//==========================//CWS EXTRA FIELDS==========================

		// Section background
		$this->start_controls_section(
			'section_background',
			[
				'label' => __( 'Background', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'tabs_background' );

		$this->start_controls_tab(
			'tab_background_normal',
			[
				'label' => __( 'Normal', 'cryptop' ),
			]
		);

		//==========================CWS EXTRA FIELDS==========================
		$this->add_group_control(
			CWS_Group_Control_Background::get_type(),
			[
				'name' => 'background',
				'types' => [ 'classic', 'gradient', 'video' ],
				'fields_options' => [
					'background' => [
						'frontend_available' => true,
					],
					'video_link' => [
						'frontend_available' => true,
					],
					'video_start' => [
						'frontend_available' => true,
					],
					'video_end' => [
						'frontend_available' => true,
					],
				],
			]
		);
		//==========================//CWS EXTRA FIELDS==========================

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_background_hover',
			[
				'label' => __( 'Hover', 'cryptop' ),
			]
		);

		//==========================//CWS EXTRA FIELDS==========================
		$this->add_group_control(
			CWS_Group_Control_Background::get_type(),
			[
				'name' => 'background_hover',
				'selector' => '{{WRAPPER}}:hover',
			]
		);
		//==========================//CWS EXTRA FIELDS==========================

		$this->add_control(
			'background_hover_transition',
			[
				'label' => __( 'Transition Duration', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0.3,
				],
				'range' => [
					'px' => [
						'max' => 3,
						'step' => 0.1,
					],
				],
				'render_type' => 'ui',
				'separator' => 'before',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

		// Background Overlay
		$this->start_controls_section(
			'section_background_overlay',
			[
				'label' => __( 'Background Overlay', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'background_background' => [ 'classic', 'gradient', 'video' ],
				],
			]
		);

		$this->start_controls_tabs( 'tabs_background_overlay' );

		$this->start_controls_tab(
			'tab_background_overlay_normal',
			[
				'label' => __( 'Normal', 'cryptop' ),
			]
		);

		//==========================CWS EXTRA FIELDS==========================
		$this->add_group_control(
			CWS_Group_Control_Background::get_type(),
			[
				'name' => 'background_overlay',
				'selector' => '{{WRAPPER}} > .elementor-background-overlay',
			]
		);
		//==========================//CWS EXTRA FIELDS==========================

		$this->add_control(
			'background_overlay_opacity',
			[
				'label' => __( 'Opacity', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => .5,
				],
				'range' => [
					'px' => [
						'max' => 1,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-background-overlay' => 'opacity: {{SIZE}};',
				],
				'condition' => [
					'background_overlay_background' => [ 'classic', 'gradient' ],
				],
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'css_filters',
				'selector' => '{{WRAPPER}} .elementor-background-overlay',
			]
		);

		$this->add_control(
			'overlay_blend_mode',
			[
				'label' => __( 'Blend Mode', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'' => __( 'Normal', 'cryptop' ),
					'multiply' => 'Multiply',
					'screen' => 'Screen',
					'overlay' => 'Overlay',
					'darken' => 'Darken',
					'lighten' => 'Lighten',
					'color-dodge' => 'Color Dodge',
					'saturation' => 'Saturation',
					'color' => 'Color',
					'luminosity' => 'Luminosity',
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-background-overlay' => 'mix-blend-mode: {{VALUE}}',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_background_overlay_hover',
			[
				'label' => __( 'Hover', 'cryptop' ),
			]
		);

		//==========================//CWS EXTRA FIELDS==========================
		$this->add_group_control(
			CWS_Group_Control_Background::get_type(),
			[
				'name' => 'background_overlay_hover',
				'selector' => '{{WRAPPER}}:hover > .elementor-background-overlay',
			]
		);
		//==========================//CWS EXTRA FIELDS==========================

		$this->add_control(
			'background_overlay_hover_opacity',
			[
				'label' => __( 'Opacity', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => .5,
				],
				'range' => [
					'px' => [
						'max' => 1,
						'step' => 0.01,
					],
				],
				'selectors' => [
					'{{WRAPPER}}:hover > .elementor-background-overlay' => 'opacity: {{SIZE}};',
				],
				'condition' => [
					'background_overlay_hover_background' => [ 'classic', 'gradient' ],
				],
			]
		);

		$this->add_group_control(
			Group_Control_Css_Filter::get_type(),
			[
				'name' => 'css_filters_hover',
				'selector' => '{{WRAPPER}}:hover > .elementor-background-overlay',
			]
		);

		$this->add_control(
			'background_overlay_hover_transition',
			[
				'label' => __( 'Transition Duration', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 0.3,
				],
				'range' => [
					'px' => [
						'max' => 3,
						'step' => 0.1,
					],
				],
				'render_type' => 'ui',
				'separator' => 'before',
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

		//==========================CWS EXTRA FIELDS==========================
		// Background Overlay (CWS)
		$this->start_controls_section(
			'section_extra_background_overlay',
			[
				'label' => esc_html__( 'Extra Background Overlay (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->start_controls_tabs( 'tabs_background_overlay_cws' );

			$this->start_controls_tab(
				'tab_background_overlay_cws_normal',
				[
					'label' => __( 'Normal', 'cryptop' ),
				]
			);

			$this->add_group_control(
				CWS_Group_Control_Background::get_type(),
				[
					'name' => 'cws_background_overlay',
					'selector' => '{{WRAPPER}} > .elementor-background-overlay-cws:before',
				]
			);

			$this->add_control(
				'cws_background_overlay_opacity',
				[
					'label' => __( 'Opacity', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'size' => .5,
					],
					'range' => [
						'px' => [
							'max' => 1,
							'step' => 0.01,
						],
					],
					'selectors' => [
						'{{WRAPPER}} > .elementor-background-overlay-cws:before' => 'opacity: {{SIZE}};',
					],
					'condition' => [
						'cws_background_overlay_background' => [ 'classic', 'gradient' ],
					],
				]
			);

			$this->add_control(
				'cws_background_overlay_extra_color',
				[
					'label' => _x( 'Overlay Color', 'Background Control', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '',
					'title' => _x( 'Overlay Color', 'Background Control', 'cryptop' ),
					'selectors' => [
						'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->end_controls_tab();

			$this->start_controls_tab(
				'tab_background_overlay_cws_hover',
				[
					'label' => __( 'Hover', 'cryptop' ),
				]
			);

			$this->add_group_control(
				CWS_Group_Control_Background::get_type(),
				[
					'name' => 'cws_background_overlay_hover',
					'selector' => '{{WRAPPER}}:hover > .elementor-background-overlay-cws:before',
				]
			);

			$this->add_control(
				'cws_background_overlay_hover_opacity',
				[
					'label' => __( 'Opacity', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'size' => .5,
					],
					'range' => [
						'px' => [
							'max' => 1,
							'step' => 0.01,
						],
					],
					'selectors' => [
						'{{WRAPPER}}:hover > .elementor-background-overlay-cws:before' => 'opacity: {{SIZE}};',
					],
					'condition' => [
						'cws_background_overlay_hover_background' => [ 'classic', 'gradient' ],
					],
				]
			);

			$this->add_control(
				'cws_background_overlay_hover_transition',
				[
					'label' => __( 'Transition Duration', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'size' => 0.3,
					],
					'range' => [
						'px' => [
							'max' => 3,
							'step' => 0.1,
						],
					],
					'selectors' => [
						'{{WRAPPER}} > .elementor-background-overlay-cws:before' => 'transition: background {{background_overlay_hover_transition.SIZE}}s, border-radius {{SIZE}}s, opacity {{background_overlay_hover_transition.SIZE}}s',
						'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'transition: background {{background_overlay_hover_transition.SIZE}}s, border-radius {{SIZE}}s, opacity {{background_overlay_hover_transition.SIZE}}s',
					],
					'render_type' => 'ui',
					'separator' => 'before',
				]
			);

			$this->add_control(
				'cws_background_overlay_extra_color_hover',
				[
					'label' => _x( 'Overlay Color', 'Background Control', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '',
					'title' => _x( 'Overlay Color', 'Background Control', 'cryptop' ),
					'selectors' => [
						'{{WRAPPER}}:hover > .elementor-background-overlay-cws:after' => 'background-color: {{VALUE}};',
					],
				]
			);

			$this->end_controls_tab();

			$this->end_controls_tabs();

			$this->add_control(
				'cws_overlay_position',
				[
					'label' => esc_html__( 'Overlay position', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} > .elementor-background-overlay-cws:before' => '{{VALUE}}: 0px;',
						'{{WRAPPER}} > .elementor-background-overlay-cws:after' => '{{VALUE}}: 0px;',
					],	
				]
			);		

			$this->add_control(
				'cws_overlay_width',
				[
					'label' => _x( 'Overlay width', 'Background Control', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ '%' ],
					'default' => [
						'unit' => '%',
						'size' => 50,
					],
					'selectors' => [
						'{{WRAPPER}} > .elementor-background-overlay-cws:before' => 'width: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} > .elementor-background-overlay-cws:after' => 'width: {{SIZE}}{{UNIT}};',
					],
				]
			);				

		$this->end_controls_section();

		// Patterns (CWS)
		$this->start_controls_section(
			'cws_section_patterns',
			[
				'label' => esc_html__( 'Patterns (CWS)', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$this->add_control(
				'cws_patterns_reload',
				[
					'label' => esc_html__( 'Reload changes', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'render_type' => 'template', //ui
					'label_on' => esc_html__( '', 'cryptop' ),
					'label_off' => esc_html__( '', 'cryptop' ),
				]
			);

			$this->add_control(
				'cws_pattern_description',
				[
					'type' => Controls_Manager::RAW_HTML,
					'content_classes' => 'elementor-control-field-description cws-elementor-info-text',
					'raw' => esc_html__( 'Turn swither to reload changes and see the new pattern*', 'cryptop' ),
					'separator' => 'none',
				]
			);

			$this->add_control(
				'cws_patterns_style',
				[
					'label' => esc_html__( 'Patterns style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'inside',
					'options' => [
						'inside' => esc_html__( 'Inside', 'cryptop' ),
						'outside' => esc_html__( 'Outside', 'cryptop' ),
					],
				]
			);

			$this->add_control(
				'cws_patterns_inside',
				[
					'label' => '',
					'type' => Controls_Manager::REPEATER,
					'render_type' => 'ui',
					'title_field' => '<img class="patterns_img_thumb" src="<# if (typeof image !== "undefined"){ #>{{{(image.url)}}}<# } #>">',
					'condition' => [
						'cws_patterns_style' => 'inside',
					],					
					'fields' => [
						[
							'name' => 'image',
							'label' => _x( 'Image', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::MEDIA,
							'title' => _x( 'Background Image', 'Background Control', 'cryptop' ),
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-image: url("{{URL}}");',
							],
						],
						[
							'name' => 'position',
							'label' => _x( 'Position', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::SELECT,
							'default' => '',
							'options' => [
								'' => _x( 'Default', 'Background Control', 'cryptop' ),
								'top left' => _x( 'Top Left', 'Background Control', 'cryptop' ),
								'top center' => _x( 'Top Center', 'Background Control', 'cryptop' ),
								'top right' => _x( 'Top Right', 'Background Control', 'cryptop' ),
								'center left' => _x( 'Center Left', 'Background Control', 'cryptop' ),
								'center center' => _x( 'Center Center', 'Background Control', 'cryptop' ),
								'center right' => _x( 'Center Right', 'Background Control', 'cryptop' ),
								'bottom left' => _x( 'Bottom Left', 'Background Control', 'cryptop' ),
								'bottom center' => _x( 'Bottom Center', 'Background Control', 'cryptop' ),
								'bottom right' => _x( 'Bottom Right', 'Background Control', 'cryptop' ),
								'custom' => _x( 'Custom', 'Background Control', 'cryptop' ),
							],
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-position: {{VALUE}};',
							],
							'condition' => [
								'image[url]!' => '',
							],
						],
						[
							'name' => 'custom_position',
							'label' => esc_html__( 'Custom position (CWS)', 'cryptop' ),
							'type' => Controls_Manager::TEXT,
							'label_block' => true,
							'default' => esc_html__( '50% auto', 'cryptop' ),
							'description' => esc_html__( 'Set left and top position', 'cryptop' ),
							'placeholder' => esc_html__( '50% 50%', 'cryptop' ),
							'responsive' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-position: {{VALUE}};',
							],				
							'condition' => [
								'position' => [ 'custom' ],
							],				
						],
						[
							'name' => 'attachment',
							'label' => _x( 'Attachment', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::SELECT,
							'default' => '',
							'options' => [
								'' => _x( 'Default', 'Background Control', 'cryptop' ),
								'scroll' => _x( 'Scroll', 'Background Control', 'cryptop' ),
								'fixed' => _x( 'Fixed', 'Background Control', 'cryptop' ),
							],
							'selectors' => [
								'(desktop+){{WRAPPER}} {{CURRENT_ITEM}}' => 'background-attachment: {{VALUE}};',
							],
							'condition' => [
								'image[url]!' => '',
							],
						],
						[
							'name' => 'attachment_alert',
							'type' => Controls_Manager::RAW_HTML,
							'content_classes' => 'elementor-control-field-description',
							'raw' => esc_html__( 'Note: Attachment Fixed works only on desktop.', 'cryptop' ),
							'separator' => 'none',
							'condition' => [
								'image[url]!' => '',
								'attachment' => 'fixed',
							],
						],
						[
							'name' => 'repeat',
							'label' => _x( 'Repeat', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::SELECT,
							'default' => '',
							'options' => [
								'' => _x( 'Default', 'Background Control', 'cryptop' ),
								'no-repeat' => _x( 'No-repeat', 'Background Control', 'cryptop' ),
								'repeat' => _x( 'Repeat', 'Background Control', 'cryptop' ),
								'repeat-x' => _x( 'Repeat-x', 'Background Control', 'cryptop' ),
								'repeat-y' => _x( 'Repeat-y', 'Background Control', 'cryptop' ),
							],
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-repeat: {{VALUE}};',
							],
							'condition' => [
								'image[url]!' => '',
							],
						],
						[
							'name' => 'size',
							'label' => _x( 'Size', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::SELECT,
							'default' => '',
							'options' => [
								'' => _x( 'Default', 'Background Control', 'cryptop' ),
								'auto' => _x( 'Auto', 'Background Control', 'cryptop' ),
								'cover' => _x( 'Cover', 'Background Control', 'cryptop' ),
								'contain' => _x( 'Contain', 'Background Control', 'cryptop' ),
								'custom' => _x( 'Custom', 'Background Control', 'cryptop' ),
							],
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-size: {{VALUE}};',
							],
							'condition' => [
								'image[url]!' => '',
							]
						],
						[
							'name' => 'custom_size',
							'label' => esc_html__( 'Custom size (CWS)', 'cryptop' ),
							'type' => Controls_Manager::TEXT,
							'label_block' => true,
							'default' => esc_html__( '50% auto', 'cryptop' ),
							'description' => esc_html__( 'Set left and top size', 'cryptop' ),
							'placeholder' => esc_html__( '50% 50%', 'cryptop' ),
							'responsive' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'background-size: {{VALUE}};',
							],				
							'condition' => [
								'size' => [ 'custom' ],
							],				
						],
						[
							'name' => 'z_index',
							'label' => esc_html__( 'z index', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 1,
							'max' => 100,
							'step' => 1,
							'default' => 1,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'z-index: {{VALUE}}',
							],
						],
						[
							'name' => 'opacity',
							'label' => esc_html__( 'Opacity', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'size_units' => [ 'px' ],
							'label_block' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'opacity: {{SIZE}}',
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,
							],
							'range' => [
								'px' => [
									'min' => 0,
									'max' => 1,
									'step' => 0.01,
								],
							],
						],


						[
							'name' => 'cws_parallaxify',
							'label' => esc_html__( 'Parallaxify on hover', 'cryptop' ),
							'type' => Controls_Manager::SWITCHER,
						],
						[
							'name' => 'cws_x_scalar',
							'label' => esc_html__( 'X-axis parallax intensity', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 2,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],						
						[
							'name' => 'cws_x_limit',
							'label' => esc_html__( 'Maximum x-axis shift', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],	
						[
							'name' => 'cws_y_scalar',
							'label' => esc_html__( 'Y-axis parallax intensity', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 2,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],	
						[
							'name' => 'cws_y_limit',
							'label' => esc_html__( 'Maximum y-axis shift', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],	
						[
							'name' => 'cws_invert_parallax',
							'label' => esc_html__( 'Invert hover', 'cryptop' ),
							'type' => Controls_Manager::SWITCHER,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],								
						],
						[
							'name' => 'cws_x_invert',
							'label' => esc_html__( 'X-axis parallax invert', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
								'cws_invert_parallax' => 'yes',
							],	
						],
						[
							'name' => 'cws_y_invert',
							'label' => esc_html__( 'Y-axis parallax invert', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
								'cws_invert_parallax' => 'yes',
							],	
						],
					],
				]
			);

			$this->add_control(
				'cws_patterns_outside',
				[
					'label' => '',
					'type' => Controls_Manager::REPEATER,
					'render_type' => 'ui', //ui
					'title_field' => '<img class="patterns_img_thumb" src="<# if (typeof image !== "undefined"){ #>{{{(image.url)}}}<# } #>">',
					'condition' => [
						'cws_patterns_style' => 'outside',
					],
					'fields' => [
						[
							'name' => 'image',
							'label' => _x( 'Image', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::MEDIA,
							'title' => _x( 'Background Image', 'Background Control', 'cryptop' ),
						],
						[
							'name' => 'position_x',
							'label' => esc_html__( 'Left', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'size_units' => [ '%' ],
							'label_block' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}} img' => 'left: {{SIZE}}{{UNIT}}',
							],
							'default' => [
								'unit' => '%',
								'size' => 0,
							],
							'range' => [
								'%' => [
									'min' => -20,
									'max' => 120,
									'step' => 1,
								],
							],
						],
						[
							'name' => 'position_y',
							'label' => esc_html__( 'Top', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'size_units' => [ '%' ],
							'label_block' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}} img' => 'top: {{SIZE}}{{UNIT}}',
							],
							'default' => [
								'unit' => '%',
								'size' => 0,
							],
							'range' => [
								'%' => [
									'min' => -20,
									'max' => 120,
									'step' => 1,
								],
							],
						],
						[
							'name' => 'attachment',
							'label' => _x( 'Attachment', 'Background Control', 'cryptop' ),
							'type' => Controls_Manager::SELECT,
							'default' => 'absolute',
							'options' => [
								'absolute' => _x( 'Absolute', 'Background Control', 'cryptop' ),
								'fixed' => _x( 'Fixed', 'Background Control', 'cryptop' ),
							],
							'selectors' => [
								'(desktop+){{WRAPPER}} {{CURRENT_ITEM}} img' => 'position: {{VALUE}};',
							],
							'condition' => [
								'image[url]!' => '',
							],
						],
						[
							'name' => 'attachment_alert',
							'type' => Controls_Manager::RAW_HTML,
							'content_classes' => 'elementor-control-field-description',
							'raw' => esc_html__( 'Note: Attachment Fixed works only on desktop.', 'cryptop' ),
							'separator' => 'none',
							'condition' => [
								'image[url]!' => '',
								'attachment' => 'fixed',
							],
						],						
						[
							'name' => 'custom_size',
							'label' => esc_html__( 'Custom size', 'cryptop' ),
							'type' => Controls_Manager::SWITCHER,
						],
						[
							'name' => 'width',
							'label' => esc_html__( 'Size (%)', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'size_units' => [ '%' ],
							'label_block' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}} img' => 'width: {{SIZE}}{{UNIT}}',
							],
							'condition' => [
								'custom_size' => 'yes',
							],
							'default' => [
								'unit' => '%',
								'size' => 15,
							],
							'range' => [
								'%' => [
									'min' => 0,
									'max' => 100,
									'step' => 1,
								],
							],
						],
						[
							'name' => 'z_index',
							'label' => esc_html__( 'z index', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 1,
							'max' => 100,
							'step' => 1,
							'default' => 1,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}}' => 'z-index: {{VALUE}}',
							],
						],
						[
							'name' => 'opacity',
							'label' => esc_html__( 'Opacity', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'size_units' => [ 'px' ],
							'label_block' => true,
							'selectors' => [
								'{{WRAPPER}} {{CURRENT_ITEM}} img' => 'opacity: {{SIZE}}',
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,
							],
							'range' => [
								'px' => [
									'min' => 0,
									'max' => 1,
									'step' => 0.01,
								],
							],
						],
						[
							'name' => 'cws_parallaxify',
							'label' => esc_html__( 'Parallaxify on hover', 'cryptop' ),
							'type' => Controls_Manager::SWITCHER,
						],
						[
							'name' => 'cws_x_scalar',
							'label' => esc_html__( 'X-axis parallax intensity', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 2,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],						
						[
							'name' => 'cws_x_limit',
							'label' => esc_html__( 'Maximum x-axis shift', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],	
						[
							'name' => 'cws_y_scalar',
							'label' => esc_html__( 'Y-axis parallax intensity', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 2,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],	
						[
							'name' => 'cws_y_limit',
							'label' => esc_html__( 'Maximum y-axis shift', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],	
						],	
						[
							'name' => 'cws_invert_parallax',
							'label' => esc_html__( 'Invert hover', 'cryptop' ),
							'type' => Controls_Manager::SWITCHER,
							'condition' => [
								'cws_parallaxify' => 'yes',
							],								
						],
						[
							'name' => 'cws_x_invert',
							'label' => esc_html__( 'X-axis parallax invert', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
								'cws_invert_parallax' => 'yes',
							],	
						],
						[
							'name' => 'cws_y_invert',
							'label' => esc_html__( 'Y-axis parallax invert', 'cryptop' ),
							'type' => Controls_Manager::NUMBER,				
							'min' => 0,
							'max' => 100,
							'step' => 1,
							'default' => 15,
							'condition' => [
								'cws_parallaxify' => 'yes',
								'cws_invert_parallax' => 'yes',
							],	
						],
					],
				]
			);

			$this->add_control(
				'cws_hide_desktop',
				[
					'label' => esc_html__( 'Hide On Desktop', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'prefix_class' => 'cws-pattern-',
					'label_on' => esc_html__( 'Hide', 'cryptop' ),
					'label_off' => esc_html__( 'Show', 'cryptop' ),
					'return_value' => 'hidden-desktop',
				]
			);

			$this->add_control(
				'cws_hide_tablet',
				[
					'label' => esc_html__( 'Hide On Tablet', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'prefix_class' => 'cws-pattern-',
					'label_on' => esc_html__( 'Hide', 'cryptop' ),
					'label_off' => esc_html__( 'Show', 'cryptop' ),
					'return_value' => 'hidden-tablet',
				]
			);

			$this->add_control(
				'cws_hide_mobile',
				[
					'label' => esc_html__( 'Hide On Mobile', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'prefix_class' => 'cws-pattern-',
					'label_on' => esc_html__( 'Hide', 'cryptop' ),
					'label_off' => esc_html__( 'Show', 'cryptop' ),
					'return_value' => 'hidden-phone',
				]
			);

		$this->end_controls_section();
		// ---Patterns (CWS)
		//==========================//CWS EXTRA FIELDS==========================

		// Section border
		$this->start_controls_section(
			'section_border',
			[
				'label' => __( 'Border', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'tabs_border' );

		$this->start_controls_tab(
			'tab_border_normal',
			[
				'label' => __( 'Normal', 'cryptop' ),
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border',
			]
		);

		$this->add_control(
			'border_radius',
			[
				'label' => __( 'Border Radius', 'cryptop' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}, {{WRAPPER}} > .elementor-background-overlay' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow',
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'tab_border_hover',
			[
				'label' => __( 'Hover', 'cryptop' ),
			]
		);

		$this->add_group_control(
			Group_Control_Border::get_type(),
			[
				'name' => 'border_hover',
				'selector' => '{{WRAPPER}}:hover',
			]
		);

		$this->add_control(
			'border_radius_hover',
			[
				'label' => __( 'Border Radius', 'cryptop' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'selectors' => [
					'{{WRAPPER}}:hover, {{WRAPPER}}:hover > .elementor-background-overlay' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_group_control(
			Group_Control_Box_Shadow::get_type(),
			[
				'name' => 'box_shadow_hover',
				'selector' => '{{WRAPPER}}:hover',
			]
		);

		$this->add_control(
			'border_hover_transition',
			[
				'label' => __( 'Transition Duration', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'separator' => 'before',
				'default' => [
					'size' => 0.3,
				],
				'range' => [
					'px' => [
						'max' => 3,
						'step' => 0.1,
					],
				],
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'background_background',
							'operator' => '!==',
							'value' => '',
						], [
							'name' => 'border_border',
							'operator' => '!==',
							'value' => '',
						],
					],
				],
				'selectors' => [
					'{{WRAPPER}}' => 'transition: background {{background_hover_transition.SIZE}}s, border {{SIZE}}s, border-radius {{SIZE}}s, box-shadow {{SIZE}}s',
					'{{WRAPPER}} > .elementor-background-overlay' => 'transition: background {{background_overlay_hover_transition.SIZE}}s, border-radius {{SIZE}}s, opacity {{background_overlay_hover_transition.SIZE}}s',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();

		// Section Shape Divider
		$this->start_controls_section(
			'section_shape_divider',
			[
				'label' => __( 'Shape Divider', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$this->start_controls_tabs( 'tabs_shape_dividers' );

		$shapes_options = [
			'' => __( 'None', 'cryptop' ),
		];

		foreach ( Shapes::get_shapes() as $shape_name => $shape_props ) {
			$shapes_options[ $shape_name ] = $shape_props['title'];
		}

		foreach ( [
			'top' => __( 'Top', 'cryptop' ),
			'bottom' => __( 'Bottom', 'cryptop' ),
		] as $side => $side_label ) {
			$base_control_key = "shape_divider_$side";

			$this->start_controls_tab(
				"tab_$base_control_key",
				[
					'label' => $side_label,
				]
			);

			$this->add_control(
				$base_control_key,
				[
					'label' => __( 'Type', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'options' => $shapes_options,
					'render_type' => 'none',
					'frontend_available' => true,
				]
			);

			$this->add_control(
				$base_control_key . '_color',
				[
					'label' => __( 'Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'condition' => [
						"shape_divider_$side!" => '',
					],
					'selectors' => [
						"{{WRAPPER}} > .elementor-shape-$side .elementor-shape-fill" => 'fill: {{UNIT}};',
					],
				]
			);

			$this->add_responsive_control(
				$base_control_key . '_width',
				[
					'label' => __( 'Width', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'unit' => '%',
					],
					'tablet_default' => [
						'unit' => '%',
					],
					'mobile_default' => [
						'unit' => '%',
					],
					'range' => [
						'%' => [
							'min' => 100,
							'max' => 300,
						],
					],
					'condition' => [
						"shape_divider_$side" => array_keys( Shapes::filter_shapes( 'height_only', Shapes::FILTER_EXCLUDE ) ),
					],
					'selectors' => [
						"{{WRAPPER}} > .elementor-shape-$side svg" => 'width: calc({{SIZE}}{{UNIT}} + 1.3px)',
					],
				]
			);

			$this->add_responsive_control(
				$base_control_key . '_height',
				[
					'label' => __( 'Height', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'max' => 500,
						],
					],
					'condition' => [
						"shape_divider_$side!" => '',
					],
					'selectors' => [
						"{{WRAPPER}} > .elementor-shape-$side svg" => 'height: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$this->add_control(
				$base_control_key . '_flip',
				[
					'label' => __( 'Flip', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'condition' => [
						"shape_divider_$side" => array_keys( Shapes::filter_shapes( 'has_flip' ) ),
					],
					'selectors' => [
						"{{WRAPPER}} > .elementor-shape-$side svg" => 'transform: translateX(-50%) rotateY(180deg)',
					],
				]
			);

			$this->add_control(
				$base_control_key . '_negative',
				[
					'label' => __( 'Invert', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'frontend_available' => true,
					'condition' => [
						"shape_divider_$side" => array_keys( Shapes::filter_shapes( 'has_negative' ) ),
					],
					'render_type' => 'none',
				]
			);

			$this->add_control(
				$base_control_key . '_above_content',
				[
					'label' => __( 'Bring to Front', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'selectors' => [
						"{{WRAPPER}} > .elementor-shape-$side" => 'z-index: 2; pointer-events: none',
					],
					'condition' => [
						"shape_divider_$side!" => '',
					],
				]
			);

			$this->end_controls_tab();
		}

		$this->end_controls_tabs();

		$this->end_controls_section();

		// Section Typography
		$this->start_controls_section(
			'section_typo',
			[
				'label' => __( 'Typography', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		if ( in_array( Scheme_Color::get_type(), Schemes_Manager::get_enabled_schemes(), true ) ) {
			$this->add_control(
				'colors_warning',
				[
					'type' => Controls_Manager::RAW_HTML,
					'raw' => __( 'Note: The following colors won\'t work if Default Colors are enabled.', 'cryptop' ),
					'content_classes' => 'elementor-panel-alert elementor-panel-alert-warning',
				]
			);
		}

		$this->add_control(
			'heading_color',
			[
				'label' => __( 'Heading Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-heading-title' => 'color: {{VALUE}};',
				],
				'separator' => 'none',
			]
		);

		$this->add_control(
			'color_text',
			[
				'label' => __( 'Text Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}}' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'color_link',
			[
				'label' => __( 'Link Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} a' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'color_link_hover',
			[
				'label' => __( 'Link Hover Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} a:hover' => 'color: {{VALUE}};',
				],
			]
		);

		$this->add_control(
			'text_align',
			[
				'label' => __( 'Text Align', 'cryptop' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => __( 'Left', 'cryptop' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => __( 'Center', 'cryptop' ),
						'icon' => 'fa fa-align-center',
					],
					'right' => [
						'title' => __( 'Right', 'cryptop' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'selectors' => [
					'{{WRAPPER}} > .elementor-container' => 'text-align: {{VALUE}};',
				],
			]
		);

		$this->end_controls_section();

		// Section Advanced
		$this->start_controls_section(
			'section_advanced',
			[
				'label' => __( 'Advanced', 'cryptop' ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$this->add_responsive_control(
			'margin',
			[
				'label' => __( 'Margin', 'cryptop' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', '%' ],
				'allowed_dimensions' => 'vertical',
				'placeholder' => [
					'top' => '',
					'right' => 'auto',
					'bottom' => '',
					'left' => 'auto',
				],
				'selectors' => [
					'{{WRAPPER}}' => 'margin-top: {{TOP}}{{UNIT}}; margin-bottom: {{BOTTOM}}{{UNIT}};',
				],
			]
		);

		$this->add_responsive_control(
			'padding',
			[
				'label' => __( 'Padding', 'cryptop' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px', 'em', '%' ],
				'selectors' => [
					'{{WRAPPER}}' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
			]
		);

		$this->add_control(
			'z_index',
			[
				'label' => __( 'Z-Index', 'cryptop' ),
				'type' => Controls_Manager::NUMBER,
				'min' => 0,
				'selectors' => [
					'{{WRAPPER}}' => 'z-index: {{VALUE}};',
				],
				'label_block' => false,
			]
		);

		$this->add_control(
			'animation',
			[
				'label' => __( 'Entrance Animation', 'cryptop' ),
				'type' => Controls_Manager::ANIMATION,
				'default' => '',
				'prefix_class' => 'animated ',
				'label_block' => false,
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'animation_duration',
			[
				'label' => __( 'Animation Duration', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => '',
				'options' => [
					'slow' => __( 'Slow', 'cryptop' ),
					'' => __( 'Normal', 'cryptop' ),
					'fast' => __( 'Fast', 'cryptop' ),
				],
				'prefix_class' => 'animated-',
				'condition' => [
					'animation!' => '',
				],
			]
		);

		$this->add_control(
			'animation_delay',
			[
				'label' => __( 'Animation Delay', 'cryptop' ) . ' (ms)',
				'type' => Controls_Manager::NUMBER,
				'default' => '',
				'min' => 0,
				'step' => 100,
				'condition' => [
					'animation!' => '',
				],
				'render_type' => 'none',
				'frontend_available' => true,
			]
		);

		$this->add_control(
			'_element_id',
			[
				'label' => __( 'CSS ID', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'title' => __( 'Add your custom id WITHOUT the Pound key. e.g: my-id', 'cryptop' ),
				'label_block' => false,
			]
		);

		$this->add_control(
			'css_classes',
			[
				'label' => __( 'CSS Classes', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'prefix_class' => '',
				'title' => __( 'Add your custom class WITHOUT the dot. e.g: my-class', 'cryptop' ),
				'label_block' => false,
			]
		);

		$this->end_controls_section();

		// Section Responsive
		$this->start_controls_section(
			'_section_responsive',
			[
				'label' => __( 'Responsive', 'cryptop' ),
				'tab' => Controls_Manager::TAB_ADVANCED,
			]
		);

		$this->add_control(
			'reverse_order_mobile',
			[
				'label' => __( 'Reverse Columns', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'return_value' => 'reverse-mobile',
				'description' => __( 'Reverse column order - When on mobile, the column order is reversed, so the last column appears on top and vice versa.', 'cryptop' ),
			]
		);

		$this->add_control(
			'heading_visibility',
			[
				'label' => __( 'Visibility', 'cryptop' ),
				'type' => Controls_Manager::HEADING,
				'separator' => 'before',
			]
		);

		$this->add_control(
			'responsive_description',
			[
				'raw' => __( 'Attention: The display settings (show/hide for mobile, tablet or desktop) will only take effect once you are on the preview or live page, and not while you\'re in editing mode in Elementor.', 'cryptop' ),
				'type' => Controls_Manager::RAW_HTML,
				'content_classes' => 'elementor-descriptor',
			]
		);

		$this->add_control(
			'hide_desktop',
			[
				'label' => __( 'Hide On Desktop', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Hide', 'cryptop' ),
				'label_off' => __( 'Show', 'cryptop' ),
				'return_value' => 'hidden-desktop',
			]
		);

		$this->add_control(
			'hide_tablet',
			[
				'label' => __( 'Hide On Tablet', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Hide', 'cryptop' ),
				'label_off' => __( 'Show', 'cryptop' ),
				'return_value' => 'hidden-tablet',
			]
		);

		$this->add_control(
			'hide_mobile',
			[
				'label' => __( 'Hide On Mobile', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'prefix_class' => 'elementor-',
				'label_on' => __( 'Hide', 'cryptop' ),
				'label_off' => __( 'Show', 'cryptop' ),
				'return_value' => 'hidden-phone',
			]
		);

		$this->end_controls_section();

		Plugin::$instance->controls_manager->add_custom_css_controls( $this );
	}

	protected function _content_template() {
		?>
		<# if ( settings.background_video_link ) { #>
			<div class="elementor-background-video-container elementor-hidden-phone">
				<div class="elementor-background-video-embed"></div>
				<video class="elementor-background-video-hosted" autoplay loop muted></video>
			</div>
		<# } #>

		<!-- ==========================CWS EXTRA FIELDS========================== -->
		<#

		//Carousel
		var use_carousel = typeof settings.cws_carousel_section !== 'undefined' && settings.cws_carousel_section == 'section-carousel';
		if (use_carousel){
			
			var carousel_args = {
				slidesToShow: parseInt(settings.cws_slides_columns),
				slidesToScroll: 1,
				rows: parseInt(settings.cws_slides_rows),
				fade: (settings.cws_slide_mode == 'fade'),
				arrows: (settings.cws_navigation == 'both' || settings.cws_navigation == 'arrows'),
				dots: (settings.cws_navigation == 'both' || settings.cws_navigation == 'dots'),
				centerMode: (settings.cws_item_center == 'yes'),
				centerPadding: (settings.cws_item_side_paddings.size + settings.cws_item_side_paddings.unit),
				autoplay: (settings.cws_autoplay == 'yes'),
				autoplaySpeed: settings.cws_autoplay_speed,
				pauseOnHover: (settings.cws_pause_on_hover == 'yes'),
				swipe: (settings.cws_swipe == 'yes'),
				infinite: (settings.cws_infinite == 'yes'),
				cssEase: settings.cws_animation,
				speed: settings.cws_animation_speed,
			}

			var carousel_str = JSON.stringify(carousel_args);
		}


		//Accordion
		var use_accordion = typeof settings.cws_accordion_section !== 'undefined' && settings.cws_accordion_section == 'section-accordion';
		if (use_accordion){
			
			var accordion_args = {
				titles: settings.cws_accordion_items,
				mode: settings.cws_accordion_mode,
				activeRow: parseInt(settings.cws_accordion_active),
				collapsible: (settings.cws_accordion_collapsible == 'yes'),
				autoHeight: (settings.cws_accordion_auto_height == 'yes'),
				openHover: (settings.cws_accordion_hover == 'yes'),
			}

			var accordion_str = JSON.stringify(accordion_args);
		}

		//Tabs
		var use_tabs = typeof settings.cws_tabs_section !== 'undefined' && settings.cws_tabs_section == 'section-tabs';
		if (use_tabs){
			
			var tabs_args = {
				titles: settings.cws_tabs_titles,
				mode: settings.cws_tabs_mode,
				animation: settings.cws_tabs_animation,
				activeTab: parseInt(settings.cws_tabs_active),
				autoHeight: (settings.cws_tabs_auto_height == 'yes'),
			}

			var tabs_str = JSON.stringify(tabs_args);
		}

		//Patterns
		var cws_patterns;
		if (settings.cws_patterns_style == 'inside'){
			cws_patterns = settings.cws_patterns_inside;
		} else if (settings.cws_patterns_style == 'outside'){
			cws_patterns = settings.cws_patterns_outside;
		}

		if(cws_patterns.length > 0){
			var cws_classes = '';
			cws_classes += settings.cws_hide_desktop == 'yes' ? ' cws_hide_desktop' : '';
			cws_classes += settings.cws_hide_tablet == 'yes' ? ' cws_hide_tablet' : '';
			cws_classes += settings.cws_hide_mobile == 'yes' ? ' cws_hide_mobile' : '';
		#>
			<div class="elementor-background-patterns-cws{{{cws_classes}}}">
			<# 	_.each( cws_patterns, function( item, index ) {
				var item_id = 'elementor-repeater-item-'+item['_id'];
				
					if (item.cws_parallaxify){						
					#>
						<div class="cws_parallax_section"<# if (item.cws_parallaxify && item.cws_invert_parallax){ #> data-invert-x="{{{item.cws_x_invert}}}" data-invert-y="{{{item.cws_y_invert}}}" <# } #> data-scalar-x="{{{item.cws_x_scalar}}}" data-limit-x="{{{item.cws_x_limit}}}" data-scalar-y="{{{item.cws_y_scalar}}}" data-limit-y="{{{item.cws_y_limit}}}">
							<div class="layer" data-depth="1.00">
					<# } #>
								<div class="{{{item_id}}} cws-background-pattern">								
									<# if (settings.cws_patterns_style == 'outside' && item.image.url){ #>
										<img src="{{{item.image.url}}}">
									<# } #>
								</div>
						
					<# if (item.cws_parallaxify){ #>
							</div>	
						</div>
					<# } #>	

			<# }); #>
			</div>	
		<# } #>
		<div class="elementor-background-overlay"></div>
		<div class="elementor-background-overlay-cws"></div>
		<!-- ==========================//CWS EXTRA FIELDS========================== -->
		
		<div class="elementor-shape elementor-shape-top"></div>
		<div class="elementor-shape elementor-shape-bottom"></div>

		<!-- ==========================CWS EXTRA FIELDS========================== -->
		<div <# if (use_carousel){ #>data-carousel_args='{{{carousel_str}}}'<# } #> class="elementor-container <# if (use_carousel){ #>elementor-slick-slider <# } #>elementor-column-gap-{{ settings.gap }}">
			<div <# if (use_accordion){ #>data-accordion_args='{{{accordion_str}}}'<# } #> <# if (use_tabs){ #>data-tabs_args='{{{tabs_str}}}'<# } #> class="<# if (use_accordion){ #>elementor-cws-accordion <# } #><# if (use_tabs){ #>elementor-cws-tabs tabs-{{settings.cws_tabs_mode}} <# } #>elementor-row"></div></div>
		</div>
		<!-- ==========================//CWS EXTRA FIELDS========================== -->
		<?php
	}

	public function before_render() {
		$settings = $this->get_settings_for_display();

		?>
		<<?php echo esc_html( $this->get_html_tag() ); ?> <?php $this->print_render_attribute_string( '_wrapper' ); ?>>
			<?php
			if ( 'video' === $settings['background_background'] ) :
				if ( $settings['background_video_link'] ) :
					$video_properties = Embed::get_video_properties( $settings['background_video_link'] );
					?>
					<div class="elementor-background-video-container elementor-hidden-phone">
						<?php if ( $video_properties ) : ?>
							<div class="elementor-background-video-embed"></div>
						<?php else : ?>
							<video class="elementor-background-video-hosted elementor-html5-video" autoplay loop muted></video>
						<?php endif; ?>
					</div>
				<?php
				endif;
			endif;

			$has_background_overlay = in_array( $settings['background_overlay_background'], [ 'classic', 'gradient' ], true ) ||
									  in_array( $settings['background_overlay_hover_background'], [ 'classic', 'gradient' ], true );

			//==========================CWS EXTRA FIELDS==========================
			//CWS Patterns
			$cws_patterns = array();

			if ($settings['cws_patterns_style'] == 'inside'){
				$cws_patterns = $settings['cws_patterns_inside'];
			} elseif ($settings['cws_patterns_style'] == 'outside'){
				$cws_patterns = $settings['cws_patterns_outside'];
			}
											  
			if (!empty($cws_patterns)){

				$cws_classes = '';
				$cws_classes .= $settings['cws_hide_desktop'] ? ' cws_hide_desktop' : '';
				$cws_classes .= $settings['cws_hide_tablet'] ? ' cws_hide_tablet' : '';
				$cws_classes .= $settings['cws_hide_mobile'] ? ' cws_hide_mobile' : '';

				echo "<div class='elementor-background-patterns-cws".esc_attr($cws_classes)."'>";
					foreach ($cws_patterns as $key => $value) {
						$item_id = 'elementor-repeater-item-'.$value['_id'];

						if ($value['cws_parallaxify']){
							echo "<div class='cws_parallax_section' ".($value['cws_parallaxify'] && $value['cws_invert_parallax'] ? "data-invert-x='".esc_attr($value['cws_x_invert'])."' data-invert-y='".esc_attr($value['cws_y_invert'])."'" : '' )." data-scalar-x='".esc_attr($value['cws_x_scalar'])."' data-limit-x='".esc_attr($value['cws_x_limit'])."' data-scalar-y='".esc_attr($value['cws_y_scalar'])."' data-limit-y='".esc_attr($value['cws_y_limit'])."'>";
								echo "<div class='layer' data-depth='1.00'>";
						}
								echo "<div class='".esc_attr($item_id)." cws-background-pattern'>";
									if ($settings['cws_patterns_style'] == 'outside' && !empty($value['image']['url'])){
										echo "<img src='".esc_url($value['image']['url'])."'>";
									}
								echo "</div>";

						if ($value['cws_parallaxify']){
								echo "</div>";
							echo "</div>";
						}
					}
				echo "</div>";
			}
			//--CWS Patterns
			//==========================//CWS EXTRA FIELDS==========================

			if ( $has_background_overlay ) :
			?>
				<div class="elementor-background-overlay"></div>
			<?php
			endif;

			if ( $settings['shape_divider_top'] ) {
				$this->print_shape_divider( 'top' );
			}

			if ( $settings['shape_divider_bottom'] ) {
				$this->print_shape_divider( 'bottom' );
			}
			?>
			<!-- ==========================CWS EXTRA FIELDS========================== -->
			<div class="elementor-container <?php echo ($settings['cws_carousel_section'] == 'section-carousel' ? 'elementor-slick-slider ' : '');?>elementor-column-gap-<?php echo esc_attr( $settings['gap'] ); ?>">
				<div class="elementor-row">
			<!-- ==========================//CWS EXTRA FIELDS========================== -->
		<?php
	}

	private function get_html_tag() {
		$html_tag = $this->get_settings( 'html_tag' );

		if ( empty( $html_tag ) ) {
			$html_tag = 'section';
		}

		return $html_tag;
	}	

}

Plugin::instance()->elements_manager->register_element_type( new CWS_Elementor_Section() );