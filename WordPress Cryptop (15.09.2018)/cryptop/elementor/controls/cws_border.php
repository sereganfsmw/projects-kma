<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CWS_Group_Control_Border extends Group_Control_Base {

	protected static $fields;

	public static function get_type() {
		return 'cws_border';
	}

	protected function init_fields() {
		$border_field = $this->get_args();

		$fields = [];

		$fields['border'] = [
			'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'' => esc_html__( 'None', 'cryptop' ),
				'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
				'double' => _x( 'Double', 'Border Control', 'cryptop' ),
				'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
				'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
			],
			'selectors' => [
				'{{SELECTOR}}' => 'border-style: {{VALUE}};',
			],
		];

		$fields['width'] = [
			'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
			'type' => Controls_Manager::DIMENSIONS,
			'selectors' => [
				'{{SELECTOR}}' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
			],
			'condition' => [
				'border!' => '',
			],
		];

		$fields['color'] = [
			'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
			'type' => Controls_Manager::COLOR,
			'selectors' => [
				'{{SELECTOR}}' => 'border-color: {{VALUE}};',
			],
			'condition' => [
				'border!' => '',
			],
		];

		//Extra field names (CWS)
		if (isset($border_field['fields_names'])){
			foreach ($border_field['fields_names'] as $key => $value) {
				$fields[$key]['label'] = $value;
			}
		}
		//--Extra field names (CWS)
		return $fields;
	}

	protected function prepare_fields( $fields ) {
		array_walk(
			$fields, function( &$field, $field_name ) {

				$border_field = $this->get_args();
				$defaults_arr = $border_field['defaults'];

				//Set defaults velues to all fields (CWS)
				if (isset($border_field['defaults'])){
					if (isset($defaults_arr[$field_name])){
						$field['default'] = $defaults_arr[$field_name];
					}
				}				
				//--Set defaults velues to all fields (CWS)		
			}
		);

		return parent::prepare_fields( $fields );
	}

	protected function get_default_options() {
		$border_field = $this->get_args();

		//Popover settings (CWS)
		$popover = [
			'popover' => false
		];
		//--Popover settings (CWS)

		return $popover;
	}
}