<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CWS_Group_Control_Typography extends Group_Control_Base {

	protected static $fields;

	private static $_scheme_fields_keys = [ 'font_family', 'font_weight' ];

	public static function get_scheme_fields_keys() {
		return self::$_scheme_fields_keys;
	}

	public static function get_type() {
		return 'cws_typography';
	}

	protected function init_fields() {
		$fields = [];

		$fields['font_size'] = [
			'label' => _x( 'Size', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ 'px', 'em', 'rem' ],
			'range' => [
				'px' => [
					'min' => 1,
					'max' => 200,
				],
			],
			'responsive' => true,
			'selector_value' => 'font-size: {{SIZE}}{{UNIT}}',
		];

		$fields['text_transform'] = [
			'label' => _x( 'Transform', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'' => esc_html__( 'Default', 'cryptop' ),
				'uppercase' => _x( 'Uppercase', 'Typography Control', 'cryptop' ),
				'lowercase' => _x( 'Lowercase', 'Typography Control', 'cryptop' ),
				'capitalize' => _x( 'Capitalize', 'Typography Control', 'cryptop' ),
				'none' => _x( 'Normal', 'Typography Control', 'cryptop' ),
			],
		];

		$fields['font_style'] = [
			'label' => _x( 'Style', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'' => esc_html__( 'Default', 'cryptop' ),
				'normal' => _x( 'Normal', 'Typography Control', 'cryptop' ),
				'italic' => _x( 'Italic', 'Typography Control', 'cryptop' ),
				'oblique' => _x( 'Oblique', 'Typography Control', 'cryptop' ),
			],
		];

		$fields['font_weight'] = [
			'label' => _x( 'Weight', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'100' => _x( 'Ultra light', 'Typography Control', 'cryptop' ),
				'300' => _x( 'Light', 'Typography Control', 'cryptop' ),
				'400' => _x( 'Normal', 'Typography Control', 'cryptop' ),
				'500' => _x( 'Bold (500)', 'Typography Control', 'cryptop' ),
				'600' => _x( 'Bold (600)', 'Typography Control', 'cryptop' ),
				'700' => _x( 'Bold (700)', 'Typography Control', 'cryptop' ),
			],
		];		

		$fields['text_decoration'] = [
			'label' => _x( 'Decoration', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'' => esc_html__( 'Default', 'cryptop' ),
				'underline' => _x( 'Underline', 'Typography Control', 'cryptop' ),
				'overline' => _x( 'Overline', 'Typography Control', 'cryptop' ),
				'line-through' => _x( 'Line Through', 'Typography Control', 'cryptop' ),
				'none' => _x( 'None', 'Typography Control', 'cryptop' ),
			],
		];

		$fields['html_tag'] = [
			'label' => _x( 'HTML Tag', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'h1' => _x( 'H1', 'Typography Control', 'cryptop' ),
				'h2' => _x( 'H2', 'Typography Control', 'cryptop' ),
				'h3' => _x( 'H3', 'Typography Control', 'cryptop' ),
				'h4' => _x( 'H4', 'Typography Control', 'cryptop' ),
				'h5' => _x( 'H5', 'Typography Control', 'cryptop' ),
				'h6' => _x( 'H6', 'Typography Control', 'cryptop' ),
				'div' => _x( 'div', 'Typography Control', 'cryptop' ),
				'p' => _x( 'p', 'Typography Control', 'cryptop' ),
				'span' => _x( 'span', 'Typography Control', 'cryptop' ),
			],
		];

		$fields['line_height'] = [
			'label' => _x( 'Line-Height', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'range' => [
				'px' => [
					'min' => 1,
				],
			],
			'responsive' => true,
			'size_units' => [ 'px', 'em' ],
			'selector_value' => 'line-height: {{SIZE}}{{UNIT}}',
		];

		$fields['letter_spacing'] = [
			'label' => _x( 'Letter Spacing', 'Typography Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'range' => [
				'px' => [
					'min' => -5,
					'max' => 10,
					'step' => 0.1,
				],
			],
			'responsive' => true,
			'selector_value' => 'letter-spacing: {{SIZE}}{{UNIT}}',
		];

		return $fields;
	}

	protected function prepare_fields( $fields ) {
		array_walk(
			$fields, function( &$field, $field_name ) {
				if ( in_array( $field_name, [ 'typography', 'popover_toggle' ] ) ) {
					return;
				}

				$selector_value = ! empty( $field['selector_value'] ) ? $field['selector_value'] : str_replace( '_', '-', $field_name ) . ': {{VALUE}};';

				$typography_field = $this->get_args();
				$defaults_arr = $typography_field['defaults'];

				if(isset($typography_field['mobile_defaults'])){
					$mobile_defaults_arr = $typography_field['mobile_defaults'];
				}
				if(isset($typography_field['tablet_defaults'])){
					$tablet_defaults_arr = $typography_field['tablet_defaults'];
				}

				//Set defaults velues to all fields (CWS)
				if (isset($typography_field['defaults'])){
					if (isset($defaults_arr[$field_name])){
						$field['default'] = $defaults_arr[$field_name];
					}
				}

				if (isset($typography_field['mobile_defaults'])){
					if (isset($mobile_defaults_arr[$field_name])){
						$field['mobile_default'] = $mobile_defaults_arr[$field_name];
					}
				}		

				if (isset($typography_field['tablet_defaults'])){
					if (isset($tablet_defaults_arr[$field_name])){
						$field['tablet_default'] = $tablet_defaults_arr[$field_name];
					}
				}				
				//--Set defaults velues to all fields (CWS)		

				if($field_name != 'html_tag'){ //Not render styles if
					$field['selectors'] = [
						'{{SELECTOR}}' => $selector_value,
					];
				}

				
			}
		);

		return parent::prepare_fields( $fields );
	}

	protected function add_group_args_to_field( $control_id, $field_args ) {
		$field_args = parent::add_group_args_to_field( $control_id, $field_args );

		$args = $this->get_args();

		if ( in_array( $control_id, self::get_scheme_fields_keys() ) && ! empty( $args['scheme'] ) ) {
			$field_args['scheme'] = [
				'type' => self::get_type(),
				'value' => $args['scheme'],
				'key' => $control_id,
			];
		}

		return $field_args;
	}

	protected function get_default_options() {
		return [
			'popover' => [
				'starter_name' => 'typography',
				'starter_title' => _x( 'Typography', 'Typography Control', 'cryptop' ),
			],
		];
	}
}