"use strict";

console.log('456: cws_svg.js');

jQuery( window ).on( 'elementor:init', function() {
	//Init Custom CWS elements
	var ControlCwsSvg = elementor.modules.controls.BaseData.extend({
		onReady: function() {

			var self = this;

			// debugger;
			var button_add = this.$el.find('.cws_svg_icon');


		/*	jQuery(document).on('click', button_add, function(event) {
				event.preventDefault();
				var parent = event.target.parentNode;
				debugger;
				cws_fun('el', parent);
			});*/

			// var cws_fun = window.cws_svg_click;

			// debugger;

			var options = _.extend({
				param: 'value',
			}, this.model.get( 'cws_svg_options' ) );

			//Initialize via plugin
			// this.ui.textarea.emojioneArea( options );
		},

		saveValue: function() {
			if (this.ui.textarea.length){
				this.setValue( this.ui.textarea[0].value );
			}
		},

		onBeforeDestroy: function() { //Lose focus from element
			this.saveValue();
		}
	} );
	elementor.addControlView( 'CWS_SVG', ControlCwsSvg );

} );