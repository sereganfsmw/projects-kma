<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class CWS_Group_Control_Background extends Group_Control_Background {

	protected static $fields;

	public static function get_type() {
		return 'cws_background';
	}

	public function init_fields() {
		$fields = [];

		$fields['background'] = [
			'label' => _x( 'Background Type', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::CHOOSE,
			'label_block' => false,
			'render_type' => 'ui',
		];

		$fields['color'] = [
			'label' => _x( 'Background Color', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::COLOR,
			'default' => '',
			'title' => _x( 'Background Color', 'Background Control', 'cryptop' ),
			'selectors' => [
				'{{SELECTOR}}' => 'background-color: {{VALUE}};',
			],
			'condition' => [
				'background' => [ 'classic', 'gradient' ],
			],
		];

		$fields['color_stop'] = [
			'label' => _x( 'Location', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ '%' ],
			'default' => [
				'unit' => '%',
				'size' => 0,
			],
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['color_b'] = [
			'label' => _x( 'Second Color', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::COLOR,
			'default' => '#f2295b',
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['color_b_stop'] = [
			'label' => _x( 'Location', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ '%' ],
			'default' => [
				'unit' => '%',
				'size' => 100,
			],
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['gradient_type'] = [
			'label' => _x( 'Type', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'linear' => _x( 'Linear', 'Background Control', 'cryptop' ),
				'radial' => _x( 'Radial', 'Background Control', 'cryptop' ),
			],
			'default' => 'linear',
			'render_type' => 'ui',
			'condition' => [
				'background' => [ 'gradient' ],
			],
			'of_type' => 'gradient',
		];

		$fields['gradient_angle'] = [
			'label' => _x( 'Angle', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SLIDER,
			'size_units' => [ 'deg' ],
			'default' => [
				'unit' => 'deg',
				'size' => 180,
			],
			'range' => [
				'deg' => [
					'step' => 10,
				],
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background-color: transparent; background-image: linear-gradient({{SIZE}}{{UNIT}}, {{color.VALUE}} {{color_stop.SIZE}}{{color_stop.UNIT}}, {{color_b.VALUE}} {{color_b_stop.SIZE}}{{color_b_stop.UNIT}})',
			],
			'condition' => [
				'background' => [ 'gradient' ],
				'gradient_type' => 'linear',
			],
			'of_type' => 'gradient',
		];

		$fields['gradient_position'] = [
			'label' => _x( 'Position', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'options' => [
				'center center' => _x( 'Center Center', 'Background Control', 'cryptop' ),
				'center left' => _x( 'Center Left', 'Background Control', 'cryptop' ),
				'center right' => _x( 'Center Right', 'Background Control', 'cryptop' ),
				'top center' => _x( 'Top Center', 'Background Control', 'cryptop' ),
				'top left' => _x( 'Top Left', 'Background Control', 'cryptop' ),
				'top right' => _x( 'Top Right', 'Background Control', 'cryptop' ),
				'bottom center' => _x( 'Bottom Center', 'Background Control', 'cryptop' ),
				'bottom left' => _x( 'Bottom Left', 'Background Control', 'cryptop' ),
				'bottom right' => _x( 'Bottom Right', 'Background Control', 'cryptop' ),
			],
			'default' => 'center center',
			'selectors' => [
				'{{SELECTOR}}' => 'background-color: transparent; background-image: radial-gradient(at {{VALUE}}, {{color.VALUE}} {{color_stop.SIZE}}{{color_stop.UNIT}}, {{color_b.VALUE}} {{color_b_stop.SIZE}}{{color_b_stop.UNIT}})',
			],
			'condition' => [
				'background' => [ 'gradient' ],
				'gradient_type' => 'radial',
			],
			'of_type' => 'gradient',
		];

		$fields['image'] = [
			'label' => _x( 'Image', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::MEDIA,
			'dynamic' => [
				'active' => true,
			],
			'title' => _x( 'Background Image', 'Background Control', 'cryptop' ),
			'selectors' => [
				'{{SELECTOR}}' => 'background-image: url("{{URL}}");',
			],
			'condition' => [
				'background' => [ 'classic' ],
			],
		];

		$fields['position'] = [
			'label' => _x( 'Position', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'default' => '',
			'options' => [
				'' => _x( 'Default', 'Background Control', 'cryptop' ),
				'top left' => _x( 'Top Left', 'Background Control', 'cryptop' ),
				'top center' => _x( 'Top Center', 'Background Control', 'cryptop' ),
				'top right' => _x( 'Top Right', 'Background Control', 'cryptop' ),
				'center left' => _x( 'Center Left', 'Background Control', 'cryptop' ),
				'center center' => _x( 'Center Center', 'Background Control', 'cryptop' ),
				'center right' => _x( 'Center Right', 'Background Control', 'cryptop' ),
				'bottom left' => _x( 'Bottom Left', 'Background Control', 'cryptop' ),
				'bottom center' => _x( 'Bottom Center', 'Background Control', 'cryptop' ),
				'bottom right' => _x( 'Bottom Right', 'Background Control', 'cryptop' ),
				//==========================CWS EXTRA FIELDS==========================
				'cws_custom' => _x( 'Custom', 'Background Control', 'cryptop' ),
				//==========================//CWS EXTRA FIELDS==========================
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background-position: {{VALUE}};',
			],
			'condition' => [
				'background' => [ 'classic' ],
				'image[url]!' => '',
			],
		];

		//==========================CWS EXTRA FIELDS==========================
		$fields['cws_custom_position'] = [
			'label' => esc_html__( 'Custom position (CWS)', 'cryptop' ),
			'type' => Controls_Manager::TEXT,
			'label_block' => true,
			'default' => esc_html__( '50% auto', 'cryptop' ),
			'description' => esc_html__( 'Set left and top position', 'cryptop' ),
			'placeholder' => esc_html__( '50% 50%', 'cryptop' ),
			'responsive' => true,
			'selectors' => [
				'{{SELECTOR}}' => 'background-position: {{VALUE}};',
			],				
			'condition' => [
				'position' => [ 'cws_custom' ],
			],				
		];
		//==========================//CWS EXTRA FIELDS==========================

		$fields['attachment'] = [
			'label' => _x( 'Attachment', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'default' => '',
			'options' => [
				'' => _x( 'Default', 'Background Control', 'cryptop' ),
				'scroll' => _x( 'Scroll', 'Background Control', 'cryptop' ),
				'fixed' => _x( 'Fixed', 'Background Control', 'cryptop' ),
			],
			'selectors' => [
				'(desktop+){{SELECTOR}}' => 'background-attachment: {{VALUE}};',
			],
			'condition' => [
				'background' => [ 'classic' ],
				'image[url]!' => '',
			],
		];

		$fields['attachment_alert'] = [
			'type' => Controls_Manager::RAW_HTML,
			'content_classes' => 'elementor-control-field-description',
			'raw' => esc_html__( 'Note: Attachment Fixed works only on desktop.', 'cryptop' ),
			'separator' => 'none',
			'condition' => [
				'background' => [ 'classic' ],
				'image[url]!' => '',
				'attachment' => 'fixed',
			],
		];

		$fields['repeat'] = [
			'label' => _x( 'Repeat', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'default' => '',
			'options' => [
				'' => _x( 'Default', 'Background Control', 'cryptop' ),
				'no-repeat' => _x( 'No-repeat', 'Background Control', 'cryptop' ),
				'repeat' => _x( 'Repeat', 'Background Control', 'cryptop' ),
				'repeat-x' => _x( 'Repeat-x', 'Background Control', 'cryptop' ),
				'repeat-y' => _x( 'Repeat-y', 'Background Control', 'cryptop' ),
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background-repeat: {{VALUE}};',
			],
			'condition' => [
				'background' => [ 'classic' ],
				'image[url]!' => '',
			],
		];

		$fields['size'] = [
			'label' => _x( 'Size', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::SELECT,
			'default' => 'cover',
			'options' => [
				'auto' => _x( 'Auto', 'Background Control', 'cryptop' ),
				'cover' => _x( 'Cover', 'Background Control', 'cryptop' ),
				'contain' => _x( 'Contain', 'Background Control', 'cryptop' ),
				//==========================CWS EXTRA FIELDS==========================
				'cws_custom' => _x( 'Custom', 'Background Control', 'cryptop' ),
				//==========================//CWS EXTRA FIELDS==========================
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background-size: {{VALUE}};',
			],
			'condition' => [
				'background' => [ 'classic' ],
				'image[url]!' => '',
			],
		];

		//==========================CWS EXTRA FIELDS==========================
		$fields['cws_custom_size'] = [
			'label' => esc_html__( 'Custom size (CWS)', 'cryptop' ),
			'type' => Controls_Manager::TEXT,
			'label_block' => true,
			'default' => esc_html__( '50% auto', 'cryptop' ),
			'description' => esc_html__( 'Set left and top size', 'cryptop' ),
			'placeholder' => esc_html__( '50% 50%', 'cryptop' ),
			'responsive' => true,
			'selectors' => [
				'{{SELECTOR}}' => 'background-size: {{VALUE}};',
			],				
			'condition' => [
				'size' => [ 'cws_custom' ],
			],				
		];
		//==========================//CWS EXTRA FIELDS==========================

		$fields['video_link'] = [
			'label' => _x( 'Video Link', 'Background Control', 'cryptop' ),
			'type' => Controls_Manager::TEXT,
			'placeholder' => 'https://www.youtube.com/watch?v=9uOETcuFjbE',
			'description' => esc_html__( 'YouTube link or video file (mp4 is recommended).', 'cryptop' ),
			'label_block' => true,
			'default' => '',
			'condition' => [
				'background' => [ 'video' ],
			],
			'of_type' => 'video',
		];

		$fields['video_start'] = [
			'label' => esc_html__( 'Start Time', 'cryptop' ),
			'type' => Controls_Manager::NUMBER,
			'description' => esc_html__( 'Specify a start time (in seconds)', 'cryptop' ),
			'placeholder' => 10,
			'condition' => [
				'background' => [ 'video' ],
			],
			'of_type' => 'video',
		];

		$fields['video_end'] = [
			'label' => esc_html__( 'End Time', 'cryptop' ),
			'type' => Controls_Manager::NUMBER,
			'description' => esc_html__( 'Specify an end time (in seconds)', 'cryptop' ),
			'placeholder' => 70,
			'condition' => [
				'background' => [ 'video' ],
			],
			'of_type' => 'video',
		];

		$fields['video_fallback'] = [
			'label' => _x( 'Background Fallback', 'Background Control', 'cryptop' ),
			'description' => esc_html__( 'This cover image will replace the background video on mobile and tablet devices.', 'cryptop' ),
			'type' => Controls_Manager::MEDIA,
			'label_block' => true,
			'condition' => [
				'background' => [ 'video' ],
			],
			'selectors' => [
				'{{SELECTOR}}' => 'background: url("{{URL}}") 50% 50%; background-size: cover;',
			],
			'of_type' => 'video',
		];

		return $fields;
	}
}