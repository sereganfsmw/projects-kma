<?php

if ( ! defined( 'ABSPATH' ) ) exit;

//Check if plugin active
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

class CWS_Elementor_Config{
	private static $instance = null;
	public static function get_instance() {
		if ( ! self::$instance )
			self::$instance = new self;
		return self::$instance;
	}
	
	public function cws_elementor_frontend_after_register_styles() {}

	public function cws_elementor_frontend_after_enqueue_styles() {
		//CSS load (in iframe)
		wp_enqueue_style('cws_elementor_css', get_template_directory_uri() . '/elementor/extends/css/cws_elementor_preview.css', array('editor-preview'));
	}

	public function cws_elementor_editor_before_enqueue_scripts() {
		//CSS load
		wp_enqueue_style( 'cws_elementor_flaticon', get_template_directory_uri() . '/fonts/flaticon/flaticon.css' );
		wp_enqueue_style('cws_elementor_css', get_template_directory_uri() . '/elementor/extends/css/cws_elementor_editor_panel.css');
	}

	public function cws_elementor_editor_after_enqueue_scripts() {}

	public function cws_elementor_fully_loaded() {
		//JS load
		wp_enqueue_script('cws_elementor_editor_panel_js', get_template_directory_uri() . '/elementor/extends/js/cws_elementor_editor_panel.js', array('jquery', 'cws_scripts'/*, 'elementor-editor'*/), '1.0', false); //Check if Elementor Editor JS is loaded
	}	
	
	public function cws_elementor_preview_before_enqueue_scripts() {}
	
	public function cws_elementor_frontend_after_register_scripts() {
		//Deregister old version Swiper slider (v3.4.2)
		// wp_deregister_script('jquery-swiper');

		//Register new version Swiper slider (v4.3.0)
/*		wp_register_script(
			'jquery-swiper',
			get_template_directory_uri() .'/js/swiper.min.js',
			[
				'jquery',
			],
			'4.3.0',
			true
		);*/
	}

	public function cws_elementor_frontend_after_enqueue_scripts() {
		//JS load
		wp_enqueue_script("jquery");
		wp_enqueue_script('cws_elementor_main_js', get_template_directory_uri() . '/elementor/extends/js/cws_elementor_scripts.js', array('jquery','elementor-frontend'), '1.0', false); //Check if Elementor Frontend JS is loaded
	}		

	public function cws_elementor_print_template($template, $widget) {


		return $template;
	}
	

	//Check if script & style enqueued
	// if ( wp_script_is( 'name', 'enqueued' ) && wp_style_is( 'name', 'enqueued' ) ) { wp_dequeue_script( 'name' ); }

	//Group element under Theme slug
	public function cws_elementor_section() {
		Elementor\Plugin::instance()->elements_manager->add_category(
			'cws-elements',
			[
				'title'  => __( '<strong>CWS Themes</strong> Elements', 'cryptop' ),
				'icon' => 'font'
			],
			0
		);
	}
	
	public function init(){
		//==============Actions==============
		add_action('elementor/init', array($this, 'cws_elementor_fully_loaded') );

		//Controls
		$this->cws_include_controls();
		add_action('elementor/controls/controls_registered', array($this, 'cws_register_controls') );

		//Elements
		add_action('elementor/elements/elements_registered', array($this, 'cws_register_elements') );
		add_action('elementor/elements/categories_registered', array($this, 'cws_elementor_section') );
		
		//Editor
		add_action('elementor/editor/before_enqueue_scripts', array($this, 'cws_elementor_editor_before_enqueue_scripts') ); // EDITOR // Before the editor scripts enqueuing.
		add_action('elementor/editor/after_enqueue_scripts', array($this, 'cws_elementor_editor_after_enqueue_scripts') ); // EDITOR // After the editor scripts enqueuing.
		
		//Frontend
		add_action('elementor/frontend/after_register_styles', array($this, 'cws_elementor_frontend_after_register_styles') ); // FRONTEND // After Elementor registers all styles.
		add_action('elementor/frontend/after_enqueue_styles', array($this, 'cws_elementor_frontend_after_enqueue_styles') ); // FRONTEND // After Elementor enqueue all styles.
		add_action('elementor/frontend/after_register_scripts', array($this, 'cws_elementor_frontend_after_register_scripts') ); // FRONTEND // After Elementor registers all scripts.
		add_action('elementor/frontend/after_enqueue_scripts', array($this, 'cws_elementor_frontend_after_enqueue_scripts') ); // FRONTEND // After Elementor enqueue all scripts.

		add_action('elementor/element/print_template', array($this, 'cws_elementor_print_template'), 10, 2 ); // Filter Elementor Template
		// add_action('elementor/frontend/after_enqueue_scripts', array($this, 'cws_elementor_preview_before_enqueue_scripts') ); 				  // PREVIEW // Before the preview styles enqueuing.

		add_action( 'elementor/editor/after_save', array($this, 'cws_on_after_save'), 10, 2);

		//Add shortcodes extra sections
		require_once( trailingslashit( get_template_directory() ) . 'elementor/extends/cws_elementor_sections.php' );

		//Add Page Settings
		require_once( trailingslashit( get_template_directory() ) . 'elementor/extends/cws_elementor_settings.php' );
		//elementor/documents/register_controls
		//Hook then start register controls
		add_action('elementor/element/post/document_settings/after_section_end', 'Elementor\cws_page_settings_controls', 10, 2 );
		//--Add Page Settings

		//Register shortcodes
		if ( is_plugin_active('cws-essentials/cws-essentials.php') ){
			add_action( 'elementor/widgets/widgets_registered', array( $this, 'cws_unregister_standart_widgets' ), 11 );
			add_action( 'elementor/init', array( $this, 'cws_unregister_standart_elements' ), 11 );
			add_action( 'elementor/init', array( $this, 'cws_register_shortcodes' ), 12 );
		}
	}

	public function cws_on_after_save($post_id, $editor_data) {
		//global $cws_theme_funcs;
		//var_dump($cws_theme_funcs->cws_g_option());

		$settings = Elementor\Plugin::$instance->documents->get( $post_id )->get_settings();
		$settings1 = Elementor\Plugin::$instance->documents->get( $post_id )->get_active_settings();
		$shouldUpdate = false;
		foreach ($editor_data as $k => $v) {
			if ('yes' === $k[$v]) {
				$k[$v] = '1';
			}
		}
		$set = json_decode(stripslashes($_POST['actions']), true);
		$set = isset($set['save_builder']['data']['settings']) ? $set['save_builder']['data']['settings'] : null;

		if ($set) {
			$newset = array();
			foreach ($set as $k => $v) {
				preg_match('/^(theme_colors|header|logo_box|menu_box|mobile_menu|sticky_menu|title_box|top_bar_box|footer|boxed|side_panel)/m', $k, $matches);
				$v = ('yes' === $v) ? '1' : $v;
				if (!empty($matches)) {
					$key = $matches[0];
					$new_subkey = substr($k, strlen($key) + 1);
					$newset[$key][$new_subkey] = $v;
				} else {
					preg_match('/^(post_)/m', $k, $matches); // filter bad words, that shouldn't be here
					if (empty($matches)) {
						$newset[$k] = $v;
					}
				}
			}
	//		var_dump($newset);
			update_post_meta($post_id, 'cws_mb_post', $newset);
		}

	}

	public function cws_unregister_standart_widgets() {
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('progress');
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('button');
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('divider');
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('alert');
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('icon-list');
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('image');
		Elementor\Plugin::instance()->widgets_manager->unregister_widget_type('shortcode');
	}

	public function cws_unregister_standart_elements() {
		Elementor\Plugin::instance()->elements_manager->unregister_element_type('column');
		Elementor\Plugin::instance()->elements_manager->unregister_element_type('section');
	}	

	public function cws_register_shortcodes() {
		if ( defined( 'ELEMENTOR_PATH' ) && class_exists( 'Elementor\Widget_Base' ) ) {
			if ( class_exists( 'Elementor\Plugin' ) ) {
				if ( is_callable( 'Elementor\Plugin', 'instance' ) ) {
					$elementor = Elementor\Plugin::instance();
					if ( isset( $elementor->widgets_manager ) ) {
						if ( method_exists( $elementor->widgets_manager, 'register_widget_type' ) ) {

							//Files shortcodes list
							$shortcodes = array(
								'cws_icon',
								'cws_text',
								'cws_banners',
								'cws_milestone',
								'cws_pricing_plan',
								'cws_progress_bar',
								'cws_button',
								'cws_pie_chart',
								'cws_divider',
								'cws_msg_box',
								'cws_embed',
								'cws_categories',
								'cws_services',
								'cws_icon_list',
								'cws_tips',
								'cws_benefits',
								'cws_image',
								'cws_blog',
								'cws_portfolio',
								'cws_staff',
								'cws_shortcode',
								'cws_testimonial',
							);

							foreach ($shortcodes as $key => $value) {
								require_once( trailingslashit( get_template_directory() ) . 'elementor/shortcodes/'. $value . '.php' );
							}

						}
					}
				}
			}
		}
	}

	public function cws_include_controls() {

		//Files controls list
		//[Control name] => [enable expression]
		$controls = array(
			'cws_svg.php' => (is_plugin_active('cws-svgicons/cws-svgicons.php')),
			'cws_typography.php' => true,
			'cws_border.php' => true,
			'cws_gradient.php' => true,
			'cws_background.php' => true,
		);

		foreach ($controls as $key => $value) {
			if ($value){
				require_once( trailingslashit( get_template_directory() ) . 'elementor/controls/'. $key );
			}
		}
	}

	public function cws_register_controls() {
		if ( defined( 'ELEMENTOR_PATH' ) && class_exists( 'Elementor\Element_Base' ) ) {
			if ( class_exists( 'Elementor\Plugin' ) ) {
				if ( is_callable( 'Elementor\Plugin', 'instance' ) ) {
					$elementor = Elementor\Plugin::instance();
					if ( isset( $elementor->controls_manager ) ) {

						if ( method_exists( $elementor->controls_manager, 'add_group_control' ) ) {
							$elementor->controls_manager->add_group_control( 'cws_background', new Elementor\CWS_Group_Control_Background() );
							$elementor->controls_manager->add_group_control( 'cws_typography', new Elementor\CWS_Group_Control_Typography() );
							$elementor->controls_manager->add_group_control( 'cws_gradient', new Elementor\CWS_Group_Control_Gradient() );
							$elementor->controls_manager->add_group_control( 'cws_border', new Elementor\CWS_Group_Control_Border() );		
						}

						if ( method_exists( $elementor->controls_manager, 'register_control' ) ) {
							if (is_plugin_active('cws-svgicons/cws-svgicons.php')){
								$elementor->controls_manager->register_control( 'CWS_SVG', new Elementor\CWS_Elementor_SVG_Control() );													
							}
						}

					}
				}
			}
		}
	}

	public function cws_register_elements() {
		if ( defined( 'ELEMENTOR_PATH' ) && class_exists( 'Elementor\Element_Base' ) ) {
			if ( class_exists( 'Elementor\Plugin' ) ) {
				if ( is_callable( 'Elementor\Plugin', 'instance' ) ) {
					$elementor = Elementor\Plugin::instance();
					if ( isset( $elementor->elements_manager ) ) {
						if ( method_exists( $elementor->elements_manager, 'register_element_type' ) ) {

							//Files elements list
							$elements = array(
								'cws_column',
								'cws_section',
							);

							if (!empty($elements)){
								foreach ($elements as $key => $value) {
									require_once( trailingslashit( get_template_directory() ) . 'elementor/elements/'. $value . '.php' );
								}
							}

						}
					}
				}
			}
		}
	}

}
CWS_Elementor_Config::get_instance()->init();

//Check if this Elementor page
function is_elementor($id = ''){
	global $post;

	if (!empty($id)){
		return \Elementor\Plugin::$instance->db->is_built_with_elementor($id);
	} else {
		return \Elementor\Plugin::$instance->db->is_built_with_elementor($post->ID);		
	}
}

?>