<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Blog
class CWS_Elementor_Blog extends Widget_Base {
	public function get_name() {
		return 'cws_blog';
	}

	public function get_title() {
		return esc_html__( 'CWS Blog', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-post-list';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		wp_enqueue_script( 'isotope' );
		wp_enqueue_script( 'imagesloaded' );
		wp_enqueue_script( 'fancybox' );

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Filter by (Get Taxonomies)
		$post_type = 'post';

		$taxes = get_object_taxonomies ( $post_type, 'object' );
		$avail_taxes = array(
			'' => esc_html__( 'None', 'cryptop' ),
		);
		foreach ( $taxes as $tax => $tax_obj ){
			$tax_name = isset( $tax_obj->labels->name ) && !empty( $tax_obj->labels->name ) ? $tax_obj->labels->name : $tax;
			$avail_taxes[$tax] = $tax_name;
		}

		$tax_controls = array();
		foreach ( $avail_taxes as $tax => $tax_name ) {
			if ($tax == '') continue;
			$terms = get_terms( $tax );
			$avail_terms = array();
			if ( !is_a( $terms, 'WP_Error' ) ){
				foreach ( $terms as $term ) {
					$avail_terms[$term->slug] = $term->name;
				}
			}

			$tax_controls[$tax] = [
				'label' => esc_html__( $tax_name, 'cryptop' ),
				'type' => Controls_Manager::SELECT2,
				'multiple' => true,
				'options' => $avail_terms,
				'condition' => [
					'filter_by' => $tax
				]				
			];		
		}
		//--Filter by (Get Taxonomies)

		$controls->start_controls_section(
			'section_blog_layout',
			[
				'label' => esc_html__( 'Blog', 'cryptop' ),
				'tab' => Controls_Manager::TAB_LAYOUT,
			]
		);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
					'default' => esc_html__( 'Blog', 'cryptop' ),
					'label_block' => true,
				]
			);

			$controls->add_control(
				'title_aligning',
				[
					'label' => esc_html__( 'Title Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .widgettitle' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'use_carousel',
				[
					'label' => esc_html__( 'Use Carousel', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'description' => esc_html__( 'To make carousel the page using JS. You can change slides using the keyboard arrows (Swiper slider)', 'cryptop' ) . sprintf( ' <a href="%1$s" target="_blank">%2$s</a>', 'http://idangero.us/swiper/demos/', esc_html__( 'Learn more.', 'cryptop' ) ),	
				]
			);

			$controls->add_control(
				'layout',
				[
					'label' => esc_html__( 'Layout', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'def',
					'options' => [
						'def' => esc_html__( 'Default', 'cryptop' ),
						'1' => esc_html__( 'Blog Large', 'cryptop' ),
						'medium' => esc_html__( 'Blog Medium', 'cryptop' ),
						'small' => esc_html__( 'Blog Small', 'cryptop' ),
						'2' => esc_html__( 'Two Columns', 'cryptop' ),
						'3' => esc_html__( 'Three Columns', 'cryptop' ),
						'4' => esc_html__( 'Four Columns', 'cryptop' ),
						'checkerboard' => esc_html__( 'Checkerboard', 'cryptop' ),
						'fw_img' => esc_html__( 'Full Width Background', 'cryptop' )
					],
				]
			);

			$controls->add_control(
				'full_width',
				[
					'label' => esc_html__( 'Full width', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',				
				]
			);

			//REPEATER
			$repeater = new Repeater();

				$repeater->add_control(
					'title',
					[
						'label' => esc_html__( 'Title', 'cryptop' ),
						'type' => Controls_Manager::TEXT,
						'default' => '',
						'title' => esc_html__( 'Title', 'cryptop' ),
					]
				);

				$repeater->add_control(
					'field',
					[
						'label' => esc_html__( 'Field', 'cryptop' ),
						'type' => Controls_Manager::TEXT,
						'default' => '',
						'title' => esc_html__( 'Field', 'cryptop' ),
					]
				);				

			$controls->add_control(
				'elements_order',
				[
					'label'       => esc_html__( 'Elements order', 'cryptop' ),
					'type'        => Controls_Manager::REPEATER,
					'show_label'  => true,
					'default' => [
						[
							'title' => esc_html__( 'Featured image', 'cryptop' ),
							'field' => esc_html__( 'featured_image', 'cryptop' ),
						],
						[
							'title' => esc_html__( 'Data', 'cryptop' ),
							'field' => esc_html__( 'data', 'cryptop' ),
						],						
						[
							'title' => esc_html__( 'Title', 'cryptop' ),
							'field' => esc_html__( 'title', 'cryptop' ),
						],
						[
							'title' => esc_html__( 'Content', 'cryptop' ),
							'field' => esc_html__( 'content', 'cryptop' ),
						],
						[
							'title' => esc_html__( 'Meta', 'cryptop' ),
							'field' => esc_html__( 'meta', 'cryptop' ),
						],
						[
							'title' => esc_html__( 'Read More', 'cryptop' ),
							'field' => esc_html__( 'read_more', 'cryptop' ),
						],																				
					],
					'fields'      => array_values( $repeater->get_controls() ),
					'title_field' => '{{{title}}}',
				]
			);
			//---REPEATER

		$controls->end_controls_section();

		$sections->advanced_carousel(['use_carousel' => 'yes']);

		$controls->start_controls_section(
			'section_blog_content',
			[
				'label' => esc_html__( 'Blog', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'content_aligning',
				[
					'label' => esc_html__( 'Content Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .post_info' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'checkerboard_aligning',
				[
					'label' => esc_html__( 'Content Aligning (Checkerboard)', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'top',
					'toggle' => false,
			        'options' => [
			            'top'    => [
			                'title' => esc_html__( 'Top', 'cryptop' ),
			                'icon' => 'fa fa-chevron-up',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-bars',
			            ],
			            'bottom' => [
			                'title' => esc_html__( 'Bottom', 'cryptop' ),
			                'icon' => 'fa fa-chevron-down',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container section.checkerboard' => 'text-align: {{VALUE}};',
					],
					'condition' => [
						'layout' => 'checkerboard'
					],						
				]
			);

			//Filter by controls
			//Output all filter taxes
			$controls->add_control(
				'filter_by',
				[
					'label' => esc_html__( 'Filter by', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '',
					'options' => $avail_taxes,
				]
			);

			//Output all filter terms
			if (!empty($tax_controls)){
				foreach ( $tax_controls as $control_name => $control_fields ) {
					$controls->add_control(
						$control_name, $control_fields
					);
				}
			}
			//--Filter by controls

			$controls->add_control(
				'items_count',
				[
					'label' => esc_html__( 'Items to display', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 100,
					'step' => 1,
					'default' => get_option( 'posts_per_page' ),
				]
			);

			$controls->add_control(
				'items_per_page',
				[
					'label' => esc_html__( 'Items per Page', 'cryptop' ),
					'description'	=> esc_html__( 'Works on Grid only', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 100,
					'step' => 1,
					'default' => get_option( 'posts_per_page' ),
					'condition' => [
						'use_carousel!' => 'yes'
					],
				]
			);			

			$controls->add_control(
				'pagination_grid',
				[
					'label' => esc_html__( 'Pagination', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'multiple' => true,
					'default' => 'standard',
					'options' => [
						'standard' =>  esc_html__( 'Standard', 'cryptop' ),
						'load_more' =>  esc_html__( 'Load More', 'cryptop' ),
						'load_on_scroll' =>  esc_html__( 'Load on Scroll', 'cryptop' ),
						'ajax' =>  esc_html__( 'Ajax', 'cryptop' ),
					],
					'condition' => [
						'use_carousel!' => 'yes'
					],					
				]
			);

			$controls->add_control(
				'pagination_aligning',
				[
					'label' => esc_html__( 'Pagination Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'center',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .pagination' => 'text-align: {{VALUE}};',
						'{{WRAPPER}} .elementor-widget-container .load_more_wrapper' => 'text-align: {{VALUE}};',
					],			        
					'condition' => [
						'use_carousel!' => 'yes',
						'pagination_grid!' => 'load_on_scroll'
					],						
				]
			);			

			$controls->add_control(
				'chars_count',
				[
					'label' => esc_html__( 'Content Character Limit', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 1000,
					'step' => 1,
					'default' => '',
				]
			);

			$controls->add_control(
				'hide_meta',
				[
					'label' => esc_html__( 'Hide meta', 'cryptop' ),
					'type' => Controls_Manager::SELECT2,
					'multiple' => true,
					'default' => '',
					'options' => [
						'title' =>  esc_html__( 'Title', 'cryptop' ),
						'cats' =>  esc_html__( 'Categories', 'cryptop' ),
						'tags' =>  esc_html__( 'Tags', 'cryptop' ),
						'author' =>  esc_html__( 'Author', 'cryptop' ),
						'likes' =>  esc_html__( 'Likes', 'cryptop' ),
						'date' =>  esc_html__( 'Date', 'cryptop' ),
						'comments' =>  esc_html__( 'Comments', 'cryptop '),
						'read_more' =>  esc_html__( 'Read More', 'cryptop '),
						'social' =>  esc_html__( 'Social Icons', 'cryptop '),
						'excerpt' =>  esc_html__( 'Excerpt', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'read_more_title',
				[
					'label' => esc_html__( 'Read More button', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter your caption', 'cryptop' ),
					'default' => esc_html__( 'Read More', 'cryptop' ),
					'label_block' => true,
				]
			);			

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_blog_style_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'grid_columns_spacings',
				[
					'label' => esc_html__( 'Columns spacings', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],					
					'default' => [
						'size' => 50,
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .cws_icon:before' => 'font-size: {{SIZE}}{{UNIT}}',
					],					
					'label_block' => true,
					'condition' => [
						'use_carousel!' => 'yes'
					],						
				]
			);

			$controls->add_control(
				'grid_rows_spacings',
				[
					'label' => esc_html__( 'Rows spacings', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],					
					'default' => [
						'size' => 50,
						'unit' => 'px',
					],
					'selectors' => [
						'{{WRAPPER}} {{CURRENT_ITEM}} .cws_icon:before' => 'font-size: {{SIZE}}{{UNIT}}',
					],					
					'label_block' => true,
					'condition' => [
						'use_carousel!' => 'yes'
					],						
				]
			);

			$controls->add_control(
				'crop_featured',
				[
					'label' => esc_html__( 'Crop featured', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',		
				]
			);

			$controls->add_control(
				'date_container',
				[
					'label' => esc_html__( 'Date container', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'outside',
					'options' => [
						'outside' => esc_html__( 'Outside', 'cryptop' ),
						'inside' => esc_html__( 'Inside', 'cryptop' ),
						'meta' => esc_html__( 'Meta', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'appear_style',
				[
					'label' => esc_html__( 'Appear style', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'none' => esc_html__( 'None', 'cryptop' ),
						'callout.bounce' => esc_html__( 'Bounce', 'cryptop' ),
						'callout.shake' => esc_html__( 'Shake', 'cryptop' ),
						'callout.flash' => esc_html__( 'Flash', 'cryptop' ),
						'callout.pulse' => esc_html__( 'Pulse', 'cryptop' ),
						'callout.swing' => esc_html__( 'Swing', 'cryptop' ),
						'callout.tada' => esc_html__( 'Tada', 'cryptop' ),
						'transition.fadeIn' => esc_html__( 'Fade In', 'cryptop' ),
						'transition.flipXIn' => esc_html__( 'Flip X In', 'cryptop' ),
						'transition.flipYIn' => esc_html__( 'Flip Y In', 'cryptop' ),
						'transition.shrinkIn' => esc_html__( 'Shrink In', 'cryptop' ),
						'transition.expandIn' => esc_html__( 'Expand In', 'cryptop' ),
						'transition.grow' => esc_html__( 'Grow', 'cryptop' ),
						'transition.slideUpBigIn' => esc_html__( 'Slide Up', 'cryptop' ),
						'transition.slideDownBigIn' => esc_html__( 'Slide Down', 'cryptop' ),
						'transition.slideLeftBigIn' => esc_html__( 'Slide Left', 'cryptop' ),
						'transition.slideRightBigIn' => esc_html__( 'Slide Right', 'cryptop' ),
						'transition.perspectiveUpIn' => esc_html__( 'Perspective Up', 'cryptop' ),
						'transition.perspectiveDownIn' => esc_html__( 'Perspective Down', 'cryptop' ),
						'transition.perspectiveLeftIn' => esc_html__( 'Perspective Left', 'cryptop' ),
						'transition.perspectiveRightIn' => esc_html__( 'Perspective Right', 'cryptop' ),
					],
				]
			);				

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_blog_style_hover',
			[
				'label' => esc_html__( 'Hover', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'link_show',
				[
					'label' => esc_html__( 'Hover Effect', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'color',
					'options' => [
						'color' => esc_html__( 'Fill Color', 'cryptop' ),
						'single_link' => esc_html__( 'Fill Color + URL', 'cryptop' ),
						'dots' => esc_html__( 'Show Dots', 'cryptop' ),
						'popup_link' => esc_html__( 'Show Image PoPup Icon', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'hover_animation',
				[
					'label' => esc_html__( 'Image Hover Animation', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'none' => esc_html__( "None", 'cryptop' ),
						'01' => esc_html__( "Zoom In 1", 'cryptop' ),
						'02' => esc_html__( "Zoom In 2", 'cryptop' ),
						'03' => esc_html__( "Zoom Out 1", 'cryptop' ),
						'04' => esc_html__( "Zoom Out 2", 'cryptop' ),
						'05' => esc_html__( "Slide", 'cryptop' ),
						'06' => esc_html__( "Rotate", 'cryptop' ),
						'07' => esc_html__( "Blur", 'cryptop' ),
						'08' => esc_html__( "Gray Scale", 'cryptop' ),
						'09' => esc_html__( "Sepia", 'cryptop' ),
						'10' => esc_html__( "Blur + Gray Scale", 'cryptop' ),
						'11' => esc_html__( "Opacity 1", 'cryptop' ),
						'12' => esc_html__( "Opacity 2", 'cryptop' ),
						'13' => esc_html__( "Flashing", 'cryptop' ),
						'14' => esc_html__( "Shine", 'cryptop' ),
						'15' => esc_html__( "Circle", 'cryptop' ),
					],
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Gradient::get_type(),
				[
					'name' => 'gradient',
					'label' => esc_html__( 'Background gradient', 'cryptop' ),
					'types' => [ 'classic', 'gradient' ],
					'selector' => '{{WRAPPER}} .blog_post .post_media .hover-effect',
					'fields_options' => [
						'background' => [
							'label' => _x( 'Hover overlay', 'Background Control', 'cryptop' ),
						],
					],					
					'defaults' => [
						'background' => 'classic',
						'color' => $cws_theme_funcs->cws_Hex2RGBA($theme_colors_first_color,0.5),
						'color_stop' => [
							'unit' => '%',
							'size' => 0,
						],
						'color_b' => $theme_colors_first_color,
						'color_b_stop' => [
							'unit' => '%',
							'size' => 100,
						],
						'gradient_type' => 'linear',
						'gradient_angle' => [
							'unit' => 'deg',
							'size' => 90,
						],
						'gradient_position' => 'center center',
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_blog_style_read_more',
			[
				'label' => esc_html__( 'Read more', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'read_more_title!' => '',
				],
			]
		);

			$controls->add_control(
				'read_more_aligning',
				[
					'label' => esc_html__( 'Read More Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'right',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .post_button.btn-read-more' => 'text-align: {{VALUE}};',
					],
					'condition' => [
						'use_carousel!' => 'yes'
					],						
				]
			);	

			$controls->add_control(
				'read_more_hover_effect',
				[
					'label' => esc_html__( 'Hover Effects', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'multiple' => true,
					'default' => 'style_1',
					'options' => [
						'style_1' =>  esc_html__( 'Style 1', 'cryptop' ),
						'style_2' =>  esc_html__( 'Style 2', 'cryptop' ),
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_blog_style_hover_title',
			[
				'label' => esc_html__( 'Title', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'title_color',
				[
					'label' => esc_html__( 'Text Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '#000',
					'selectors' => [
						'{{WRAPPER}} .widgettitle' => 'color: {{VALUE}};',
					],
					'scheme' => [
						'type' => Scheme_Color::get_type(),
						'value' => Scheme_Color::COLOR_1,
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .widgettitle',
					'scheme' => Scheme_Typography::TYPOGRAPHY_3,
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_blog', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
/*	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_blog', $this, 'js'));
	}*/
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Blog() );