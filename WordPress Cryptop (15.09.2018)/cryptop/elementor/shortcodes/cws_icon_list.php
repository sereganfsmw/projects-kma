<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Icon list
class CWS_Elementor_Icon_list extends Widget_Base {
	public function get_name() {
		return 'icon-list';
	}

	public function get_title() {
		return esc_html__( 'CWS Icon list', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-bullet-list';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_icon',
			[
				'label' => esc_html__( 'Icon List', 'cryptop' ),
			]
		);

		//Fill array with icons
		$ficons = cws_get_all_flaticon_icons();
		$fl_icons = [];
		$fl_icons_include = [];

		foreach ($ficons as $key => $value) {
			$fl_icons['flaticon-'.$value] = $value;
			$fl_icons_include[] = 'flaticon-'.$value;
		}
		//--Fill array with icons

		$controls->add_control(
			'view',
			[
				'label' => esc_html__( 'Layout', 'cryptop' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => 'traditional',
				'options' => [
					'traditional' => [
						'title' => esc_html__( 'Default', 'cryptop' ),
						'icon' => 'eicon-editor-list-ul',
					],
					'inline' => [
						'title' => esc_html__( 'Inline', 'cryptop' ),
						'icon' => 'eicon-ellipsis-h',
					],
				],
				'render_type' => 'template',
				'classes' => 'elementor-control-start-end',
				'label_block' => false,
			]
		);

		$controls->add_control(
			'icon_list',
			[
				'label' => '',
				'type' => Controls_Manager::REPEATER,
				'title_field' => '<i class="{{ icon }}" aria-hidden="true"></i> {{{ text }}}',
				'default' => [
					[
						'text' => esc_html__( 'List Item #1', 'cryptop' ),
						'icon' => 'fa fa-check',
					],
					[
						'text' => esc_html__( 'List Item #2', 'cryptop' ),
						'icon' => 'fa fa-times',
					],
					[
						'text' => esc_html__( 'List Item #3', 'cryptop' ),
						'icon' => 'fa fa-dot-circle-o',
					],
				],
				'fields' => [
					[
						'name' => 'text',
						'label' => esc_html__( 'Text', 'cryptop' ),
						'type' => Controls_Manager::TEXT,
						'label_block' => true,
						'placeholder' => esc_html__( 'List Item', 'cryptop' ),
						'default' => esc_html__( 'List Item', 'cryptop' ),
					],
					[
						'name' => 'icon_lib',
						'label' => esc_html__( 'Icon library', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => 'fontawesome',
						'options' => [
							'fontawesome' => esc_html__( 'Font Awesome', 'cryptop' ),
							'flaticons' => esc_html__( 'CWS Flaticons', 'cryptop' ),
						],
						'label_block' => true,
					],
					[
						'name' => 'icon',
						'label' => esc_html__( 'Font Awesome', 'cryptop' ),
						'type' => Controls_Manager::ICON,
						'label_block' => true,
						'default' => 'fa fa-check',
						'condition' => [
							'icon_lib' => 'fontawesome'
						]
					],
					[
						'name' => 'icon',
						'label' => esc_html__( 'CWS Flaticons', 'cryptop' ),
						'type' => Controls_Manager::ICON,
						'label_block' => true,
						'options' => $fl_icons,
						'include' => $fl_icons_include,			
						'condition' => [
							'icon_lib' => 'flaticons'
						],
					],
					[
						'name' => 'link',
						'label' => esc_html__( 'Link', 'cryptop' ),
						'type' => Controls_Manager::URL,
						'label_block' => true,
						'placeholder' => esc_html__( 'https://your-link.com', 'cryptop' ),
					],
				],
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_icon_list',
			[
				'label' => esc_html__( 'List', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_responsive_control(
			'column_count',
			[
				'label' => esc_html__( 'Columns count', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'1' => esc_html__( '1', 'cryptop' ),
					'2' => esc_html__( '2', 'cryptop' ),
					'3' => esc_html__( '3', 'cryptop' ),
					'4' => esc_html__( '4', 'cryptop' ),
				],
				'default' => '1',
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-items' => '-webkit-column-count: {{VALUE}}; -moz-column-count: {{VALUE}}; column-count: {{VALUE}};',
				],
			]
		);

		$controls->add_responsive_control(
			'space_between',
			[
				'label' => esc_html__( 'Space Between', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-items:not(.elementor-inline-items) .elementor-icon-list-item' => 'padding-bottom: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .elementor-icon-list-items:not(.elementor-inline-items) .elementor-icon-list-item:not(:first-child)' => 'margin-top: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item' => 'margin-right: calc({{SIZE}}{{UNIT}}/2); margin-left: calc({{SIZE}}{{UNIT}}/2)',
					'{{WRAPPER}} .elementor-icon-list-items.elementor-inline-items' => 'margin-right: calc(-{{SIZE}}{{UNIT}}/2); margin-left: calc(-{{SIZE}}{{UNIT}}/2)',
					'body.rtl {{WRAPPER}} .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item:after' => 'left: calc(-{{SIZE}}{{UNIT}}/2)',
					'body:not(.rtl) {{WRAPPER}} .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item:after' => 'right: calc(-{{SIZE}}{{UNIT}}/2)',
				],
			]
		);

		$controls->add_responsive_control(
			'icon_align',
			[
				'label' => esc_html__( 'Alignment', 'cryptop' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'left' => [
						'title' => esc_html__( 'Start', 'cryptop' ),
						'icon' => 'eicon-h-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'cryptop' ),
						'icon' => 'eicon-h-align-center',
					],
					'right' => [
						'title' => esc_html__( 'End', 'cryptop' ),
						'icon' => 'eicon-h-align-right',
					],
				],
				'prefix_class' => 'elementor%s-align-',
			]
		);

		$controls->add_control(
			'divider',
			[
				'label' => esc_html__( 'Divider', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'label_off' => esc_html__( 'Off', 'cryptop' ),
				'label_on' => esc_html__( 'On', 'cryptop' ),
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-item:after' => 'content: ""',
				],
				'return_value' => 'yes',
				'separator' => 'before',
			]
		);

		$controls->add_control(
			'divider_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'solid' => esc_html__( 'Solid', 'cryptop' ),
					'double' => esc_html__( 'Double', 'cryptop' ),
					'dotted' => esc_html__( 'Dotted', 'cryptop' ),
					'dashed' => esc_html__( 'Dashed', 'cryptop' ),
				],
				'default' => 'solid',
				'condition' => [
					'divider' => 'yes',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-items:not(.elementor-inline-items) .elementor-icon-list-item:after' => 'border-top-style: {{VALUE}}',
					'{{WRAPPER}} .elementor-icon-list-items.elementor-inline-items .elementor-icon-list-item:after' => 'border-left-style: {{VALUE}}',
				],
			]
		);

		$controls->add_control(
			'divider_weight',
			[
				'label' => esc_html__( 'Weight', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 1,
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 20,
					],
				],
				'condition' => [
					'divider' => 'yes',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-items:not(.elementor-inline-items) .elementor-icon-list-item:after' => 'border-top-width: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .elementor-inline-items .elementor-icon-list-ite:after' => 'border-left-width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$controls->add_control(
			'divider_width',
			[
				'label' => esc_html__( 'Width', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'units' => [ '%' ],
				'default' => [
					'unit' => '%',
				],
				'condition' => [
					'divider' => 'yes',
					'view!' => 'inline',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-item:after' => 'width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$controls->add_control(
			'divider_height',
			[
				'label' => esc_html__( 'Height', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ '%', 'px' ],
				'default' => [
					'unit' => '%',
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 100,
					],
					'%' => [
						'min' => 1,
						'max' => 100,
					],
				],
				'condition' => [
					'divider' => 'yes',
					'view' => 'inline',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-item:after' => 'height: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$controls->add_control(
			'divider_color',
			[
				'label' => esc_html__( 'Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#ddd',
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_3,
				],
				'condition' => [
					'divider' => 'yes',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-item:after' => 'border-color: {{VALUE}}',
				],
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_icon_style',
			[
				'label' => esc_html__( 'Icon', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-icon i' => 'color: {{VALUE}};',
				],
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_1,
				],
			]
		);

		$controls->add_control(
			'icon_color_hover',
			[
				'label' => esc_html__( 'Hover', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-item:hover .elementor-icon-list-icon i' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->add_responsive_control(
			'icon_size',
			[
				'label' => esc_html__( 'Size', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 14,
				],
				'range' => [
					'px' => [
						'min' => 6,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-icon' => 'width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-icon-list-icon i' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-icon-list-icon i:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_text_style',
			[
				'label' => esc_html__( 'Text', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'text_color',
			[
				'label' => esc_html__( 'Text Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-text' => 'color: {{VALUE}};',
				],
				'scheme' => [
					'type' => Scheme_Color::get_type(),
					'value' => Scheme_Color::COLOR_2,
				],
			]
		);

		$controls->add_control(
			'text_color_hover',
			[
				'label' => esc_html__( 'Hover', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-item:hover .elementor-icon-list-text' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->add_control(
			'text_indent',
			[
				'label' => esc_html__( 'Text Indent', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'max' => 50,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-icon-list-text' => is_rtl() ? 'padding-right: {{SIZE}}{{UNIT}};' : 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$controls->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'icon_typography',
				'selector' => '{{WRAPPER}} .elementor-icon-list-item',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;

		$extra_params = array();
		$settings = $this->get_settings();

		foreach ( $settings['icon_list'] as $index => $item ){
			$repeater_setting_key = $this->get_repeater_setting_key( 'text', 'icon_list', $index );
			$extra_params[$index] = $repeater_setting_key;

			$this->add_render_attribute( $repeater_setting_key, 'class', 'elementor-icon-list-text' );
		}

		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_icon_list', $this, 'php', $extra_params));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_icon_list', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Icon_list() );