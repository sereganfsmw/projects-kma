<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Pie Chart
class CWS_Elementor_Pie_chart extends Widget_Base {
	public function get_name() {
		return 'cws_pie_chart';
	}

	public function get_title() {
		return esc_html__( 'CWS Pie Chart', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-pie-chart';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {

		wp_enqueue_script( 'pie_chart' );

		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		$controls->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
				'default' => esc_html__( 'Progress', 'cryptop' ),
				'label_block' => true,
			]
		);

		$controls->add_control(
			'progress',
			[
				'label' => esc_html__( 'Progress', 'cryptop' ),
				"description"	=> esc_html__( 'In Percents', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => '%' ,
				'render_type' => 'template',
				'range' => [
					'%' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => '%',
					'size' => 75,
				],
			]
		);

		$controls->add_control(
			'duration',
			[
				'label' => esc_html__( 'Animation Duration', 'cryptop' ),
				'description' => esc_html__( 'In Milliseconds', 'cryptop' ),
				'type' => Controls_Manager::NUMBER,				
				'min' => 1000,
				'max' => 10000,
				'step' => 1000,
				'default' => 3000,
			]
		);

		$controls->add_control(
			'circle_width',
			[
				'label' => esc_html__( 'Circle width', 'cryptop' ),
				'description' => esc_html__( 'In Pixels', 'cryptop' ),
				'type' => Controls_Manager::NUMBER,				
				'min' => 1,
				'max' => 20,
				'step' => 1,
				'default' => 10,
			]
		);			

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'custom_title_color',
				[
					'label' => esc_html__( 'Title Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#000000',					
					'default' => '#000000',					
					'selectors' => [
						'{{WRAPPER}} .cws_pie_char_title' => 'color: {{VALUE}}',
					],				
				]
			);

			$controls->add_control(
				'custom_line_color',
				[
					'label' => esc_html__( 'Bar Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#E1E9F4',
					'default' => '#E1E9F4',
					'render_type' => 'template',
				]
			);

			$controls->add_control(
				'custom_bar_color_start',
				[
					'label' => esc_html__( 'Line Color (start)', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $theme_colors_first_color,
					'default' => $theme_colors_first_color,
					'render_type' => 'template',
				]
			);

			$controls->add_control(
				'custom_bar_color_end',
				[
					'label' => esc_html__( 'Line Color (end)', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => '#fff',
					'default' => '#fff',
					'render_type' => 'template',				
				]
			);

			$controls->add_control(
				'custom_percents_color',
				[
					'label' => esc_html__( 'Percents Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $theme_colors_first_color,
					'default' => $theme_colors_first_color,	
					'selectors' => [
						'{{WRAPPER}} .cws_pie_char_value' => 'color: {{VALUE}}',
					],				
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_pie_chart', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_pie_chart', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Pie_chart() );