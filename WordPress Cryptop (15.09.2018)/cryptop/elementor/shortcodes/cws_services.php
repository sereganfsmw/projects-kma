<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Services
class CWS_Elementor_Services extends Widget_Base {
	public function get_name() {
		return 'cws_services';
	}

	public function get_title() {
		return esc_html__( 'CWS Services', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-diamond';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Add extra section
		$sections->icons();

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
					'default' => esc_html__( 'How we work', 'cryptop' ),
					'label_block' => true,
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_service_title',
					'label'	=> 'Title Typography',
					'condition' => [
						'title!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'subtitle',
				[
					'label' => esc_html__( 'Prefix', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'placeholder' => esc_html__( 'Enter your subtitle', 'cryptop' ),
					'default' => esc_html__( 'How we work', 'cryptop' ),
					'label_block' => true,
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'subtitle_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_service_title .cws_service_subtitle',
					'label'	=> 'Prefix Typography',
					'condition' => [
						'subtitle!' => ''
					],
					'exclude' => ['html_tag'],
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h6',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'description',
				[
					'label' => esc_html__( 'Description', 'cryptop' ),
					'rows' => 10,
					'type' => Controls_Manager::TEXTAREA,
					'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
					'default' => esc_html__( 'Text content', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'description_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_service_desc',
					'label'	=> 'Description Typography',
					'condition' => [
						'description!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 14,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_responsive_control(
				'text_aligning',
				[
					'label' => esc_html__( 'Text Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-widget-container' => 'text-align: {{VALUE}};',
					],
					'condition' => [
						'bottom_line_divider!' => 'yes'
					],	
				]
			);

			$controls->add_responsive_control(
				'custom_text_width',
				[
					'label' => esc_html__( 'Content Max Width', 'cryptop' ),
					'description' => esc_html__( 'In Percents', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ '%' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => '%',
						'size' => 100,
					],
					'tablet_default' => [
						'unit' => '%',
						'size' => 100,
					],
					'mobile_default' => [
						'unit' => '%',
						'size' => 100,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_service_info' => 'max-width: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$controls->add_responsive_control(
				'icon_aligning',
				[
					'label' => esc_html__( 'Icon Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'none',
					'toggle' => false,
					'render_type' => 'template',
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-chevron-left',
						],
						'none' => [
							'title' => esc_html__( 'Top', 'cryptop' ),
							'icon' => 'fa fa-chevron-up',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .cws_service_icon_wrapper' => 'float: {{VALUE}};',
					],
				]
			);

			$controls->add_responsive_control(
				'side_line_divider',
				[
					'label' => esc_html__( 'Side Line', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'label_block' => true,
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'bottom_line_divider',
								'operator' => '!=',
								'value' => 'yes',
							], [
								'name' => 'image_divider',
								'operator' => '!=',
								'value' => 'yes',
							],
						],
					],
				]
			);

			$controls->add_control(
				'side_lines',
				[
					'label' => esc_html__( 'Side Line Position', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'right',
					'toggle' => false,
					'render_type' => 'template',
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-chevron-left',
						],
						'double' => [
							'title' => esc_html__( 'Double', 'cryptop' ),
							'icon' => 'fa fa-pause',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-chevron-right',
						],
					],
					'condition' => [
						'side_line_divider' => 'yes',
					],
				]
			);

			$controls->add_control(
				'bottom_line_divider',
				[
					'label' => esc_html__( 'Bottom Line Divider', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'side_line_divider',
								'operator' => '!=',
								'value' => 'yes',
							], [
								'name' => 'image_divider',
								'operator' => '!=',
								'value' => 'yes',
							], [
								'name' => 'icon_aligning',
								'operator' => '!=',
								'value' => 'none',
							]
						],
					],
				]
			);

			$controls->add_responsive_control(
				'image_divider',
				[
					'label' => esc_html__( 'Image Divider', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'label_block' => true,
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'side_line_divider',
								'operator' => '!=',
								'value' => 'yes',
							], [
								'name' => 'bottom_line_divider',
								'operator' => '!=',
								'value' => 'yes',
							], [
								'name' => 'icon_aligning',
								'operator' => '==',
								'value' => 'none',
							]
						],
					],
				]
			);	

			$controls->add_control(
				'img',
				[
					'label' => esc_html__( 'Image', 'cryptop' ),
					'type' => Controls_Manager::MEDIA,
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'image_divider',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'icon_aligning',
								'operator' => '==',
								'value' => 'none',
							]
						],
					],					
					'default' => [
						'url' => '',
					],
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_responsive_control(
				'icon_margins',
				[
					'label' => esc_html__( 'Icon Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 20,
						'left' => 0,
					],
					'tablet_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 20,
						'left' => 0,
					],
					'mobile_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 10,
						'left' => 0,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_service_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'conditions' => [
						'relation' => 'or',
						'terms' => [
							[
								'name' => 'icon_fontawesome',
								'operator' => '!=',
								'value' => ''
							], [
								'name' => 'icon_flaticons',
								'operator' => '!=',
								'value' => ''
							]
						]
					],
				]
			);

			$controls->add_responsive_control(
				'title_margins',
				[
					'label' => esc_html__( 'Title Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .cws_service_title' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
					],
					'tablet_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
					],
					'mobile_default' => [
						'unit' => 'px',
						'top' => 0,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
					],
					'condition' => [
						'title!' => ''
					],
				]
			);

			$controls->add_responsive_control(
				'description_margins',
				[
					'label' => esc_html__( 'Description Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .cws_service_desc' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'unit' => 'px',
						'top' => 15,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
					],
					'tablet_default' => [
						'unit' => 'px',
						'top' => 15,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
					],
					'mobile_default' => [
						'unit' => 'px',
						'top' => 10,
						'right' => 0,
						'bottom' => 0,
						'left' => 0,
					],
					'condition' => [
						'description!' => ''
					],
				]
			);

			$controls->add_control(
				'customize_colors',
				[
					'label' => esc_html__( 'Customize Colors', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			/*------------------------TITLE POPOVER------------------------*/
			$controls->add_control(
				'title_popover',
				[
					'label' => esc_html__( 'Title Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'title',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'title_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000',
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}} .cws_service_title' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'title_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000',
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_service_title' => 'color: {{VALUE}}',
						],			
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------SUBTITLE POPOVER------------------------*/
			$controls->add_control(
				'subtitle_popover',
				[
					'label' => esc_html__( 'Subtitle Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'subtitle',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'subtitle_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000',
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}} .cws_service_subtitle' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'subtitle_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000',
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_service_subtitle' => 'color: {{VALUE}}',
						],			
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------DESCRIPTION POPOVER------------------------*/
			$controls->add_control(
				'description_popover',
				[
					'label' => esc_html__( 'Description Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							]
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'description_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#474747',
						'default' => '#474747',
						'selectors' => [
							'{{WRAPPER}} .cws_service_desc' => 'color: {{VALUE}}',
						],					
					]
				);

				$controls->add_control(
					'description_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#474747',
						'default' => '#474747',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_service_desc' => 'color: {{VALUE}}',
						],				
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------DIVIDER POPOVER------------------------*/
			$controls->add_control(
				'divider_popover',
				[
					'label' => esc_html__( 'Divider Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							]
						],
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'customize_colors',
										'operator' => '==',
										'value' => 'yes'
									]
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'side_line_divider',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'bottom_line_divider',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					]				
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'divider_color',
					[
						'label' => esc_html__( 'Divider color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $theme_colors_first_color,
						'default' => $theme_colors_first_color,
						'selectors' => [
							'{{WRAPPER}} .elementor-widget-container.bottom_divider .cws_service_item:before' => 'background-color: {{VALUE}}',
						],
						'condition' => [
							'bottom_line_divider' => ['yes']
						],				
					]
				);

				$controls->add_control(
					'divider_color_1',
					[
						'label' => esc_html__( 'Divider color 1', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $theme_colors_first_color,
						'default' => $theme_colors_first_color,
						'selectors' => [
							'{{WRAPPER}} .elementor-widget-container .cws_service_item .cws_left_divider:after' => 'background-color: {{VALUE}}',
							'{{WRAPPER}} .elementor-widget-container .cws_service_item .cws_right_divider:after' => 'background-color: {{VALUE}}',
						],
						'condition' => [
							'side_line_divider' => ['yes']
						],				
					]
				);

				$controls->add_control(
					'divider_color_2',
					[
						'label' => esc_html__( 'Divider color 2', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#E1E9F4',
						'default' => '#E1E9F4',
						'selectors' => [
							'{{WRAPPER}} .elementor-widget-container .cws_service_item .cws_left_divider:before' => 'background-color: {{VALUE}}',
							'{{WRAPPER}} .elementor-widget-container .cws_service_item .cws_right_divider:before' => 'background-color: {{VALUE}}',
						],
						'condition' => [
							'side_line_divider' => ['yes']
						],
					]
				);

			$controls->end_popover();
			/*------------------------------------------------*/

			$controls->add_control(
				'customize_icon',
				[
					'label' => esc_html__( 'Customize Icon', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'hover_block_animation',
				[
					'label' => esc_html__( 'Zoom on hover', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_custom_icon',
			[
				'label' => esc_html__( 'Custom icon', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
				'condition' => [
					'customize_icon' => ['yes']
				],				
			]
		);

			$controls->add_control(
				'icon_shape',
				[
					'label' => esc_html__( 'Icon shape', 'cryptop' ),
					'type' => Controls_Manager::SELECT,				
					'default' => 'round',
					'options' => [
						'none' => esc_html__( 'None', 'cryptop' ),
						'round' => esc_html__( 'Round', 'cryptop' ),
						'square' => esc_html__( 'Square', 'cryptop' ),
						'diamond' => esc_html__( 'Diamond', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'custom_icon_size',
				[
					'label' => esc_html__( 'Icon size', 'cryptop' ),
					'description' => esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 20,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_icon:before' => 'font-size: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$controls->add_control(
				'custom_icon_wrapper_size',
				[
					'label' => esc_html__( 'Icon Wrapper size', 'cryptop' ),
					'description' => esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 200,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 80,
					],
					'selectors' => [
						'{{WRAPPER}} .cws_service_item.custom_icon:not(.shape_none) .cws_service_icon_container' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .elementor-widget-container.bottom_divider .cws_service_item:not(.shape_none):before' => 'height: calc(100% - {{SIZE}}{{UNIT}} - 10px);',
						'{{WRAPPER}} .elementor-widget-container.bottom_divider .cws_service_item:before' => 'left: calc({{SIZE}}{{UNIT}} / 2 - 1px);',
						'{{WRAPPER}} .elementor-widget-container.bottom_divider .cws_service_item:not(.shape_none)' => 'min-height: calc({{SIZE}}{{UNIT}} + 20px);',
					],
				]
			);

			$controls->start_controls_tabs( 'tabs_button_style');

				$controls->start_controls_tab(
					'tab_button_normal',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),						
					]
				);

					$controls->add_control(
						'inner_border_popover',
						[
							'label' => esc_html__( 'Inner Border', 'cryptop' ),
							'type' => Controls_Manager::POPOVER_TOGGLE,
							'label_off' => esc_html__( 'Default', 'cryptop' ),
							'label_on' => esc_html__( 'Custom', 'cryptop' ),
							'return_value' => 'yes',		
							'conditions' => [
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_shape',
										'operator' => '!=',
										'value' => 'none',
									],
								],
							],
						]
					);

					$controls->start_popover();

						$controls->add_control(
							'inner_border',
							[
								'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::SELECT,
								'options' => [
									'' => esc_html__( 'None', 'cryptop' ),
									'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
									'double' => _x( 'Double', 'Border Control', 'cryptop' ),
									'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
									'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
								],
								'selectors' => [
									'{{WRAPPER}} .cws_service_icon_container:after' => 'border-style: {{VALUE}};',
								],		
							]
						);

						$controls->add_control(
							'inner_border_width',
							[
								'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::DIMENSIONS,
								'selectors' => [
									'{{WRAPPER}} .cws_service_icon_container:after' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
									'{{WRAPPER}} .cws_service_icon_container:before' => 'top: {{TOP}}{{UNIT}} !important; right: {{RIGHT}}{{UNIT}} !important; bottom: {{BOTTOM}}{{UNIT}} !important; left: {{LEFT}}{{UNIT}} !important;',
								],
								'condition' => [
									'inner_border!' => '',
								],	
							]
						);					

						$controls->add_control(
							'inner_border_color',
							[
								'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::COLOR,
								'selectors' => [
									'{{WRAPPER}} .cws_service_icon_container:after' => 'border-color: {{VALUE}};',
								],
								'condition' => [
									'inner_border!' => '',
								],
							]
						);	

					$controls->end_popover();

					$controls->add_control(
						'outer_border_popover',
						[
							'label' => esc_html__( 'Outer Border', 'cryptop' ),
							'type' => Controls_Manager::POPOVER_TOGGLE,
							'label_off' => esc_html__( 'Default', 'cryptop' ),
							'label_on' => esc_html__( 'Custom', 'cryptop' ),
							'return_value' => 'yes',
							'conditions' => [
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_shape',
										'operator' => '!=',
										'value' => 'none',
									],
								],
							],		
						]
					);

					$controls->start_popover();

						$controls->add_control(
							'outer_border',
							[
								'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::SELECT,
								'options' => [
									'' => esc_html__( 'None', 'cryptop' ),
									'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
									'double' => _x( 'Double', 'Border Control', 'cryptop' ),
									'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
									'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
								],
								'selectors' => [
									'{{WRAPPER}} .cws_service_icon_container:before' => 'border-style: {{VALUE}};',
								],		
							]
						);

						$controls->add_control(
							'outer_border_width',
							[
								'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::DIMENSIONS,
								'selectors' => [
									'{{WRAPPER}} .cws_service_icon_container:before' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
								],
								'condition' => [
									'outer_border!' => '',
								],	
							]
						);					

						$controls->add_control(
							'outer_border_color',
							[
								'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::COLOR,
								'selectors' => [
									'{{WRAPPER}} .cws_service_icon_container:before' => 'border-color: {{VALUE}};',
								],
								'condition' => [
									'outer_border!' => '',
								],
							]
						);	

					$controls->end_popover();

					$controls->add_control(
						'background_color',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#fff',
							'value' => '#fff',
							'selectors' => [
								'{{WRAPPER}} .cws_service_icon_container:before' => 'background-color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'icon_color',
						[
							'label' => esc_html__( 'Icon color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => '#545454',
							'default' => '#545454',
							'selectors' => [
								'{{WRAPPER}} .cws_service_icon_container .cws_icon' => 'color: {{VALUE}}',
							],				
						]
					);				

				$controls->end_controls_tab();

				$controls->start_controls_tab(
					'tab_button_hover',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),						
					]
				);

					$controls->add_control(
						'inner_border_hover_popover',
						[
							'label' => esc_html__( 'Inner Border', 'cryptop' ),
							'type' => Controls_Manager::POPOVER_TOGGLE,
							'label_off' => esc_html__( 'Default', 'cryptop' ),
							'label_on' => esc_html__( 'Custom', 'cryptop' ),
							'return_value' => 'yes',
							'conditions' => [
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_shape',
										'operator' => '!=',
										'value' => 'none',
									],
								],
							],	
						]
					);

					$controls->start_popover();

						$controls->add_control(
							'inner_border_hover',
							[
								'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::SELECT,
								'options' => [
									'' => esc_html__( 'None', 'cryptop' ),
									'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
									'double' => _x( 'Double', 'Border Control', 'cryptop' ),
									'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
									'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
								],
								'selectors' => [
									'{{WRAPPER}}:hover .cws_service_icon_container:after' => 'border-style: {{VALUE}};',
								],		
							]
						);

						$controls->add_control(
							'inner_border_hover_width',
							[
								'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::DIMENSIONS,
								'selectors' => [
									'{{WRAPPER}}:hover .cws_service_icon_container:after' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
									'{{WRAPPER}}:hover .cws_service_icon_container:before' => 'top: {{TOP}}{{UNIT}} !important; right: {{RIGHT}}{{UNIT}} !important; bottom: {{BOTTOM}}{{UNIT}} !important; left: {{LEFT}}{{UNIT}} !important;',
								],
								'condition' => [
									'inner_border_hover!' => '',
								],	
							]
						);					

						$controls->add_control(
							'inner_border_hover_color',
							[
								'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::COLOR,
								'selectors' => [
									'{{WRAPPER}}:hover .cws_service_icon_container:after' => 'border-color: {{VALUE}};',
								],
								'condition' => [
									'inner_border_hover!' => '',
								],
							]
						);	

					$controls->end_popover();

					$controls->add_control(
						'outer_border_hover_popover',
						[
							'label' => esc_html__( 'Outer Border', 'cryptop' ),
							'type' => Controls_Manager::POPOVER_TOGGLE,
							'label_off' => esc_html__( 'Default', 'cryptop' ),
							'label_on' => esc_html__( 'Custom', 'cryptop' ),
							'return_value' => 'yes',
							'conditions' => [
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_shape',
										'operator' => '!=',
										'value' => 'none',
									],
								],
							],			
						]
					);

					$controls->start_popover();

						$controls->add_control(
							'outer_border_hover',
							[
								'label' => _x( 'Border Type', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::SELECT,
								'options' => [
									'' => esc_html__( 'None', 'cryptop' ),
									'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
									'double' => _x( 'Double', 'Border Control', 'cryptop' ),
									'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
									'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
								],
								'selectors' => [
									'{{WRAPPER}}:hover .cws_service_icon_container:before' => 'border-style: {{VALUE}};',
								],	
							]
						);

						$controls->add_control(
							'outer_border_hover_width',
							[
								'label' => _x( 'Border Width', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::DIMENSIONS,
								'selectors' => [
									'{{WRAPPER}}:hover .cws_service_icon_container:before' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
								],
								'condition' => [
									'outer_border_hover!' => '',
								],	
							]
						);					

						$controls->add_control(
							'outer_border_hover_color',
							[
								'label' => _x( 'Border Color', 'Border Control', 'cryptop' ),
								'type' => Controls_Manager::COLOR,
								'selectors' => [
									'{{WRAPPER}}:hover .cws_service_icon_container:before' => 'border-color: {{VALUE}};',
								],
								'condition' => [
									'outer_border_hover!' => '',
								],
							]
						);

					$controls->end_popover();

					$controls->add_control(
						'background_color_hover',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => $theme_colors_first_color,
							'selectors' => [
								'{{WRAPPER}}:hover .cws_service_icon_container:before' => 'background-color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'icon_color_hover',
						[
							'label' => esc_html__( 'Icon color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'value' => '#fff',
							'default' => '#fff',
							'selectors' => [
								'{{WRAPPER}}:hover .cws_service_icon_container .cws_icon' => 'color: {{VALUE}}',
							],				
						]
					);					

					$controls->add_control(
						'hover_animation',
						[
							'label' => esc_html__( 'Hover Animation', 'cryptop' ),
							'type' => Controls_Manager::HOVER_ANIMATION,
						]
					);

				$controls->end_controls_tab();

			$controls->end_controls_tabs();

		$controls->end_controls_section();
	}

	public function render_content() {
		//Edit buttons
		if ( Plugin::$instance->editor->is_edit_mode() ) {
			$this->render_edit_tools();
		}

		$classes = '';

		if ($this->get_data('widgetType') == 'cws_services'){

			if ($this->get_settings('side_line_divider') == 'yes'){
				$classes .= ' side_divider';
			}

			$classes .= $this->get_settings('hover_block_animation') == 'yes' ? ' elementor-block-animation' : '';

			if (!empty($this->get_settings('side_lines'))){
				$classes .= ' side_'.$this->get_settings('side_lines');
			}

			if ($this->get_settings('bottom_line_divider') == 'yes' && $this->get_settings('icon_aligning') == 'left'){
				$classes .= ' bottom_divider';
			}
		}

		echo "<div class='elementor-widget-container".esc_attr($classes)."'>";

			ob_start();
				$skin = $this->get_current_skin();
				if ( $skin ) {
					$skin->set_parent( $this );
					$skin->render();
				} else {
					$this->render();
				}
			$widget_content = ob_get_clean();
			$widget_content = apply_filters( 'elementor/widget/render_content', $widget_content, $this );
			echo sprintf('%s', $widget_content);
			
		echo "</div>";
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;

		$this->add_inline_editing_attributes( 'description', 'none' );
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_services', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {}
	
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Services() );