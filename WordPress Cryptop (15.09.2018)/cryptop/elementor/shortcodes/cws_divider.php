<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Divider
class CWS_Elementor_Divider extends Widget_Base {
	public function get_name() {
		return 'divider';
	}

	public function get_title() {
		return esc_html__( 'CWS Divider', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-divider';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Add extra section
		$sections->icons(false);

		$controls->start_controls_section(
			'section_image',
			[
				'label' => esc_html__( 'Image', 'cryptop' ),
			]
		);

		$controls->add_control(
			'icon_img',
			[
				'label' => esc_html__( 'Image Instead of Icon', 'cryptop' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => '',
				],
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_divider',
			[
				'label' => esc_html__( 'Divider', 'cryptop' ),
			]
		);

		$controls->add_control(
			'style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'options' => [
					'solid' => esc_html__( 'Solid', 'cryptop' ),
					'double' => esc_html__( 'Double', 'cryptop' ),
					'dotted' => esc_html__( 'Dotted', 'cryptop' ),
					'dashed' => esc_html__( 'Dashed', 'cryptop' ),
				],
				'default' => 'solid',
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator .left-divider-part' => 'border-top-style: {{UNIT}};',
					'{{WRAPPER}} .elementor-divider-separator .right-divider-part' => 'border-top-style: {{UNIT}};',
				],
			]
		);

		$controls->add_control(
			'weight',
			[
				'label' => esc_html__( 'Weight', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 2,
					'unit' => 'px',
				],
				'range' => [
					'px' => [
						'min' => 1,
						'max' => 10,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator .left-divider-part' => 'border-top-width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-divider-separator .right-divider-part' => 'border-top-width: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-divider-separator.has_bg:before' => 'height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$controls->add_control(
			'color',
			[
				'label' => esc_html__( 'Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => $theme_colors_first_color,
				'value' => $theme_colors_first_color,
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator .left-divider-part' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .elementor-divider-separator .right-divider-part' => 'border-color: {{VALUE}};',
					'{{WRAPPER}} .elementor-divider-separator i' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->add_responsive_control(
			'i-size',
			[
				'label' => esc_html__( 'Icon Size', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'max' => 200,
					],
				],
				'default' => [
					'size' => 20,
					'unit' => 'px',
				],
				'tablet_default' => [
					'unit' => 'px',
				],
				'mobile_default' => [
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator span i' => 'font-size: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} .elementor-divider-separator .icon_image' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
				],
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'icon_fontawesome',
							'operator' => '!=',
							'value' => ''
						], [
							'name' => 'icon_flaticons',
							'operator' => '!=',
							'value' => ''
						], [
							'name' => 'icon_img[url]',
							'operator' => '!=',
							'value' => ''
						]
					]
				],
			]
		);

		$controls->add_responsive_control(
			'i-spacings',
			[
				'label' => esc_html__( 'Icon Spacings', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'max' => 200,
					],
				],
				'default' => [
					'size' => 15,
					'unit' => 'px',
				],
				'tablet_default' => [
					'unit' => 'px',
				],
				'mobile_default' => [
					'unit' => 'px',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator .icon_wrapper' => 'margin: 0px {{SIZE}}{{UNIT}};',
				],
				'conditions' => [
					'relation' => 'or',
					'terms' => [
						[
							'name' => 'icon_fontawesome',
							'operator' => '!=',
							'value' => ''
						], [
							'name' => 'icon_flaticons',
							'operator' => '!=',
							'value' => ''
						], [
							'name' => 'icon_img[url]',
							'operator' => '!=',
							'value' => ''
						]
					]
				],
			]
		);

		$controls->add_control(
			'background_line',
			[
				'label' => esc_html__( 'Add Background Line', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'name' => 'icon_fontawesome',
							'operator' => '==',
							'value' => ''
						], [
							'name' => 'icon_flaticons',
							'operator' => '==',
							'value' => ''
						], [
							'name' => 'icon_img[url]',
							'operator' => '==',
							'value' => ''
						]
					]
				],
			]
		);

		$controls->add_control(
			'background_line_color',
			[
				'label' => esc_html__( 'Background Line Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'default' => '#e3e3e3',
				'value' => '#e3e3e3',
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator.has_bg:before' => 'background-color: {{VALUE}};',
				],
				'condition' => [
					'background_line' => 'yes'
				]
			]
		);

		$controls->add_responsive_control(
			'width',
			[
				'label' => esc_html__( 'Width', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => ['px'],
				'range' => [
					'px' => [
						'max' => 3000,
					],
				],
				'default' => [
					'size' => 35,
					'unit' => 'px',
				],
				'tablet_default' => [
					'unit' => '%',
				],
				'mobile_default' => [
					'unit' => '%',
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator .left-divider-part' => 'width: calc({{SIZE}}{{UNIT}} / 2);',
					'{{WRAPPER}} .elementor-divider-separator .right-divider-part' => 'width: calc({{SIZE}}{{UNIT}} / 2);',
				],
			]
		);

		$controls->add_responsive_control(
			'align',
			[
				'label' => esc_html__( 'Alignment', 'cryptop' ),
				'type' => Controls_Manager::CHOOSE,
				'options' => [
					'flex-start' => [
						'title' => esc_html__( 'Left', 'cryptop' ),
						'icon' => 'fa fa-align-left',
					],
					'center' => [
						'title' => esc_html__( 'Center', 'cryptop' ),
						'icon' => 'fa fa-align-center',
					],
					'flex-end' => [
						'title' => esc_html__( 'Right', 'cryptop' ),
						'icon' => 'fa fa-align-right',
					],
				],
				'toggle' => false,
				'default' => '',
				'selectors' => [
					'{{WRAPPER}} .elementor-divider-separator' => '-webkti-justify-content: {{VALUE}}; -moz-justify-content: {{VALUE}}; -ms-justify-content: {{VALUE}}; justify-content: {{VALUE}};',
				],
			]
		);

		$controls->add_responsive_control(
			'gap',
			[
				'label' => esc_html__( 'Gap', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'default' => [
					'size' => 15,
				],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-divider' => 'margin-top: {{SIZE}}{{UNIT}}; margin-bottom: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$controls->add_control(
			'view',
			[
				'label' => esc_html__( 'View', 'cryptop' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_divider', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_divider', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Divider() );