<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Text
class CWS_Elementor_Text extends Widget_Base {
	public function get_name() {
		return 'cws_text';
	}

	public function get_title() {
		return esc_html__( 'CWS Text', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-align-left';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		//Add extra section
		$sections->icons();

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Some facts', 'cryptop' ),
					'title' => esc_html__( 'Title', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .title_wrapper .widgettitle',
					'label'	=> esc_html__( 'Title Typography', 'cryptop' ),
					'render_type' => 'template',
					'condition' => [
						'title!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 50,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h2',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 40,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h2',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 32,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h2',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'subtitle',
				[
					'label' => esc_html__( 'Subtitle', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Why People Choose Us', 'cryptop' ),
					'title' => esc_html__( 'Title', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'subtitle_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .widgetsubtitle',
					'label'	=> 'Subtitle Typography',
					'condition' => [
						'subtitle!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 22,
							'unit' => 'px'
						],
						'text_transform' => 'Capitalize',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 20,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 17,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'add_divider',
				[
					'label' => esc_html__( 'Add divider', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'conditions' => [
						'relation' => 'or',
						'terms' => [
							[
								'name' => 'title',
								'operator' => '!=',
								'value' => ''
							], [
								'name' => 'subtitle',
								'operator' => '!=',
								'value' => ''
							]
						]
					]
				]
			);

			$controls->add_responsive_control(
				'text_alignment',
				[
					'label' => esc_html__( 'Text Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'tablet_default' => 'left',
					'mobile_default' => 'center',
					'toggle' => false,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .cws_textmodule .cws_textmodule_content_wrapper' => 'text-align: {{VALUE}}',
					],
				]
			);

			$controls->add_responsive_control(
				'icon_position',
				[
					'label' => esc_html__( 'Icon Position', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,
					'options' => [
						'row' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'row-reverse' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .cws_textmodule.has_icon .cws_textmodule_content_wrapper .title_wrapper .flex-title' => 'flex-direction: {{VALUE}}; -ms-flex-direction: {{VALUE}}; -moz-flex-direction: {{VALUE}}; -webkit-flex-direction: {{VALUE}};',
					],
					'conditions' => [
						'relation' => 'or',
						'terms' => [
							[
								'name' => 'icon_fontawesome',
								'operator' => '!=',
								'value' => ''
							], [
								'name' => 'icon_flaticons',
								'operator' => '!=',
								'value' => ''
							], [
								'name' => 'icon_svg',
								'operator' => '!=',
								'value' => ''
							]
						]
					]
				]
			);

			$controls->add_control(
				'size',
				[
					'label' => esc_html__( 'Size Icon', 'cryptop' ),
					'type' => Controls_Manager::SELECT,				
					'options' => [
						'3x' => esc_html__( 'Medium', 'cryptop' ),
						'2x' => esc_html__( 'Small', 'cryptop' ),
						'lg' => esc_html__( 'Mini', 'cryptop' ),
						'4x' => esc_html__( 'Large', 'cryptop' ),
						'5x' => esc_html__( 'Extra Large', 'cryptop' ),
					],			
					'default' => '3x',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_lib',
										'operator' => '!=',
										'value' => 'svg'
									]
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					]
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'content_typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_1,
					'selector' => '{{WRAPPER}} .cws_textmodule_content',
					'label'	=> esc_html__('Content Typography', 'cryptop'),
					'condition' => [
						'content!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'div',
						'line_height' => [
							'size' => 1.4,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 18,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.4,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.4,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'exclude' => ['html_tag']
				]
			);	

			$controls->add_control(
				'content',
				[
					'label' => esc_html__( 'Text', 'cryptop' ),
					'type' => Controls_Manager::WYSIWYG,
					'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
					'default' => esc_html__( 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab soluta eum magnam tenetur quis, similique nostrum perferendis cumque ut, officia iusto ad voluptatem explicabo unde sint praesentium delectus aut. Nemo.', 'cryptop' ),
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'customize_icon_size',
				[
					'label' => esc_html__( 'Customize Icon Size', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_lib',
										'operator' => '!=',
										'value' => 'svg'
									],
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					],
				]
			);

			$controls->add_control(
				'size_i',
				[
					'label' => esc_html__( 'Icon Size(px)', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'min' => 1,
					'max' => 100,
					'step' => 1,
					'default' => 40,
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'icon_lib',
										'operator' => '!=',
										'value' => 'svg'
									], [
										'name' => 'customize_icon_size',
										'operator' => '==',
										'value' => 'yes'
									],
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					],
					'selectors' => [
						'{{WRAPPER}} .cws_service_icon:before' => 'font-size: {{VALUE}}px',
					],
				]
			);

			$controls->add_control(
				'customize_margins',
				[
					'label' => esc_html__( 'Customize Margins', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_responsive_control(
				'subtitle_margins',
				[
					'label' => esc_html__( 'Subtitle Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .widgetsubtitle' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_margins',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'subtitle',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);

			$controls->add_responsive_control(
				'icon_margins',
				[
					'label' => esc_html__( 'Icon Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'customize_margins',
										'operator' => '==',
										'value' => 'yes'
									]
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					],
				]
			);

			$controls->add_responsive_control(
				'title_margins',
				[
					'label' => esc_html__( 'Title Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .title_wrapper' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'top' => 15,
						'right' => 0,
						'bottom' => 5,
						'left' => 0
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_margins',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'title',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);
			
			$controls->add_responsive_control(
				'content_margins',
				[
					'label' => esc_html__( 'Content Margins', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%', 'em' ],
					'selectors' => [
						'{{WRAPPER}} .cws_textmodule_content' => 'margin: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'default' => [
						'top' => 25,
						'right' => 0,
						'bottom' => 0,
						'left' => 0
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_margins',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'content',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);

			
			$controls->add_control(
				'customize_colors',
				[
					'label' => esc_html__( 'Customize Colors', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'custom_subtitle_color',
				[
					'label' => esc_html__( 'Subtitle Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => $theme_colors_first_color,
					'value' => $theme_colors_first_color,
					'selectors' => [
						'{{WRAPPER}} .widgetsubtitle' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'subtitle',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);			

			$controls->add_control(
				'custom_icon_color',
				[
					'label' => esc_html__( 'Icon Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => "#000",
					'value' => "#000",
					'selectors' => [
						'{{WRAPPER}} .cws_textmodule_icon_wrapper' => 'color: {{VALUE}}',
						'{{WRAPPER}} .cws_textmodule_icon_wrapper i' => 'fill: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'relation' => 'and',
								'terms' => [
									[
										'name' => 'customize_colors',
										'operator' => '==',
										'value' => 'yes'
									]
								]
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'icon_fontawesome',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'icon_flaticons',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						]
					]
				]
			);

			$controls->add_control(
				'custom_title_color',
				[
					'label' => esc_html__( 'Title Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => "#000",
					'value' => "#000",
					'selectors' => [
						'{{WRAPPER}} .widgettitle' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'title',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);

			$controls->add_control(
				'custom_divider_color',
				[
					'label' => esc_html__( 'Divider Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => $theme_colors_first_color,
					'value' => $theme_colors_first_color,
					'selectors' => [
						'{{WRAPPER}} .text-divider' => 'background-color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'add_divider',
								'operator' => '==',
								'value' => 'yes',
							],
						],
					],
				]
			);

			$controls->add_control(
				'custom_font_color',
				[
					'label' => esc_html__( 'Font Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => "#000",
					'value' => "#000",
					'selectors' => [
						'{{WRAPPER}}' => 'color: {{VALUE}}',
					],
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'content',
								'operator' => '!=',
								'value' => '',
							],
						],
					],
				]
			);	


		$controls->end_controls_section();
	}
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_text', $this, 'php'));
	}
	protected function content_template() {}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Text() );