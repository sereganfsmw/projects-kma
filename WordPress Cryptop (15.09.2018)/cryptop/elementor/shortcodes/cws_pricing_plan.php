<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Pricing plan
class CWS_Elementor_Pricing_plan extends Widget_Base {
	public function get_name() {
		return 'cws_pricing_plan';
	}

	public function get_title() {
		return esc_html__( 'CWS Pricing plan', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-dollar';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );
		$second_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['second_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'General', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

			$controls->add_control(
				'title',
				[
					'label' => esc_html__( 'Title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Premium', 'cryptop' ),
					'title' => esc_html__( 'Title', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'title_typography',
					'selector' => '{{WRAPPER}} .cws_pricing_plan_headers .cws_pricing_plan_title',
					'label'	=> esc_html__( 'Title Typography', 'cryptop' ),
					'render_type' => 'template',
					'condition' => [
						'title!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 30,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h3',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 30,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h3',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 30,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h3',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'subtitle',
				[
					'label' => esc_html__( 'Subtitle', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Plan', 'cryptop' ),
					'title' => esc_html__( 'Subtitle', 'cryptop' ),
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'subtitles_typography',
					'selector' => '{{WRAPPER}} .cws_pricing_plan_subtitle',
					'label'	=> esc_html__( 'Subtitle Typography', 'cryptop' ),
					'render_type' => 'template',
					'condition' => [
						'subtitle!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 16,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'h4',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
				]
			);

			$controls->add_control(
				'add_extra_button',
				[
					'label' => esc_html__( 'Add extra button', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

			$controls->add_control(
				'extra_button_title',
				[
					'label' => esc_html__( 'Extra Button title', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'free setup', 'cryptop' ),
					'title' => esc_html__( 'Button title', 'cryptop' ),
					'condition' => [
						'add_extra_button' => 'yes'
					],					
				]
			);

			$controls->add_control(
				'extra_button_url',
				[
					'label' => esc_html__( 'Extra Button Url', 'cryptop' ),
					'type' => Controls_Manager::URL,
					'placeholder' => esc_html__( 'http://', 'cryptop' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					],
					'condition' => [
						'add_extra_button' => 'yes'
					],						
				]
			);

			$controls->add_control(
				'price_subtitle',
				[
					'label' => esc_html__( 'Price Subtitle', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Min Price', 'cryptop' ),
					'title' => esc_html__( 'Price Subtitle', 'cryptop' ),
				]
			);

			$controls->add_control(
				'price',
				[
					'label' => esc_html__( 'Price', 'cryptop' ),
					'type' => Controls_Manager::NUMBER,				
					'step' => 1,
					'default' => 7.50,
				]
			);

			$controls->add_group_control(
				CWS_Group_Control_Typography::get_type(),
				[
					'name' => 'price_typography',
					'selector' => '{{WRAPPER}} .cws_pricing_plan_price_wrapper .cws_pricing_plan_price',
					'label'	=> esc_html__( 'Price Typography', 'cryptop' ),
					'render_type' => 'template',
					'condition' => [
						'price!' => ''
					],
					'defaults' => [
						'font_size' => [
							'size' => 45,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'tablet_defaults' => [
						'font_size' => [
							'size' => 45,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'mobile_defaults' => [
						'font_size' => [
							'size' => 45,
							'unit' => 'px'
						],
						'text_transform' => 'none',
						'font_style' => 'normal',
						'font_weight' => '400',
						'text_decoration' => 'none',
						'html_tag' => 'p',
						'line_height' => [
							'size' => 1.2,
							'unit' => 'em'
						],
						'letter_spacing' => [
							'size' => 0,
							'unit' => 'px'
						],
					],
					'exclude' => ['html_tag']
				]
			);			

			$controls->add_control(
				'price_currency',
				[
					'label' => esc_html__( 'Currency', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => '$',
					'title' => esc_html__( 'Currency', 'cryptop' ),
					'condition' => [
						'price!' => ''
					],
				]
			);

			$controls->add_control(
				'price_description',
				[
					'label' => esc_html__( 'Price description', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'per 10GH/s', 'cryptop' ),
					'title' => esc_html__( 'Price description', 'cryptop' ),
				]
			);

			$controls->add_control(
				'content_subtitle',
				[
					'label' => esc_html__( 'Content Subtitle', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Expiry', 'cryptop' ),
					'title' => esc_html__( 'Content Subtitle', 'cryptop' ),
				]
			);

			$controls->add_control(
				'content',
				[
					'label' => esc_html__( 'Content', 'cryptop' ),
					'type' => Controls_Manager::TEXTAREA,
					'rows' => 10,
					'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
					'default' => esc_html__( '1 year', 'cryptop' ),
				]
			);	

			$controls->add_responsive_control(
				'align',
				[
					'label' => esc_html__( 'Align', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
					'toggle' => false,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'None', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}} .cws_pricing_plan_wrapper .cws_pricing_plan_item' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'button_text',
				[
					'label' => esc_html__( 'Button caption', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Purchase Plan', 'cryptop' ),
					'title' => esc_html__( 'Button caption', 'cryptop' ),
				]
			);

			$controls->add_control(
				'button_url',
				[
					'label' => esc_html__( 'Url', 'cryptop' ),
					'type' => Controls_Manager::URL,
					'placeholder' => esc_html__( 'http://', 'cryptop' ),
					'show_external' => true,
					'default' => [
						'url' => '',
						'is_external' => true,
						'nofollow' => true,
					],
					'condition' => [
						'button_text!' => ''
					],						
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'customize_colors',
				[
					'label' => esc_html__( 'Customize Colors', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'box_shadow_color',
				[
					'label' => esc_html__( 'Shadow Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'value' => $first_color,
					'default' => $first_color,
					'selectors' => [
						'{{WRAPPER}}:hover .elementor-widget-container:before' => 'box-shadow: 0px 0px 60px 0px {{VALUE}}',
					],			
				]
			);

			/*------------------------TITLE POPOVER------------------------*/
			$controls->add_control(
				'title_popover',
				[
					'label' => esc_html__( 'Title Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'title',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'title_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_headers .cws_pricing_plan_title' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'title_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_headers .cws_pricing_plan_title' => 'color: {{VALUE}}',
						],			
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------SUBTITLE POPOVER------------------------*/
			$controls->add_control(
				'subtitle_popover',
				[
					'label' => esc_html__( 'Subtitle Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'relation' => 'or',
								'terms' => [
									[
										'name' => 'subtitle',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'price_subtitle',
										'operator' => '!=',
										'value' => ''
									], [
										'name' => 'content_subtitle',
										'operator' => '!=',
										'value' => ''
									]
								]
							]
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'subtitle_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#898989',
						'default' => '#898989',
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_subtitle' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'subtitle_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $second_color,
						'default' => $second_color,
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_subtitle' => 'color: {{VALUE}}',
						],			
					]
				);				
			
			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------PRICE POPOVER------------------------*/
			$controls->add_control(
				'price_popover',
				[
					'label' => esc_html__( 'Price Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'price',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'price_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_price_wrapper .cws_pricing_plan_price' => 'color: {{VALUE}}',
						],				
					]
				);

				$controls->add_control(
					'price_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_price_wrapper .cws_pricing_plan_price' => 'color: {{VALUE}}',
						],			
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------DESCRIPTION POPOVER------------------------*/
			$controls->add_control(
				'description_popover',
				[
					'label' => esc_html__( 'Description Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'price_description',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'description_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_price_wrapper .cws_pricing_plan_price_description' => 'color: {{VALUE}}',
						],					
					]
				);

				$controls->add_control(
					'description_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_price_wrapper .cws_pricing_plan_price_description' => 'color: {{VALUE}}',
						],				
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			/*------------------------CONTENT POPOVER------------------------*/
			$controls->add_control(
				'content_popover',
				[
					'label' => esc_html__( 'Content Color', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'conditions' => [
						'relation' => 'and',
						'terms' => [
							[
								'name' => 'customize_colors',
								'operator' => '==',
								'value' => 'yes',
							], [
								'name' => 'content',
								'operator' => '!=',
								'value' => '',
							],
						],
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'content_color',
					[
						'label' => esc_html__( 'Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_content_wrapper .cws_pricing_plan_content' => 'color: {{VALUE}}',
						],				
					]
				);

				$controls->add_control(
					'content_color_hover',
					[
						'label' => esc_html__( 'Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_content_wrapper .cws_pricing_plan_content' => 'color: {{VALUE}}',
						],				
					]
				);				

			$controls->end_popover();
			/*------------------------------------------------*/

			$controls->add_control(
				'customize_buttons',
				[
					'label' => esc_html__( 'Customize Buttons', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => 'yes',
				]
			);

			$controls->add_control(
				'extra_button_color',
				[
					'label' => esc_html__( 'Extra button', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'condition' => [
						'customize_buttons' => 'yes'
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'extra_button_color_font',
					[
						'label' => esc_html__( 'Button Font Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000',
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_extra_button' => 'color: {{VALUE}}',
						],			
					]
				);	

				$controls->add_control(
					'extra_button_color_font_hover',
					[
						'label' => esc_html__( 'Button Font Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_extra_button:hover' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'extra_button_color_background',
					[
						'label' => esc_html__( 'Button Background Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $second_color,
						'default' => $second_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_extra_button' => 'background-color: {{VALUE}}',
						],			
					]
				);	

				$controls->add_control(
					'extra_button_color_background_hover',
					[
						'label' => esc_html__( 'Button Background Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_extra_button:hover' => 'background-color: {{VALUE}}',
						],			
					]
				);							

			$controls->end_popover();

			$controls->add_control(
				'button_color',
				[
					'label' => esc_html__( 'Button', 'cryptop' ),
					'type' => Controls_Manager::POPOVER_TOGGLE,
					'label_off' => esc_html__( 'Default', 'cryptop' ),
					'label_on' => esc_html__( 'Custom', 'cryptop' ),
					'return_value' => 'yes',
					'condition' => [
						'customize_buttons' => 'yes'
					],					
				]
			);

			$controls->start_popover();

				$controls->add_control(
					'button_color_font',
					[
						'label' => esc_html__( 'Button Font Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_main_button' => 'color: {{VALUE}}',
						],			
					]
				);	

				$controls->add_control(
					'button_color_font_hover',
					[
						'label' => esc_html__( 'Button Font Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_main_button:hover' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'button_color_font_block_hover',
					[
						'label' => esc_html__( 'Button Font Color (Block Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000',
						'default' => '#000',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_main_button' => 'color: {{VALUE}}',
						],			
					]
				);

				$controls->add_control(
					'button_color_background',
					[
						'label' => esc_html__( 'Button Background Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $first_color,
						'default' => $first_color,
						'selectors' => [
							'{{WRAPPER}} .cws_pricing_plan_main_button' => 'background-color: {{VALUE}}; border-color: {{VALUE}};',
						],			
					]
				);	

				$controls->add_control(
					'button_color_background_hover',
					[
						'label' => esc_html__( 'Button Background Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#fff',
						'default' => '#fff',
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_main_button:hover' => 'background-color: {{VALUE}}; border-color: {{VALUE}};',
						],			
					]
				);

				$controls->add_control(
					'button_color_background_block_hover',
					[
						'label' => esc_html__( 'Button Background Color (Hover)', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => $second_color,
						'default' => $second_color,
						'selectors' => [
							'{{WRAPPER}}:hover .cws_pricing_plan_main_button' => 'background-color: {{VALUE}}; border-color: {{VALUE}};',
						],			
					]
				);							

			$controls->end_popover();

		$controls->end_controls_section();
	}

	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_pricing_plan', $this, 'php'));
	}
	protected function content_template() {}

	protected function _get_initial_config() {

		global $cws_theme_funcs;
		$first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		$config = [
			'widget_type' => $this->get_name(),
			'keywords' => $this->get_keywords(),
			'categories' => $this->get_categories(),
		];

		$out = array_merge( parent::_get_initial_config(), $config );

		//Default background for cws-banner
		$out['controls']['_background_background']['default'] = 'classic';
		$out['controls']['_background_color']['default'] = '#FFFFFF';
		$out['controls']['_background_color']['value'] = '#FFFFFF';

		$out['controls']['_background_hover_background']['default'] = 'classic';
		$out['controls']['_background_hover_color']['default'] = $first_color;
		$out['controls']['_background_hover_color']['value'] = $first_color;

		$out['controls']['_background_hover_transition']['default']['size'] = '0.2';

		//Default paddings for cws-banner
		$out['controls']['_padding']['default'] = [
			'unit' => 'px',
			'top' => '40',
			'right' => '40',
			'bottom' => '40',
			'left' => '40',
			'isLinked' => false
		];

		return $out;
	}

	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Pricing_plan() );