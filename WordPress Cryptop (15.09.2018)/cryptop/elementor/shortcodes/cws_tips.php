<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Tips
class CWS_Elementor_Tips extends Widget_Base {
	public function get_name() {
		return 'cws_tips';
	}

	public function get_title() {
		return esc_html__( 'CWS Tips', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-dot-circle-o';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_general',
			[
				'label' => esc_html__( 'Tips', 'cryptop' ),
			]
		);

		//Fill array with icons
		$ficons = cws_get_all_flaticon_icons();
		$fl_icons = [];
		$fl_icons_include = [];

		foreach ($ficons as $key => $value) {
			$fl_icons['flaticon-'.$value] = $value;
			$fl_icons_include[] = 'flaticon-'.$value;
		}
		//--Fill array with icons

		$controls->add_control(
			'tips_img',
			[
				'label' => esc_html__( 'Image', 'cryptop' ),
				'type' => Controls_Manager::MEDIA,
				'default' => [
					'url' => Utils::get_placeholder_image_src(),
				],
			]
		);

		$controls->add_control(
			'tips_list',
			[
				'label' => '',
				'type' => Controls_Manager::REPEATER,
				'title_field' => '<i class="{{ icon }}" aria-hidden="true"></i> {{{ title }}}',
				//'title_field' => '<i class="{{ social }}"></i> {{{ social.replace( \'fa fa-\', \'\' ).replace( \'-\', \' \' ).replace( /\b\w/g, function( letter ){ return letter.toUpperCase() } ) }}}',
				'default' => [
					[
						'text' => esc_html__( 'List Item #1', 'cryptop' ),
						'icon' => 'fa fa-check',
					],
					[
						'text' => esc_html__( 'List Item #2', 'cryptop' ),
						'icon' => 'fa fa-times',
					],
					[
						'text' => esc_html__( 'List Item #3', 'cryptop' ),
						'icon' => 'fa fa-dot-circle-o',
					],
				],
				'fields' => [
					[
						'name' => 'tips_style',
						'label' => esc_html__( 'Tips style', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => 'dots',
						'options' => [
							'dots' => esc_html__( 'Dots', 'cryptop' ),
							'number' => esc_html__( 'Number', 'cryptop' ),
							'icon' => esc_html__( 'Icon', 'cryptop' ),
						],
					],
					[
						'name' => 'title',
						'label' => esc_html__( 'Tips title', 'cryptop' ),
						'type' => Controls_Manager::TEXT,
						'label_block' => true,
						'placeholder' => esc_html__( 'Enter title here', 'cryptop' ),
						'default' => esc_html__( 'Title', 'cryptop' ),
					],				
					[
						'name' => 'text',
						'label' => esc_html__( 'Tips content', 'cryptop' ),
						'rows' => 10,
						'type' => Controls_Manager::TEXTAREA,
						'placeholder' => esc_html__( 'Type your text here', 'cryptop' ),
					],
					[
						'name' => 'tip-spacings',
						'label' => esc_html__( 'Tip Spacing', 'cryptop' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}}.hotspot-item .tip' => 'margin: {{SIZE}}{{UNIT}}',
						],
						'default' => [
							'unit' => 'px',
							'size' => 10,
						],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
							],
						],
					],		
					[
						'name' => 'x_coord',
						'label' => esc_html__( 'X Cordinates', 'cryptop' ),
						'description'	=> esc_html__( 'In Percents', 'cryptop' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ '%' ],
						'label_block' => true,
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}}.hotspot-item' => 'left: {{SIZE}}{{UNIT}}',
						],
						'default' => [
							'unit' => '%',
							'size' => 50,
						],
						'range' => [
							'%' => [
								'min' => 0,
								'max' => 100,
							],
						],
					],					
					[
						'name' => 'y_coord',
						'label' => esc_html__( 'Y Cordinates', 'cryptop' ),
						'description'	=> esc_html__( 'In Percents', 'cryptop' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ '%' ],
						'label_block' => true,
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}}.hotspot-item' => 'top: {{SIZE}}{{UNIT}}',
						],
						'default' => [
							'unit' => '%',
							'size' => 50,
						],
						'range' => [
							'%' => [
								'min' => 0,
								'max' => 100,
							],
						],
					],
					[
						'name' => 'icon_lib',
						'label' => esc_html__( 'Icon library', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'default' => 'fontawesome',
						'options' => [
							'fontawesome' => esc_html__( 'Font Awesome', 'cryptop' ),
							'flaticons' => esc_html__( 'CWS Flaticons', 'cryptop' ),
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'tips_style',
									'operator' => '==',
									'value' => 'icon',
								],
							],
						],
						'label_block' => true,
					],
					[
						'name' => 'icon',
						'label' => esc_html__( 'Font Awesome', 'cryptop' ),
						'type' => Controls_Manager::ICON,
						'label_block' => true,
						'default' => 'fa fa-check',
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'tips_style',
									'operator' => '==',
									'value' => 'icon',
								], [
									'name' => 'icon_lib',
									'operator' => '==',
									'value' => 'fontawesome',
								],
							],
						],
					],
					[
						'name' => 'icon',
						'label' => esc_html__( 'CWS Flaticons', 'cryptop' ),
						'type' => Controls_Manager::ICON,
						'label_block' => true,
						'options' => $fl_icons,
						'include' => $fl_icons_include,
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'tips_style',
									'operator' => '==',
									'value' => 'icon',
								], [
									'name' => 'icon_lib',
									'operator' => '==',
									'value' => 'flaticons',
								],
							],
						],
					],
					[
						'name' => 'icon_color',
						'label' => esc_html__( 'Tip Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'value' => '#000000',
						'default' => '#000000',
						'selectors' => [
							'{{WRAPPER}} .elementor-widget-container .cwstooltip-wrapper .cws-hotspots {{CURRENT_ITEM}}.hotspot-item .tip i' => 'color: {{VALUE}}',
							'{{WRAPPER}} .elementor-widget-container .cwstooltip-wrapper .cws-hotspots {{CURRENT_ITEM}}.hotspot-item .tip i:after' => 'background-color: {{VALUE}}',
						],	
					],
					[
						'name' => 'icon_size',
						'label' => esc_html__( 'Tip Size', 'cryptop' ),
						'type' => Controls_Manager::SLIDER,
						'size_units' => [ 'px' ],
						'selectors' => [
							'{{WRAPPER}} .elementor-widget-container .cwstooltip-wrapper .cws-hotspots {{CURRENT_ITEM}}.hotspot-item .tip i' => 'font-size: {{SIZE}}{{UNIT}}; line-height: {{SIZE}}{{UNIT}}; width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
							'{{WRAPPER}} .elementor-widget-container .cwstooltip-wrapper .cws-hotspots {{CURRENT_ITEM}}.hotspot-item .tip i:after' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
						],
						'default' => [
							'unit' => 'px',
							'size' => 20,
						],
						'range' => [
							'px' => [
								'min' => 0,
								'max' => 100,
							],
						],
					],
					[
						'name' => 'vertical_align',
						'label' => esc_html__( 'Vertical Alignment', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'options' => [
							'top' => [
								'title' => esc_html__( 'Top', 'cryptop' ),
								'icon' => 'fa fa-chevron-up',
							],
							'bottom' => [
								'title' => esc_html__( 'Bottom', 'cryptop' ),
								'icon' => 'fa fa-chevron-down',
							],
						],
					],
					[
						'name' => 'horizontal_align',
						'label' => esc_html__( 'Horizontal Alignment', 'cryptop' ),
						'type' => Controls_Manager::CHOOSE,
						'options' => [
							'left' => [
								'title' => esc_html__( 'Left', 'cryptop' ),
								'icon' => 'fa fa-chevron-left',
							],
							'right' => [
								'title' => esc_html__( 'Right', 'cryptop' ),
								'icon' => 'fa fa-chevron-right',
							],
						],
					],
					[
						'name' => 'link',
						'label' => esc_html__( 'Link', 'cryptop' ),
						'type' => Controls_Manager::URL,
						'label_block' => true,
						'placeholder' => esc_html__( 'https://your-link.com', 'cryptop' ),
					],
				],
			]
		);

		$controls->add_control(
			'tips_animation',
			[
				'label' => esc_html__( 'Tooltip animation', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'fade',
				'options' => [
					'grow' => esc_html__( 'Grow', 'cryptop' ),
					'fade' => esc_html__( 'Fade', 'cryptop' ),
					'fade_slide' => esc_html__( 'Fade and Slide', 'cryptop' ),
					'swing' => esc_html__( 'Swing', 'cryptop' ),
					'slide' => esc_html__( 'Slide', 'cryptop' ),
				],
				'prefix_class' => 'animation-',	
			]
		);
	
		$controls->add_control(
			'show_tips',
			[
				'label' => esc_html__( 'Display tooltips by default', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => '',
			]
		);

		$controls->add_control(
			'hotspot_opacity',
			[
				'label' => esc_html__( 'Hotspot opacity', 'cryptop' ),
				'type' => Controls_Manager::NUMBER,				
				'min' => 0,
				'max' => 1,
				'step' => 0.1,
				'default' => 1,
				'selectors' => [
					'{{WRAPPER}} .elementor-widget-container .cwstooltip-wrapper .cws-hotspots .hotspot-item.pulse-active .tip i:after' => 'opacity: {{VALUE}}',
				],
			]
		);

		$controls->add_control(
			'tooltip_trigger',
			[
				'label' => esc_html__( 'Tooltip trigger when user', 'cryptop' ),
				'type' => Controls_Manager::CHOOSE,
				'default' => 'hover',
				'toggle' => false,
				'options' => [
					'hover'    => [
						'title' => esc_html__( 'Hover', 'cryptop' ),
						'icon' => 'fa fa-mouse-pointer',
					],
					'click' => [
						'title' => esc_html__( 'Click', 'cryptop' ),
						'icon' => 'fa fa-hand-pointer-o',
					],
				],
			]
		);		

		$controls->add_control(
			'view',
			[
				'label' => esc_html__( 'View', 'cryptop' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'pulse_animation',
			[
				'label' => esc_html__( 'Display pulse animation', 'cryptop' ),
				'type' => Controls_Manager::SWITCHER,
				'default' => 'yes',
			]
		);

		$controls->add_control(
			'pulse_color_start',
			[
				'label' => esc_html__( 'Pulse Color Start', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'value' => '#ffffff',
				'default' => '#ffffff',
				'condition' => [
					'pulse_animation' => 'yes'
				],				
			]
		);

		$controls->add_control(
			'pulse_color_end',
			[
				'label' => esc_html__( 'Pulse Color End', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'value' => '#ffffff',
				'default' => '#ffffff',
				'condition' => [
					'pulse_animation' => 'yes'
				],
			]
		);

		$controls->add_control(
			'tooltip-bg-color',
			[
				'label' => esc_html__( 'Tooltip Background Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'value' => '#ffffff',
				'default' => '#ffffff',
				'separator' => 'before',
				'selectors' => [
					'{{WRAPPER}} .hotspot-item .cws-tooltip' => 'background-color: {{VALUE}};',
				],	
			]
		);

		$controls->add_control(
			'tooltip-bg-color_hover',
			[
				'label' => esc_html__( 'Tooltip Background Color on Hover', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'value' => '#ffffff',
				'default' => '#ffffff',
				'selectors' => [
					'{{WRAPPER}} .hotspot-item:hover .cws-tooltip' => 'background-color: {{VALUE}};',
				],	
			]
		);

		$controls->add_control(
			'tooltip-font-colors',
			[
				'label' => esc_html__( 'Tooltip Font Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'value' => '#000000',
				'default' => '#000000',
				'selectors' => [
					'{{WRAPPER}} .hotspot-item .cws-tooltip' => 'color: {{VALUE}};',
				],	
			]
		);

		$controls->add_control(
			'tooltip-font-colors_hover',
			[
				'label' => esc_html__( 'Tooltip Font Color on Hover', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'value' => $theme_colors_first_color,
				'default' => $theme_colors_first_color,
				'selectors' => [
					'{{WRAPPER}} .hotspot-item:hover .cws-tooltip' => 'color: {{VALUE}};',
				],	
			]
		);

		$controls->add_control(
			'tooltip-paddings',
			[
				'label' => esc_html__( 'Tooltip Paddings', 'cryptop' ),
				'type' => Controls_Manager::DIMENSIONS,
				'size_units' => [ 'px' ],
				'separator' => 'before',
				'selectors' => [
					'{{WRAPPER}} .cws-hotspots .hotspot-item .cws-tooltip' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
				],
				'default' => [
					'unit' => 'px',
					'top' => 7,
					'right' => 10,
					'bottom' => 7,
					'left' => 10
				],
			]
		);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_tips', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_tips', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Tips() );