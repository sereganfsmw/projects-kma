<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


require_once ELEMENTOR_PATH . '/includes/controls/groups/image-size.php';
//CWS Image
class CWS_Elementor_Image extends Widget_Base {
	public function get_name() {
		return 'image';
	}

	public function get_title() {
		return esc_html__( 'CWS Image', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-insert-image';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_image',
			[
				'label' => esc_html__( 'Image', 'cryptop' ),
			]
		);

			$controls->add_control(
				'image',
				[
					'label' => esc_html__( 'Choose Image', 'cryptop' ),
					'type' => Controls_Manager::MEDIA,
					'dynamic' => [
						'active' => true,
					],
					'default' => [
						'url' => Utils::get_placeholder_image_src(),
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Image_Size::get_type(),
				[
					'name' => 'image', // Usage: `{name}_size` and `{name}_custom_dimension`, in this case `image_size` and `image_custom_dimension`.
					'default' => 'large',
					'separator' => 'none',
				]
			);

			$controls->add_responsive_control(
				'align',
				[
					'label' => esc_html__( 'Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'options' => [
						'left' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
					],
					'selectors' => [
						'{{WRAPPER}}' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'caption',
				[
					'label' => esc_html__( 'Caption', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => '',
					'placeholder' => esc_html__( 'Enter your image caption', 'cryptop' ),
				]
			);

			$controls->add_control(
				'link_to',
				[
					'label' => esc_html__( 'Link to', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'none',
					'options' => [
						'none' => esc_html__( 'None', 'cryptop' ),
						'file' => esc_html__( 'Media File', 'cryptop' ),
						'custom' => esc_html__( 'Custom URL', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'link',
				[
					'label' => esc_html__( 'Link to', 'cryptop' ),
					'type' => Controls_Manager::URL,
					'dynamic' => [
						'active' => true,
					],
					'placeholder' => esc_html__( 'https://your-link.com', 'cryptop' ),
					'condition' => [
						'link_to' => 'custom',
					],
					'show_label' => false,
				]
			);

			$controls->add_control(
				'open_lightbox',
				[
					'label' => esc_html__( 'Lightbox', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'default',
					'options' => [
						'default' => esc_html__( 'Default', 'cryptop' ),
						'yes' => esc_html__( 'Yes', 'cryptop' ),
						'no' => esc_html__( 'No', 'cryptop' ),
					],
					'condition' => [
						'link_to' => 'file',
					],
				]
			);

			$controls->add_control(
				'view',
				[
					'label' => esc_html__( 'View', 'cryptop' ),
					'type' => Controls_Manager::HIDDEN,
					'default' => 'traditional',
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style_image',
			[
				'label' => esc_html__( 'Image', 'cryptop' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_responsive_control(
				'width',
				[
					'label' => esc_html__( 'Width', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'unit' => '%',
					],
					'tablet_default' => [
						'unit' => '%',
					],
					'mobile_default' => [
						'unit' => '%',
					],
					'size_units' => [ '%', 'px', 'vw' ],
					'range' => [
						'%' => [
							'min' => 1,
							'max' => 100,
						],
						'px' => [
							'min' => 1,
							'max' => 1000,
						],
						'vw' => [
							'min' => 1,
							'max' => 100,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-image .elementor-image-wrapper' => 'width: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$controls->add_responsive_control(
				'space',
				[
					'label' => esc_html__( 'Max Width', 'cryptop' ) . ' (%)',
					'type' => Controls_Manager::SLIDER,
					'default' => [
						'unit' => '%',
					],
					'tablet_default' => [
						'unit' => '%',
					],
					'mobile_default' => [
						'unit' => '%',
					],
					'size_units' => [ '%' ],
					'range' => [
						'%' => [
							'min' => 1,
							'max' => 100,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-image .elementor-image-wrapper' => 'max-width: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$controls->add_control(
				'opacity',
				[
					'label' => esc_html__( 'Opacity', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'max' => 1,
							'min' => 0.10,
							'step' => 0.01,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-image img' => 'opacity: {{SIZE}};',
					],
				]
			);

			$controls->add_control(
				'hover_animation',
				[
					'label' => esc_html__( 'Hover Animation', 'cryptop' ),
					'type' => Controls_Manager::HOVER_ANIMATION,
				]
			);

			$controls->add_group_control(
				Group_Control_Border::get_type(),
				[
					'name' => 'image_border',
					'selector' => '{{WRAPPER}} .elementor-image img',
					'separator' => 'before',
				]
			);

			$controls->add_responsive_control(
				'image_border_radius',
				[
					'label' => esc_html__( 'Border Radius', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%' ],
					'selectors' => [
						'{{WRAPPER}} .elementor-image img' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'image_box_shadow',
					'exclude' => [
						'box_shadow_position',
					],
					'selector' => '{{WRAPPER}} .elementor-image img',
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style_overlay',
			[
				'label' => esc_html__( 'Overlay', 'cryptop' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->start_controls_tabs( 'tabs_image_overlay' );

				$controls->start_controls_tab(
					'image_overlay_normal',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),
					]
				);

					$controls->add_group_control(
						CWS_Group_Control_Gradient::get_type(),
						[
							'name' => 'overlay',
							'label' => esc_html__( 'Image overlay', 'cryptop' ),
							'types' => [ 'classic', 'gradient' ],
							'selector' => '{{WRAPPER}} .elementor-image .elementor-image-overlay',
							'defaults' => [
								'background' => 'classic',
								'color' => '',
								'color_stop' => [
									'unit' => '%',
									'size' => 0,
								],
								'color_b' => '',
								'color_b_stop' => [
									'unit' => '%',
									'size' => 100,
								],
								'gradient_type' => 'linear',
								'gradient_angle' => [
									'unit' => 'deg',
									'size' => 90,
								],
								'gradient_position' => 'center center',
							],
						]
					);

					$controls->add_control(
						'overlay_opacity',
						[
							'label' => esc_html__( 'Opacity', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'range' => [
								'px' => [
									'max' => 1,
									'min' => 0.10,
									'step' => 0.01,
								],
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,
							],								
							'selectors' => [
								'{{WRAPPER}} .elementor-image .elementor-image-overlay' => 'opacity: {{SIZE}};',
							],
						]
					);

				$controls->end_controls_tab();

				$controls->start_controls_tab(
					'image_overlay_hover',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),
					]
				);

					$controls->add_group_control(
							CWS_Group_Control_Gradient::get_type(),
							[
								'name' => 'overlay_hover',
								'label' => esc_html__( 'Image overlay', 'cryptop' ),
								'types' => [ 'classic', 'gradient' ],
								'selector' => '{{WRAPPER}} .elementor-image .elementor-image-overlay:hover',
								'defaults' => [
									'background' => 'classic',
									'color' => '',
									'color_stop' => [
										'unit' => '%',
										'size' => 0,
									],
									'color_b' => '',
									'color_b_stop' => [
										'unit' => '%',
										'size' => 100,
									],
									'gradient_type' => 'linear',
									'gradient_angle' => [
										'unit' => 'deg',
										'size' => 90,
									],
									'gradient_position' => 'center center',
								],
							]
						);

					$controls->add_control(
						'overlay_hover_opacity',
						[
							'label' => esc_html__( 'Opacity', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'range' => [
								'px' => [
									'max' => 1,
									'min' => 0.10,
									'step' => 0.01,
								],							
							],
							'default' => [
								'unit' => 'px',
								'size' => 1,
							],								
							'selectors' => [
								'{{WRAPPER}} .elementor-image .elementor-image-overlay:hover' => 'opacity: {{SIZE}};',
							],
						]
					);

					$controls->add_control(
						'overlay_hover_transition',
						[
							'label' => esc_html__( 'Transition Duration', 'cryptop' ),
							'type' => Controls_Manager::SLIDER,
							'default' => [
								'size' => 0.5,
							],
							'range' => [
								'px' => [
									'max' => 5,
									'step' => 0.1,
								],
							],
							'selectors' => [
								'{{WRAPPER}} .elementor-image .elementor-image-overlay' => 'transition: background-color {{SIZE}}s, background-image {{SIZE}}s, opacity {{SIZE}}s'
							],
						]
					);

				$controls->end_controls_tab();

			$controls->end_controls_tabs();

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style_caption',
			[
				'label' => esc_html__( 'Caption', 'cryptop' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_control(
				'caption_align',
				[
					'label' => esc_html__( 'Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'options' => [
						'left' => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
						'justify' => [
							'title' => esc_html__( 'Justified', 'cryptop' ),
							'icon' => 'fa fa-align-justify',
						],
					],
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .widget-image-caption' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'text_color',
				[
					'label' => esc_html__( 'Text Color', 'cryptop' ),
					'type' => Controls_Manager::COLOR,
					'default' => '',
					'selectors' => [
						'{{WRAPPER}} .widget-image-caption' => 'color: {{VALUE}};',
					],
					'scheme' => [
						'type' => Scheme_Color::get_type(),
						'value' => Scheme_Color::COLOR_3,
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'caption_typography',
					'selector' => '{{WRAPPER}} .widget-image-caption',
					'scheme' => Scheme_Typography::TYPOGRAPHY_3,
				]
			);

			$controls->add_responsive_control(
				'caption_space',
				[
					'label' => esc_html__( 'Spacing', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
						],
					],
					'selectors' => [
						'{{WRAPPER}} .widget-image-caption' => 'margin-top: {{SIZE}}{{UNIT}};',
					],
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_image', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_image', $this, 'js'));
	}

	public function get_link_url( $settings ) {
		if ( 'none' === $settings['link_to'] ) {
			return false;
		}

		if ( 'custom' === $settings['link_to'] ) {
			if ( empty( $settings['link']['url'] ) ) {
				return false;
			}
			return $settings['link'];
		}

		return [
			'url' => $settings['image']['url'],
		];
	}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Image() );