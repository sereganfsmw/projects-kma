<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//Icon
class CWS_Elementor_Icon extends Widget_Base {
	public function get_name() {
		return 'cws_icon';
	}

	public function get_title() {
		return esc_html__( 'CWS Icon', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'fa fa-at';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;
		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_icon',
			[
				'label' => esc_html__( 'Icon List', 'cryptop' ),
				'tab' => Controls_Manager::TAB_CONTENT,
			]
		);

		//Fill array with icons
		$ficons = cws_get_all_flaticon_icons();
		$fl_icons = [];
		$fl_icons_include = [];

		foreach ($ficons as $key => $value) {
			$fl_icons['flaticon-'.$value] = $value;
			$fl_icons_include[] = 'flaticon-'.$value;
		}
		//--Fill array with icons

		//REPEATER
		$repeater = new Repeater();
		$repeater->add_control(
			'icon_lib',
			[
				'label' => esc_html__( 'Icon library', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'fontawesome',
				'options' => [
					'fontawesome' => esc_html__( 'Font Awesome', 'cryptop' ),
					'flaticons' => esc_html__( 'CWS Flaticons', 'cryptop' ),
				],
				'label_block' => true,
			]
		);

		$repeater->add_control(
			'icon_fa',
			[
				'label' => esc_html__( 'Font Awesome', 'cryptop' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'default' => 'fa fa-check',
				'condition' => [
					'icon_lib' => 'fontawesome'
				]
			]
		);

		$repeater->add_control(
			'icon_fl',
			[
				'label' => esc_html__( 'CWS Flaticons', 'cryptop' ),
				'type' => Controls_Manager::ICON,
				'label_block' => true,
				'options' => $fl_icons,
				'include' => $fl_icons_include,
				'condition' => [
					'icon_lib' => 'flaticons'
				],
			]
		);

		$repeater->add_control(
			'icon_shape',
			[
				'label' => esc_html__( 'Icon shape', 'cryptop' ),
				'type' => Controls_Manager::SELECT,				
				'default' => 'round',
				'options' => [
					'none' => esc_html__( 'None', 'cryptop' ),
					'round' => esc_html__( 'Round', 'cryptop' ),
					'square' => esc_html__( 'Square', 'cryptop' ),
					'diamond' => esc_html__( 'Diamond', 'cryptop' ),
				],
			]
		);

		$repeater->add_control(
			'custom_icon_size',
			[
				'label' => esc_html__( 'Icon size', 'cryptop' ),
				'description' => esc_html__( 'In Pixels', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 20,
				],
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}} .cws_icon:before' => 'font-size: {{SIZE}}{{UNIT}}',
				],
				'conditions' => [
					'relation' => 'and',
					'terms' => [
						[
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_lib',
									'operator' => '!=',
									'value' => 'svg'
								]
							]
						], [
							'relation' => 'or',
							'terms' => [
								[
									'name' => 'icon_fa',
									'operator' => '!=',
									'value' => ''
								], [
									'name' => 'icon_fl',
									'operator' => '!=',
									'value' => ''
								]
							]
						]
					]
				]
			]
		);

		$repeater->add_control(
			'custom_icon_wrapper_size',
			[
				'label' => esc_html__( 'Icon Wrapper size', 'cryptop' ),
				'description' => esc_html__( 'In Pixels', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => [ 'px' ],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 80,
				],
				'condition' => [
					'icon_shape!' => 'none',
				],
				'selectors' => [
					'{{WRAPPER}} {{CURRENT_ITEM}} .icon-container:not(.shape-none)' => 'width: {{SIZE}}{{UNIT}}; height: {{SIZE}}{{UNIT}};',
					'{{WRAPPER}} {{CURRENT_ITEM}} .icon-container:not(.shape-none) a' => 'line-height: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$repeater->start_controls_tabs( 'tabs_button_style');

			$repeater->start_controls_tab(
				'tab_button_normal',
				[
					'label' => esc_html__( 'Normal', 'cryptop' ),						
				]
			);

				$repeater->add_control(
					'outer_border_enable',
					[
						'label' => esc_html__( 'Outer Border', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_shape',
									'operator' => '!=',
									'value' => 'none',
								],
							],
						],					
					]
				);

				$repeater->add_control(
					'outer_border',
					[
						'label' => _x( 'Border Type (Outer)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'options' => [
							'' => esc_html__( 'None', 'cryptop' ),
							'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
							'double' => _x( 'Double', 'Border Control', 'cryptop' ),
							'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
							'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
						],
						'condition' => [
							'outer_border_enable' => 'yes',
						],							
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'border-style: {{VALUE}};',
						],		
					]
				);

				$repeater->add_control(
					'outer_border_width',
					[
						'label' => _x( 'Border Width (Outer)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
							'{{WRAPPER}} {{CURRENT_ITEM}} .icon-container:not(.shape-none):before' => 'top: calc({{TOP}}{{UNIT}} - 1px) !important; right: calc({{RIGHT}}{{UNIT}} - 1px) !important; bottom: calc({{BOTTOM}}{{UNIT}} - 1px) !important; left: calc({{LEFT}}{{UNIT}} - 1px) !important;',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'outer_border',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'outer_border_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					]
				);					

				$repeater->add_control(
					'outer_border_color',
					[
						'label' => _x( 'Border Color (Outer)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}} {{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'border-color: {{VALUE}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'outer_border',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'outer_border_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					]
				);

				$repeater->add_control(
					'inner_border_enable',
					[
						'label' => esc_html__( 'Inner Border', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_shape',
									'operator' => '!=',
									'value' => 'none',
								],
							],
						],					
					]
				);

				$repeater->add_control(
					'inner_border',
					[
						'label' => _x( 'Border Type (Inner)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'options' => [
							'' => esc_html__( 'None', 'cryptop' ),
							'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
							'double' => _x( 'Double', 'Border Control', 'cryptop' ),
							'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
							'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
						],
						'condition' => [
							'inner_border_enable' => 'yes',
						],								
						'selectors' => [
							'{{CURRENT_ITEM}} .icon-container:not(.shape-none):before' => 'border-style: {{VALUE}};',
						],		
					]
				);

				$repeater->add_control(
					'inner_border_width',
					[
						'label' => _x( 'Border Width (Inner)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'selectors' => [
							'{{CURRENT_ITEM}} .icon-container:not(.shape-none):before' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'inner_border',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'inner_border_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					]
				);					

				$repeater->add_control(
					'inner_border_color',
					[
						'label' => _x( 'Border Color (Inner)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
							'{{CURRENT_ITEM}} .icon-container:not(.shape-none):before' => 'border-color: {{VALUE}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'inner_border',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'inner_border_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					]
				);	

				$repeater->add_control(
					'background_color',
					[
						'label' => esc_html__( 'Background Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#f2f2f2',
						'value' => '#f2f2f2',
						'selectors' => [
							'{{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'background-color: {{VALUE}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_shape',
									'operator' => '!=',
									'value' => 'none',
								],
							],
						],
					]
				);

				$repeater->add_control(
					'icon_color',
					[
						'label' => esc_html__( 'Icon Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'scheme' => [
							'type' => Scheme_Color::get_type(),
							'value' => Scheme_Color::COLOR_2,
						],
						'default' => CRYPTOP_FIRST_COLOR,					
						'selectors' => [
							'{{CURRENT_ITEM}} .cws_icon:before' => 'color: {{VALUE}}',
							'{{CURRENT_ITEM}} .cws_icon .svg' => 'fill: {{VALUE}}',
						],				
					]
				);

			$repeater->end_controls_tab();

			$repeater->start_controls_tab(
				'tab_button_hover',
				[
					'label' => esc_html__( 'Hover', 'cryptop' ),						
				]
			);

				$repeater->add_control(
					'outer_border_hover_enable',
					[
						'label' => esc_html__( 'Outer Border', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_shape',
									'operator' => '!=',
									'value' => 'none',
								],
							],
						],					
					]
				);

				$repeater->add_control(
					'outer_border_hover',
					[
						'label' => _x( 'Border Type (Outer)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'options' => [
							'' => esc_html__( 'None', 'cryptop' ),
							'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
							'double' => _x( 'Double', 'Border Control', 'cryptop' ),
							'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
							'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
						],
						'condition' => [
							'outer_border_hover_enable' => 'yes',
						],							
						'selectors' => [
							'{{WRAPPER}}:hover {{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'border-style: {{VALUE}};',
						],		
					]
				);

				$repeater->add_control(
					'outer_border_hover_width',
					[
						'label' => _x( 'Border Width (Outer)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'selectors' => [
							'{{WRAPPER}}:hover {{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
							'{{WRAPPER}}:hover {{CURRENT_ITEM}} .icon-container:not(.shape-none):before' => 'top: calc({{TOP}}{{UNIT}} - 1px) !important; right: calc({{RIGHT}}{{UNIT}} - 1px) !important; bottom: calc({{BOTTOM}}{{UNIT}} - 1px) !important; left: calc({{LEFT}}{{UNIT}} - 1px) !important;',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'outer_border_hover',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'outer_border_hover_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],	
					]
				);					

				$repeater->add_control(
					'outer_border_hover_color',
					[
						'label' => _x( 'Border Color (Outer)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
							'{{WRAPPER}}:hover {{CURRENT_ITEM}} .icon-container:not(.shape-none):after' => 'border-color: {{VALUE}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'outer_border_hover',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'outer_border_hover_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					]
				);	

				$repeater->add_control(
					'inner_border_hover_enable',
					[
						'label' => esc_html__( 'Inner Border', 'cryptop' ),
						'type' => Controls_Manager::SWITCHER,
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_shape',
									'operator' => '!=',
									'value' => 'none',
								],
							],
						],					
					]
				);

				$repeater->add_control(
					'inner_border_hover',
					[
						'label' => _x( 'Border Type (Inner)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::SELECT,
						'options' => [
							'' => esc_html__( 'None', 'cryptop' ),
							'solid' => _x( 'Solid', 'Border Control', 'cryptop' ),
							'double' => _x( 'Double', 'Border Control', 'cryptop' ),
							'dotted' => _x( 'Dotted', 'Border Control', 'cryptop' ),
							'dashed' => _x( 'Dashed', 'Border Control', 'cryptop' ),
						],
						'condition' => [
							'inner_border_hover_enable' => 'yes',
						],							
						'selectors' => [
							'{{CURRENT_ITEM}}:hover .icon-container:not(.shape-none):before' => 'border-style: {{VALUE}};',
						],	
					]
				);

				$repeater->add_control(
					'inner_border_hover_width',
					[
						'label' => _x( 'Border Width (Inner)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::DIMENSIONS,
						'selectors' => [
							'{{CURRENT_ITEM}}:hover .icon-container:not(.shape-none):before' => 'border-width: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'inner_border_hover',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'inner_border_hover_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],					
					]
				);					

				$repeater->add_control(
					'inner_border_hover_color',
					[
						'label' => _x( 'Border Color (Inner)', 'Border Control', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'selectors' => [
							'{{CURRENT_ITEM}}:hover .icon-container:not(.shape-none):before' => 'border-color: {{VALUE}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'inner_border_hover',
									'operator' => '!=',
									'value' => '',
								], [
									'name' => 'inner_border_hover_enable',
									'operator' => '==',
									'value' => 'yes',
								],
							],
						],
					]
				);

				$repeater->add_control(
					'background_color_hover',
					[
						'label' => esc_html__( 'Background Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'default' => '',
						'value' => '',
						'selectors' => [
							'{{CURRENT_ITEM}}:hover .icon-container:not(.shape-none):after' => 'background-color: {{VALUE}};',
						],
						'conditions' => [
							'relation' => 'and',
							'terms' => [
								[
									'name' => 'icon_shape',
									'operator' => '!=',
									'value' => 'none',
								],
							],
						],
					]
				);

				$repeater->add_control(
					'icon_hover_color',
					[
						'label' => esc_html__( 'Icon Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'scheme' => [
							'type' => Scheme_Color::get_type(),
							'value' => Scheme_Color::COLOR_2,
						],
						'default' => CRYPTOP_FIRST_COLOR,					
						'selectors' => [
							'{{CURRENT_ITEM}}:hover .cws_icon:before' => 'color: {{VALUE}}',
							'{{CURRENT_ITEM}}:hover .cws_icon .svg' => 'fill: {{VALUE}}',
						],				
					]
				);

				$repeater->add_control(
					'hover_animation',
					[
						'label' => esc_html__( 'Hover Animation', 'cryptop' ),
						'type' => Controls_Manager::HOVER_ANIMATION,
					]
				);

			$repeater->end_controls_tab();

		$repeater->end_controls_tabs();

		$repeater->add_control(
			'link',
			[
				'label' => esc_html__( 'Link', 'cryptop' ),
				'type' => Controls_Manager::URL,
				'placeholder' => esc_html__( 'http://', 'cryptop' ),
				'show_external' => true,
				'default' => [
					'url' => '',
					'is_external' => true,
					'nofollow' => true,
				],
			]
		);

		$repeater->add_control(
			'title',
			[
				'label' => esc_html__( 'Title', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'default' => '',
				'title' => esc_html__( 'Title', 'cryptop' ),
			]
		);

		$controls->add_control(
			'icons_list',
			[
				'label'       => esc_html__( 'Icons', 'cryptop' ),
				'type'        => Controls_Manager::REPEATER,
				'show_label'  => true,
				'default' => [
					[
						'title' => esc_html__( 'Icon #1', 'cryptop' ),
						'icon_fa' => 'fa fa-check',
					],
				],
				'fields'      => array_values( $repeater->get_controls() ),
				'title_field' => '<i class="{{ (icon_lib == "fontawesome" ? icon_fa : icon_fl) }}" aria-hidden="true"></i> {{{title}}}',
			]
		);
		//---REPEATER

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Style', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);
	
			$controls->add_control(
				'layout',
				[
					'label' => esc_html__( 'Layout', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'block',
					'options' => [
						'block' => [
							'title' => esc_html__( 'Block', 'cryptop' ),
							'icon' => 'eicon-editor-list-ul',
						],
						'inline-block' => [
							'title' => esc_html__( 'Inline', 'cryptop' ),
							'icon' => 'eicon-ellipsis-h',
						],
					],
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container .icon-wrapper' => 'display: {{VALUE}}',
					],					
					'label_block' => false,
				]
			);	

			$controls->add_control(
				'aligning',
				[
					'label' => esc_html__( 'Aligning', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'default' => 'left',
			        'options' => [
			            'left'    => [
			                'title' => esc_html__( 'Left', 'cryptop' ),
			                'icon' => 'fa fa-align-left',
			            ],
			            'center' => [
			                'title' => esc_html__( 'Center', 'cryptop' ),
			                'icon' => 'fa fa-align-center',
			            ],
			            'right' => [
			                'title' => esc_html__( 'Right', 'cryptop' ),
			                'icon' => 'fa fa-align-right',
			            ],
			        ],
			        'render_type' => 'template',
			        'selectors' => [
						'{{WRAPPER}} .elementor-widget-container' => 'text-align: {{VALUE}};',
					],
				]
			);

			$controls->add_control(
				'vertical_spacings',
				[
					'label' => esc_html__( 'Icons spacings', 'cryptop' ),
					'description' => esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 10,
					],
					'selectors' => [
						'{{WRAPPER}} .icons-wrapper .icon-wrapper' => 'margin-bottom: {{SIZE}}{{UNIT}}',
					],
					'condition' => [
						'layout' => 'block'
					]
				]
			);

			$controls->add_control(
				'horizontal_spacings',
				[
					'label' => esc_html__( 'Icons spacings', 'cryptop' ),
					'description' => esc_html__( 'In Pixels', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'size_units' => [ 'px' ],
					'range' => [
						'px' => [
							'min' => 0,
							'max' => 100,
							'step' => 1,
						],
					],
					'default' => [
						'unit' => 'px',
						'size' => 10,
					],
					'selectors' => [
						'{{WRAPPER}} .icons-wrapper .icon-wrapper' => 'margin-right: {{SIZE}}{{UNIT}}',
					],
					'condition' => [
						'layout' => 'inline-block'
					]
				]
			);

		$controls->end_controls_section();
	}

	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_icon', $this, 'php'));
	}

	//Defaults values
	protected function _get_initial_config() {
		$config = [
			'widget_type' => $this->get_name(),
			'keywords' => $this->get_keywords(),
			'categories' => $this->get_categories(),
		];

		$out = array_merge( parent::_get_initial_config(), $config );

		$out['controls']['_css_classes']['label'] = 'CSS Classes';
		$out['controls']['_css_classes']['default'] = '';
		return $out;
	}

	protected function content_template() {}
	public function render_plain_content($instance = []) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Icon() );