<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Message Box
class CWS_Elementor_Message_box extends Widget_Base {
	public function get_name() {
		return 'alert';
	}

	public function get_title() {
		return esc_html__( 'CWS Message Box', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-alert';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$icons_def = array(
			'icon_lib' => 'fontawesome',
			'icon_fontawesome' => 'fa fa-info-circle',
			'icon_flaticons' => '',
			'icon_svg' => ''
		);

		//Add extra section
		$sections->icons(false, $icons_def);

		$controls->start_controls_section(
			'section_alert',
			[
				'label' => esc_html__( 'Alert', 'cryptop' ),
			]
		);

		$controls->add_control(
			'alert_type',
			[
				'label' => esc_html__( 'Type', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'info',
				'options' => [
					'info' => esc_html__( 'Info', 'cryptop' ),
					'success' => esc_html__( 'Success', 'cryptop' ),
					'warning' => esc_html__( 'Warning', 'cryptop' ),
					'danger' => esc_html__( 'Danger', 'cryptop' ),
				],
			]
		);

		$controls->add_control(
			'alert_title',
			[
				'label' => esc_html__( 'Title & Description', 'cryptop' ),
				'type' => Controls_Manager::TEXT,
				'placeholder' => esc_html__( 'Enter your title', 'cryptop' ),
				'default' => esc_html__( 'This is an Alert', 'cryptop' ),
				'label_block' => true,
			]
		);

		$controls->add_control(
			'alert_description',
			[
				'label' => esc_html__( 'Content', 'cryptop' ),
				'type' => Controls_Manager::TEXTAREA,
				'placeholder' => esc_html__( 'Enter your description', 'cryptop' ),
				'default' => esc_html__( 'I am a description. Click the edit button to change this text.', 'cryptop' ),
				'separator' => 'none',
				'show_label' => false,
			]
		);

		$controls->add_control(
			'show_dismiss',
			[
				'label' => esc_html__( 'Dismiss Button', 'cryptop' ),
				'type' => Controls_Manager::SELECT,
				'default' => 'show',
				'options' => [
					'show' => esc_html__( 'Show', 'cryptop' ),
					'hide' => esc_html__( 'Hide', 'cryptop' ),
				],
			]
		);

		$controls->add_control(
			'view',
			[
				'label' => esc_html__( 'View', 'cryptop' ),
				'type' => Controls_Manager::HIDDEN,
				'default' => 'traditional',
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_type',
			[
				'label' => esc_html__( 'Alert', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'background',
			[
				'label' => esc_html__( 'Background Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-alert' => 'background-color: {{VALUE}};',
				],
			]
		);

		$controls->add_control(
			'border_color',
			[
				'label' => esc_html__( 'Border Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-alert' => 'border-color: {{VALUE}};',
				],
			]
		);

		$controls->add_responsive_control(
			'border_left-width',
			[
				'label' => esc_html__( 'Left Border Width', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 0,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-alert' => 'border-left-width: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$controls->add_responsive_control(
			'padding_from-left',
			[
				'label' => esc_html__( 'Left Padding', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 200,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 75,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-alert' => 'padding-left: {{SIZE}}{{UNIT}};',
				],
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_icon',
			[
				'label' => esc_html__( 'Icon', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'icon_color',
			[
				'label' => esc_html__( 'Icon Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-alert i' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->add_control(
			'icon_size',
			[
				'label' => esc_html__( 'Icon size', 'cryptop' ),
				"description"	=> esc_html__( 'In Pixels', 'cryptop' ),
				'type' => Controls_Manager::SLIDER,
				'size_units' => ['px'],
				'range' => [
					'px' => [
						'min' => 0,
						'max' => 100,
						'step' => 1,
					],
				],
				'default' => [
					'unit' => 'px',
					'size' => 36,
				],
				'selectors' => [
					'{{WRAPPER}} .elementor-alert i:before' => 'font-size: {{SIZE}}{{UNIT}};',
				],
			]
		);		

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_title',
			[
				'label' => esc_html__( 'Title', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'title_color',
			[
				'label' => esc_html__( 'Text Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-alert-title' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'alert_title',
				'selector' => '{{WRAPPER}} .elementor-alert-title',
				'scheme' => Scheme_Typography::TYPOGRAPHY_1,
			]
		);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_description',
			[
				'label' => esc_html__( 'Description', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

		$controls->add_control(
			'description_color',
			[
				'label' => esc_html__( 'Text Color', 'cryptop' ),
				'type' => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .elementor-alert-description' => 'color: {{VALUE}};',
				],
			]
		);

		$controls->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name' => 'alert_description',
				'selector' => '{{WRAPPER}} .elementor-alert-description',
				'scheme' => Scheme_Typography::TYPOGRAPHY_3,
			]
		);

		$controls->end_controls_section();
	}

	protected function _get_initial_config() {
		$config = [
			'widget_type' => $this->get_name(),
			'keywords' => $this->get_keywords(),
			'categories' => $this->get_categories(),
		];

		$out = array_merge( parent::_get_initial_config(), $config );

		//Default borders for cws-banner
		$out['controls']['_border_radius']['default'] = [
			'unit' => 'px',
			'top' => '5',
			'right' => '5',
			'bottom' => '5',
			'left' => '5',
			'isLinked' => false
		];

		$out['controls']['_box_shadow_box_shadow']['default'] = [
			'horizontal' => 0,
			'vertical' => 0,
			'blur' => 30,
			'spread' => 0,
			'color' => 'rgba(45,116,221,0.7)'
		];
		$out['controls']['_box_shadow_box_shadow_type']['default'] = 'yes';

		return $out;
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;

		$this->add_inline_editing_attributes( 'alert_title', 'none' );

		$this->add_inline_editing_attributes( 'alert_description' );

		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_msg_box', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_msg_box', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Elementor_Message_box() );