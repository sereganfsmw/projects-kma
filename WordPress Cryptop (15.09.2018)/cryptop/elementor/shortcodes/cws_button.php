<?php
namespace Elementor;

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

//CWS Button
class CWS_Button extends Widget_Base {
	public function get_name() {
		return 'button';
	}

	public function get_title() {
		return esc_html( 'CWS Button', 'cryptop' );
	}

	public function get_icon() {
		// Icon name from the Elementor font file, http://dtbaker.net/web-development/creating-your-own-custom-elementor-widgets/
		return 'eicon-button';
	}

	public function get_categories() {
		return [ 'cws-elements' ];
	}

	protected function _register_controls() {
		$controls = $this;

		global $cws_theme_funcs;
		//Colors
		$theme_colors_first_color = esc_attr( $cws_theme_funcs->cws_get_meta_option( 'theme_colors' )['first_color'] );

		//List of controls, https://github.com/pojome/elementor/blob/master/docs/content/controls/reference.md
		$sections = new CWS_Elementor_Sections($this);

		$controls->start_controls_section(
			'section_button',
			[
				'label' => esc_html__( 'Button', 'cryptop' ),
			]
		);

			$controls->add_control(
				'button_type',
				[
					'label' => esc_html__( 'Type', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => '',
					'options' => [
						'' => esc_html__( 'Default', 'cryptop' ),
						'info' => esc_html__( 'Info', 'cryptop' ),
						'success' => esc_html__( 'Success', 'cryptop' ),
						'warning' => esc_html__( 'Warning', 'cryptop' ),
						'danger' => esc_html__( 'Danger', 'cryptop' ),
					],
					'prefix_class' => 'elementor-button-',
				]
			);

			$controls->add_control(
				'text',
				[
					'label' => esc_html__( 'Text', 'cryptop' ),
					'type' => Controls_Manager::TEXT,
					'default' => esc_html__( 'Click me', 'cryptop' ),
					'placeholder' => esc_html__( 'Click me', 'cryptop' ),
				]
			);

			$controls->add_control(
				'link',
				[
					'label' => esc_html__( 'Link', 'cryptop' ),
					'type' => Controls_Manager::URL,
					'placeholder' => esc_html__( 'https://your-link.com', 'cryptop' ),
					'default' => [
						'url' => '#',
					],
				]
			);

			$controls->add_responsive_control(
				'align',
				[
					'label' => esc_html__( 'Alignment', 'cryptop' ),
					'type' => Controls_Manager::CHOOSE,
					'options' => [
						'left'    => [
							'title' => esc_html__( 'Left', 'cryptop' ),
							'icon' => 'fa fa-align-left',
						],
						'center' => [
							'title' => esc_html__( 'Center', 'cryptop' ),
							'icon' => 'fa fa-align-center',
						],
						'right' => [
							'title' => esc_html__( 'Right', 'cryptop' ),
							'icon' => 'fa fa-align-right',
						],
						'justify' => [
							'title' => esc_html__( 'Justified', 'cryptop' ),
							'icon' => 'fa fa-align-justify',
						],
					],
					'prefix_class' => 'elementor%s-align-',
					'default' => '',
				]
			);

			$controls->add_control(
				'size',
				[
					'label' => esc_html__( 'Size', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'sm',
					'options' => [
						'xs' => esc_html__( 'Extra Small', 'cryptop' ),
						'sm' => esc_html__( 'Small', 'cryptop' ),
						'md' => esc_html__( 'Medium', 'cryptop' ),
						'lg' => esc_html__( 'Large', 'cryptop' ),
						'xl' => esc_html__( 'Extra Large', 'cryptop' ),
					],
				]
			);

			$controls->add_control(
				'icon',
				[
					'label' => esc_html__( 'Icon', 'cryptop' ),
					'type' => Controls_Manager::ICON,
					'label_block' => true,
					'default' => '',
				]
			);

			$controls->add_control(
				'icon_align',
				[
					'label' => esc_html__( 'Icon Position', 'cryptop' ),
					'type' => Controls_Manager::SELECT,
					'default' => 'left',
					'options' => [
						'left' => esc_html__( 'Before', 'cryptop' ),
						'right' => esc_html__( 'After', 'cryptop' ),
					],
					'condition' => [
						'icon!' => '',
					],
				]
			);

			$controls->add_control(
				'icon_indent',
				[
					'label' => esc_html__( 'Icon Spacing', 'cryptop' ),
					'type' => Controls_Manager::SLIDER,
					'range' => [
						'px' => [
							'max' => 50,
						],
					],
					'condition' => [
						'icon!' => '',
					],
					'selectors' => [
						'{{WRAPPER}} .elementor-button .elementor-align-icon-right' => 'margin-left: {{SIZE}}{{UNIT}};',
						'{{WRAPPER}} .elementor-button .elementor-align-icon-left' => 'margin-right: {{SIZE}}{{UNIT}};',
					],
				]
			);

			$controls->add_control(
				'view',
				[
					'label' => esc_html__( 'View', 'cryptop' ),
					'type' => Controls_Manager::HIDDEN,
					'default' => 'traditional',
				]
			);

		$controls->end_controls_section();

		$controls->start_controls_section(
			'section_style',
			[
				'label' => esc_html__( 'Button', 'cryptop' ),
				'tab' => Controls_Manager::TAB_STYLE,
			]
		);

			$controls->add_group_control(
				Group_Control_Typography::get_type(),
				[
					'name' => 'typography',
					'scheme' => Scheme_Typography::TYPOGRAPHY_4,
					'selector' => '{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button',
				]
			);

			$controls->start_controls_tabs( 'tabs_button_style' );

				$controls->start_controls_tab(
					'tab_button_normal',
					[
						'label' => esc_html__( 'Normal', 'cryptop' ),
					]
				);

				$controls->add_control(
					'button_text_color',
					[
						'label' => esc_html__( 'Text Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'default' => '#ffffff',
						'selectors' => [
							'{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'color: {{VALUE}};',
						],
					]
				);

				$controls->add_control(
					'background_color',
					[
						'label' => esc_html__( 'Background Color', 'cryptop' ),
						'type' => Controls_Manager::COLOR,
						'default' => $theme_colors_first_color,
						'selectors' => [
							'{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'background-color: {{VALUE}};',
						],
					]
				);

				$controls->end_controls_tab();

				$controls->start_controls_tab(
					'tab_button_hover',
					[
						'label' => esc_html__( 'Hover', 'cryptop' ),
					]
				);

					$controls->add_control(
						'hover_color',
						[
							'label' => esc_html__( 'Text Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => $theme_colors_first_color,
							'selectors' => [
								'{{WRAPPER}} a.elementor-button:hover, {{WRAPPER}} .elementor-button:hover' => 'color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'button_background_hover_color',
						[
							'label' => esc_html__( 'Background Color', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => '#ffffff',
							'selectors' => [
								'{{WRAPPER}} a.elementor-button:hover, {{WRAPPER}} .elementor-button:hover' => 'background-color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'button_hover_border_color',
						[
							'label' => esc_html__( 'Border Color On Hover', 'cryptop' ),
							'type' => Controls_Manager::COLOR,
							'default' => $theme_colors_first_color,
							'condition' => [
								'border_border!' => '',
							],
							'selectors' => [
								'{{WRAPPER}} a.elementor-button:hover, {{WRAPPER}} .elementor-button:hover' => 'border-color: {{VALUE}};',
							],
						]
					);

					$controls->add_control(
						'hover_animation',
						[
							'label' => esc_html__( 'Hover Animation', 'cryptop' ),
							'type' => Controls_Manager::HOVER_ANIMATION,
						]
					);

				$controls->end_controls_tab();

			$controls->end_controls_tabs();

			$controls->add_group_control(
				CWS_Group_Control_Border::get_type(),
				[
					'name' => 'border',
					'selector' => '{{WRAPPER}} .elementor-button',
					'separator' => 'before',
					'defaults' => [
						'border' => 'solid',
						'width' => [
							'top' => 1,
							'right' => 1,
							'bottom' => 1,
							'left' => 1,
							'unit' => 'px'
						],
						'color' => $theme_colors_first_color,
					],			
					'mobile_defaults' => [
						'border' => 'solid',
						'width' => [
							'top' => 1,
							'right' => 1,
							'bottom' => 1,
							'left' => 1,
							'unit' => 'px'
						],
						'color' => $theme_colors_first_color,
					],		
					'tablet_defaults' => [
						'border' => 'solid',
						'width' => [
							'top' => 1,
							'right' => 1,
							'bottom' => 1,
							'left' => 1,
							'unit' => 'px'
						],
						'color' => $theme_colors_first_color,
					],									
				]
			);		

			$controls->add_control(
				'border_radius',
				[
					'label' => esc_html__( 'Border Radius', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', '%' ],
				'default' => [
					'top' => 4,
					'right' => 4,
					'bottom' => 4,
					'left' => 4
				],
					'selectors' => [
						'{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'border-radius: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
				]
			);

			$controls->add_group_control(
				Group_Control_Box_Shadow::get_type(),
				[
					'name' => 'button_box_shadow',
					'selector' => '{{WRAPPER}} .elementor-button',
				]
			);

			$controls->add_control(
				'custom_paddings',
				[
					'label' => esc_html__( 'Custom paddings', 'cryptop' ),
					'type' => Controls_Manager::SWITCHER,
					'default' => '',
				]
			);

			$controls->add_responsive_control(
				'text_padding',
				[
					'label' => esc_html__( 'Padding', 'cryptop' ),
					'type' => Controls_Manager::DIMENSIONS,
					'size_units' => [ 'px', 'em', '%' ],
					'step' => 0.1,
					'default' => [
						'top' => 15,
						'right' => 35,
						'bottom' => 15,
						'left' => 35
					],
					'selectors' => [
						'{{WRAPPER}} a.elementor-button, {{WRAPPER}} .elementor-button' => 'padding: {{TOP}}{{UNIT}} {{RIGHT}}{{UNIT}} {{BOTTOM}}{{UNIT}} {{LEFT}}{{UNIT}};',
					],
					'separator' => 'before',
					'condition' => [
						'custom_paddings' => 'yes',
					],
				]
			);

		$controls->end_controls_section();
	}

	//PHP template (refresh elements)
	protected function render( $instance = [] ) {
		global $cws_essentials_funcs;

		$this->add_inline_editing_attributes( 'text', 'none' ); //Inline editind field for button caption

		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_button', $this, 'php'));
	}

	//JavaScript "Backbone" template (live preview)
	protected function _content_template() {
		global $cws_essentials_funcs;
		echo sprintf("%s", $cws_essentials_funcs->render_shortcodes('cws_button', $this, 'js'));
	}
	public function render_plain_content( $instance = [] ) {}
}

Plugin::instance()->widgets_manager->register_widget_type( new CWS_Button() );