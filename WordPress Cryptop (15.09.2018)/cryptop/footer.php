<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Cryptop
 * @since Cryptop 1.0
 */
global $cws_theme_funcs;
?>
		<div class="scroll_block">
			<span class="scroll_to_top fadeOut<?php $footer = $cws_theme_funcs->cws_get_meta_option('footer'); sprintf('%s', (isset($footer['fixed']) && $footer['fixed'] == '1' ? ' fixed' : '')); ?>"></span>
		</div>
	</div><!-- #main -->

	<?php
		if ($cws_theme_funcs){
			print $cws_theme_funcs->cws_page_footer();
		} else {
		?>

		<div class="copyrights_area">
			<div class="container">
				<div class="copyrights_container a-center">
					<div class="copyrights"><?php esc_html_e('Copyright ', 'cryptop'); ?> <?php echo date("Y");?></div>
				</div>
			</div>
		</div>

		<?php
		}
	?> 
	</div>
<!-- end body cont -->
<?php
wp_footer();
?>
</body>
</html>