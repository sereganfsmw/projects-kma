-- phpMyAdmin SQL Dump
-- version 4.0.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 10 2014 г., 14:33
-- Версия сервера: 5.5.35-log
-- Версия PHP: 5.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(60) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `cookie` varchar(32) NOT NULL,
  `name` varchar(15) NOT NULL,
  `avatar` varchar(40) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `admin`
--

INSERT INTO `admin` (`id`, `login`, `pass`, `cookie`, `name`, `avatar`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'bdf070cbf59eb2999e59b8ef11a73981', 'Администратор', 'users/admin/thumbs.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postid` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `name` varchar(60) NOT NULL,
  `post` text NOT NULL,
  `comment_time` varchar(30) NOT NULL,
  `comment_date` varchar(30) NOT NULL,
  `avatar` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `postid` (`postid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` varchar(60) NOT NULL,
  `ctime` varchar(10) NOT NULL,
  `cdate` varchar(15) NOT NULL,
  `title` varchar(80) NOT NULL,
  `post` mediumtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=41 ;

--
-- Дамп данных таблицы `post`
--

INSERT INTO `post` (`id`, `author`, `ctime`, `cdate`, `title`, `post`) VALUES
(23, 'Администратор', '19:07:59', '28.10.14', 'Тема №1', 'Вывод текста с форматированием <b>жирный</b>, <i>италик </i>, <u>подчеркнутый</u>, <strike>зачеркнутый </strike>, <b><span style="color: orange;">оранжевый</span>, <span style="color: red;">красный</span>, <span style="color: lime;">зеленый</span>, <span style="color: blue;">синий </span>, <span style="color: magenta;">магента</span>.</b><br>\r\n'),
(24, 'Администратор', '19:12:14', '28.10.14', 'Тема №2', 'Список:<br><ul><li>Первый</li><li>Второй</li><li>Третий</li><li>Четвертый</li><li>Пятый</li></ul><p>Нумерованый список:</p><p><ol><li>Первый</li><li>Второй</li><li>Третий</li><li>Четвертый</li><li>Пятый</li></ol></p>'),
(25, 'Администратор', '20:14:42', '28.10.14', 'Тема №3', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Добавление картинки в тему №3.</p>\r\n<p>&nbsp;<img src="WYSIWYG/Tinymce/upload_dir/Avatar.jpg" alt="" width="187" height="229" /></p>\r\n<p>Картинка</p>\r\n</body>\r\n</html>'),
(26, 'Администратор', '20:42:48', '28.10.14', 'Тема №4', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Добавляем таблицу , размером 3 х 3</p>\r\n<table id="table14973" style="height: 48px;" border="1" width="79">\r\n<tbody>\r\n<tr>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">1</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">2</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">3</span></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">4</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">5</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">6</span></td>\r\n</tr>\r\n<tr>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">7</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">8</span></td>\r\n<td style="background-color: #000000; border-color: #ffffff; text-align: center;"><span style="color: #ffffff;">9</span></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>Делаем табуляцию</p>\r\n<blockquote>1<br />\r\n<blockquote>2<br />\r\n<blockquote>3<br />\r\n<blockquote>4</blockquote>\r\n</blockquote>\r\n</blockquote>\r\n</blockquote>\r\n</body>\r\n</html>'),
(38, 'Администратор', '22:54:18', '30.10.14', 'Тема №5', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Добавляем спец символы , Х<sub>2</sub> Х<sup>2</sup>&nbsp; а также смайлики <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /> <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cry.gif" alt="cry" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-foot-in-mouth.gif" alt="foot-in-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-frown.gif" alt="frown" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-innocent.gif" alt="innocent" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-kiss.gif" alt="kiss" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-money-mouth.gif" alt="money-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-sealed.gif" alt="sealed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-smile.gif" alt="smile" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-surprised.gif" alt="surprised" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-tongue-out.gif" alt="tongue-out" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-undecided.gif" alt="undecided" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-wink.gif" alt="wink" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-yell.gif" alt="yell" /> и тд...</p>\r\n</body>\r\n</html>'),
(39, 'Администратор', '22:58:14', '30.10.14', 'Тема №6', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p>Форматирование текста, положение теста, размер шрифта:</p>\r\n<p style="text-align: left;">Первый</p>\r\n<p style="text-align: center;">Второй</p>\r\n<p style="text-align: right;">Третий</p>\r\n<p style="text-align: left;"><span style="background-color: #ff6600;">Разный Шрифт</span></p>\r\n<p style="text-align: left;"><span style="font-family: arial black,avant garde; font-size: 18pt;">Первый</span></p>\r\n<p style="text-align: left;"><span style="font-family: comic sans ms,sans-serif; font-size: 18pt;">Второй</span></p>\r\n<p style="text-align: left;"><span style="font-family: times new roman,times; font-size: 18pt;">Третий</span></p>\r\n</body>\r\n</html>'),
(40, 'Администратор', '23:01:57', '30.10.14', 'Тема №7', '<!DOCTYPE html>\n<html>\n<head>\n</head>\n<body>\n<p>Добавляем видео с Youtube <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-smile.gif" alt="smile" /></p>\n<p>Добавляем спец символы , Х<sub>2</sub> Х<sup>2</sup>&nbsp; а также смайлики <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cool.gif" alt="cool" /> <img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-cry.gif" alt="cry" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-embarassed.gif" alt="embarassed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-foot-in-mouth.gif" alt="foot-in-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-frown.gif" alt="frown" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-innocent.gif" alt="innocent" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-kiss.gif" alt="kiss" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-laughing.gif" alt="laughing" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-money-mouth.gif" alt="money-mouth" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-sealed.gif" alt="sealed" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-smile.gif" alt="smile" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-surprised.gif" alt="surprised" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-tongue-out.gif" alt="tongue-out" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-undecided.gif" alt="undecided" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-wink.gif" alt="wink" /><img src="WYSIWYG/Tinymce/plugins/emoticons/img/smiley-yell.gif" alt="yell" /> и тд...</p>\n<p>Добавляем таблицу , размером 3 х 3</p>\n<p><iframe src="//www.youtube.com/embed/3jbLIkITRow" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe></p>\n</body>\n</html>');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `active` varchar(10) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `name` varchar(15) NOT NULL,
  `last_name` varchar(15) NOT NULL,
  `age` int(11) NOT NULL,
  `city` varchar(15) NOT NULL,
  `email` varchar(30) NOT NULL,
  `cookie` varchar(32) NOT NULL,
  `key_user` varchar(33) NOT NULL,
  `avatar` varchar(40) NOT NULL,
  `register_time` varchar(20) NOT NULL,
  `register_date` varchar(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `active`, `username`, `password`, `name`, `last_name`, `age`, `city`, `email`, `cookie`, `key_user`, `avatar`, `register_time`, `register_date`) VALUES
(1, '0', 'user', 'user', 'Serega', 'MoST', 22, 'Николаев', 'personal.notes@yandex.ru', '', '8480093613fd4d3727b16da88166343f', 'users/user/thumbs.jpg', '20:58:39', '09.11.14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
