<?php
session_start();

class ctrl{

    public function __construct() {
        $this->db = new db();
        
        if(!empty($_COOKIE['uid']) && !empty($_COOKIE['key'])){
           //$this-> user = $this->db->query("SELECT * FROM users WHERE id = ? AND cookie = ?",$_COOKIE['uid'],$_COOKIE['key']);
          if ($_SESSION['user']) {$this->user=true;}
          if ($_SESSION['admin']) {$this->admin=true;}
        }
        else 
        {
          $this->user=false;
          $this->admin=false;
        }
    }
    
    public function out($tplname,$nested=FALSE){
    if(!$nested){
        $this->tpl = $tplname;
        include 'tpl/main.php';
    } else
        include 'tpl/'.$tplname;
        
    }
}

class app {
    public function __construct($path) {
        //echo $path;
        $this->route = explode('/', $path);
        
        $this->run();
    }
    
private function run(){
    $url= array_shift($this->route);
    
    if(!preg_match('#^[a-zA-Z0-9.,-]*$#', $url))
            throw new Exception('Invalid path');
    $ctrlName = 'ctrl'. ucfirst($url);
    if (file_exists('app/'.$ctrlName)) {
        $this->runController($ctrlName);
    } else{
        array_unshift($this->route, $url);
        $this->runController('ctrlIndex');
    }
}

private function runController($ctrlName){
        include "app/".$ctrlName.".php";
        
       $ctrl =  new $ctrlName();
       
       if(empty($this->route)||empty($this->route[0])){
           $ctrl->index();            
       } else {
           if (empty($this->route))
               $method = 'index';
           else
           $method = array_shift($this->route);
           if (method_exists($ctrl,$method)){
               if (empty($this->route))
               $ctrl->$method();
               else
                   call_user_func_array (array($ctrl,$method), $this->route);  
       } else
//include('404/404.html');
 echo "
<!DOCTYPE html>
<html>
<head>
<meta charset='UTF-8'>
<title>404</title>
<link rel='shortcut icon' href='favicon.png'>

<link href='css/bootstrap.css' rel='stylesheet'>
<link href='css/font-awesome.css' rel='stylesheet'>
<link rel='stylesheet' href='./404/css/style.css' media='screen' type='text/css' />



</head>

<body>
<div id='demo_navi'>
  <div class='logo'><b>Personal Blog</b></div></a><div class='name'><a href ='/' class='btn btn-primary' type='button'>Go to <i class='fa fa-home'></i> Home</a></div>
</div>
  
<audio controls autoplay loop>
<source src='./404/404.mp3'  type='audio/mpeg'>
</audio>

<script src='./404/js/svg.js'></script>
<script src='./404/js/perlin-noise-simplex.js'></script>
<div style='position: relative; top:20%;' id='notfound'></div>
<script src='./404/js/index.js'></script>

</body>

</html>
";
//throw new Exception('Error 404');
    }
}


}