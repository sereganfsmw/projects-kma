<script type="text/javascript">
$(document).ready(function($) {
    $('li').removeClass('active');
    $('#add-post').addClass('active');
});
</script>

<? if ($this->user){ 
print "
<link rel='stylesheet' href='WYSIWYG/Imperavi/redactor.css' />
<script src='WYSIWYG/Imperavi/redactor.js'></script>

<script type='text/javascript'>
$(document).ready(
	function()
	{
		$('#redactor_content').redactor({
			imageUpload: 'WYSIWYG/Imperavi/image_upload.php',
			fileUpload: 'WYSIWYG/Imperavi/file_upload.php',
			fixed: true
		});
	}
);
</script>

<center><h3 class='muted'><i class='fa fa-file-text-o'></i> Добавить пост:</h3></center>
";}?>

<? if ($this->admin){ 
print "
<script type='text/javascript' src='WYSIWYG/Tinymce/tinymce.min.js'></script>
<script type='text/javascript'>
tinymce.init({
        selector: 'textarea',
        plugins: [
                'advlist autolink lists charmap print preview hr anchor pagebreak spellchecker',
                'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking',
                'table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern image link media moxiemanager' //autosave
        ],

        toolbar1: 'newdocument fullscreen | bold italic underline strikethrough | removeformat | alignleft aligncenter alignright alignjustify | formatselect fontselect fontsizeselect | forecolor backcolor | preview', //styleselect fullpage print
        toolbar2: 'undo redo | cut copy paste | searchreplace | bullist numlist | outdent indent blockquote  | link unlink anchor insertfile image media code | insertdatetime',
        toolbar3: 'table | hr  | subscript superscript | charmap emoticons | ltr rtl | spellchecker | visualchars visualblocks nonbreaking template pagebreak restoredraft',

		
        menubar: false,
        //toolbar_items_size: 'small',

        style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ],

        templates: [
                {title: 'Test template 1', content: 'Test 1'},
                {title: 'Test template 2', content: 'Test 2'}
        ]
});</script>
";}?>


<form class='form-horizontal' method='POST'>
    <input type='text' class='input-xxlarge' style='width:99%;' name='title' value='<?=@$this->post['title'] ?>' placeholder='Заголовок'/>

<div id='page'>
    <textarea id='redactor_content' name='content' style='width:100%;height:200px;' ><?=@$this->post['post']?></textarea>
</div>

    <hr>
    <center><button class="btn btn-success" type="submit"><i class="fa fa-plus"></i> Добавить</button></center>
    
</form>