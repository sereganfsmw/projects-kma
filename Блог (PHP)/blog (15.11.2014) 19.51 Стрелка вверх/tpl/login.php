<script type="text/javascript">
$(document).ready(function($) {
	$('li').removeClass('active');
	$('#log-in').addClass('active');
});
</script>

<div class="row">
	<div class="span4 offset4 well" style="width:300px;">
		<legend>Log - in</legend>
		<? echo $this->error; ?>
		<form id="auth" method="POST">

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-user"></i></span>
		<input class="span3" id="prependedInput" type="text" name="login" placeholder="Username">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-key"></i></span>
		<input class="span3" id="prependedInput" type="password" name="pass" placeholder="Password">
		</div>

		<button type="submit" name="submit" class="btn btn-large btn-info btn-block">Войти <i class="fa fa-sign-in"></i></button>
		</form>    
	</div>
</div>

<script src="my_js/login.js"></script>