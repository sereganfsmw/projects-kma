<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Personal Blog</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="MoST">

  <!-- Bootstrap v2.3.2 CSS -->
  <link href="css/bootstrap.css" rel="stylesheet">
	<link rel="shortcut icon" href="favicon.png">

    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
        background-color: #f5f5f5;
      }
    </style>

    <!-- Bootstrap v2.3.2 Responsive CSS -->
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Font Awesome 4.2.0 CSS -->
    <link href="css/font-awesome.css" rel="stylesheet">
	
    <!-- Add-on CSS -->
    <link href="css/callout.css" rel="stylesheet">
    <link href="css/kbd.css" rel="stylesheet">
    <link href="css/panels.css" rel="stylesheet">

    <!-- Blog CSS -->
    <link href="css/blog.css" rel="stylesheet">

    <!-- Color Picker CSS -->
    <link href="css/colorpicker.css" rel="stylesheet">

</head>

       <!-- Navbar -->
    <div class="navbar navbar-inverse navbar-fixed-top">
     
      <div class="navbar-inner">
        <form class="navbar-form pull-left"><label class="open"><i class="fa fa-eyedropper fa-2x"></i></label></form>
        <div class="container">
          <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- <a class="brand" href="#">Project name</a> -->
          <a data-original-title="Версия сборки:" href="#" data-toggle="popover" data-placement="bottom" data-content="21:59 | 05/11/2014" title=""class="brand" href="#">Personal Blog</a>
          <div class="nav-collapse collapse">
         
          <form class="navbar-form pull-right user"><? if (($this->user) or ($this->admin)) { echo "<i class='fa fa-male'></i> Вы зашли как:<b>". $_COOKIE['name']. "</b>";}?></form>
            <ul class="nav"> 
              
<!-- ________________________________________________________М Е Н Ю________________________________________________________ -->

      <li id="home"><a href="/?home"><i class="fa fa-home"></i> Домой</a></li>
			<li id="blogs" class="active"><a href="/"><i class="fa fa-th-list"></i> Блоги</a></li>
				<li id="reg"><a href="/?reg"><i class="fa fa-user"></i> Регистрация</a></li>

				<? if ((!$this->user) and (!$this->admin)){ /*Если пользователь не авторизован*/?>
				<li id="log-in"><a href="/?login"><i class="fa fa-sign-in"></i> Войти</a></li>
				<? } ?>

				<? if ($this->admin) { /*Если зашол администратор*/?>
				<li id="control-panel"><a href="/?control"><i class="fa fa-sliders"></i> Управление</a></li>
				<? } ?>

        <? if ($this->user) { /*Если зашол пользователь*/?>
        <li id="info-user"><a href="/?info/<?=$_COOKIE['username'];?>"><i class="fa fa-info-circle"></i> Информация</a></li>
        <? } ?>

				<? if (($this->user) or ($this->admin)) {/*Если зашол пользователь или админ*/?>
				<li id="add-post"><a href="/?add"><i class="fa fa-file-text"></i> Добавить пост</a></li>
				<li><a href="/?logoff"><i class="fa fa-power-off"></i> Выйти</a></li>
				<? } ?>
            </ul>
          </div>
        </div>
      </div>
    </div>	

<!-- Left Panel -->
<div class="left_panel">  
  <!-- Color Picker -->
  <button id="reset_color" class="btn btn-primary" type="button"><i class="fa fa-refresh"></i> Reset color</button>
  <div id="flat"></div>
</div>  

<body>

<!-- ________________________________________________________А Л Е Р Т Ы________________________________________________________ -->
<!-- Slide Alerts -->
<div style="display:none" class="success-box alert-box">
<div class="msg">Success &#8211; Your message will goes here</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="error-box alert-box">
<div class="msg"><b>Внимание!</b> &#8211; Таблица пользователей очищена...</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="info-box alert-box">
<div class="msg">Info &#8211; Information message will goes here</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="warning-box alert-box">
<div class="msg"><b>Внимание!</b> &#8211; Введите логин и пароль.</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<div style="display:none" class="download-box alert-box">
<div class="msg">Внимание &#8211; Все коментарии удалены.</div>
<p><a class="toggle-alert" href="#">Toggle</a></p>
</div>

<!-- Slide Down Notification -->
<div style="top: -68px; display:none;" class="info message">
     <h3>Информация!</h3>
     <p>Вы зашли как <b>Серега Мост</b> (Администратор).</p>
</div>

<div style="top: -68px; display:none;" class="error message">
     <h3>Удален!</h3>
     <p>Акаунт пользователя удален, но вот и все ты зделал свой выбор...</p>
</div>

<div style="top: -68px; display:none;" class="warning message">
     <h3>Внимание!</h3>
     <p>Пользователь зарегестрирован , но вы должны активировать акаунт, пожалуста проверте почту для активации вашего акаунта.</p>
</div>

<div style="top: -68px; display:none;" class="success message">
     <h3>Успешно!</h3>
     <p>Акаунт пользователя активирован. Спасибо. Вы можете авторизоваться...</p>
</div>
<!-- ______________________________________________________________________________________________________________________________ -->

<div class="container">

<!-- Bootstrap JavaScript -->
<script src="js/jquery.js"></script>
<script src="js/application.js"></script>

<script src="js/bootstrap-tooltip.js"></script>
<script src="js/bootstrap-popover.js"></script>
<script src="js/bootstrap-dropdown.js"></script>

<script src="js/bootstrap-transition.js"></script>
<script src="js/bootstrap-alert.js"></script>
<script src="js/bootstrap-modal.js"></script>

<script src="js/bootstrap-scrollspy.js"></script>
<script src="js/bootstrap-tab.js"></script>

<script src="js/bootstrap-button.js"></script>
<script src="js/bootstrap-collapse.js"></script>
<script src="js/bootstrap-carousel.js"></script>
<script src="js/bootstrap-typeahead.js"></script>

<!-- Color Picker JavaScript -->
<script src="js/tinycolor.js"></script>
<script src="js/colorpicker.js"></script>

<!-- Cookies JavaScript -->
<script src="js/cookies.js"></script>

<!-- Всплывающие Alerts -->
<script src="js/alert-growl.js"></script>

<!-- Стрелка вверх -->
<script src="js/jquery.scrollUp.js"></script>

<? //echo $_COOKIE['name'];//print_r ($this->user);//$_SESSION['user'] //print_r ($this->user);//print_r($_COOKIE); ?>


<script src="my_js/main.js"></script>


<? if ($this->admin) {?>
<script>
//Если зашол Администратор то показать (Notification)
//Slide Down Notification
$(function() {

if(!$$c.get('admin'))
{
// hideAllMessages();   
$('.info').animate({top:"0"}, 500); //Slide

$$c.set('admin', 'notification', 10000);
}

});
</script>
<? } ?>

<SCRIPT LANGUAGE="JavaScript">
        function serega() 
        { 
        //alert('Serega');
        $('.info').show('slow', function() {
          $('.info').animate({top:"0"}, 500); //Slide
        });
        } 
</SCRIPT>

<!-- ________________________________________________________Ш А Б Л О Н Ы________________________________________________________ -->
<? $this->out($this->tpl,true); ?>
</div>  <!-- /container -->


<div class="navbar navbar-inverse navbar-fixed-bottom" style="height: 20px;">
    <div class="navbar-inner">
        <div class="pull-left muted">
            Powered by <b>Bootstrap</b> & <b>Font Awesome</b> <i class="fa fa-flag"></i>
        </div>
        <div class="pull-right text-info">
            Design by <b><kbd>Serega</kbd> <code>MoST</code></b>. Copyright © 2014 All Rights Reserved <i class="fa fa-exclamation-triangle"></i>
        </div>
        
    </div>
</div>

    </body>
</html>


