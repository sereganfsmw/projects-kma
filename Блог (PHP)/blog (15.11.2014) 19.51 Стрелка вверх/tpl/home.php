<script type="text/javascript">
$(document).ready(function($) {
	$('li').removeClass('active');
	$('#home').addClass('active');
});

</script>
<div class="hero-unit">
	<div class="container">
		<h1>Приветствуем! <i class="fa fa-flag"></i></h1>
		<p>Создавайте свои персональные блоги и делитесь с друзьями, всего за несколько простых шагов.</p>
		<p>
			<a a href="/?reg" class="btn btn-primary btn-large">1. Зарегестрируйтесь <i class="fa fa-plus-circle"></i></a>
			<a a href="/?add" class="btn btn-success btn-large">2. Создавайте блоги <i class="fa fa-bars"></i></a>
			<a a href="/" class="btn btn-danger btn-large">3. Делитесь своими идеями <i class="fa fa-pencil-square-o"></i></a>
		</p>
	</div>
</div>

<div class="well">
	<p>Представляю вашему вниманию возможности блога:</p>
	          <div class="row-fluid">
            <div class="span4">
              <h2>Оформление:</h2>
              <p>Дизайн сайта выполнен в темных тонах с разноцветными елементами управления.</p>
              <p>
<div class="btn-group">
	<button type="button" class="btn btn-danger btn-mini">Button</button>
	<button type="button" class="btn btn-success btn-mini">Button</button>
	<button type="button" class="btn btn-warning btn-mini">Button</button>
	<button type="button" class="btn btn-default btn-mini">Button</button>
	<button type="button" class="btn btn-inverse btn-mini">Button</button>
</div>
              </p>
            </div>
            <div class="span4">
              <h2>Просмотр блогов:</h2>
              <p>Для удобства чтения блогов, просмотр блогов выполнен в виде акардиона.</p>
              <p>

		
<div class="accordion" id="accordion2">
	<div class="accordion-group">
		<div class="accordion-heading">
		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
		Акардион
		</a>
		</div>
		<div id="collapseOne" class="accordion-body collapse">
			<div class="accordion-inner">
			Текст блока
			</div>
		</div>
		</div>
</div>

              </p>
            </div>
            <div class="span4">
              <h2>Коментарии:</h2>
              <p>Вы можете коментировать темы блогов созданые пользователями, а также просматривать пользователей которые крментировали вашу тему.</p>
            </div>
          </div>
</div>

<ul class="thumbnails">
  <li class="span4">
    <div class="thumbnail">
      <img src="img/WYSIWYG.jpg" tabindex="0">
      <div class="caption">
        <h3>WYSIWYG редактор:</h3>
        <p>Редактируйте ваши статти благодаря современому и стильному текстовому редактору.</p>
        <p><a href="img/WYSIWYG.jpg" class="btn btn-primary">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
  <li class="span4">
    <div class="thumbnail">
      <img src="img/Color_picker.jpg" tabindex="0"/>
      <div class="caption">
        <h3>Color Picker:</h3>
        <p>Меняйте дизайн сайта на свой вкус, благодаря продвинутому колор-пикеру, который позволяет подобрать абсолютно любой оттенок цвета.</p>
        <p><a href="img/Color_picker.jpg" class="btn btn-danger">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
  <li class="span4">
    <div class="thumbnail">
      <img src="img/Responsive.jpg" tabindex="0">
      <div class="caption">
        <h3>Адаптивный интерфейс:</h3>
        <p>С какого устройства вы б не заходили благодаря адаптивному дизайну (Responsive) интерфейс подстраиваеться под устройство на котором открыты блоги.</p>
        <p><a href="img/Responsive.jpg" class="btn btn-success">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
</ul>

<ul class="thumbnails">
  <li class="span4">
    <div class="thumbnail">
      <img src="img/404.jpg" tabindex="0">
      <div class="caption">
        <h3>Стильная страница "404":</h3>
        <p>При вводе неправильного адреса вы попадаете на страницу 404, не беда вы всегда можете вернуться назад.</p>
        <p><a href="img/404.jpg" class="btn btn-warning">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
  <li class="span4">
    <div class="thumbnail">
      <img src="img/Notification.jpg" tabindex="0">
      <div class="caption">
        <h3>"Notification" Alerts:</h3>
        <p>Чтобы не отвлекать пользователя на сайте использованы модыфицырованые "alert" благодаря чему всплывающие сообщения больше не помешают вашей работе.</p>
        <p><a href="img/Notification.jpg" class="btn btn-default">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
  <li class="span4">
    <div class="thumbnail">
      <img src="img/Font_awesome.jpg" tabindex="0">
      <div class="caption">
        <h3>Стилизированые иконки:</h3>
        <p>В большинстве елементах управления на сайте использованы иконки из безплатного пакета "Font Awesome v4.2.0".</p>
        <p><a href="img/Font_awesome.jpg" class="btn btn-inverse">Просмотр  <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
</ul>

<ul class="thumbnails">
  <li class="span4">
    <div class="thumbnail">
      <img src="img/login.jpg" tabindex="0">
      <div class="caption">
        <h3>Удобные формы:</h3>
        <p>Интуитивно понятные поля благодаря значкам с подсказками и "Placeholders".</p>
        <p><a href="img/login.jpg" class="btn btn-success">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
  <li class="span4">
    <div class="thumbnail">
      <img src="img/check_user.jpg" tabindex="0">
      <div class="caption">
        <h3>Проверки авторизации:</h3>
        <p>Неавторизированые пользователи могут лишь просматривать темы, но не могут оставлять коментарии, все проверки учтены.</p>
        <p><a href="img/check_user.jpg" class="btn btn-info">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
  <li class="span4">
    <div class="thumbnail">
      <img src="img/content.jpg" tabindex="0">
      <div class="caption">
        <h3>Добавление любого контента:</h3>
        <p>Пользователи в своих темам могут добавлять абсолютно любой контент: текст,аудио,видео,изображения,таблицы, смайлики.</p>
        <p><a href="img/content.jpg" class="btn btn-danger">Просмотр <i class="fa fa-picture-o"></i></a></p>
      </div>
    </div>
  </li>
</ul>

<style>

 img img[tabindex="0"] {
  cursor: zoom-in;
}
 img[tabindex="0"]:focus {
  position: fixed;
  z-index: 10;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  width: auto;
  height: auto;
  max-width: 99%;
  max-height: 99%;
  margin: auto;
  box-shadow: 0 0 20px #000, 0 0 0 1000px rgba(0,0,0,.5);
  
}
 img[tabindex="0"]:focus,  /* убрать строку, если не нужно, чтобы при клике на увеличенное фото, оно возвращалось в исходное состояние */
 img[tabindex="0"]:focus ~ * {
  pointer-events: none;
  cursor: zoom-out;
}
</style>