<center><h3 class="muted"><i class="fa fa-bars"></i> Блоги:</h3></center>


<!-- Акардион -->
<div class="accordion" id="accordion2">
<? foreach ($this->posts as $key => $value) {

//$comment_count2 = $this->db->query("SELECT count(postid) FROM comment WHERE postid=?",$value['id'])->all();
//print_r($comment_count2);
//echo $comment_count2['0']['count(postid)'];

$comment_count = $this->db->query("SELECT count(postid) FROM comment WHERE postid=?",$value['id'])->assoc();
//print_r($comment_count);
//echo $comment_count['count(postid)'];
?>
<center><h2><a class="topic" href="/?topic/<?=$value['id']?>"><?=$value['title'];?> <i class="fa fa-chevron-right"></i></a></h2></center>
<div class="accordion-group">
    <div class="accordion-heading">
    <label class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?=$value['id'];?>">
<!-- Хидер акардиона -->
                            <div class="row">
                                <div class="span12">
                                    <div class="span3"><i class="icon-user"></i> <b>Автор:</b> <?= $value['author'];?></div>
                                    <div class="span3"><i class="fa fa-clock-o"></i> <b>Время:</b> <?= $value['ctime'];?></div>
                                    <div class="span3"><i class="icon-calendar"></i> <b>Дата:</b> <?= $value['cdate'];?></div>
                                    <div class="span2"><i class="fa fa-comment"></i> <b>Коментарии:</b> <span class="badge"><?= $comment_count['count(postid)'];?></span></div>
                                    <i class="fa fa-chevron-down"></i>
                                </div>
                            </div>

    </label>
    </div>
<!-- Контент акардиона -->
        <div id="collapse<?=$value['id'];?>" class="accordion-body collapse">
            <div class="accordion-inner" style="padding: 0px;">

                            <div class="row">
                                <div class="span12">
                                    <div style="margin:0px;padding: 10px;" class="post"><?= $value['post'];?></div> <!-- span12 post-->
                                </div>
                            </div>

            </div>
        </div>
    </div>


<? if($this->admin) {?>
<span class="btn-group">
    <a href="/?edit/<?=$value['id']?>" class="btn btn-mini btn-default"><i class="fa fa-pencil"></i> Редактировать</a>
    <a href="/?del/<?=$value['id']?>" class="btn btn-mini btn-danger" onclick="return confirm('Точно удалить ?');"><i class="fa fa-trash"></i> Удалить</a>
</span>
<? } ?>
<hr style="border-top: 1px dashed black;">
<? } ?>
</div>
<br>