<center><h3 class="muted"><i class="fa fa-pencil-square-o"></i> Коментарии:</h3></center>

<div class="row">
    <div class="span12">
    <h2><?=$this->post['title'];?></a></h2>
        <div class="row">
            <div class="span4"><b>Автор:</b> <?= $this->post['author'];?></div>
            <div class="span3"><b>Время:</b> <?= $this->post['ctime'];?></div>
            <div class="span3"><b>Дата:</b> <?= $this->post['cdate'];?></div>
        </div>
        <div style="margin:20px 0 0 0;padding: 10px;" class="span12 post"><b>Текст:</b> <?= $this->post['post'];?></div> <!-- span12 post-->
    </div>
</div>

<? if($this->admin) {?>
<span class="btn-group">
    <a href="/?edit/<?=$this->post['id']?>" class="btn btn-mini"><i class="fa fa-pencil"></i> Редактировать</a>
    <a href="/?del/<?=$this->post['id']?>" class="btn btn-mini btn-danger" onclick="return confirm('Точно удалить ?');"><i class="fa fa-trash"></i> Удалить</a>
</span>
<?}?>

<hr>

<div class="comments">
<!-- Вывести коментарии-->
     <? foreach ($this->comments as $c) { $i++;?>
     <div class="comment">

<div class="row">
    <div class="span2">Коментарий <span class="badge badge-warning">#<?=$i?></span></div>
</div>
<div class="row">
  <div class="span1"><img class="img-rounded" src="<?=$c['avatar']?>"></div>

      <div class="span4"><i class="icon-user"></i> <a href="/?info/<?=$c['username'];?>"><?=$c['name'];?></a> 
    | <i class="fa fa-clock-o"></i> <?=$c['comment_time'];?>, <i class="icon-calendar"></i> <?=$c['comment_date'];?>
    </div>
  <div class="span10"><i class="icon-comment"></i> <?=$c['post']?></div>
        <?if ($this->admin) {?>
        <div class="span10"><a href="/?delComment/<?=$c['id']?>/<?=$this->post['id']?>" class="btn btn-mini btn-success" onclick="return confirm('Точно удалить ?');"><i class="fa fa-trash"></i> Удалить коментарий</a></div>
        <?}?>
</div>
<hr>
<?}?>
     <h4>Добавить комментарий:</h4>
<!-- Если зашол пользователь или админ-->
     <? if (($this->user) or ($this->admin)) {?>
     <form id="comment_form" action="/?addComment/<?=$this->post['id']?>" class="form-inline well" method="POST">
         <input type="text" name="post" style="width:86%;" class="input-xxlarge" placeholder="Коментарий"> <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i> Добавить</button>
     </form>

     <? }else { ?>
<!-- Если пользователь не авторизирован-->     
<div class='alert alert-info'>
    <button type='button' class='close' data-dismiss='alert'>&times;</button>
    <strong>Ошибка!</strong> Только авторизированные пользователи могут добавлять комментарии <i class='fa fa-exclamation-triangle'></i>. <a href="/?login" class="btn btn-mini btn-primary"><i class="fa fa-sign-in"></i> Авторизация</a>
</div>
     <? } ?>

</div>
<br>

<script src="my_js/topic.js"></script>