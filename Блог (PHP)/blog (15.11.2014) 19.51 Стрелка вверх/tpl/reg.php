


<script src="js/ajax_image_upload.js"></script>
<link href="css/file_drop.css" rel="stylesheet" type="text/css" />


<div class="row">
	<div class="span4 well" style="width:300px;">
		<legend><i class="fa fa-info-circle"></i> Регистрация <button id="avatar_show" class="btn btn-block btn-success" type="button">Аватар <i class="fa fa-chevron-right"></i></button></legend>
		<? echo $this->error; ?>
		<form id="register_form" method="POST">

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-user"></i></span>
		<input class="span3" id="prependedInput" type="text" name="user" placeholder="Пользователь" maxlength="15" title="A-Z a-z">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-key"></i></span>
		<input class="span3" id="prependedInput" type="password" name="pass" placeholder="Пароль">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-male"></i></span>
		<input class="span3" id="prependedInput" type="text" name="name" placeholder="Имя" maxlength="15">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-male"></i></span>
		<input class="span3" id="prependedInput" type="text" name="last_name" placeholder="Фамилия" maxlength="15">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-calendar"></i></span>
		<input class="span1" id="prependedInput" type="text" name="age" placeholder="Возраст" maxlength="2" title="0-9">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-compass"></i></span>
		<input class="span3" id="prependedInput" type="text" name="city" placeholder="Город">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-at"></i></span>
		<input class="span3" id="prependedInput" type="text" name="email" value="personal.notes@yandex.ru" placeholder="Електроная почта">
		</div>
		<button type="submit" name="submit" class="btn btn-large btn-inverse btn-block">Зарегестрироваться <i class="fa fa-sign-in"></i></button>
		</form>    
	</div>

	<div id="avatar" class="span4 well">
	<legend><i class="fa fa-camera"></i> Аватар</legend>

		<div id="drop-files" ondragover="return false">

			<!-- Область для перетаскивания -->
			<p>Перетащите изображение сюда</p>
	        <form id="frm">
	        	<input type="file" id="uploadbtn"/>
	        </form>
		</div>

	</div>

	<div id="preview" class="span3 well" style="width:300px;">
	<legend><i class="fa fa-picture-o"></i> Предпросмотр</legend>

    <!-- Область предпросмотра -->
	<div id="uploaded-holder"> 
		<div id="dropped-files">
        	<!-- Кнопки загрузить и удалить, а также количество файлов -->
        	<div id="upload-button">
             		
						<button id="delete" class="btn btn-danger btn-block" type="button"><i class="fa fa-times"></i> Отмена</button>
					
              
			</div>  
        </div>
	</div>

	</div>

</div>



<script type="text/javascript">
$(document).ready(function($) {
	$('li').removeClass('active');
	$('#reg').addClass('active');
	$('#avatar').hide();

	$('input[name=user]').attr('oncontextmenu','return false;'); //Запрет контекстного меню
	$('input[name=age]').attr('oncontextmenu','return false;'); //Запрет контекстного меню

});
</script>

<script src="my_js/reg.js"></script>