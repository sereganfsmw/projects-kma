<script type="text/javascript">
$(document).ready(function($) {
	$('li').removeClass('active');
	$('#info-user').addClass('active');
});
</script>

<script src="js/ajax_image_upload.js"></script>
<link href="css/file_drop.css" rel="stylesheet" type="text/css" />

<? foreach ($this->users as $key => $value) { ?>
<div class="row">
            <div class="span6" id="info">
                <div class="panel panel-primary">
                    <div class="panel-heading">

                        <h3 class="panel-title">Информация о пользователе: <strong class="text-right" style="font-size: 20pt;"><?= $value['name'];?></strong></h3>
                    </div>
                    <div class="panel-body">
                        <div class="row-fluid">
                            <div class="span4">
                                <img class="img-polaroid" src="<?= $value['avatar'];?>" alt="User Pic">
                            </div>
                            <div>

                                <br>
                                <table class="table table-condensed table-user-information" style="margin:10px 0px 0px 0px;">
                                    <tbody>
                                    <tr>
                                        <td><i class="fa fa-user"></i> Фамилия:</td>
                                        <td><?= $value['last_name'];?></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-male"></i> Акаунт:</td>
                                        <td><?= $value['username'];?></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-key"></i> Пароль:</td>
                                        <td class="muted">скрыт</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-users"></i> Група:</td>
                                        <td>Пользователи</td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-birthday-cake"></i> Возраст:</td>
                                        <td><?= $value['age'];?></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-home"></i> Откуда:</td>
                                        <td><?= $value['city'];?></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-envelope-o"></i> Електроная почта:</td>
                                        <td><?= $value['email'];?></td>
                                    </tr>
                                    <tr>
                                        <td><i class="fa fa-sign-in"></i> Зарегистрирован:</td>
                                        <td><i class="fa fa-clock-o"></i> <?= $value['register_time'];?> , <i class="fa fa-calendar"></i> <?= $value['register_time'];?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
					<a class="btn btn-inverse" id="message" data-toggle="modal" href='#modal-id'><i class="icon-envelope icon-white"></i></a>
						
						<? if  (($value['username']==$_COOKIE['username']) or ($this->admin)) {/*Пользователь может редактировать свои даные, если зашол он*/?>
							<button id="edit" class="btn btn-success" type="button"><i class="icon-edit icon-white"></i> Редактировать данные</button>
							<a href="/?delUser/<?=$value['username']?>" class="btn btn-danger" onclick="return confirm('Точно удалить, подумай назад пути нет... ?');"><i class="icon-remove icon-white"></i> Удалить пользователя</a>
						<? } ?>
                        
                    </div>
                </div>
            </div>

	<div class="span2 well" id="fields" style="display:none;width:200px;">
		<legend><i class="fa fa-info-circle"></i> Данные</legend>
		<? echo $this->error; ?>
		<form id="information_form" method="POST">

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-user"></i></span>
		<input class="span2" id="prependedInput" type="text" name="user" placeholder="Пользователь" value="<?= $value['username'];?>" disabled="disabled">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-key"></i></span>
		<input class="span2" id="prependedInput" type="password" name="pass" placeholder="Пароль" value="<?= $value['password'];?>">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-male"></i></span>
		<input class="span2" id="prependedInput" type="text" name="name" placeholder="Имя" maxlength="15" value="<?= $value['name'];?>">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-male"></i></span>
		<input class="span2" id="prependedInput" type="text" name="last_name" placeholder="Фамилия" maxlength="15" value="<?= $value['last_name'];?>">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-calendar"></i></span>
		<input class="span1" id="prependedInput" type="text" name="age" placeholder="Возраст" maxlength="2" title="0-9" value="<?= $value['age'];?>">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-compass"></i></span>
		<input class="span2" id="prependedInput" type="text" name="city" placeholder="Город" value="<?= $value['city'];?>">
		</div>

		<div class="input-prepend">
		<span class="add-on"><i class="fa fa-at"></i></span>
		<input class="span2" id="prependedInput" type="text" name="email" placeholder="Електроная почта" value="<?= $value['email'];?>" disabled="disabled">
		</div>
		<button type="submit" name="submit" class="btn btn-primary btn-block">Изменить информацию <i class="fa fa-pencil-square-o"></i></button>
		<button id="cancel" class="btn btn-danger btn-block" type="button"> Отменить <i class="fa fa-ban"></i></button>
		</form>    
	</div>

	<div id="avatar" style="display:none" class="span4 well">
	<legend><i class="fa fa-camera"></i> Аватар</legend>

		<div id="drop-files" ondragover="return false">

			<!-- Область для перетаскивания -->
			<p>Перетащите изображение сюда</p>
	        <form id="frm">
	        	<input type="file" id="uploadbtn" file="<?= $value['avatar'];?>"/>
	        </form>
		</div>

	</div>

	<div id="preview" class="span3 well" style="width:300px;">
	<legend><i class="fa fa-picture-o"></i> Предпросмотр</legend>

    <!-- Область предпросмотра -->
	<div id="uploaded-holder"> 
		<div id="dropped-files">
        	<!-- Кнопки загрузить и удалить, а также количество файлов -->
        	<div id="upload-button">
             		
						<button id="delete" class="btn btn-danger btn-block" type="button"><i class="fa fa-times"></i> Отмена</button>
					
              
			</div>  
        </div>
	</div>

	</div>
</div>
</div>
<? } ?>

<script src="my_js/info.js"></script>

<div class="modal fade" id="modal-id" style="display:none;">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Сообщение:</h4>
			</div>
			<div class="modal-body">
				<input type="text" name="message" id="input" class="input-xxlarge" placeholder="Сообщение пользователю <?= $value['name'];?>">
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Отмена</button>
				<button type="button" class="btn btn-primary"><i class="fa fa-envelope"></i> Отправить</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->