//Main.php

// Главные сприпты-->
$( document ).ready(function() {
//Cookies
if(!$$c.get('bgcolor'))
{
color_cookies='#f5f5f5';
$$c.set('bgcolor', '#f5f5f5', 10000);
}
else
{
color_cookies=$$c.get('bgcolor');
//alert(color_cookies);
}


//Reset Color
$('#reset_color').click(function() {
  $('body').css("background-color", '#f5f5f5');
  $$c.set('bgcolor', '#f5f5f5', 10000);
  //color_cookies='#f5f5f5';
$('.accordion-heading').css("color", "black");
$('a.topic').css("color", "black");
});


//Left Panel
var flag=0;
$('.open').on('click', function(){
if(flag==0)
  {$("div.left_panel").animate({left:'560px'},500); $(".open").css('color','white'); flag=1; return;}
if(flag==1)
 {$("div.left_panel").animate({left:0},500); $(".open").css('color',''); flag=0; return;}
});


//Скрываем слайд-алерты
$(function() {

$(".alert-box .toggle-alert").click(function(){
  $(this).closest(".alert-box").slideUp();
  return false;
});
});


//Slide Down Notification
$(function() {
var myMessages = ['info','warning','error','success']; // define the messages types    
function hideAllMessages()
{
     var messagesHeights = new Array(); // this array will store height for each
   
     for (i=0; i<myMessages.length; i++)
     {
          messagesHeights[i] = $('.' + myMessages[i]).outerHeight();
          $('.' + myMessages[i]).css('top', -messagesHeights[i]); //move element outside viewport   
     }
}

     //Скрыть все слайды
     hideAllMessages();
     
     //Скрыть по нажатию на сообщение
     $('.message').click(function(){   
          $(this).animate({top: -$(this).outerHeight()}, 500, function() {
          $('.message').hide('slow');
          });
      });  


});  


//Color Picker
  $("#flat").ColorPickerSliders({
      flat: true,
  swatches: false,
  color: color_cookies,
      invalidcolorsopacity: 0,
              onchange: function(container, color) {
              var body = $('body');
              var acordion = $('.accordion-heading');
              var topic = $('a.topic');

              body.css("background-color", color.tiny.toRgbString());
              $$c.set('bgcolor', color.tiny.toRgbString(), 10000);

              if (color.cielch.l < 60) {
                  //body.css("color", "white");
                  acordion.css("color", "white");
                  topic.css("color", "white");
              }
              else {
                  //body.css("color", "black");
                  acordion.css("color", "black");
                  topic.css("color", "black");
              }
          }
  });

  
  //Если пользователь зарегестрировался
  if($$c.get('registred'))
  {
  // hideAllMessages();   
  $('.warning').show('slow', function() {
     $('.warning').animate({top:"0"}, 500); //Slide
  });
 

  $$c.set('registred', '', 0);
  }    

            //Если пользователь активировал акаунт
            if($$c.get('actived'))
            {
            // hideAllMessages();   
            $('.success').show('slow', function() {
               $('.success').animate({top:"0"}, 500); //Slide
            });

            $$c.set('actived', '', 0);
            } 

                      //Если пользователь удалил акаунт
                      if($$c.get('deleted'))
                      {
                      // hideAllMessages();   
                      $('.error').show('slow', function() {
                         $('.error').animate({top:"0"}, 500); //Slide
                      });

                      $$c.set('deleted', '', 0);
                      } 

                                  //Все пользователи удалены
                                  if($$c.get('users_del'))
                                  {
                                  // hideAllMessages();   
                                  //$('.error_users').animate({top:"0"}, 500); //Slide
                                  $('.error-box').slideDown();

                                  $$c.set('users_del', '', 0);
                                  } 

                                              //Все коментарии удалены
                                              if($$c.get('comments_del'))
                                              {
                                              // hideAllMessages();   
                                              //$('.error_comments').animate({top:"0"}, 500); //Slide
                                              $('.download-box').slideDown();

                                              $$c.set('comments_del', '', 0);
                                              } 

      //Стрелка Вверх
      $(function () {
          $.scrollUp({
          animation: 'fade',
          //activeOverlay: '#00FFFF',
          scrollImg: { active: true, type: 'background', src: 'img/top.png' }
          });
      });

});
