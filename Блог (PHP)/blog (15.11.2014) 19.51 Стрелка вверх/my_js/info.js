//Info.php
$(document).ready(function() {

	$('#edit').click(function(event) {
		$('#info').fadeOut('slow', function() {
			$('#fields').fadeIn('slow');
			$('#avatar').fadeIn('slow');
		});

	});

	$('#cancel').click(function(event) {
		$('#avatar').fadeOut('slow');
		$('#preview').fadeOut('slow');
		$('#fields').fadeOut('slow', function() {
			$('#info').fadeIn('slow');
		});

	});

	$('#message').click(function(event) {
		$('#modal-id').show();
	});





	//Ограничения на ввод символов в поле A-Z (64-91) a-z (96-123) Backspace (8) Shift (16) TAB (9)
	$('input[name=user]').keypress(function(event) {
		if (!((event.charCode>64 && event.charCode<91) || (event.charCode>96 && event.charCode<123) || event.which==8 || event.keyCode==9 || event.which==16 )) return false;
		if (event.ctrlKey) return false //запрет CTRL+V, CTRL+X, CTRL+C
	});

	//Ограничения на ввод символов в поле 0-9 (47-58) Backspace (8) TAB (9)
	$('input[name=age]').keypress(function(event) {
		if (!((event.charCode>47 && event.charCode<58) || event.which==8 || event.keyCode==9 )) return false;
		if (event.ctrlKey) return false //запрет CTRL+V, CTRL+X, CTRL+C
	});
	

// Проверка полей в info.php
$('#information_form').submit(function () {
var flag = true;
//uploadFiles(); //!!!!!!! Рабочая

	//Пользователь
	if ($('input[name=user]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите пользователя", { type: 'success' });
			    }, 100);
	flag=false;
	};

    //Пароль
	if ($('input[name=pass]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите пароль", { type: 'info' });
			    }, 500);
	flag=false;
	};

    //Имя
	if ($('input[name=name]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите имя", { type: 'warning' });
			    }, 1000);
	flag=false;
	};

	//Фамилия
	if ($('input[name=last_name]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите фамилию", { type: 'danger' }); 
			    }, 1500);
	flag=false;
	};

	//Возраст
	if ($('input[name=age]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите возраст", { type: 'success' });
			    }, 2000);
	flag=false;
	};

	//Город
	if ($('input[name=city]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите город", { type: 'info' });
			    }, 2500);
	flag=false;
	};

	//Електроная почта
	if ($('input[name=email]').val() == '')
	{
			    setTimeout(function() {
			        $.bootstrapGrowl("Ошибка: ","Введите електроную почту", { type: 'warning' });
			    }, 3000);
	flag=false;
	};

//uploadfiles();
if (flag == false)
{
  return false;
}
else{$('input[name=user]').removeAttr('disabled');};
    
});

});