//Control.php
$(document).ready(function() {

	$('select').click(function() {
		user=$(this).val(); //Выделеный пользователь
		$('#delete').removeAttr('disabled'); //Включить елемент
		$('#delete').attr("href","/?delUser/"+user);

		$('#activate').removeAttr('disabled'); //Включить елемент
		$('#activate').attr("href","/?active/"+user+"/yes");

		$('#edit').removeAttr('disabled'); //Включить елемент
		$('#edit').attr("href","/?info/"+user);

		$('#deactivate').removeAttr('disabled'); //Включить елемент
		$('#deactivate').attr("href","/?deactive/"+user);
		//alert(user);
	});
	
});
