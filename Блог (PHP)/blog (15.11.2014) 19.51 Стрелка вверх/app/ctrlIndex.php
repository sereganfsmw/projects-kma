<?php
session_start(); //Начать сессию
//Роутер

//Отправка почты
class TEmail{
    public $from_email;
    public $from_name;
    public $to_email;
    public $to_name;
    public $subject;
    public $data_charset='UTF-8';
    public $send_charset='windows-1251';
    public $body='';
    public $type='text/plain';

        function mime_header_encode($str, $data_charset, $send_charset)
            {
            if($data_charset != $send_charset)
                $str=iconv($data_charset,$send_charset.'//IGNORE',$str);
            return ('=?'.$send_charset.'?B?'.base64_encode($str).'?=');
            }
            
        function send(){

            $dc=$this->data_charset;
            $sc=$this->send_charset;
            //Кодируем поля адресата, темы и отправителя
            $enc_to=$this->mime_header_encode($this->to_name,$dc,$sc).' <'.$this->to_email.'>';
            $enc_subject=$this->mime_header_encode($this->subject,$dc,$sc);
            $enc_from=$this->mime_header_encode($this->from_name,$dc,$sc).' <'.$this->from_email.'>';
            //Кодируем тело письма
            $enc_body=$dc==$sc?$this->body:iconv($dc,$sc.'//IGNORE',$this->body);
            //Оформляем заголовки письма
            $headers='';
            $headers.="Mime-Version: 1.0\r\n";
            $headers.="Content-type: ".$this->type."; charset=".$sc."\r\n";
            $headers.="From: ".$enc_from."\r\n";
            //Отправляем
            return mail($enc_to,$enc_subject,$enc_body,$headers);
            }
}

class ctrlIndex extends ctrl {
    
    function index(){
        
        $this->posts = $this->db->query("SELECT * FROM post ORDER BY ctime DESC")->all();
        //echo "Hello, MVC World!!";
        $this->out('posts.php');
    }

    function control(){
        //if (!$this->admin) return header ("Location: /");

        $this->posts = $this->db->query("SELECT * FROM post ORDER BY ctime DESC")->all();
        $this->users = $this->db->query("SELECT * FROM users")->all();
        //echo "Hello, MVC World!!";
        $this->out('control.php');
    }

    function AllUsersDELETE () {//Удалить всех пользователей
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("TRUNCATE TABLE users");
        setcookie('users_del', 'deleted', time()+86400*30, '/');
        header("Location: /");
    }

    function AllCommentsDELETE () {//Удалить все коментарии
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("TRUNCATE TABLE comment");
        setcookie('comments_del', 'deleted', time()+86400*30, '/');
        header("Location: /");
    }
    function info ($user) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");

        if(!empty($_POST)){
        
        $user=$_POST['user'];
        $pass=$_POST['pass'];
        $name=$_POST['name'];
        $last_name=$_POST['last_name'];
        $age=$_POST['age'];
        $city=$_POST['city'];

        $this->db->query("UPDATE users SET password = ?, name = ?, last_name = ?, age = ?, city = ? WHERE username = ?",$pass,$name,$last_name,$age,$city,$user);
        header("Location: /?info/".$user);
        }            
        if ($user == 'admin') {echo "<center><h3 class='muted'><i class='fa fa-ban'></i> Страница Администратора не доступна для простых пользователей ...</h3></center>";}
        $this->users = $this->db->query("SELECT * FROM users WHERE username = ?",$user)->all();

        $this->out('info.php');
    }

    function delUser ($user) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM users WHERE username = ?",$user);
        setcookie('deleted', $user, time()+86400*30, '/');

        setcookie('uid', '', 0, '/');
        setcookie('key', '', 0, '/');
        setcookie('name', '', 0, '/');
        setcookie('admin', '', 0, '/');
        setcookie('username', '', 0, '/');
        session_unset(); //Очистить переменые сессии
        header("Location: /");
    }

    function reg(){
        
        if(!empty($_POST)){
            
            $active='0';//Пользователь зарегистрирован но не активирован
            $user=$_POST['user'];
            $pass=$_POST['pass'];
            $name=$_POST['name'];
            $last_name=$_POST['last_name'];
            $age=$_POST['age'];
            $city=$_POST['city'];
            $mail=$_POST['email'];
            $avatar='users/'.$user.'/thumbs.jpg';
            $key=md5(date('YmdHis'));

            $register_time = date("H:i:s");
            $register_date = date("d.m.y");

            $this->db->query("INSERT INTO users (active,username,password,name,last_name,age,city,email,avatar,register_time,register_date,key_user) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)",$active,$user,$pass,$name,$last_name,$age,$city,$mail,$avatar,$register_time,$register_date,$key);
           
/*
$email->from_email='personal-blog.esy.es@mail.com';
$email->from_name='PersonalBlog';
$email->to_email='personal.notes@yandex.ru';
$email->to_name='Имя пользователя';
$email->subject='Активация акаунта';
$email->type='text/html';
*/		   
            $email=new TEmail;
            $email->from_email='personal-blog.esy.es@mail.com';
            $email->from_name='PersonalBlog';
            $email->to_email=$mail;
            $email->to_name=$name;
            $email->subject='Активация акаунта';
            $email->type='text/html';
            $email->body='			
            <body style=" border: 3px dotted white; padding: 20px;
            background: #a9e4f7; /* Old browsers */
            background: -moz-linear-gradient(top,  #a9e4f7 0%, #0fb4e7 100%); /* FF3.6+ */
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#a9e4f7), color-stop(100%,#0fb4e7)); /* Chrome,Safari4+ */
            background: -webkit-linear-gradient(top,  #a9e4f7 0%,#0fb4e7 100%); /* Chrome10+,Safari5.1+ */
            background: -o-linear-gradient(top,  #a9e4f7 0%,#0fb4e7 100%); /* Opera 11.10+ */
            background: -ms-linear-gradient(top,  #a9e4f7 0%,#0fb4e7 100%); /* IE10+ */
            background: linear-gradient(to bottom,  #a9e4f7 0%,#0fb4e7 100%); /* W3C */
            ">

			<h1>Здраствуйте!</h1>
			<p>Спасибо что вы зарегистрировались в системе блогов.</p>
			<center><font style="color:white;font-size: 40pt;"><b>PersonalBlog</b></font></center>
			<p><u>Ваши данные:</u></p>
            <hr>
            <ul>
    			<li><b>Пользователь:</b><font color="white"> '.$user.'</font></li>
    			<li><b>Пароль:</b><font color="white"> '.$pass.'</font></li>
    			<li><b>Имя:</b><font color="white"> '.$name.'</font></li>
    			<li><b>Фамилия:</b><font color="white"> '.$last_name.'</font></li>
    			<li><b>Возраст:</b><font color="white"> '.$age.'</font></li>
    			<li><b>Откуда вы:</b><font color="white"> '.$city.'</font></li>
    			<li><b>Почта:</b><font color="white"> '.$mail.'</font></li>
            </ul>
            <hr>
			<font color="white">Время регистрации: '.$register_time.'</font>
            <p><font color="white"><b><i>[ Для активации акаунта проследуйте по сылке</i></b> (<a style="color:red;text-decoration: none;" href="http://personal-blog.esy.es/?active/'.$user.'/'.$key.'"><b>активировать</b></a>) ]</font></p><br>
            <div style="text-align: center;color:white;border: 4px double white;">С ув. Администратор, <b>Серега Мост<b></div>
            </body>
            ';
            $email->send();

            setcookie('registred', $user, time()+86400*30, '/');
            header("Location: /");
        }
        $this->out('reg.php');
    }

    function deactive ($user) {
        
        $this->db->query("UPDATE users SET active = ? WHERE username = ?", '0',$user);

        header("Location: /?login");
    }

    function active ($user,$key) {
        
        $this->db->query("UPDATE users SET active = ? WHERE username = ? AND key_user = ?", '1',$user,$key);

            if ($this->admin) {//Если зашол админ он может активировать без ключа
                $this->db->query("UPDATE users SET active = ? WHERE username = ?", '1',$user);
                }

        setcookie('actived', $user, time()+86400*30, '/');
        header("Location: /?login");
    }
    
    function home(){
        $this->out('home.php');
    }

    function login(){
$error="
<div class='alert alert-danger'>
    <button type='button' class='close' data-dismiss='alert'>&times;</button>
    <strong>Ошибка!</strong><br> Неправильный логин или пароль. <i class='fa fa-exclamation-triangle'></i>
</div>
";
$active_message="
<div class='alert alert-warning'>
    <button type='button' class='close' data-dismiss='alert'>&times;</button>
    <strong>Внимание!</strong><br> Вы не активировали акаунт. <i class='fa fa-exclamation-triangle'></i>
</div>
";

        if(!empty($_POST)){
$active = $this->db->query("SELECT active FROM users WHERE username = ?",$_POST['login'])->assoc();
$status=$active['active'];

            $user = $this->db->query("SELECT * FROM users WHERE username = ? AND password = ?",$_POST['login'],$_POST['pass'])->assoc();
            $admin = $this->db->query("SELECT * FROM admin WHERE login = ? AND pass = ?",$_POST['login'],md5($_POST['pass']))->assoc();

            $this->error = $error;
            //echo $user;
            if ($user) { //Если пользователь зарегистрирован

                    if ($status==1) {//Пользователь зарегистрирован и активирован
                        $key = md5(microtime().rand(0, 10000));
                        setcookie('uid', $user['id'], time()+86400*30, '/');
                        setcookie('key', $key, time()+86400*30, '/');
                        setcookie('name', $user['name'], time()+86400*30, '/');
                        setcookie('username', $user['username'], time()+86400*30, '/');
                        $this->db->query("UPDATE users SET cookie = ? WHERE id = ?",$key,$user['id']);
                        $_SESSION['user'] = true;
                        header("Location: /");
                    } else {$this->error = $active_message;}

            } //else {$this->error = $error;}

            if ($admin) {
                //Алминистратор
                $key = md5(microtime().rand(0, 10000));
                setcookie('uid', $admin['id'], time()+86400*30, '/');
                setcookie('key', $key, time()+86400*30, '/');
                setcookie('name', $admin['name'], time()+86400*30, '/');
                setcookie('username', $admin['login'], time()+86400*30, '/');
                $this->db->query("UPDATE admin SET cookie = ? WHERE id = ?",$key,$admin['id']);
                $_SESSION['admin'] = true;
                header("Location: /");
            } //else {$this->error = $error;}
            //{$this->error = "<script>alert_warning();</script>";}
            
        }
        
        $this->out('login.php');
    }
    
    function logoff () {
        setcookie('uid', '', 0, '/');
        setcookie('key', '', 0, '/');
        setcookie('name', '', 0, '/');
        setcookie('admin', '', 0, '/');
        setcookie('username', '', 0, '/');
        session_unset(); //Очистить переменые сессии
        return header("Location: /");
    }
            
    function add() {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if (!empty($_POST['title'])) {
            $date = date("d.m.y");
            $time = date("H:i:s");

            $this->db->query("INSERT INTO post (author,ctime,cdate,title,post) VALUES(?,?,?,?,?)",$_COOKIE['name'],$time,$date,htmlspecialchars($_POST['title']),$_POST['content']);
            header("Location: /");
        }
        
        $this->out('add.php');
    }
    
    function del ($id) {
        //echo $id;
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM post WHERE id = ?",$id);
        header("Location: /");
    }
    
    function edit ($id) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        if (!empty($_POST)){
            $this->db->query("UPDATE post SET title = ?, post = ? WHERE id = ?",  htmlspecialchars($_POST['title']),$_POST['content'],$id);
            return header("Location: /");
        }
        $this->post = $this->db->query("SELECT * FROM post WHERE id = ?",$id)->assoc();
        $this->out('add.php');
    }

    function topic ($id) {
        
        $this->post = $this->db->query("SELECT * FROM post WHERE id = ?",$id)->assoc();
        
        $this->comments = $this->db->query("SELECT * FROM comment WHERE postid = ? ORDER BY id DESC",$id)->all();
        $this->out('topic.php');
    }
    
    function addComment ($postid){
        $user = $_COOKIE['username'];
        $name = $_COOKIE['name'];
        $post = $_POST['post'];
        $time = date("H:i:s");
        $date = date("d.m.y");
        $avatar='users/'.$user.'/thumbs.jpg';


        $this->db->query("INSERT INTO comment(postid,username,name,post,comment_time,comment_date,avatar) VALUES(?,?,?,?,?,?,?)",$postid,$user,$name,$post,$time,$date,$avatar);
        setcookie('comment_add', 'true', time()+86400*30, '/');
        header("Location: /?topic/". intval($postid));
               
    }
    
    function delComment ($commId,$postid) {
        if ((!$this->user) and (!$this->admin)) return header ("Location: /");
        
        $this->db->query("DELETE FROM comment WHERE id = ?",$commId);
        header("Location: /?topic/". intval($postid));
    }
}