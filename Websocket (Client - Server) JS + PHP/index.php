<!DOCTYPE html>
<html>
<head>
<head>
	<!--<meta charset='UTF-8'>-->
	<meta charset='windows-1251'>
	
	<title>Lab 5 by MoST</title>
	
    <!-- jQuery -->
<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.js" type="text/javascript"></script>

    <!-- Bootstrap -->
<link href="css/bootstrap.css" rel="stylesheet">

    <!-- Font-awesome -->
<link href="css/font-awesome.css" rel="stylesheet">

    <!-- Lab 5 -->
<link href="css/lab_5.css" media="all" rel="stylesheet">
<link href="css/grid.css" media="all" rel="stylesheet">
<!--<link href='http://fonts.googleapis.com/css?family=Alegreya+SC:700,400italic' rel='stylesheet' type='text/css' />-->
</head>

<body>	

<script language="javascript" type="text/javascript">  
$(document).ready(function(){
	//create a new WebSocket object.
	var wsUri = "ws://localhost:9000/server.php"; 	
	websocket = new WebSocket(wsUri); 

	//connection is closed
	websocket.onclose 	= function(ev){
		$('.container').slideUp();
		//alert('Connection Closed , Server not Running...');
		$('#images').fadeOut();
		$('#leftborder').hide();
		$('#show_gallery').attr('disabled','disabled');
		$('#status').html('Status: <span class="badge badge-inverse">OFFLINE</span>');
		$('#myModal').modal('hide');
		$('#grids').fadeOut('slow', function() {
			$('#main').fadeIn('slow');
		});
	}; 
	
	/*websocket.onerror	= function(ev){
	alert('ERROR');
	};*/
	
	//connection is open 
	websocket.onopen = function(ev) { 
		$('.container').slideUp();
		$('#leftborder').show();
		//alert('Connected');
		$('#show_gallery').removeAttr('disabled');
		$('#status').html('Status: <span class="badge badge-success">ONLINE</span>');
	}
	
	
	websocket.onmessage = function(ev) {
		var msg = JSON.parse(ev.data); //PHP sends Json data
		var type = msg.type; //���
		var id = msg.id; //id ��������
		var image = msg.image; //���� � ��������
		var about = msg.about; //�������� ��������
		var user = msg.user; //������������
$( "#users" ).append( "<p>"+user+"</p>" );
		
	if (image == selected)
	{
	$('#modal-img').attr('src', image);

	$('#modal-id').val(id);
	$('#modal-path').val(image);
	$('#modal-about').val(about);	

	$('#myModal').modal('show');
	}
}

			$("img").click(function(event) {
				selected=$(this).attr('src');
				//selected=$(this).parent().parent().find('#pic').attr('src');
				//alert(selected);
				
				//prepare json data
				var msg = {
				id: '',
				image: selected,
				about : ''
				};

			//convert and send data to server
			websocket.send(JSON.stringify(msg));
			});


			$('#show_gallery').click(function(event) {
				$('#main').fadeOut('slow', function() {
					$('#grids').fadeIn('slow');
				});
			});
	
});
</script>

			
<div id="main">
<h1 class="muted"><i class="fa fa-picture-o"></i> Lab 5 Hover Image</h1>
<span><u>�������� 505 ��:</u> <br><br><i class="fa fa-chevron-down"></i>�������� �.�. & ������ �.�.<i class="fa fa-chevron-down"></i></span>
<p>������������ Slide-�������� ��� ��������� ���������� � <b>����������������� ������� <i class="fa fa-users"></i></b>.<br>� ��������������  Web-Sockets (PHP) & JQuery (JSON) & Responsive Grid (CSS)</p>
<button id="show_gallery" class="btn btn-danger text-center" type="button" disabled="disabled">Gallery <i class="fa fa-th"></i></button>
<p class="muted"><b><i class="fa fa-cog fa-spin"></i> Server v2.0</b></p>
<p id="status"></p>


<div class="container">
<div class="progress progress-info progress-striped active">
<div class="bar" style="width: 100%">Conecting</div>
</div>

<div class="progress progress-success progress-striped active">
<div class="bar" style="width: 100%">to</div>
</div>

<div class="progress progress-warning progress-striped active">
<div class="bar" style="width: 100%">Server</div>
</div>

<div class="progress progress-danger progress-striped active">
<div class="bar" style="width: 100%">Please Wait...</div>
</div>
</div>


<h1 id="most" class="muted">Design by <u>Serega MoST</u>. Copyright � 2014 All Rights Reserved <i class="fa fa-exclamation-triangle"></i></h1>
</div>

<style>
#leftpanel{
display:none;
position: absolute;
background: black;
width: 1px;
height:100%;
z-index:99999;
border-right: 1px solid white;
color:white;
font-size: 15px;
line-height: 10px;
}

#leftborder{
display:none;
position: absolute;
background: black;
width: 1px;
height:100%;
z-index:99999;
color:white;
}
</style>

<div id="leftpanel">
<p><u>Users:</u></p>
<div id="users" style="display:none;"></div>

</div>
<div id="leftborder">
</div>

<script>
$("#leftborder").mouseenter(function() {
	$("#leftpanel").show().animate({width: "50%"}, 500,function(){$("#users").fadeIn()});
});

$("#leftpanel").mouseleave(function() {
	$("#users").hide();
	$("#leftpanel").animate({width: "1px"}, 500,function(){$("#leftpanel").hide()});
});
</script>

<div style="display:none" id="grids">
	<section id="photos">
<?php 
$d = opendir("img");
$files = array();
while ( false !== ($name = readdir($d)) ){
    if ($name == "." or $name == ".." )
        continue;
$files[] = $name;
}
shuffle($files); //������������� ������

//print_r($files);
//echo "<br>";
for ($i = 0; $i <= (count($files)-1); $i++)
{
//echo $files[$i];

$rand = $files[rand(0, count($files)-1)];

echo "<div class='box'><img id='pic' src='img/$files[$i]'>";
echo "<div class='inner' width='100%' height='100%'><img src='img/$rand'></div>";
//echo "<div class='inner' width='100%' height='100%'><img src='img/Wallpaper (574).jpg'></div>";
echo "</div>";
} 

if(empty($files)) {
echo 'Not Found';
}

closedir($d);
?>
	</section>
</div>

<script src="js/jquery-hoverdirection.js"></script> 
<script>
$(function () {

  $('.box').hoverDirection();
  
  // Example of calling removeClass method after a CSS animation
  $('.box .inner').on('animationend', function (event) {
    var $box = $(this).parent();
    $box.filter('[class*="-leave-"]').hoverDirection('removeClass');
  });
});
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only"></span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-info-circle"></i> ����������:</h4>
      </div>
      <div class="modal-body">

    	<img id="modal-img" class="img-polaroid span2">

      	<div class="input-append" style="margin:10px;">
      		<input id="modal-id" class="span4" id="appendedInput" type="text" readonly>
      		<span class="add-on"><i class="fa fa-tag"></i></span>
      	</div>
      	<div class="input-append" style="margin:10px;">
      		<input id="modal-path" class="span4" id="appendedInput" type="text" readonly>
      		<span class="add-on"><i class="fa fa-picture-o"></i></span>
      	</div>
      	<div class="input-append" style="margin:10px;">
      		<input id="modal-about" class="span4" id="appendedInput" type="text" readonly>
      		<span class="add-on"><i class="fa fa-pencil-square-o"></i></span>
      	</div>

  </div>
      <div class="modal-footer">
      	
        <button type="button" class="btn btn-default" data-dismiss="modal">�������</button>
      </div>
    </div>
  </div>
</div>

</body>
</html>